<?php
use App\Libraries\Helper;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/testGlass', function(){
    $custArr = array('cust_name' => "test" , 'cust_code' => '5803f2f6383b2098');
    $result = Helper::get_glass_token($custArr);
    print_r($result);
});




$router->post('tests/exec', 'ExampleController@exec');
$router->post('tests/noti', 'ExampleController@pushnoti');

$router->post('users', 'UserController@addUser');
$router->post('admin/createUser', 'UserController@addUserByAdmin');
$router->put('admin/users/{userId}', 'UserController@updateUserByAdmin');

$router->post('users/login', 'UserController@login');
$router->post('users/resetPassword', 'UserController@resetPassword');
$router->post('users/updatePassword', 'UserController@updatePassword');
$router->get('users/getChannel/{userId}', 'UserController@getChannel');
$router->get('users/getChannelByContactId/{contactId}', 'UserController@getChannelByContactId');
$router->get('users/_me/roles', 'UserController@getMyRoles');
$router->get('users/_me/permissions', 'UserController@getMyPermissions');
$router->put('users/_me/updatePassword', 'UserController@updateMyPassword');
$router->post('users/logout', 'UserController@logout');
$router->post('users/doesEmailExist', 'UserController@doesEmailExist');
$router->post('users/acceptTermAndCondition', 'UserController@acceptTermAndCondition');
$router->get('users/event', 'UserController@getListEventCanConfig');
$router->put('users/updateEvent', 'UserController@configEventCanConfig');

$router->put('users/quickCode', 'UserController@setQuickCode');
$router->post('users/isCorrectQuickCode', 'UserController@isCorrectQuickCode');

$router->put('users/{id}', 'UserController@updateUser');
$router->get('users/{id}', 'UserController@getUserById');
$router->post('type_codes/_find', 'TypeCodeController@find');

$router->get('users/_me/config', 'UserController@getMyConfig');
$router->put('users/_me/config', 'UserController@setMyConfig');
$router->get('users/_me/locations', 'UserController@getMyLocations');


$router->post('users/search', 'UserController@getListUser');
$router->post('users/admin/search', 'UserController@getListUser');
$router->post('users/status', 'UserController@updateUserStatus');
$router->post('contacts', 'ContactController@create');
$router->post('contacts/_find', 'ContactController@find');
$router->post('contacts/_findCount', 'ContactController@findCount');
$router->get('contacts/_me', 'ContactController@getCurrentUserContact');
$router->put('contacts/_me', 'ContactController@updateCurrentUserContact');
$router->get('contacts/{id}', 'ContactController@getById');
$router->put('contacts/{id}', 'ContactController@update');

$router->post('contacts/admin/_find', 'ContactController@find');
$router->post('contacts/admin/_findCount', 'ContactController@findCount');

//todo: set permission
$router->post('contacts/{contact_id}/interaction', 'InteractionController@create');
$router->post('contacts/{contact_id}/interaction/_find', 'InteractionController@find');
$router->post('contacts/{contact_id}/interaction/_findCount', 'InteractionController@findCount');
$router->get('contacts/{contact_id}/interaction/{id}', 'InteractionController@getOne');
$router->put('contacts/{contact_id}/interaction/{id}', 'InteractionController@update');
$router->delete('contacts/{contact_id}/interaction/{id}', 'InteractionController@delete');

$router->post('valuations', 'ValuationController@create');
$router->post('valuations/_begin', 'ValuationController@begin');
$router->post('valuations/_find', 'ValuationController@find');
$router->post('valuations/_findCount', 'ValuationController@findCount');
$router->post('valuations/_cancel', 'ValuationController@cancel');
$router->get('testNotification/{userId}', 'TestController@testNotification');
$router->post('valuations/addInspectionItem', 'ValuationController@addInspectionItem');
$router->post('valuations/{valuationId}/{sectionType}/inspectionItemGroup', 'ValuationController@addInspectionItemGroup');
$router->put('valuations/{id}/updateVehicle', 'ValuationController@updateVehicle');
$router->post('valuations/{id}/version', 'ValuationController@createNewVersion');
$router->post('valuations/{id}/releaseVersion', 'ValuationController@releaseVersion');
$router->post('valuations/{id}/cancelModifyingVersion', 'ValuationController@cancelModifyingVersion');
$router->get('valuations/{id}/version', 'ValuationController@getAllVersion');
$router->get('valuations/{id}/version/{version}', 'ValuationController@getValuationVersion');
$router->get('valuations/{id}/version/{version}/inspectionItems', 'ValuationController@getInspectionItemOfOldVersion');


$router->put('valuations/updateGeneralReconditionCost/{valuationId}', 'ValuationController@updateGeneralReconditionCost');
$router->get('valuations/getGeneralReconditionCost/{valuationId}', 'ValuationController@getGeneralReconditionCost');

$router->post('valuations/{id}/_accept', 'ValuationController@accept');

$router->put('valuations/{id}/requestValuation', 'ValuationController@requestValuation');

$router->get('valuations/{id}/wholesaleValues', 'ValuationController@getWholesaleValues');
$router->put('valuations/{id}/wholesaleValues', 'ValuationController@updateWholesaleValues');

$router->get('valuations/{id}', 'ValuationController@getValuation');

$router->get('valuations/{id}/reconditioningCost', 'ValuationController@getReconditioningCost');
$router->put('valuations/{id}/reconditioningCost', 'ValuationController@updateReconditioningCost');

$router->get('valuations/{id}/salesInfo', 'ValuationController@getSalesInfo');
$router->put('valuations/{id}/salesInfo', 'ValuationController@updateSalesInfo');

$router->get('valuations/getInspectionItem/{valuationId}/{sectionGroup}', 'ValuationController@getInspectionItem');
$router->get('valuations/getInspectionItem/{valuationId}', 'ValuationController@getAllInspectionItem');

$router->get('valuations/getValuationDetail/{valuationId}', 'ValuationController@getValuationDetail');
$router->get('valuations/getVehicleDetail/{valuationId}', 'ValuationController@getVehicleDetail');

$router->put('valuations/{id}', 'ValuationController@update');
$router->put('valuations/{valuationId}/inspectionItem/{inspectionItemId}', 'ValuationController@updateInspectionItem');

$router->delete('valuations/{valuationId}/inspectionItem/{inspectionItemId}', 'ValuationController@deleteInspectionItem');
$router->delete('valuations/{valuationId}/{sectionType}/inspectionItemGroup', 'ValuationController@deleteInspectionItemGroup');

$router->get('valuations/{id}/mechanicalDetails', 'ValuationController@getMechanicalDetail');
$router->put('valuations/{id}/mechanicalDetails', 'ValuationController@updateMechanicalDetail');

$router->post('valuations/_findByCustomerId', 'ValuationController@findByCustomerId');
$router->post('valuations/_countByCustomerId', 'ValuationController@countByCustomerId');

$router->get('inspectionItems/{sectionType}', 'InspectionItemController@getAll');
$router->get('inspectionItems/{valuationId}/{inspectionItemId}', 'InspectionItemController@getItem');

$router->post('makes/_find', 'MakeController@find');

$router->post('vehicleModels/_find', 'VehicleModelController@find');

$router->post('vehicleStock/_find', 'VehicleController@stockFind');
$router->post('vehicleStock/_findCount', 'VehicleController@stockCount');

//$router->post('vehicleStock/_find', 'VehicleController@stockFind');
//$router->post('vehicleStock/_findCount', 'VehicleController@stockCount');

$router->get('vehicles/getVehicle/{vin}/{vehicleYear}', 'VehicleController@getVehicle');
$router->get('vehicles/getVehicleDiagram/{bodyType}', 'VehicleController@getVehicleDiagram');
$router->get('vehicles/{id}', 'VehicleController@getOne');
$router->get('vehicles/{id}/options', 'VehicleOptionController@getOptions');
$router->put('vehicles/{id}/options', 'VehicleOptionController@updateOptions');

$router->get('vehicles/{id}/extraOptions', 'VehicleOptionController@getAllExtraOption');
$router->post('vehicles/{id}/extraOptions', 'VehicleOptionController@createExtraOption');
$router->delete('vehicles/{vehicle_id}/extraOptions/{id}', 'VehicleOptionController@deleteExtraOption');
$router->delete('vehicles/{vehicle_id}/extraOptions', 'VehicleOptionController@deleteMultiExtraOption');

$router->get('vehicles/{vehicle_id}/images', 'VehicleImageController@getPrimary');
$router->post('vehicles/{vehicle_id}/images', 'VehicleImageController@create');
$router->get('vehicles/{vehicle_id}/images/{image_id}', 'VehicleImageController@get');
$router->put('vehicles/{vehicle_id}/images/{image_id}', 'VehicleImageController@update');

$router->post('manualVehicle/create', 'VehicleForTestDriveController@create');

$router->put('valuations/{id}/complete', 'ValuationController@complete');
$router->put('valuations/{id}/approve', 'ValuationController@approve');
$router->put('valuations/{id}/reprice', 'ValuationController@reprice');
$router->put('valuations/{id}/approveReprice', 'ValuationController@approveReprice');
$router->put('valuations/{id}/rejectReprice', 'ValuationController@rejectReprice');
$router->put('valuations/{id}/trade', 'ValuationController@trade');
$router->put('valuations/{id}/tradein', 'ValuationController@tradein');

$router->post('valuations/_sync', 'ValuationController@sync');//not used yet

$router->delete('valuations/{id}', 'ValuationController@delete');

$router->put('valuations/{id}/overallRating', 'ValuationController@updateOverallRating');
//some day it will be merged
$router->put('valuations/{id}/offerPrice', 'ValuationController@updateOverallRating');

$router->get('valuations/{id}/vehicle', 'ValuationController@getValuationVehicle');

$router->get('country', 'CountryController@getAll');
$router->post('country/getByCode', 'CountryController@getByCode');

//$router->get('channel', 'ChannelController@getAll');
$router->get('channel', 'ChannelController@getMyChannel');

$router->post('glasses/export', 'GlassDataController@export');
$router->get('glasses/info', 'GlassDataController@info');
$router->get('glasses/download/{file_name}', 'GlassDataController@download');
$router->post('glasses/code/{fieldName}', 'GlassModelController@findGlassCode');
$router->get('glasses/modelDetail/{glassCode}', 'GlassModelController@getModelDetailByGlassCode');
$router->get('glasses/optionList/{glassCode}', 'GlassModelController@getOptionList');
$router->get('glasses/transmissionType', 'GlassTransmissionTypeController@getTransmissionType');

$router->get('glassModel/getFamily/{makeName}', 'GlassModelController@getFamily');
$router->get('glassModel/getVariant/{makeName}/{family}', 'GlassModelController@getVariant');
$router->get('glassModel/getReleaseYear/{makeName}/{family}', 'GlassModelController@getReleaseYear');
$router->get('glassModel/getReleaseYear/{makeName}/{family}/{variant}', 'GlassModelController@getReleaseYear');
$router->get('glassModel/getReleaseMonth/{makeName}/{family}/{releaseYear}', 'GlassModelController@getReleaseMonth');
$router->get('glassModel/getReleaseMonth/{makeName}/{family}/{releaseYear}/{variant}', 'GlassModelController@getReleaseMonth');
$router->get('glassModel/getModelDetail/{valuationId}/{makeName}/{family}/{releaseYear}/{releaseMonth}', 'GlassModelController@getModelDetail');
$router->get('glassModel/getModelDetail/{valuationId}/{makeName}/{family}/{releaseYear}/{releaseMonth}/{variant}', 'GlassModelController@getModelDetail');

$router->post('ppsr', 'PpsrController@sendEmail');

$router->post('admin/login', 'UserController@adminLogin');

$router->post('company', 'CompanyController@createCompany');
$router->post('company/updateTermAndCondition', 'CompanyController@updateMoTermAndCondition');
$router->get('company/getTermAndCondition', 'CompanyController@getTermAndCondition');
$router->put('company/{id}', 'CompanyController@updateCompany');
$router->get('company/{id}', 'CompanyController@getCompany');
$router->post('company/{id}/setAdmin', 'CompanyController@createDefaultUser');//unused

$router->post('company/_find', 'CompanyController@find');
$router->post('company/_findCount', 'CompanyController@findCount');

$router->get('companyGroups', 'CompanyGroupController@getAll');// not used
$router->post('companyGroup', 'CompanyGroupController@create');
$router->post('companyGroup/_find', 'CompanyGroupController@find');
$router->post('companyGroup/_findCount', 'CompanyGroupController@findCount');
$router->put('companyGroup/{id}', 'CompanyGroupController@update');
$router->get('companyGroup/{id}', 'CompanyGroupController@getOne');
$router->get('companyGroup/{id}/roles', 'CompanyGroupController@getRoleByCompanyGroup');


$router->get('permission', 'PermissionController@getAll');
$router->get('permissionByGroup', 'PermissionController@getAllPermissionSortByGroup');
$router->get('permission/user/{userId}', 'PermissionController@getUserPermissions');
$router->put('permission/user/{userId}', 'PermissionController@setUserPermissions');
$router->put('permission/{id}','PermissionController@updatePermission');
$router->get('role', 'RoleController@getAll');
$router->post('role', 'RoleController@create');
$router->get('role/{role_id}', 'RoleController@getOne');
$router->put('role/{role_id}', 'RoleController@update');
$router->get('role/{role_id}/permissions', 'RoleController@getRolePermissions');
$router->delete('role/{role_id}', 'RoleController@delete');
$router->post('role/search', 'RoleController@searchRolesList');


$router->post('location', 'LocationController@create');

$router->post('location/_find', 'LocationController@find');
$router->post('location/_findCount', 'LocationController@findCount');

$router->get('location', 'LocationController@getListLocationAccess');
$router->post('location/all', 'LocationController@getListAll');
$router->get('location/{id}', 'LocationController@getOne');
$router->put('location/{id}', 'LocationController@update');

$router->post('externalGroup', 'ExternalGroupController@create');
$router->get('externalGroup/{id}','ExternalGroupController@getExternalGroupById');
$router->put('externalGroup/{id}', 'ExternalGroupController@updateExternalGroup');
$router->delete('externalGroup/{id}', 'ExternalGroupController@deleteExternalGroup');


$router->get('appProperty', 'AppPropertyController@getServerProperty');

$router->put('appProperty/externalOffer', 'AppPropertyController@updateExternalOffer');
$router->get('appProperty/externalOffer', 'AppPropertyController@getExternalOffer');

$router->put('appProperty/valuationTimeout', 'AppPropertyController@updateValuationTimeout');
$router->get('appProperty/valuationTimeout', 'AppPropertyController@getValuationTimeout');

$router->put('appProperty/optionSetting', 'AppPropertyController@updateOptionSetting');
$router->get('appProperty/optionSetting', 'AppPropertyController@getOptionSetting');
$router->post('appProperty/getSetting', 'AppPropertyController@getSetting');



$router->put('appProperty/updateSpeedySetting', 'AppPropertyController@updateSpeedySetting');
$router->get('appProperty/speedySetting', 'AppPropertyController@getSpeedySetting');

$router->put('appProperty/update', 'AppPropertyController@updateAppProperty');
$router->post('appProperty/get', 'AppPropertyController@getAppProperty');

$router->post('externalOffer', 'ExternalOfferController@add');
$router->post('externalOffer/outgoing', 'ExternalOfferController@getOutgoing');
$router->post('externalOffer/bid', 'ExternalOfferController@bid');
$router->post('externalOffer/accept', 'ExternalOfferController@accept');
$router->post('externalOffer/reject', 'ExternalOfferController@reject');
$router->post('externalOffer/search', 'ExternalOfferController@searchExternalOfferGroup');

$router->get('externalOffer/group', 'ExternalOfferController@getGroup');
$router->get('externalOffer/{valuationId}', 'ExternalOfferController@getOffer');
$router->get('externalOffer/getById/{id}', 'ExternalOfferController@getById');

$router->post('externalOffer/inComing', 'ExternalOfferController@getInComing');
$router->post('externalOffer/blockVehicle', 'ExternalOfferController@blockVehicle');
$router->post('externalOffer/setLocation', 'ExternalOfferController@setLocation');
$router->post('externalOffer/setVehicleReadyToShip', 'ExternalOfferController@setVehicleReadyToShip');
$router->post('externalOffer/setTransportTriggered', 'ExternalOfferController@setTransportTriggered');
$router->post('externalOffer/setShipped', 'ExternalOfferController@setShipped');
$router->post('externalOffer/setDelivered', 'ExternalOfferController@setDelivered');
$router->post('externalOffer/offerWon', 'ExternalOfferController@getOfferWon');
$router->post('externalOffer/purchaseHistory', 'ExternalOfferController@getPurchaseHistory');
$router->post('externalOffer/soldHistory', 'ExternalOfferController@getSoldHistory');

$router->post('dealer/search','CompanyController@searchDealerLocation');

$router->post('testDrives/create', 'TestDriveController@create');
$router->get('testDrives', 'TestDriveController@getListTestDrive');
$router->post('testDrives/search', 'TestDriveController@searchTestDrive');
$router->post('testDrives/searchCount', 'TestDriveController@searchTestDriveCount');
$router->put('testDrives/complete/{testDriveId}', 'TestDriveController@completeTestDrive');
$router->get('testDrives/{id}', 'TestDriveController@getById');


$router->post('testDrives/vehicle/_find', 'VehicleController@testDrivesFind');
$router->post('testDrives/vehicle/_findCount', 'VehicleController@testDrivesCount');

//API: Auction Session
$router->post('auctions/session', 'AuctionSessionController@createAuctionSession');
$router->post('auctions/session/list', 'AuctionSessionController@getAuctionSession');
$router->put('auctions/session/{sessionId}', 'AuctionSessionController@updateAuctionSession');
$router->delete('auctions/session/{sessionId}', 'AuctionSessionController@deleteAuctionSession');
$router->get('auctions/session/{sessionId}', 'AuctionSessionController@getAuctionSessionById');
$router->get('auctions/active', 'AuctionSessionController@getActiveSession');
$router->get('auctions/nextSession', 'AuctionSessionController@getNextSession');
$router->post('auctions/session/{sessionId}/_findVehicle', 'AuctionSessionController@findVehicleInSession');
$router->post('auctions/session/{sessionId}/_countVehicle', 'AuctionSessionController@countVehicleInSession');
$router->post('auctions/session/{sessionId}/history', 'AuctionSessionController@history');

$router->post('auctions/session/{sessionId}/item/{itemId}/bid', 'BidController@bid');
$router->post('auctions/autoBid', 'BidController@autoBid');//call locally

//API: Auction
$router->post('auctions/sendToAuction', 'AuctionController@submitAuction');
$router->put('auctions/editAuctionListing/{auctionItemId}','AuctionController@editAuctionListing');
$router->post('auctions/search','AuctionController@searchByListType');
$router->get('auctions/searchItem/{auction_item}','AuctionController@searchItem');

$router->post('auctions/watchList','AuctionController@addWatchList');
$router->delete('auctions/watchList','AuctionController@removeWatchList');

$router->post('auctions/requestList','AuctionController@getRequestList');

$router->post('auctions/request/approval','AuctionController@approveOrRejectRequestAuction');

$router->post('auctions/recentView','AuctionController@addRecentView');

$router->post('auctions/trading/buyNow','AuctionController@buyNow');

//API: Auction Setting
$router->put('auctions/settings','AppPropertyController@changeAuctionSettings');
$router->get('auctions/settings','AppPropertyController@getAuctionSettings');

$router->put('auctions/session/{session_id}/items/{item_id}/userSetting', 'BidController@updateUserSetting');
$router->get('auctions/session/{session_id}/items/{item_id}/userSetting', 'BidController@getUserSetting');
$router->put('auctions/{auctionId}','AuctionController@updateAuctionItem');
$router->post('auctions/request/cancel','AuctionController@cancelAuctionRequest');
$router->get('auctions/{auctionId}','AuctionController@getAuctionById');
$router->post('auctions/resend','AuctionController@resendAuction');

//API: Wish List Management
$router->post('wishLists','WishListController@createWishList');
$router->post('wishLists/search','WishListController@searchWishList');
$router->get('wishLists/{id}','WishListController@getWishListById');
$router->put('wishLists/{id}','WishListController@updateWishList');

$router->put('transaction/_paymentReceived', 'TransactionController@paymentReceived');
$router->post('transaction/_find','TransactionController@find');
$router->post('transaction/admin/_find','TransactionController@find');
$router->post('transaction/_findCount','TransactionController@findCount');
$router->post('transaction/admin/_findCount','TransactionController@findCount');
//$router->post('transaction/{id}/dispute/item','TransactionController@addItem');
$router->get('transaction/{id}','TransactionController@getOne');
$router->put('transaction/{id}/inspect','TransactionController@doInspect');
$router->post('transaction/{id}/dispute','TransactionController@createDispute');
$router->get('transaction/{id}/dispute','TransactionController@getAllDispute');
$router->get('transaction/{id}/dispute/{dispute_id}/items','TransactionController@getAllDisputeItem');
$router->get('transaction/{id}/dispute/{dispute_id}/availableItems','TransactionController@getAllAvailableDisputeItem');
$router->put('transaction/{id}/accept', 'TransactionController@accept');
$router->put('transaction/{id}/readyForTransport', 'TransactionController@readyForTransport');
$router->put('transaction/{id}/transportBooked', 'TransactionController@transportBooked');
$router->put('transaction/{id}/inTransit', 'TransactionController@inTransit');
$router->put('transaction/{id}/shipped', 'TransactionController@shipped');
$router->put('transaction/{id}/setStatusReturning', 'TransactionController@setStatusReturning');
$router->put('transaction/{id}/setStatusReturned', 'TransactionController@setStatusReturned');
$router->put('transaction/{id}/setStatusDocumenting', 'TransactionController@setStatusDocumenting');
$router->put('transaction/{id}/changeStatus', 'TransactionController@setTransactionStatus');
$router->put('transaction/{id}/receiveSellerPayment','TransactionController@receiveSellerPayment');
$router->post('transaction/{id}/documents','TransactionController@documents');

$router->post('dispute/_findCompanyDispute','DisputeController@findCompanyDispute');
$router->post('dispute/_countCompanyDispute','DisputeController@countCompanyDispute');

$router->post('dispute/_find','DisputeController@find');
$router->post('dispute/_findCount','DisputeController@findCount');

$router->get('dispute/{id}','DisputeController@getOne');
$router->put('dispute/{id}/resolve','DisputeController@resolve');
$router->put('dispute/{id}/accept','DisputeController@accept');
$router->put('dispute/{id}/underReview','DisputeController@setUnderReview');

$router->post('dispute/{id}/note','DisputeController@createNote');
$router->delete('dispute/{id}/note/{note_id}','DisputeController@deleteNote');
$router->get('dispute/{id}/note','DisputeController@getAllNote');

//API: Menu Permission
$router->get('menu','MenuController@getListMenu');
$router->post('menu/setRole','MenuController@setMenuForRole');
$router->get('role/{roleId}/menu/','MenuController@getRoleMenu');

//API: Accounting
$router->post('accounting/{transactionId}/documents','AccountingController@uploadDocument');
$router->get('accounting/{transactionId}/documents','AccountingController@getDocument');


//API: Data Access Management (Location)
$router->post('dataAccess','DataAccessController@setDataAccessForUser');
$router->get('dataAccess/{userId}','DataAccessController@getDataAccessByUser');
$router->put('dataAccess/{userId}','DataAccessController@updateDataAccessByUser');


$router->post('notifications','NotificationMessageController@getNotificationByUser');
$router->put('appProperty/transactionFees','AppPropertyController@changeTransactionFees');
$router->get('appProperty/transactionFees', 'AppPropertyController@getTransactionFees');
$router->get('regions', 'RegionController@getRegionLists');


//todo
$router->get('dashboard','DashboardController@getListDashBoard');
$router->get('dashboard/locations','DashboardController@getAccessableLocations');
$router->post('dashboard','DashboardController@getDashboardDetail');

//test
$router->get('test','TestController@test');
$router->get('test/noti','TestController@sendTest');
$router->get('test/export','TestController@export');
$router->get('test/sendTest/{user_id}','TestController@sendTest');
$router->get('test/pdf','TestController@pdf');
$router->get('test/notice/{userId}','TestController@notice');
