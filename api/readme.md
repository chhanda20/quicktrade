# Flex office

Flexoffice backend application build with [nodejs](https://nodejs.org/en/) and using [Sails framework](http://sailsjs.org)

## Table of content
1. [document & reference](#1-document-&-reference)
2. [install & run](#2-install-&-run)
3. [note](#5-note)

## `1. document & reference`
- source manager - [bitbucket]
## `2. install & run`
### `install`
- install [php](http://php.net/downloads.php) - Start PHP server (or using apache, tomcat...)   
- install [node](https://nodejs.org/en/download/) - Run build.js file of swagger to generate apidoc   
- install [composer](https://getcomposer.org/download/) - Dependency Manager for PHP   
- install [mysql](https://www.mysql.com/downloads/) - Database   
### `run`
**install composer module**
- delete composer.lock and then retry install if any error apear
```sh
$ cd project-folder
$ sudo composer install
```
   
**import project database**
- create database   
```sh
$ mysql -u<account> -p<password>
$ create database <databasename>;
$ exit;
```

- import base data
```sh
$ cd project-folder
$ mysql -u<account> -p<password> <databasename> < moto_min.sql
```

**config environment**
- clone file .env
```sh
$ cd project-folder
$ cp .env.dev .env
```
- edit .env match with current local database
```
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=<databasename>
DB_USERNAME=<account>
DB_PASSWORD=<password>
```

**start project**   
```sh
$ cd project-folder
$ php -t public -S localhost:8082
```
**manual generate document**
```sh
$ cd project-folder/public/_doc
$ node build.js
```
- document html will be generate at project-folder/public/_doc/web/index.html

## `3. note`

This document may be expired now 😱 😱 😱, Please update and keep it alive 😘 😘 😘


## Note 26/12/2018

Login with account: admin@siliconstack.com.au and password is 12345678