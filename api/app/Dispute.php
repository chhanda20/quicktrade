<?php

namespace App;

use app\Libraries\Dashboard;
use Illuminate\Support\Facades\DB;

class Dispute extends Backend
{

    const STATUS_NEW                    = 1;
    const STATUS_IN_PROGRESS            = 2;
    const STATUS_UNDER_REVIEW           = 3;
    const STATUS_RESOLVE                = 4;

    const USER_TYPE_NO_ONE              = 0;
    const USER_TYPE_BUYER               = 1;
    const USER_TYPE_SELLER              = 2;
    const USER_TYPE_BOTH                = 3;

    const DECISION_PAY_COST_VARIANCE    = 1;
    const DECISION_NO_CHANGE            = 2;
    const DECISION_RETURN_VEHICLE       = 3;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'dispute';

    function updateFaultIdsByType($type, $autoSave = true) {
        $transaction = Transaction::find($this->transaction_id);
        switch ($type) {
            case Dispute::USER_TYPE_NO_ONE:
                $this->fault_user_ids = '';
                break;
            case Dispute::USER_TYPE_BUYER:
                $this->fault_user_ids = $transaction->buyer_id;
                break;
            case Dispute::USER_TYPE_SELLER:
                $this->fault_user_ids = $transaction->seller_id;
                break;
            case Dispute::USER_TYPE_BOTH:
                $this->fault_user_ids = $transaction->buyer_id.','.$transaction->seller_id;
                break;
        }
        if ($autoSave) {
            $this->save();
        }
    }

    public static function filterQuery($type, $user, $ownStat = NULL, $locationIds = NULL, $createDate = NULL) {
        $query = Dispute::select('dispute.*')
                    ->join('transaction', 'dispute.transaction_id', '=', 'transaction.id');

        switch ($type) {
            case Dashboard::DISPUTES_FOUND_AT_FAULT:
                $query->join('user', function ($join) {
                    $join->on(DB::raw(0), '<>', DB::raw("find_in_set(user.id, dispute.fault_user_ids)"));
                })->whereIn("user.location_id", $locationIds);
                break;
            case Dashboard::DISPUTES_RAISED_AGAINST_YOU:

                $query->join('user', function ($query) use ($locationIds) {
                    $query->on("transaction.seller_id", "user.id")
                        ->orOn("transaction.buyer_id", "user.id");
                })->where('dispute.creator_id', '<>', $user->id)
                    ->whereIn("user.location_id", $locationIds);
                break;
        }

        //filter
        if ($ownStat) {
            $query->where('user.id', $user->id);
        }

        if ($createDate && $createDate['start'] && $createDate['end']) {
            $times = [$createDate['start'], $createDate['end']];
            $query->whereBetween('dispute.create_date', $times);
        }
        return $query;
    }

    public static function percentage($type, $user, $ownStat = NULL, $locationIds = NULL, $createDate = NULL) {
        $query = Vehicle::filterQuery(
            Dashboard::CARS_SOLD_AT_AUCTION,
            $user,
            $ownStat,
            $locationIds,
            $createDate
        );
        $carSoldAtAuction = $query ? $query->count(DB::raw('distinct vehicle.id')) : 0;

        $carSoldExternalOffer = ExternalOffer::filterQuery(
            Dashboard::TOTAL_OUTBOUND_CARS_SOLD,
            $user,
            $ownStat,
            $locationIds,
            $createDate
        )->count(DB::raw('distinct external_offer.id'));

        $totalCarSold = $carSoldAtAuction + $carSoldExternalOffer;

        $faultType = NULL;
        switch ($type) {
            case Dashboard::PERCENTAGE_DISPUTES_AT_FAULTS_V_TOTAL_CARS_SOLD:
                $faultType = Dashboard::DISPUTES_FOUND_AT_FAULT;
                break;
            case Dashboard::PERCENTAGE_DISPUTES_RAISED_V_TOTAL_CARS_SOLD:
                $faultType = Dashboard::DISPUTES_RAISED_AGAINST_YOU;
                break;
        }
        $numberFault = Dispute::filterQuery($faultType, $user, $ownStat, $locationIds, $createDate)->count();

        if ($totalCarSold > 0) {
            return floatval(number_format($numberFault / $totalCarSold * 100, 2)) . "%";
        } else {
            return "0%";
        }
    }

    public static function getListDisputeByTransactionId($transactionId, $status){
        return DB::select("select * from dispute WHERE transaction_id = $transactionId and status = $status");
    }

}