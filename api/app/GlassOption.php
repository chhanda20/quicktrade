<?php

namespace App;

class GlassOption
    extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'glass_option';

    public static function getByIds($ids) {
        return GlassOption::whereIn('glass_option_id', $ids)->get();
    }

}
