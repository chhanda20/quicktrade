<?php

namespace App\Libraries;


class ErrorCodes
{
    const INVALID                               = "invalid";
    const INVALID_INPUT                         = "invalid_input";
    const INVALID_TOKEN                         = "invalid_token";
    const INVALID_REQUEST                       = "invalid_request";
    const USER_DEACTIVATED                      = "user_deactivated";
    const USER_BLOCKED                          = "user_blocked";
    const INACTIVE                              = "inactive";
    const SUSPEND                               = "suspend";
    const INCORRECT_PASSWORD                    = "incorrect_password";
    const NOT_FOUND                             = "not_found";
    const LOCATION_NOT_FOUND                    = "location_not_found";
    const USER_NOT_FOUND                        = "user_not_found";
    const ACCESS_DENIED                         = "access_denied";
    const NOT_EXISTS                            = "not_exists";
    const UNIQUE                                = "unique";
    const EXISTED                               = "existed";
    const REQUIRED                              = "required";
    const ALPHA_NUM                             = "alpha_num";
    const ALPHA_DASH                            = "alpha_dash";
    const ALPHA                                 = "alpha";
    const LENGTH_MIN_INVALID                    = "length_min_invalid";
    const LENGTH_MAX_INVALID                    = "length_max_invalid";
    const NUMERIC                               = "numeric";
    const INTEGER                               = "integer";
    const ARRAY_TYPE                            = "array";
    const STRING                                = "string";
    const ACCEPTED                              = "accepted";
    const TOO_LOW                               = "too_low";
    const OUT_SIDE_VALID_RANGE                  = "out_side_valid_range";

    const CONTACT_NOT_FOUND                     = "contact_not_found";
    const CONTACT_ALREADY_LINKED                = "contact_already_linked";

    const USER_CODE_DUPLICATE                   = "user_code_duplicate";
    const LOCATION_CODE_DUPLICATE               = "location_code_duplicate";

    const NO_BID                                = "no_bid";

    const SYS_ERR                               = "sys_err";
    const CREATE_FAILED                         = "create_failed";
    const UPDATE_FAILED                         = "update_failed";
    const AUCTION_SESSION_DUPLICATE             = "auction_session_duplicate";
    const AUCTION_ON_LIVE                       = "auction_on_live";
    const VALUATION_ALREADY_SUBMITTED           = "valuation_already_submitted";
    const AUCTION_NOT_AVAIL                     = "auction_not_avail";

    const VALUATION_HAS_BEEN_TAKEN_BY_OTHER     = "valuation_has_taken";

    const BOOLEAN                               = "boolean";
    const EXTERNAL_GROUP_NOT_FOUND              = "external_group_not_found";
    const ROLE_CODE_DUPLICATE                   = "role_code_duplicate";
    const PERMISSION_NOT_ALLOW                  = "access_denied";
    const INVALID_BID_STEP                      = "invalid_bid_step";
    const TIME_IN_PAST                          = "time_in_past";
    const AUCTION_SESSION_HAS_VEHICLE           = "auction_session_has_vehicle";
    const VEHICLE_NOT_AVAIL                     = "vehicle_not_avail";

    const BID_PRICE_MUST_BIGGER                 = "bid_price_must_bigger";
    const PAYMENT_NOT_ENOUGH                    = "payment_not_enough";
    const VALIDATE_MESSAGE_CONVERTER = [
        'required' => ErrorCodes::REQUIRED,
        'max' => ErrorCodes::LENGTH_MAX_INVALID,
        'integer' => ErrorCodes::NUMERIC,
        'in' => ErrorCodes::INVALID
    ];

    const NOT_VALID_TIME_EDIT_AUCTION_SESSION   = "not_valid_time_edit_auction_session";
}