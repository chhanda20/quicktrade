<?php

namespace app\Libraries;

use paragraph1\phpFCM\Client;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Recipient\Topic;

class Notification
{
    private $channel;
    private $data;

    public function setChannel($channel)
    {
        $this->channel = $channel;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Send notifciation data to notification server
     *
     * @param array $data Notification data
     * @return \Illuminate\Http\JsonResponse
     */
    public function notify()
    {
//        if (env('APP_NOTIFICATION')) {
            // Web
            $client = new \GuzzleHttp\Client(['base_uri' => env('APP_NOTIFICATION_BASE_URL')]);
            $response = $client->post(env('APP_NOTIFICATION_PATH'), [
                'json' => [
                    'channel' => $this->channel,
                    'data' => $this->data,
                ]
            ]);
            $bodyResponse = json_decode($response->getBody()->getContents(), true);


        // \LOG::debug('--> 10000000000000');
        // \LOG::debug('json_encode($bodyResponse)');
        // \LOG::debug(json_encode($bodyResponse));

//            if (isset($bodyResponse['status']) && $bodyResponse['status'] == 'sent') {
//                return TRUE;
//            }
//            return FALSE;

        // \LOG::debug('--> ');
        // \LOG::debug('--> ');
        // \LOG::debug('--> 10000000000000');
        // \LOG::debug('--> Firebase - mobile');
            // Firebase - mobile
            $apiKey = env('FIREBASE_API_KEY');
            $client = new Client();

        // \LOG::debug('--> 10000000000000');
        // \LOG::debug('(string)$client');


            $client->setApiKey($apiKey);
        // \LOG::debug('--> 10000000000000');
        // \LOG::debug('setApiKey($apiKey) $apiKey:');
        // \LOG::debug($apiKey);

            $client->injectHttpClient(new \GuzzleHttp\Client());

        // \LOG::debug('--> 10000000000000');
        // \LOG::debug('$client->injectHttpClient(new \GuzzleHttp\Client());');

            $message = new Message();
            $message->addRecipient(new Topic($this->channel));
            $customData = $this->data;
            unset($customData['title']);
            unset($customData['message']);

            $messageData = [];
            foreach ($customData as $key => $value) {
                $messageData[$key] = $value;
            }
            $message->setNotification(new \paragraph1\phpFCM\Notification($this->data['title'], $this->data['message']))
                ->setData($messageData);

        // \LOG::debug('--> 10000000000000');
        // \LOG::debug('$message->setNotification');

            $response = $client->send($message);

        // \LOG::debug('--> 10000000000000');
        // \LOG::debug('$response = $client->send($message)');

        
            // \LOG::debug('(string)$response->getBody():');

            // \LOG::debug((string)$response->getBody());
            return TRUE;
//        }
        return FALSE;
    }

    public static function pushWebMsg($channel, $data) {
        $client = new \GuzzleHttp\Client(['base_uri' => env('APP_NOTIFICATION_BASE_URL')]);
        $response = $client->post(env('APP_NOTIFICATION_PATH'), [
            'json' => [
                'channel' => $channel,
                'data' => $data,
            ]
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }
}