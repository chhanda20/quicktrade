<?php

namespace App\Libraries;

use App\Vehicle;

class WishlistHelper
{
    static function wishlistLabelQuery($wishlists, $aliasVehicleTable = 'veh', $aliasAuctionItemTable = NULL) {
        $matchingQuery = '';
        foreach ($wishlists as $index => $wishlist) {
            $wistlistSubQuery = '';

            $conditions   = [];
            $conditions[] = WishlistHelper::queryMatchingMakeInfo($wishlist, $aliasVehicleTable);
            $conditions[] = WishlistHelper::queryMatchingCustomData($wishlist, $aliasVehicleTable, $aliasAuctionItemTable);
            $conditions[] = WishlistHelper::queryMatchingTransmission($wishlist, $aliasVehicleTable);

            foreach ($conditions as $index => $condition) {
                $wistlistSubQuery .= $index && $wistlistSubQuery && $condition ? ' and '.$condition : $condition;
            }
            $wistlistSubQuery = $wistlistSubQuery ? "($wistlistSubQuery)" : '';
            $matchingQuery .= $index && $wistlistSubQuery && $matchingQuery ? ' or '.$wistlistSubQuery : $wistlistSubQuery;
        }

        $query = $matchingQuery ? " , case when $matchingQuery then true else false end as math_wishlist " : ', 0 as math_wishlist ';
        return $query;
    }

    static function queryMatchingMakeInfo($wishList, $aliasVehicleTable = 'vehicle') {
        $result     = '';
        $make       = collect();
        $model      = collect();
        $variant    = collect();

        if ($wishList->makeInfo) {
            $arr = json_decode($wishList->makeInfo, true);
            if ($arr) {
                foreach ($arr as $i) {

                    $i = (object)$i;
                    if (property_exists($i, "make")) {
                        $i->make = str_replace('\'', '"', $i->make);
                        $i->make = trim($i->make);
                    } else
                        $i->make = "";
                    if (property_exists($i, "model")) {
                        $i->model = str_replace('\'', '"', $i->model);
                        $i->model = trim($i->model);
                    } else
                        $i->model = "";


                    if (property_exists($i, "variant")) {
                        $i->variant = str_replace('\'', '"', $i->variant);
                        $i->variant = trim($i->variant);
                    } else {
                        $i->variant = "";
                    }
                    if ($i->make) {
                        $make->push("'" . $i->make . "'");
                    }
                    if ($i->model) {
                        $model->push("'" . $i->model . "'");
                    }
                    if ($i->variant) {
                        $variant->push("'" . $i->variant . "'");
                    }
                }

                $make = $make->unique()->values()->all();
                $model = $model->unique()->values()->all();
                $variant = $variant->unique()->values()->all();

                if (sizeof($make) > 0) {
                    $make = implode(",", $make);
                    $result .= " ($aliasVehicleTable.make IN ($make))";
                }
                if (sizeof($model) > 0) {
                    $model = implode(",", $model);
                    $result = $result ? $result.' AND ' : $result;
                    $result .= " ($aliasVehicleTable.model_family IN ($model))";
                }
                if (sizeof($variant) > 0) {
                    $variant = implode(",", $variant);
                    $result = $result ? $result.' AND ' : $result;
                    $result .= " ($aliasVehicleTable.variant IN ($variant))";
                }
            }
        }
        return $result;
    }

    static function queryMatchingCustomData($wishList, $prefixVehicleTable = 'vehicle', $aliasAuctionItemTable = NULL) {
        $result = '';
        if ($wishList->locationId) {
            $result = " $prefixVehicleTable.trade_in_location IN ($wishList->locationId) ";
        }

        if ($aliasAuctionItemTable != NULL && $wishList->price_start != null && $wishList->price_end != null) {
            $result = $result ? $result.' AND ' : $result;
            $result .= " (($aliasAuctionItemTable.start_price between $wishList->price_start AND $wishList->price_end) OR ($aliasAuctionItemTable.buy_now_price between $wishList->price_start AND $wishList->price_end)) ";
        }

        if ($wishList->odometer_start != null && $wishList->odometer_end != null) {
            $result = $result ? $result.' AND ' : $result;
            $result .= " ($prefixVehicleTable.odometer between $wishList->odometer_start AND $wishList->odometer_end) ";
        }

        if ($wishList->year_start != null && $wishList->year_end != null) {
            $result = $result ? $result.' AND ' : $result;
            $result .= " (year($prefixVehicleTable.build_date) between $wishList->year_start AND $wishList->year_end) ";
        }
        return $result;
    }

    static function queryMatchingTransmission($wishList, $aliasVehicleTable = 'vehicle') {
        $result = '';
        $transmission = collect();
        $transmissionArr = json_decode($wishList->transmission, true);
        if ($transmissionArr) {
            foreach ($transmissionArr as $type) {
                $x = str_replace('\'', '"', $type);
                $x = trim($x);
                if ($x) {
                    $transmission->push("'" . $x . "'");
                }
            }

            $transmission = $transmission->unique()->values()->all();

            $transmission = implode(",", $transmission);
            if ($transmission) {
                $result .= " ($aliasVehicleTable.transmission IN ($transmission))";
            }
        }
        return $result;
    }
}