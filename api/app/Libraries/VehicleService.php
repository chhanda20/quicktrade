<?php

namespace App\Libraries;

use App\Vehicle;

class VehicleService
{
    /**
     * Look up at database or REQUEST to VINDICATOR SERVICE
     * @param $vin
     * @return null
     */
    public static function getVehicle($vin, $vehicleYear)
    {
        $vehicleModel = new Vehicle();
        $vehicle = $vehicleModel->getVehicle($vin, $vehicleYear);
        if ($vehicle) {
            return $vehicle;
        }
        return NULL;
    }
}