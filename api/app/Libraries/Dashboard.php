<?php

namespace app\Libraries;

class Dashboard {
    const TOTAL_TEST_DRIVE = "total_test_drive";

    const TOTAL_VALUATIONS = "total_valuations";
    const TOTAL_TRADE_INS_WON = "total_trade-ins_won";
    const VALUATION_TO_TRADE_IN_PERCENTAGE = "valuation_to_trade_in_percentage";

    const TOTAL_OUTBOUND_OFFER_REQUESTS = "total_outbound_offer_requests";
    const TOTAL_OUTBOUND_OFFER_REQUESTS_ACCEPTED = "total_outbound_offer_requests_accepted";
    const TOTAL_OUTBOUND_CARS_SOLD = "total_outbound_cars_sold";
    const TOTAL_BIDS_MADE = "total_bids_made";
    const TOTAL_BIDS_WON = "total_bids_won";
    const TOTAL_BID_ACCEPTED = "total_bid_accepted";
    const TOTAL_INBOUND_OFFERS_BOUGHT = "total_inbound_offers_bought";
    const PERCENTAGE_BIDS_WON = "percentage_bids_won";

    const CARS_WIN_BOUGHT = "cars_win/bought";
    const CARS_LISTED = "cars_listed";
    const CARS_SOLD_AT_AUCTION = "cars_sold_at_auction";
    const PERCENTAGE_CARS_LISTED_V_SOLD = "percentage_cars_listed_v_sold";
    const CARS_ON_WISHLIST_APPEARING_ON_NEXT_AUCTION = "cars_on_wishlist_appearing_on_next_auction";

    const DISPUTES_RAISED_AGAINST_YOU = "disputes_raised_against_you";
    const DISPUTES_FOUND_AT_FAULT = "disputes_found_at_fault";
    const PERCENTAGE_DISPUTES_AT_FAULTS_V_TOTAL_CARS_SOLD = "percentage_disputes_at_faults_v_total_cars_sold";
    const PERCENTAGE_DISPUTES_RAISED_V_TOTAL_CARS_SOLD = "percentage_disputes_raised_v_total_cars_sold";
}


