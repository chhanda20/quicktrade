<?php

namespace app\Libraries;

class Event
{
    //this event is default, don't have config notification/email in user setting
    const AUCTION_SESSION_OUTCOME                       = 'auction_session_outcome';

    const VALUATION_REQUEST                             = 'valuation_request';
    const VALUATION_COMPLETE                            = 'valuation_approve';
    const VALUATION_REQUEST_NOTIFY_1                    = 'valuation_request_notify_1';
    const VALUATION_REQUEST_NOTIFY_2                    = 'valuation_request_notify_2';
    const VALUATION_ACCEPTED                            = 'valuation_accepted';
    const VALUATION_CANCEL                              = 'valuation_cancel';
    const VALUER_COMPLETE_VALUATION                     = 'valuer_complete_valuation';
    const APPROVE_PRICE_MAX                             = 'approve_price_max';
    const APPROVE_PRICE_ONLY                            = 'approve_price_only';
    const ADJUST_PRICE_MAX                              = 'adjust_price_max';
    const ADJUST_PRICE_ONLY                             = 'adjust_price_only';
                
    const AUCTION_BID                                   = 'auction_bid';
                
    const TRANSACTION_NEW_BUYER                         = 'transaction_new_buyer';
    const TRANSACTION_NEW_SELLER                        = 'transaction_new_seller';
                
    const PARAMS_TYPE_USER                              = 'user_id';
    const PARAMS_TYPE_LOCATION                          = 'location_id';
                
    const WISHLIST_EXTERNAL_OFFER                       = "wishlist_external_offer";
    const WISHLIST_AUCTION                              = "wishlist_auction";
                
    const BEST_OFFER                                    = "best_offer";
                
    const TRANSACTION_NEW                               = 'transaction_new';
    const TRANSACTION_PAYMENT_RECEIVE                   = 'transaction_payment_receive';
    const TRANSACTION_VEHICLE_PICKED_UP                 = 'transaction_vehicle_picked_up';
    const TRANSACTION_VEHICLE_ARRIVED                   = 'transaction_vehicle_arrived';
                
    const TRANSACTION_VEHICLE_ACCEPT                    = 'transaction_vehicle_accept';
    const TRANSACTION_COMPLETE                          = 'transaction_complete';
    const TRANSACTION_WAITING_RETURN                    = 'transaction_waiting_return';

    const TRANSACTION_RESOLVE_DISPUTE_PAY_COST_VARIANT  = 'transaction_resolve_dispute_pay_cost_variant';
    const TRANSACTION_RESOLVE_DISPUTE_RETURN_VEHICLE    = 'transaction_resolve_dispute_return_vehicle';
    const TRANSACTION_RESOLVE_DISPUTE_NO_CHANGE         = 'transaction_resolve_dispute_no_change';

    const EXTERNAL_OFFER_RETURNED                       = 'external_offer_returned';
    const WON_BEST_OFFER                                = 'won_best_offer';
    const BEST_OFFER_ACCEPTED                           = 'best_offer_accepted';
    const VEHICLE_TRADED                                = 'vehicle_traded';

    const OUTBID                                        = 'outbid';
    const AUCTION_WON                                   = 'auction_won';

    const DISPUTE_RAISED                                = 'dispute_raised';

    const RETURN_VEHICLE_REMINDER                       = 'return_vehicle_reminder';

    static public function data($event, $sourceId) {

    }

    static public function notificationHeader($event) {
        $title = str_replace('_', ' ', $event);
        $message = 'new notification for event: '.$title;

        return ['title' => $title, 'message' => $message];
    }

    static public function emailHeader($event) {
        
    }
}