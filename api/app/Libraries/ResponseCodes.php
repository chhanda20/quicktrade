<?php

namespace App\Libraries;


abstract class ResponseCodes
{
    const NOT_FOUND             = '1000';
    const INVALID_TOKEN         = '1001';
    const NOT_AVAILABLE_TO_EDIT = '1002';
    const PERMISSION_NOT_ALLOW  = '1003';
    const INVALID_INPUT         = '1004';
}