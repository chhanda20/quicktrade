<?php

namespace App\Libraries;

use App\AppProperty;
use App\BidLog;
use App\Console\Commands\DoAutoBid;
use App\AuctionSession;
use App\SessionHasItem;
use App\Vehicle;
use App\User;
use Illuminate\Support\Facades\DB;

class AuctionBidHelper
{
    /**
     * Look up at database or REQUEST to VINDICATOR SERVICE
     * @param $vin
     * @return null
     */
    public static function bid($sessionId, $itemId, $bid_price, $user_id, $bid_type = BidLog::TYPE_MANUAL, $step = 0)
    {
        \LOG::debug("bid(sessionId=$sessionId, itemId=$itemId, bid_price=$bid_price, user_id=$user_id");

        $time = date("Y-m-d H:i:s");

        $si = SessionHasItem::where('auction_session_id', $sessionId)->where('auction_item_id', $itemId)->first();
        if (!$si) {
            return ErrorCodes::NOT_FOUND;
        }

        if ($step == 0) {
            $customPrice = AuctionSession::find($sessionId)->custom_bid_price_increment;
            $step = $customPrice ? $customPrice : AppProperty::getConfig(AppProperty::AUCTION_PRICE_INCREMENT);
            \LOG::debug("bid_step = $step");

            if(!$step) {
                \LOG::error("No app config: AUCTION_PRICE_INCREMENT");
                return ErrorCodes::SYS_ERR;
            }
        }

        $session = DB::select("SELECT * FROM auction_session WHERE id=:id AND :backend_time BETWEEN startDate and endDate", ['id' => $sessionId, 'backend_time' => $time]);

        if (sizeof($session) == 0) {
            return ErrorCodes::INACTIVE;
        }

        $delta = $bid_price - $si->bid_price;

        if ($delta <= 0) {
            return ErrorCodes::TOO_LOW;
        }

        if ($delta % $step != 0) {
            return ErrorCodes::INVALID_BID_STEP;
        }

        $updated = SessionHasItem::where('auction_session_id', $sessionId)->where('auction_item_id', $itemId)->where('bid_price', "<", $bid_price)->update(['bid_price' => $bid_price, 'bid_user_id' => $user_id]);

        if ($updated == 0) {
            return ErrorCodes::TOO_LOW;
        }

        $log = new BidLog();
        $log->user_id = $user_id;
        $log->auction_session_id = $sessionId;
        $log->auction_item_id = $itemId;
        $log->bid_price = $bid_price;
        $log->type = $bid_type;
        $log->save();

        \LOG::info("user[$user_id] sucessfully BID sesssion[$sessionId], item[$itemId] with price $bid_price at time[$time]");


        $err = Notification::pushWebMsg(Event::AUCTION_BID . "_$sessionId", [
            'user_id' => $user_id,
            'auction_session_id' => $sessionId,
            'auction_item_id' => $itemId,
            'bid_price' => $bid_price,
        ]);

        if ($err && $err['status'] !== 'sent') {
            ob_start();
            $result = ob_get_clean();
            \LOG::error('Sent Bid Noti Error : ' . $result);
        }

        return null;
    }

    public static function diffInSeconds($start, $end) {
        $start_datetime = strtotime($start);
        $end_datetime = strtotime($end);
        $interval  = $end_datetime - $start_datetime;
        return round($interval);
    }

    public static function autoBid() {
        $count = 0;
        $time = date('Y-m-d H:i:s');

        $currentStartTime = AppProperty::getConfig(AppProperty::AUTO_BID_STARTED_TIME);

        if ($currentStartTime === null) { // becarefull with ===
            AppProperty::setConfig(AppProperty::AUTO_BID_STARTED_TIME, '');
            \LOG::debug("Init AUTO_BID_STARTED_TIME");
        }

        $result = DB::update("UPDATE `app_property` SET `value` = :backendTime WHERE `app_property`.`id` = 'auto_bid_started_time' AND (trim(value) = '' OR Cast(:backendTime2 as DateTime) - Cast(value as DateTime) > 15)", ['backendTime' => $time, 'backendTime2' => $time]);

        if ($result == 0) {
//            \LOG::info("Another AutoBid process has been started $currentStartTime before!");
            return -1;
        }

        AppProperty::setConfig(AppProperty::AUTO_BID_STARTED_TIME, $time);


        $step = AppProperty::getConfig(AppProperty::AUCTION_PRICE_INCREMENT);


        if(!$step) {
            \LOG::error("No app config: AUCTION_PRICE_INCREMENT");
            return -1;
        }

        $sessions = DB::select("SELECT * FROM auction_session WHERE :backend_time BETWEEN startDate and endDate", ['backend_time' => $time]);

        foreach ($sessions as $session) {
            $maxBiderId = 0;
            $maxPrice   = 0;

            $items = DB::select("SELECT auction_item_id FROM session_has_item WHERE auction_session_id=$session->id AND status=:status", ['status' => SessionHasItem::STATUS_APPROVED]);
            \LOG::info("======================== AUTO BID=====================");
            \LOG::debug("bid_step = $step");

            \LOG::debug("Session[$session->id]");
            foreach ($items as $item) {
                \LOG::debug("   Item[$item->auction_item_id]");
                $current = SessionHasItem::where('auction_session_id', $session->id)->where('auction_item_id', $item->auction_item_id)->first();
                $currentPrice = $current->bid_price;

                $settings = DB::select("SELECT user_id,max_price FROM bid_setting WHERE auction_session_id=$session->id AND auction_item_id=$item->auction_item_id AND (max_price-$step)>$currentPrice ORDER BY max_price DESC, create_date ASC LIMIT 2");

                $size_of_setting = sizeof($settings);

                if ($size_of_setting > 0) {
                    if($current->bid_user_id !== $settings[0]->user_id) {
                        if ($size_of_setting > 1) {
                            $high = $currentPrice;
                            if ($high < $settings[1]->max_price) {
                                $high = floor(($settings[1]->max_price - $currentPrice) / $step) * $step + $currentPrice;
                            }
                            $nextPrice = $high + $step;
                        } else {
                            $nextPrice = $currentPrice + $step;
                        }

                        if ($nextPrice > $maxPrice) {
                            $maxPrice   = $nextPrice;
                            $maxBiderId = $settings[0]->user_id;
                        }

                        $err = AuctionBidHelper::bid($session->id, $item->auction_item_id, $nextPrice, $settings[0]->user_id, BidLog::TYPE_AUTO, $step);
                        if ($err) {
                            \LOG::warn("AUTO_BID ERROR $err");
                        } else {
                            $count++;
                        }
                    }
                }
            }
            $bider = User::find($maxBiderId);
            if ($bider) {
                AuctionSession::sendOutbid($session->id, $bider);
            }
        }

        AppProperty::setConfig(AppProperty::AUTO_BID_STARTED_TIME, '');

        return $count;

    }
}