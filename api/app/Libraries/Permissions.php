<?php
/**
 * Created by PhpStorm.
 * User: ntnam
 * Date: 11/24/16
 * Time: 14:53
 */

namespace App\Libraries;


class Permissions
{

    //user
    const USER_CREATE = "user_create";
    const USER_UPDATE = "user_update";
    const USER_GET = "user_get";
    const USER_SUSPEND = "user_suspend";
    const USER_ADMIN_GET = "user_admin_get";
    const USER_ADMIN_UPDATE = "user_admin_update";

    //contact
    const CONTACT_CREATE = "contact_create";
    const CONTACT_GET = "contact_get";
    const CONTACT_UPDATE = "contact_update";

    //valuation
    const VALUATION_CREATE = "valuation_create";
    const VALUATION_GET = "valuation_get";
    const VALUATION_CANCEL = "valuation_cancel";
    const VALUATION_EDIT = "valuation_edit";
    const VALUATION_PERFORM = "valuation_perform";
    const VALUATION_APPROVE = "valuation_approve";
    const VALUATION_REPRICE = "valuation_reprice";
    const VALUATION_DELETE = "valuation_delete";

//    const ESTIMATED_COST_UPDATE = "estimated_cost_update";
//    const ESTIMATED_COST_CREATE = "estimated_cost_create";
//    const GENERAL_RECONDITION_COST_UPDATE = "general_recondition_cost_update";
//    const WHOLESALE_VALUE_UPDATE = "wholesale_value_update";
//    const RECONDITION_COST_UPDATE = "recondition_cost_update";
//    const SALES_INFO_UPDATE = "sales_info_update";
//    const OVERALL_RATING_UPDATE = "overall_rating_update";
    const NOTIFICATION_HEAD_VALUER = "notification_head_valuer";
    const NOTIFICATION_SALE_MANAGER = "notification_sale_manager";

    //vehicle
    const VEHICLE_EDIT = "vehicle_edit";
    const VEHICLE_GET = "vehicle_get";
    const VEHICLE_CREATE = "vehicle_create";
    const VEHICLE_VIEW = "vehicle_view";
    const VEHICLE_UPDATE = "vehicle_update";
    const VEHICLE_TRADE = "vehicle_trade";

    //company
    const COMPANY_CREATE = "company_create";
    const COMPANY_EDIT = "company_edit";
    const COMPANY_GET = "company_get";
    const DEALER_SETTINGS_EDIT = "dealer_settings_edit";


    //permission
    const PERMISSION_GET = "permission_get";
    const PERMISSION_EDIT = "permission_edit";
    const PERMISSION_ADMIN_GET = "permission_admin_get";
    const PERMISSION_NAME_EDIT = "permission_name_edit";
    //role
    const ROLE_GET = "role_get";
    const ROLE_CREATE = "role_create";
    const ROLE_EDIT = "role_edit";
    const SYSTEM_ROLE_CREATE = "system_role_create";
    const SYSTEM_ROLE_EDIT = "system_role_edit";
    const SYSTEM_ROLE_DELETE = "system_role_delete";
    const CUSTOM_ROLE_DELETE = "custom_role_delete";

    //location
    const LOCATION_CREATE = "location_create";
    const LOCATION_GET = "location_get";
    const LOCATION_EDIT = "location_edit";

    //external group
    const EXTERNAL_GROUP_CREATE = "external_group_create";
    const EXTERNAL_GROUP_GET = "external_group_get";
    const EXTERNAL_GROUP_EDIT = "external_group_edit";
    const EXTERNAL_GROUP_DELETE = "external_group_delete";

    //external offer
    const EXTERNAL_OFFER_BID = "external_offer_bid";
    const EXTERNAL_OFFER_ACCEPT = "external_offer_accept";
    const EXTERNAL_OFFER_EDIT = "external_offer_edit";
    const EXTERNAL_OFFER_CREATE = "external_offer_create";
    const EXTERNAL_OFFER_VIEW = "external_offer_view";
    const EXTERNAL_OFFER_DISCARD = "external_offer_discard";

    //test drive
    const TEST_DRIVE_CREATE = "test_drive_create";
    const TEST_DRIVE_GET = "test_drive_view";
    const TEST_DRIVE_EDIT = "test_drive_edit";

    //Auction Session
    const AUCTION_SESSION_CREATE = "auction_session_create";
    const AUCTION_SESSION_EDIT = "auction_session_edit";
    const AUCTION_SESSION_DELETE = "auction_session_delete";
    const AUCTION_SESSION_VIEW = "auction_session_view";

    //Auction Item
    const AUCTION_ITEM_APPROVE = "auction_item_approve";
    const AUCTION_ITEM_LISTING_VIEW = "auction_item_listing_view";
    const AUCTION_ITEM_EDIT = "auction_item_edit";
    const BID_ON_VEHICLE = "bid_on_vehicle";
    const SEND_TO_AUCTION = "send_to_auction";
    const AUCTION_SETTING_EDIT = "auction_setting_edit";

    //Watch List
    const WATCH_LIST_CREATE = "watch_list_create";
    const WATCH_LIST_DELETE = "watch_list_delete";

    //Wish List
    const WISH_LIST_VIEW = "wish_list_view";
    const WISH_LIST_CREATE = "wish_list_create";
    const WISH_LIST_EDIT = "wish_list_edit";
    const WISH_LIST_DELETE = "wish_list_delete";

    //Transaction
    const TRANSACTION_VIEW = "transaction_view";
    const VEHICLE_ACCEPT = "vehicle_accept";

    //Dispute
    const DISPUTE_RESOLVE = "dispute_resolve";
    const DISPUTE_INFO_VIEW = "dispute_info_view";
    const DISPUTE_LODGE = "dispute_lodge";
    const DISPUTE_CREATE = "dispute_create";

    //Menu
    const MENU_GET = "menu_get";
    const MENU_SET = "menu_set";

    //Data Access
    const DATA_ACCESS_EDIT = "data_access_edit";
    const DATA_ACCESS_GET = "data_access_get";

    //others
//    const BRING_VEHICLE_INTO_STOCK = "bring_vehicle_into_stock";
//    const SEARCH_VEHICLES = "search_vehicles";
//    const ACCESS_TEST_DRIVE_INFORMATION = "access_test_drive_information";
    const PPSR_GET = "ppsr_get";
    const SYSTEM_CONFIG = "system_config";
    const SYSTEM_ADMIN_GET = "system_admin_get";


    public static function getPermissions($userId)
    {
        // GETTING FROM CACHE!!! Will be implemented later!
    }
}