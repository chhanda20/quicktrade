<?php

namespace App\Libraries;

use App\Contact;
use App\Libraries\PDFGeneratorService;
use GuzzleHttp\Client;
use App\Document;

class PDFTemplate {

    public static function generate($user, $documentType, $data) {
        switch ($documentType) {
            case Document::TRANSPORT_FEE:
            case Document::FEE:
            case Document::PAYMENT_RECEIPT:
            case Document::CREDIT_NOTE_COST_VARIANCE:
            case Document::CREDIT_NOTE_RETURN_VEHICLE:
            case Document::TRANSPORT_FEE_RETURN_VEHICLE:
                return self::basic($user, $data);
        }
        return null;
    }

    public static function testPdf($user) {
        $body = "
            <div id='wrapper'>
                <div class='block'>
                <h2>title</h2>

                <div class='col-left'>
                    <div class='bold-text'>xxxxxx: XXXX</div>
                    <div class='date'>Date: date</div>
                </div>

                <div class='col-right'>
                    2/58 Lorimer St<br />
                    Docklands VIC 3008<br />
                    Australia<br />
                    Phone: XXXXXXXX<br />
                    ABN: XX XXX XXX XXX
                </div>
                <div style='clear: both'></div>
            </div>

            <div class='block'>
                <h2>details</h2>

                <div class='bill-wrapper'>
                    <div class='bold-text bill-to'>Bill to:</div>
                    <div class='bill-detail'>
                        billTo
                    </div>
                </div>

                <div class='invoice'>
                    <div class='row'>
                        <div class='col_1 bold-text'>ITEM</div>
                        <div class='col_2 bold-text'>DESCRIPTION</div>
                        <div class='col_3 bold-text'>AMOUNT (inc GST)</div>
                        <div style='clear: both'></div>
                    </div>
                    <div class='row no-padding no-border' style='padding-top: 20px;'>
                    <div class='col_1'>Vehicle</div>
                    <div class='col_2'>2017 Holden Astra Manual 2.8l…………. - Blue</div>
                    <div class='col_3'>24000</div>
                    <div style='clear: both'></div>
                </div>

                <div class='row no-padding no-border'>
                    <div class='col_1'></div>
                    <div class='col_2 vin' style='margin-left: 0px;'>VIN - XXXXXXXXXXXXXXXXX</div>
                    <div class='col_3'>&nbsp;</div>
                    <div style='clear: both'></div>
                </div>

                <div class='row no-padding' style='padding-bottom: 20px'>
                    <div class='col_1'>&nbsp;</div>
                    <div class='col_2'>REG - ABC123</div>
                    <div class='col_3'>&nbsp;</div>
                    <div style='clear: both'></div>
                </div>

                <div class='row'>
                    <div class='col_1'>FEE</div>
                    <div class='col_2'>Moto Buyer Fee</div>
                    <div class='col_3'>200.00</div>
                    <div style='clear: both'></div>
                </div>

                <div class='row'>
                    <div class='col_1'>TRANSPORT</div>
                    <div class='col_2'>Transport</div>
                    <div class='col_3'>80.00</div>
                    <div style='clear: both'></div>
                </div>
                </div>

                <div class='invoice no-border price-table'>
                    <div class='row no-border no-padding'>
                        <div class='col_1'>&nbsp;</div>
                        <div class='col_2' style='text-align: right'>GST(10%):</div>
                        <div class='col_3'>gst</div>
                        <div style='clear: both'></div>
                    </div>

                    <div class='row no-border no-padding'>
                        <div class='col_1'>&nbsp;</div>
                        <div class='col_2' style='text-align: right'>Total (inc GST):</div>
                        <div class='col_3'>total</div>
                        <div style='clear: both'></div>
                    </div>

                    <div class='row no-border no-padding'>
                        <div class='col_1'>&nbsp;</div>
                        <div class='col_2' style='text-align: right'>Amount Paid:</div>
                        <div class='col_3'>amountPaid</div>
                        <div style='clear: both'></div>
                    </div>

                    <div class='row no-border no-padding'>
                        <div class='col_1'>&nbsp;</div>
                        <div class='col_2' style='text-align: right'>AMOUNT DUE:</div>
                        <div class='col_3 bold-text'>amountDue</div>
                        <div style='clear: both'></div>
                    </div>
                </div>

                <div style='clear: both'></div>
            </div>

            <div class='block'>
                <h2>how to pay</h2>

                <div class='col-left'>
                    <div class='bold-text'>Bank Deposit Pay using EFT:</div>
                    <div>
                        Name: MO<br />
                        BSB: xxxxxx<br />
                        AC#: xxxxxxxx<br />
                        Ref#: IV00001911
                    </div>
                </div>

                <div class='col-right bold-text term' style='margin-top: 21px;'>
                    Payment terms on invoice
                </div>
                <div style='clear: both'></div>
            </div>
        </div>";

        $html = self::_pdfLayout($body);
        return PDFGeneratorService::generate($html);
    }

    public static function basic($buyer, $data) {

        $itemsHtml = "";
        foreach ($data['items'] as $item) {
            $key    = $item['item'];
            $des    = $item['description'];
            $amount = $item['amount']; 
            $itemDiv = "
                <div class='row no-padding no-border' style='padding-top: 20px;'>
                    <div class='col_1'>$key</div>
                    <div class='col_2'>$des</div>
                    <div class='col_3'>$amount</div>
                    <div style='clear: both'></div>
                </div>";
            $itemsHtml = $itemsHtml.$itemDiv;
        }

        $gst        = $data['summary']['gst'];
        $total      = $data['summary']['total'];
        $amountPaid = $data['summary']['amount paid'];
        $amountDue  = $data['summary']['amount due'];
        
        $title      = $data['title'];
        $date       = $data['date'];
        $billTo     = $data['bill to'];

        $body = "
            <div id='wrapper'>
                <div class='block'>
                <h2>$title</h2>

                <div class='col-left'>
                    <div class='bold-text'>xxxxxx: XXXX</div>
                    <div class='date'>Date: $date</div>
                </div>

                <div class='col-right'>
                    2/58 Lorimer St<br />
                    Docklands VIC 3008<br />
                    Australia<br />
                    Phone: XXXXXXXX<br />
                    ABN: XX XXX XXX XXX
                </div>
                <div style='clear: both'></div>
            </div>

            <div class='block'>
                <h2>details</h2>

                <div class='bill-wrapper'>
                    <div class='bold-text bill-to'>Bill to:</div>
                    <div class='bill-detail'>
                        $billTo
                    </div>
                </div>

                <div class='invoice'>
                    <div class='row'>
                        <div class='col_1 bold-text'>ITEM</div>
                        <div class='col_2 bold-text'>DESCRIPTION</div>
                        <div class='col_3 bold-text'>AMOUNT (inc GST)</div>
                        <div style='clear: both'></div>
                    </div>
                    $itemsHtml
                </div>

                <div class='invoice no-border price-table'>
                    <div class='row no-border no-padding'>
                        <div class='col_1'>&nbsp;</div>
                        <div class='col_2' style='text-align: right'>GST(10%):</div>
                        <div class='col_3'>$$gst</div>
                        <div style='clear: both'></div>
                    </div>

                    <div class='row no-border no-padding'>
                        <div class='col_1'>&nbsp;</div>
                        <div class='col_2' style='text-align: right'>Total (inc GST):</div>
                        <div class='col_3'>$$total</div>
                        <div style='clear: both'></div>
                    </div>

                    <div class='row no-border no-padding'>
                        <div class='col_1'>&nbsp;</div>
                        <div class='col_2' style='text-align: right'>Amount Paid:</div>
                        <div class='col_3'>$$amountPaid</div>
                        <div style='clear: both'></div>
                    </div>

                    <div class='row no-border no-padding'>
                        <div class='col_1'>&nbsp;</div>
                        <div class='col_2' style='text-align: right'>AMOUNT DUE:</div>
                        <div class='col_3 bold-text'>$$amountDue</div>
                        <div style='clear: both'></div>
                    </div>
                </div>

                <div style='clear: both'></div>
            </div>

            <div class='block'>
                <h2>how to pay</h2>

                <div class='col-left'>
                    <div class='bold-text'>Bank Deposit Pay using EFT:</div>
                    <div>
                        Name: MO<br />
                        BSB: xxxxxx<br />
                        AC#: xxxxxxxx<br />
                        Ref#: IV00001911
                    </div>
                </div>

                <div class='col-right bold-text term' style='margin-top: 21px;'>
                    Payment terms on invoice
                </div>
                <div style='clear: both'></div>
            </div>
        </div>";

        $html = self::_pdfLayout($body);
        return PDFGeneratorService::generate($html);
    }

    private static function _pdfLayout($body) {
        $html = '<div>';
        $html .= self::_header();
        $html .= $body;
        $html .= self::_footer();
        $html .= '</div>';
        return $html;
    }

    private static function _header() {
        $imageUrl = base_path('public/images/logo.png');
        $cssUrl = base_path('public/css/pdf.css');
        return '<!DOCTYPE html>
            <html>
            <head lang="en">
                <meta charset="UTF-8">
                <title></title>
                <link rel="stylesheet" href="' .$cssUrl. '">
            </head>
            <body>
                <div id="header"><img src="'.$imageUrl.'" alt=""/></div>
            ';
    }

    private static function _footer() {
        return '</body>
            </html>';
    }
}