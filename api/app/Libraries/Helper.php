<?php

namespace App\Libraries;

use App\NotificationMessage;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Support\Facades\DB;

abstract class Helper
{
    /**
     * List all files in a directory
     *
     */
    public static function getDirectoryList($directory)
    {
        $results = array();
        $handler = opendir($directory);
        while ($file = readdir($handler)) {
            if ($file != "." && $file != ".." && $file != "index.html") {
                $results[] = $file;
            }
        }
        closedir($handler);
        return $results;
    }

    /**
     * Hash a string
     *
     */
    public static function hash($string)
    {
        return (new BcryptHasher)->make($string);
    }

    public static function generateCharacter()
    {
        $possible = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);
        return $char;
    }


    /**
     * Generate random string with length
     *
     */
    public static function generateGuid($stringLength = 40)
    {
        $guid = '';
        for ($i = 0; $i < $stringLength; $i++) {
            $guid .= self::generateCharacter();
        }
        return $guid;
    }

    /**
     * Send notification message for web, mobile based on location
     *
     */
    public static function notify($eventCode, $param, $title, $message, $customData = [], $paramType)
    {
        //This function is dead x( Hope that one day we reuse notify channel system again one more time...
        $eventModel = new \App\Event();
        $channels = $eventModel->getByCode($eventCode);

        foreach ($channels as $channel) {
            $notification = new Notification();
            $channel = self::channelName($channel->name, $channel->type, $param);

            \LOG::info("SEND NOTIFICATION TO $channel");

            $notification->setChannel($channel);
            $notiData = [
                'eventCode' => $eventCode,
                'title' => $title,
                'message' => $message
            ];
            if (is_array($customData) && $customData) {
                foreach ($customData as $key => $value) {
                    $notiData[$key] = $value;
                }
            }
            $notification->setData($notiData);
            $notification->notify();

            //store notification in db

            $notificationMessage = new NotificationMessage();
            $notificationMessage->channel_id= $channel->id;
            $notificationMessage->channel_name = $channel;
            $notificationMessage->event_code = $eventCode;
            $notificationMessage->param = $param;
            $notificationMessage->param_type = $paramType;
            $notificationMessage->title = $title;
            $notificationMessage->message = $message;
            $notificationMessage->custom_data = json_encode($notiData);
            $notificationMessage->is_read = false;

            $notificationMessage->save();

        }
    }

    public static function notifyChannel($eventCode, $userId, $title, $message, $customData = [])
    {
        $eventModel = new \App\Event();
        $notification = new Notification();

        $channel = 'user_'.$userId;

        \LOG::info("SEND NOTIFICATION TO $channel");

        $notification->setChannel($channel);
        $notiData = [
            'eventCode' => $eventCode,
            'title' => $title,
            'message' => $message
        ];
        if (is_array($customData) && $customData) {
            foreach ($customData as $key => $value) {
                $notiData[$key] = $value;
            }
        }
        $notification->setData($notiData);
        $notification->notify();

        //store notification in db

        $notificationMessage                = new NotificationMessage();
        $notificationMessage->channel_id    = 0;                        //Don't use anymore, now we send notification to specify user insteed of permission channel
        $notificationMessage->channel_name  = $channel;                 //Don't use anymore, now we send notification to specify user insteed of permission channel
        $notificationMessage->event_code    = $eventCode;
        $notificationMessage->param         = $userId;
        $notificationMessage->param_type    = 'user_id';
        $notificationMessage->title         = $title;
        $notificationMessage->message       = $message;
        $notificationMessage->custom_data   = json_encode($notiData);
        $notificationMessage->is_read       = false;

        $notificationMessage->save();
    }

    /**
     * Create channel name
     */
    public static function channelName($channelName, $type, $param) {
        switch ($type) {
            case 'role':
                $prefix = '_loc_' ;
                break;
            case 'user':
                $prefix = '_' ;
        }
        return $channelName . $prefix . $param;
    }

    /**
     * Get time difference in minutes
     */

    public static function timeDiffMin($dateTo, $dateFrom) {
        $toTime = strtotime($dateTo);
        $fromTime = strtotime($dateFrom);
        return round(abs($toTime - $fromTime) / 60,2);
    }

    /**
     * authorized user has permission for each features
     * @param string $userId
     * @param array $valuateFeatures
     * String list of permission to check
     * @return boolean|true if user has permission|false if user do not have permission
     */
    public static function valuatePermission($userId, $valuateFeatures){

        $stringList = implode("','", $valuateFeatures);
        $sqlValidatePermission = "select * from (
	        SELECT
            `user`.id as user_id,
            permission.id as permission_id,
            permission.`name` as permission_name ,permission.is_enabled as is_enabled
            FROM
            `user`
            INNER JOIN user_role ON `user`.id = user_role.user_id
            INNER JOIN user_permission ON `user`.id = user_permission.user_id
            INNER JOIN permission ON user_permission.permission_id = permission.id 
            UNION ALL (SELECT
            `user`.id as t,
            permission.id,
            permission.`name`,permission.is_enabled as is_enabled
            FROM
            `user`
            INNER JOIN user_role ON `user`.id = user_role.user_id
            INNER JOIN role_permission ON user_role.role_id = role_permission.role_id
            INNER JOIN permission ON role_permission.permission_id = permission.id ) 
            ) as list
            where list.user_id = $userId AND list.permission_name IN ('$stringList') AND list.is_enabled = TRUE
            ";
        $listPermission = DB::select($sqlValidatePermission);
        if(!$listPermission)
            return false;
        return true;
    }


    public static function get_glass_token($custArr = array())
    {
        if(!empty($custArr))
        {
            $body['customer'] = $custArr['cust_name'];
            $body['reference'] = $custArr['cust_code'];
            $body['systemCode'] = env('GLASS_SYS_CODE');
            $body['apiKey'] = env('GLASS_API_KEY');

           // dd(env('GLASS_API_SERVER'). "/v1/Authorise");
             $headers = array("Content-Type: application/json");
            $result = self::initCurl(env('GLASS_AUTH_SERVER') . "/v1/Authorise", $headers, $body );

            return $result = json_decode($result);
        }

    }

    public static function initCurl($path, $headers = null ,$body = null ,$method = "CURLOPT_POST")
    {
        //echo $method."\n".$path."<br>" ;
        //    print_r($body);
        if(!isset($headers)) {
            $headers = array("Content-Type: application/json",
                "Content-Length: " . strlen($body));
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $path);

        if ($method == "CURLOPT_POST") {

            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

        } else if ($method == "CURLOPT_HTTPGET") {
            curl_setopt($ch, CURLOPT_HTTPGET, 1);

        } else if ($method == "PUT") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
            curl_setopt($ch, CURLOPT_HEADER, false);
        }  else if ($method == "DELETE") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $server_output = curl_exec($ch);

        curl_close ($ch);
        // dd($server_output) ;
        return $server_output;
    }
}
