<?php
/**
 * Created by IntelliJ IDEA.
 * User: ntnam
 * Date: 6/2/17
 * Time: 09:20
 */

namespace app\Libraries;


class IdTransformer
{

    public static function toClientTransactionId($id) {
        return 'T' . str_pad($id, 9, '0', STR_PAD_LEFT);
    }
    

    public static function toClientDisputeId($id) {
        return 'D' . str_pad($id, 9, '0', STR_PAD_LEFT);
    }

}