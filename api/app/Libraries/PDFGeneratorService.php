<?php

namespace App\Libraries;

use App\Vehicle;
use Dompdf\Dompdf;

class PDFGeneratorService
{
    public static function generatePaymentReceipt($userName, $vehicle) {
        $html = "<h1>$userName</h1></br>
                <h2>vehicle</h2></br>
                <h3>196 hoang dieu h2</h3>";
        PDFGeneratorService::generate($html);
    }
    /**
     * @param $html template to generate file
     * @param string $size
     * @param string $orientation
     * @param bool $isStream stream to brower
     * @return
     */
    public static function generate($html, $size = 'A4', $orientation = 'portrait', $isStream = false) {
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper($size, $orientation);
        $dompdf->render();
        if ($isStream) {
            $dompdf->stream();
        }
        return $dompdf->output();
    }
}