<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class CompanySetting extends Backend
{

    const STATUS_CODE_INACTIVE = 0;
    const STATUS_CODE_ACTIVE = 1;

    const HEAD_VALUER_APPROVAL = "head_valuer_approval";
    const RECEIVE_EXTERNAL_OFFER = "receive_external_offer";
    const TEST_DRIVE_TERMS = "test_drive_terms";
    const TERM_AND_CONDITION = "term_and_condition";
    const MO_TERM_AND_CONDITION = "dst_term_and_condition";

    protected $table = 'company_setting';
    protected $fillable = ['key'];

    public static function createDefaultSetting($companyId, $userId){
        //todo: feature/900/term-and-condition need add default term and condition or validate as required field
        $currentDate = date('Y-m-d H:i:s');
        $insertSQL = "INSERT INTO `company_setting` (`company_id`, `key`, `value`,`status_code`, `creator_id`, `create_date`, `updater_id`, `update_date`) VALUES
                      ($companyId,'head_valuer_approval', 1, 1, $userId, '$currentDate', $userId, '$currentDate'),
                      ($companyId,'receive_external_offer', 1, 1, $userId, '$currentDate', $userId, '$currentDate'),
                      ($companyId,'test_drive_terms','Test Drive Terms and Condition', 1, $userId, '$currentDate', $userId, '$currentDate'),
                      ($companyId,'term_and_condition', 'Need define default term and condition', 1, $userId, '$currentDate', $userId, '$currentDate')";
        DB::statement($insertSQL);
        return true;
    }

    public static function getCompanySetting($companyId)
    {
        $result = DB::select("SELECT `key`,`value` from company_setting where company_setting.company_id = $companyId ");
        if($result)
            return $result;
        else
            return NULL;
    }
}
