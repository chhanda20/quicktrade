<?php

namespace App;

class ExternalGroupOffer extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'external_group_offer';
    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

}
