<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlassTransmissionType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id'
    ];

    protected $table = 'glass_transmission_type';
}
