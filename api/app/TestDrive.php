<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Carbon\Carbon;

class TestDrive extends Backend
{

    const STATUS_CODE_INACTIVE = 0;
    const STATUS_CODE_IN_PROGRESS = 1;
    const STATUS_CODE_COMPLETE = 2;

    protected $table = 'test_drive';

    /**
     * @param null $status:     status TestDrive: Inactive/InProgress/Compelete...
     * @param null $user:       of user who interact with result list
     * @param null $locationIds locations id of TestDrive
     * @param null $createDate  data range dictionary for filter [start: Date, end: Date]
     * @return array
     */
    public static function filterQuery($status = NULL, $user, $ownStat = NULL, $locationIds = NULL, $createDate = NULL) {

        $query = TestDrive::select('test_drive.*');
        if ($status) {
            $query->whereIn('status', $status);
        }
        if ($ownStat) {
            $query->where(function($query) use ($user) {
                $query->where('test_drive.creator_id', $user->id);
            });
        }
//        else {
//            $ids = User::usersSameCompanyQuery($user->company_group_id)->select('user.id')->get();
//            $query->whereIn('test_drive.creator_id', $ids);
//        }
        if ($locationIds) {
            $query->whereIn('test_drive.location_id', $locationIds);
        }
        if ($createDate && $createDate['start'] && $createDate['end']) {
            $times = [$createDate['start'], $createDate['end']];
            $query->whereBetween('test_drive.create_date', $times);
        }

        return $query;
    }

    public static function getListTestDriveByLocation($locationId)
    {
        $result = DB::select("SELECT v.make as brand, v.model_family AS model, v.build_date AS `year`, v.colour AS color, t.duration,
                                t.return_date, t.create_date,
                                CONCAT(contact.first_name, ' ', contact.last_name) as salesPerson, t.comment, t.status, t.vehicle_rating, t.service_rating
                                FROM
                                test_drive AS t
                                INNER JOIN vehicle AS v ON t.vehicle_id = v.id
                                INNER JOIN `user` AS u ON t.creator_id = u.id
                                INNER JOIN contact ON u.contact_id = contact.id
                                WHERE t.location_id = $locationId");

        if ($result)
            return $result;
        else
            return NULL;
    }

    public static function completeTestDriveSession($testDriveId, $comment, $vehicleRating, $serviceRating, $marketingOption, $marketingCode)
    {
        $result = DB::table('test_drive')
            ->where('id', $testDriveId)
            ->update(['status' => TestDrive::STATUS_CODE_COMPLETE,
                'update_date' => date('Y-m-d H:i:s'),
                'comment' => $comment,
                'vehicle_rating' => $vehicleRating,
                'service_rating' => $serviceRating,
                'marketing_option' => $marketingOption,
                'marketing_code' => $marketingCode]);

        return $result;
    }

    public function searchTestDrive($locationId, $input)
    {

        $limit              = $input->get('limit');
        $offset             = $input->get('offset');
        $status             = $input->get('status');
        $customerId         = $input->get('customer_id');
        $rangeFrom          = $input->get('range_from');
        $rangeTo            = $input->get('range_to') . ' 23:59:59';
        $vehicleTableName   = $input->get('manual_vehicle') ? 'manual_vehicle' : 'vehicle';

        $order = "order by update_date";
        if ($input->has("orderBy")) {
            $order = "ORDER BY " . $input->get("orderBy");
            /*if ($input->has("orderDesc") && $input->get("orderDesc")) {
                $order .= ' DESC ';
            }*/
        }
        $order .= ' DESC ';
        $query = "";
        if ($input->has("query")) {
            $query_input = $input->get('query');
            if ($query_input) {
                $query .= " AND CONCAT(v.make, v.build_date, v.colour , v.model_family ) LIKE '%$query_input%'";
            }
        }

        $customerQuery = "";
        if($customerId){
            $customerQuery = " AND t.customer_id = $customerId ";
        }

        $rangeQuery = "";
        if($rangeFrom && $rangeTo){
            $rangeQuery .= " AND ('$rangeFrom' <= t.create_date  AND t.create_date <= '$rangeTo') ";
        }
        if(!$status)
            $status = array(TestDrive::STATUS_CODE_IN_PROGRESS);

        $stringList = implode(",", $status);
        $testDriveList = DB::select("Select m.*, CONCAT_WS(' ', contact.first_name, contact.last_name) customer_name
                                      from (SELECT t.id, t.location_id, t.return_date, t.create_date, t.`comment`, t.duration, t.`marketing_option`, t.`marketing_code`, t.`update_date`,
                                                v.make, v.build_date, v.colour AS color, v.model_family AS model, null AS stock_number,
                                                CONCAT(contact.first_name, ' ', contact.last_name) as salesPerson, t.status,t.customer_id, t.vehicle_rating, t.service_rating, v.registration_number
                                                FROM
                                                test_drive AS t
                                                INNER JOIN vehicle AS v ON t.vehicle_id = v.id
                                                INNER JOIN `user` AS u ON t.creator_id = u.id
                                                INNER JOIN contact ON u.contact_id = contact.id
                                                WHERE t.location_id = $locationId 
                                                AND t.manual_vehicle <> TRUE
                                                AND status in ($stringList) 
                                                $rangeQuery
                                                $customerQuery  
                                                $query        
                                                ) as m
                                      INNER JOIN contact ON m.customer_id = contact.id 
                                
                                UNION 
                                
                                (Select m.*, CONCAT_WS(' ', contact.first_name, contact.last_name) customer_name
                                      from (SELECT t.id, t.location_id, t.return_date, t.create_date, t.`comment`, t.duration, t.`marketing_option`, t.`marketing_code`, t.`update_date`,
                                                v.make, v.build_date, v.colour AS color, v.model_family AS model, v.`stock_number`,
                                                CONCAT(contact.first_name, ' ', contact.last_name) as salesPerson, t.status,t.customer_id, t.vehicle_rating, t.service_rating, v.registration_number
                                                FROM
                                                test_drive AS t
                                                INNER JOIN vehicle_for_testdrive AS v ON t.vehicle_id = v.id
                                                INNER JOIN `user` AS u ON t.creator_id = u.id
                                                INNER JOIN contact ON u.contact_id = contact.id
                                                WHERE t.location_id = $locationId
                                                AND t.manual_vehicle = TRUE
                                                AND status in ($stringList) 
                                                $rangeQuery
                                                $customerQuery  
                                                $query        
                                                ) as m
                                      INNER JOIN contact ON m.customer_id = contact.id)
                                $order
                                LIMIT $offset, $limit
                            ");

        /*$roleListSize = DB::select("select count(*) as count FROM test_drive AS t
                                    INNER JOIN vehicle AS v ON t.vehicle_id = v.id
                                    WHERE t.location_id = $locationId
                                    AND status in ($stringList) 
                                    $rangeQuery
                                    $customerQuery 
                                    $query");*/

        $roleListSize = DB::select("select COUNT(*) AS count FROM (SELECT t.id AS tid FROM test_drive AS t  
                                    INNER JOIN vehicle AS v ON t.vehicle_id = v.id
                                    WHERE t.location_id = $locationId
                                    AND status in ($stringList) 
                                    AND t.manual_vehicle <> TRUE
                                    $rangeQuery
                                    $customerQuery 
                                    $query
                                    
                                    UNION ALL
                                    
                                    SELECT t.id AS tid2 FROM test_drive AS t  
                                    INNER JOIN vehicle_for_testdrive AS vt ON t.vehicle_id = vt.id
                                    WHERE t.location_id = $locationId
                                    AND status in ($stringList) 
                                    AND t.manual_vehicle = TRUE
                                    $rangeQuery
                                    $customerQuery 
                                    $query) AS td");


        if ($testDriveList){
            $result = array($roleListSize[0], "testDriveList" => $testDriveList);
            return $result;
        }
        else
            return NULL;
    }

    public static function findByVehicleId($vehicleId, $status){
        return DB::select("select * from test_drive where vehicle_id = $vehicleId and status = $status");
    }

    public static function outdateTestDrives() {
        $now    = Carbon::now();
        $date   = $now->toDateTimeString(); 
        return TestDrive::where('return_date', '<', $date)->where('status', TestDrive::STATUS_CODE_IN_PROGRESS)->get();
    }
}
