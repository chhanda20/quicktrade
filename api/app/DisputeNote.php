<?php

namespace App;

use Illuminate\Support\Facades\DB;

class DisputeNote extends Backend
{

    const TYPE_ = 1;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'dispute_note';

}
