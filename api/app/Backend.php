<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Backend extends Model
{
    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public function fillFromJson($json, $fields) {
        foreach ($fields as $field) {
            if ($json->has($field)) {
                $this->{$field} = $json->get($field);
            }
        }
    }
}
