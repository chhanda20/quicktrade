<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Location extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['name', 'location_code', 'status_code', 'company_id', 'lmct', 'address', 'suburb', 'country', 'state', 'postcode'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'location';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 2;

    public static function getListLocation($companyGroupId, $accessableLocationIds, $input){

        $where = "";
        //search by status
        if($input !=null && $input->has('status')) {
            $status = $input->get('status');
            $where .= ' where location.status_code IN (' . implode(',', $status) . ") ";
        }else{
            $where .= " where location.status_code = 2";
        }

        if($accessableLocationIds){
            $ids = implode(",", $accessableLocationIds);
            $where .= " AND company_group.id = $companyGroupId AND location.id IN ($ids)";
        }

        //set limit, offset
        $paging = "";
        if($input !=null && $input->has('offset')){
            $offset = $input->get('offset');
            $limit = $input->get('limit');
            $paging = "LIMIT $offset, $limit";
        }
        //search by company id
        if($input !=null && $input->has('company_id')){
            $companyId = $input->get('company_id');
            $where .= " AND location.company_id = $companyId";
        }

        if($input !=null && $input->has('company_group_id')){
            $companyGroupId1 = $input->get('company_group_id');
            $where .= "AND company_group.id = $companyGroupId1";
        }

        //search by query
        if ($input !=null && $input->has('query')) {
            $where .= " AND location.name LIKE '%" . $input->get('query') . "%' ";
        }

        //order by
        $order = "";
        if($input){
            if ($input->has("orderBy")) {
                $order = "ORDER BY " . $input->get("orderBy");
                if ($input->has("orderDesc") && $input->get("orderDesc")) {
                    $order .= ' DESC ';
                }
            }
        }


        $query = "SELECT SQL_CALC_FOUND_ROWS location.*, country_state.name as state_text,company_group.name as company_group_name,contact.company_name
                            FROM
                            location
                            INNER JOIN company ON location.company_id = company.id
                            INNER JOIN company_group ON company.company_group_id = company_group.id
                            LEFT JOIN contact ON contact.id = company.contact_id
                            LEFT JOIN country_state ON location.state = country_state.id 
                            
                            $where $order $paging ";

        \LOG::debug($query);

        $locationList = DB::select($query);
        $locationListSize = DB::select("select FOUND_ROWS() as count");



        $result = array($locationListSize[0], "locationList" => $locationList);
        return $result;
    }

    public static function getLocationById($locationId){
        return DB::select("SELECT location.* , company.company_group_id 
                            FROM location
                            INNER JOIN company ON location.company_id = company.id
                            where location.id = $locationId");
    }

    public static function getLocationCodeInCompanyGroup($locationCode,$companyId ){
        return DB::select("SELECT `location`.id FROM `location` join company on location.company_id = company.id 
                              WHERE location_code = '$locationCode' 
                              AND company.company_group_id IN (select company.company_group_id from company where company.id = $companyId)");
    }

    public static function accessableLocationsQuery($userId) {
        $dataAccess = DataAccess::where('user_id', $userId)->first();

        $includes = [];
        $excludes = [];
        if ($dataAccess->include != '') {
            $includes = array_map('intval', explode(',', $dataAccess->include));
        }
        if ($dataAccess->exclude != '') {
            $excludes = array_map('intval', explode(',', $dataAccess->exclude));
        }
        //[fix-me] need improve this code performent - check range, unset array, release
        $locationIds = range($dataAccess->range_from, $dataAccess->range_to);
        $locationIds = array_merge ($locationIds, $includes);
        $locationIds = array_diff($locationIds, $excludes);
        $locationIds = array_unique($locationIds);

        $locations = Location::whereIn('location.id', $locationIds);
        return $locations;
    }

    public static function rating($locationId, $type) {
        $dealerKey      = $type.'_id';
        $expiredDay    = AppProperty::where('id', 'rating_expired_time')->first()->value;
        $startDate      = Carbon::now()->subDays($expiredDay);

        $totalTrace = Transaction::join('valuation', 'valuation.id', '=', 'transaction.valuation_id')
            ->join('vehicle', 'vehicle.id', '=', 'valuation.vehicle_id')
            ->where('vehicle.trade_in_location', $locationId)
            ->whereDate('transaction.create_date', '>', $startDate)
            ->count();
        if ($totalTrace < 5) {
            return 0;
        }

        $totalFault = Dispute::join('transaction', 'transaction.id', '=', 'dispute.transaction_id')
            ->join('valuation', 'valuation.id', '=', 'transaction.valuation_id')
            ->join('vehicle', 'vehicle.id', '=', 'valuation.vehicle_id')
            ->whereRaw("find_in_set(transaction.$dealerKey, dispute.fault_user_ids)")
            ->where('vehicle.trade_in_location', $locationId)
            ->whereDate('transaction.create_date', '>', $startDate)
            ->count();

        $rate = $totalFault / $totalTrace;
        if ($rate <= 0.1) {
            return 5;
        } else if ($rate <= 0.15) {
            return 4;
        } else if ($rate <= 0.19) {
            return 3;
        } else if ($rate < 0.24) {
            return 2;
        } else {
            return 1;
        }
    }

    public static function updateRating($locationId) {
        $location = Location::find($locationId);
        if ($location) {
            $location->seller_rating = Location::ratingAsSeller($locationId);
            $location->buyer_rating = Location::ratingAsBuyer($locationId);
            $location->save();
        }
    }

    public static function updateAllRatingSystem() {
        $locations = Location::all();
        foreach ($locations as $location) {
            Location::updateRating($location->id);
        }

    }

    public static function ratingAsSeller($locationId) {
        return Location::rating($locationId, 'seller');
    }

    public static function ratingAsBuyer($locationId) {
        return Location::rating($locationId, 'buyer');
    }

    public static function isSpeedySeller($locationId) {
        $time = Transaction::select(DB::raw("avg(timestampdiff(hour, `transaction`.`payment_receive_time`, `transaction`.`update_date`)) as avg_hour_diff"))
                    ->join('valuation', 'valuation.id', '=', 'transaction.valuation_id')
                    ->join('vehicle', 'vehicle.id', '=', 'valuation.vehicle_id')
                    ->whereNotNull('transaction.payment_receive_time')
                    ->where('vehicle.trade_in_location', $locationId)
                    ->where('transaction.status', Transaction::STATUS_COMPLETE)
                    ->first()
                    ->avg_hour_diff;
        return $time ? $time < AppProperty::where('id', 'speedy_seller_avg_time')->first()->value : NULL;
    }

    public static function isSpeedyBuyer($locationId) {
        $time = Transaction::select(DB::raw("avg(timestampdiff(hour, `transaction`.`create_date`, `transaction`.`payment_receive_time`)) as avg_hour_diff"))
                    ->join('valuation', 'valuation.id', '=', 'transaction.valuation_id')
                    ->join('vehicle', 'vehicle.id', '=', 'valuation.vehicle_id')
                    ->whereNotNull('transaction.payment_receive_time')
                    ->where('vehicle.trade_in_location', $locationId)
                    ->where('transaction.status', Transaction::STATUS_COMPLETE)
                    ->first()
                    ->avg_hour_diff;
        return $time ? $time < AppProperty::where('id', 'speedy_buyer_avg_time')->first()->value : NULL;
    } 
}
