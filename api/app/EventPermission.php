<?php

namespace App;

use Illuminate\Support\Facades\DB;

class EventPermission extends Backend
{
    const CREATED_AT    = NULL;
    const UPDATED_AT    = NULL;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];


    protected $table = 'event_permission';
}
