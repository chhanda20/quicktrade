<?php

namespace App\Http\Controllers;



use App\Dispute;
use App\DisputeItem;
use App\DisputeNote;
use App\Document;
use App\Libraries\ErrorCodes;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\Libraries\Event;
use App\Location;
use App\Transaction;
use App\User;
use App\Valuation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use WindowsAzure\Common\ServicesBuilder;
use MicrosoftAzure\Storage\Blob\Models\CreateContainerOptions;
use MicrosoftAzure\Storage\Blob\Models\PublicAccessType;
use MicrosoftAzure\Storage\Common\ServiceException;


class DisputeController extends Controller
{

    public function findCompanyDispute(Request $request) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'offset' => 'required',
            'limit' => 'required',
            'status' => 'array',
            'query' => 'string',
            'type' => 'required|in:all,seller,buyer',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $apiPermission = array(Permissions::DISPUTE_INFO_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $json = $request->json();

        $offset = $json->get('offset');
        $limit = $json->get('limit');

        $where = "";

        if ($json->has('status')) {
            $status = implode(',', $json->get('status'));
            $where = " dis.status IN ($status)";
        }

        if ($json->has('query')) {
            $text = $json->get('query');
            $text = str_replace('\'', '"', $text); // for safety reasons;
            if ($text) {
                $query = "(YEAR(build_date) LIKE '$text%') OR (CAST(va.stock_number as CHAR) LIKE '$text%')";
                $query .= "OR MATCH(make, family, series, style, engine_type, engine_size, variant, cylinder, transmission, registration_number, body_type) AGAINST ('$text*' IN BOOLEAN MODE)";

                if ($where) {
                    $where .= ' AND ';
                }
                $where .= "($query) ";
            }
        }

        $locationIds = implode(',', $user->getAccessableLocationIds());

        if ($json->get('type') === 'seller') {
            if ($where) {
                $where .= ' AND ';
            }
            $where .= " seller.location_id IN ($locationIds) ";
        }

        if ($json->get('type') === 'buyer') {
            if ($where) {
                $where .= ' AND ';
            }
            $where .= " buyer.location_id IN ($locationIds) ";
        }


        if ($where) {
            $where = 'WHERE ' . $where;
        }

        $result = DB::select("
          SELECT ve.*,ve.id vehicle_id, buyer.id buyer_id, buyer_contact.first_name buyer_first_name, buyer_contact.last_name buyer_last_name,
           seller.id seller_id, seller_contact.first_name seller_first_name, seller_contact.last_name seller_last_name, va.overall_rating,
           agent_contact.first_name support_agent_first_name, agent_contact.last_name support_agent_last_name,
           t.*, dis.*, t.rid transaction_rid, (seller.location_id IN ($locationIds)) is_seller, (buyer.location_id IN ($locationIds)) is_buyer  FROM transaction t
            RIGHT JOIN dispute dis ON dis.transaction_id=t.id
            LEFT JOIN valuation va ON (va.version=t.valuation_version AND (va.id=t.valuation_id OR va.origin_id=t.valuation_id))
            LEFT JOIN vehicle ve ON ve.id=va.vehicle_id
            LEFT JOIN `user` buyer ON buyer.id=t.buyer_id
            LEFT JOIN contact buyer_contact ON buyer_contact.id=buyer.contact_id
            LEFT JOIN `user` seller ON seller.id=t.seller_id
            LEFT JOIN contact seller_contact ON seller_contact.id=seller.contact_id
            LEFT JOIN `user` agent ON agent.id=dis.support_agent_id 
            LEFT JOIN `contact` agent_contact ON agent_contact.id = agent.contact_id
            $where
            ORDER BY t.create_date DESC
            LIMIT $offset, $limit
        ");

        foreach($result as $r) {
            $details = [];
            if ($r->make) {
                $details[] = trim($r->make);
            }
            if ($r->family) {
                $details[] = trim($r->family);
            }
            if ($r->series) {
                $details[] = trim($r->series);
            }
            if ($r->style) {
                $details[] = trim($r->style);
            }
            if ($r->engine_type) {
                $details[] = trim($r->engine_type);
            }
            if ($r->engine_capacity) {
                $details[] = trim($r->engine_capacity);
            }
            if ($r->engine_size) {
                $details[] = trim($r->engine_size);
            }
            if ($r->variant) {
                $details[] = trim($r->variant);
            }
            if ($r->cylinder) {
                $details[] = trim($r->cylinder);
            }
            if ($r->transmission) {
                $details[] = trim($r->transmission);
            }
            $r->vehicle_name = implode(' ', $details);
        }

        return $this->responseData($result);
    }

    public function countCompanyDispute(Request $request) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'query' => 'string',
            'status' => 'array',
            'type' => 'required|in:all,seller,buyer',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $apiPermission = array(Permissions::DISPUTE_INFO_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $json = $request->json();

        $where = "";

        if ($json->has('status')) {
            $status = implode(',', $json->get('status'));
            $where = " dis.status IN ($status)";
        }

        if ($json->has('query')) {
            $text = $json->get('query');
            $text = str_replace('\'', '"', $text); // for safety reasons;
            if ($text) {
                $query = "(YEAR(build_date) LIKE '$text%') OR (CAST(va.stock_number as CHAR) LIKE '$text%')";
                $query .= "OR MATCH(make, family, series, style, engine_type, engine_size, variant, cylinder, transmission, registration_number, body_type) AGAINST ('$text*' IN BOOLEAN MODE)";

                if ($where) {
                    $where .= ' AND ';
                }
                $where .= "($query) ";
            }
        }

        $locationIds = implode(',', $user->getAccessableLocationIds());

        if ($json->get('type') === 'all' || $json->get('type') === 'seller') {
            if ($where) {
                $where .= ' AND ';
            }
            $where .= " seller.location_id IN ($locationIds) ";
        }

        if ($json->get('type') === 'all' || $json->get('type') === 'buyer') {
            if ($where) {
                $where .= ' AND ';
            }
            $where .= " buyer.location_id IN ($locationIds) ";
        }

        if ($where) {
            $where = 'WHERE ' . $where;
        }

        $result = DB::select("
          SELECT COUNT(*) as count FROM transaction t
            RIGHT JOIN dispute dis ON dis.transaction_id=t.id
            LEFT JOIN valuation va ON (va.version=t.valuation_version AND (va.id=t.valuation_id OR va.origin_id=t.valuation_id))
            LEFT JOIN vehicle ve ON ve.id=va.vehicle_id
            LEFT JOIN `user` buyer ON buyer.id=t.buyer_id
            LEFT JOIN contact buyer_contact ON buyer_contact.id=buyer.contact_id
            LEFT JOIN `user` seller ON seller.id=t.seller_id
            LEFT JOIN contact seller_contact ON seller_contact.id=seller.contact_id
            LEFT JOIN `user` agent ON agent.id=dis.support_agent_id 
            LEFT JOIN `contact` agent_contact ON agent_contact.id = agent.contact_id
            $where
        ");

        return $this->responseData($result[0]);
    }

    public function find(Request $request) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'offset' => 'required',
            'limit' => 'required',
            'status' => 'array',
            'query' => 'string',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $apiPermission = array(Permissions::DISPUTE_INFO_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $json = $request->json();

        $offset = $json->get('offset');
        $limit = $json->get('limit');

        $where = "";

        if ($json->has('status')) {
            $status = implode(',', $json->get('status'));
            $where = " dis.status IN ($status)";
        }

        if ($json->has('query')) {
            $text = $json->get('query');
            $text = str_replace('\'', '"', $text); // for safety reasons;
            if ($text) {
                $query = "(YEAR(build_date) LIKE '$text%') OR (CAST(va.stock_number as CHAR) LIKE '$text%')";
                $query .= "OR MATCH(make, family, series, style, engine_type, engine_size, variant, cylinder, transmission, registration_number, body_type) AGAINST ('$text*' IN BOOLEAN MODE)";

                if ($where) {
                    $where .= ' AND ';
                }
                $where .= "($query) ";
            }
        }

        if ($where) {
            $where = 'WHERE ' . $where;
        }

        $result = DB::select("
          SELECT ve.*,ve.id vehicle_id, buyer.id buyer_id, buyer_contact.first_name buyer_first_name, buyer_contact.last_name buyer_last_name,
           seller.id seller_id, seller_contact.first_name seller_first_name, seller_contact.last_name seller_last_name, va.overall_rating,
           agent_contact.first_name support_agent_first_name, agent_contact.last_name support_agent_last_name,
           t.*, dis.*, t.rid transaction_rid FROM transaction t
            RIGHT JOIN dispute dis ON dis.transaction_id=t.id
            LEFT JOIN valuation va ON (va.version=t.valuation_version AND (va.id=t.valuation_id OR va.origin_id=t.valuation_id))
            LEFT JOIN vehicle ve ON ve.id=va.vehicle_id
            LEFT JOIN `user` buyer ON buyer.id=t.buyer_id
            LEFT JOIN contact buyer_contact ON buyer_contact.id=buyer.contact_id
            LEFT JOIN `user` seller ON seller.id=t.seller_id
            LEFT JOIN contact seller_contact ON seller_contact.id=seller.contact_id
            LEFT JOIN `user` agent ON agent.id=dis.support_agent_id 
            LEFT JOIN `contact` agent_contact ON agent_contact.id = agent.contact_id
            $where
            ORDER BY t.create_date DESC
            LIMIT $offset, $limit
        ");

        foreach($result as $r) {
            $details = [];
            if ($r->make) {
                $details[] = trim($r->make);
            }
            if ($r->family) {
                $details[] = trim($r->family);
            }
            if ($r->series) {
                $details[] = trim($r->series);
            }
            if ($r->style) {
                $details[] = trim($r->style);
            }
            if ($r->engine_type) {
                $details[] = trim($r->engine_type);
            }
            if ($r->engine_capacity) {
                $details[] = trim($r->engine_capacity);
            }
            if ($r->engine_size) {
                $details[] = trim($r->engine_size);
            }
            if ($r->variant) {
                $details[] = trim($r->variant);
            }
            if ($r->cylinder) {
                $details[] = trim($r->cylinder);
            }
            if ($r->transmission) {
                $details[] = trim($r->transmission);
            }
            $r->vehicle_name = implode(' ', $details);
        }

        return $this->responseData($result);
    }

    public function findCount(Request $request) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'status' => 'array'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $apiPermission = array(Permissions::DISPUTE_INFO_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $json = $request->json();

        $where = "";

        if ($json->has('status')) {
            $status = implode(',', $json->get('status'));
            $where = " dis.status IN ($status)";
        }

        if ($json->has('query')) {
            $text = $json->get('query');
            $text = str_replace('\'', '"', $text); // for safety reasons;
            if ($text) {
                $query = "(YEAR(build_date) LIKE '$text%') OR (CAST(va.stock_number as CHAR) LIKE '$text%')";
                $query .= "OR MATCH(make, family, series, style, engine_type, engine_size, variant, cylinder, transmission, registration_number, body_type) AGAINST ('$text*' IN BOOLEAN MODE)";

                if ($where) {
                    $where .= ' AND ';
                }
                $where .= "($query) ";
            }
        }

        if ($where) {
            $where = 'WHERE ' . $where;
        }

        $result = DB::select("
          SELECT COUNT(*) as count FROM transaction t
            RIGHT JOIN dispute dis ON dis.transaction_id=t.id
            LEFT JOIN valuation va ON (va.version=t.valuation_version AND (va.id=t.valuation_id OR va.origin_id=t.valuation_id))
            LEFT JOIN vehicle ve ON ve.id=va.vehicle_id
            LEFT JOIN `user` buyer ON buyer.id=t.buyer_id
            LEFT JOIN contact buyer_contact ON buyer_contact.id=buyer.contact_id
            LEFT JOIN `user` seller ON seller.id=t.seller_id
            LEFT JOIN contact seller_contact ON seller_contact.id=seller.contact_id
            LEFT JOIN `user` agent ON agent.id=dis.support_agent_id 
            LEFT JOIN `contact` agent_contact ON agent_contact.id = agent.contact_id
            $where
        ");

        return $this->responseData($result[0]);
    }



    public function setUnderReview(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $apiPermission = array(Permissions::DISPUTE_RESOLVE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $dispute = Dispute::find($id);
        if (!$dispute) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        $userType = $request->input('user_type');

        $dispute = Dispute::where('id', $id)->where('status', Dispute::STATUS_IN_PROGRESS)->first();
        $dispute->status = Dispute::STATUS_UNDER_REVIEW;
        if ($userType) {
            $dispute->updateFaultIdsByType($userType, false);
        }
        $dispute->save();

        if (!$dispute) {
            return $this->responseError(ErrorCodes::INVALID, 'Not in progress status');
        }
        return $this->responseData(['status' => 'success']);
    }

    public function accept(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $apiPermission = array(Permissions::DISPUTE_RESOLVE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $dispute = Dispute::find($id);
        if (!$dispute) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        $count = Dispute::where('id', $id)->where('status', Dispute::STATUS_NEW)->update([
            'status' => Dispute::STATUS_IN_PROGRESS,
            'support_agent_id' => $user->id,
            'accept_date' => date('Y-m-d H:i:s')
        ]);

        if ($count == 0) {
            return $this->responseError(ErrorCodes::INVALID, 'Not in new status');
        }
        return $this->responseData(['status' => 'success']);
    }

    public function getOne(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $apiPermission = array(Permissions::DISPUTE_INFO_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $result = DB::select("
          SELECT ve.*,ve.id vehicle_id,
           valuer.id valuer_id, 
           seller_location.id seller_location_id,
           seller_location.name seller_location_name,
           va.overall_rating,
           buyer_location.name buyer_location_name,
           agent_contact.first_name support_agent_first_name,
           agent_contact.last_name support_agent_last_name,
           seller.contact_id seller_contact_id,
           t.*, dis.* FROM transaction t
            RIGHT JOIN dispute dis ON dis.transaction_id=t.id
            LEFT JOIN valuation va ON (va.version=t.valuation_version AND (va.id=t.valuation_id OR va.origin_id=t.valuation_id))
            LEFT JOIN vehicle ve ON ve.id=va.vehicle_id
            LEFT JOIN `user` valuer ON valuer.id=va.valuer_id
            LEFT JOIN `user` seller ON seller.id=t.seller_id
            LEFT JOIN `location` seller_location ON seller_location.id=seller.location_id
            LEFT JOIN `user` buyer ON buyer.id=t.buyer_id
            LEFT JOIN `location` buyer_location ON buyer.id=buyer.location_id
            LEFT JOIN `user` agent ON agent.id=dis.support_agent_id 
            LEFT JOIN `contact` agent_contact ON agent_contact.id = agent.contact_id
           WHERE dis.id = $id
            LIMIT 1
        ");


//        $result = DB::select("
//          SELECT ve.*,ve.id vehicle_id, valuer.id valuer_id, valuer_contact.first_name valuer_first_name, valuer_contact.last_name valuer_last_name,
//           salesperson.id salesperson_id, salesperson_contact.first_name salesperson_first_name, salesperson_contact.last_name salesperson_last_name, salesperson_location.name salesperson_location_name,
//           salesperson_contact.work_phone salesperson_work_number, salesperson_contact.email salesperson_email,
//           va.overall_rating,
//           buyer_contact.first_name buyer_first_name, buyer_contact.last_name buyer_last_name, buyer_contact.work_phone buyer_work_number, buyer_contact.email buyer_email, buyer_location.name buyer_location_name,
//           t.*, dis.* FROM transaction t
//            RIGHT JOIN dispute dis ON dis.transaction_id=t.id
//            LEFT JOIN valuation va ON va.id=t.valuation_id
//            LEFT JOIN vehicle ve ON ve.id=va.vehicle_id
//            LEFT JOIN `user` valuer ON valuer.id=va.valuer_id
//            LEFT JOIN contact valuer_contact ON valuer_contact.id=valuer.contact_id
//            LEFT JOIN `user` salesperson ON salesperson.id=va.user_id
//            LEFT JOIN contact salesperson_contact ON salesperson_contact.id=salesperson.contact_id
//            LEFT JOIN `location` salesperson_location ON salesperson_location.id=salesperson.location_id
//            LEFT JOIN `user` buyer ON buyer.id=t.buyer_id
//            LEFT JOIN `contact` buyer_contact ON buyer_contact.id=buyer.contact_id
//            LEFT JOIN `location` buyer_location ON buyer.id=buyer.location_id
//           WHERE dis.id = $id
//            LIMIT 1
//        ");

        foreach($result as $r) {
            $details = [];
            if ($r->make) {
                $details[] = trim($r->make);
            }
            if ($r->family) {
                $details[] = trim($r->family);
            }
            if ($r->series) {
                $details[] = trim($r->series);
            }
            if ($r->style) {
                $details[] = trim($r->style);
            }
            if ($r->engine_type) {
                $details[] = trim($r->engine_type);
            }
            if ($r->engine_capacity) {
                $details[] = trim($r->engine_capacity);
            }
            if ($r->engine_size) {
                $details[] = trim($r->engine_size);
            }
            if ($r->variant) {
                $details[] = trim($r->variant);
            }
            if ($r->cylinder) {
                $details[] = trim($r->cylinder);
            }
            if ($r->transmission) {
                $details[] = trim($r->transmission);
            }
            $r->vehicle_name = implode(' ', $details);
        }
        if (sizeof($result) == 0) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        return $this->responseData($result[0]);
    }

    public function createNote(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'note' => 'required|string'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);
        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $json = $request->json();

        $a = new DisputeNote();
        $a->creator_id = $user->id;
        $a->dispute_id = $id;
        $a->note = $json->get('note');
        $a->save();

        $contact =  $user->getContact();

        if ($contact) {
            $a->creator_first_name = $contact->first_name;
            $a->creator_last_name = $contact->last_name;
        }

        return $this->responseData($a);
    }

    public function deleteNote(Request $request, $id, $note_id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $a = DisputeNote::where('dispute_id', $id)->where('id', $note_id);
        if (!$a) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        $a->delete();

        return $this->responseData($a);
    }

    public function getAllNote(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $r = DB::select("
            SELECT c.first_name creator_first_name, c.last_name creator_last_name, n.* FROM dispute_note n 
            LEFT JOIN `user` u ON u.id=n.creator_id
            LEFT JOIN `contact` c ON c.id=u.contact_id
            WHERE n.dispute_id = $id
        ");
        return $this->responseData($r);
    }


    public function resolve(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'items' => 'array',
            'decision' => 'required|integer',
            'general_recondition_cost' => 'integer',
            'user_type' => 'integer'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $apiPermission = array(Permissions::DISPUTE_RESOLVE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $d = Dispute::find($id);
        if (!$d) {
            return $this->responseError(ErrorCodes::NOT_FOUND, "No such Dispute");
        }

        $json = $request->json();
        $items = $json->get('items');

        foreach($items as $item) {
            if (key_exists('id', $item) && key_exists('cost_variance', $item)) {
                $a = DisputeItem::find($item['id']);
                if ($a) {
                    $a->cost_variance = $item['cost_variance'];
                    $a->save();
                }
            }
        }

        $transaction        = Transaction::find($d->transaction_id);
        $type = $json->get('user_type');
        switch ($type) {
            case Dispute::USER_TYPE_NO_ONE:
                $d->fault_user_ids = '';
                break;
            case Dispute::USER_TYPE_BUYER:
                $d->fault_user_ids = $transaction->buyer_id;
                break;
            case Dispute::USER_TYPE_SELLER:
                $d->fault_user_ids = $transaction->seller_id;
                break;
            case Dispute::USER_TYPE_BOTH:
                $d->fault_user_ids = $transaction->buyer_id.','.$transaction->seller_id;
                break;
        }

        $d->decision        = $json->get('decision');
        $d->status          = Dispute::STATUS_RESOLVE;

        switch ($d->decision) {
            case Dispute::DECISION_PAY_COST_VARIANCE:
            case Dispute::DECISION_NO_CHANGE:
                if ($d->decision == Dispute::DECISION_PAY_COST_VARIANCE) {
                    $event = Event::TRANSACTION_RESOLVE_DISPUTE_PAY_COST_VARIANT;
                    \LOG::debug("start generate document for event: [$event]");
                    Document::generatorPDFAndUploadAzure($transaction->seller_id, $event, $transaction->id, 'seller');
                    Document::generatorPDFAndUploadAzure($transaction->seller_id, $event, $transaction->id, 'buyer');

                    $buyerData = [];
                    $buyerData['attach'] = Document::generatorPDFAndUploadAzure($transaction->buyer_id, $event, $id, 'email');
    
                    User::notice($transaction->buyer_id, $event, $transaction->id, $buyerData);
                } else {
                    $event = Event::TRANSACTION_RESOLVE_DISPUTE_NO_CHANGE;
                    $buyerData = [];
                    $sellerData = [];

                    \LOG::debug("start generate document for event: [$event]");
                    Document::generatorPDFAndUploadAzure($transaction->seller_id, $event, $transaction->id, 'seller');
                    Document::generatorPDFAndUploadAzure($transaction->seller_id, $event, $transaction->id, 'buyer');
    
                    User::notice($transaction->buyer_id, $event, $id, $buyerData);
                    User::notice($transaction->seller_id, $event, $id, $sellerData);
                }

                $vehicle = $transaction->getVehicle();
                $n = $vehicle->replicate();
        
                $n->company_group_id = $user->company_group_id;
                $n->creator_id = $user->id;
                $n->create_date = date("Y/m/d H:i:s");
                $n->update_date = date("Y/m/d H:i:s");
                $n->location_id = $user->location_id;
                $n->trade_in_location = $user->location_id;
                $n->save();
        
                $vehicle->cloneImage($n->id);
        
                $valuation = $transaction->getValuation();
                $v = $valuation->replicate();
                $v->user_id = $transaction->buyer_id;
                $v->vehicle_id = $n->id;
                $v->company_group_id = User::find($transaction->buyer_id)->company_group_id;
                $v->status = Valuation::STATUS_COMPLETE_TRADED_IN;
                $v->estimated_recon_cost = 0;
                $v->requested_approve_price = 0;
                $v->negotiating_price = 0;
                $v->max_price = 0;
                $v->offer_value = 0;
                $v->valuation_value = 0;
                $v->requested_price = 0;
                $v->trade_price = 0;
                $v->offer_price = 0;
                $v->total_recon_cost = 0;
                $v->create_date = date("Y-m-d H:i:s");
                $v->update_date = date("Y-m-d H:i:s");
                $v->save();

                break;
            case Dispute::DECISION_RETURN_VEHICLE:                
                $event = Event::TRANSACTION_RESOLVE_DISPUTE_RETURN_VEHICLE;
                $buyerData = [];
                $sellerData = [];

                \LOG::debug("start generate document for event: [$event]");

                $buyerData['attach'] = Document::generatorPDFAndUploadAzure($transaction->buyer_id, $event, $id, 'buyer');
                Document::generatorPDFAndUploadAzure($transaction->seller_id, $event, $id, 'seller');

                User::notice($transaction->buyer_id, $event, $transaction->id, $buyerData);
                User::notice($transaction->seller_id, $event, $transaction->id, $sellerData);
                break;
        }

        if ($json->has('general_recondition_cost')) {
            $d->general_recondition_cost_variance = $json->get('general_recondition_cost');
        }

        $d->save();

        Transaction::updateStatus($d->transaction_id, Transaction::STATUS_DOCUMENTING);

        $locationId = Valuation::where('valuation.id', $transaction->valuation_id)
                        ->join('vehicle', 'valuation.vehicle_id', '=', 'vehicle.id')
                        ->first()
                        ->location_id;

        $ids = explode(",", $d->fault_user_ids);
        foreach ($ids as $id) {
            if ($id != '') {
                Location::updateRating($locationId);
            }
        }

        return $this->responseData($d);
    }

}
