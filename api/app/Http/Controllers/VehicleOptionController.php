<?php

namespace App\Http\Controllers;

use App\GlassModelOption;
use App\GlassOption;
use App\Libraries\ErrorCodes;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\User;
use App\Vehicle;
use App\VehicleOption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class VehicleOptionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function updateOptions(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VEHICLE_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $vehicle = Vehicle::find($id);
        if (!$vehicle) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, [], "No such vehicle");
        }

        $v = Validator::make($request->all(), [
            'options' => 'required|array'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $options = $request->json()->get('options');
        $ids = GlassOption::getByIds($options);

        $result = DB::SELECT("
            SELECT vehicle_option_code_id
            FROM `vehicle_option`
            WHERE type=:type AND `vehicle_id` =:vehicle_id"
            , [
                'type' => VehicleOption::TYPE_OPTIONAL,
                'vehicle_id' => $vehicle->id
            ]);

        $selected = [];
        foreach ($result as $i) {
            $selected[$i->vehicle_option_code_id] = TRUE;
        }

        foreach ($ids as $j) {
            $price = 0;
            $i = trim($j->glass_option_id);
            if (isset($selected[$i])) {
                unset($selected[$i]);
            } else {
                $glassModelOption = GlassModelOption::select('option')
                    ->where('glass_code', $vehicle->glass_guide_code)
                    ->where('rec_type', 'Optional:')
                    ->first();
                $optionStr = $glassModelOption->option;
                $optionParts = explode(',', $optionStr);
                foreach ($optionParts as $part) {
                    $temp = explode(':', $part);
                    $optionCode = trim($temp[0]);
                    if ($i == $optionCode) {
                        $price = $temp[1];
                    }
                }
                $newOption = new VehicleOption();
                $newOption->creator_id = $newOption->updater_id = $user->id;
                $newOption->create_date = $newOption->update_date = new \DateTime('NOW');
                $newOption->type = VehicleOption::TYPE_OPTIONAL;
                $newOption->qty = 1;
                $newOption->vehicle_option_code_id = $i;
                $newOption->vehicle_id = $vehicle->id;
                $newOption->price = $price;
                $newOption->save();

                \Log::debug("Select $i");
            }
        }

        foreach ($selected as $i => $v) {
            VehicleOption::where('vehicle_id', $vehicle->id)->where('vehicle_option_code_id', $i)->delete();
        }
        return $this->getOptions($request, $id);
    }

    public function getOptions(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VEHICLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $vehicle = Vehicle::find($id);
        if (!$vehicle) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, [], "No such vehicle");
        }


        if (!$vehicle->glass_guide_code) {
            return $this->responseError(ErrorCodes::INVALID, [], "Vehicle[$id] doesn't have glass_guide_code");
        }

//        $result = DB::SELECT("SELECT options FROM `vehicle_model_option` o RIGHT JOIN `vehicle_model` m ON o.vehicle_model_id = m.id
//            WHERE m.code = :model_code LIMIT 1", ['model_code' => $vehicle->glass_guide_code]);

        $code = trim($vehicle->glass_guide_code). '%';

        $result = DB::SELECT("SELECT `option` FROM `glass_model_option` WHERE glass_code LIKE '$code' AND rec_type LIKE 'Optional:%'");


        if (!$result) {
            return $this->responseData([]);
        }

        $available = [];
        foreach($result as $r) {
            $array = explode(',', $r->option);
            foreach ($array as $i) {
                $k = explode(':', $i);
                $k = trim($k[0]);
                array_push($available, $k);
            }
        }

        $available = GlassOption::getByIds($available);

        $result = DB::SELECT("SELECT vehicle_option_code_id FROM `vehicle_option` WHERE type=:type AND `vehicle_id` =:vehicle_id"
            , ['type' => VehicleOption::TYPE_OPTIONAL, 'vehicle_id' => $vehicle->id]);


        $selected = [];
        foreach ($result as $i) {
            $selected[$i->vehicle_option_code_id] = TRUE;
        }

        $options = [];
        foreach ($available as $k) {
            $key = trim($k->glass_option_id);
            $value = trim($k->name);
            $options[$key] = [
                'name' => $value
            ];
            if (isset($selected[$key])) {
                $options[$key]['selected'] = TRUE;
            } else {
                $options[$key]['selected'] = FALSE;
            }

        }

        return $this->responseData($options);
    }

    public function getAllExtraOption(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VEHICLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $vehicle = Vehicle::find($id);
        if (!$vehicle) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, [], "No such vehicle");
        }

//        if (!$vehicle->vehicle_model_code) {
//            return $this->responseData([]);
//        }

        $result = VehicleOption::where('vehicle_id', $vehicle->id)->where('type', VehicleOption::TYPE_EXTRA)->get();

        return $this->responseData($result);
    }

    public function createExtraOption(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VEHICLE_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $vehicle = Vehicle::find($id);
        if (!$vehicle) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, [], "No such vehicle");
        }

//        if (!$vehicle->vehicle_model_code) {
//            return $this->responseError(ErrorCodes::INVALID, [], "Vehicle[$id] doesn't have vehicle_model_code");
//        }

        $v = Validator::make($request->all(), [
            'description' => 'required'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $value = $request->json()->get("description");

        if (is_string($value)) {
            $newOption = new VehicleOption();
            $newOption->creator_id = $newOption->updater_id = $user->id;
            $newOption->create_date = $newOption->update_date = (new \DateTime('NOW'))->format('Y-m-d H:i:s');
            $newOption->type = VehicleOption::TYPE_EXTRA;
            $newOption->qty = 1;
            $newOption->vehicle_id = $vehicle->id;
            $newOption->description = $value;
            $newOption->save();
            return $this->responseData($newOption);
        } else {
            $new = [];
            foreach ($value as $val) {
                $newOption = new VehicleOption();
                $newOption->creator_id = $newOption->updater_id = $user->id;
                $newOption->create_date = $newOption->update_date = (new \DateTime('NOW'))->format('Y-m-d H:i:s');
                $newOption->type = VehicleOption::TYPE_EXTRA;
                $newOption->qty = 1;
                $newOption->vehicle_id = $vehicle->id;
                $newOption->description = $val;
                $newOption->save();
                array_push($new, $newOption);
            }
            return $this->responseData($new);
        }
    }

    public function deleteExtraOption(Request $request, $vehicle_id, $id)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VEHICLE_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $vehicle = Vehicle::find($vehicle_id);
        if (!$vehicle) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, [], "No such vehicle");
        }

        $option = VehicleOption::find($id);

        if (!$option) {
            return $this->responseError(ErrorCodes::NOT_EXISTS);
        }

        if ($option->vehicle_id == $vehicle->id && $option->type == VehicleOption::TYPE_EXTRA) {
            $option->delete();
            return $this->responseData($option);
        }

        return $this->responseError(ErrorCodes::INVALID);
    }
    
    public function deleteMultiExtraOption(Request $request, $vehicle_id)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VEHICLE_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){
            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $vehicle = Vehicle::find($vehicle_id);
        if (!$vehicle) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, [], "No such vehicle");
        }

        $v = Validator::make($request->all(), [
            'ids' => 'required|array'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $items = VehicleOption::where('vehicle_id', $vehicle->id)->where('type', VehicleOption::TYPE_EXTRA)->whereIn('id', $request->json()->get('ids'))->get();

        VehicleOption::where('vehicle_id', $vehicle->id)->where('type', VehicleOption::TYPE_EXTRA)->whereIn('id', $request->json()->get('ids'))->delete();

        return $this->responseData($items);
    }


}
