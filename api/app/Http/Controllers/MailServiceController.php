<?php
/**
 * Created by PhpStorm.
 * User: huyda
 * Date: 8/7/17
 * Time: 1:12 PM
 */


namespace App\Http\Controllers;

use App\Contact;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\Libraries\Event;

class MailServiceController {

    static function send($user, $event, $sourceId, $data) {
        if (User::enableForEvent($user->id, $event, 'email') !== false) {
            \LOG::debug("send notification/email to user $user->email event $event | email");

            $titlePices     = explode("_", $event);
            $title          = implode(" ", $titlePices);
            $data['title']  = $title;
            $data['attach'] = isset($data['attach']) ? $data['attach'] : null;
            $data['body'] = isset($data['body']) ? $data['body'] : null;
            return MailServiceController::sendCommonMail($user, $data);
        } else {
            \LOG::debug("send notification/email to user $user->email event $event | disable email");
        }
    }

    static function sendCommonMail($user, $data) {
        if (!$user->receive_email) {
            return;
        }
        $client = new Client(['base_uri' => env('APP_MAIL_URL')]);
        $var = [
            'json' => [
                'to_email' => $user->email,
                'fullname' => Contact::find($user->contact_id)->full_name(),
                'attach'   => $data['attach'],
                'title'    => $data['title'],
                'body'     => $data['body']
            ]];
        $response = null;
        try {
            $response = $client->post('send', $var);
        } catch (Exception $error) {
            \LOG::debug("--> 999 Can't send email: [$error]");
        }
        return $response;
    }

    static function emailContentGenerator($user, $event, $sourceId) {
        

        return $response;
    }

    static function sendWishlistAppear($buyer, $attach) {
        if (!$buyer->receive_email) {
            return;
        }

        $client = new Client(['base_uri' => env('APP_MAIL_URL')]);
        $response = $client->post('wishlist/notify', [
            'json' => [
                'to_email' => $buyer->email,
                'fullname' => Contact::find($buyer->contact_id)->full_name(),
                'attach'   => $attach
            ]
        ]);

        return $response;
    }

    static function sendPaymentRequest($buyer, $attach) {
        if (!$buyer->receive_email) {
            return;
        }

        $client = new Client(['base_uri' => env('APP_MAIL_URL')]);
        $response = $client->post('transaction/paymentRequest', [
            'json' => [
                'to_email' => $buyer->email,
                'fullname' => Contact::find($buyer->contact_id)->full_name(),
                'attach'   => $attach
            ]
        ]);

        return $response;
    }

    static function sendPaymentReceipt($buyer, $attach) {
        if (!$buyer->receive_email) {
            return;
        }

        $client = new Client(['base_uri' => env('APP_MAIL_URL')]);
        $response = $client->post('transaction/paymentReceipt', [
            'json' => [
                'to_email' => $buyer->email,
                'fullname' => Contact::find($buyer->contact_id)->full_name(),
                'attach'   => $attach
            ]
        ]);

        return $response;
    }

    static function sendCreditNote($user, $attach) {
        if (!$user->receive_email) {
            return;
        }
        $client = new Client(['base_uri' => env('APP_MAIL_URL')]);
        $response = $client->post('transaction/creditNote', [
            'json' => [
                'to_email' => $user->email,
                'fullname' => Contact::find($user->contact_id)->full_name(),
                'attach'   => $attach
            ]
        ]);

        return $response;
    }

    static function sendDebitNote($seller, $attach) {
        if (!$seller->receive_email) {
            return;
        }
        $client = new Client(['base_uri' => env('APP_MAIL_URL')]);
        $response = $client->post('transaction/debitNote', [
            'json' => [
                //clean
                'to_email' => $seller->email,
                'fullname' => Contact::find($seller->contact_id)->full_name(),
                'attach'   => $attach
            ]
        ]);

        return $response;
    }

    static function sendDispute($user, $attach) {
        if (!$user->receive_email) {
            return;
        }
        $client = new Client(['base_uri' => env('APP_MAIL_URL')]);
        $response = $client->post('transaction/creditNote', [
            'json' => [
                'to_email' => $user->email,
                'fullname' => Contact::find($user->contact_id)->full_name(),
                'attach'   => $attach
            ]
        ]);

        return $response;
    }
}