<?php
/**
 * Created by PhpStorm.
 * User: ntnam
 * Date: 11/14/16
 * Time: 15:04
 */

namespace App\Http\Controllers;

use App\Contact;
use App\Country;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\Libraries\ResponseMessage;
use App\User;
use App\Libraries\ErrorCodes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    /**
     * ContactController constructor.
     */
    public function __construct()
    {
    }

    public function getCurrentUserContact(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        if ($user->contact_id) {
            return $this->responseData(Contact::find($user->contact_id));
        }

        return $this->responseData([]);
    }

    public function updateCurrentUserContact(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        $v = Contact::validator($request->all());

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        //Getting current user's company_id
        if ($user->location_id) {
            $location = DB::table("location")->where("id", $user->location_id)->first();
            if ($location) {
                $company_id = $location->company_id;
            }
        }

        if (!$company_id) {
            Log::warn("User[$user->id] doesn't belong to any company.");
            return $this->responseError(ErrorCodes::ACCESS_DENIED);
        }

        $contact = NULL;

        if ($user->contact_id) {
            $contact = Contact::find($user->contact_id);
        } else {
            $contact = new Contact();
        }

        $contact->fill($request->all());
        $contact->status_code = Contact::STATUS_CODE_ACTIVE;
        $contact->salesperson_id = $user->id;
        $contact->updater_id = $user->id;

        $contact->save();

        if ($user->contact_id != $contact->id) {
            $user->contact_id = $contact->id;
            $user->save();
        }

        return $this->responseData($contact);
    }

    public function getById(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::CONTACT_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        //Getting current user's company_id
        if ($user->location_id) {
            $location = DB::table("location")->where("id", $user->location_id)->first();
            if ($location) {
                $company_id = $location->company_id;
            }
        }

        $contact = DB::table("contact")->where("id", $id)->first();

        if ($contact) {
            // make sure contact and user are in the same company


            $country = null;
            if ($contact->country) {
                $country = Country::find($contact->country);
            }
            $contact->country_name = $country ? $country->name: null;
            return $this->responseData($contact);
        }
        return $this->responseError(ErrorCodes::NOT_EXISTS);
    }

    public function create(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::CONTACT_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        //Getting current user's company_id
        if ($user->location_id) {
            $location = DB::table("location")->where("id", $user->location_id)->first();
            if ($location) {
                $company_id = $location->company_id;
            }
        }

        if (!$company_id) {
            Log::warn("User[$user->id] doesn't belong to any company.");
        }

        $v = Contact::validator($request->all());

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $contact = new Contact();
        $contact->fill($request->all());
        $contact->status_code = Contact::STATUS_CODE_ACTIVE;

        if (!$contact->salesperson_id) {
            $contact->salesperson_id = $user->id;
        }

//        if (array_key_exists('company_group_id', $user)) {
            $contact->company_group_id = $user->company_group_id;
//        }

        $contact->save();

        return $this->responseData($contact);
    }

    public function find(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_group_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::CONTACT_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'query' => 'max:50',
            'limit' => 'integer|min:1|max:100',
            'offset' => 'integer|min:0',
            'orderBy' => 'in:first_name,last_name,location,salesperson',
            'orderDesc' => 'boolean',
            'contactNotLinkUser' => 'boolean',
            'companyContactOnly' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

//        //Getting current user's company_group_id
//        if ($user->location_id) {
//            $location = DB::table("location")->where("id", $user->location_id)->first();
//            if ($location) {
//                $company_group_id = $location->company_group_id;
//            }
//        }
        $company_group_id = $user->company_group_id;

        if (!$company_group_id) {
            Log::warn("User[$user->id] doesn't belong to any company.");
            return $this->responseData([]);
        }

        $query = $request->json()->get('query');
        $limit = $request->json()->get('limit');
        $offset = $request->json()->get('offset');
        $orderBy = $request->json()->get('orderBy');
        $orderDesc = $request->json()->get('orderDesc');
        $contactNotLinkUser = $request->json()->get('contactNotLinkUser');
        $companyContactOnly = $request->json()->get('companyContactOnly');

        if (!$orderBy) {
            if (is_numeric($query)) {
                $orderBy = "mobile_phone";
            } else {
                $orderBy = "first_name, last_name";
            }
        }
        if($request->path() == 'contacts/admin/_find'){
            $company_group_id = NULL;
        }
        $contacts = Contact::search($company_group_id, $offset, $limit, $orderBy, $orderDesc, $query, $contactNotLinkUser, $companyContactOnly);
        \Log::info("check number of contacts return ".count($contacts));
        return $this->responseData($contacts);
    }

    public function findCount(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_group_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::CONTACT_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'query' => 'max:50'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

//        //Getting current user's company_group_id
//        if ($user->location_id) {
//            $location = DB::table("location")->where("id", $user->location_id)->first();
//            if ($location) {
//                $company_group_id = $location->company_group_id;
//            }
//        }
        $company_group_id = $user->company_group_id;
        if (!$company_group_id) {
            Log::warn("User[$user->id] doesn't belong to any company.");
            return $this->responseData([]);
        }

        $query = $request->json()->get('query');
        $contactNotLinkUser = $request->json()->get('contactNotLinkUser');
        $companyContactOnly = $request->json()->get('companyContactOnly');

        if($request->path() == 'contacts/admin/_findCount'){
            $company_group_id = NULL;
        }
        $result = Contact::searchCount($company_group_id, $query, $contactNotLinkUser, $companyContactOnly);

        return $this->responseData($result);
    }

    public function update(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::CONTACT_UPDATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){
            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        //Getting current user's company_id
        if ($user->location_id) {
            $location = DB::table("location")->where("id", $user->location_id)->first();
            if ($location) {
                $company_id = $location->company_id;
            }
        }


        $v = Contact::validator($request->all());

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $contact = Contact::find($id);

        if ($contact) {

            $contact->fill($request->all());

            $contact->updater_id = $user->id;

            $contact->save();

            return $this->responseData($contact);
        } else {
            return $this->responseError(ErrorCodes::NOT_EXISTS);
        }

    }
}