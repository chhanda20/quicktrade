<?php

namespace App\Http\Controllers;


use App\Libraries\ErrorCodes;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\Libraries\ResponseMessage;
use App\TestDrive;
use App\User;
use App\Vehicle;
use App\VehicleForTestDrive;
use DateTime;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TestDriveController extends Controller
{
    public function __construct()
    {
    }

    public function create(Request $request)
    {
        $token = $request->header("token");
        $vehicleId = $request->json()->get('vehicleId');
        $returnedDate = $request->json()->get('returnedDate');
        $customerId = $request->json()->get('customerId');
        $manual_vehicle = $request->json()->get('manual_vehicle') ? true : false;

        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

//        check permission

        $apiPermission = array(Permissions::TEST_DRIVE_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        try{
            $validationData = [
                'token' => $token,
                'vehicleId' => $vehicleId,
                'returnedDate' => $returnedDate,
                'customerId' => $customerId,
            ];
            $v = Validator::make($validationData, [
                'token' => 'required|alpha_num|exists:mysql.user,token',
                'returnedDate' => 'required',
                'customerId' => 'required|numeric|exists:mysql.contact,id',
            ], [
                'required' => ErrorCodes::REQUIRED,
                'alpha_num' => ErrorCodes::ALPHA_NUM,
                'exists' => ErrorCodes::NOT_EXISTS,
                'numeric' => ErrorCodes::NUMERIC,
            ]);
            if ($v->fails()) {
                $errorMessages = $v->errors()->getMessages();
                return $this->responseError(ErrorCodes::INVALID, $errorMessages);
            }

            //                'vehicleId' => 'required|numeric|exists:mysql.vehicle,id',
            if ($manual_vehicle) {
                if (!VehicleForTestDrive::find($vehicleId)) {
                    $this->responseError(ErrorCodes::NOT_EXISTS, '');
                }
            } else {
                if (!Vehicle::find($vehicleId)) {
                    $this->responseError(ErrorCodes::NOT_EXISTS, '');
                }
            }



            $responseMessage = new ResponseMessage();

            //check vehicle not in test drive progress
            if(TestDrive::findByVehicleId($vehicleId,TestDrive::STATUS_CODE_IN_PROGRESS))
                return $this->responseError(ErrorCodes::VEHICLE_NOT_AVAIL);

            $testDrive = new TestDrive();
            $testDrive->location_id = $user->location_id;
            $testDrive->vehicle_id = $vehicleId;
            $testDrive->return_date = $returnedDate;
            $testDrive->customer_id = $customerId;
            $testDrive->creator_id = $user->id;
            $testDrive->create_date = date('Y-m-d H:i:s');
            $testDrive->updater_id = $user->id;
            $testDrive->update_date = date('Y-m-d H:i:s');
            $testDrive->status = TestDrive::STATUS_CODE_IN_PROGRESS;
            $testDrive->manual_vehicle = $manual_vehicle;
            // calculate datetime different between current and return date
            $date_a = new DateTime('now');
            $date_b = new DateTime($returnedDate);

            $interval = date_diff($date_a,$date_b);
            $duration = $interval->format('%dd %H:%I:%S');
            $testDrive->duration = $duration;
            $createResult = $testDrive->save();
            if($createResult == 1){
                $statusCode = 200;
                $responseMessage->setData('success', true);
                $responseMessage->setData('testDriveId', $testDrive->id);
                $result = $responseMessage->getMessage();

                return response()->json($result, $statusCode);
            }else{
                \Log::info("Cannot create Test Drive");
                return $this->responseError(ErrorCodes::SYS_ERR);
            }
        }catch (\Exception $exception){
            \Log::error("Exception ".$exception);
            return $this->responseError(ErrorCodes::SYS_ERR);
        }


    }

    public function getListTestDrive(Request $request){
        $token = $request->header("token");

        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        //check permission
        $apiPermission = array(Permissions::TEST_DRIVE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
//todo :change from user location to user access data
        $listTestDrive = TestDrive::getListTestDriveByLocation($user->location_id);
        if(!$listTestDrive){
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }



        return $this->responseData($listTestDrive);
    }


    public function searchTestDriveCount(Request $request){
        $token = $request->header("token");
        $user = NULL;


        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        //check permission to view test drive
        $apiPermission = array(Permissions::TEST_DRIVE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $v = Validator::make($request->all(), [

            'query' => 'max:100',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $input = $request->json();
        $responseMessage = new ResponseMessage();
        $testDrive  = new TestDrive();
        $testDriveList = $testDrive->searchTestDrive($user->location_id, $input);

        $statusCode = 200;

        $responseMessage->setData('list_size', $testDriveList[0]);
//        $responseMessage->setData('testDriveList', $testDriveList['testDriveList']);

        $result = $responseMessage->getMessage();

//        $testDriveList1 = $testDriveList['testDriveList'];
//        if(!$testDriveList1)
//            $testDriveList1 = array();
        return response()->json($result, $statusCode);
    }

    public function searchTestDrive(Request $request){
        $token = $request->header("token");
        $user = NULL;


        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        //check permission to view test drive
        $apiPermission = array(Permissions::TEST_DRIVE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $v = Validator::make($request->all(), [
            'query' => 'max:100',
            'limit' => 'required|integer|min:1|max:100',
            'offset' => 'required|integer|min:0',
            'orderBy' => 'in:first_name,last_name,create_date,update_date,return_date',
            'orderDesc' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $input = $request->json();
        $responseMessage = new ResponseMessage();
        $testDrive  = new TestDrive();
        $testDriveList = $testDrive->searchTestDrive($user->location_id, $input);

        $statusCode = 200;

        $testDriveList1 = $testDriveList['testDriveList'];
        if(!$testDriveList1)
            $testDriveList1 = array();

        return response()->json($testDriveList1, $statusCode);

    }

    public function getById(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }
        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $r = TestDrive::find($id);

        if (!$r) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        return $this->responseData($r);
    }

    public function completeTestDrive(Request $request, $testDriveId){
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }
        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        //check permission to complete test drive
        $apiPermission = array(Permissions::TEST_DRIVE_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        try{

            $v = Validator::make($request->all(), [
                'vehicle_rating' => 'required|integer|min:0|max:10',
                'service_rating' => 'required|integer|min:0|max:10',

            ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

            if ($v->fails()) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
            }

            $comment = $request->json()->get('comment');
            $vehicleRating = $request->json()->get('vehicle_rating');
            $serviceRating = $request->json()->get('service_rating');
            $marketingOption = $request->json()->get('marketing_option');
            $marketingCode = $request->json()->get('marketing_code');

            $resultUpdate = TestDrive::completeTestDriveSession($testDriveId, $comment, $vehicleRating, $serviceRating, $marketingOption, $marketingCode);

            if($resultUpdate ==0) {
                return $this->responseError(ErrorCodes::NOT_FOUND);
            }

            $responseMessage = new ResponseMessage();
            $statusCode = 200;
            $responseMessage->setData('success', true);
            $result = $responseMessage->getMessage();

            return response()->json($result, $statusCode);
        } catch (\Exception $exception){
            return $this->responseError(ErrorCodes::SYS_ERR);
        }


    }


}
