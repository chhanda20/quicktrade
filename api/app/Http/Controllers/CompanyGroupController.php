<?php

namespace App\Http\Controllers;

use App\CompanyGroup;
use App\Libraries\ErrorCodes;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\Role;
use App\RolePermission;
use App\RoleHasMenu;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class CompanyGroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getAll(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::SYSTEM_ADMIN_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if (!$valuationResult) {

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $list = CompanyGroup::where('status_code', CompanyGroup::STATUS_ACTIVE)->get();
        return $this->responseData($list);
    }

    public function create(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::COMPANY_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if (!$valuationResult) {

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'name' => 'required|min:1|max:256',
            'status_code' => 'required|min:1|max:2',
            'description' => 'max:255'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $companyGroup = new CompanyGroup();

        $input = $request->toArray();
        $input = array_filter($input, 'strlen'); // remove null values

        $companyGroup->fill($input);

        $companyGroup->save();

        //get list default role
        $listDefaultRole = Role::getDefaultRole();
        //todo: create default role :


        foreach ($listDefaultRole as $defaultRole) {
            //insert default role for new company group
            $newRole = new Role();
            $newRole->name = $defaultRole->name;
            $newRole->code = $defaultRole->code;
            $newRole->company_group_id = $companyGroup->id;
            $newRole->save();
            //get permission list of default role
            $listRolePermission = RolePermission::getListPerMissionByRole($defaultRole->id);
            //set permission for role
            $data = array();
            $rolePermission = new RolePermission();
            foreach ($listRolePermission as $permission) {
                $temp = array('role_id' => $newRole->id, 'permission_id' => $permission->id);
                array_push($data, $temp);
            }
            $rolePermission->insert($data); // Query Builder

            $defaultMenus = RoleHasMenu::where('role_id', $defaultRole->id)->get();
            foreach ($defaultMenus as $defaultMenu) {
                $roleHasMenu                = new RoleHasMenu();
                $roleHasMenu->role_id       = $newRole->id;
                $roleHasMenu->menu_id       = $defaultMenu->menu_id;
                $roleHasMenu->creator_id    = $user->id;
                $roleHasMenu->save();
            }
        }


        return $this->responseData($companyGroup);
    }

    public function find(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::COMPANY_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if (!$valuationResult) {

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'query' => 'max:100',
            'status_code' => 'required|array',
            'limit' => 'required|integer|min:1|max:100',
            'offset' => 'required|integer|min:0',
            'orderBy' => 'in:name',
            'orderDesc' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $input = $request->json();
        $offset = $input->get('offset');
        $limit = $input->get('limit');
        $status_code = $input->get('status_code');

        $query = '';
        $orderBy = '';

        if ($input->has('query')) {
            $query = " AND name LIKE '%" . $input->get('query') . "%' ";
        }

        if ($input->has('orderBy')) {
            $orderBy = " ORDER BY " . $input->get('orderBy');

            if ($input->has('orderDesc')) {
                $orderBy .= ' DESC ';
            }
        }
        $stringList = implode(",", $status_code);

        $r = DB::select("
            SELECT * FROM company_group 
            WHERE status_code in ($stringList)
            $query
            $orderBy
            LIMIT $offset, $limit    
        ");
        return $this->responseData($r);
    }

    public function findCount(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::COMPANY_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if (!$valuationResult) {

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'query' => 'max:100',
            'status_code' => 'required|array'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $input = $request->json();
        $query = '';
        $status_code = $input->get('status_code');
        $stringList = implode(",", $status_code);

        if ($input->has('query')) {
            $query = "AND name LIKE '%" . $input->get('query') . "%' ";
        }

        $r = DB::select("
            SELECT COUNT(*) AS count FROM company_group 
            WHERE status_code in ($stringList)
            $query
        ");
        return $this->responseData($r[0]);
    }

    public function update(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::COMPANY_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if (!$valuationResult) {

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'name' => 'min:1|max:256',
            'status_code' => 'min:1|max:2',
            'description' => 'max:255'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $companyGroup = CompanyGroup::find($id);

        if (!$companyGroup) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        $input = $request->toArray();
        $input = array_filter($input, 'strlen'); // remove null values

        $companyGroup->fill($input);
        $companyGroup->save();

        return $this->responseData($companyGroup);
    }

    public function getOne(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::COMPANY_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if (!$valuationResult) {

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $companyGroup = CompanyGroup::find($id);

        if (!$companyGroup) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        return $this->responseData($companyGroup);
    }

    public function getRoleByCompanyGroup(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::ROLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if (!$valuationResult) {

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $resultDB = Role::getRoleByCompanyGroupId($id);

        return $this->responseData($resultDB);
    }
}
