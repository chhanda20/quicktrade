<?php

namespace App\Http\Controllers;

use App\GlassTransmissionType;
use App\Libraries\ErrorCodes;
use App\GlassModel;
use App\User;
use App\Valuation;
use App\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class GlassTransmissionTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getTransmissionType(Request $request)
    {
        $token = $request->header('token');
        $validationData = ['token' => $token];

        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token'
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $list = GlassTransmissionType::all()->pluck('description')->toArray();
        $list = array_map('trim', $list);

        return $this->responseData($list);
    }

}
