<?php

namespace App\Http\Controllers;

use App\Libraries\ErrorCodes;
use App\Libraries\Permissions;
use App\TypeCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TypeCodeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function find(Request $request)
    {
        $v = Validator::make($request->all(), [
            'type' => 'max:100'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $type = $request->json()->get("type");

        $result = NULL;

        if($type) {
            $result = TypeCode::where('type', $type)->get();
        } else {
            $result = TypeCode::get();
        }

        return $this->responseData($result);
    }
}
