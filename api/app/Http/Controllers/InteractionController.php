<?php

namespace App\Http\Controllers;


use App\Contact;
use App\Interaction;
use App\Libraries\ErrorCodes;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class InteractionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    /**
     * @apiDefine InvalidInput
     * @apiError  invalid_input .
     */
    /**
     * create : creates new interaction
     * @param Request $request
     * @param $contact_id
     * @return \Illuminate\Http\JsonResponse
     */
/**
 * @api {post} /contacts/{contact_id}/interaction  Add New interaction
 * @apiHeader {json} content type and token of Logged in users .
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Content-type": "application/json"
 *       "token" : "BTgMAeJ5UKp4t8dr9jw3bjZhWbMVW33m2Klm6KTe"
 *     }
 * @apiName New Interaction
 * @apiGroup Customer
 *
 * @apiParam {String} input_date  interaction date
 * @apiParam {Integer} type interaction type.
 * @apiParam {text} note comments.
 *
 ** @apiSuccess {Object}  interaction       customers's recently added interaction information.
 * @apiSuccessExample {json} Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *  "input_date": "2019-10-11 12:56:25",
 *  "type": 1,
 *  "note": "test note",
 *  "creator_id": 2,
 *  "contact_id": "295",
 *  "update_date": "2019-10-09 15:44:28",
 *  "create_date": "2019-10-09 15:44:28",
 *  "id": 77
 * }
 * @apiError InvalidInput
 * @apiErrorExample {json} Error-Response:
 * {
 * "error": {
 * "code": "invalid_input",
 * "detail": {
 * "input_date": [
 * "required"
 * ],
 * "type": [
 * "required"
 * ],
 * "note": [
 * "required"
 * ]
 * }
 * }
 * }
 **/
    public function create(Request $request, $contact_id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'input_date' => 'required|string',
            'type' => 'required|integer',
            'note' => 'required|string'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $contact = Contact::find($contact_id);
        if(!$contact) {
            return $this->responseError(ErrorCodes::NOT_FOUND, 'Contact not found');
        }

        $input = $request->toArray();
        $interaction =new Interaction();
        $interaction->fill($input);
        $interaction->creator_id = $user->id;
        $interaction->contact_id = $contact_id;
        $interaction->save();
        return $this->responseData($interaction);
    }
    /**
     * @api {post} contacts/{contact_id}/interaction/_find  find interaction
     * @apiHeader {json} content type and token of Logged in users .
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "Content-type": "application/json"
     *       "token" : "BTgMAeJ5UKp4t8dr9jw3bjZhWbMVW33m2Klm6KTe"
     *     }
     * @apiName Search Interaction
     * @apiGroup Customer
     *
     * @apiParam {integer} offset  interaction date
     * @apiParam {Integer} limit interaction type.
     *
     ** @apiSuccess {Object}  interaction       customers's recently added interaction information.
     * @apiSuccessExample {json} Success-Response:
     *  HTTP/1.1 200 OK
     *  {
     *  "input_date": "2019-10-11 12:56:25",
     *  "type": 1,
     *  "note": "test note",
     *  "creator_id": 2,
     *  "contact_id": "295",
     *  "update_date": "2019-10-09 15:44:28",
     *  "create_date": "2019-10-09 15:44:28",
     *  "id": 77
     * }
     * @apiError InvalidInput
     * @apiErrorExample {json} Error-Response:
     * {
     * "error": {
     * "code": "invalid_input",
     * "detail": {
     * "input_date": [
     * "required"
     * ],
     * "type": [
     * "required"
     * ],
     * "note": [
     * "required"
     * ]
     * }
     * }
     * }
     **/
    public function find(Request $request, $contact_id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'offset' => 'required|integer',
            'limit' => 'required|integer',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $json = $request->json();

        $offset = $json->get('offset');
        $limit = $json->get('limit');

        $contact = Contact::find($contact_id);
        if(!$contact) {
            return $this->responseError(ErrorCodes::NOT_FOUND, 'Contact not found');
        }

        $r = DB::select("SELECT * FROM interaction WHERE contact_id=:contactId ORDER BY input_date DESC LIMIT $offset, $limit", ['contactId' => $contact_id]);
        return $this->responseData($r);
    }

    public function findCount(Request $request, $contact_id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $contact = Contact::find($contact_id);
        if(!$contact) {
            return $this->responseError(ErrorCodes::NOT_FOUND, 'Contact not found');
        }
        $r = DB::select("SELECT COUNT(*) AS count FROM interaction WHERE contact_id=:contactId ", ['contactId' => $contact_id]);
        return $this->responseData($r[0]);
    }

    public function getOne(Request $request, $contact_id, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $interaction = Interaction::where('contact_id', $contact_id)->where('id', $id)->first();
        if (!$interaction) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        return $this->responseData($interaction);
    }

    public function delete(Request $request, $contact_id, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $interaction = Interaction::where('contact_id', $contact_id)->where('id', $id)->first();
        if (!$interaction) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        $interaction->delete();
        return $this->responseData($interaction);
    }

    public function update(Request $request, $contact_id, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'input_date' => 'required|string',
            'type' => 'required|integer',
            'note' => 'required|string'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $interaction = Interaction::where('contact_id', $contact_id)->where('id', $id)->first();
        if (!$interaction) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        $input = $request->toArray();
        $interaction->fill($input);
        $interaction->save();
        return $this->responseData($interaction);
    }

}
