<?php

namespace App\Http\Controllers;

ini_set('max_execution_time', 180); //3 minutes


use App\AppProperty;
use App\Libraries\ErrorCodes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use phpDocumentor\Reflection\Types\Object_;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class GlassDataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    public function fetch($table, $db)
    {
        $columns = Schema::getColumnListing($table);
        $a = "(" . $columns[0];
        $b = "(:" . $columns[0];
        for ($i = 1; $i < count($columns); $i++) {
            $a .= "," . $columns[$i];
            $b .= ",:" . $columns[$i];
        }
        $a .= ") ";
        $b .= ") ";

        $count = DB::table($table)->count();
        $i = 0;

        Log::info("Start fetching $count rows from $table");
        while ($i < $count) {
            $r = DB::table($table)->offset($i)->limit(100)->get();
            $i += count($r);

            Log::info("From $table - $count, fetched $i");

            foreach ($r AS $row) {
//                LOG::info("INSERT INTO ".$table." ".$a." VALUES ".$b);
                $stm = $db->prepare("INSERT INTO " . $table . " " . $a . " VALUES " . $b);

                foreach ($columns as $col) {
//                    LOG::info($row->{$col});
                    $stm->bindValue(":" . $col, $row->{$col});
                }
                $stm->execute();
            }
        }
    }

    public function download(Request $request, $file_name)
    {
        $tmp = '../glass/';
        $filePath = $tmp.$file_name . ".zip";

        clearstatcache(); // file_exists() result are cached - we must remove cache first;

        if (file_exists($filePath)) {
            return response()->download($filePath);
        }
        Log::warn("FILE $filePath not found");

        return $this->responseError(ErrorCodes::NOT_FOUND);
    }

    public function info()
    {
        $result = [];
        $r = AppProperty::find(AppProperty::GLASS_DATA_EXPORTED_FILE);
        if($r) {
            $result['fileName'] = $r->value;
        }
        $r = AppProperty::find(AppProperty::GLASS_DATA_EXPORTED_TIME);
        if($r) {
            $result['exportedTime'] = $r->value;
        }
        return $this->responseData($result);
    }

    public function export()
    {
        $tmp = '../glass/';
        $fileName = $this->generateRandomString();
        $db_host = env('DB_HOST');
        $db_port = env('DB_PORT');
        $db_db = env('DB_DATABASE');
        $db_user = env('DB_USERNAME');
        $db_pass = env('DB_PASSWORD');

        $pwd = exec('pwd');
        $user_loggin = exec('id -un');

        $script = "sqlite-converter.sh 2>&1 -h $db_host -u $db_user -p$db_pass $db_db --port=$db_port glass_make glass_body_type glass_engine_type glass_km_adjustment glass_km_category glass_model glass_model_option glass_option glass_transmission_type | sqlite3 $tmp$fileName.db && zip -j $tmp$fileName.zip $tmp$fileName.db";
        $output = shell_exec($script);

        $r = AppProperty::find(AppProperty::GLASS_DATA_EXPORTED_FILE);
        if (!$r) {
            $r = new AppProperty();
            $r->id = AppProperty::GLASS_DATA_EXPORTED_FILE;
        }
        $r->value = $fileName;
        $r->save();

        $r = AppProperty::find(AppProperty::GLASS_DATA_EXPORTED_TIME);
        if (!$r) {
            $r = new AppProperty();
            $r->id = AppProperty::GLASS_DATA_EXPORTED_TIME;
        }
        $r->value = (new \DateTime())->format('Y-m-d H:i:s');
        $r->save();

        return $this->responseData(['result' => $output, 'pwd' => $pwd, 'script' => $script, 'user_loggin' => $user_loggin]);
    }

    public function export_old()
    {
        $fileName = $this->generateRandomString();
        $old_path = getcwd();


        // TODO: try both exec and shell_exec but not working. System not show error neither success
        $test = exec("sudo ./root/tmp/mysql2sqlite.sh -h 192.168.10.202 -u moto_dev -pMMS123456 moto_dev glass_make glass_body_type glass_engine_type glass_km_adjustment glass_km_category glass_model glass_model_option glass_option glass_transmission_type | sqlite3 /root/tmp/sqlite3123.db",$stdoutbuf, $returnbuf);
        Log::info("test123 ".$test);
        $filePath = "/tmp/" . $fileName . ".db";


        if (file_exists($filePath)) {
            unlink($filePath);
        }

        echo "/tmp/sqlite3" . $fileName.".db";
        //TODO: Try to use laravel process to run shell script. not work 
        $test2 = 'sudo ./root/tmp/mysql2sqlite.sh -h 192.168.10.202 -u moto_dev -pMMS123456 moto_dev glass_make glass_body_type glass_engine_type glass_km_adjustment glass_km_category glass_model glass_model_option glass_option glass_transmission_type | sqlite3 /root/tmp/sqlite3123.db';
        $process2 = new Process($test2);
        $process2->run();

        if (!$process2->isSuccessful()) {
            throw new ProcessFailedException($process2);
        }

        Log::info("result process ". $process2->getOutput());

        $db = new MyDB($filePath);

        $sql = <<<EOF
    
            CREATE TABLE glass_body_type
            (
                glass_body_type_id VARCHAR(3) NOT NULL,
                description VARCHAR(30) NOT NULL,
                PRIMARY KEY (glass_body_type_id)
            );
            
            CREATE TABLE glass_engine_type
            (
                glass_engine_type_id VARCHAR(2) NOT NULL,
                description VARCHAR(30) NOT NULL,
                PRIMARY KEY (glass_engine_type_id)
            );
            
            CREATE TABLE glass_transmission_type
            (
                glass_transmission_type_id VARCHAR(2) NOT NULL,
                description VARCHAR(30) NOT NULL,
                PRIMARY KEY (glass_transmission_type_id)
            );
            
            CREATE TABLE glass_make
            (
                glass_make_id VARCHAR(3) NOT NULL,
                description VARCHAR(30) NOT NULL,
                PRIMARY KEY (glass_make_id)
            );
            
            CREATE TABLE glass_km_category
            (
                glass_km_category_id INT(11) NOT NULL,
                glass_code VARCHAR(20) NOT NULL,
                km_category VARCHAR(11) NOT NULL,
                average_kms INT(11) NOT NULL,
                nvic VARCHAR(6) NOT NULL,
                PRIMARY KEY (glass_km_category_id)
            );
            
            CREATE TABLE glass_km_adjustment
            (
                glass_km_adjustment_id INT(11) NOT NULL,
                km_category VARCHAR(11) NULL,
                over_under VARCHAR(10) NULL,
                up_to_kms INT(11) NULL,
                adjust_amount DECIMAL(12,2) NULL,
                PRIMARY KEY (glass_km_adjustment_id)
            );
            
            CREATE TABLE glass_option
            (
                glass_option_id VARCHAR(4) NOT NULL,
                name VARCHAR(50) NOT NULL,
                PRIMARY KEY (glass_option_id)
            );
            
            CREATE TABLE glass_model
            (
                glass_model_id INT(11) NOT NULL,
                glass_code VARCHAR(20) NOT NULL,
                make VARCHAR(3) NOT NULL,
                family VARCHAR(24) NOT NULL,
                variant VARCHAR(30) NULL,
                series VARCHAR(22) NULL,
                style VARCHAR(22) NOT NULL,
                release_month INT(11) NOT NULL,
                release_year INT(11) NOT NULL,
                engine VARCHAR(24) NOT NULL,
                engine_capacity INT(11) NOT NULL,
                engine_size VARCHAR(5) NOT NULL,
                transmission VARCHAR(20) NOT NULL,
                cylinder VARCHAR(4) NOT NULL,
                recommended_retail_price DECIMAL(12,2) NULL,
                below_recommended_retail_price DECIMAL(12,2) NULL,
                wheel_size VARCHAR(5) NULL,
                above_recommended_retail_price DECIMAL(12,2) NULL,
                nvic VARCHAR(6) NOT NULL,
                body_type_id VARCHAR(3) NOT NULL,
                engine_type_id VARCHAR(3) NOT NULL,
                transmission_type_id VARCHAR(3) NOT NULL,
                PRIMARY KEY (glass_model_id)
            );
            
            CREATE TABLE glass_model_option
            (
                glass_model_option_id INT(11) NOT NULL,
                glass_code VARCHAR(20) NOT NULL,
                rec_type VARCHAR(9) NOT NULL,
                `option` VARCHAR(150) NOT NULL,
                nvic VARCHAR(6) NOT NULL,
                PRIMARY KEY (glass_model_option_id)
            );
EOF;

        $ret = $db->exec($sql);
        if (!$ret) {
            echo $db->lastErrorMsg();
        } else {
            echo $db->changes(), "Tables successfully\n";
        }

//        $this->fetch('glass_body_type', $db);
//        $this->fetch('glass_engine_type', $db);
//        $this->fetch('glass_transmission_type', $db);
//        $this->fetch('glass_make', $db);
//        $this->fetch('glass_km_category', $db);
//        $this->fetch('glass_km_adjustment', $db);
//
//        $this->fetch('glass_option', $db);
//
//        $this->fetch('glass_model', $db);
//
//        $this->fetch('glass_model_option', $db);


        $db->close();

//        $zip = new \ZipArchive();
//        if ($zip->open('/tmp/'. $fileName .'.zip', \ZipArchive::CREATE) === TRUE) {
//            $zip->addFile($filePath, $fileName.'.db');
//            $zip->close();
//            Log::info("Zipped file " . ('/tmp/'. $fileName .'.zip') );
//        } else {
//            Log::error("Can't open " . ('/tmp/'. $fileName .'.zip'));
//        }
//        Log::info("check time 1 ".date('m/d/Y h:i:s a', time()));

        $r = AppProperty::find(AppProperty::GLASS_DATA_EXPORTED_FILE);
        if (!$r) {
            $r = new AppProperty();
            $r->id = AppProperty::GLASS_DATA_EXPORTED_FILE;
        }
        $r->value = $fileName;
        $r->save();

        $r = AppProperty::find(AppProperty::GLASS_DATA_EXPORTED_TIME);
        if (!$r) {
            $r = new AppProperty();
            $r->id = AppProperty::GLASS_DATA_EXPORTED_TIME;
        }
        $r->value = (new \DateTime())->format('Y-m-d H:i:s');
        $r->save();

        $this->responseData($r);
    }
}


/*class MyDB extends \SQLite3
{
    function __construct($path)
    {
        $this->open($path);
    }

    function put($data)
    {

    }
}*/