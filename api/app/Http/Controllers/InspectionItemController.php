<?php

namespace App\Http\Controllers;

use App\InspectionItem;
use App\InspectionItemImage;
use App\Libraries\ErrorCodes;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\TypeCode;
use App\User;
use App\ValuationInspectionItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InspectionItemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getAll($sectionType) {
        $data = [];
        if ($sectionType == 'ALL') {
            $vehicleSections = ['EXTERIOR_SECTION', 'INTERIOR_SECTION', 'MECHANICAL_SECTION'];
            $sections = TypeCode::whereIn('type',$vehicleSections)->get();
        } else {
            $sections = TypeCode::where('type', $sectionType)->get();
        }

        foreach ($sections as $section) {
            $row = [
                'section_id' => $section->code,
                'section_name' => $section->value,
                'description' => $section->description,
            ];
            $items = InspectionItem::where('section_type', $section->code)->get()->toArray();
            $row['items'] = $items;
            $data[] = $row;
        }
        return $this->responseData($data);
    }

    public function getItem(Request $request, $valuationId, $inspectionItemId)
    {
        $token = $request->header('token');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token'       => $token,
            'valuationId' => $valuationId,
            'inspectionItemId' => $inspectionItemId,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'valuationId' => 'numeric|required|exists:mysql.valuation,id',
            'inspectionItemId' => 'numeric|required|exists:mysql.inspection_item,id'
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $inspectionItem = ValuationInspectionItem::where('valuation_id', $valuationId)->where('inspection_item_id', $inspectionItemId)->first();

        if (!$inspectionItem) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        $data['repair_cost'] = $inspectionItem->repair_cost;
        $data['note'] = $inspectionItem->note;

        return $this->responseData($data);
    }
}
