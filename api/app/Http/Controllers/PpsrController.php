<?php

namespace App\Http\Controllers;

use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\Libraries\ResponseMessage;
use App\User;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Libraries\ErrorCodes;
use GuzzleHttp\Client;

class PpsrController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function sendEmail(Request $request)
    {
        $responseMessage = new ResponseMessage();
        $token = $request->header('token');

        $validationData = [
            'token' => $token,
        ];
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        if (!$user->receive_email) {
            return;
        }

        $apiPermission = array(Permissions::PPSR_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }
        $userModel = new User();
        $user = $userModel->getContactFieldsByToken($token, ['c.first_name', 'c.last_name', 'u.email']);
        $fullname = $user->first_name . ' ' . $user->last_name;
        $email = $user->email;
        try {
            $client = new Client(['base_uri' => env('APP_MAIL_URL')]);
            $response = $client->post('ppsr/sendEmail', [
                'json' => [
                    'to_email' => $email,
                    'fullname' => $fullname,
                ]
            ]);
            $statusCode = 200;
            $responseMessage->setData('success', TRUE);
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $bodyResponse = json_decode($response->getBody()->getContents(), true);
            $statusCode = 400;
            if (isset($bodyResponse['error']['code'])) {
                $responseMessage->setErrorCode($bodyResponse['error']['code']);
                if (isset($bodyResponse['error']['detail'])) {
                    $responseMessage->setErrorDetail($bodyResponse['error']['detail']);
                }
            } else {
                $responseMessage->setErrorCode(ErrorCodes::INVALID);
            }
        }

        return $this->responseData(['success' => TRUE]);
    }
}
