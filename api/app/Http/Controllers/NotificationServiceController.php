<?php
/**
 * Created by PhpStorm.
 * User: huyda
 * Date: 8/7/17
 * Time: 1:12 PM
 */


namespace App\Http\Controllers;

use App\Contact;
use App\Libraries\Event;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\Libraries\Helper;
use Exception;

class NotificationServiceController {

    static function send($user, $event, $sourceId, $data) {
        if (User::enableForEvent($user->id, $event, 'notification') !== false) {
            \LOG::debug("send notification/email to user $user->email event $event | notification");
            try {
                $header = Event::notificationHeader($event);
                Helper::notifyChannel($event, $user->id, $header['title'], $header['message'], $data);
            } catch (Exception $error) {
                \LOG::debug("--> 999 Can't send notification: [$error]");
            }
        } else {
            \LOG::debug("send notification/email to user $user->email event $event | disable notification");
        }
    }
}