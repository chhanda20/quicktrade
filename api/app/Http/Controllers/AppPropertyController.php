<?php

namespace App\Http\Controllers;

use App\AppProperty;
use App\AuctionSession;
use App\CompanySetting;
use App\Libraries\ErrorCodes;

use App\Libraries\Helper;
use App\Libraries\Permissions;

use App\Libraries\ResponseMessage;
use App\Location;
use App\RegionCommon;
use App\User;

use App\Valuation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AppPropertyController extends Controller
{

    public function getServerProperty(Request $request) {
        return $this->responseData([
            'date' => date('Y-m-d H:i:s')
        ]);
    }
    public function updateExternalOffer(Request $request)
    {
        $token = $request->header('token');
        $externalOfferTimeout = $request->json()->get('external_offer_timeout');
        $externalOfferExpiry = $request->json()->get('external_offer_expiry');
        $externalOfferAcceptanceTimeout = $request->json()->get('external_offer_acceptance_timeout');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::SYSTEM_CONFIG);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'externalOfferTimeout' => $externalOfferTimeout,
            'externalOfferExpiry' => $externalOfferExpiry,
            'externalOfferAcceptanceTimeout' => $externalOfferAcceptanceTimeout,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'externalOfferTimeout' => 'required|numeric',
            'externalOfferExpiry' => 'required|numeric',
            'externalOfferAcceptanceTimeout' => 'required|numeric',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        DB::table('app_property')->where('id', AppProperty::EXTERNAL_OFFER_TIMEOUT)->update(['value' => $externalOfferTimeout]);
        DB::table('app_property')->where('id', AppProperty::EXTERNAL_OFFER_EXPIRY)->update(['value' => $externalOfferExpiry]);
        DB::table('app_property')->where('id', AppProperty::EXTERNAL_OFFER_ACCEPTANCE_TIMEOUT)->update(['value' => $externalOfferAcceptanceTimeout]);

        return $this->responseData(['success' => TRUE]);
    }

    public function getExternalOffer(Request $request)
    {
        $token = $request->header('token');
        $validationData = ['token' => $token];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token'
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $appPropertyModel = new AppProperty();
        $externalOfferSettings = [
            AppProperty::EXTERNAL_OFFER_TIMEOUT,
            AppProperty::EXTERNAL_OFFER_ACCEPTANCE_TIMEOUT,
            AppProperty::EXTERNAL_OFFER_EXPIRY
        ];
        $externalOffers = $appPropertyModel->getAppProperty($externalOfferSettings);
        $result = [];
        foreach ($externalOffers as $extOffer) {
            $result[$extOffer->id] = intval($extOffer->value);
        }

        return $this->responseData($result);
    }

    public function getSetting(Request $request)
    {
        $token = $request->header('token');
        $setting = $request->json()->get('setting');
        $validationData = [
            'token' => $token,
            'setting' => $setting
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'setting' => 'required|array',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'array' => ErrorCodes::ARRAY_TYPE,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $result = AppProperty::whereIn('id', $setting)->get();
        return $this->responseData($result);
    }

    public function updateValuationTimeout(Request $request)
    {
        $token = $request->header('token');
        $firstNotificationTimeout = $request->json()->get('first_notification_timeout');
        $secondNotificationTimeout = $request->json()->get('second_notification_timeout');
        $valuationTimeout = $request->json()->get('valuation_timeout');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::SYSTEM_CONFIG);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'firstNotificationTimeout' => $firstNotificationTimeout,
            'secondNotificationTimeout' => $secondNotificationTimeout,
            'valuationTimeout' => $valuationTimeout,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'firstNotificationTimeout' => 'required|numeric',
            'secondNotificationTimeout' => 'required|numeric',
            'valuationTimeout' => 'required|numeric',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        DB::table('app_property')->where('id', AppProperty::FIRST_NOTIFICATION_TIMEOUT)->update(['value' => $firstNotificationTimeout]);
        DB::table('app_property')->where('id', AppProperty::SECOND_NOTIFICATION_TIMEOUT)->update(['value' => $secondNotificationTimeout]);
        DB::table('app_property')->where('id', AppProperty::VALUATION_TIMEOUT)->update(['value' => $valuationTimeout]);

        return $this->responseData(['success' => TRUE]);
    }

    public function getValuationTimeout(Request $request)
    {
        $token = $request->header('token');
        $validationData = ['token' => $token];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token'
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $appPropertyModel = new AppProperty();
        $valuationSettings = [
            AppProperty::FIRST_NOTIFICATION_TIMEOUT,
            AppProperty::SECOND_NOTIFICATION_TIMEOUT,
            AppProperty::VALUATION_TIMEOUT
        ];
        $externalOffers = $appPropertyModel->getAppProperty($valuationSettings);
        $result = [];
        foreach ($externalOffers as $extOffer) {
            $result[$extOffer->id] = intval($extOffer->value);
        }

        return $this->responseData($result);
    }

    public function updateOptionSetting(Request $request)
    {
        $token = $request->header('token');
        $headValuerApproval = $request->json()->get('head_valuer_approval');
        $receiveExternalOffer = $request->json()->get('receive_external_offer');
        $testDriveTerms = $request->json()->get('test_drive_terms');

        $user = NULL;
        try {
            $user = $this->currentUser($request, [Permissions::DEALER_SETTINGS_EDIT]);
        } catch (Exception $e) {
            return $this->responseError($e->getMessage());
        }

        $validationData = [
            'token' => $token,
            'headValuerApproval' => $headValuerApproval,
            'receiveExternalOffer' => $receiveExternalOffer,
            'testDriveTerms' => $testDriveTerms

        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'headValuerApproval' => 'required|boolean',
            'receiveExternalOffer' => 'required|boolean',
            'testDriveTerms' => 'required'

        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'boolean' => ErrorCodes::BOOLEAN,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'numeric' => ErrorCodes::NUMERIC
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

//        DB::table('app_property')->where('id', AppProperty::HEAD_VALUER_APPROVAL)->update(['value' => $headValuerApproval]);
//        DB::table('app_property')->where('id', AppProperty::RECEIVE_EXTERNAL_OFFER)->update(['value' => $receiveExternalOffer]);


        if(!$headValuerApproval) {
            $headValuerApproval = CompanySetting::STATUS_CODE_INACTIVE;
            Valuation::where('status', Valuation::STATUS_PENDING)->update(['status' => Valuation::STATUS_COMPLETE]);;
        }

        if(!$receiveExternalOffer)
            $receiveExternalOffer = CompanySetting::STATUS_CODE_INACTIVE;
        $headValuerApprovalKey = CompanySetting::HEAD_VALUER_APPROVAL;
        $receiveExternalOfferKey = CompanySetting::RECEIVE_EXTERNAL_OFFER;
        $testDriveTermsKey = CompanySetting::TEST_DRIVE_TERMS;
        $locationInfo = Location::find($user->location_id);
        $currentDate = date('Y-m-d H:i:s');
        //update to company_setting table: headValuerApproval,receiveExternalOffer,Test Drive term and condition
        $updateCompanySettingSQL = "UPDATE company_setting
                                        SET 
                                        `value` = CASE `key`
                                            WHEN '$headValuerApprovalKey' THEN '$headValuerApproval'
                                            WHEN '$receiveExternalOfferKey' THEN '$receiveExternalOffer'
                                            WHEN '$testDriveTermsKey' THEN '$testDriveTerms'
                                          
                                        END,
                                            company_setting.update_date = '$currentDate',
                                            company_setting.updater_id = $user->id
                                    WHERE company_id = $locationInfo->company_id";

        DB::select($updateCompanySettingSQL);

        return $this->responseData(['success' => TRUE]);
    }

    public function getOptionSetting(Request $request)
    {
        $token = $request->header('token');

        $validationData = ['token' => $token];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token'
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }
        // get value from company setting
        $locationInfo = Location::find($user->location_id);
        $companySettings = CompanySetting::getCompanySetting($locationInfo->company_id);
        if(!$companySettings){
            CompanySetting::createDefaultSetting($locationInfo->company_id,$user->id);
            $companySettings = CompanySetting::getCompanySetting($locationInfo->company_id);
        }
        $response = new ResponseMessage();
        foreach ($companySettings as $companySetting){
            $value = $companySetting->value;

            if($value == '1')
                $value = true;
            if($value == '0'){$value = false;}
            $response->setData($companySetting->key, $value);
        }
        $result = $response->getMessage();
        return $this->responseData($result);
    }

    public function changeAuctionSettings(Request $request){
        $token = $request->header('token');
        $defaultDuration = $request->json()->get('auction_default_duration');
        $priceIncrement = $request->json()->get('auction_price_increment');

        $validationData = ['token' => $token,
                            'defaultDuration' => $defaultDuration,
                            'priceIncrement' => $priceIncrement];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'defaultDuration' => 'required',
            'priceIncrement' => 'required'
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }
        // CHECKING PERMISSION
        $apiPermission = array(Permissions::AUCTION_SETTING_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        //fixme: please find another way to update with one query.

        $session = AuctionSession::getActiveSessions();
        if (sizeof($session) > 0) {
            return $this->responseError(ErrorCodes::INVALID, $session, 'There are an active auction session');
        }

        DB::table('app_property')->where('id', AppProperty::AUCTION_PRICE_INCREMENT)->update(['value' => $priceIncrement]);
        DB::table('app_property')->where('id', AppProperty::AUCTION_DEFAULT_DURATION)->update(['value' => $defaultDuration]);
        return $this->responseData(['success' => TRUE]);
    }

    public function getAuctionSettings(Request $request){
        $token = $request->header('token');
        $validationData = ['token' => $token];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token'
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $appPropertyModel = new AppProperty();
        $auctionSettingsKey = [
            AppProperty::AUCTION_DEFAULT_DURATION,
            AppProperty::AUCTION_PRICE_INCREMENT
        ];
        $auctionSettings = $appPropertyModel->getAppProperty($auctionSettingsKey);
        $result = [];
        foreach ($auctionSettings as $auctionSetting) {
            $result[$auctionSetting->id] = intval($auctionSetting->value);
        }
        $rejectReason = DB::select("SELECT code,value from type_code WHERE type = 'AUCTION_REJECT_REASON' and language_code = 'en'");
        $result['rejectReason'] = $rejectReason;
        return $this->responseData($result);
    }
    //14Jul2017 - NNDHuy: API to update moto fee for buyer and seller. Update shipping price base on from - to region
    public function changeTransactionFees(Request $request){
        $token = $request->header('token');
        $buyer_fee = $request->json()->get('buyer_fee');
        $seller_fee = $request->json()->get('seller_fee');
        $transportPrices = $request->json()->get('transport_prices');

        $validationData = ['token' => $token,
            'buyer_fee' => $buyer_fee,
            'seller_fee' => $seller_fee,
            'transport_prices' => $transportPrices];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'buyer_fee' => 'required',
            'seller_fee' => 'required',
            'transport_prices' => 'array',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'array' => ErrorCodes::ARRAY_TYPE
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }
        // CHECKING PERMISSION
        $apiPermission = array(Permissions::AUCTION_SETTING_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        //fixme: please find another way to update with one query.

        $session = AuctionSession::getActiveSessions();
        if (sizeof($session) > 0) {
            return $this->responseError(ErrorCodes::INVALID, $session, 'There are an active auction session');
        }

        DB::table('app_property')->where('id', AppProperty::SELLER_MOTO_FEE)->update(['value' => $seller_fee]);
        DB::table('app_property')->where('id', AppProperty::BUYER_MOTO_FEE)->update(['value' => $buyer_fee]);

        //update transport price
        RegionCommon::updateTransportPrice($transportPrices);

        return $this->responseData(['success' => TRUE]);
    }

    public function getTransactionFees(Request $request){
        $token = $request->header('token');
        $validationData = ['token' => $token];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token'
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $appPropertyModel = new AppProperty();
        $externalOfferSettings = [
            AppProperty::BUYER_MOTO_FEE,
            AppProperty::SELLER_MOTO_FEE
        ];
        $externalOffers = $appPropertyModel->getAppProperty($externalOfferSettings);
        $result = [];
        foreach ($externalOffers as $extOffer) {
            $result[$extOffer->id] = intval($extOffer->value);
        }
        $listTransportPrices = DB::select("select * from transport_price");
        $result["transport_price"] = $listTransportPrices;
        return $this->responseData($result);
    }

    public function updateSpeedySetting(Request $request) {
        $user = User::where('token', $request->header("token"))->first();
        if(!$user)
            return $this->responseError(ErrorCodes::INVALID_TOKEN);

        // check permission
        $apiPermission = array(Permissions::SYSTEM_CONFIG);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if (!$valuationResult) {
            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $v = Validator::make($request->all(), [
            'speedy_seller_avg_time' => 'integer',
            'speedy_buyer_avg_time' => 'integer',
            'rating_expired_time' => 'integer'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $speedySellerAvgTime = $request->json()->get("speedy_seller_avg_time");
        $speedyBuyerAvgTime = $request->json()->get("speedy_buyer_avg_time");
        $expiredTime = $request->json()->get('rating_expired_time');

        $result = NULL;

        if ($speedySellerAvgTime) {
            DB::table('app_property')->where('id', 'speedy_seller_avg_time')->update (['value' => $speedySellerAvgTime]);
            $result['speedy_seller_avg_time'] = (int)AppProperty::where('id', 'speedy_seller_avg_time')->first()->value;
        }

        if ($speedyBuyerAvgTime) {
            DB::table('app_property')->where('id', 'speedy_buyer_avg_time')->update (['value' => $speedyBuyerAvgTime]);
            $result['speedy_buyer_avg_time'] = (int)AppProperty::where('id', 'speedy_buyer_avg_time')->first()->value;
        }

        if ($expiredTime) {
            DB::table('app_property')->where('id', 'rating_expired_time')->update (['value' => $expiredTime]);
            $result['rating_expired_time'] = (int)AppProperty::where('id', 'rating_expired_time')->first()->value;

            Location::updateAllRatingSystem();
        }

        Location::updateAllRatingSystem();

        return $this->responseData($result);
    }

    public function getSpeedySetting(Request $request) {
        $result                           = NULL;
        $result['speedy_seller_avg_time'] = (int)AppProperty::where('id', 'speedy_seller_avg_time')->first()->value;
        $result['speedy_buyer_avg_time']  = (int)AppProperty::where('id', 'speedy_buyer_avg_time')->first()->value;
        $result['rating_expired_time']    = (int)AppProperty::where('id', 'rating_expired_time')->first()->value;
        return $this->responseData($result);
    }

    function getAppProperty(Request $request) {
        $user = NULL;
        try {
            $user = $this->currentUser($request);
            $user->validateIsMo();
        } catch(Exception $e) {
            return $this->responseError($e->getMessage());
        }
        $ids = $request->json()->get("ids");
        $appProperties = AppProperty::whereIn('id', $ids)->get();

        $result = [];
        foreach ($appProperties as $appProperty) {
            $result[$appProperty->id] = intval($appProperty->value);
        }

        return $this->responseData($result);
    }

    function updateAppProperty(Request $request) {
        $user = NULL;
        try {
            $user = $this->currentUser($request);
            $user->validateIsMo();
        } catch(Exception $e) {
            return $this->responseError($e->getMessage());
        }

        $inputs = $request->all();

        foreach ($inputs as $id => $value) {
            AppProperty::where('id', $id)->update(['value' => $value]);
        }

        return $this->responseData(['success' => true]);
    }
}