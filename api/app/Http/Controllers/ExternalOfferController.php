<?php

namespace App\Http\Controllers;

use App\BidHistory;
use App\Document;
use App\Libraries\Helper;
use App\Libraries\ErrorCodes;
use App\Libraries\Event;

use App\AppProperty;
use App\ExternalGroup;
use App\ExternalGroupOffer;
use App\ExternalOffer;
use App\WishList;
use App\Libraries\IdTransformer;
use App\Libraries\Permissions;
use App\Libraries\ResponseMessage;
use App\Location;
use App\OfferBid;
use App\Transaction;
use App\User;
use App\Valuation;
use App\ValuationInspectionItem;
use App\VehicleBlocked;
use App\VehicleOption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use PhpParser\Comment\Doc;

class ExternalOfferController extends Controller
{
    public function add(Request $request)
    {
        $token = $request->header('token');
        $arrivalDate = $request->json()->get('arrivalDate');
        $valuationId = $request->json()->get('valuationId');
        $group = $request->json()->get('group');

        $validationData = [
            'token' => $token,
            'arrivalDate' => $arrivalDate,
            'valuationId' => $valuationId,
            'group' => $group,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'arrivalDate' => 'required|string',
            'valuationId' => 'required|numeric|exists:mysql.valuation,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'string' => ErrorCodes::STRING,
            'array' => ErrorCodes::ARRAY_TYPE,
            'unique' => ErrorCodes::EXISTED
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $user = User::where('token', $token)->first();
        // CHECKING PERMISSION
        $apiPermission = array(Permissions::EXTERNAL_OFFER_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        //todo : check valuation not send to auction
        $valuation = Valuation::find($valuationId);
        if(!$valuation){
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        $valuationStatusArr =[Valuation::STATUS_SOLD_OUT, Valuation::STATUS_SUBMIT_TO_AUCTION, Valuation::STATUS_LISTED_FOR_AUCTION];
        if(in_array($valuation->status, $valuationStatusArr) ){
            \Log::info("Valuation is on auction list");
            return $this->responseError(ErrorCodes::VALUATION_ALREADY_SUBMITTED);
        }
        $externalOfferModel = new ExternalOffer();
        $externalOfferModel->arrival_date = $arrivalDate;
        $externalOfferModel->create_date = date('Y-m-d H:i:s');
        $externalOfferModel->status = ExternalOffer::STATUS_PENDING;
        $externalOfferModel->creator_id = $user->id;
        $externalOfferModel->valuation_id = $valuationId;
        $externalOfferModel->save();

        if ($group) {

            $externalOffer = ExternalOffer::find($externalOfferModel->id);
            $externalOffer->public_group = 0;
            $externalOffer->save();

            foreach ($group as $groupId) {
                $externalGOfferModel = new ExternalGroupOffer();
                $externalGOfferModel->external_group_id = $groupId;
                $externalGOfferModel->external_offer_id = $externalOfferModel->id;
                $externalGOfferModel->save();
            }
        } else {
            $externalOffer = ExternalOffer::find($externalOfferModel->id);
            $externalOffer->public_group = 1;
            $externalOffer->save();
        }

        $locationIds = WishList::locationsMatchingExternalOffer($externalOffer->id);
        $event = Event::WISHLIST_EXTERNAL_OFFER;
        $data = [];

        foreach ($locationIds as $locationId) {
            User::noticeLocation($locationId, $event, $externalOffer->id, $data);
        }

        return $this->responseData(['success' => TRUE]);
    }

    public function blockVehicle(Request $request)
    {
        $token = $request->header('token');
        $vehicleId = $request->json()->get('vehicleId');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::EXTERNAL_OFFER_DISCARD);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'vehicleId' => $vehicleId,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'vehicleId' => 'required|integer|exists:mysql.vehicle,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'integer' => ErrorCodes::NUMERIC,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $user = User::where('token', $token)->first();
        $vehicleBlockedModel = new VehicleBlocked();
        $vehicleBlockedModel->vehicle_id = $vehicleId;
        $vehicleBlockedModel->location_id = $user->location_id;
        $vehicleBlockedModel->save();

        return $this->responseData(['success' => TRUE]);
    }

    public function getOutgoing(Request $request)
    {
        $input = $request->json();
        $token = $request->header('token');
        $status = $request->json()->get('status');
        $limit = $request->json()->get('limit');
        $offset = $request->json()->get('offset');

        $orderDesc = FALSE;
        if ($input->has('orderDesc')) {
            $orderDesc = $input->get('orderDesc');
        }

        $validationData = [
            'token' => $token,
            'status' => $status,
            'limit' => $limit,
            'offset' => $offset,
            'orderDesc' => $orderDesc,
        ];
        $validationRule = [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'status' => 'required|array',
            'limit' => 'required|integer|min:1|max:100',
            'offset' => 'required|integer|min:0',
            'orderDesc' => 'boolean'
        ];

        $makeInfo = NULL;
        if ($input->has('makeInfo')) {
            $makeInfo = $input->get('makeInfo');
            $validationData['makeInfo'] = $makeInfo;
            $validationRule['makeInfo'] = 'required|array';
        }

        $odoRange = NULL;
        if ($input->has('odoRange')) {
            $odoRange = $input->get('odoRange');
            $validationData['odoRange'] = $odoRange;
            $validationRule['odoRange'] = 'required';
        }

        $yearRange = NULL;
        if ($input->has('yearRange')) {
            $yearRange = $input->get('yearRange');
            $validationData['yearRange'] = $yearRange;
            $validationRule['yearRange'] = 'required';
        }

        $vehicleLocation = NULL;
        if ($input->has('vehicleLocation')) {
            $vehicleLocation = $input->get('vehicleLocation');
            $validationData['vehicleLocation'] = $vehicleLocation;
            $validationRule['vehicleLocation'] = 'required|array';
        }

        $transmisionType = NULL;
        if ($input->has('transmisionType')) {
            $transmisionType = $input->get('transmisionType');
            $validationData['transmisionType'] = $transmisionType;
            $validationRule['transmisionType'] = 'required|array';
        }
        $lastDay = NULL;
        if ($input->has('lastDay')) {
            $lastDay = $input->get('lastDay');
            $validationData['lastDay'] = $lastDay;
            $validationRule['lastDay'] = 'required|integer|min:0';
        }

        $dateRange = NULL;
        if ($input->has('dateRange')) {
            $dateRange = $input->get('dateRange');
            $validationData['dateRange'] = $dateRange;
            $validationRule['dateRange'] = 'required';
        }

        $v = Validator::make($validationData, $validationRule, [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'integer' => ErrorCodes::INTEGER,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'boolean' => ErrorCodes::BOOLEAN,
            'array' => ErrorCodes::ARRAY_TYPE,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $user = User::where('token', $token)->first();

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::VALUATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }



        $filter = [];
        if ($makeInfo) {
            $filter['makeInfo'] = $makeInfo;
        }
        if ($odoRange) {
            $filter['odoRange'] = $odoRange;
        }
        if ($yearRange) {
            $filter['yearRange'] = $yearRange;
        }
        if ($vehicleLocation) {
            $filter['vehicleLocation'] = $vehicleLocation;
        }
        if ($transmisionType) {
            $filter['transmisionType'] = $transmisionType;
        }
        if ($lastDay) {
            $filter['lastDay'] = $lastDay;
        }
        if ($dateRange) {
            $filter['dateRange'] = $dateRange;
        }

        $offerList = ExternalOffer::getOutgoing($status, $user->id, $limit, $offset, $orderDesc, $filter);

        $result = [];
        $sections = [
            ValuationInspectionItem::EXTERIOR_SECTION,
            ValuationInspectionItem::INTERIOR_SECTION,
            ValuationInspectionItem::MECHANICAL_SECTION
        ];


        if (empty($offerList)) {
            $str = implode(",", $offerList);
            \LOG::debug("offerList: $str");
        }

        foreach ($offerList as $offer) {
            $offer->est_transport_cost = 0;
            foreach ($sections as $section) {
                if (empty($offerList)) {
                    \LOG::debug("offer: $offer | section: $section");
                }

                $subSection = [];
                $subItems = [];
                $valuationIIModel = new ValuationInspectionItem();
                $inspectionItems = $valuationIIModel->getItemsBySectionGroup($offer->valuation_id, $section);
                $total = 0;
                foreach ($inspectionItems as $item) {
                    $total += $item->repair_cost;
                    $subItems[] = [
                        'name' => $item->item,
                        'cost' => $item->repair_cost,
                    ];
                }
                if ($section == ValuationInspectionItem::EXTERIOR_SECTION) {
                    $valuation = Valuation::find($offer->valuation_id);
                    if ($valuation && $valuation->general_recondition_cost) {
                        $subItems[] = [
                            'name' => 'General Recondition Cost',
                            'cost' => $valuation->general_recondition_cost,
                        ];
                        $total += $valuation->general_recondition_cost;
                    }
                }
                $subSection['total_cost'] = $total;
                $subSection['items'] = $subItems;
                $offer->condition[strtolower($section)] = $subSection;
            }
            $options = VehicleOption::getDetail($offer->vehicle_id);
            $offer->car_option = $options ? TRUE : FALSE;
            $offer->option_list = $options;
            $result[] = $offer;
        }
        return $this->responseData($result);
    }

    public function bid(Request $request)
    {
        $token = $request->header('token');
        $externalOfferId = $request->json()->get('externalOfferId');
        $price = $request->json()->get('price');
        $expiryDate = $request->json()->get('expiryDate');

        $validationData = [
            'token' => $token,
            'externalOfferId' => $externalOfferId,
            'price' => $price,
            'expiryDate' => $expiryDate,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'externalOfferId' => 'required|numeric|exists:mysql.external_offer,id',
            'price' => 'required|numeric',
            'expiryDate' => 'required|string',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'string' => ErrorCodes::STRING,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }
        $externalOffer = ExternalOffer::find($externalOfferId);

        if ($externalOffer->status != ExternalOffer::STATUS_PENDING && $externalOffer->status != ExternalOffer::STATUS_OFFER_MADE) {
            return $this->responseError(ErrorCodes::INVALID, 'Status should be pending or offer_made');
        }

        $externalOfferExpiry = AppProperty::where('id', AppProperty::EXTERNAL_OFFER_EXPIRY)->first()->value;

        $expiryDate = date_create($expiryDate);
        $defaultDate = date_create($externalOffer->arrival_date);
        if ($externalOfferExpiry == 1) {
            $dateString = 'day';
        } else {
            $dateString = 'days';
        }
        date_add($defaultDate, date_interval_create_from_date_string($externalOfferExpiry . ' ' . $dateString));
        $diff = date_diff($defaultDate, $expiryDate);
        $dayDiff = intval($diff->format("%R%a"));
        if ($dayDiff < 0) {
            return $this->responseError(ErrorCodes::INVALID, 'Expiry date must be greater or equal to ' . $defaultDate->format('Y-m-d'));
        }

        $user = User::where('token', $token)->first();
        // CHECKING PERMISSION
        $apiPermission = array(Permissions::EXTERNAL_OFFER_BID);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $offerBidModel = new OfferBid();
        $offerBidModel->external_offer_id = $externalOfferId;
        $offerBidModel->user_id = $user->id;
        $offerBidModel->bid_date = date('Y-m-d H:i:s');
        $offerBidModel->bid_price = $price;
        $offerBidModel->expiry_date = $expiryDate;
        $offerBidModel->save();

        //Store a copy of bid to history transfer
        $bidHistory = new BidHistory();
        $bidHistory->trade_type = BidHistory::EXTERNAL_OFFER;
        $bidHistory->trade_id = $externalOfferId;
        $bidHistory->user_id = $user->id;
        $bidHistory->bid_date = date('Y-m-d H:i:s');
        $bidHistory->bid_price = $price;
        $bidHistory->save();

        if ($externalOffer->status != ExternalOffer::STATUS_OFFER_MADE) {
            $externalOffer->status = ExternalOffer::STATUS_OFFER_MADE;
            $externalOffer->save();
        }

        return $this->responseData(['success' => TRUE]);
    }

    public function accept(Request $request)
    {
        $token = $request->header('token');
        $externalOfferId = $request->json()->get('externalOfferId');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::EXTERNAL_OFFER_ACCEPT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'externalOfferId' => $externalOfferId,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'externalOfferId' => 'required|integer|exists:mysql.external_offer,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'integer' => ErrorCodes::NUMERIC,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }
        
        $externalOfferModel = ExternalOffer::find($externalOfferId);
        $externalOfferModel->status = ExternalOffer::STATUS_ACCEPTED;
        $externalOfferModel->save();

        $bestOffer = OfferBid::bestBid($externalOfferModel->valuation_id);
        if ($bestOffer) {
            DB::table('offer_bid')
                ->where('external_offer_id', $externalOfferId)
                ->where('id', '!=', $bestOffer->bid_id)
                ->delete();
        }

        $event = Event::BEST_OFFER_ACCEPTED;
        $buyerData = [];

        if ($bestOffer) {

            User::notice($bestOffer->buyer_id, $event, $externalOfferId, $buyerData);
        }

        return $this->responseData(['success' => TRUE]);
    }

    public function reject(Request $request)
    {
        $token = $request->header('token');
        $externalOfferId = $request->json()->get('externalOfferId');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::EXTERNAL_OFFER_ACCEPT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'externalOfferId' => $externalOfferId,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'externalOfferId' => 'required|integer|exists:mysql.external_offer,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'integer' => ErrorCodes::NUMERIC,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        DB::table('offer_bid')->where('external_offer_id', $externalOfferId)->delete();
        $externalOfferModel = ExternalOffer::find($externalOfferId);
        $externalOfferModel->delete();

        return $this->responseData(['success' => TRUE]);
    }

    public function setLocation(Request $request)
    {
        $token = $request->header('token');
        $locationId = $request->json()->get('locationId');
        $externalOfferId = $request->json()->get('externalOfferId');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::EXTERNAL_OFFER_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $validationData = [
            'token' => $token,
            'externalOfferId' => $externalOfferId,
            'locationId' => $locationId,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'externalOfferId' => 'required|integer|exists:mysql.external_offer,id',
            'locationId' => 'required|integer|exists:mysql.location,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'integer' => ErrorCodes::NUMERIC,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $externalOffer = ExternalOffer::find($externalOfferId);
        $externalOffer->status = ExternalOffer::STATUS_LOCATION_SET;
        $externalOffer->receive_location = $locationId;
        $externalOffer->save();
        // check if vehicle is arrival,if yes then create transaction
        $vehicle = DB::select("SELECT veh.id,veh.trade_in_location FROM `external_offer` join valuation val on val.id = external_offer.valuation_id
                                  JOIN vehicle veh ON veh.id = val.vehicle_id where external_offer.id = $externalOfferId");

    $transaction_id = null;
        if(count($vehicle) > 0 && $vehicle[0]->trade_in_location) {
            DB::transaction(function () use ($externalOffer, $externalOfferId, $locationId, $vehicle) {
                //create new record to transaction table
                $bestOffer = DB::select("Select * from offer_bid where external_offer_id = $externalOfferId LIMIT 0,1");

                $transaction = new Transaction();
                $transaction->valuation_id = $externalOffer->valuation_id;
                $transaction->buyer_id = $bestOffer[0]->user_id;
                $transaction->seller_id = $externalOffer->creator_id;

                $transaction->status = Transaction::STATUS_WAITING_PAYMENT;
                $transaction->vehicle_price = $bestOffer[0]->bid_price;
                $transaction->valuation_version = Valuation::find($externalOffer->valuation_id)->version;
                $transaction->source = Transaction::SOURCE_EXTERNAL_OFFER;
                $transaction->source_id = $externalOfferId;
                $transaction->buyer_location = $locationId;
                $transaction->seller_location = $vehicle[0]->trade_in_location;

                $externalOffer->status = ExternalOffer::STATUS_VEHICLE_READY_TO_SHIP;
                $externalOffer->save();

                //create rid
                $transaction->save();
                $transaction_id = $transaction->id;
                $transaction->rid = IdTransformer::toClientTransactionId($transaction_id);
                Transaction::setMotoFee($transaction);
                $transaction->save();

                //update rating
                $locationId = Valuation::where('valuation.id', $transaction->valuation_id)
                    ->join('vehicle', 'vehicle.id', '=', 'valuation.vehicle_id')
                    ->first()
                    ->location_id;
                Location::updateRating($locationId);

                //update status valuation table
                DB::table('valuation')
                    ->where('id', '=', $externalOffer->valuation_id)
                    ->update(['status' => Valuation::STATUS_SOLD_OUT]);

                $event = Event::TRANSACTION_NEW;

                $sellerData = [];
                $buyerData  = [];

                \LOG::debug("start generate document for event: [$event]");
                $buyerData['attach'] =  Document::generatorPDFAndUploadAzure($transaction->seller_id, $event, $transaction->id, 'creator');

                User::notice($transaction->seller_id, $event, $transaction->id, $sellerData);
                User::notice($transaction->buyer_id, $event, $transaction->id, $buyerData);
            });
        } else {
            \Log::debug("externalOffer->id dont have trade_in_location: $externalOffer->id");
        }

        return $this->responseData([
            'success' => TRUE,
            'transaction_id' => $transaction_id
            ]);
    }

    public function setVehicleReadyToShip(Request $request)
    {
        $token = $request->header('token');
        $externalOfferId = $request->json()->get('externalOfferId');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::VEHICLE_TRADE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'externalOfferId' => $externalOfferId,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'externalOfferId' => 'required|integer|exists:mysql.external_offer,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'integer' => ErrorCodes::NUMERIC,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $externalOffer = ExternalOffer::find($externalOfferId);
        $externalOffer->status = ExternalOffer::STATUS_VEHICLE_READY_TO_SHIP;
        $externalOffer->save();

        return $this->responseData(['success' => TRUE]);
    }

    public function setTransportTriggered(Request $request)
    {
        $token = $request->header('token');
        $externalOfferId = $request->json()->get('externalOfferId');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::VEHICLE_TRADE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'externalOfferId' => $externalOfferId,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'externalOfferId' => 'required|integer|exists:mysql.external_offer,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'integer' => ErrorCodes::NUMERIC,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $externalOffer = ExternalOffer::find($externalOfferId);
        $externalOffer->status = ExternalOffer::STATUS_TRANSPORT_TRIGGERED;
        $externalOffer->save();

        return $this->responseData(['success' => TRUE]);
    }

    public function setShipped(Request $request)
    {
        $token = $request->header('token');
        $externalOfferId = $request->json()->get('externalOfferId');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::VEHICLE_TRADE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'externalOfferId' => $externalOfferId,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'externalOfferId' => 'required|integer|exists:mysql.external_offer,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'integer' => ErrorCodes::NUMERIC,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $externalOffer = ExternalOffer::find($externalOfferId);
        $externalOffer->status = ExternalOffer::STATUS_SHIPPED;
        $externalOffer->save();

        return $this->responseData(['success' => TRUE]);
    }

    public function setDelivered(Request $request)
    {
        $token = $request->header('token');
        $externalOfferId = $request->json()->get('externalOfferId');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::VEHICLE_TRADE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'externalOfferId' => $externalOfferId,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'externalOfferId' => 'required|integer|exists:mysql.external_offer,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'integer' => ErrorCodes::NUMERIC,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $externalOffer = ExternalOffer::find($externalOfferId);
        $externalOffer->status = ExternalOffer::STATUS_DELIVERED;
        $externalOffer->save();

        return $this->responseData(['success' => TRUE]);
    }

    public function getOffer(Request $request, $valuationId)
    {
        $token = $request->header('token');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::EXTERNAL_OFFER_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'valuationId' => $valuationId,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'valuationId' => 'required|integer|exists:mysql.valuation,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'integer' => ErrorCodes::NUMERIC,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $externalOffer = OfferBid::getOfferBid($valuationId);

        $id = NULL;
        $arrivalDate = NULL;
        $createDate = NULL;
        $status = NULL;
        $creatorId = NULL;
        $bestOffer = NULL;
        if ($externalOffer) {
            $id = $externalOffer->id;
            $arrivalDate = $externalOffer->arrival_date;
            $createDate = $externalOffer->create_date;
            $status = $externalOffer->status;
            $creatorId = $externalOffer->creator_id;
            $bestOfferInclude = [
                ExternalOffer::STATUS_AWAITING_RESPONSE,
                ExternalOffer::STATUS_ACCEPTED,
                ExternalOffer::STATUS_LOCATION_SET,
                ExternalOffer::STATUS_VEHICLE_READY_TO_SHIP,
                ExternalOffer::STATUS_TRANSPORT_TRIGGERED,
                ExternalOffer::STATUS_SHIPPED,
                ExternalOffer::STATUS_DELIVERED,
            ];
            if (in_array($status, $bestOfferInclude) && $externalOffer->bid_id) {
                $bestOffer = [
                    "bid_id" => $externalOffer->bid_id,
                    "user_id"=> $externalOffer->user_id,
                    "bid_date" => $externalOffer->bid_date,
                    "bid_price"=> $externalOffer->bid_price,
                    "expiry_date"=> $externalOffer->expiry_date
                ];
            }
        }

        $data = [
            'id' => $id,
            'arrival_date' => $arrivalDate,
            'create_date' => $createDate,
            'system_time' => date('Y-m-d H:i:s'),
            'status' => $status,
            'creator_id'=> $creatorId,
            'best_offer' => $bestOffer
        ];

        return $this->responseData($data);
    }

    public function getById(Request $request, $id)
    {
        $token = $request->header('token');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::EXTERNAL_OFFER_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'id' => $id,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'id' => 'required|integer|exists:mysql.external_offer,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'integer' => ErrorCodes::NUMERIC,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $externalOffer = OfferBid::getOfferBidId($id);

        $id = NULL;
        $arrivalDate = NULL;
        $createDate = NULL;
        $status = NULL;
        $creatorId = NULL;
        $bestOffer = NULL;
        if ($externalOffer) {
            $id = $externalOffer->id;
            $arrivalDate = $externalOffer->arrival_date;
            $createDate = $externalOffer->create_date;
            $status = $externalOffer->status;
            $creatorId = $externalOffer->creator_id;
            $bestOfferInclude = [
                ExternalOffer::STATUS_AWAITING_RESPONSE,
                ExternalOffer::STATUS_ACCEPTED,
                ExternalOffer::STATUS_LOCATION_SET,
                ExternalOffer::STATUS_VEHICLE_READY_TO_SHIP,
                ExternalOffer::STATUS_TRANSPORT_TRIGGERED,
                ExternalOffer::STATUS_SHIPPED,
                ExternalOffer::STATUS_DELIVERED,
            ];
            if (in_array($status, $bestOfferInclude) && $externalOffer->bid_id) {
                $bestOffer = [
                    "bid_id" => $externalOffer->bid_id,
                    "user_id"=> $externalOffer->user_id,
                    "bid_date" => $externalOffer->bid_date,
                    "bid_price"=> $externalOffer->bid_price,
                    "expiry_date"=> $externalOffer->expiry_date
                ];
            }
        }

        $data = [
            'id' => $id,
            'arrival_date' => $arrivalDate,
            'create_date' => $createDate,
            'system_time' => date('Y-m-d H:i:s'),
            'status' => $status,
            'creator_id'=> $creatorId,
            'best_offer' => $bestOffer
        ];

        return $this->responseData($data);
    }

    public function getInComing(Request $request)
    {
        $input = $request->json();
        $token = $request->header('token');
        $status = $request->json()->get('status');
        $limit = $request->json()->get('limit');
        $offset = $request->json()->get('offset');

        $orderDesc = FALSE;

        if ($input->has('orderDesc')) {
            $orderDesc = $input->get('orderDesc');
        }

        $validationData = [
            'token' => $token,
            'status' => $status,
            'limit' => $limit,
            'offset' => $offset,
            'orderDesc' => $orderDesc,
        ];
        $validationRule = [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'status' => 'required|array',
            'limit' => 'required|integer|min:1|max:100',
            'offset' => 'required|integer|min:0',
            'orderDesc' => 'boolean'
        ];

        $makeInfo = NULL;
        if ($input->has('makeInfo')) {
            $makeInfo = $input->get('makeInfo');
            $validationData['makeInfo'] = $makeInfo;
            $validationRule['makeInfo'] = 'required|array';
        }

        $odoRange = NULL;
        if ($input->has('odoRange')) {
            $odoRange = $input->get('odoRange');
            $validationData['odoRange'] = $odoRange;
            $validationRule['odoRange'] = 'required';
        }

        $yearRange = NULL;
        if ($input->has('yearRange')) {
            $yearRange = $input->get('yearRange');
            $validationData['yearRange'] = $yearRange;
            $validationRule['yearRange'] = 'required';
        }

        $vehicleLocation = NULL;
        if ($input->has('vehicleLocation')) {
            $vehicleLocation = $input->get('vehicleLocation');
            $validationData['vehicleLocation'] = $vehicleLocation;
            $validationRule['vehicleLocation'] = 'required|array';
        }

        $transmisionType = NULL;
        if ($input->has('transmisionType')) {
            $transmisionType = $input->get('transmisionType');
            $validationData['transmisionType'] = $transmisionType;
            $validationRule['transmisionType'] = 'required|array';
        }

        $lastDay = NULL;
        if ($input->has('lastDay')) {
            $lastDay = $input->get('lastDay');
            $validationData['lastDay'] = $lastDay;
            $validationRule['lastDay'] = 'required|integer|min:0';
        }

        $dateRange = NULL;
        if ($input->has('dateRange')) {
            $dateRange = $input->get('dateRange');
            $validationData['dateRange'] = $dateRange;
            $validationRule['dateRange'] = 'required';
        }

        $v = Validator::make($validationData, $validationRule, [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'integer' => ErrorCodes::INTEGER,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'boolean' => ErrorCodes::BOOLEAN,
            'array' => ErrorCodes::ARRAY_TYPE,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }
        $user = User::getCompany($token);

        $apiPermission = array(Permissions::EXTERNAL_OFFER_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $filter = [];
        if ($makeInfo) {
            $filter['makeInfo'] = $makeInfo;
        }
        if ($odoRange) {
            $filter['odoRange'] = $odoRange;
        }
        if ($yearRange) {
            $filter['yearRange'] = $yearRange;
        }
        if ($vehicleLocation) {
            $filter['vehicleLocation'] = $vehicleLocation;
        }
        if ($transmisionType) {
            $filter['transmisionType'] = $transmisionType;
        }
        if ($lastDay) {
            $filter['lastDay'] = $lastDay;
        }
        if ($dateRange) {
            $filter['dateRange'] = $dateRange;
        }

        $offerList = ExternalOffer::getIncoming($status, $user->id, $user->location_id, $user->company_id, $limit, $offset, $orderDesc, $filter);
        $result = [];
        $sections = [
            ValuationInspectionItem::EXTERIOR_SECTION,
            ValuationInspectionItem::INTERIOR_SECTION,
            ValuationInspectionItem::MECHANICAL_SECTION
        ];
        foreach ($offerList as $offer) {
            $offer->est_transport_cost = 0;
            foreach ($sections as $section) {
                $subSection = [];
                $subItems = [];
                $valuationIIModel = new ValuationInspectionItem();
                $inspectionItems = $valuationIIModel->getItemsBySectionGroup($offer->valuation_id, $section);
                $total = 0;
                foreach ($inspectionItems as $item) {
                    $total += $item->repair_cost;
                    $subItems[] = [
                        'name' => $item->item,
                        'cost' => $item->repair_cost,
                    ];
                }
                if ($section == ValuationInspectionItem::EXTERIOR_SECTION) {
                    $valuation = Valuation::find($offer->valuation_id);
                    if ($valuation && $valuation->general_recondition_cost) {
                        $subItems[] = [
                            'name' => 'General Recondition Cost',
                            'cost' => $valuation->general_recondition_cost,
                        ];
                        $total += $valuation->general_recondition_cost;
                    }
                }

                $subSection['total_cost'] = $total;
                $subSection['items'] = $subItems;
                $offer->condition[strtolower($section)] = $subSection;
            }
            $options = VehicleOption::getDetail($offer->vehicle_id);
            $offer->car_option = $options ? TRUE : FALSE;
            $offer->option_list = $options;
            $result[] = $offer;
        }
        return $this->responseData($result);
    }

    public function getOfferWon(Request $request)
    {
        $input = $request->json();
        $token = $request->header('token');
        $status = $request->json()->get('status');
        $limit = $request->json()->get('limit');
        $offset = $request->json()->get('offset');

        $orderDesc = FALSE;
        if ($input->has('orderDesc')) {
            $orderDesc = $input->get('orderDesc');
        }

        $validationData = [
            'token' => $token,
            'status' => $status,
            'limit' => $limit,
            'offset' => $offset,
            'orderDesc' => $orderDesc,
        ];
        $validationRule = [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'status' => 'required|array',
            'limit' => 'required|integer|min:1|max:100',
            'offset' => 'required|integer|min:0',
            'orderDesc' => 'boolean'
        ];

        $makeInfo = NULL;
        if ($input->has('makeInfo')) {
            $makeInfo = $input->get('makeInfo');
            $validationData['makeInfo'] = $makeInfo;
            $validationRule['makeInfo'] = 'required|array';
        }

        $odoRange = NULL;
        if ($input->has('odoRange')) {
            $odoRange = $input->get('odoRange');
            $validationData['odoRange'] = $odoRange;
            $validationRule['odoRange'] = 'required';
        }

        $yearRange = NULL;
        if ($input->has('yearRange')) {
            $yearRange = $input->get('yearRange');
            $validationData['yearRange'] = $yearRange;
            $validationRule['yearRange'] = 'required';
        }

        $vehicleLocation = NULL;
        if ($input->has('vehicleLocation')) {
            $vehicleLocation = $input->get('vehicleLocation');
            $validationData['vehicleLocation'] = $vehicleLocation;
            $validationRule['vehicleLocation'] = 'required|array';
        }

        $transmisionType = NULL;
        if ($input->has('transmisionType')) {
            $transmisionType = $input->get('transmisionType');
            $validationData['transmisionType'] = $transmisionType;
            $validationRule['transmisionType'] = 'required|array';
        }

        $lastDay = NULL;
        if ($input->has('lastDay')) {
            $lastDay = $input->get('lastDay');
            $validationData['lastDay'] = $lastDay;
            $validationRule['lastDay'] = 'required|integer|min:0';
        }

        $dateRange = NULL;
        if ($input->has('dateRange')) {
            $dateRange = $input->get('dateRange');
            $validationData['dateRange'] = $dateRange;
            $validationRule['dateRange'] = 'required';
        }

        $v = Validator::make($validationData, $validationRule, [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'integer' => ErrorCodes::INTEGER,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'boolean' => ErrorCodes::BOOLEAN,
            'array' => ErrorCodes::ARRAY_TYPE,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }
        $user = User::getCompany($token);
        $apiPermission = array(Permissions::EXTERNAL_OFFER_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $filter = [];
        if ($makeInfo) {
            $filter['makeInfo'] = $makeInfo;
        }
        if ($odoRange) {
            $filter['odoRange'] = $odoRange;
        }
        if ($yearRange) {
            $filter['yearRange'] = $yearRange;
        }
        if ($vehicleLocation) {
            $filter['vehicleLocation'] = $vehicleLocation;
        }
        if ($transmisionType) {
            $filter['transmisionType'] = $transmisionType;
        }
        if ($lastDay) {
            $filter['lastDay'] = $lastDay;
        }
        if ($dateRange) {
            $filter['dateRange'] = $dateRange;
        }

        $offerList = ExternalOffer::getOfferWon($status, $user->location_id, $user->company_id, $limit, $offset, $orderDesc, $filter);
        $result = [];
        $sections = [
            ValuationInspectionItem::EXTERIOR_SECTION,
            ValuationInspectionItem::INTERIOR_SECTION,
            ValuationInspectionItem::MECHANICAL_SECTION
        ];
        foreach ($offerList as $offer) {
            $offer->est_transport_cost = 0;$valuation = Valuation::find($offer->valuation_id);
            if($valuation){
                $offer->total_recon_cost = $valuation->total_recon_cost;
                }
            foreach ($sections as $section) {
                $subSection = [];
                $subItems = [];
                $valuationIIModel = new ValuationInspectionItem();
                $inspectionItems = $valuationIIModel->getItemsBySectionGroup($offer->valuation_id, $section);
                $total = 0;
                foreach ($inspectionItems as $item) {
                    $total += $item->repair_cost;
                    $subItems[] = [
                        'name' => $item->item,
                        'cost' => $item->repair_cost,
                    ];
                }

                if ($section == ValuationInspectionItem::EXTERIOR_SECTION) {

                    if ($valuation && $valuation->general_recondition_cost) {
                        $subItems[] = [
                            'name' => 'General Recondition Cost',
                            'cost' => $valuation->general_recondition_cost,
                        ];
                        $total += $valuation->general_recondition_cost;
                    }
                }
                $subSection['total_cost'] = $total;
                $subSection['items'] = $subItems;
                $offer->condition[strtolower($section)] = $subSection;
            }
            $options = VehicleOption::getDetail($offer->vehicle_id);
            $offer->car_option = $options ? TRUE : FALSE;
            $offer->option_list = $options;
            $result[] = $offer;
        }
        return $this->responseData($result);
    }

    public function getPurchaseHistory(Request $request)
    {
        $input = $request->json();
        $token = $request->header('token');
        $status = $request->json()->get('status');
        $limit = $request->json()->get('limit');
        $offset = $request->json()->get('offset');

        $orderDesc = FALSE;
        if ($input->has('orderDesc')) {
            $orderDesc = $input->get('orderDesc');
        }

        $validationData = [
            'token' => $token,
            'status' => $status,
            'limit' => $limit,
            'offset' => $offset,
            'orderDesc' => $orderDesc,
        ];
        $validationRule = [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'status' => 'required|array',
            'limit' => 'required|integer|min:1|max:100',
            'offset' => 'required|integer|min:0',
            'orderDesc' => 'boolean'
        ];

        $makeInfo = NULL;
        if ($input->has('makeInfo')) {
            $makeInfo = $input->get('makeInfo');
            $validationData['makeInfo'] = $makeInfo;
            $validationRule['makeInfo'] = 'required|array';
        }

        $odoRange = NULL;
        if ($input->has('odoRange')) {
            $odoRange = $input->get('odoRange');
            $validationData['odoRange'] = $odoRange;
            $validationRule['odoRange'] = 'required';
        }

        $yearRange = NULL;
        if ($input->has('yearRange')) {
            $yearRange = $input->get('yearRange');
            $validationData['yearRange'] = $yearRange;
            $validationRule['yearRange'] = 'required';
        }

        $vehicleLocation = NULL;
        if ($input->has('vehicleLocation')) {
            $vehicleLocation = $input->get('vehicleLocation');
            $validationData['vehicleLocation'] = $vehicleLocation;
            $validationRule['vehicleLocation'] = 'required|array';
        }

        $transmisionType = NULL;
        if ($input->has('transmisionType')) {
            $transmisionType = $input->get('transmisionType');
            $validationData['transmisionType'] = $transmisionType;
            $validationRule['transmisionType'] = 'required|array';
        }

        $lastDay = NULL;
        if ($input->has('lastDay')) {
            $lastDay = $input->get('lastDay');
            $validationData['lastDay'] = $lastDay;
            $validationRule['lastDay'] = 'required|integer|min:0';
        }

        $dateRange = NULL;
        if ($input->has('dateRange')) {
            $dateRange = $input->get('dateRange');
            $validationData['dateRange'] = $dateRange;
            $validationRule['dateRange'] = 'required';
        }

        $v = Validator::make($validationData, $validationRule, [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'integer' => ErrorCodes::INTEGER,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'boolean' => ErrorCodes::BOOLEAN,
            'array' => ErrorCodes::ARRAY_TYPE,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }
        $user = User::getCompany($token);
        $apiPermission = array(Permissions::EXTERNAL_OFFER_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $filter = [];
        if ($makeInfo) {
            $filter['makeInfo'] = $makeInfo;
        }
        if ($odoRange) {
            $filter['odoRange'] = $odoRange;
        }
        if ($yearRange) {
            $filter['yearRange'] = $yearRange;
        }
        if ($vehicleLocation) {
            $filter['vehicleLocation'] = $vehicleLocation;
        }
        if ($transmisionType) {
            $filter['transmisionType'] = $transmisionType;
        }
        if ($lastDay) {
            $filter['lastDay'] = $lastDay;
        }
        if ($dateRange) {
            $filter['dateRange'] = $dateRange;
        }

        $offerList = ExternalOffer::getPurchaseHistory($status, $user->company_group_id, $limit, $offset, $orderDesc, $filter);
        $result = [];
        $sections = [
            ValuationInspectionItem::EXTERIOR_SECTION,
            ValuationInspectionItem::INTERIOR_SECTION,
            ValuationInspectionItem::MECHANICAL_SECTION
        ];
        foreach ($offerList as $offer) {
            $offer->est_transport_cost = 0;
            foreach ($sections as $section) {
                $subSection = [];
                $subItems = [];
                $valuationIIModel = new ValuationInspectionItem();
                $inspectionItems = $valuationIIModel->getItemsBySectionGroup($offer->valuation_id, $section);
                $total = 0;
                foreach ($inspectionItems as $item) {
                    $total += $item->repair_cost;
                    $subItems[] = [
                        'name' => $item->item,
                        'cost' => $item->repair_cost,
                    ];
                }
                if ($section == ValuationInspectionItem::EXTERIOR_SECTION) {
                    $valuation = Valuation::find($offer->valuation_id);
                    if ($valuation && $valuation->general_recondition_cost) {
                        $subItems[] = [
                            'name' => 'General Recondition Cost',
                            'cost' => $valuation->general_recondition_cost,
                        ];
                        $total += $valuation->general_recondition_cost;
                    }
                }
                $subSection['total_cost'] = $total;
                $subSection['items'] = $subItems;
                $offer->condition[strtolower($section)] = $subSection;
            }
            $options = VehicleOption::getDetail($offer->vehicle_id);
            $offer->car_option = $options ? TRUE : FALSE;
            $offer->option_list = $options;
            $result[] = $offer;
        }
        return $this->responseData($result);
    }

    public function getSoldHistory(Request $request)
    {
        $input = $request->json();
        $token = $request->header('token');
        $status = $request->json()->get('status');
        $limit = $request->json()->get('limit');
        $offset = $request->json()->get('offset');

        $orderDesc = FALSE;
        if ($input->has('orderDesc')) {
            $orderDesc = $input->get('orderDesc');
        }

        $validationData = [
            'token' => $token,
            'status' => $status,
            'limit' => $limit,
            'offset' => $offset,
            'orderDesc' => $orderDesc,
        ];
        $validationRule = [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'status' => 'required|array',
            'limit' => 'required|integer|min:1|max:100',
            'offset' => 'required|integer|min:0',
            'orderDesc' => 'boolean'
        ];

        $makeInfo = NULL;
        if ($input->has('makeInfo')) {
            $makeInfo = $input->get('makeInfo');
            $validationData['makeInfo'] = $makeInfo;
            $validationRule['makeInfo'] = 'required|array';
        }

        $odoRange = NULL;
        if ($input->has('odoRange')) {
            $odoRange = $input->get('odoRange');
            $validationData['odoRange'] = $odoRange;
            $validationRule['odoRange'] = 'required';
        }

        $yearRange = NULL;
        if ($input->has('yearRange')) {
            $yearRange = $input->get('yearRange');
            $validationData['yearRange'] = $yearRange;
            $validationRule['yearRange'] = 'required';
        }

        $vehicleLocation = NULL;
        if ($input->has('vehicleLocation')) {
            $vehicleLocation = $input->get('vehicleLocation');
            $validationData['vehicleLocation'] = $vehicleLocation;
            $validationRule['vehicleLocation'] = 'required|array';
        }

        $transmisionType = NULL;
        if ($input->has('transmisionType')) {
            $transmisionType = $input->get('transmisionType');
            $validationData['transmisionType'] = $transmisionType;
            $validationRule['transmisionType'] = 'required|array';
        }

        $lastDay = NULL;
        if ($input->has('lastDay')) {
            $lastDay = $input->get('lastDay');
            $validationData['lastDay'] = $lastDay;
            $validationRule['lastDay'] = 'required|integer|min:0';
        }

        $dateRange = NULL;
        if ($input->has('dateRange')) {
            $dateRange = $input->get('dateRange');
            $validationData['dateRange'] = $dateRange;
            $validationRule['dateRange'] = 'required';
        }

        $v = Validator::make($validationData, $validationRule, [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'integer' => ErrorCodes::INTEGER,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'boolean' => ErrorCodes::BOOLEAN,
            'array' => ErrorCodes::ARRAY_TYPE,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }
        $user = User::getCompany($token);
        $apiPermission = array(Permissions::EXTERNAL_OFFER_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $filter = [];
        if ($makeInfo) {
            $filter['makeInfo'] = $makeInfo;
        }
        if ($odoRange) {
            $filter['odoRange'] = $odoRange;
        }
        if ($yearRange) {
            $filter['yearRange'] = $yearRange;
        }
        if ($vehicleLocation) {
            $filter['vehicleLocation'] = $vehicleLocation;
        }
        if ($transmisionType) {
            $filter['transmisionType'] = $transmisionType;
        }
        if ($lastDay) {
            $filter['lastDay'] = $lastDay;
        }
        if ($dateRange) {
            $filter['dateRange'] = $dateRange;
        }

        $offerList = ExternalOffer::getSoldHistory($status, $user->company_group_id, $limit, $offset, $orderDesc, $filter);
        $result = [];
        $sections = [
            ValuationInspectionItem::EXTERIOR_SECTION,
            ValuationInspectionItem::INTERIOR_SECTION,
            ValuationInspectionItem::MECHANICAL_SECTION
        ];
        foreach ($offerList as $offer) {
            $offer->est_transport_cost = 0;
            foreach ($sections as $section) {
                $subSection = [];
                $subItems = [];
                $valuationIIModel = new ValuationInspectionItem();
                $inspectionItems = $valuationIIModel->getItemsBySectionGroup($offer->valuation_id, $section);
                $total = 0;
                foreach ($inspectionItems as $item) {
                    $total += $item->repair_cost;
                    $subItems[] = [
                        'name' => $item->item,
                        'cost' => $item->repair_cost,
                    ];
                }
                if ($section == ValuationInspectionItem::EXTERIOR_SECTION) {
                    $valuation = Valuation::find($offer->valuation_id);
                    if ($valuation && $valuation->general_recondition_cost) {
                        $subItems[] = [
                            'name' => 'General Recondition Cost',
                            'cost' => $valuation->general_recondition_cost,
                        ];
                        $total += $valuation->general_recondition_cost;
                    }
                }
                $subSection['total_cost'] = $total;
                $subSection['items'] = $subItems;
                $offer->condition[strtolower($section)] = $subSection;
            }
            $options = VehicleOption::getDetail($offer->vehicle_id);
            $offer->car_option = $options ? TRUE : FALSE;
            $offer->option_list = $options;
            $result[] = $offer;
        }
        return $this->responseData($result);
    }


    public function getGroup(Request $request)
    {
        $token = $request->header('token');
        $validationData = ['token' => $token];

        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token'
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $user = User::getCompany($token);
        $groups = ExternalGroup::orderBy('name')->where('company_id', [$user->company_id])->get();

        return $this->responseData($groups);
    }

    public function searchExternalOfferGroup(Request $request){
        $token = $request->header("token");
        $user = NULL;


        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [

            'query' => 'max:100',
            'limit' => 'required|integer|min:1|max:100',
            'offset' => 'required|integer|min:0',
            'orderBy' => 'in:first_name,last_name',
            'orderDesc' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $apiPermission = array(Permissions::EXTERNAL_GROUP_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

//            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $input = $request->json();
        $responseMessage = new ResponseMessage();

        $externalOfferGroup = new ExternalGroup();
        //Get list user with pagination and full size
        $externalOfferGroupList = $externalOfferGroup->searchExternalGroup($input, $user->id);

        $statusCode = 200;

        $responseMessage->setData('list_size', $externalOfferGroupList[0]);
        $responseMessage->setData('externalOfferGroupList', $externalOfferGroupList['externalOfferGroupList']);

        $result = $responseMessage->getMessage();

        return response()->json($result, $statusCode);
    }


}