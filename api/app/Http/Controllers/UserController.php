<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Channel;
use App\ChannelUser;
use App\Company;
use App\CompanyGroup;
use App\ConfigEmailNotification;
use App\Event;
use App\Contact;
use App\DataAccess;
use App\EventPermission;
use App\Libraries\Permissions;
use App\Libraries\ResponseMessage;
use App\Libraries\Helper;
use App\Location;
use App\Permission;
use App\User;
use App\UserPermission;
use App\UserRole;
use GuzzleHttp\Exception\ClientException;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Libraries\ErrorCodes;
use GuzzleHttp\Client;
use Illuminate\Validation\Rule;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMyLocations(Request $request) {
        $user = NULL;
        $company_group_id = NULL;
        $token = $request->header("token");
        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        return $this->responseData($user->getAccessableLocationIds());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addUser(Request $request)
    {

        $token = $request->header("token");
        $user = NULL;
        $company_group_id = NULL;

        if ($token && $token != 'webmaster') {
            $user = User::where('token', $token)->first();
        }

        if ($token != 'webmaster' && !$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $company_group_id = null;
        //get location id of login user
        $location_id = null;

        if ($token != 'webmaster') {
            $apiPermission = array(Permissions::USER_CREATE);
            $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
            if(!$valuationResult){
                return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
            }

            // get company id of request user
            $company_group_id = $user->company_group_id;
            //get location id of login user
            $location_id = $user->location_id;
        }

        $responseMessage = new ResponseMessage();
        $result = $this->addUserAndContact($request, $responseMessage, $location_id, $company_group_id);

        if ($token != 'webmaster') {
            $events = Event::all();
            foreach ($events as $event) {
                if (ConfigEmailNotification::where([['user_id', '=', $user->id],
                                                    ['event', '=', $event->code]])->first() != NULL) {
                    continue;
                }
                $config             = new ConfigEmailNotification();
                $config->user_id    = $user->id;
                $config->event      = $event->code;
                $config->config     = ConfigEmailNotification::CONFIG_BOTH;
                $config->save();
            }
        }

        return $result;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addUserByAdmin(Request $request){
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $apiPermission = array(Permissions::USER_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $locationId = $request->json()->get('location_id');
        $company_group_id = $request->json()->get('company_group_id');
        $responseMessage = new ResponseMessage();
        $result = $this->addUserAndContact($request, $responseMessage, $locationId,$company_group_id);

        return $result;
    }

    /**
     * @param Request $request
     * @param $userId
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function updateUserByAdmin(Request $request, $userId){
        $token = $request->header("token");
        $userLogged = NULL;

        if ($token) {
            $userLogged = User::where('token', $token)->first();
        }

        if (!$userLogged) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $apiPermission = array(Permissions::USER_ADMIN_UPDATE);
        $valuationResult = Helper::valuatePermission($userLogged->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $locationId = $request->json()->get('location_id');
        $company_group_id = $request->json()->get('company_group_id');
        $responseMessage = new ResponseMessage();

        $errorMessages = '';
        $v = Validator::make($request->all(), [
            'first_name' => 'required',
            'password' => 'alpha_num|min:6',
            'code' => 'required',
            'address1' => 'required',
            'suburb' => 'required',
            'country' => 'required|integer',
            'mobileNumber' => 'required',
            'privacy' => 'required',
            'gender' => 'integer',
            'state' => 'integer',
            'roleId' => 'required',
            'range_from' => 'required',
            'range_to' => 'required',

        ], [
            'required' => ErrorCodes::REQUIRED,
            'email' => ErrorCodes::INVALID,
            'unique' => ErrorCodes::UNIQUE,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'min' => ErrorCodes::LENGTH_MIN_INVALID,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
        }
        if ($errorMessages) {
            $statusCode = 400;
            $responseMessage->setErrorCode(ErrorCodes::INVALID_INPUT);
            $responseMessage->setErrorDetail($errorMessages);
        } else {
            // update user info
            $user = User::find($userId);
            if(!$user){
                $statusCode = 400;
                $responseMessage->setErrorCode(ErrorCodes::USER_NOT_FOUND);
                $result = $responseMessage->getMessage();
                return response()->json($result, $statusCode);
            }
            $code = $request->json()->get('code');
            $password = $request->json()->get('password');
            if($code != $user->user_code){
                $userDTO = new User();
                $validateUserCode = $userDTO->validateUserCodeAvailable($code,$userId);
                if($validateUserCode->count < 1)
                    $user->user_code = $code;
                else{
                    $statusCode = 400;
                    $responseMessage->setErrorCode(ErrorCodes::USER_CODE_DUPLICATE);
                    $result = $responseMessage->getMessage();
                    return response()->json($result, $statusCode);
                }
            }

            if($password) {
                $user->password = Helper::hash($password);
            }

            $newCompanyId = Location::find($locationId)->company_id;

            $user->location_id = $locationId;
            $user->company_group_id = $company_group_id;
            $user->update_date = date('Y-m-d H:i:s');
            $user->updateCompanyChannel($newCompanyId);

            $user->save();

            // if contact existing, update user to link to new contact
            $existingContact = $request->json()->get('existingContact');
            if($existingContact){
                // contact exist, check id if contact is not link to any user.
                $contactId = $request->json()->get('existContactId');
                $contact = Contact::find($contactId);
                if(!$contact){
                    $statusCode = 400;
                    $responseMessage->setErrorCode(ErrorCodes::CONTACT_NOT_FOUND);

                    $result = $responseMessage->getMessage();
                    return response()->json($result, $statusCode);
                }
                //link contact to user

                $user->contact_id = $contactId;
                $user->save();
            }
            //update contact info
            $contact = Contact::find($user->contact_id);
            $this->inputContactInfo($request, $contact);
            $contact->update_date = date('Y-m-d H:i:s');
            $contact->save();

            //update user_role
            $userRole = new UserRole();

            $userRole->deleteAllRole($user->id);

            $listRole = $request->json()->get('roleId');
            $data = array();
            foreach($listRole as $roleId){
                $temp = array('user_id'=>$user->id, 'role_id'=> $roleId);
                array_push($data,$temp);
            }

            $userRole->insert($data); // Query Builder

            //update user channel base on role. First delete all channel user belong to except user channel. Then link list channel to user
            $channelUser = new ChannelUser();
            $channelUser->updateChannelUser($user->id,$listRole);

            //update permission
            $permissions = $request->json()->get('permissions');
            if (isset($permissions) && is_array($permissions)) {
                $apiPermission = array(Permissions::PERMISSION_EDIT);
                $valuationResult = Helper::valuatePermission($userLogged->id, $apiPermission);
                if(!$valuationResult){
                    return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
                }
                //remove user permission
                DB::statement("DELETE From user_permission WHERE user_id = $userId");
                //insert user permission
                foreach ($permissions as $p) {
                    if (Permission::find($p)) {
                        $userPermission = new UserPermission();
                        $userPermission->user_id = $userId;
                        $userPermission->permission_id = $p;
                        $userPermission->is_enabled = true;
                        $userPermission->creator_id = $user->id;
                        $userPermission->updater_id = $user->id;
                        $userPermission->save();
                    } else {
                        \LOG::warn("--> 999 There is no permission_id[$p]");
                    }
                }
            }
            
            $rangeFrom = $request->json()->get('range_from');
            $rangeTo = $request->json()->get('range_to');
            $include = $request->json()->get('include');
            $exclude = $request->json()->get('exclude');
            //update data access
            $dataAccess = DataAccess::getByUserId($userId);
            if(!$dataAccess){
                $dataAccess = new DataAccess();
                $dataAccess->user_id = $userId;
                $dataAccess->type = 2;
            }else
                $dataAccess = DataAccess::find($dataAccess->id);
            $dataAccess->range_from = $rangeFrom;
            $dataAccess->range_to = $rangeTo;
            $dataAccess->include = $include;
            $dataAccess->exclude = $exclude;
            $dataAccess->save();

            $statusCode = 201;
            $responseMessage->setData('success', true);
        }
        $result = $responseMessage->getMessage();

        return $result;
    }

    /**
     * @param Request $request
     * @param ResponseMessage $responseMessage
     * @param $location_id
     * @param $company_group_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addUserAndContact(Request $request, ResponseMessage $responseMessage, $location_id, $company_group_id){
        
        $user = NULL;
        $newUser = new User();
        try {
            $user = $this->currentUser($request, [Permissions::USER_CREATE]);
        } catch(Exception $e) {
            return $this->responseError($e->getMessage());
        }

        $errorMessages = '';
        $v = Validator::make($request->all(), [
            'first_name' => 'required',

            'password' => 'required|alpha_num|min:6',
            'email' => 'required|email|unique:mysql.user,email',
            'code' => 'required|unique:mysql.user,user_code',
            'address1' => 'required',
            'suburb' => 'required',

            'country' => 'required|integer',
            'mobileNumber' => 'required',
            'privacy' => 'required',
            'gender' => 'integer',
            'state' => 'integer',
            'roleId' => 'required',
            'existingContact' => 'required',
            'range_from' => 'required',
            'range_to' => 'required',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'email' => ErrorCodes::INVALID,
            'unique' => ErrorCodes::UNIQUE,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'min' => ErrorCodes::LENGTH_MIN_INVALID,
            'integer' => ErrorCodes::INTEGER
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
        }
        if($request->json()->has('permission')){
            $v = Validator::make($request->all(), [
                'permissions' => 'array',
            ], [
                'array' => ErrorCodes::ARRAY_TYPE,
            ]);
            if ($v->fails()) {
                $errorMessages = $v->errors()->getMessages();
            }
        }

        if ($errorMessages) {
            $statusCode = 400;
            $responseMessage->setErrorCode(ErrorCodes::INVALID_INPUT);
            $responseMessage->setErrorDetail($errorMessages);
        } else {

            try{
                $existingContact = $request->json()->get('existingContact');

                //map data to create new user
                $password = $request->json()->get('password');
                
                $code = $request->json()->get('code');

                $newUser->user_code = $code;
                $newUser->active = 1;
                $newUser->suspend = 0;
                $newUser->cause_of_concern = 0;
                $newUser->location_id = $location_id;

                $newUser->email = $request->json()->get('email');
                $newUser->password = Helper::hash($password);
                $newUser->token = Helper::generateGuid();
                $newUser->change_password_token = NULL;
                $newUser->login_counter = 0;
                $newUser->create_date = date('Y-m-d H:i:s');
                $newUser->update_date = date('Y-m-d H:i:s');
                $newUser->company_group_id = $company_group_id;

                if($existingContact){
                    // contact exist, check id if contact is not link to any user.
                    $contactId = $request->json()->get('existContactId');
                    $contact = Contact::find($contactId);
                    if(!$contact){
                        $statusCode = 400;
                        $responseMessage->setErrorCode(ErrorCodes::CONTACT_NOT_FOUND);

                        $result = $responseMessage->getMessage();
                        return response()->json($result, $statusCode);
                    }
                    // check if contact already link to user
                    $checkContactNotLinkToOtherUser = $newUser->validateContactAvailable($contactId);

                    if($checkContactNotLinkToOtherUser && $checkContactNotLinkToOtherUser->count >0){
                        $statusCode = 400;
                        $responseMessage->setErrorCode(ErrorCodes::CONTACT_ALREADY_LINKED);

                        $result = $responseMessage->getMessage();
                        return response()->json($result, $statusCode);
                    }
                    //link contact to user

                    $newUser->contact_id = $contactId;
                    $newUser->save();

                    //add user to channel company
                    $channelUser = new ChannelUser();
                    $companyId                  = Location::find($location_id)->company_id;
                    $channelUser->user_id       = $newUser->id;
                    $channelUser->channel_id    = Company::getChannelName($companyId);
                    $channelUser->save();

                }else{

                    // new contact
                    $contact = new Contact();
                    $contactEmail = $request->json()->get('email');
                    $v = Validator::make($request->all(), [
                        'email' => 'required|email|unique:mysql.contact,email',
                    ], [
                        'required' => ErrorCodes::REQUIRED,
                        'email' => ErrorCodes::INVALID,
                        'unique' => ErrorCodes::UNIQUE,
                    ]);
                    if ($v->fails()) {
                        $errorMessages = $v->errors()->getMessages();
                        $statusCode = 400;
                        $responseMessage->setErrorCode(ErrorCodes::INVALID_INPUT);
                        $responseMessage->setErrorDetail($errorMessages);
                        $result = $responseMessage->getMessage();

                        return response()->json($result, $statusCode);
                    }


                    $this->inputContactInfo($request, $contact);

                    $contact->company_group_id = $company_group_id;

                    $contact->status_code = Contact::STATUS_CODE_ACTIVE;

                    $contact->create_date = date('Y-m-d H:i:s');
                    $contact->update_date = date('Y-m-d H:i:s');

                    $contact->save();

                    $newUser->contact_id = $contact->id;

                    $newUser->save();
                }
                $user_role = new UserRole();

                $listRole = $request->json()->get('roleId');
                $data = array();
                foreach($listRole as $roleId){
                    $temp = array('user_id'=>$newUser->id, 'role_id'=> $roleId);
                    array_push($data,$temp);
                }
                $user_role->insert($data); // Query Builder

                //add user to channel base on role. By default, user will be added to user channel
                $channel = new Channel();

                $channelInfos = $channel->getByTypeAndRole('user',$listRole);

                if($channelInfos){
                    $channelUser = new ChannelUser();
                    $prepareDataForInsert = array();
                    foreach ($channelInfos as $info){
                        $temp = array('user_id'=>$newUser->id, 'channel_id'=> $info->id);
                        array_push($prepareDataForInsert,$temp);

                    }
                    $channelUser->insert($prepareDataForInsert);
                }

                //insert user permission
                $permissions = $request->json()->get('permissions') ? $request->json()->get('permissions') : [];
                $userId = $newUser->id;
                foreach ($permissions as $p) {
                    if (Permission::find($p)) {
                        $userPermission = new UserPermission();
                        $userPermission->user_id = $userId;
                        $userPermission->permission_id = $p;
                        $userPermission->is_enabled = true;
                        $userPermission->save();
                    } else {
                        \LOG::warn("There is no permission_id[$p]");
                    }
                }
                //insert data access
                $rangeFrom = $request->json()->get('range_from');
                $rangeTo = $request->json()->get('range_to');
                $include = $request->json()->get('include');
                $exclude = $request->json()->get('exclude');

                $dataAccess = new DataAccess();
                $dataAccess->user_id = $userId;
                $dataAccess->type = DataAccess::LOCATION_TYPE;
                $dataAccess->range_from = $rangeFrom;
                $dataAccess->range_to = $rangeTo;
                $dataAccess->include = $include;
                $dataAccess->exclude = $exclude;
                $dataAccess->save();


                $statusCode = 201;
                $responseMessage->setData('success', true);
                $responseMessage->setData('userId',$newUser->id);

                $this->createDefaultNotificationMailConfig($newUser->id);
            }catch (\Exception $ex){
                \LOG::error("Error in create user " . $ex);
                $statusCode = 201;
                $responseMessage->setData('success', false);
            }

        }
        $result = $responseMessage->getMessage();

        return response()->json($result, $statusCode);
    }

    /**
     * @param $userId
     */
    public function createDefaultNotificationMailConfig($userId) {
        foreach (Event::all() as $event) {
            $currentConfig = ConfigEmailNotification::where([
                ['user_id', '=', $userId],
                ['event', '=', $event->code]
            ])->first();

            if ($currentConfig != NULL) {
                echo "~~~$currentConfig~~~";
                continue;
            }
            echo 'create_new';
            $config             = new ConfigEmailNotification();
            $config->user_id    = $userId;
            $config->event      = $event->code;
            $config->config     = ConfigEmailNotification::CONFIG_BOTH;
            $config->save();
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * @api {post} /users/logout
     * @apiName Logout
     * @apiGroup User
     * @apiHeader {json} content type and token of Logged in users .
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "Content-type": "application/json"
     *       "token" : "BTgMAeJ5UKp4t8dr9jw3bjZhWbMVW33m2Klm6KTe"
     *     }
     *
     * @apiSuccess {Object} UserDetails
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     * []
     */

    public function logout(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;
        if ($token) {
            $user = User::where('token', $token)->first();
        }
        $activity               = new Activity();
        $activity->type         = Activity::TYPE_LOGOUT;
        $activity->source_id    = $user->id;
        $activity->create_date  = date("Y-m-d H:i:s");
        $activity->save();
        return $this->responseData([]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminLogin(Request $request)
    {
        $v = Validator::make($request->all(), [
            'email' => 'required|email|exists:mysql.user,email',
            'password' => 'required|alpha_num|min:6',
        ], [
            'email.required' => ErrorCodes::REQUIRED,
            'email.email' => ErrorCodes::INVALID,
            'email.exists' => ErrorCodes::NOT_EXISTS,
            'password.required' => ErrorCodes::REQUIRED,
            'password.alpha_num' => ErrorCodes::ALPHA_NUM,
            'password.min' => ErrorCodes::LENGTH_MIN_INVALID,
        ]);
        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $userModel = new User();

        $email = $request->json()->get('email');
        $password = $request->json()->get('password');
        $user = $userModel->getLoginInfo($email);

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID, $v->errors()->getMessages());
        }

        if (!$user->active) {
            return $this->responseError(ErrorCodes::INACTIVE, $v->errors()->getMessages());
        }
        if ($user->suspend) {
            \LOG::debug('$user->suspend = true');
            return $this->responseError(ErrorCodes::SUSPEND, $v->errors()->getMessages());
        }

        if (crypt($password, $user->password) == $user->password) {
            DB::table('user')
                ->where('email', $email)
                ->update(['login_counter' => 0]);

            unset($user->active);
            unset($user->suspend);
            unset($user->login_counter);
            unset($user->password);

//            if(!$this->checkCompanyStatus($user->id)){
//                return $this->responseError(ErrorCodes::SUSPEND);
//            }
            
            $user->has_quick_code = property_exists($user, 'quick_code') && $user->quick_code ? 1 : 0;

            unset($user->quick_code);

            $user->roles = [];
            $roles = DB::select("SELECT DISTINCT(id) FROM `role` r INNER JOIN `user_role` ur ON r.id=ur.role_id WHERE ur.user_id=:user_id", ['user_id' => $user->id]);
            foreach ($roles as $role) {
                array_push($user->roles, $role->id);
            }

//            if (!(in_array(7, $user->roles))) {
//                \LOG::info("User[$user->id] doesn't have super_admin role => access_denied");
//                return $this->responseError(ErrorCodes::ACCESS_DENIED);
//            }

            if ($user->company_group_id != 1) {
                \LOG::info("User[$user->id] doesn't belong to this group => access_denied");
                return $this->responseError(ErrorCodes::ACCESS_DENIED);
            }

            $permissions = DB::select("
                        SELECT * from permission WHERE id in (
                            SELECT id FROM permission p RIGHT JOIN user_permission up on p.id=up.permission_id WHERE up.user_id=?
                        ) OR id in (
                            SELECT rp.permission_id 'id' FROM role_permission rp RIGHT JOIN user_role ur on rp.role_id=ur.role_id WHERE ur.user_id=?
                        )
                    ", [$user->id, $user->id]);
            $user->permissions = [];
            foreach ($permissions as $permission) {
                array_push($user->permissions, $permission->name);
            }
            //get user menu access
            $menuList = User::getUserMenuAccessById($user->id);
            $user->menu = $menuList;

            $activity               = new Activity();
            $activity->type         = Activity::TYPE_LOGIN;
            $activity->source_id    = $user->id;
            $activity->create_date  = date("Y-m-d H:i:s");
            $activity->save();

            return $this->responseData($user);
        }

        $MAX_ATTEMPT = 5;

//        ++$user->login_counter;
//        $user->suspend = $user->login_counter >= $MAX_ATTEMPT;
//        $user->save();
        DB::table('user')
            ->where('email', $email)
            ->update([
                'login_counter' => (++$user->login_counter),
                'suspend' => ($user->login_counter >= $MAX_ATTEMPT)
            ]);

        if ($user->login_counter >= $MAX_ATTEMPT) {
            \LOG::debug('$user->login_counter >= $MAX_ATTEMPT');
            return $this->responseError(ErrorCodes::SUSPEND);
        }

        return $this->responseError(ErrorCodes::INVALID);
    }
    /**
     * @api {post} /users/login  Login
     * @apiName userLogin
     * @apiGroup User
     *
     * @apiParam {String} email Users email address.
     * @apiParam {String} password Users password.
     *
     * @apiSuccess {Object} UserDetails
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *    "id": 2,
     *     "title": null,
     *    "gender": 0,
     *     "first_name": "Super Admin",
     *     "last_name": "1",
     *    "status_code": 2,
     *    "privacy_flag": 3,
     *    "privacy_flag_update_time": "2019-07-09 16:13:42",
     *    "company_name": "",
     *    "company_type": null,
     *    "ABN": null,
     *    "ACN": null,
     *    "tax_type": null,
     *    "address1": "435 hoang van thu tan binh hcm",
     *    "address2": "",
     *    "suburb": "700000",
     *    "state": 36,
     *    "postcode": "",
     *    "country": 13,
     *    "postal_address1": null,
     *    "postal_address2": null,
     *    "postal_suburb": null,
     *    "postal_state": null,
     *    "postal_postcode": null,
     *    "postal_country": null,
     *    "email": "test@siliconstack.com.au",
     *    "email_alternate": null,
     *    "mobile_phone": "12345678",
     *    "home_phone": null,
     *    "work_phone": null,
     *    "fax_number": null,
     *    "preferred_contact_method": null,
     *    "driver_license_number": null,
     *    "dealer_license_number": null,
     *    "fleet_number": null,
     *    "bank_branch_name": null,
     *    "bank_account_name": null,
     *    "bank_bsb_number": null,
     *    "bank_account_number": null,
     *    "note": "",
     *    "salesperson_id": 0,
     *    "updater_id": null,
     *    "prospect_level": null,
     *    "create_date": "2017-03-03 03:03:23",
     *    "update_date": "2019-07-09 16:13:42",
     *    "interested_vehicle": null,
     *    "company_group_id": 1,
     *    "contact_id": 1,
     *    "token": "BTgMAeJ5UKp4t8dr9jw3bjZhWbMVW33m2Klm6KTe",
     *    "location_id": 1,
     *    "need_accept_term_and_condition": 0,
     *    "has_quick_code": 1,
     *    "roles": [
     *    " Super Admin"
     *    ],
     *    "permissions": [
     *    "vehicle_trade",
     *    "company_create",
     *    "company_edit",
     *    "company_get",
     *    "system_config",
     *    "user_admin_update",
     *    "user_admin_get",
     *    "system_admin_get",
     *    "company_delete",
     *    "system_role_create",
     *    "system_role_edit",
     *    "system_role_delete",
     *    "auction_session_create",
     *    "auction_session_edit",
     *    "auction_session_delete",
     *    "auction_setting_edit",
     *    "auction_item_approve",
     *    "dispute_resolve",
     *    "debit_note_create",
     *    "credit_note_create",
     *    "valuation_get",
     *    "valuation_cancel",
     *    "user_create",
     *    "user_update",
     *    "user_get",
     *    "contact_create",
     *    "contact_get",
     *    "contact_update",
     *    "vehicle_edit",
     *    "vehicle_get",
     *    "permission_get",
     *    "permission_edit",
     *    "role_get",
     *    "role_create",
     *    "role_edit",
     *    "location_create",
     *    "location_get",
     *    "location_edit",
     *    "permission_admin_get",
     *    "auction_session_view",
     *    "location_delete",
     *    "user_delete",
     *    "vehicle_photo_edit",
     *    "contact_delete",
     *    "payment_request_create",
     *    "payment_request_view",
     *    "payment_receipt_upload",
     *    "debit_note_view",
     *    "credit_note_view",
     *    "invoice_view",
     *    "invoice_upload",
     *    "custom_role_delete",
     *    "dispute_info_view",
     *    "permission_name_edit",
     *    "auction_item_listing_view",
     *    "watch_list_create",
     *    "watch_list_delete",
     *    "transaction_view",
     *    "menu_get",
     *    "menu_set",
     *    "vehicle_create",
     *    "vehicle_update",
     *    "valuation_create",
     *    "valuation_edit",
     *    "valuation_perform",
     *    "valuation_approve",
     *    "valuation_reprice",
     *    "valuation_delete",
     *    "external_group_create",
     *    "external_group_get",
     *    "external_group_edit",
     *    "external_offer_bid",
     *    "external_offer_accept",
     *    "external_offer_edit",
     *    "external_offer_create",
     *    "ppsr_get",
     *    "external_group_delete",
     *    "external_offer_view",
     *    "external_offer_discard",
     *    "send_to_auction",
     *    "wish_list_create",
     *    "wish_list_edit",
     *    "wish_list_delete",
     *    "bid_on_vehicle",
     *    "vehicle_photo_add",
     *    "vehicle_accept",
     *    "dispute_lodge",
     *    "test_drive_create ",
     *    "test_drive_view",
     *    "test_drive_edit",
     *    "auction_item_edit",
     *    "dispute_create",
     *    "wish_list_view",
     *    "dealer_settings_edit",
     *    "data_access_edit",
     *    "data_access_get",
     *    "wholesale_value_update",
     *    "sales_info_update",
     *    "user_suspend"
     *     ],
     *    "menu": [
     *    {
     *    "site_menu": "mo_admin",
     *    "group": [
     *     {
     *    "group_name": "Members",
     *     "menu_items": [
     *    {
     *      "code": 1,
     *     "name": "Dealer Groups",
     *     "group_name": "Members",
     *     "site_menu": "mo_admin"
     *     },
     *     {
     *    "code": 2,
     *    "site_menu": "mo_admin"
     *    },
     *    {
     *    "code": 3,
     *    "name": "Location",
     *    "group_name": "Members",
     *    "site_menu": "mo_admin"
     *     }
     *     ]
     *     },
     *     {
     *    "group_name": "Users",
     *    "menu_items": [
     *    {
     *    "code": 4,
     *    "name": "Statistics",
     *    "group_name": "Users",
     *    "site_menu": "mo_admin"
     *     },
     *     {
     *    "code": 5,
     *    "name": "Manage",
     *    "group_name": "Users",
     *    "site_menu": "mo_admin"
     *    },
     *    {
     *    "code": 6,
     *    "name": "Roles",
     *    "group_name": "Users",
     *    "site_menu": "mo_admin"
     *    },
     *     {
     *    "code": 7,
     *    "name": "Permissions",
     *    "group_name": "Users",
     *    "site_menu": "mo_admin"
     *     }
     *     ]
     *     },
     *     {
     *    "group_name": "SETTINGS",
     *    "menu_items": [
     *     {
     *    "code": 8,
     *    "name": "Valuation System",
     *    "group_name": "SETTINGS",
     *    "site_menu": "mo_admin"
     *    },
     *     {
     *    "code": 9,
     *    "name": "External Offering System",
     *    "group_name": "SETTINGS",
     *    "site_menu": "mo_admin"
     *    },
     *    {
     *    "code": 10,
     *    "name": "Auction System",
     *    "group_name": "SETTINGS",
     *    "site_menu": "mo_admin"
     *    },
     *    {
     *    "code": 41,
     *    "name": "Fees",
     *    "group_name": "SETTINGS",
     *    "site_menu": "mo_admin"
     *    },
     *    {
     *    "code": 45,
     *    "name": "Rating system",
     *    "group_name": "SETTINGS",
     *    "site_menu": "mo_admin"
     *    },
     *    {
     *    "code": 50,
     *    "name": "General",
     *    "group_name": "SETTINGS",
     *    "site_menu": "mo_admin"
     *     }
     *     ]
     *     },
     *    {
     *    "group_name": "AUCTION",
     *    "menu_items": [
     *    {
     *    "code": 11,
     *    "name": "Auction sessions",
     *    "group_name": "AUCTION",
     *    "site_menu": "mo_admin"
     *    },
     *     {
     *    "code": 12,
     *    "name": "Request List",
     *    "group_name": "AUCTION",
     *    "site_menu": "mo_admin"
     *    },
     *     {
     *    "code": 13,
     *    "name": "Auction Listings",
     *    "group_name": "AUCTION",
     *    "site_menu": "mo_admin"
     *     },
     *     {
     *    "code": 14,
     *    "name": "Live Auction",
     *    "group_name": "AUCTION",
     *    "site_menu": "mo_admin"
     *    },
     *    {
     *    "code": 51,
     *    "name": "Auction history",
     *    "group_name": "AUCTION",
     *    "site_menu": "mo_admin"
     *    }
     *    ]
     *    },
     *     {
     *    "group_name": "DISPUTE CENTRE",
     *    "menu_items": [
     *    {
     *    "code": 15,
     *    "name": "Current",
     *    "group_name": "DISPUTE CENTRE",
     *    "site_menu": "mo_admin"
     *    },
     *     {
     *    "code": 16,
     *    "name": "Resolved",
     *    "group_name": "DISPUTE CENTRE",
     *    "site_menu": "mo_admin"
     *     }
     *    ]
     *     },
     *     {
     *    "group_name": "TRANSACTION",
     *    "menu_items": [
     *     {
     *    "code": 39,
     *    "name": "Current",
     *    "group_name": "TRANSACTION",
     *    "site_menu": "mo_admin"
     *    },
     *     {
     *    "code": 40,
     *    "name": "Complete",
     *    "group_name": "TRANSACTION",
     *    "site_menu": "mo_admin"
     *    }
     *    ]
     *    },
     *    {
     *    "group_name": "VALUATION",
     *    "menu_items": [
     *    {
     *    "code": 46,
     *    "name": "Requests",
     *    "group_name": "VALUATION",
     *    "site_menu": "mo_admin"
     *     },
     *    {
     *    "code": 47,
     *    "name": "In Progress",
     *    "group_name": "VALUATION",
     *    "site_menu": "mo_admin"
     *    },
     *    {
     *    "code": 48,
     *    "name": "Pending Approval",
     *    "group_name": "VALUATION",
     *    "site_menu": "mo_admin"
     *    },
     *    {
     *    "code": 49,
     *    "name": "Complete",
     *    "group_name": "VALUATION",
     *    "site_menu": "mo_admin"
     *     }
     *    ]
     *    }
     *     ]
     *    },
     *    {
     *    "site_menu": "dealership",
     *    "group": [
     *    {
     *    "group_name": "CONFIGURE",
     *    "menu_items": [
     *     {
     *    "code": 17,
     *    "name": "Locations",
     *    "group_name": "CONFIGURE",
     *    "site_menu": "dealership"
     *     },
     *     {
     *    "code": 18,
     *    "name": "Roles",
     *    "group_name": "CONFIGURE",
     *    "site_menu": "dealership"
     *    },
     *     {
     *    "code": 19,
     *    "name": "Permissions",
     *    "group_name": "CONFIGURE",
     *    "site_menu": "dealership"
     *     },
     *    {
     *    "code": 20,
     *    "name": "Users",
     *    "group_name": "CONFIGURE",
     *    "site_menu": "dealership"
     *     },
     *     {
     *    "code": 21,
     *    "name": "Settings",
     *    "group_name": "CONFIGURE",
     *    "site_menu": "dealership"
     *    },
     *     {
     *    "code": 44,
     *    "name": "External Offer Group",
     *    "group_name": "CONFIGURE",
     *    "site_menu": "dealership"
     *     }
     *    ]
     *     },
     *    {
     *    "group_name": "CUSTOMERS",
     *    "menu_items": [
     *     {
     *    "code": 22,
     *    "name": "Customers",
     *    "group_name": "CUSTOMERS",
     *    "site_menu": "dealership"
     *     }
     *    ]
     *    },
     *    {
     *    "group_name": "VALUATION",
     *    "menu_items": [
     *    {
     *    "code": 23,
     *    "name": "Requests",
     *    "group_name": "VALUATION",
     *    "site_menu": "dealership"
     *    },
     *    {
     *    "code": 24,
     *    "name": "In Progress",
     *    "group_name": "VALUATION",
     *    "site_menu": "dealership"
     *     },
     *    {
     *    "code": 25,
     *    "name": "Pending Approval",
     *    "group_name": "VALUATION",
     *    "site_menu": "dealership"
     *    },
     *     {
     *    "code": 26,
     *    "name": "Complete",
     *    "group_name": "VALUATION",
     *    "site_menu": "dealership"
     *     }
     *    ]
     *    },
     *     {
     *    "group_name": "PLACE MY CAR",
     *    "menu_items": [
     *     {
     *    "code": 27,
     *    "name": "Incoming Offers",
     *    "group_name": "PLACE MY CAR",
     *    "site_menu": "dealership"
     *     },
     *     {
     *    "code": 28,
     *    "name": "Outgoing Offers",
     *    "group_name": "PLACE MY CAR",
     *    "site_menu": "dealership"
     *     }
     *     ]
     *    },
     *     {
     *    "group_name": "TEST DRIVES",
     *    "menu_items": [
     *    {
     *    "code": 29,
     *    "name": "In Progress",
     *    "group_name": "TEST DRIVES",
     *    "site_menu": "dealership"
     *     },
     *    {
     *    "code": 30,
     *    "name": "Complete",
     *    "group_name": "TEST DRIVES",
     *    "site_menu": "dealership"
     *     }
     *     ]
     *    },
     *    {
     *    "group_name": "AUCTION",
     *    "menu_items": [
     *    {
     *    "code": 31,
     *    "name": "Stock List",
     *    "group_name": "AUCTION",
     *    "site_menu": "dealership"
     *     },
     *     {
     *    "code": 32,
     *    "name": "Auction Listings",
     *    "group_name": "AUCTION",
     *    "site_menu": "dealership"
     *     },
     *     {
     *    "code": 33,
     *    "name": "Live Auction",
     *    "group_name": "AUCTION",
     *    "site_menu": "dealership"
     *    },
     *     {
     *    "code": 34,
     *    "name": "Wish List",
     *    "group_name": "AUCTION",
     *    "site_menu": "dealership"
     *     }
     *     ]
     *     },
     *    {
     *    "group_name": "DISPUTE CENTRE",
     *    "menu_items": [
     *     {
     *    "code": 35,
     *    "name": "Current",
     *    "group_name": "DISPUTE CENTRE",
     *    "site_menu": "dealership"
     *    },
     *    {
     *    "code": 36,
     *    "name": "Resolved",
     *    "group_name": "DISPUTE CENTRE",
     *    "site_menu": "dealership"
     *     }
     *     ]
     *     },
     *    {
     *    "group_name": "TRANSACTION",
     *    "menu_items": [
     *    {
     *    "code": 37,
     *    "name": "Current",
     *    "group_name": "TRANSACTION",
     *    "site_menu": "dealership"
     *     },
     *     {
     *    "code": 38,
     *    "name": "Complete",
     *    "group_name": "TRANSACTION",
     *    "site_menu": "dealership"
     *     }
     *    ]
     *     },
     *     {
     *    "group_name": "DASHBOARD",
     *    "menu_items": [
     *     {
     *    "code": 42,
     *    "name": "Dashboard",
     *    "group_name": "DASHBOARD",
     *    "site_menu": "dealership"
     *     }
     *    ]
     *    },
     *    {
     *    "group_name": "ARRIVING VEHICLE",
     *    "menu_items": [
     *    {
     *    "code": 43,
     *    "name": "Arriving Vehicle",
     *    "group_name": "ARRIVING VEHICLE",
     *    "site_menu": "dealership"
     *    "}
     *    "]
     *    "}
     *    "]
     *    "}
     *    "],
    "company_id": 1
     *    "}
     *
     *
     */

    public function login(Request $request)
    {
        \LOG::info((string)$request);
        $user = null;
        $responseMessage = new ResponseMessage();
        $errorMessages = '';
        $v = Validator::make($request->all(), [
            'email' => 'required|email|exists:mysql.user,email',
            'password' => 'required|alpha_num|min:6',
        ], [
            'email.required' => ErrorCodes::REQUIRED,
            'email.email' => ErrorCodes::INVALID,
            'email.exists' => ErrorCodes::NOT_EXISTS,
            'password.required' => ErrorCodes::REQUIRED,
            'password.alpha_num' => ErrorCodes::ALPHA_NUM,
            'password.min' => ErrorCodes::LENGTH_MIN_INVALID,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
        }
        if ($errorMessages) {
            $statusCode = 400;
            $responseMessage->setErrorCode(ErrorCodes::INVALID_INPUT);
            $responseMessage->setErrorDetail($errorMessages);
        }
        else {
            $requestData = $request->json()->all();
           // dd($requestData);
            $email = $requestData['email'];;
            $password = $requestData['password'];
            $userModel = new User();
            $user = $userModel->getLoginInfo($email);

            $company_group = CompanyGroup::find($user->company_group_id);

            if (!$company_group || $company_group->status_code !== CompanyGroup::STATUS_ACTIVE) {
                return $this->responseError(ErrorCodes::SUSPEND, 'The Company Group has been suspended');
            }

            if ($user) {
                if (!$user->active) {
                    $statusCode = 400;
                    $responseMessage->setErrorCode(ErrorCodes::INACTIVE);
                } else if ($user->suspend) {
                    $statusCode = 400;
                    $responseMessage->setErrorCode(ErrorCodes::SUSPEND);
                } else {
                    if (crypt($password, $user->password) == $user->password) {
                        $statusCode = 200;

                        DB::table('user')
                            ->where('email', $email)
                            ->update(['login_counter' => 0]);
                        unset($user->active);
                        unset($user->suspend);
                        unset($user->login_counter);
                        unset($user->password);

                        if(!$this->checkCompanyStatus($user->id)){
                            $statusCode = 400;
                            $responseMessage->setErrorCode(ErrorCodes::SUSPEND);
                            $result = $responseMessage->getMessage();
                            return response()->json($result, $statusCode);
                        }

                        $user->has_quick_code = property_exists($user, 'quick_code') && $user->quick_code ? 1 : 0;

                        unset($user->quick_code);

                        $user->roles = [];
                        $roles = DB::select("SELECT DISTINCT(name) FROM `role` r INNER JOIN `user_role` ur ON r.id=ur.role_id WHERE ur.user_id=:user_id", ['user_id' => $user->id]);
                        foreach ($roles as $role) {
                            array_push($user->roles, $role->name);
                        }
                        //get user permission
                        $permissions = DB::select("
                            SELECT * from permission WHERE id in (
                                SELECT id FROM permission p RIGHT JOIN user_permission up on p.id=up.permission_id WHERE up.user_id=?
                            ) OR id in (
                                SELECT rp.permission_id 'id' FROM role_permission rp RIGHT JOIN user_role ur on rp.role_id=ur.role_id WHERE ur.user_id=?
                            )
                        ", [$user->id, $user->id]);
                        $user->permissions = [];
                        foreach ($permissions as $permission) {
                            array_push($user->permissions, $permission->name);
                        }

                        //get user menu access
                        $menuList = User::getUserMenuAccessById($user->id);
                        $user->menu = $menuList;
                        if ($user->location_id) {
                            $location = Location::find($user->location_id);
                            if ($location) {
                                $user->company_id = $location->company_id;
                            }
                        }

                        $responseMessage->setObjectData($user);

                        $activity               = new Activity();
                        $activity->type         = Activity::TYPE_LOGIN;
                        $activity->source_id    = $user->id;
                        $activity->create_date  = date("Y-m-d H:i:s");

                        $activity->save();

                    } else {
                        $suspend = 0;
                        $maxAttempt = 5;
                        $loginCounter = $user->login_counter;
                        $loginCounter++;
                        if ($loginCounter == $maxAttempt) {
                            $suspend = 1;
                            $responseMessage->setErrorCode(ErrorCodes::SUSPEND);
                        } else {
                            $responseMessage->setErrorCode(ErrorCodes::INCORRECT_PASSWORD);
                            $responseMessage->setErrorMessage($loginCounter, "login_attempts");
                        }
                        DB::table('user')
                            ->where('email', $email)
                            ->update([
                                'login_counter' => $loginCounter,
                                'suspend' => $suspend
                            ]);
                        $statusCode = 400;

                    }
                }

            } else {
                $statusCode = 404;
                $responseMessage->setErrorCode(ErrorCodes::NOT_FOUND);
            }
        }

        $result = $responseMessage->getMessage();
        return response()->json($result, $statusCode);
    }

    /**
     * @param $userId
     * @return bool
     */
    public function checkCompanyStatus($userId){
        //link to company to check if company status is active. If not,return access deny
        $checkCompStatus = DB::select("SELECT company.status_code 
                                    FROM user 
                                    INNER JOIN location ON user.location_id = location.id 
                                    INNER JOIN company ON location.company_id = company.id 
                                    WHERE user.id=:user_id",
            ['user_id' => $userId]);
        foreach ($checkCompStatus as $company){
            if($company->status_code == Company::STATUS_INACTIVE){
                \Log::info("Company status is inactive. User Access Deny");
                return false;
            }
        }

        return true;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function doesEmailExist(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::USER_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $v = Validator::make($request->all(), [
            'email' => 'required|min:1'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $user = new User();
        $u = $user->getLoginInfo($request->json()->get('email'));
        return $this->responseData(['exists' => $u != null]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMyRoles(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $result = [];
        $roles = DB::select("SELECT DISTINCT(name) FROM `role` r INNER JOIN `user_role` ur ON r.id=ur.role_id WHERE ur.user_id=:user_id", ['user_id' => $user->id]);
        foreach ($roles as $role) {
            array_push($result, $role->name);
        }
        return $this->responseData($result);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMyPermissions(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $result = [];
        $permissions = User::getPermission($user->id);

        foreach ($permissions as $permission) {
            array_push($result, $permission->name);
        }
        return $this->responseData($result);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPassword(Request $request)
    {
        $responseMessage = new ResponseMessage();
        $errorMessages = '';
        $v = Validator::make($request->all(), [
            'email' => 'required|email|exists:mysql.user,email',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'email' => ErrorCodes::INVALID,
            'exists' => ErrorCodes::NOT_EXISTS
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
        }
        if ($errorMessages) {
            $statusCode = 400;
            $responseMessage->setErrorCode(ErrorCodes::INVALID_INPUT);
            $responseMessage->setErrorDetail($errorMessages);
        } else {
            $email = $request->json()->get('email');
            Log::info("Reset email: $email");
            $userModel = new User();
            $user = $userModel->getContactFields($email, ['u.active', 'u.suspend', 'c.first_name', 'c.last_name']);
            if ($user) {
                if (!$user->active) {
                    $statusCode = 400;
                    $responseMessage->setErrorCode(ErrorCodes::INACTIVE);
                } else if ($user->suspend) {
                    $statusCode = 400;
                    $responseMessage->setErrorCode(ErrorCodes::SUSPEND);
                } else {
                    do {
                        $passwordToken = Helper::generateGuid();
                        $tokenUser = User::where('change_password_token', $passwordToken)->first();
                    } while ($tokenUser);

                    DB::table('user')
                        ->where('email', $email)
                        ->update(['change_password_token' => $passwordToken]);

                    $fullname = $user->first_name . ' ' . $user->last_name;
                    $resetLink = rtrim(env('APP_FRONTEND_URL'), '/') . '/reset-password/' . $passwordToken;

                    try {
                        $client = new Client(['base_uri' => env('APP_MAIL_URL')]);
                        $response = $client->post('users/sendForgotPasswordEmail', [
                            'json' => [
                                'to_email' => $email,
                                'fullname' => $fullname,
                                'reset_link' => $resetLink,
                            ]
                        ]);
                        $statusCode = 200;
                        $responseMessage->setData('success', TRUE);
                    } catch (ClientException $e) {
                        $response = $e->getResponse();
                        $bodyResponse = json_decode($response->getBody()->getContents(), true);
                        $statusCode = 400;
                        if (isset($bodyResponse['error']['code'])) {
                            $responseMessage->setErrorCode($bodyResponse['error']['code']);
                            if (isset($bodyResponse['error']['detail'])) {
                                $responseMessage->setErrorDetail($bodyResponse['error']['detail']);
                            }
                        } else {
                            $responseMessage->setErrorCode(ErrorCodes::INVALID);
                        }
                    }
                }
            } else {
                $statusCode = 404;
                $responseMessage->setErrorCode(ErrorCodes::NOT_FOUND);
            }
        }
        $result = $responseMessage->getMessage();
        return response()->json($result, $statusCode);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(Request $request)
    {
        $responseMessage = new ResponseMessage();
        $errorMessages = '';
        $v = Validator::make($request->all(), [
            'password_token' => 'required|alpha_num|exists:mysql.user,change_password_token',
            'password' => 'required|alpha_num|min:6',
        ], [
            'password_token.required' => ErrorCodes::REQUIRED,
            'password_token.alpha_num' => ErrorCodes::ALPHA_NUM,
            'password_token.exists' => ErrorCodes::NOT_EXISTS,
            'password.required' => ErrorCodes::REQUIRED,
            'password.alpha_num' => ErrorCodes::ALPHA_NUM,
            'password.min' => ErrorCodes::LENGTH_MIN_INVALID,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
        }

        if ($errorMessages) {
            $statusCode = 400;
            $responseMessage->setErrorCode(ErrorCodes::INVALID_INPUT);
            $responseMessage->setErrorDetail($errorMessages);
        } else {
            $passwordToken = $request->json()->get('password_token');
            $password = $request->json()->get('password');
            $user = User::where('change_password_token', $passwordToken)->first();
            if ($user) {
                $user->password = Helper::hash($password);
                $user->change_password_token = NULL;
                $user->save();
                $statusCode = 200;
                $responseMessage->setData('success', TRUE);
            } else {
                $statusCode = 404;
                $responseMessage->setErrorCode(ErrorCodes::NOT_FOUND);
            }
        }
        $result = $responseMessage->getMessage();
        return response()->json($result, $statusCode);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMyPassword(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'password' => 'required|alpha_num|min:6',
        ], [
            'password_token.required' => ErrorCodes::REQUIRED,
            'password_token.alpha_num' => ErrorCodes::ALPHA_NUM,
            'password_token.exists' => ErrorCodes::NOT_EXISTS,
            'password.required' => ErrorCodes::REQUIRED,
            'password.alpha_num' => ErrorCodes::ALPHA_NUM,
            'password.min' => ErrorCodes::LENGTH_MIN_INVALID,
        ]);
        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $password = $request->json()->get('password');
        $user->password = Helper::hash($password);
        $user->save();

        return $this->responseData([]);
    }

    /**
     * @param Request $request
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChannel(Request $request, $userId)
    {
        $token = $request->header("token");
        $validationData = [
            'token' => $token,
            'userId' => $userId,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'userId' => 'required|numeric|exists:mysql.user,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $userModel = new User();
        $channels = $userModel->getChannel($userId);

        if (!$channels) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        return $this->responseData($channels);
    }

    /**
     * @param Request $request
     * @param $contactId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChannelByContactId(Request $request, $contactId)
    {
        $token = $request->header("token");
        $validationData = [
            'token' => $token,
            'contactId' => $contactId,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'contactId' => 'required|numeric|exists:mysql.contact,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $userModel = new User();
        $channels = $userModel->getChannelByContactId($contactId);

        if (!$channels) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        $user = User::where('contact_id', $contactId)->first();
        if (!$user->location_id) {
            return $this->responseError(ErrorCodes::LOCATION_NOT_FOUND);
        }
        $channelList = [];
        foreach ($channels as $channel) {
            $obj  = new \stdClass();

            if ($channel->type == 'role') {
                // Attach location id to channel
                $obj->name = Helper::channelName($channel->name, $channel->type, $user->location_id);
            } else if ($channel->type == 'company') {
                $obj->name = $channel->name;
            } else {
                // Attach user id to channel
                $obj->name = Helper::channelName($channel->name, $channel->type, $user->id);
            }

            $channelList[] = $obj;
        }
        return $this->responseData($channelList);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setQuickCode(Request $request)
    {
        $token = $request->header("token");

        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'quick_code' => 'required'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $user->quick_code = Hash::make($request->json()->get('quick_code'));

        $user->save();

        return $this->responseData([
            'email' => $user->email,
            'user_id' => $user->id,
            'contact_id' => $user->contact_id
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function isCorrectQuickCode(Request $request)
    {
        $v = Validator::make($request->all(), [
            'quick_code' => 'required',
            'email' => 'required'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $input = $request->json();
        $email = $input->get('email');
        $quick_code = $input->get('quick_code');

        $user = User::where('email', $email)->first();

        if (!$user) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        if (Hash::check($quick_code, $user->quick_code)) {
            return $this->responseData([
                'email' => $user->email,
                'user_id' => $user->id,
                'contact_id' => $user->contact_id
            ]);
        }

        return $this->responseError(ErrorCodes::INVALID);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUser(Request $request , $id){
        $token = $request->header("token");

        $userLogged = NULL;

        if ($token) {
            $userLogged = User::where('token', $token)->first();
        }

        if (!$userLogged) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $responseMessage = new ResponseMessage();

        $apiPermission = array(Permissions::USER_UPDATE);
        $valuationResult = Helper::valuatePermission($userLogged->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $errorMessages = '';
        $v = Validator::make($request->all(), [
            'first_name' => 'required',

            'password' => 'alpha_num|min:6',
            'code' => 'required',
            'address1' => 'required',
            'suburb' => 'required',

            'country' => 'required|integer',
            'mobileNumber' => 'required',
            'privacy' => 'required',
            'gender' => 'integer',
            'state' => 'integer',
            'roleId' => 'required',

        ], [
            'required' => ErrorCodes::REQUIRED,
            'email' => ErrorCodes::INVALID,
            'unique' => ErrorCodes::UNIQUE,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'min' => ErrorCodes::LENGTH_MIN_INVALID,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
        }
        if ($errorMessages) {
            $statusCode = 400;
            $responseMessage->setErrorCode(ErrorCodes::INVALID_INPUT);
            $responseMessage->setErrorDetail($errorMessages);
        } else {

            // update user info
            $user = User::find($id);
            if(!$user){
                $statusCode = 400;
                $responseMessage->setErrorCode(ErrorCodes::USER_NOT_FOUND);
                $result = $responseMessage->getMessage();
                return response()->json($result, $statusCode);
            }
            $code = $request->json()->get('code');
            $password = $request->json()->get('password');
            if($code != $user->user_code){
                $userDTO = new User();
                $validateUserCode = $userDTO->validateUserCodeAvailable($code,$id);
                if($validateUserCode->count < 1)
                    $user->user_code = $code;
                else{
                    $statusCode = 400;
                    $responseMessage->setErrorCode(ErrorCodes::USER_CODE_DUPLICATE);
                    $result = $responseMessage->getMessage();
                    return response()->json($result, $statusCode);
                }

            }

            if($password)
                $user->password = Helper::hash($password);

            $user->update_date = date('Y-m-d H:i:s');

            $user->save();

            // if contact existing, update user to link to new contact
            $existingContact = $request->json()->get('existingContact');
            if($existingContact){
                // contact exist, check id if contact is not link to any user.
                $contactId = $request->json()->get('existContactId');
                $contact = Contact::find($contactId);
                if(!$contact){
                    $statusCode = 400;
                    $responseMessage->setErrorCode(ErrorCodes::CONTACT_NOT_FOUND);

                    $result = $responseMessage->getMessage();
                    return response()->json($result, $statusCode);
                }
                //link contact to user

                $user->contact_id = $contactId;
                $user->save();
            }
            //update contact info
            $contact = Contact::find($user->contact_id);
            $this->inputContactInfo($request, $contact);
            $contact->update_date = date('Y-m-d H:i:s');
            $contact->save();

            //update user_role
            $userRole = new UserRole();

            $userRole->deleteAllRole($user->id);

            $listRole = $request->json()->get('roleId');
            $data = array();
            foreach($listRole as $roleId){
                $temp = array('user_id'=>$user->id, 'role_id'=> $roleId);
                array_push($data,$temp);
            }

            $userRole->insert($data); // Query Builder

            //update user channel base on role. First delete all channel user belong to except user channel. Then link list channel to user
            $channelUser = new ChannelUser();
            $channelUser->updateChannelUser($user->id,$listRole);

            //update permission
            $permissions = $request->json()->get('permissions');
            if($permissions){
                $apiPermission = array(Permissions::PERMISSION_EDIT);
                $valuationResult = Helper::valuatePermission($userLogged->id, $apiPermission);
                if(!$valuationResult){

                    return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
                }

                //remove user permission
                DB::statement("DELETE From user_permission WHERE user_id = $id");
                //insert user permission

                foreach ($permissions as $p) {
                    if (Permission::find($p)) {
                        $userPermission = new UserPermission();
                        $userPermission->user_id = $id;
                        $userPermission->permission_id = $p;
                        $userPermission->is_enabled = true;
                        $userPermission->creator_id = $user->id;
                        $userPermission->updater_id = $user->id;
                        $userPermission->save();
                    } else {
                        \LOG::warn("There is no permission_id[$p]");
                    }
                }
            }

            $rangeFrom = $request->json()->get('range_from');
            $rangeTo = $request->json()->get('range_to');
            $include = $request->json()->get('include');
            $exclude = $request->json()->get('exclude');
            if($rangeFrom){
                //update data access
                $dataAccess = DataAccess::getByUserId($id);
                if(!$dataAccess){
                    $dataAccess = new DataAccess();
                    $dataAccess->user_id = $id;
                    $dataAccess->type = 2;
                }else
                    $dataAccess = DataAccess::find($dataAccess->id);


                $dataAccess->range_from = $rangeFrom;
                $dataAccess->range_to = $rangeTo;
                $dataAccess->include = $include;
                $dataAccess->exclude = $exclude;
                $dataAccess->save();

            }



            $statusCode = 201;
            $responseMessage->setData('success', true);
        }
        $result = $responseMessage->getMessage();

        return response()->json($result, $statusCode);
    }

    /**
     * @param Request $request
     * @param $user
     */
    public function inputContactInfo(Request $request, $contact)
    {

        $contact->title = $request->json()->get('title');
        $contact->gender = $request->json()->get('gender');
        $contact->address1 = $request->json()->get('address1');
        $contact->address2 = $request->json()->get('address2');

        $contact->suburb = $request->json()->get('suburb');
        $postcode = $request->json()->get('postcode');
        if($postcode)
            $contact->postcode = $postcode;
        $contact->country = $request->json()->get('country');
        $contact->state = $request->json()->get('state');

        $contact->mobile_phone = $request->json()->get('mobileNumber');
        $contact->email = $request->json()->get('email');
        $contact->privacy_flag = $request->json()->get('privacy');
        $contact->privacy_flag_update_time = date('Y-m-d H:i:s');
        $contact->note = $request->json()->get('note');
        $contact->company_name = $request->json()->get('companyName');

        $contact->first_name = $request->json()->get('first_name');
        $contact->last_name = $request->json()->get('last_name');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getListUser(Request $request){
        $token = $request->header("token");
        $user = NULL;


        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::USER_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        // get company id of request user

        $company_group_id = $user->company_group_id;

        $v = Validator::make($request->all(), [

            'query' => 'max:100',
            'limit' => 'required|integer|min:1|max:100',
            'offset' => 'required|integer|min:0',
            'orderBy' => 'in:first_name,last_name',
            'orderDesc' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $input = $request->json();
        $responseMessage = new ResponseMessage();
        $user = new User();
        //Get list user with pagination and full size
        if($request->path() == 'users/admin/search'){
            $company_group_id = NULL;
        }
        $userList = $user->getListAllUser($input,$company_group_id);

        $statusCode = 200;

        $responseMessage->setData('list_size', $userList[0]);
        $responseMessage->setData('userList', $userList['userList']);

        $result = $responseMessage->getMessage();

        return response()->json($result, $statusCode);

    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserById(Request $request, $id){
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }
        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $responseMessage = new ResponseMessage();
        // validate permission to view other user info
        $apiPermission = array(Permissions::USER_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }


        $user = new User();
        $userInfo = $user -> getUserInfo($id);
        if(!$userInfo){
            $statusCode = 400;
            $responseMessage->setErrorCode(ErrorCodes::USER_NOT_FOUND);

            $result = $responseMessage->getMessage();
            return response()->json($result, $statusCode);
        }
        $statusCode = 200;
        $responseMessage->setData('userInfo', $userInfo);
        $result = $responseMessage->getMessage();
        return response()->json($result, $statusCode);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUserStatus(Request $request){
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }
        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $responseMessage = new ResponseMessage();
        // check if user is admin or role/permission to change user status

        $apiPermission = array(Permissions::USER_SUSPEND);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $userId = $request->json()->get('userId');
        $status = $request->json()->get('status');

        //check if userId available
        $userInfo = User::find($userId);
        if(!$userInfo){
            return $this->responseError(ErrorCodes::USER_NOT_FOUND);
        }

        //change user status
        if($status == User::STATUS_CODE_INACTIVE){
            $userInfo->suspend = true;
            $userInfo->active = true;
        }elseif ($status == User::STATUS_CODE_ACTIVE){
            $userInfo->suspend=false;
            $userInfo->active = true;
        }else{
            return $this->responseError(ErrorCodes::INVALID_REQUEST);
        }
        $userInfo->save();

        $statusCode = 200;
        $responseMessage->setData("result","success");
        $result = $responseMessage->getMessage();
        return response()->json($result, $statusCode);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMyConfig(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }
        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $azure_account_name = env('azure_account_name');
        $azure_sas = env('azure_sas');

        return $this->responseData([
            'receive_email' => $user->receive_email,
            'receive_noti' => $user->receive_noti,
            'azure_account_key' => env('azure_account_key'),
            'azure_account_name' => $azure_account_name,
//            'azure_inspection_item' => "https://$azure_account_name.blob.core.windows.net/inspection-item$azure_sas",
//            'azure_user_avatar' => "https://$azure_account_name.blob.core.windows.net/user-avatar$azure_sas",
//            'azure_vehicle_image' => "https://$azure_account_name.blob.core.windows.net/vehicle-image$azure_sas"
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setMyConfig(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }
        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'receive_email' => 'integer|min:0|max:1',
            'receive_noti' => 'integer|min:0|max:1'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $json = $request->json();

        if ($json->has('receive_email')) {
            $user->receive_email = $json->get('receive_email');
        }

        if ($json->has('receive_noti')) {
            $user->receive_noti = $json->get('receive_noti');
        }

        $user->save();

        return $this->responseData([
            'receive_email' => $user->receive_email,
            'receive_noti' => $user->receive_noti
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    /**
     * @api {post} /users/acceptTermAndCondition
     * @apiName AcceptTerms&Condition
     * @apiGroup User
     * @apiHeader {json} content type and token of Logged in users .
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "Content-type": "application/json"
     *       "token" : "BTgMAeJ5UKp4t8dr9jw3bjZhWbMVW33m2Klm6KTe"
     *     }
     *
     * @apiSuccess {Object} UserDetails
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     * {"accept":true}
     */
    public function acceptTermAndCondition(Request $request) {
        $user = NULL;
        try {
            $user = $this->currentUser($request);
        } catch(Exception $e) {
            return $this->responseError($e->getMessage());
        }

        $user->need_accept_term_and_condition = false;
        $user->save();

        return $this->responseData(['accept' => true]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getListEventCanConfig(Request $request) {
        $user = NULL;
        try {
            $user = $this->currentUser($request);
        } catch(Exception $e) {
            return $this->responseError($e->getMessage());
        }

        $events = User::listEventCanConfigGroupByCategory($user->id);
        return $this->responseData($events);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function configEventCanConfig(Request $request) {
        $user = NULL;
        try {
            $user = $this->currentUser($request);

            $v = Validator::make($request->all(), [
                'event_codes'   => 'required|array',
            ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);
            if ($v->fails()) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
            }
        } catch(Exception $e) {
            return $this->responseError($e->getMessage());
        }

        $event_codes    = $request->json()->get('event_codes');

        $result = 0;
        foreach ($event_codes as $event) {
            $result += ConfigEmailNotification::where('user_id', $user->id)
                ->where('event', $event['code'])
                ->update(['config' => $event['config']]);
        }

        return $this->responseData(['updated' => $result]);
    }
}