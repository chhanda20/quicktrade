<?php

namespace App\Http\Controllers;



use App\BidSetting;
use App\Libraries\AuctionBidHelper;
use App\Libraries\ErrorCodes;
use App\Libraries\Event;
use App\Libraries\Helper;
use App\Libraries\Notification;
use App\Libraries\Permissions;
use App\AuctionSession;
use App\SessionHasItem;
use App\User;
use App\NotificationStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class BidController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function updateUserSetting(Request $request, $session_id, $item_id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        //todo check permission
//        $apiPermission = array(Permissions::ROLE_GET);
//        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
//        if(!$valuationResult){
//
//            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
//        }
        $v = Validator::make($request->all(), [
            'max_price' => 'required|numeric',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $session_has_item = SessionHasItem::where('auction_session_id', $session_id)->where('auction_item_id', $item_id)->first();
        if (!$session_has_item || $session_has_item->status != SessionHasItem::STATUS_APPROVED) {
            return $this->responseError(ErrorCodes::NOT_FOUND, 'session_has_item not found or not in the right status');
        }

        $bidSetting = BidSetting::where('auction_session_id', $session_id)->where('auction_item_id', $item_id)->where('user_id', $user->id)->first();


        $json = $request->json();
        $max_price = $json->get('max_price');

        if ($bidSetting && $max_price <= $bidSetting->max_price) {
            return $this->responseError(ErrorCodes::BID_PRICE_MUST_BIGGER, "Max price can't be smaller than previous value");
        }

        if (!$bidSetting) {
            $bidSetting = new BidSetting();
            $bidSetting->auction_session_id = $session_id;
            $bidSetting->auction_item_id = $item_id;
            $bidSetting->user_id = $user->id;
            $bidSetting->max_price = $max_price;
            $bidSetting->save();
        } else {
            BidSetting::where('auction_session_id', $session_id)->where('auction_item_id', $item_id)->where('user_id', $user->id)->update(['max_price' => $max_price ]);
            $bidSetting = BidSetting::where('auction_session_id', $session_id)->where('auction_item_id', $item_id)->where('user_id', $user->id)->first();
        }


        return $this->responseData($bidSetting);
    }

    public function getUserSetting(Request $request, $session_id, $item_id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

//        $v = Validator::make($request->all(), [
//        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);
//
//        if ($v->fails()) {
//            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
//        }

        $session_has_item = SessionHasItem::where('auction_session_id', $session_id)->where('auction_item_id', $item_id)->first();
        if (!$session_has_item ) {
            return $this->responseError(ErrorCodes::NOT_FOUND, 'session_has_item not found');
        }

        $bidSetting = BidSetting::where('auction_session_id', $session_id)->where('auction_item_id', $item_id)->where('user_id', $user->id)->first();

        if (!$bidSetting) {
            $bidSetting = new BidSetting();
            $bidSetting->session_id = $session_id;
            $bidSetting->item_id = $item_id;
            $bidSetting->user_id = $user->id;
            $bidSetting->max_price = 0;
        }

        return $this->responseData($bidSetting);
    }


    public function bid(Request $request, $sessionId, $itemId) {
        $sessionId = (int) $sessionId;
        $itemId = (int) $itemId;

        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'bid_price' => 'required|integer|min:0',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $apiPermission = array(Permissions::BID_ON_VEHICLE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $json = $request->json();

        $bid_price = $json->get('bid_price');

        $err = AuctionBidHelper::bid($sessionId, $itemId, $bid_price, $user->id);

        if ($err) {
            return $this->responseError($err);
        }

        AuctionSession::sendOutbid($sessionId, $user);

        return $this->responseData(['bid_price' => $bid_price]);
    }

    public function autoBid(Request $request) {
        return $this->responseData(AuctionBidHelper::autoBid());
    }
}
