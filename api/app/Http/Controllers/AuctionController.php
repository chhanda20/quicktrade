<?php

namespace App\Http\Controllers;


use App\AppProperty;
use App\Auction;
use App\AuctionSession;
use App\Document;
use App\Libraries\ErrorCodes;
use App\Libraries\Helper;
use App\Libraries\IdTransformer;
use App\Libraries\Permissions;
use App\Libraries\ResponseMessage;
use App\Location;
use App\RegionCommon;
use App\SessionHasItem;
use App\TestDrive;
use App\Transaction;
use App\User;
use App\Valuation;
use App\Vehicle;
use App\Libraries\Event;
use Carbon\Carbon;
use App\WishList;
use DateTime;
use Exception;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AuctionController extends Controller
{
    public function __construct()
    {
    }

    public function submitAuction(Request $request)
    {
        $token              = $request->header("token");
        $valuationId        = $request->json()->get('valuation_id');
        $startPrice         = $request->json()->get('start_price');
        $buyNowPrice        = $request->json()->get('buy_now_price');
        $ppsrCleared        = $request->json()->get('ppsr_cleared');
        $allInfoCorrect     = $request->json()->get('all_info_correct');
        $termsAgreed        = $request->json()->get('terms_agreed');
        $extended           = $request->json()->get('extended');
        $exRental           = $request->json()->get('ex_rental');
        $exHire             = $request->json()->get('ex_hire');
        $auctionSessionId   = $request->json()->get('auctionSessionId');

        $user = $this->checkUserToken($request->header("token"));
        if(!$user)
            return $this->responseError(ErrorCodes::INVALID_TOKEN);


        //check permission to submit Vehicle to Auction
        $apiPermission = array(Permissions::SEND_TO_AUCTION);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        try {
            $validationData = [
                'token' => $token,
                'valuationId' => $valuationId,
                'startPrice' => $startPrice,
                'buyNowPrice' => $buyNowPrice,
                'auctionSessionId' => $auctionSessionId,

            ];
            $v = Validator::make($validationData, [
                'token' => 'required|alpha_num|exists:mysql.user,token',
                'valuationId' => 'required',
                'startPrice' => 'required|numeric',
                'buyNowPrice' => 'required|numeric',
                'auctionSessionId' => 'required|exists:mysql.auction_session,id',

            ], [
                'required' => ErrorCodes::REQUIRED,
                'alpha_num' => ErrorCodes::ALPHA_NUM,
                'exists' => ErrorCodes::NOT_EXISTS,
                'numeric' => ErrorCodes::NUMERIC,
            ]);
            if ($v->fails()) {
                $errorMessages = $v->errors()->getMessages();
                return $this->responseError(ErrorCodes::INVALID, $errorMessages);
            }

            if(!$allInfoCorrect || !$ppsrCleared || !$termsAgreed){
                return $this->responseError(ErrorCodes::INVALID);
            }

            if(!$extended) $extended=0;
            if(!$exRental) $exRental=0;
            if(!$exHire) $exHire=0;

            $responseMessage = new ResponseMessage();

            //insert record to auction item table
            //validate valuation id
            $valuation = Valuation::find($valuationId);
            if(!$valuation){
                \Log::info("Cannot create Auction Session");
                return $this->responseError(ErrorCodes::CREATE_FAILED);
            }

            //validate if valuation already submit
            if(!Auction::isValuationCanSubmit($valuationId)) {
                return $this->responseError(ErrorCodes::VALUATION_ALREADY_SUBMITTED);
            }
            //validate if session auction is not live or in past
            if(!AuctionSession::validateSessionTime($auctionSessionId))
                return $this->responseError(ErrorCodes::AUCTION_ON_LIVE);
            $auction = new Auction();
            $auction->valuation_id = $valuationId;
            $auction->location_id = $user->location_id;
            $auction->start_price = $startPrice;
            $auction->buy_now_price = $buyNowPrice;
            $auction->ppsr_cleared = $ppsrCleared;
            $auction->all_info_correct = $allInfoCorrect;
            $auction->terms_agreed = $termsAgreed;
            $auction->extended = $extended;
            $auction->ex_rental = $exRental;
            $auction->ex_hire = $exHire;
            $auction->status = Auction::STATUS_CODE_PENDING ;
            $auction->creator_id = $user->id;

            //link session to auction
            $createResult = $auction->save();

            Auction::linkAuctionToSession($auctionSessionId, $auction->id, $startPrice);
            //change status of valuation to not in stock
            $valuation->status = Valuation::STATUS_SUBMIT_TO_AUCTION;
            $valuation->save();

            if ($createResult == 1) {
                $statusCode = 200;
                $responseMessage->setData('success', true);
                $result = $responseMessage->getMessage();

                return response()->json($result, $statusCode);
            } else {
                \Log::info("Cannot create Auction Session");
                return $this->responseError(ErrorCodes::CREATE_FAILED);
            }
        } catch (\Exception $exception) {
            \Log::error("Exception " . $exception);
            return $this->responseError(ErrorCodes::SYS_ERR);
        }


    }

    public function searchByListType(Request $request)
    {
        $user = $this->checkUserToken($request->header("token"));
        if(!$user)
            return $this->responseError(ErrorCodes::INVALID_TOKEN);

        // check permission
        $apiPermission = array(Permissions::AUCTION_ITEM_LISTING_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }


        $v = Validator::make($request->all(), [
            'limit' => 'required|integer|min:1|max:100',
            'offset' => 'required|integer|min:0',
            'orderDesc' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $input = $request->json();
        $responseMessage = new ResponseMessage();


        //Get list auction session with pagination and full size

//        $location = null;
        $where = "";

        $where = $this->generateWhereQueryVehicleSearchFilter($input, $where, $user);

        if ($input->has("trade_price")) {
            $trade_price = $input->get('trade_price');
            if (sizeof($trade_price) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'trade_price must be an array contains 2 number');
            }

            $where .= " AND ((auc.start_price between $trade_price[0] AND $trade_price[1]) OR (auc.buy_now_price between $trade_price[0] AND $trade_price[1]))";
        }

        $seller_rating = $input->get('seller_rating');
        if ($seller_rating) {
            $ratings = implode(",", $seller_rating);
            $where .= " AND find_in_set(lo.seller_rating, '$ratings') ";
        }

        $userLocationAccess = implode(',', $user->getAccessableLocationIds());

        $listAuction = Auction::getAll($input,$user->id,$where,$userLocationAccess);
        if (!$listAuction) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        $statusCode = 200;

        foreach ($listAuction['auctionList'] as $auction) {
            \LOG::debug(print_r($auction, true));
            $location = $auction->trade_in_location;
            $sellerId = $auction->creator_id;
            $postCode = $auction->postcode;

            $auction->seller_rating = Location::ratingAsSeller($auction->trade_in_location);
            $auction->buyer_rating  = Location::ratingAsBuyer($location);
            $auction->quick_seller  = Location::isSpeedySeller($location);
            $auction->region_text   = $postCode ? RegionCommon::getRegionNameByPostcode($postCode) : NULL;


            $buyerLocation      = $user->location_id;
            $sellerLocation     = $auction->trade_in_location;
            $buyerPostcode      = Location::find($buyerLocation)->postcode;
            $sellerPostcode     = Location::find($sellerLocation)->postcode;
            if($buyerPostcode && $sellerPostcode) {
                $buyerRegion            = RegionCommon::getRegionByPostcode($buyerPostcode);
                $sellerRegion           = RegionCommon::getRegionByPostcode($sellerPostcode);

                $min = min($buyerRegion, $sellerRegion);
                $max = max($sellerRegion, $buyerRegion);
                $auction->delivery_cost = DB::select("SELECT * FROM transport_price WHERE from_region = $min AND to_region = $max")[0]->price;
            }
        }

        $responseMessage->setData('list_size', $listAuction[0]);
        $responseMessage->setData('auctionList', $listAuction['auctionList']);

        $result = $responseMessage->getMessage();

        return response()->json($result, $statusCode);

    }

    public function generateWhereQueryVehicleSearchFilter($input,$where,$user){
        if ($input->has('location')) {
            if (sizeof($input->get('location')) == 0) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'location must contain at least 1 number');
            }
            $location = implode(",", $input->get('location'));
            $where .= " AND veh.trade_in_location IN ($location) ";
        } else {
            if (!$user->location_id) {
                \LOG::warn("User[$user->id] doesn't have location_id");
            }
            $location = $user->location_id;
        }
        //test-trigger

        if ($input->has("odometer")) {
            $odometer = $input->get('odometer');
            if (sizeof($odometer) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'odometer must be an array contains 2 number');
            }
            $where .= " AND (veh.odometer between $odometer[0] AND $odometer[1])";
        }

        if ($input->has("year")) {
            $year = $input->get('year');
            if (sizeof($year) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'year must be an array contains 2 number');
            }
            $where .= " AND (year(veh.build_date) between $year[0] AND $year[1])";
        }

        $transmission = collect();

        if ($input->has("transmissionType")) {
            foreach ($input->get('transmissionType') as $type) {
                $x =  str_replace('\'', '"', $type);
                $x =  trim($x);
                if ($x) {
                    $transmission->push("'" . $x . "'");
                }
            }

            $transmission = $transmission->unique()->values()->all();

            $transmission = implode(",", $transmission);
            if($transmission) {
                $where .= " AND (veh.transmission IN ($transmission))";
            }
        }

        $make = collect();
        $model = collect();
        $variant = collect();

        if ($input->has("makeInfo")) {
            $arr = $input->get('makeInfo');
            foreach ($arr as $i) {
                $i = (object) $i;

                $i->make = str_replace('\'', '"', $i->make);
                $i->make = trim($i->make);

                $i->model = str_replace('\'', '"', $i->model);
                $i->model = trim($i->model);

                $i->variant = str_replace('\'', '"', $i->variant);
                $i->variant = trim($i->variant);

                if ($i->make) {
                    $make->push("'" . $i->make. "'" );
                }
                if ($i->model) {
                    $model->push("'" . $i->model. "'" );
                }
                if ($i->variant) {
                    $variant->push("'" . $i->variant. "'" );
                }
            }
        }

        $make = $make->unique()->values()->all();
        $model = $model->unique()->values()->all();
        $variant = $variant->unique()->values()->all();

        if (sizeof($make) > 0) {
            $make = implode(",", $make);
            $where .= " AND (make IN ($make))";
        }
        if (sizeof($model) > 0) {
            $model = implode(",", $model);
            $where .= " AND (model_family IN ($model))";
        }
        if (sizeof($variant) > 0) {
            $variant = implode(",", $variant);
            $where .= " AND (variant IN ($variant))";
        }

        if ($input->has("query")) {
            $text = $input->get('query');
            $text = str_replace('\'', '"', $text); // for safety reasons;
            if ($text) {
                $query = "(YEAR(build_date) LIKE '$text*')";
                $query .= "OR MATCH(make, family, series, style, engine_type, engine_size, variant, cylinder, transmission, registration_number, body_type) AGAINST ('$text*' IN BOOLEAN MODE)";
                $where .= " AND ($query) ";
            }
        }

        return $where;
    }

    public function addWatchList(Request $request){
        $user = $this->checkUserToken($request->header("token"));
        if(!$user)
            return $this->responseError(ErrorCodes::INVALID_TOKEN);

        //check permission
        $apiPermission = array(Permissions::WATCH_LIST_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [

            'auction_id' => 'required|integer',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $auctionItemId = $request->json()->get('auction_id');
        $auctionItem = Auction::find($auctionItemId);
        if(!$auctionItem){
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        $result = Auction::addToWatchList($auctionItemId,$user->id);
        if($result!=1){
            return $this->responseError(ErrorCodes::CREATE_FAILED);
        }
        $responseMessage = new ResponseMessage();
        $statusCode = 200;
        $responseMessage->setData('success', true);
        $result = $responseMessage->getMessage();

        return response()->json($result, $statusCode);

    }

    public function removeWatchList(Request $request){
        $user = $this->checkUserToken($request->header("token"));
        if(!$user)
            return $this->responseError(ErrorCodes::INVALID_TOKEN);

        //check permission
        $apiPermission = array(Permissions::WATCH_LIST_DELETE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'auction_id' => 'required|integer',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $auctionItemId = $request->json()->get('auction_id');
        $auctionItem = Auction::find($auctionItemId);
        if(!$auctionItem){
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        $result = Auction::removeFromWatchList($auctionItemId,$user->id);
        if($result!=1){
            return $this->responseError(ErrorCodes::CREATE_FAILED);
        }
        $responseMessage = new ResponseMessage();
        $statusCode = 200;
        $responseMessage->setData('success', true);
        $result = $responseMessage->getMessage();

        return response()->json($result, $statusCode);

    }

    private function checkUserToken($token){
        if ($token) {
            $user = User::where('token', $token)->first();
            return $user;
        }
    }
    public function getRequestList(Request $request){
        $user = $this->checkUserToken($request->header("token"));
        if(!$user)
            return $this->responseError(ErrorCodes::INVALID_TOKEN);

        //check permission
        $apiPermission = array(Permissions::AUCTION_ITEM_APPROVE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [

            'limit' => 'required|integer|min:1|max:100',
            'offset' => 'required|integer|min:0',
            'orderDesc' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $input = $request->json();

        $where = "";
        $where = $this->generateWhereQueryVehicleSearchFilter($input, $where, $user);

        if ($input->has("trade_price")) {
            $trade_price = $input->get('trade_price');
            if (sizeof($trade_price) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'trade_price must be an array contains 2 number');
            }
            $where .= " AND (trade_price between $trade_price[0] AND $trade_price[1])";
        }

        $listAuction = Auction::getRequestList($input, $where);
        $responseMessage = new ResponseMessage();
        $statusCode = 200;
        if (!$listAuction) {
            $responseMessage->setData('list_size', array("count" => 0));
            $responseMessage->setData('requestList', array());
        }else{
            $responseMessage->setData('list_size', $listAuction[0]);
            $responseMessage->setData('requestList', $listAuction['requestList']);
        }
        $result = $responseMessage->getMessage();

        return response()->json($result, $statusCode);
    }

    public function approveOrRejectRequestAuction(Request $request) {
        $user = $this->checkUserToken($request->header("token"));
        if(!$user)
            return $this->responseError(ErrorCodes::INVALID_TOKEN);


        // check permission
        $apiPermission = array(Permissions::AUCTION_ITEM_APPROVE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if (!$valuationResult) {

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [

            'request_id' => 'required|integer',
            'action' => 'required'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $input = $request->json();
        $status = 1;
        $comment = "";
        $sessionId = 0 ;
        $valuationStatus = 0;

        // change auction item status
        $requestId = $input->get('request_id');
        $rejectCode = $input->get('reject_code');


        if ($input->get('action') == 'approve') {
            $sessionId = $input->get('session_id');
            //validate sessionId
            if(!AuctionSession::find($sessionId)){
                \Log::error("Auction Session Not found $sessionId");
                return $this->responseError(ErrorCodes::NOT_FOUND);
            }
            $status = Auction::STATUS_CODE_APPROVE;
            $valuationStatus = Valuation::STATUS_LISTED_FOR_AUCTION;
            // update auction to session
            Auction::updateAuctionItemToSession($requestId, $sessionId, $status);

            //send notice here
            $locationIds = WishList::locationsMatchingAuction($requestId);
            $event = Event::WISHLIST_AUCTION;
            $data = [];
    
            foreach ($locationIds as $locationId) {
                User::noticeLocation($locationId, $event, $requestId, $data);
            }

        } else {
            $comment = $input->get('comment');
            $status = Auction::STATUS_CODE_REJECT;
            $valuationStatus = Valuation::STATUS_COMPLETE_TRADED_IN;
            AuctionSession::updateSessionHasItemReject($requestId);
        }

        $auctionItem = Auction::find($requestId);
        if(!$auctionItem){
            \Log::error("Auction Item Not found $requestId");
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        $auctionItem->status = $status;
        $auctionItem->comment = $comment;
        $auctionItem->reject_code = $rejectCode;
        $auctionItem->save();

        //update valuation status to be in stock
        $valuation = Valuation::find($auctionItem->valuation_id);
        if(!$valuation){
            \Log::error("Valuation Not found $auctionItem->valuation_id ");
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        $valuation->status = $valuationStatus;
        $valuation->save();

        if ($input->get('action') == 'approve') {
            // $userWishlist = $valuation->usersHaveMatchWishlist();
            // foreach ($userWishlist as $user) {
            //     MailServiceController::sendWishlistAppear($user, '');
            //     User::notice($user->id, Event::TRANSACTION_NOTIFY, $t->id, $customData);
            // }
        }

        $responseMessage = new ResponseMessage();
        $statusCode = 200;
        $responseMessage->setData('success', true);
        $result = $responseMessage->getMessage();

        return response()->json($result, $statusCode);
    }

    public function addRecentView(Request $request){
        $user = $this->checkUserToken($request->header("token"));
        if(!$user)
            return $this->responseError(ErrorCodes::INVALID_TOKEN);

        //check permission
        $apiPermission = array(Permissions::AUCTION_ITEM_LISTING_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [

            'auction_id' => 'required|integer',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $auctionItemId = $request->json()->get('auction_id');
        $auctionItem = Auction::find($auctionItemId);
        if(!$auctionItem){
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        $result = Auction::addToRecentList($auctionItemId,$user->id);
        if($result!=1){
            return $this->responseError(ErrorCodes::CREATE_FAILED);
        }
        $responseMessage = new ResponseMessage();
        $statusCode = 200;
        $responseMessage->setData('success', true);
        $result = $responseMessage->getMessage();

        return response()->json($result, $statusCode);
    }

    public function buyNow(Request $request){
        $user = $this->checkUserToken($request->header("token"));
        if(!$user)
            return $this->responseError(ErrorCodes::INVALID_TOKEN);

        // check permission
        $apiPermission = array(Permissions::BID_ON_VEHICLE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

//            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [

            'auction_id' => 'required|integer',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $auctionItemId = $request->json()->get('auction_id');

        //check auction item id valid
        $auctionItem = Auction::find($auctionItemId);
        if(!$auctionItem){
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        // check if auction is online,if yes,not allow to buy now.
        if(Auction::checkItemInLiveAuction($auctionItemId)){
            return $this->responseError(ErrorCodes::AUCTION_ON_LIVE);
        }
        if($auctionItem->status != Auction::STATUS_CODE_APPROVE){
            return $this->responseError(ErrorCodes::AUCTION_NOT_AVAIL);
        }
        //change status of auction item,session has item
        Auction::updateAuctionItemStatus($auctionItemId,Auction::STATUS_CODE_BUY_OUT);

        // change status of valuation.

        $valuation = Valuation::find($auctionItem->valuation_id);
        $valuation->status = Valuation::STATUS_SOLD_OUT;
        $valuation->save();
        // create transaction

        $transaction = new Transaction();
        $transaction->valuation_id = $valuation->id;
        $transaction->buyer_id = $user->id;
        $transaction->seller_id = $auctionItem->creator_id;

        $transaction->status = Transaction::STATUS_WAITING_PAYMENT;
        $transaction->vehicle_price = $auctionItem->buy_now_price;
        $transaction->valuation_version = $valuation->version;
        $transaction->source = Transaction::SOURCE_AUCTION_BUY_NOW;
        $transaction->source_id = $auctionItemId;
        $transaction->buyer_location = $user->location_id;
        $vehicle = Vehicle::find($valuation->vehicle_id);
        $transaction->seller_location = $vehicle->trade_in_location;
        //create rid
        $transaction->save();
        $transaction->rid = IdTransformer::toClientTransactionId($transaction->id);
        Transaction::setMotoFee($transaction);

        $transaction->save();

        //update rating
        $locationId = Valuation::where('valuation.id', $transaction->valuation_id)
            ->join('vehicle', 'vehicle.id', '=', 'valuation.vehicle_id')
            ->first()
            ->location_id;
        Location::updateRating($locationId);

        //send mail - payment receipt
        $buyer = User::find($transaction->buyer_id);
        $event = Event::TRANSACTION_NEW;
        $buyerData = [];

        \LOG::debug("start generate document for event: [$event]");
        $buyerData['attach'] =  Document::generatorPDFAndUploadAzure($transaction->seller_id, $event, $transaction->id, 'creator');

        User::notice($transaction->buyer_id, $event, $transaction->id, $buyerData);

        //end create transaction
        $responseMessage = new ResponseMessage();
        $statusCode = 200;
        $responseMessage->setData('success', true);
        $result = $responseMessage->getMessage();

        return response()->json($result, $statusCode);
    }

    public function updateAuctionItem(Request $request, $auctionId){
        $user = $this->checkUserToken($request->header("token"));
        if(!$user)
            return $this->responseError(ErrorCodes::INVALID_TOKEN);

        $valuationId = $request->json()->get('valuation_id');
        $startPrice = $request->json()->get('start_price');
        $buyNowPrice = $request->json()->get('buy_now_price');
        $auctionSessionId = $request->json()->get('auctionSessionId');

        $validationData = [
            'valuationId' => $valuationId,
            'startPrice' => $startPrice,
            'buyNowPrice' => $buyNowPrice,
            'auctionSessionId' => $auctionSessionId,

        ];
        $v = Validator::make($validationData, [
            'valuationId' => 'required',
            'startPrice' => 'required|numeric',
            'buyNowPrice' => 'required|numeric',
            'auctionSessionId' => 'required|exists:mysql.auction_session,id',

        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        //check permission
        $apiPermission = array(Permissions::AUCTION_ITEM_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }



        //check auction item id valid
        $auctionItem = Auction::find($auctionId);
        if(!$auctionItem){
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        //validate if session auction is not live or in past
        if(!AuctionSession::validateSessionTime($auctionSessionId))
            return $this->responseError(ErrorCodes::AUCTION_ON_LIVE);

        $auctionItem->valuation_id = $valuationId;
        $auctionItem->location_id = $user->location_id;
        $auctionItem->start_price = $startPrice;
        $auctionItem->buy_now_price = $buyNowPrice;
        $updateResult = $auctionItem->save();

        //update session has item
        $pendingStatus = Auction::STATUS_CODE_PENDING;
        DB::statement("update session_has_item Set auction_session_id = $auctionSessionId,bid_price = $startPrice WHERE auction_item_id = $auctionId AND status = $pendingStatus");

        $responseMessage = new ResponseMessage();
        if ($updateResult == 1) {
            $statusCode = 200;
            $responseMessage->setData('success', true);
            $result = $responseMessage->getMessage();

            return response()->json($result, $statusCode);
        } else {
            \Log::info("Cannot create Auction Session");
            return $this->responseError(ErrorCodes::CREATE_FAILED);
        }

    }

    public function cancelAuctionRequest(Request $request){
        $user = $this->checkUserToken($request->header("token"));
        if(!$user)
            return $this->responseError(ErrorCodes::INVALID_TOKEN);

        // check permission
        $apiPermission = array(Permissions::AUCTION_ITEM_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [

            'auction_id' => 'required|integer',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $auctionItemId = $request->json()->get('auction_id');

        //check auction item id valid
        $auctionItem = Auction::find($auctionItemId);
        if(!$auctionItem){
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        //remove auction item,remove session has item
        $updateResult = Auction::removeFromAuction($auctionItemId);

        //update valuation status
        $valuation = Valuation::find($auctionItem->valuation_id);
        if(!$valuation){
            \Log::error("Valuation Not found $auctionItem->valuation_id ");
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        $valuation->status = Valuation::STATUS_COMPLETE_TRADED_IN;
        $valuation->save();

        $responseMessage = new ResponseMessage();
        if ($updateResult == 1) {
            $statusCode = 200;
            $responseMessage->setData('success', true);
            $result = $responseMessage->getMessage();

            return response()->json($result, $statusCode);
        } else {
            \Log::info("Cannot cancel Auction Session");
            return $this->responseError(ErrorCodes::CREATE_FAILED);
        }

    }

    public function getAuctionById(Request $request, $auctionId)
    {
        $user = $this->checkUserToken($request->header("token"));
        if (!$user)
            return $this->responseError(ErrorCodes::INVALID_TOKEN);

        // check permission
        $apiPermission = array(Permissions::AUCTION_ITEM_LISTING_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        //check auction item id valid
        $auctionItem = Auction::find($auctionId);
        if (!$auctionItem) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        $listAuction = Auction::getAuctionById($auctionId, $user->id);

        $buyerLocation      = $user->location_id;
        $sellerLocation     = User::find($listAuction[0]->creator_id)->location_id;
        $buyerPostcode      = Location::find($buyerLocation)->postcode;
        $sellerPostcode     = Location::find($sellerLocation)->postcode;
        if($buyerPostcode && $sellerPostcode) {
            $buyerRegion                    = RegionCommon::getRegionByPostcode($buyerPostcode);
            $sellerRegion                   = RegionCommon::getRegionByPostcode($sellerPostcode);
            $min = min($buyerRegion, $sellerRegion);
            $max = max($sellerRegion, $buyerRegion);
            $listAuction[0]->delivery_cost  = DB::select("SELECT * FROM transport_price WHERE from_region = $min AND to_region = $max")[0]->price;
        }

        if (!$listAuction) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        return response()->json($listAuction[0]);
    }

    public function resendAuction(Request $request){

        $token = $request->header("token");

        $startPrice = $request->json()->get('start_price');
        $buyNowPrice = $request->json()->get('buy_now_price');
        $ppsrCleared = $request->json()->get('ppsr_cleared');
        $allInfoCorrect = $request->json()->get('all_info_correct');
        $termsAgreed = $request->json()->get('terms_agreed');
        $extended = $request->json()->get('extended');
        $exRental = $request->json()->get('ex_rental');
        $exHire = $request->json()->get('ex_hire');
        $auctionSessionId = $request->json()->get('auctionSessionId');
        $auctionId = $request->json()->get('auctionId');

        $user = $this->checkUserToken($request->header("token"));
        if(!$user)
            return $this->responseError(ErrorCodes::INVALID_TOKEN);


        //check permission
        $apiPermission = array(Permissions::SEND_TO_AUCTION);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        try {
            $validationData = [
                'token' => $token,
                'startPrice' => $startPrice,
                'buyNowPrice' => $buyNowPrice,
                'auctionSessionId' => $auctionSessionId,

            ];
            $v = Validator::make($validationData, [
                'token' => 'required|alpha_num|exists:mysql.user,token',
                'startPrice' => 'required|numeric',
                'buyNowPrice' => 'required|numeric',
                'auctionSessionId' => 'required|exists:mysql.auction_session,id',

            ], [
                'required' => ErrorCodes::REQUIRED,
                'alpha_num' => ErrorCodes::ALPHA_NUM,
                'exists' => ErrorCodes::NOT_EXISTS,
                'numeric' => ErrorCodes::NUMERIC,
            ]);
            if ($v->fails()) {
                $errorMessages = $v->errors()->getMessages();
                return $this->responseError(ErrorCodes::INVALID, $errorMessages);
            }

            if(!$allInfoCorrect || !$ppsrCleared || !$termsAgreed){
                return $this->responseError(ErrorCodes::INVALID);
            }

            if(!$extended) $extended=0;
            if(!$exRental) $exRental=0;
            if(!$exHire) $exHire=0;

            $responseMessage = new ResponseMessage();

            //check auction item id valid
            $auctionItem = Auction::find($auctionId);
            if(!$auctionItem){
                return $this->responseError(ErrorCodes::NOT_FOUND);
            }

            //validate valuation id
            $valuation = Valuation::find($auctionItem->valuation_id);
            if(!$valuation){
                \Log::info("Cannot create Auction Session");
                return $this->responseError(ErrorCodes::CREATE_FAILED);
            }

            //validate if session auction is not live or in past
            if(!AuctionSession::validateSessionTime($auctionSessionId))
                return $this->responseError(ErrorCodes::AUCTION_ON_LIVE);

            $auctionItem->location_id = $user->location_id;
            $auctionItem->start_price = $startPrice;
            $auctionItem->buy_now_price = $buyNowPrice;
            $auctionItem->ppsr_cleared = $ppsrCleared;
            $auctionItem->all_info_correct = $allInfoCorrect;
            $auctionItem->terms_agreed = $termsAgreed;
            $auctionItem->extended = $extended;
            $auctionItem->ex_rental = $exRental;
            $auctionItem->ex_hire = $exHire;
            $auctionItem->status = Auction::STATUS_CODE_PENDING ;

            //link session to auction
            $createResult = $auctionItem->save();

            //update session has item
            $pendingStatus = Auction::STATUS_CODE_PENDING;
            $rejectStatus = Auction::STATUS_CODE_REJECT;
//            DB::statement("
//                update session_has_item
//                Set auction_session_id = $auctionSessionId,
//                    status = $pendingStatus,
//                    bid_price = $startPrice
//                WHERE auction_item_id = $auctionId AND status = $rejectStatus");

            DB::statement("
                update session_has_item
                Set auction_session_id = $auctionSessionId,
                    status = $pendingStatus,
                    bid_price = $startPrice
                WHERE auction_item_id = $auctionId");

            //change status of valuation to not in stock
            $valuation->status = Valuation::STATUS_SUBMIT_TO_AUCTION;
            $valuation->save();

            if ($createResult == 1) {
                $statusCode = 200;
                $responseMessage->setData('success', true);
                $result = $responseMessage->getMessage();

                return response()->json($result, $statusCode);
            } else {
                \Log::info("Cannot create Auction Session");
                return $this->responseError(ErrorCodes::CREATE_FAILED);
            }
        } catch (\Exception $exception) {
            \Log::error("Exception " . $exception);
            return $this->responseError(ErrorCodes::SYS_ERR);
        }
    }

    public function editAuctionListing(Request $request, $auctionItemId) {
        $token              = $request->header("token");
        $valuationId        = $request->json()->get('valuation_id');
        $startPrice         = $request->json()->get('start_price');
        $buyNowPrice        = $request->json()->get('buy_now_price');
        $ppsrCleared        = $request->json()->get('ppsr_cleared');
        $allInfoCorrect     = $request->json()->get('all_info_correct');
        $termsAgreed        = $request->json()->get('terms_agreed');
        $extended           = $request->json()->get('extended');
        $exRental           = $request->json()->get('ex_rental');
        $exHire             = $request->json()->get('ex_hire');
        $auctionSessionId   = $request->json()->get('auctionSessionId');

        $user               = NULL;
        try {
            $user           = $this->currentUser($request);
            $nextSession    = AuctionSession::find($auctionSessionId);
            $nextSession->validateInEditableTime();
        } catch(Exception $e) {
            return $this->responseError($e->getMessage());
        }

        //check permission to submit Vehicle to Auction
        $apiPermission = array(Permissions::SEND_TO_AUCTION);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        try {
            $validationData = [
                'token' => $token,
                'valuationId' => $valuationId,
                'startPrice' => $startPrice,
                'buyNowPrice' => $buyNowPrice,
                'auctionSessionId' => $auctionSessionId,

            ];
            $v = Validator::make($validationData, [
                'token' => 'required|alpha_num|exists:mysql.user,token',
                'valuationId' => 'required',
                'startPrice' => 'required|numeric',
                'buyNowPrice' => 'required|numeric',
                'auctionSessionId' => 'exists:mysql.auction_session,id',

            ], [
                'required' => ErrorCodes::REQUIRED,
                'alpha_num' => ErrorCodes::ALPHA_NUM,
                'exists' => ErrorCodes::NOT_EXISTS,
                'numeric' => ErrorCodes::NUMERIC,
            ]);
            if ($v->fails()) {
                $errorMessages = $v->errors()->getMessages();
                return $this->responseError(ErrorCodes::INVALID, $errorMessages);
            }

            if(!$allInfoCorrect || !$ppsrCleared || !$termsAgreed){
                return $this->responseError(ErrorCodes::INVALID);
            }

            if(!$extended) $extended=0;
            if(!$exRental) $exRental=0;
            if(!$exHire) $exHire=0;

            $responseMessage = new ResponseMessage();

            //insert record to auction item table
            //validate valuation id
            $valuation = Valuation::find($valuationId);
            if(!$valuation){
                \Log::info("Cannot create Auction Session");
                return $this->responseError(ErrorCodes::CREATE_FAILED);
            }

            //validate if valuation already submit
            if(!Auction::isValuationCanSubmit($valuationId)) {
                return $this->responseError(ErrorCodes::VALUATION_ALREADY_SUBMITTED);
            }
            //validate if session auction is not live or in past
            if(!AuctionSession::validateSessionTime($auctionSessionId))
                return $this->responseError(ErrorCodes::AUCTION_ON_LIVE);
            $auction = Auction::find($auctionItemId);
            $auction->valuation_id = $valuationId;
            $auction->location_id = $user->location_id;
            $auction->start_price = $startPrice;
            $auction->buy_now_price = $buyNowPrice;
            $auction->ppsr_cleared = $ppsrCleared;
            $auction->all_info_correct = $allInfoCorrect;
            $auction->terms_agreed = $termsAgreed;
            $auction->extended = $extended;
            $auction->ex_rental = $exRental;
            $auction->ex_hire = $exHire;
            $auction->status = Auction::STATUS_CODE_PENDING ;
            $auction->creator_id = $user->id;

            //link session to auction
            $createResult = $auction->save();

            Auction::linkAuctionToSession($auctionSessionId, $auction->id, $startPrice);
            //change status of valuation to not in stock
            $valuation->status = Valuation::STATUS_SUBMIT_TO_AUCTION;
            $valuation->save();

            if ($createResult == 1) {
                $statusCode = 200;
                $responseMessage->setData('success', true);
                $result = $responseMessage->getMessage();

                return response()->json($result, $statusCode);
            } else {
                \Log::info("Cannot create Auction Session");
                return $this->responseError(ErrorCodes::CREATE_FAILED);
            }
        } catch (\Exception $exception) {
            \Log::error("Exception " . $exception);
            return $this->responseError(ErrorCodes::SYS_ERR);
        }
    }

    public function searchItem(Request $request, $auctionItemId) {
        $user = NULL;
        try {
            $user = $this->currentUser($request, [Permissions::AUCTION_ITEM_LISTING_VIEW]);
        } catch(Exception $e) {
            return $this->responseError($e->getMessage());
        }

        $auction = Auction::find($auctionItemId);
        $hasItem = SessionHasItem::where('auction_item_id', $auctionItemId)->first();
        if ($auction && $hasItem) {
            $auction->auction_session_id            = $hasItem->auction_session_id;
            $auction->auction_session_start_date    = AuctionSession::find($hasItem->auction_session_id)->startDate;
        }
        return response()->json($auction, 200);
    }
}
