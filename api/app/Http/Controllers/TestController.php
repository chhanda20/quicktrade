<?php

namespace App\Http\Controllers;

use App\Http\Controllers\MailServiceController;
use App\Http\Controllers\NotificationServiceController;
use App\AppProperty;
use App\Auction;
use App\AuctionSession;
use App\CallRoute;
use App\CompanySetting;
use App\Libraries\ErrorCodes;

use App\Libraries\Helper;
use App\Libraries\PDFGeneratorService;
use App\Libraries\Permissions;

use App\Libraries\ResponseMessage;
use App\Location;
use App\RegionCommon;
use App\Transaction;
use App\User;

use App\ExternalOffer;
use App\Libraries\Event;
use App\OfferBid;
use App\Valuation;
use App\Libraries\PDFTemplate;

use Dompdf\Dompdf;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Prophecy\Call\Call;

class TestController extends Controller
{
    /**
     * TestController constructor.
     */
    public function __construct() {
        $this->code = 10;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function test(Request $request) {

        return CallRoute::test();
    }

    public function noti(Request $request) {
        CallRoute::test();
    }

    static function export() {
        $class = new GlassDataController();
        return $class->export();
    }

    function sendTest(Request $request) {
        return CallRoute::sendAll();

        return 'haha';
    }

    public function pdf(Request $request) {
        $user = new \stdClass();
        $user->id = 100;
        PDFTemplate::testPdf($user);
    }

    public function testNotification(Request $request, $userId)
    {
//        $userId = 4;
        $locationId = 1;
        $title = 'New Valuation Request 2';
        $message = 'A new valuation is just created';
        $customData = ['valuationId' => 1];
//        Helper::notify(Event::VALUATION_REQUEST, $locationId, $title, $message, $customData, Event::PARAMS_TYPE_LOCATION);
        Helper::notifyChannel(Event::VALUATION_REQUEST, $userId, $title, $message, $customData);
        return $this->responseData(['success' => TRUE]);
    }

    public function notice(Request $request, $userId)
    {
        $event = 'transaction_new';
        $sourceId = 1;
        $data = [];
        \LOG::debug("send notification/email to user $userId event $event");
        $user = User::find($userId);

        //todo: uncomment and complete dat code :(
        //generate document and up to azure
//        $pdf            = PDFTemplate::pdf($user, $event, $sourceId, $data);
//        $attach         = Document::uploadToAzure(Document::DOCUMENT_TYPE_PAYMENT_RECEIPT, $pdf, $sourceId);
//        $data['attach'] = $attach;

        try {
            \LOG::debug('--> 1000');
            \LOG::debug('NotificationServiceController::send');
            NotificationServiceController::send($user, $event, $sourceId, $data);
            MailServiceController::send($user, $event, $sourceId, $data);
        } catch(Exception $e) {
            \LOG::debug('--> 999'.$e);
        }
        return $this->responseData(['success' => TRUE]);
    }
}