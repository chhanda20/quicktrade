<?php

namespace App\Http\Controllers;

class StaticFileController extends Controller
{

    public function doc()
    {
        $path = __DIR__ . "/../../../doc/target/index.html";
        var_dump($path);
        $var = readfile($path);
        return $var;
    }

    public function main()
    {
        $path = __DIR__ . "/../../../doc/target/main.css";
        var_dump($path);
        $var = readfile($path);
        return $var;
    }

    public function bundle()
    {
        $path = __DIR__ . "/../../../doc/target/bundle.js";
        var_dump($path);
        $var = readfile($path);
        return $var;
    }
}
