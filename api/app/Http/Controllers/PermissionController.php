<?php

namespace App\Http\Controllers;

use App\Libraries\ErrorCodes;
use App\Libraries\Event;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\Permission;
use App\User;
use App\UserPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PermissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getAll(Request $request)
    {
        //this function will return list permission base on permission of logged in user permission_get or permission_admin_get
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::PERMISSION_ADMIN_GET);
        $permissionAdminGet = Helper::valuatePermission($user->id, $apiPermission);
        $where = "";
        if($permissionAdminGet){
            $r = Permission::getListPermission($where);
            return $this->responseData($r);
        }

        //only get permission for MO & Dealer or Dealer only
        $apiPermission = array(Permissions::PERMISSION_GET);
        $permissionGet = Helper::valuatePermission($user->id, $apiPermission);
        if($permissionGet){
            $where = " WHERE p.category <> 1 ";
            $r = Permission::getListPermission($where);
            return $this->responseData($r);
        }else{
            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

    }

    public function getAllPermissionSortByGroup(Request $request)
    {
        //this function will return list permission base on permission of logged in user permission_get or permission_admin_get
        //this permission return same data as getAll but different response structure,requested by Phong 26Jun2017
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::PERMISSION_ADMIN_GET);
        $permissionAdminGet = Helper::valuatePermission($user->id, $apiPermission);

        $apiPermission = array(Permissions::PERMISSION_GET);
        $permissionGet = Helper::valuatePermission($user->id, $apiPermission);

        $where = " ";
        if(!$permissionAdminGet && !$permissionGet)
            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);

        if($permissionGet && !$permissionAdminGet){
            //only get permission for MO & Dealer or Dealer only
            $where = " AND p.category <> 1 ";
        }
        //get all group permission
        $listGroup = Permission::getPermissionGroup();
        $result = array();
        foreach ($listGroup as $group){
            $query = " Where p.category = $group->category " .$where;
            $r = Permission::getListPermission($query);
            if($r){
                $permissions = array();
                $permissions["category_id"] = $group->category;
                $permissions["category_name"] = $group->category_name;
                $permissions["permissions"] = $r;
                array_push($result,$permissions);
            }
        }

        return $this->responseData($result);



    }

    public function getUserPermissions(Request $request, $userId)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::PERMISSION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }


        $u = User::find($userId);

        if (!$u) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        $r = DB::select('SELECT p.id,p.alias as name,p.code,p.category,p.is_enabled FROM permission p LEFT JOIN user_permission up ON p.id=up.permission_id WHERE up.user_id=:user_id', [
            'user_id' => $u->id
        ]);
        return $this->responseData($r);
    }

    public function setUserPermissions(Request $request, $userId)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::PERMISSION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'permissions' => 'required|array'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $u = User::find($userId);

        if (!$u) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        //remove user permission
        DB::statement("DELETE From user_permission WHERE user_id = $userId");
        //insert user permission
        $permissions = $request->json()->get('permissions');
        foreach ($permissions as $p) {
            if (Permission::find($p)) {
                $userPermission = new UserPermission();
                $userPermission->user_id = $userId;
                $userPermission->permission_id = $p;
                $userPermission->is_enabled = true;
                $userPermission->creator_id = $user->id;
                $userPermission->updater_id = $user->id;
                $userPermission->save();
            } else {
                \LOG::warn("There is no permission_id[$p]");
            }
        }

        return $this->responseData(['success' => TRUE]);
    }

    public function updatePermission(Request $request, $id){
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::PERMISSION_NAME_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'name' => 'required'

        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $permissionRecord = Permission::find($id);
        if(!$permissionRecord){
            return $this->responseError(ErrorCodes::INVALID_INPUT);
        }

        $permissionRecord->alias = $request->json()->get('name');
        $permissionRecord->description = $request->json()->get('description');
        $permissionRecord->updater_id = $user->id;
        $permissionRecord->save();
        return $this->responseData($permissionRecord);
    }
}
