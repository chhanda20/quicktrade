<?php

namespace App\Http\Controllers;

use App\Libraries\ErrorCodes;
use App\GlassModel;
use App\ModelOption;
use App\Option;
use App\User;
use App\Valuation;
use App\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class GlassModelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getFamily(Request $request, $makeName)
    {
        $makeName = urldecode($makeName);
        $token = $request->header('token');
        $glassModel = new GlassModel();

        $validationData = [
            'token' => $token,
            'makeName' => $makeName
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'makeName' => 'required|string'
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'string' => ErrorCodes::STRING,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
        ]);

        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $familyList = $glassModel->getFamilyList($makeName);
        $familyArr = [];
        foreach ($familyList as $family) {
            $familyArr[] = $family->family;
        }
        return $this->responseData(['family' => $familyArr]);
    }

    public function getVariant(Request $request, $makeName, $family)
    {
        $makeName = urldecode($makeName);
        $token = $request->header('token');
        $glassModel = new GlassModel();

        $validationData = [
            'token' => $token,
            'makeName' => $makeName,
            'family' => $family,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'makeName' => 'required|string',
            'family' => 'required|string',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'string' => ErrorCodes::STRING,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
        ]);

        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $variants = $glassModel->getVariant($makeName, $family);
        $variantArr = [];
        foreach ($variants as $variant) {
            $variantArr[] = $variant->variant;
        }
        return $this->responseData(['variant' => $variantArr]);
    }

    public function getReleaseYear(Request $request, $makeName, $family, $variant = '')
    {
        $makeName = urldecode($makeName);
        $variant = urldecode($variant);
        $token = $request->header('token');
        $glassModel = new GlassModel();

        $validationData = [
            'token' => $token,
            'makeName' => $makeName,
            'family' => $family
        ];

        $validationRules =   [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'makeName' => 'required|string',
            'family' => 'required|string',
        ];

        if ($variant) {
            $validationData['variant'] = $variant;
            $validationRules['variant'] =  'required|string';
        }

        $v = Validator::make($validationData, $validationRules, [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'string' => ErrorCodes::STRING,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
        ]);

        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $releaseYears = $glassModel->getReleaseYear($makeName, $family, $variant);
        $releaseYearArr = [];
        foreach ($releaseYears as $year) {
            $releaseYearArr[] = $year->release_year;
        }
        return $this->responseData(['releaseYear' => $releaseYearArr]);
    }

    public function getReleaseMonth(Request $request, $makeName, $family, $releaseYear, $variant = '')
    {
        $makeName = urldecode($makeName);
        $variant = urldecode($variant);
        $token = $request->header('token');
        $glassModel = new GlassModel();

        $validationData = [
            'token' => $token,
            'makeName' => $makeName,
            'family' => $family,
            'releaseYear' => $releaseYear,
        ];
        $validationRules = [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'makeName' => 'required|string',
            'family' => 'required|string',
            'releaseYear' => 'required|integer',
        ];

        if ($variant) {
            $validationData['variant'] = $variant;
            $validationRules['variant'] =  'required|string';
        }
        $v = Validator::make($validationData, $validationRules, [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'string' => ErrorCodes::STRING,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
        ]);

        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $releaseMonths = $glassModel->getReleaseMonth($makeName, $family, $variant, $releaseYear);
        $releaseMonthArr = [];
        foreach ($releaseMonths as $month) {
            $releaseMonthArr[] = $month->release_month;
        }
        return $this->responseData(['releaseMonth' => $releaseMonthArr]);
    }

    public function getModelDetail(Request $request, $valuationId, $makeName, $family, $releaseYear, $releaseMonth, $variant = '')
    {
        $makeName = urldecode($makeName);
        $variant = urldecode($variant);
        $token = $request->header('token');
        $glassModel = new GlassModel();

        $validationData = [
            'token' => $token,
            'makeName' => $makeName,
            'family' => $family,
            'releaseYear' => $releaseYear,
            'releaseMonth' => $releaseMonth,
            'valuationId' => $valuationId,
        ];

        $validationRules =  [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'makeName' => 'required|string',
            'family' => 'required|string',
            'releaseYear' => 'required|integer',
            'releaseMonth' => 'required|integer',
            'valuationId' => 'numeric|required|exists:mysql.valuation,id',
        ];

        if ($variant) {
            $validationData['variant'] = $variant;
            $validationRules['variant'] =  'required|string';
        }

        $v = Validator::make($validationData, $validationRules, [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'string' => ErrorCodes::STRING,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
        ]);

        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $modelDetail = $glassModel->getModelDetail($makeName, $family, $releaseYear, $releaseMonth, $variant);
        return $this->responseData($modelDetail);
    }

    public function getOptionList(Request $request, $glassCode) {
        $modelOptionModel = new ModelOption();
        $optionGroup = $modelOptionModel->getOptionByCode($glassCode);
        $optionList = [];
        if ($optionGroup && $optionGroup->option_group) {
            $options = explode(',', $optionGroup->option_group);
            foreach ($options as $option) {
                $optionArr[] = trim($option);
            }
            $optionStr = "'" . implode("','", $optionArr) . "'";
            $optionModel = new Option();
            $optionList = $optionModel->getOption($optionStr);
        }
        return $this->responseData($optionList);
    }


    public function getModelDetailByGlassCode(Request $request, $glassCode)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $sql = "
            SELECT RTRIM(gm.style) body_type, RTRIM(gm.engine_size) engine_size, RTRIM(gm.engine_capacity) engine_capacity, RTRIM(gm.cylinder) cylinder, RTRIM(gm.transmission) transmission, glass_code, TRIM(engine) engine
            FROM glass_model gm
            WHERE glass_code = ?
            LIMIT 0,1";
        $data = DB::select($sql, [$glassCode]);
        return $this->responseData($data);
    }

    private function addFilter($where, $condition)
    {
        if ($where) {
            return $where . ' AND ' . $condition;
        }
        return $condition;
    }

    public function findGlassCode(Request $request, $fieldName)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        //TODO: CHECKING PERMISSION

        $v = Validator::make($request->all(), [
            'make' => '',
            'family' => '',
            'variant' => '',
            'release_year' => '',
            'release_month' => '',
            'body_type' => '',
            'transmission' => ''
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        $input = $request->json();

        if ($fieldName == 'body_type') {
            $fieldName = 'style';
        }

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $query = 'SELECT DISTINCT TRIM(' .$fieldName. ') AS r FROM glass_model ';
        $where = '';

        $params = array();

        if($input->has('make')) {
            $make = $input->get('make');
            if (is_string($make)) {
                $where = $this->addFilter($where, 'make = :make');
                $params['make'] = $make;
            } else {
                if (sizeof($make) > 0) {
                    $condition = "";
                    foreach($make as $m) {
                        $x = str_replace('\'', '"', $m);
                        if ($condition == "") {
                            $condition = "make = '$x'";
                        } else {
                            $condition .= " OR make = '$x'";
                        }
                    }
                    $where = $this->addFilter($where, "($condition)");
                }
            }
        }

        if($input->has('family')) {
            $family = $input->get('family');

            if (is_string($family)) {
                $where = $this->addFilter($where, 'family = :family');
                $params['family'] = $family;
            } else {
                if (sizeof($family) > 0) {
                    $condition = "";
                    foreach($family as $m) {
                        $x = str_replace('\'', '"', $m);
                        if ($condition == "") {
                            $condition = "family = '$x'";
                        } else {
                            $condition .= " OR family = '$x'";
                        }
                    }
                    $where = $this->addFilter($where, "($condition)");
                }
            }
        }

        if($input->has('variant')) {
            $where = $this->addFilter($where, 'variant = :variant');
            $params['variant'] = $input->get("variant");
        }

        if($input->has('release_year')) {
            $where = $this->addFilter($where, 'release_year = :release_year');
            $params['release_year'] = $input->get("release_year");
        }

        if($input->has('release_month')) {
            $where = $this->addFilter($where, 'release_month = :release_month');
            $params['release_month'] = $input->get("release_month");
        }

        if($input->has('body_type')) {
            $where = $this->addFilter($where, 'style = :body_type');
            $params['body_type'] = $input->get("body_type");
        }

        if($input->has('transmission')) {
            $where = $this->addFilter($where, 'transmission = :transmission');
            $params['transmission'] = $input->get("transmission");
        }

        if ($where) {
            $query = $query . ' WHERE '. $where;
        }

        $data = DB::select($query, $params);

        $output = array_map(function ($object) { return $object->r; }, $data);

        return $this->responseData($output);
    }

}
