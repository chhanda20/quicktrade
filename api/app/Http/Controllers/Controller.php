<?php

namespace App\Http\Controllers;

use App\Libraries\ErrorCodes;
use App\Libraries\Helper;
use App\Libraries\ResponseMessage;
use Exception;
use App\User;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected $code = 0;

    /**
     * @param $request
     * @param null $permissions Array permission need check
     * @return null user
     * @throws Exception Exception(ErrorCodes::INVALID_TOKEN), etc...
     */
    public function currentUser($request, $permissions = NULL) {
        $token = $request->header("token");
        $user = NULL;

        if ($token != 'admin_siliconstack') {
            if ($token) {
                $user = User::where('token', $token)->first();
            }
            if (!$user) {
                throw new Exception(ErrorCodes::INVALID_TOKEN);
            }
            if ($permissions != NULL && count($permissions) != 0) {
                $this->checkPermission($user, $permissions);
            }
        }

        return $user;
    }

    public function checkPermission($user, $permissions) {
        $valuationResult = Helper::valuatePermission($user->id, $permissions);
        if(!$valuationResult){
            throw new Exception(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
    }

    public function responseError($code, $detail = NULL, $messageText = NULL, $messageKey = NULL, $statusCode = 400)
    {
        $responseMessage = new ResponseMessage();
        $responseMessage->setErrorCode($code);
        if ($detail) {
            $responseMessage->setErrorDetail($detail);
        }
        if ($messageText) {
            $responseMessage->setErrorMessage($messageText, $messageKey);
        }
        $result = $responseMessage->getMessage();
        return response()->json($result, $statusCode);
    }

    public function responseData($data, $statusCode = 200)
    {
        $responseMessage = new ResponseMessage();
        $responseMessage->setObjectData($data);
        $result = $responseMessage->getMessage();
        return response()->json($result, $statusCode);
    }

    public function response($code, $data = NULL, $message = NULL) {
        if ($message == NULL) {
            $message = $code == 200 ? "success" : "fail";
        }
        $reponseCode = $this->code."-".$code;

        $encode = [
            "code"      => $reponseCode,
            "data"      => $data,
            "data"      => $data,
            "message"   => $message
        ];
        return response()->json($encode);
    }
}
