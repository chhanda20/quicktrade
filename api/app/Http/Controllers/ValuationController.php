<?php

namespace App\Http\Controllers;

use App\AppProperty;
use App\Auction;
use App\Company;
use App\Document;
use App\ExternalOffer;
use App\Libraries\FeatureCode;
use App\Libraries\Helper;
use App\Libraries\IdTransformer;
use App\Libraries\PDFTemplate;
use App\Libraries\Permissions;
use App\Libraries\ResponseCodes;
use App\Libraries\VehicleService;
use App\Libraries\ErrorCodes;
use App\Libraries\Notification;
use App\Libraries\Event;

use App\Contact;
use App\Location;
use App\OfferBid;
use App\Role;
use App\Transaction;
use App\TypeCode;
use App\User;
use App\Valuation;
use App\Vehicle;
use App\ModelOption;
use App\Option;
use App\ValuationInspectionItem;
use App\InspectionItemImage;
use App\Channel;
use App\Make;


use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\CountValidator\Exception;

class ValuationController extends Controller
{
    public function __construct() {
        $this->code = FeatureCode::VALUATION;
    }

    public function findCount(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }


        $isMo  = $user->isMo();

        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'status' => 'required',
            'query' => 'max:100'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $user_company_group_id = $user->company_group_id;
//        if ($user->location_id) {
//            $location = DB::table("location")->where("id", $user->location_id)->first();
//            if ($location) {
//                $company = Company::find($location->company_id);
//                if ($company) {
//                    $user_company_group_id = $company->company_id;
//                }
//            }
//        }

        if (!$user_company_group_id) {
            Log::warn("User[$user_company_group_id] has company_group_id = NULL");
            return $this->responseError(ErrorCodes::ACCESS_DENIED);
        }

        //TODO: CHECKING PERMISSION

        $company_group_id = $user_company_group_id;
        $status = $request->json()->get('status');
        $query = $request->json()->get('query');
        $userLocationAccess = implode(',', $user->getAccessableLocationIds());
        if ($status == Valuation::STATUS_PENDING) {
            $result = Valuation::searchPendingCount($company_group_id, $status, TRUE, $user->id, $query, $isMo);
        } else {
            $result = Valuation::searchCount($company_group_id, $status, $query, $userLocationAccess, $isMo);
        }
        return $this->responseData($result);
    }

    public function find(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'status' => 'required',
            'query' => 'max:100',
            'limit' => 'required|integer|min:1|max:100',
            'offset' => 'required|integer|min:0',
            'orderBy' => 'in:make,model,valuation_create_date',
            'orderDesc' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $user_company_group_id = $user->company_group_id;
//        if ($user->location_id) {
//            $location = DB::table("location")->where("id", $user->location_id)->first();
//            if ($location) {
//                $user_company_id = $location->company_id;
//            }
//        }

        if (!$user_company_group_id) {
            Log::warn("User[$user->id] has company_id = NULL");
            return $this->responseError(ErrorCodes::ACCESS_DENIED);
        }

        //TODO: CHECKING PERMISSION

        $view_as_mo        = $request->json()->get('view_as_mo') ? true : false;
        $company_group_id   = $user_company_group_id;
        $status             = $request->json()->get('status');
        $limit              = $request->json()->get('limit');
        $offset             = $request->json()->get('offset');
        $orderBy            = $request->json()->get('orderBy');
        $orderDesc          = $request->json()->get('orderDesc');
        $query              = $request->json()->get('query');
        $limitUserId        = $request->json()->get('ownStat') ? $user->id : NULL;

        $isMo              = $user->company_group_id == 1 && $view_as_mo;

        if (!$orderBy) {
            $orderBy = 'valuation_create_date';
            $orderDesc = TRUE;
        }
       // dd($user->getAccessableLocationIds());
        $userLocationAccess = implode(',', $user->getAccessableLocationIds());

        if ($status == Valuation::STATUS_PENDING) {
            $result = Valuation::searchPending($company_group_id, $offset, $limit, $orderBy, $orderDesc, TRUE, $user->id, $query, $isMo);
        } else {
            $result = Valuation::search($company_group_id, $offset, $limit, $status, $orderBy, $orderDesc, $query, $userLocationAccess, $limitUserId, $user->location_id, $isMo);
        }

        foreach ($result as $item) {
            $item->ownership_type = json_decode($item->ownership_type, true);
        }

        return $this->responseData($result);
    }


    public function delete(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_DELETE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, ['id' => ErrorCodes::NOT_EXISTS], 'No such valuation');
        }

        $valuation->status = Valuation::STATUS_DELETED;
        //delete valuation in DB
//        $valuation->save();
        $valuation->delete();
        return $this->responseData($valuation);
    }

    public function begin(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION

        $apiPermission = array(Permissions::VALUATION_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $v = Validator::make($request->all(), [
            'customer_id' => 'required|integer',
            'vin' => 'max:100',
            'build_date' => 'string',
            'variant' => '',
            'registration_number' => 'max:100',
            'make' => '',
            'model_family' => 'max:45',
            'colour' => 'integer',
            'registration_expiry_date' => 'date',
            'has_one_owner' => 'boolean',
            'has_service_book' => 'boolean',
            'has_service_history' => 'boolean',
            'keys_location' => 'max:100',
            'compliance_date' => 'date',
            'odometer' => 'integer',
            'engine_number' => 'max:100',
            'i_vehicle' => 'integer',
            'i_make' => '',
            'i_model' => '',
            'i_variant' => '',
            'i_institution' => '',
            'i_payout' => '',
            'i_monthly_payment' => '',
            'i_finance_owe' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $input = $request->json();
        LOG::debug("called begin");
        LOG::debug((string)$request);

        $vehicle = NULL;
        $vin = $input->get('vin');
        $build_date = $input->get('build_date');

        if ($vin && !empty($vin)) {
            if (!$build_date) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, ['year' => ErrorCodes::INVALID], 'Year can not be empty since VIN is not.');
            }

            // GET Vehicle Id & update info;
            $vehicleData = VehicleService::getVehicle($vin, $build_date);

            $vehicle = Vehicle::where('vin', $vin)->first();
        } else {
            $vin = NULL; // set to null in case the vin = "";
        }

        //create or update vehicle;

        if (!$vehicle) {
            $vehicle = new Vehicle();

            if ($input->has('vin')) {
                $vehicle->vin = $input->get('vin');
            }

            if ($input->has('build_date')) {
                $vehicle->build_date = $input->get('build_date');
            }

            if ($input->has('registration_number')) {
                $vehicle->registration_number = $input->get('registration_number');
            }

            if ($input->has('make')) {
                $vehicle->make = $input->get('make');
            }

            if ($input->has('model_family')) {
                $vehicle->model_family = $input->get('model_family');
            }

            if ($input->has('variant')) {
                $vehicle->variant = $input->get('variant');
            }

            if ($input->has('colour')) {
                $vehicle->colour = $input->get('colour');
            }

            if ($input->has('registration_expiry_date')) {
                $vehicle->registration_expiry_date = $input->get('registration_expiry_date');
            }

            if ($input->has('keys_location')) {
                $vehicle->keys_location = $input->get('keys_location');
            }

            if ($input->has('compliance_date')) {
                $vehicle->compliance_date = $input->get('compliance_date');
            }

            if ($input->has('odometer')) {
                $vehicle->odometer = $input->get('odometer');
            }

            if ($input->has('engine_number')) {
                $vehicle->engine_number = $input->get('engine_number');
            }

            $vehicle->save();
        }

        $valuation = new Valuation();
        $valuation->valuer_id = NULL;
        $valuation->has_service_book = $input->get('has_service_book');
        $valuation->has_service_history = $input->get('has_service_history');
        $valuation->has_one_owner = $input->get('has_one_owner');
        $valuation->status = Valuation::STATUS_BEGIN;
        $valuation->company_group_id = $user->company_group_id;

        // Validate requester
        $customer = Contact::find($input->get('customer_id'));
        if (!$customer) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, ['customer_id' => [ErrorCodes::NOT_EXISTS]]);
        }

//        $user_company_id = NULL;
//        if ($user->location_id) {
//            $location = DB::table("location")->where("id", $user->location_id)->first();
//            if ($location) {
//                $user_company_id = $location->company_id;
//            }
//        }
//        if (!$user_company_id) {
//            Log::warn("User[$user_company_id] has company_id = NULL");
//            return $this->responseError(ErrorCodes::ACCESS_DENIED);
//        }

        $valuation->customer_id = $customer->id;
        $valuation->user_id = $user->id; // creator
        $valuation->version_creator = $user->id;

        if ($vehicle) {
            $valuation->vehicle_id = $vehicle->id;
        }


//        if ($customer->company_id != $user_company_id) {
//            return $this->responseError(ErrorCodes::INVALID_INPUT, ['customer_id' => [ErrorCodes::INVALID]]);
//        }

//        $valuation->company_id = $user_company_id;

        // Validate vehicle_id

        if ($valuation->vehicle_id) {
            $vehicle = Vehicle::find($valuation->vehicle_id);
            if (!$vehicle) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, ['vehicle_id' => [ErrorCodes::NOT_EXISTS]]);
            }
        }

        $valuation->keys_location = $input->get('keys_location');

        $valuation->i_finance_owe = $input->get('i_finance_owe');
        $valuation->i_institution = $input->get('i_institution');
        $valuation->i_payout = $input->get('i_payout');
        $valuation->i_monthly_payment = $input->get('i_monthly_payment');

        $valuation->i_vehicle = $input->get('i_vehicle');
        $valuation->i_make = $input->get('i_make');
        $valuation->i_model = $input->get('i_model');
        $valuation->i_variant = $input->get('i_variant');
        $valuation->repairable_write_off = $input->get('repairable_write_off');
        if($input->get('special_disclosures') != NULL){
            $valuation->special_disclosures = implode(",",$input->get('special_disclosures'));
        }
        $valuation->ownership_type = json_encode($input->get('ownership_type'));
        $valuation->save();
        $valuation->ownership_type = json_decode($valuation->ownership_type, true);
        return $this->responseData($valuation);
    }

    public function requestValuation(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $apiPermission = array(Permissions::VALUATION_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'start_performing' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, ['id' => ErrorCodes::NOT_EXISTS], 'No such valuation');
        }

        if ($request->json()->has('start_performing') && $request->json()->get('start_performing')) {
            $valuation->status = Valuation::STATUS_IN_PROCESS;
            $valuation->valuer_id = $user->id;
            $valuation->save();
        } else {
            $valuation->status = Valuation::STATUS_WAITING;
            $valuation->save();

            $vehicle = Vehicle::find($valuation->vehicle_id);

            $title = 'New Valuation Request';
            $message = '';
            $webMessage = '';
            $msgParts = [];
            $webMsgParts = [];

            if ($vehicle->buildDate) {
                $msgParts[] = $vehicle->buildDate;
                $webMsgParts[] = $vehicle->buildDate;
            }


            $msgParts[] = trim($vehicle->make);
            $webMsgParts[] = trim($vehicle->make);

            if ($vehicle->model_family) {
                $msgParts[] = $vehicle->model_family;
                $webMsgParts[] = $vehicle->model_family;
            }

            if ($vehicle->colour) {
                $message .= " ";
                $colorRow = TypeCode::where('type', 'COLOR_TYPE')->where('code', $vehicle->colour)->first();
                if ($colorRow) {
                    $msgParts[] = trim($colorRow->value);
                    $webMsgParts[] = trim($colorRow->value);
                }
            }

            if ($vehicle->registration_number) {
                $msgParts[] = $vehicle->registration_number;
            }

            if ($msgParts) {
                $message = implode(' ', $msgParts);
            }
            if ($webMsgParts) {
                $webMessage = implode(' ', $webMsgParts);
            }

            $customData = [
                'valuationId' => $valuation->id,
                'time' => date('Y-m-d H:i:s'),
                'web_message' => $webMessage,
                'registration_number' => $vehicle->registration_number,
            ];

            $permissions = [Permissions::VALUATION_PERFORM, Permissions::NOTIFICATION_HEAD_VALUER];
            User::noticePermission($user->location_id, Event::VALUATION_REQUEST, $valuation->id, $permissions, $customData);
        }

        return $this->responseData($valuation);
    }

    public function create(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::VALUATION_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        //handle api
        $v = Validator::make($request->all(), [
            'customer_id' => 'required|integer',
            'vin' => 'max:100',
            'build_date' => 'string',
            'variant' => '',
            'registration_number' => 'max:100',
            'make' => '',
            'model_family' => 'max:45',
            'colour' => 'integer',
            'registration_expiry_date' => 'date',
            'has_one_owner' => 'boolean',
            'has_service_book' => 'boolean',
            'has_service_history' => 'boolean',
            'keys_location' => 'max:100',
            'compliance_date' => 'date',
            'odometer' => 'integer',
            'engine_number' => 'max:100',
            'i_vehicle' => 'integer',
            'i_make' => '',
            'i_model' => '',
            'i_variant' => '',
            'i_institution' => '',
            'i_payout' => '',
            'i_monthly_payment' => '',
            'i_finance_owe' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $input = $request->json();

        LOG::debug((string)$request);

        $vehicle = NULL;
        $vin = $input->get('vin');
        $build_date =  strtotime($input->get('build_date'));
        $year = date('Y', $build_date);

        if ($vin && !empty($vin)) {
            if (!$year) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, ['year' => ErrorCodes::INVALID], 'Year can not be empty since VIN is not.');
            }

            // GET Vehicle Id & update info;
            $vehicleData = VehicleService::getVehicle($vin, $year);

            $vehicle = Vehicle::where('vin', $vin)->first();
        } else {
            $vin = NULL; // set to null in case the vin = "";
        }

        if (!$vehicle) {
            $vehicle = new Vehicle();
        }

        //create or update vehicle;

        if ($input->has('vin')) {
            $vehicle->vin = $input->get('vin');
        }

        if ($input->has('build_date')) {
            $vehicle->build_date = $input->get('build_date');
        }

        if ($input->has('registration_number')) {
            $vehicle->registration_number = $input->get('registration_number');
        }

        if ($input->has('make')) {
            $vehicle->make = $input->get('make');
        }

        if ($input->has('model_family')) {
            $vehicle->model_family = $input->get('model_family');
        }

        if ($input->has('variant')) {
            $vehicle->variant = $input->get('variant');
        }

        if ($input->has('colour')) {
            $vehicle->colour = $input->get('colour');
        }

        if ($input->has('registration_expiry_date')) {
            $vehicle->registration_expiry_date = $input->get('registration_expiry_date');
        }


        if ($input->has('keys_location')) {
            $vehicle->keys_location = $input->get('keys_location');
        }

        if ($input->has('compliance_date')) {
            $vehicle->compliance_date = $input->get('compliance_date');
        }

        if ($input->has('odometer')) {
            $vehicle->odometer = $input->get('odometer');
        }

        if ($input->has('engine_number')) {
            $vehicle->engine_number = $input->get('engine_number');
        }

        $vehicle->vin = $vin;
//        $vehicle->build_date = date_create();
//        $vehicle->build_date->setDate($year, 1, 1);

        $vehicle->save();

        $valuation = new Valuation();
        $valuation->valuer_id = NULL;
        $valuation->has_service_book = $input->get('has_service_book');
        $valuation->has_service_history = $input->get('has_service_history');
        $valuation->has_one_owner = $input->get('has_one_owner');
        $valuation->company_group_id = $user->company_group_id;
        $valuation->status = Valuation::STATUS_WAITING;


        if ($input->has('has_one_owner')) {
            $valuation->has_one_owner = $input->get('has_one_owner');
        }

        if ($input->has('has_service_book')) {
            $valuation->has_service_book = $input->get('has_service_book');
        }

        if ($input->has('has_service_history')) {
            $valuation->has_service_history = $input->get('has_service_history');
        }

        // Validate requester
        $customer = Contact::find($input->get('customer_id'));
        if (!$customer) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, ['customer_id' => [ErrorCodes::NOT_EXISTS]]);
        }

//        $user_company_id = NULL;
//        if ($user->location_id) {
//            $location = DB::table("location")->where("id", $user->location_id)->first();
//            if ($location) {
//                $user_company_id = $location->company_id;
//            }
//        }
//        if (!$user_company_id) {
//            Log::warn("User[$user_company_id] has company_id = NULL");
//            return $this->responseError(ErrorCodes::ACCESS_DENIED);
//        }

        $valuation->customer_id = $customer->id;
        $valuation->user_id = $user->id; // creator
        $valuation->version_creator = $user->id;

        if ($vehicle) {
            $valuation->vehicle_id = $vehicle->id;
        }


//        if ($customer->company_id != $user_company_id) {
//            return $this->responseError(ErrorCodes::INVALID_INPUT, ['customer_id' => [ErrorCodes::INVALID]]);
//        }

//        $valuation->company_id = $user_company_id;

        // Validate vehicle_id
        if ($valuation->vehicle_id) {
            $vehicle = Vehicle::find($valuation->vehicle_id);
            if (!$vehicle) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, ['vehicle_id' => [ErrorCodes::NOT_EXISTS]]);
            }
        }

        $valuation->keys_location = $input->get('keys_location');
        $valuation->i_finance_owe = $input->get('i_finance_owe');
        $valuation->i_institution = $input->get('i_institution');
        $valuation->i_payout = $input->get('i_payout');
        $valuation->i_monthly_payment = $input->get('i_monthly_payment');

        $valuation->i_vehicle = $input->get('i_vehicle');
        $valuation->i_make = $input->get('i_make');
        $valuation->i_model = $input->get('i_model');
        $valuation->i_variant = $input->get('i_variant');
        $valuation->repairable_write_off = $input->get('repairable_write_off');
        if($input->has('special_disclosures')){
            $valuation->special_disclosures = implode(",",$input->get('special_disclosures'));
        }
        if($input->has('ownership_type')){
            $valuation->ownership_type = json_encode($input->get('ownership_type'));
        }
        $valuation->save();

        $title = 'New Valuation Request';
        $message = '';
        $webMessage = '';
        $msgParts = [];
        $webMsgParts = [];

        if ($year) {
            $msgParts[] = $year;
            $webMsgParts[] = $year;
        }

        $msgParts[] = trim($vehicle->make);
        $webMsgParts[] = trim($vehicle->make);

        if ($vehicle->model_family) {
            $msgParts[] = $vehicle->model_family;
            $webMsgParts[] = $vehicle->model_family;
        }

        if ($vehicle->colour) {
            $message .= " ";
            $colorRow = TypeCode::where('type', 'COLOR_TYPE')->where('code', $vehicle->colour)->first();
            if ($colorRow) {
                $msgParts[] = trim($colorRow->value);
                $webMsgParts[] = trim($colorRow->value);
            }
        }

        if ($vehicle->registration_number) {
            $msgParts[] = $vehicle->registration_number;
        }

        if ($msgParts) {
            $message = implode(' ', $msgParts);
        }
        if ($webMsgParts) {
            $webMessage = implode(' ', $webMsgParts);
        }

        $customData = [
            'valuationId' => $valuation->id,
            'time' => date('Y-m-d H:i:s'),
            'web_message' => $webMessage,
            'registration_number' => $vehicle->registration_number,
        ];
        $permissions = [Permissions::VALUATION_PERFORM, Permissions::NOTIFICATION_HEAD_VALUER];
        User::noticePermission($user->location_id, Event::VALUATION_REQUEST, $valuation->id, $permissions, $customData);

        $customer->interested_vehicle = $input->get('interested_vehicle');
        $customer->save();


        return $this->responseData($valuation);
    }


    public function cancel(Request $request)
    {

        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }
        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        //checking permission
        $apiPermission = array(Permissions::VALUATION_CANCEL);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        //Getting current user's company_id
//        if ($user->location_id) {
//            $location = DB::table("location")->where("id", $user->location_id)->first();
//            if ($location) {
//                $company_id = $location->company_id;
//            }
//        }

        //TODO: CHECKING PERMISSION

        $v = Validator::make($request->all(), [
            'id' => 'required|integer'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $id = $request->json()->get('id');
        $valuationModel = new Valuation();
        $valuation = $valuationModel->getValuationById($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, ['id' => ErrorCodes::NOT_EXISTS], 'No such valuation');
        }

        if ($valuation->status != Valuation::STATUS_IN_PROCESS) {
            return $this->responseError(ErrorCodes::INVALID, ['status' => ErrorCodes::INVALID], "Not in progress");
        }

        DB::table('valuation')->where('id', $id)->update(['status' => Valuation::STATUS_WAITING]);
//        DB::table('valuation')->where('id', $id)->delete();
        $title = 'A Valuation Is Canceled';
        $message = '';
        $webMessage = '';
        $msgParts = [];
        $webMsgParts = [];

        if ($valuation->year) {
            $msgParts[] = $valuation->year;
            $webMsgParts[] = $valuation->year;
        }

        if ($valuation->make) {
            $msgParts[] = $valuation->make;
            $webMsgParts[] = $valuation->make;
        }

        if ($valuation->model_family) {
            $msgParts[] = $valuation->model_family;
            $webMsgParts[] = $valuation->model_family;
        }

        if ($valuation->colour) {
            $msgParts[] = $valuation->colour;
            $webMsgParts[] = $valuation->colour;
        }

        if ($valuation->registration_number) {
            $msgParts[] = $valuation->registration_number;
        }

        if ($msgParts) {
            $message = implode(' ', $msgParts);
        }

        if ($webMsgParts) {
            $webMessage = implode(' ', $webMsgParts);
        }

        $va = Valuation::find($valuation->id);
        $va->notify_sent_counter = 0;
        $va->save();

        $customData = [
            'valuationId' => $valuation->id,
            'time' => date('Y-m-d H:i:s'),
            'web_message' => $webMessage,
        ];

        $permissions = [
            Permissions::VALUATION_CANCEL, 
            Permissions::VALUATION_PERFORM
        ];
        User::noticePermission($user->location_id, Event::VALUATION_CANCEL, $valuation->id, $permissions, $customData);
        
        $permissions = [Permissions::VALUATION_PERFORM, Permissions::NOTIFICATION_HEAD_VALUER];
        User::noticePermission($user->location_id, Event::VALUATION_REQUEST, $valuation->id, $permissions, $customData);

        return $this->responseData($valuation);
    }

    public function update(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        if (!$this->isReadyForUpdate($id)) {
            return $this->responseError(ResponseCodes::NOT_AVAILABLE_TO_EDIT);
//            [fix-me]
//            return $this->response(Resp, NULL, 'Valuation is not allow to edit');
        }

        $apiPermission = array(Permissions::VALUATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        //Getting current user's company_id
//        if ($user->location_id) {
//            $location = DB::table("location")->where("id", $user->location_id)->first();
//            if ($location) {
//                $company_id = $location->company_id;
//            }
//        }

        //TODO: CHECKING PERMISSION

        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, ['id' => ErrorCodes::NOT_EXISTS], 'No such valuation');
        }

        if ($valuation->status == Valuation::STATUS_COMPLETE_TRADED_IN) {
            return $this->responseError(ErrorCodes::INVALID, ['status' => ErrorCodes::INVALID], "is in traded_in");
        }

        $vehicle = NULL;

        if ($valuation->vehicle_id) {
            $vehicle = Vehicle::find($valuation->vehicle_id);
        }
        if ($vehicle) {
            $vin = $request->json()->get("vin");
            if ($vin && $vehicle->vin != $vin) {
                $otherVehicle = Vehicle::find($vin);
                if ($otherVehicle) {
                    $otherVehicle->fill((array)$vehicle);
                    $vehicle = $otherVehicle;

                    $valuation->vehicle_id = $vehicle->id;
                    $valuation->save();
                }
            }
        } else {
            $vehicle = new Vehicle();
        }

        $input = $request->toArray();
        $input = array_filter($input, 'strlen'); // remove null values

        $vehicle->fill($input);
        $vehicle->save();


        $valuation->fill($input);
        $valuation->save();

        return $this->responseData($valuation);
    }

    public function getValuationVehicle(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VEHICLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        //Getting current user's company_id
//        if ($user->location_id) {
//            $location = DB::table("location")->where("id", $user->location_id)->first();
//            if ($location) {
//                $company_id = $location->company_id;
//            }
//        }

        //TODO: CHECKING PERMISSION

        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, ['id' => ErrorCodes::NOT_EXISTS], 'No such valuation');
        }

        $vehicle = NULL;

        if ($valuation->vehicle_id) {
            $vehicle = Vehicle::find($valuation->vehicle_id);
        }

        if ($vehicle) {
            return $this->responseData($vehicle);
        }
        return $this->responseError(ErrorCodes::NOT_EXISTS, ['id' => ErrorCodes::NOT_EXISTS], 'No such vehicle');
    }

    public function test(Request $request, $id)
    {

        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::NOT_EXISTS);
        }

        return $this->responseData($valuation);
    }


    public function accept(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_PERFORM);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        //Getting current user's company_id
//        if ($user->location_id) {
//            $location = DB::table("location")->where("id", $user->location_id)->first();
//            if ($location) {
//                $company_id = $location->company_id;
//            }
//        }

        //TODO: CHECKING PERMISSION

        $valuationModel = new Valuation();
        $valuation = $valuationModel->getValuationById($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, ['id' => ErrorCodes::NOT_EXISTS], 'No such valuation');
        }

        if ($valuation->status != Valuation::STATUS_WAITING) {
            return $this->responseError(ErrorCodes::INVALID, ['status' => ErrorCodes::INVALID], "Not in waiting queue");
        }

        DB::table('valuation')->where('id', $id)->where('status', Valuation::STATUS_WAITING)
            ->update([
                'status' => Valuation::STATUS_IN_PROCESS,
                'valuer_id' => $user->id
            ]);
//        DB::query("UPDATE `valuation` SET status=:new_status, valuer_id=:user_id WHERE status=:current_status AND id=:valuation_id",
//            ['new_status' => Valuation::STATUS_IN_PROCESS,
//                'current_status' => Valuation::STATUS_WAITING,
//                'valuer_id' => $user->id,
//                'valuation_id' => $id
//            ]);

        // Don't send notification to user who accepted the request
        // $otherValuerChannel = Channel::where('name', 'other_valuer')->first();
        // DB::table('channel_user')
        //     ->where('channel_id', $otherValuerChannel->id)
        //     ->where('channel_id', $user->id)
        //     ->delete();

        $title = 'Valuation Accepted';

        $message = '';
        $webMessage = '';
        $msgParts = [];
        $webMsgParts = [];

        if ($valuation->year) {
            $msgParts[] = $valuation->year;
            $webMsgParts[] = $valuation->year;
        }

        $msgParts[] = trim($valuation->make);
        $webMsgParts[] = trim($valuation->make);

        if ($valuation->model_family) {
            $msgParts[] = $valuation->model_family;
            $webMsgParts[] = $valuation->model_family;
        }

        if ($valuation->colour) {
            $message .= " ";
            $colorRow = TypeCode::where('type', 'COLOR_TYPE')->where('code', $valuation->colour)->first();
            if ($colorRow) {
                $msgParts[] = trim($colorRow->value);
                $webMsgParts[] = trim($colorRow->value);
            }
        }

        if ($valuation->registration_number) {
            $msgParts[] = $valuation->registration_number;
        }

        if ($msgParts) {
            $message = implode(' ', $msgParts);
        }
        if ($webMsgParts) {
            $webMessage = implode(' ', $webMsgParts);
        }

        try {
            $customData = [
                'valuationId' => $valuation->id,
                'userId' => $user->id,
                'time' => date('Y-m-d H:i:s'),
                'web_message' => $webMessage,
                'registration_number' => $valuation->registration_number,
            ];
            $permissions = [Permissions::VALUATION_PERFORM, Permissions::NOTIFICATION_HEAD_VALUER];
            User::noticePermission($user->location_id, Event::VALUATION_ACCEPTED, $valuation->id, $permissions, $customData);
        } catch (Exception $e) {
            \LOG::error($e->getMessage());
        }

        $valuation = Valuation::find($id);
        $valuationDuration = 20; // minutes
        $valuation->valuation_duration = $valuationDuration * 60 * 1000;
        $valuation->duration_unit = 'millisecond';
        $valuation->ownership_type = json_decode($valuation->ownership_type, true);

        if ($valuation->valuer_id == $user->id && $valuation->status == Valuation::STATUS_IN_PROCESS) {
            return $this->responseData($valuation);
        }

        return $this->responseError(ErrorCodes::ACCEPTED, ['valuer_id' => $valuation->valuer_id]);
    }

    public function getVehicleDetail(Request $request, $valuationId)
    {
        $token = $request->header('token');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        //checking permission
        $apiPermission = array(Permissions::VEHICLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'valuationId' => $valuationId,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'valuationId' => 'numeric|required|exists:mysql.valuation,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $valuationModel = new Valuation();
        $valuationDetail = $valuationModel->getVehicleDetail($valuationId);
        if (!$valuationDetail) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        $data = $valuationDetail;

        $optionList = NULL;
        $glassCode = $valuationDetail->glass_guide_code;
        $modelOptionModel = new ModelOption();
        $optionGroup = $modelOptionModel->getOptionByCode($glassCode);
        if ($optionGroup && $optionGroup->option_group) {
            $options = explode(',', $optionGroup->option_group);
            foreach ($options as $option) {
                $optionArr[] = trim($option);
            }
            $optionStr = "'" . implode("','", $optionArr) . "'";
            $optionModel = new Option();
            $optionList = $optionModel->getOption($optionStr);
        }
        $data->option_list = $optionList;
        return $this->responseData($data);
    }

    public function addInspectionItem(Request $request)
    {
        $token = $request->header('token');

        $valuationId = $request->json()->get('valuationId');
        $inspectionItemId = $request->json()->get('inspectionItemId');
        $repairCost = $request->json()->get('repairCost');
        $note = $request->json()->get('note');


        $validationData = [
            'token' => $token,
            'valuationId' => $valuationId,
            'inspectionItemId' => $inspectionItemId,
            'repairCost' => $repairCost,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'valuationId' => 'numeric|required|exists:mysql.valuation,id',
            'inspectionItemId' => 'numeric|required|exists:mysql.inspection_item,id',
            'repairCost' => 'numeric|required',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
            'array' => ErrorCodes::ARRAY_TYPE,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $item = ValuationInspectionItem::where('valuation_id', $valuationId)
            ->where('inspection_item_id', $inspectionItemId)
            ->first();
        if ($item) {
            return $this->responseError(ErrorCodes::EXISTED, "Duplicate entry (valuationId, inspectionItemId)");
        }

        $user = User::where('token', $token)->first();
        //checking permission

        $apiPermission = array(Permissions::VALUATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $valuationIIModel = new ValuationInspectionItem();
        $valuationIIModel->valuation_id = $valuationId;
        $valuationIIModel->inspection_item_id = $inspectionItemId;
        $valuationIIModel->repair_cost = $repairCost;
        $valuationIIModel->note = $note;
        $valuationIIModel->creator_id = $user->id;
        $valuationIIModel->updater_id = $user->id;
        $valuationIIModel->save();

        $this->updateTotalRecondictionCost($valuationId);

        return $this->responseData(['success' => TRUE]);
    }

    public function addInspectionItemGroup(Request $request, $valuationId, $sectionType)
    {
        $token = $request->header('token');
        $user = User::where('token', $token)->first();

        //checking permission
        $apiPermission = array(Permissions::VALUATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $items = $request->json()->get('items');

        $validationData = [
            'token' => $token,
            'valuationId' => $valuationId,
            'sectionType' => $sectionType,
            'items' => $items
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'valuationId' => 'numeric|required|exists:mysql.valuation,id',
            'sectionType' => 'string|required',
            'items' => 'array|required'
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'array' => ErrorCodes::ARRAY_TYPE,
            'string' => ErrorCodes::STRING
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }
        ValuationInspectionItem::deleteBySection($valuationId, $sectionType);
        foreach ($items as $item) {

            $inspectionItemId = isset($item['inspectionItemId']) ? $item['inspectionItemId'] : '';
            $repairCost = isset($item['repairCost']) ? $item['repairCost'] : '';
            $note = isset($item['note']) ? $item['note'] : '';

            $validationItemData = [
                'inspectionItemId' => $inspectionItemId,
                'repairCost' => $repairCost,
            ];
            $vItem = Validator::make($validationItemData, [
                'inspectionItemId' => 'numeric|required|exists:mysql.inspection_item,id',
                'repairCost' => 'numeric|required'
            ], [
                'required' => ErrorCodes::REQUIRED,
                'alpha_num' => ErrorCodes::ALPHA_NUM,
                'exists' => ErrorCodes::NOT_EXISTS,
                'numeric' => ErrorCodes::NUMERIC,
                'array' => ErrorCodes::ARRAY_TYPE,
            ]);
            if ($vItem->fails()) {
                $errorMessages = $vItem->errors()->getMessages();
                return $this->responseError(ErrorCodes::INVALID, $errorMessages);
            }

            $itemModel = ValuationInspectionItem::where('valuation_id', $valuationId)
                ->where('inspection_item_id', $inspectionItemId)
                ->first();
            if ($itemModel) {
                return $this->responseError(ErrorCodes::EXISTED, "Duplicate entry (valuationId: $valuationId, inspectionItemId: $inspectionItemId)");
            }

            $valuationIIModel = new ValuationInspectionItem();
            $valuationIIModel->valuation_id = $valuationId;
            $valuationIIModel->inspection_item_id = $inspectionItemId;
            $valuationIIModel->repair_cost = $repairCost;
            $valuationIIModel->note = $note;
            $valuationIIModel->creator_id = $user->id;
            $valuationIIModel->updater_id = $user->id;
            $valuationIIModel->save();

            $this->updateTotalRecondictionCost($valuationId);
        }
        return $this->responseData(['success' => TRUE]);

    }

    public function getInspectionItem(Request $request, $valuationId, $sectionGroup)
    {
        $token = $request->header('token');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'valuationId' => $valuationId,
            'sectionGroup' => $sectionGroup
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'valuationId' => 'numeric|required|exists:mysql.valuation,id',
            'sectionGroup' => 'alpha_dash|required',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
            'alpha_dash' => ErrorCodes::ALPHA_DASH,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $valuationIItemModel = new ValuationInspectionItem();
        $valuationSections = $valuationIItemModel->getSection($valuationId, $sectionGroup);
        $data = array();
        foreach ($valuationSections as $section) {
            $section->items = $valuationIItemModel->getItems($valuationId, $section->section_type);
            $data[] = $section;
        }
        return $this->responseData($data);
    }

    public function getAllInspectionItem(Request $request, $valuationId)
    {
        $token = $request->header('token');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'valuationId' => $valuationId
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'valuationId' => 'numeric|required|exists:mysql.valuation,id'
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
            'alpha_dash' => ErrorCodes::ALPHA_DASH,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $valuationIItemModel = new ValuationInspectionItem();
        $sectionGroups = [
            ValuationInspectionItem::EXTERIOR_SECTION,
            ValuationInspectionItem::INTERIOR_SECTION,
            ValuationInspectionItem::MECHANICAL_SECTION
        ];
        $sectionGroupObj = new \stdClass();

        foreach ($sectionGroups as $sectionGroup) {
            $data = [];
            $property = strtolower($sectionGroup);
            $valuationSections = $valuationIItemModel->getSection($valuationId, $sectionGroup);
            foreach ($valuationSections as $section) {
                $section->items = $valuationIItemModel->getItems($valuationId, $section->section_type);
                $data[] = $section;
            }
            $sectionGroupObj->$property = $data;
        }
        return $this->responseData($sectionGroupObj);
    }

    public function getValuationDetail(Request $request, $valuationId)
    {
        $token = $request->header('token');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        //checking permission
        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $validationData = [
            'token' => $token,
            'valuationId' => $valuationId
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'valuationId' => 'numeric|required|exists:mysql.valuation,id'
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
            'alpha' => ErrorCodes::ALPHA,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $valuationModel = new Valuation();
        $valuationDetail = $valuationModel->getVehicleDetail($valuationId);
        if (!$valuationDetail) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        $details = [];
        if ($valuationDetail->make) {
            $details[] = $valuationDetail->make;
        }
        if ($valuationDetail->family) {
            $details[] = $valuationDetail->family;
        }
        if ($valuationDetail->series) {
            $details[] = $valuationDetail->series;
        }
        if ($valuationDetail->style) {
            $details[] = $valuationDetail->style;
        }
        if ($valuationDetail->engine) {
            $details[] = $valuationDetail->engine;
        }
        if ($valuationDetail->engine_capacity) {
            $details[] = $valuationDetail->engine_capacity;
        }
        if ($valuationDetail->engine_size) {
            $details[] = $valuationDetail->engine_size;
        }
        if ($valuationDetail->variant) {
            $details[] = $valuationDetail->variant;
        }
        if ($valuationDetail->cylinder) {
            $details[] = $valuationDetail->cylinder;
        }
        if ($valuationDetail->transmission) {
            $details[] = $valuationDetail->transmission;
        }
        $valuationName = implode(' ', $details);
        $valuationDetail->valuation_name = $valuationName;

        return $this->responseData($valuationDetail);
    }

    public function updateInspectionItem(Request $request, $valuationId, $inspectionItemId)
    {
        $token = $request->header('token');

        if ($request->json()->has('note')) {
            $note = $request->json()->get('note');
        }
        $repairCost = $request->json()->get('repairCost');

        $user = NULL;
        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        if (!$this->isReadyForUpdate($valuationId)) {
            return $this->responseError(ResponseCodes::NOT_AVAILABLE_TO_EDIT);
//            [fix-me]
//            return $this->response(Resp, NULL, 'Valuation is not allow to edit');
        }
        //checking permission
        $apiPermission = array(Permissions::VALUATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'valuationId' => $valuationId,
            'inspectionItemId' => $inspectionItemId,
            'repairCost' => $repairCost,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'valuationId' => 'numeric|required|exists:mysql.valuation,id',
            'inspectionItemId' => 'numeric|required|exists:mysql.inspection_item,id',
            'repairCost' => 'numeric|required',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
            'array' => ErrorCodes::ARRAY_TYPE,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $updateData['repair_cost'] = $repairCost;
        if (isset($note)) {
            $updateData['note'] = $note;
        }
        
        $inspectionItem = ValuationInspectionItem::where('valuation_id', $valuationId)->where('inspection_item_id', $inspectionItemId)->first();
        if ($inspectionItem) {
            DB::table('valuation_inspection_item')
                ->where('valuation_id', $valuationId)
                ->where('inspection_item_id', $inspectionItemId)
                ->update($updateData);
        }

        $this->updateTotalRecondictionCost($valuationId);
        return $this->responseData(['success' => TRUE]);
    }

    public function deleteInspectionItem(Request $request, $valuationId, $inspectionItemId)
    {
        $token = $request->header('token');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        if (!$this->isReadyForUpdate($valuationId)) {
            return $this->responseError(ResponseCodes::NOT_AVAILABLE_TO_EDIT);
//            [fix-me]
//            return $this->response(Resp, NULL, 'Valuation is not allow to edit');
        }

        $apiPermission = array(Permissions::VALUATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'valuationId' => $valuationId,
            'inspectionItemId' => $inspectionItemId,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'valuationId' => 'numeric|required|exists:mysql.valuation,id',
            'inspectionItemId' => 'numeric|required|exists:mysql.inspection_item,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $inspectionItem = ValuationInspectionItem::where('valuation_id', $valuationId)->where('inspection_item_id', $inspectionItemId)->first();
        if (!$inspectionItem) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, NULL, '(valuationId,inspectionItem) do not exist');
        }

        DB::table('valuation_inspection_item')
            ->where('valuation_id', $valuationId)
            ->where('inspection_item_id', $inspectionItemId)
            ->delete();

        $this->updateTotalRecondictionCost($valuationId);

        return $this->responseData(['success' => TRUE]);
    }

    public function deleteInspectionItemGroup(Request $request, $valuationId, $sectionType)
    {
        $token = $request->header('token');
        $user = User::where('token', $token)->first();

        $apiPermission = array(Permissions::VALUATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        if (!$this->isReadyForUpdate($valuationId)) {
            return $this->responseError(ResponseCodes::NOT_AVAILABLE_TO_EDIT);
//            [fix-me]
//            return $this->response(Resp, NULL, 'Valuation is not allow to edit');
        }

        if(!$valuationResult){
            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'valuationId' => $valuationId,
            'sectionType' => $sectionType,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'valuationId' => 'numeric|required|exists:mysql.valuation,id',
            'sectionType' => 'string|required'
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
            'string' => ErrorCodes::STRING
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        ValuationInspectionItem::deleteBySection($valuationId, $sectionType);
        $this->updateTotalRecondictionCost($valuationId);

        return $this->responseData(['success' => TRUE]);
    }

    public function getMechanicalDetail(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $result = DB::select("SELECT test_driven, mechanical_additional_detail FROM `valuation` WHERE id=?", [$id]);
        if (!$result) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        return $this->responseData($result[0]);
    }

    public function updateMechanicalDetail(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        if (!$this->isReadyForUpdate($id)) {
            return $this->responseError(ResponseCodes::NOT_AVAILABLE_TO_EDIT);
//            [fix-me]
//            return $this->response(Resp, NULL, 'Valuation is not allow to edit');
        }

        $apiPermission = array(Permissions::VALUATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'test_driven' => 'required',
            'mechanical_additional_detail' => 'max:255',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $valuation = Valuation::find($id);

        if ($valuation) {
            $valuation->test_driven = $request->json()->get('test_driven');
            if ($request->json()->has('mechanical_additional_detail')) {
                $valuation->mechanical_additional_detail = $request->json()->get('mechanical_additional_detail');
            }
            $valuation->save();
            return $this->responseData($valuation);
        }
        return $this->responseError(ErrorCodes::INVALID);
    }

    /**
     * @param $valuationId: valuation id
     */
    private function isReadyForUpdate($valuationId) {
        return true;

        $isOffering         = count(DB::table('external_offer')->where('valuation_id', $valuationId)
                                    ->whereNotIn('status', [ExternalOffer::STATUS_PASSED, ExternalOffer::STATUS_PENDING])
                                    ->first()) > 0;

        $isTradingOrAuction = count(DB::table('valuation')
                                    ->join('auction_item', 'valuation.id', '=', 'auction_item.valuation_id')
                                    ->join('session_has_item', 'auction_item.id', '=', 'session_has_item.auction_item_id')
                                    ->where('valuation.id', '=', $valuationId)
                                    ->whereIn('session_has_item.status', [Auction::STATUS_CODE_PENDING, Auction::STATUS_CODE_APPROVE])
                                    ->first()) > 0;

        $isSetPriceAlready  = count(DB::table('valuation')
                                        ->where('id', '=', $valuationId)
                                        ->whereNotNull('offer_price')
                                        ->whereNotNull('max_price')
                                        ->first()) > 0;

        return !($isOffering || $isTradingOrAuction) || $isSetPriceAlready;
    }

    /**
     * @param Request $request
     * @param $id valuation Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateVehicle(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        if (!$this->isReadyForUpdate($id)) {
            return $this->responseError(ResponseCodes::NOT_AVAILABLE_TO_EDIT);
//            [fix-me]
//            return $this->response(Resp, NULL, 'Valuation is not allow to edit');
        }

        //checking permission
        $apiPermission = array(Permissions::VEHICLE_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $v = Validator::make($request->all(), [
            'make' => '',
            'model_code' => '',
            'registration_number' => '',
            'variant' => '',
            'colour' => '',
            'manufacturer_color' => '',
            'body_type' => '',
            'first_reg_date' => '',
            'registration_expiry_date' => '',
            'registered_state' => '',
            'build_date' => '',
            'compliance_date' => '',
            'engine_type' => '',
            'engine_size' => '',
            'engine_capacity' => '',
            'cylinder' => '',
            'fuel_type' => '',
            'transmission' => '',
            'drive_train' => '',
            'body_no' => '',
            'vin' => '',
            'chassis_number' => '',
            'trim' => '',
            'trim_color' => '',
            'glass_guide_code' => ''
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $valuation = Valuation::find($id);

        $vehicle = null;

        if (!$valuation) {
            return $this->responseError(ErrorCodes::NOT_FOUND, "Valuation not found");
        }

        $input = $request->toArray();
        $input = array_filter($input, 'strlen'); // remove null values

        if (array_key_exists('vin', $input)) {
            $vehicle = Vehicle::where('vin', $input['vin'])->first();
        }

        if (!$vehicle && $valuation->vehicle_id) {
            $vehicle = Vehicle::find($valuation->vehicle_id);
        }
        if (!$vehicle) {
            $vehicle = new Vehicle();
        }

        $vehicle->fill($input);
        $vehicle->save();

        if ($valuation->vehicle_id != $vehicle->id) {
            $valuation->vehicle_id = $vehicle->id;
            $valuation->save();
        }

        return $this->responseData($vehicle);
    }

    public function getWholesaleValues(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::INVALID);
        }

        if ($valuation->status == Valuation::STATUS_WAITING) {
            return $this->responseError(ErrorCodes::INVALID, ['valuation_id' => 'Waiting valuation']);
        }

        $vehicle = NULL;
        if ($valuation->vehicle_id) {
            $vehicle = Vehicle::find($valuation->vehicle_id);
        }

        if (!$vehicle) {
            return $this->responseError(ErrorCodes::INVALID, ['id' => 'Valuation has not vehicle_id']);
        }

        $glass = [
            'retail' => 0,
            'below' => 0,
            'above' => 0
        ];

        if ($vehicle->model_code) {
            $result = DB::select('SELECT recommended_retail_price retail, 
                                below_recommended_retail_price below, 
                                above_recommended_retail_price above
                    FROM `glass_model` WHERE glass_code=?', [$vehicle->model_code]);
            if ($result) {
                $glass = $result[0];
            }
        }

        $km_adjustment = [
            'retail' => 0,
            'below' => 0,
            'above' => 0
        ];

        $result = DB::select('SELECT * FROM `glass_km_adjustment` WHERE `up_to_kms` >= :km ORDER BY `adjust_amount` LIMIT 1', ['km' => $vehicle->odometer]);
        if ($result) {
            $value = intval($result[0]->adjust_amount);
            $km_adjustment = [
                'retail' => $value,
                'below' => $value,
                'above' => $value
            ];
        }

        $value_of_options = [
            'retail' => $valuation->value_of_options,
            'below' => $valuation->value_of_options,
            'above' => $valuation->value_of_options
        ];

        return $this->responseData([
            'glass' => $glass,
            'km_adjustment' => $km_adjustment,
            'value_of_options' => $value_of_options
        ]);
    }

    public function updateWholesaleValues(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        if (!$this->isReadyForUpdate($id)) {
            return $this->responseError(ResponseCodes::NOT_AVAILABLE_TO_EDIT);
//            [fix-me]
//            return $this->response(Resp, NULL, 'Valuation is not allow to edit');
        }

        $apiPermission = array(Permissions::VALUATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::INVALID);
        }

        $v = Validator::make($request->all(), [
            'retail' => ''
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $json = $request->json();

        $value_of_options = $json->get('value_of_options');
        if ($value_of_options && $value_of_options["retail"]) {
            $valuation->value_of_options = $value_of_options["retail"];
            $valuation->save();
        }

        return $this->getWholesaleValues($request, $id);
    }

    public function getReconditioningCost(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::INVALID);
        }

        $valuationIIModel = new ValuationInspectionItem();

        $items = $valuationIIModel->getItems($id, NULL);

        $exterior = 0;
        $interior = 0;
        $mechanical = 0;

        foreach ($items as $i) {
            if ($i->section_type == 100) {
                $interior += $i->repair_cost;
            } elseif ($i->section_type == 200) {
                $mechanical += $i->repair_cost;
            } else {
                $exterior += $i->repair_cost;
            }
        }

        $estimated_recon_cost = $valuation->estimated_recon_cost;
//        if (!$estimated_recon_cost) {
//            $estimated_recon_cost = $exterior + $interior + $mechanical;
//        }

        return $this->responseData([
            'exterior' => $exterior,
            'interior' => $interior,
            'mechanical' => $mechanical,
            'estimated_recon_cost' => $estimated_recon_cost
        ]);
    }

    private function updateTotalRecondictionCost($valudationId)
    {
        $valuation = Valuation::find($valudationId);

        if (!$valuation) {
            return -1;
        }

        $valuation->total_recon_cost = 0;

        $valuationIIModel = new ValuationInspectionItem();

        $items = $valuationIIModel->getItems($valudationId, NULL);

        foreach ($items as $i) {
            $valuation->total_recon_cost += $i->repair_cost;
        }
        $valuation->total_recon_cost += $valuation->general_recondition_cost;
        $valuation->save();

        return $valuation->total_recon_cost;
    }

    public function updateReconditioningCost(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        if (!$this->isReadyForUpdate($id)) {
            return $this->responseError(ResponseCodes::NOT_AVAILABLE_TO_EDIT);
//            [fix-me]
//            return $this->response(Resp, NULL, 'Valuation is not allow to edit');
        }

        $apiPermission = array(Permissions::VALUATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::INVALID);
        }

        $v = Validator::make($request->all(), [
            'estimated_recon_cost' => 'required:numeric'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $valuation->estimated_recon_cost = $request->json()->get('estimated_recon_cost');
        $valuation->save();
        return $this->responseData($request->json());
    }

    public function complete(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_PERFORM);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $valuationModel = new Valuation();
        $valuation = $valuationModel->getValuationById($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        if ($valuation->valuer_id != $user->id) {
            return $this->responseError(ErrorCodes::VALUATION_HAS_BEEN_TAKEN_BY_OTHER);
        }

        if ($valuation->status != Valuation::STATUS_IN_PROCESS) {
            return $this->responseError(ErrorCodes::INVALID, ['status' => "Not in progress"]);
        }

        $status = User::find($valuation->user_id)->headValuerApproval() ? Valuation::STATUS_PENDING : Valuation::STATUS_COMPLETE;

        DB::table('valuation')->where('id', $id)
            ->update(['status' => $status, 'version_creator' => $user->id]);

        $log = Valuation::find($id)->status;
        Log::debug("Valuation::find(id)->status:[$log], id:[$id]");
        $title = 'Valuation Is Completed';

        $message = '';
        $webMessage = '';
        $msgParts = [];
        $webMsgParts = [];

        if ($valuation->year) {
            $msgParts[] = $valuation->year;
            $webMsgParts[] = $valuation->year;
        }

        $msgParts[] = trim($valuation->make);
        $webMsgParts[] = trim($valuation->make);

        if ($valuation->model_family) {
            $msgParts[] = $valuation->model_family;
            $webMsgParts[] = $valuation->model_family;
        }

        if ($valuation->colour) {
            $msgParts[] = $valuation->colour;
            $webMsgParts[] = $valuation->colour;
        }

        if ($valuation->registration_number) {
            $msgParts[] = $valuation->registration_number;
        }

        if ($msgParts) {
            $message = implode(' ', $msgParts);
        }
        if ($webMsgParts) {
            $webMessage = implode(' ', $webMsgParts);
        }

        $customData = [
            'valuationId' => $valuation->id,
            'time' => date('Y-m-d H:i:s'),
            'web_message' => $webMessage,
            'registration_number' => $valuation->registration_number,
        ];

        $locationId = User::find($valuation->user_id)->location_id;
        $permissions = [
            Permissions::NOTIFICATION_SALE_MANAGER, 
            Permissions::NOTIFICATION_HEAD_VALUER
        ];
        User::noticePermission($locationId, Event::VALUER_COMPLETE_VALUATION, $valuation->id, $permissions, $customData);
        User::notice($valuation->user_id, Event::VALUER_COMPLETE_VALUATION, $valuation->id, $customData);

        $valuation = $valuationModel->getValuationById($id);
        return $this->responseData($valuation);
    }

    public function createNewVersion(Request $request, $id)
    {

        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        //checking permission
        $apiPermission = array(Permissions::VALUATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'fromVersion' => ''
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        
        
        \LOG::info("User[$user->id] createValuation[$id] version");

        $v = Valuation::find($id);

        if (!$v) {
            return $this->responseError(ErrorCodes::NOT_FOUND, 'no such valuation!');
        }
       // print_r($v);
       // dd($v->version + 1 );
        $json = $request->json();

        $new_version = $v->version + 1;
        echo "new version : ". $new_version."<br>";



        if ($json->has("fromVersion")) {
            echo $user->id.",".$id.",".$new_version.",".$json->get('fromVersion');

            $r = Valuation::createVersion($user->id, $id, $new_version, $json->get('fromVersion'));
        } else {
            echo "in else";
            $r = Valuation::createVersion($user->id, $id, $new_version, $json->get('version'));
        }

        if ($r == -1) {
        	return $this->responseError(ErrorCodes::NOT_FOUND, ['code'=>$r]);
        } else if ($r == -2) {
        	return $this->responseError(ErrorCodes::EXISTED, ['code'=>$r]);
        }
        else if ($r == -3) {
        	return $this->responseError(ErrorCodes::INVALID_INPUT, ['code'=>$r], "Valuation status[$v->status] is not really for creating new version");
        }
        else if ($r == -4) {
        	$v = Valuation::find($id);
        	return $this->responseError(ErrorCodes::INVALID_INPUT, ['user_id' => $v->version_locker_user_id, 'code'=>$r], "Valuation is modifying by someone");
        }  else if ($r == -5) {
        	return $this->responseError(ErrorCodes::INVALID_INPUT, ['code'=>$r], "Valuation is not the lastest version");
        } else if ($r < 0) {
        	return $this->responseError(ErrorCodes::SYS_ERR, ['code'=>$r]);
        }
        
        \LOG::info("User[$user->id] createValuation[$id] => version[$new_version]]");

        return $this->responseData(['msg' => 'success', 'old_valuation_id' => $r, 'newVersionId' => $new_version]);
    }
    
    public function releaseVersion(Request $request, $id)
    {
    	$token = $request->header("token");
    	$user = NULL;
//    	$company_id = NULL;
    	
    	if ($token) {
    		$user = User::where('token', $token)->first();
    	}
    	
    	if (!$user) {
    		return $this->responseError(ErrorCodes::INVALID_TOKEN);
    	}

        $apiPermission = array(Permissions::VALUATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
    	
    	\LOG::info("User[$user->id] releaseVersion Valuation[$id] version");
    	
    	$v = Valuation::find($id);
    	if (!$v) {
    		return $this->responseError(ErrorCodes::NOT_FOUND, 'No such valuation!');
    	}
    	
    	if ($v->origin_id != null && $v->origin_id != $v->id) {
    		return $this->responseError(ErrorCodes::INVALID, 'This is not a lasted version');
    	}
    	
    	if ($v->version_locker_user_id == 0) {
    		return $this->responseError(ErrorCodes::INVALID, 'version has been released');
    	}
    	
    	if ($v->version_locker_user_id != $user->id) {
    		return $this->responseError(ErrorCodes::INVALID, 'You are not editing this one.');
    	}
    	
    	$pv = $v->getParentVersion();
    	if (!$pv) {
    		return $this->responseError(ErrorCodes::INVALID, 'No any previous version');
    	}
    	
    	//if ($v->hasSameValues($pv) && $v->hasSamePictures($pv)) { // temperory commented by SS - E0033
        if ($v->hasSameValues($pv)){
    		Valuation::removeLastVersion($id);
            \LOG::debug("Nothing changed in $id => will now roll back");
    		return $this->responseData(['code'=> 1, 'msg'=>'Nothing changes! will now rollback.']);
    	}
    	
    	Valuation::unsetLocker($id);
        \LOG::debug("Valuation[$id] has been changed => release new version");
    	
    	return $this->responseData(['code'=> 0, 'msg'=>'new version will be kept']);
    }
    
    public function cancelModifyingVersion(Request $request, $id)
    {
    	$token = $request->header("token");
    	$user = NULL;
//    	$company_id = NULL;
    	
    	if ($token) {
    		$user = User::where('token', $token)->first();
    	}
    	
    	if (!$user) {
    		return $this->responseError(ErrorCodes::INVALID_TOKEN);
    	}
        $apiPermission = array(Permissions::VALUATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
    	\LOG::info("User[$user->id] cancelModifyingVersion Valuation[$id] version");
    	
    	$v = Valuation::find($id);
    	if (!$v) {
    		return $this->responseError(ErrorCodes::NOT_FOUND, 'No such valuation!');
    	}
    	
    	if ($v->origin_id != null && $v->origin_id != $v->id) {
    		return $this->responseError(ErrorCodes::INVALID, 'This is not a lasted version');
    	}
    	
    	if ($v->version_locker_user_id == 0) {
    		return $this->responseError(ErrorCodes::INVALID, 'version has been released');
    	}
    	
    	if ($v->version_locker_user_id != $user->id) {
    		return $this->responseError(ErrorCodes::INVALID, 'You are not editing this one.');
    	}
    	
    	$pv = $v->getPreviousVersion();
    	if (!$pv) {
    		return $this->responseError(ErrorCodes::INVALID, 'No any previous version');
    	}
    	
    	Valuation::removeLastVersion($id);
    	
    	return $this->responseData(['code'=> 0]);
    }

    public function getAllVersion(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){
            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $result = DB::select('SELECT v.id valuation_id, version, v.version_creator, first_name, last_name, v.create_date, v.version_locker_user_id FROM `valuation` v LEFT JOIN `user` u on v.version_creator=u.id LEFT JOIN `contact` c on u.contact_id = c.id WHERE v.id=? OR v.origin_id=? ORDER BY v.version DESC', [$id, $id]);

        return $this->responseData($result);
    }

    public function approve(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_APPROVE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $valuationModel = new Valuation();
        $valuation = $valuationModel->getValuationById($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::INVALID);
        }

        if ($valuation->status != Valuation::STATUS_PENDING) {
            return $this->responseError(ErrorCodes::INVALID, ['status' => "Not in pending"]);
        }

        DB::table('valuation')->where('id', $id)->update(['status' => Valuation::STATUS_COMPLETE]);

        // The sales manager will be provided the valuation price and maximum valuation price.
        $title = 'Approve valuation';

        $message = '';
        $webMessage = '';
        $msgParts = [];
        $webMsgParts = [];

        if ($valuation->year) {
            $msgParts[] = $valuation->year;
            $webMsgParts[] = $valuation->year;
        }

        if ($valuation->make) {
            $msgParts[] = $valuation->make;
            $webMsgParts[] = $valuation->make;
        }

        if ($valuation->model_family) {
            $msgParts[] = $valuation->model_family;
            $webMsgParts[] = $valuation->model_family;
        }

        if ($valuation->colour) {
            $msgParts[] = $valuation->colour;
            $webMsgParts[] = $valuation->colour;
        }

        if ($valuation->registration_number) {
            $msgParts[] = $valuation->registration_number;
        }

        if ($msgParts) {
            $message = implode(' ', $msgParts);
            $message .= ". Offer value: " . $valuation->offer_value;
            $message .= ". Valuation value: " . $valuation->valuation_value;
        }

        if ($webMsgParts) {
            $webMessage = implode(' ', $webMsgParts);
        }

        $customData = [
            'valuationId' => $valuation->id,
            'time' => date('Y-m-d H:i:s'),
            'web_message' => $webMessage,
            'offer_value' => $valuation->offer_value,
            'valuation_value' => $valuation->valuation_value,
        ];
        
        $permissions = [Permissions::NOTIFICATION_SALE_MANAGER];
        User::noticePermission($user->location_id, Event::APPROVE_PRICE_MAX, $valuation->id, $permissions, $customData);

        // The sales personnel will be provided the valuation price only.

        if ($msgParts) {
            $message = implode(' ', $msgParts);
            $message .= ". Offer Value: " . $valuation->offer_value;
        }

        $customData = [
            'valuationId' => $valuation->id,
            'time' => date('Y-m-d H:i:s'),
            'web_message' => $webMessage,
            'offer_value' => $valuation->offer_value,
        ];

        User::notice($valuation->user_id, Event::APPROVE_PRICE_ONLY, $valuation->id, $customData);   

        return $this->responseData($valuation);
    }

    public function reprice(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_REPRICE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $valuationModel = new Valuation();
        $valuation = $valuationModel->getValuationById($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::INVALID);
        }

        if ($valuation->status != Valuation::STATUS_COMPLETE) {
            return $this->responseError(ErrorCodes::INVALID, ['status' => "Not in completed list"]);
        }

        $v = Validator::make($request->all(), [
            'requested_price' => 'required|numeric'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $requestedPrice = $request->json()->get('requested_price');

        $status = $user->headValuerApproval() ? Valuation::STATUS_PENDING_REEVALUATE : Valuation::STATUS_COMPLETE;

        DB::table('valuation')->where('id', $id)
            ->update([
                'status' => $status,
                'requested_price' => $requestedPrice
            ]);

        $valuation->requested_price = $requestedPrice;
        // The sales manager will be provided the valuation price and maximum valuation price.
        $title = 'Approve re-price';

        $message = '';
        $webMessage = '';
        $msgParts = [];
        $webMsgParts = [];

        if ($valuation->year) {
            $msgParts[] = $valuation->year;
            $webMsgParts[] = $valuation->year;
        }

        if ($valuation->make) {
            $msgParts[] = $valuation->make;
            $webMsgParts[] = $valuation->make;
        }

        if ($valuation->model_family) {
            $msgParts[] = $valuation->model_family;
            $webMsgParts[] = $valuation->model_family;
        }

        if ($valuation->colour) {
            $msgParts[] = $valuation->colour;
            $webMsgParts[] = $valuation->colour;
        }

        if ($valuation->registration_number) {
            $msgParts[] = $valuation->registration_number;
        }

        if ($msgParts) {
            $message = implode(' ', $msgParts);
            $message .= ". Requested Price: " . $valuation->requested_price;
            $message .= ". Offer Value: " . $valuation->offer_value;
            $message .= ". Valuation Value: " . $valuation->valuation_value;
        }

        if ($webMsgParts) {
            $webMessage = implode(' ', $webMsgParts);
        }

        $customData = [
            'valuationId' => $valuation->id,
            'requested_price' => $valuation->requested_price,
            'valuation_value' => $valuation->valuation_value,
            'web_message' => $webMessage,
            'time' => date('Y-m-d H:i:s'),
            'registration_number' => $valuation->registration_number,
            'offer_value' => $valuation->offer_value,

        ];

        $permissions = [Permissions::NOTIFICATION_SALE_MANAGER];
        User::noticePermission($user->location_id, Event::ADJUST_PRICE_MAX, $valuation->id, $permissions, $customData);

        // The sales personnel will be provided the valuation price only.

        if ($msgParts) {
            $message = implode(' ', $msgParts);
            $message .= ". Requested Price: " . $valuation->requested_price;
            $message .= ". Offer Value: " . $valuation->offer_value;
        }

        $customData = [
            'valuationId' => $valuation->id,
            'requested_price' => $valuation->requested_price,
            'web_message' => $webMessage,
            'time' => date('Y-m-d H:i:s'),
            'registration_number' => $valuation->registration_number,
            'offer_value' => $valuation->offer_value,
        ];
        User::notice($valuation->user_id, Event::ADJUST_PRICE_ONLY, $valuation->id, $customData);

        return $this->responseData($valuation);
    }

    public function approveReprice(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $apiPermission = array(Permissions::VALUATION_APPROVE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::INVALID);
        }

        if ($valuation->status != Valuation::STATUS_PENDING_REEVALUATE) {
            return $this->responseError(ErrorCodes::INVALID, ['status' => "Not in pending list"]);
        }

        if (!$valuation->requested_price) {
            return $this->responseError(ErrorCodes::INVALID, ['requested_price' => "is empty"]);
        }


        Valuation::createVersion($user->id, $valuation->id, $valuation->version + 1);

        $valuation = Valuation::find($id);

        $valuation->status = Valuation::STATUS_COMPLETE;
        if ($valuation->requested_price < $valuation->valuation_value) {
            $valuation->offer_value = $valuation->requested_price;
        } else {
            $valuation->valuation_value = $valuation->requested_price;
        }
        $valuation->requested_price = 0;
        $valuation->save();

        return $this->responseData($valuation);
    }

    public function rejectReprice(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_APPROVE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::INVALID);
        }

        if ($valuation->status != Valuation::STATUS_PENDING_REEVALUATE) {
            return $this->responseError(ErrorCodes::INVALID, ['status' => "Not in pending list"]);
        }

        if (!$valuation->requested_price) {
            return $this->responseError(ErrorCodes::INVALID, ['requested_price' => "is empty"]);
        }

        $valuation->status = Valuation::STATUS_COMPLETE;
        $valuation->requested_price = 0;
        $valuation->save();

        return $this->responseData($valuation);
    }

    public function trade(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VEHICLE_TRADE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::INVALID);
        }

        if ($valuation->status != Valuation::STATUS_COMPLETE) {
            return $this->responseError(ErrorCodes::INVALID, ['status' => "Not in completed list"]);
        }

        $v = Validator::make($request->all(), [
            'note' => 'max:256',
            'trade_price' => 'numeric',
            'proposed_arrival_date' => 'date'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $json = $request->json();

        $valuation->note = $json->get('note');
        $valuation->trade_price = $json->get('trade_price');
        $valuation->proposed_arrival_date = $json->get('proposed_arrival_date');
        $valuation->status = Valuation::STATUS_COMPLETE_TRADED;
        $valuation->save();

        //send mail - payment receipt
        $bestOffer = OfferBid::bestBid($valuation->id);
        if ($bestOffer) {
            $event      = Event::VEHICLE_TRADED;
            $buyerData  = [];
    
            User::notice($bestOffer->buyer_id, $event, $bestOffer->external_offer_id, $buyerData);
        }

        return $this->responseData($valuation);
    }

    public function tradein(Request $request, $id)
    {
        \LOG::info((string)$request);
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VEHICLE_TRADE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::INVALID);
        }

        if ($valuation->status != Valuation::STATUS_COMPLETE_TRADED) {
            return $this->responseError(ErrorCodes::INVALID, ['status' => "Not in pending list"]);
        }

        $v = Validator::make($request->all(), [
            'note' => 'max:256',
//            'trade_price' => 'double',
            'stock_number' => ''
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $json = $request->json();
        $valuation->status = Valuation::STATUS_COMPLETE_TRADED_IN;
        $valuation->note = $json->get('note');
        $valuation->proposed_arrival_date = date('Y-m-d H:i:s');
//        if ($json->has('trade_price')) {
//            $valuation->trade_price = $json->get('trade_price');
//        }
        if ($json->has('stock_number')) {
            $valuation->stock_number = $json->get('stock_number');
        }
        $locationId = 0;
        if ($json->has('location_id')) {
            $locationId = $json->get('location_id');
        }
        if($locationId == 0){
            $locationId = $user->location_id;
        }

        //start transaction
        $valuation->save();
        
        $vehicle = $valuation->getVehicle();
        $vehicle->location_id = $user->location_id;
        $vehicle->save();

        $v = Vehicle::find($valuation->vehicle_id);
        $v->trade_in_location = $locationId;
        $v->trade_in = date('Y-m-d H:i:s');
        $v->save();

        //todo: feature/865 + feature/36: missing send notification to company channel
        // $userWishlist = $valuation->usersHaveMatchWishlist();
        // foreach ($userWishlist as $user) {
        //     Log::debug("sending mail to: $user->email\n");
            // MailServiceController::sendWishlistAppear($user, '');
        // }

        //begin insert to transaction table
        //get external id, find by valuation id and status
        $externalStatus = [
            ExternalOffer::STATUS_ACCEPTED,
            ExternalOffer::STATUS_LOCATION_SET,
            ExternalOffer::STATUS_VEHICLE_READY_TO_SHIP,
            ExternalOffer::STATUS_TRANSPORT_TRIGGERED,
            ExternalOffer::STATUS_SHIPPED,
            ExternalOffer::STATUS_DELIVERED,
        ];
        $statusStr = implode(',', $externalStatus);
        $externalOffer = DB::select("Select * from external_offer where valuation_id = $id AND status IN ($statusStr) ORDER BY id DESC
        LIMIT 0,1");

        if($externalOffer && $externalOffer[0]->receive_location != null){
            //transaction will be create when: external offer has accepted buyer, seller set vehicle for valuation,buyer set location to receive vehicle
            $externalOfferId = $externalOffer[0]->id;
            $offerBid = DB::select("Select * from offer_bid where external_offer_id = $externalOfferId LIMIT 0,1");

            $buyerId = OfferBid::where('external_offer_id', $externalOffer[0]->id)->first()->user_id;
            $buyer = User::find($buyerId);

            $buyer_location = $externalOffer[0]->receive_location;
            $sellerId = $externalOffer[0]->creator_id;
            $transaction = $this->createTransactionRecord($valuation, $offerBid[0], $externalOfferId, $buyer_location,$locationId,$sellerId);

            $updateExternalOffer = ExternalOffer::find($externalOffer[0]->id);
            $updateExternalOffer->status = ExternalOffer::STATUS_VEHICLE_READY_TO_SHIP;
            $updateExternalOffer->save();

            $valuation->status = Valuation::STATUS_SOLD_OUT;
            $valuation->save();

            $sellerData = [];
            $buyerData  = [];
            $event = Event::TRANSACTION_NEW;
            \LOG::debug("start generate document for event: [$event]");
            $buyerData['attach'] = Document::generatorPDFAndUploadAzure($transaction->seller_id, $event, $transaction->id, 'creator');

            User::notice($transaction->seller_id, $event, $transaction->id, $sellerData);
            User::notice($transaction->buyer_id, $event, $transaction->id, $buyerData);
        }
        //end insert to transaction

        return $this->responseData($valuation);
    }

    public function getSalesInfo(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;
//        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::INVALID);
        }
        $vehicle = NULL;
        if ($valuation->vehicle_id) {
            $vehicle = Vehicle::find($valuation->vehicle_id);
        }

        $sale_status = 0;
        $valuation_price = 0;
        $max_valuation_price = 0;

        if ($vehicle) {
            $sale_status = $vehicle->sale_status;
        }

        if ($valuation->offer_value) {
            $valuation_price = $valuation->offer_value;
        }

        if ($valuation->valuation_value) {
            $max_valuation_price = $valuation->valuation_value;
        }

        return $this->responseData(['sale_status' => $sale_status,
            'valuation_price' => $valuation_price,
            'max_valuation_price' => $max_valuation_price]);
    }

    public function updateSalesInfo(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        if (!$this->isReadyForUpdate($id)) {
            return $this->responseError(ResponseCodes::NOT_AVAILABLE_TO_EDIT);
//            [fix-me]
//            return $this->response(Resp, NULL, 'Valuation is not allow to edit');
        }

        $apiPermission = array(Permissions::VALUATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::INVALID);
        }
        $vehicle = NULL;
        if ($valuation->vehicle_id) {
            $vehicle = Vehicle::find($valuation->vehicle_id);
        }

        $v = Validator::make($request->all(), [
            'sale_status' => 'integer|min:0|max:100',
            'valuation_price' => 'numeric',
            'max_valuation_price' => 'numeric'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $json = $request->json();

        if ($json->has('sale_status')) {
            $vehicle->sale_status = $json->get("sale_status");
            $vehicle->save();
        }

        if ($vehicle) {
            $changed = false;

            if ($json->has('valuation_price')) {
                $valuation->offer_value = $json->get('valuation_price');
                $changed = true;
            }

            if ($json->has('max_valuation_price')) {
                $valuation->valuation_value = $json->get('max_valuation_price');
                $changed = true;
            }

            if ($changed) {
                $valuation->save();
            }
        }

        return $this->getSalesInfo($request, $id);
    }

    public function updateOverallRating(Request $request, $id)
    {

        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        if (!$this->isReadyForUpdate($id)) {
            return $this->responseError(ResponseCodes::NOT_AVAILABLE_TO_EDIT);
//            [fix-me]
//            return $this->response(Resp, NULL, 'Valuation is not allow to edit');
        }

        $apiPermission = array(Permissions::VALUATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::INVALID);
        }

        $v = Validator::make($request->all(), [
            'overall_rating' => 'integer|min:0|max:100'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        if($request->json()->has('overall_rating')){
            $valuation->overall_rating = $request->json()->get('overall_rating');
        }
        if ($request->json()->has('offer_price')){
            $valuation->offer_price = $request->json()->get('offer_price');
        }
        $valuation->save();
        $valuation->ownership_type = json_decode($valuation->ownership_type, true);
        return $this->responseData($valuation);
    }



    public function getValuationVersion(Request $request, $id, $version)
    {

        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $valuation = Valuation::orWhere(function ($query) use (&$id, &$version) {
            $query->where('origin_id', $id)
                ->where('version', $version);
        })->orWhere(function ($query) use (&$id, &$version)  {
            $query->where('id', $id)
                ->where('version', $version);
        })->first();

        if (!$valuation) {
            return $this->responseError(ErrorCodes::NOT_EXISTS);
        }

        return $this->responseData($valuation);
    }

    public function getInspectionItemOfOldVersion(Request $request, $id, $version)
    {

        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $valuation = Valuation::orWhere(function ($query) use (&$id, &$version) {
            $query->where('origin_id', $id)
                ->where('version', $version);
        })->orWhere(function ($query) use (&$id, &$version)  {
            $query->where('id', $id)
                ->where('version', $version);
        })->first();

        if (!$valuation) {
            return $this->responseError(ErrorCodes::NOT_EXISTS);
        }

        $valuationIItemModel = new ValuationInspectionItem();
        $sectionGroups = [
            ValuationInspectionItem::EXTERIOR_SECTION,
            ValuationInspectionItem::INTERIOR_SECTION,
            ValuationInspectionItem::MECHANICAL_SECTION
        ];
        $sectionGroupObj = new \stdClass();

        foreach ($sectionGroups as $sectionGroup) {
            $data = [];
            $property = strtolower($sectionGroup);
            $valuationSections = $valuationIItemModel->getSection($valuation->id, $sectionGroup);
            foreach ($valuationSections as $section) {
                $section->items = $valuationIItemModel->getItems($valuation->id, $section->section_type);
                $data[] = $section;
            }
            $sectionGroupObj->$property = $data;
        }
        return $this->responseData($sectionGroupObj);
    }

    public function getValuation(Request $request, $id)
    {

        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }


        $valuation = Valuation::find($id);

        if (!$valuation) {
            return $this->responseError(ErrorCodes::INVALID);
        }
        $valuationCreator = User::find($valuation->user_id);
        if(!$valuationCreator){
            return $this->responseError(ErrorCodes::INVALID);
        }

        //check user can access valuation
        $userLocationAccess = $user->getAccessableLocationIds();
        if(!in_array($valuationCreator->location_id, $userLocationAccess) && !$user->isMo()){
            \Log::info("User cannot access this valuation because data access not allow");
            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $valuation->ownership_type = json_decode($valuation->ownership_type, true);
        return $this->responseData($valuation);
    }

    public function updateGeneralReconditionCost(Request $request, $valuationId)
    {
        $token = $request->header('token');
        $generalCost = $request->json()->get('generalCost');

        $user = NULL;
        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        if (!$this->isReadyForUpdate($valuationId)) {
            return $this->responseError(ResponseCodes::NOT_AVAILABLE_TO_EDIT);
//            [fix-me]
//            return $this->response(Resp, NULL, 'Valuation is not allow to edit');
        }

        $apiPermission = array(Permissions::VALUATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $validationData = [
            'token' => $token,
            'valuationId' => $valuationId,
            'generalCost' => $generalCost,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'valuationId' => 'numeric|required|exists:mysql.valuation,id',
            'generalCost' => 'numeric|required',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        DB::table('valuation')
            ->where('id', $valuationId)
            ->update([
                'general_recondition_cost' => $generalCost,
            ]);


        $this->updateTotalRecondictionCost($valuationId);
        return $this->responseData(['success' => TRUE]);
    }

    public function getGeneralReconditionCost(Request $request, $valuationId)
    {
        $token = $request->header('token');

        $user = NULL;
        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $validationData = [
            'token' => $token,
            'valuationId' => $valuationId,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'valuationId' => 'numeric|required|exists:mysql.valuation,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $valuation = Valuation::find($valuationId);
        $generalReconditionCost = $valuation->general_recondition_cost;
        return $this->responseData(['generalReconditionCost' => $generalReconditionCost]);
    }

    public function findByCustomerId(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'offset' => 'required|integer|min:0',
            'limit' => 'required|integer',
            'customer_id' => 'required|integer'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $json = $request->json();

        $valuations = DB::select('SELECT v.*, h.make, h.model_family ,h.registration_number FROM valuation v LEFT JOIN vehicle h ON v.vehicle_id=h.id WHERE customer_id=:customer_id ORDER BY update_date DESC LIMIT :offset, :limit', [
            'customer_id' => $json->get("customer_id"),
            'limit' => $json->get("limit"),
            'offset' => $json->get("offset")
        ]);
        return $this->responseData($valuations);
    }

    public function countByCustomerId(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'customer_id' => 'required|integer'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $json = $request->json();

        $valuations = DB::select('SELECT COUNT(*) AS count FROM valuation WHERE customer_id=?', [$json->get("customer_id")]);
        return $this->responseData($valuations[0]);
    }


    public function sync(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'status' => 'required|integer',
            'customer_id' => 'required|integer',
            'vin' => 'max:100',
            'build_date' => 'string',
            'variant' => '',
            'registration_number' => 'max:100',
            'make' => '',
            'model_family' => 'max:45',
            'colour' => 'integer',
            'registration_expiry_date' => 'date',
            'has_one_owner' => 'boolean',
            'has_service_book' => 'boolean',
            'has_service_history' => 'boolean',
            'trade_in_location' => 'max:100',
            'keys_location' => 'max:100',
            'compliance_date' => 'date',
            'odometer' => 'integer',
            'engine_number' => 'max:100',
            'i_vehicle' => 'integer',
            'i_make' => '',
            'i_model' => '',
            'i_variant' => '',
            'i_institution' => '',
            'i_payout' => '',
            'i_monthly_payment' => '',
            'i_finance_owe' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $input = $request->json();
        $vehicle = null;

        if ($input->has('vin')) {
            $vehicle = Vehicle::where('vin', $input->get('vin'))->first();
            if (!$vehicle) {
                $vehicle = new Vehicle();
            }
        }

        $vehicle->company_group_id = array_key_exists('company_group_id', $user) ? $user->company_group_id : null;
        $vehicle->fillFromJson($input, ['company_group_id', 'rego', 'vinIndex', 'colour', 'registration_number', 'engine_number', 'engine_type_id', 'engine_size',
            'cylinder', 'fuel_type', 'doors_count', 'key_number', 'radio_code', 'boot_key', 'ignition_key', 'wheel_size', 'odometer', 'odometer_date',
            'registration_expiry_date', 'registered_state', 'build_date', 'compliance_date', 'first_reg_date', 'delivery_date', 'delivery_odo',
            'last_service_date', 'last_service_odo', 'last_service_dealer', 'status_code', 'warranty_type', 'fleet_company_id', 'fleet_no', 'ssm_service_model_id',
            'customer_note', 'followup_note', 'notes', 'glass_guide_code', 'glass_guide_nvic', 'transmission', 'drive_train', 'chassis_number',
            'trim', 'vehicle_model_code', 'has_one_owner', 'has_service_book', 'has_service_history', 'condition', 'trade_in_location', 'keys_location',
            'model_family', 'make_id', 'model_code', 'model_description', 'family', 'variant', 'series', 'style', 'manuf_id',
            'driver_id', 'owner_id', 'body_type_id', 'manufacturer_color', 'body_no', 'trim_color', 'creator_id', 'create_date', 'updater_id', 'update_date',
            'sale_status', 'make', 'body_type', 'engine_type', 'engine_capacity']);
        $vehicle->save();

        $valuation = new Valuation();
        $valuation->fillFromJson($input, ['valuer_id', 'status', 'customer_id', 'user_id', 'vehicle_id', 'create_date', 'update_date', 'trade_in_location',
            'keys_location', 'company_id', 'estimated_recon_cost', 'requested_approve_price', 'negotiating_price', 'max_price', 'has_one_owner',
            'has_service_history', 'has_service_book', 'notify_sent_counter', 'test_driven', 'mechanical_additional_detail', 'offer_value', 'valuation_value', 'requested_price',
            'trade_price', 'proposed_arrival_date', 'note', 'value_of_options', 'overall_rating', 'general_recondition_cost', 'total_recon_cost', 'offer_price',]);

        $valuation->valuer_id = $user->id;
        $valuation->user_id = $user->id;
        $valuation->location_id = $user->location_id;
        $valuation->company_group_id;

        $valuation->save();
        return $this->responseData([
            'valuation_id' => $valuation->id,
            'vehicle_id' => $vehicle->id
        ]);
    }

    /**
     * @param $bestOffer
     * @param $externalOfferId
     * @param $valuation
     * @param $buyer_location
     * @param $seller_location
     * @param $sellerId
     */
    private function createTransactionRecord($valuation, $bestOffer, $externalOfferId, $buyer_location, $seller_location,$sellerId)
    {
        //get valuation

        $transaction = new Transaction();
        $transaction->valuation_id = $valuation->id;
        $transaction->buyer_id = $bestOffer->user_id;
        $transaction->seller_id = $sellerId;

        $transaction->status = Transaction::STATUS_WAITING_PAYMENT;
        $transaction->vehicle_price = $bestOffer->bid_price;
        $transaction->valuation_version = $valuation->version;
        $transaction->source = Transaction::SOURCE_EXTERNAL_OFFER;
        $transaction->source_id = $externalOfferId;
        $transaction->buyer_location = $buyer_location;
        $transaction->seller_location = $seller_location;
        //create rid
        $transaction->save();
        $transaction->rid = IdTransformer::toClientTransactionId($transaction->id);
        Transaction::setMotoFee($transaction);
        $transaction->save();

        //update rating
        $locationId = Valuation::where('valuation.id', $transaction->valuation_id)
            ->join('vehicle', 'vehicle.id', '=', 'valuation.vehicle_id')
            ->first()
            ->location_id;
        Location::updateRating($locationId);

        return $transaction;
    }
}