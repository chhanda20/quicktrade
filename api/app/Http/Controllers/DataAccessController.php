<?php

namespace App\Http\Controllers;


use App\AppProperty;
use App\Auction;
use App\AuctionSession;
use App\DataAccess;
use App\Libraries\AuctionBidHelper;
use App\Libraries\ErrorCodes;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\Libraries\ResponseMessage;
use App\User;
use DateTime;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DataAccessController extends Controller
{
    public function __construct()
    {
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setDataAccessForUser(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }


        //check permission to Set Data Access
        $apiPermission = array(Permissions::DATA_ACCESS_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        try {

            $input = $request->json();
            $target_user_id = $input->get('user_id');

            $rangeFrom = $input->get('range_from');
            $rangeTo = $input->get('range_to');
            $include = $input->get('include');
            $exclude = $input->get('exclude');

            $validationData = [
                'token' => $token,
                'user_id' => $target_user_id,
                'range_from' => $rangeFrom,
                'range_to' => $rangeTo
            ];
            $v = Validator::make($validationData, [
                'token' => 'required|alpha_num|exists:mysql.user,token',
                'user_id' => 'required',
                'range_from' => 'required',
                'range_to' => 'required',
            ], [
                'required' => ErrorCodes::REQUIRED,
                'alpha_num' => ErrorCodes::ALPHA_NUM,
                'exists' => ErrorCodes::NOT_EXISTS

            ]);
            if ($v->fails()) {
                $errorMessages = $v->errors()->getMessages();
                return $this->responseError(ErrorCodes::INVALID, $errorMessages);
            }
            $responseMessage = new ResponseMessage();
            //todo: check if user_id has available

            $dataAccess = DataAccess::getByUserId($target_user_id);

            if ($dataAccess) {
                //if yes,update
                $dataAccess = DataAccess::find($dataAccess->id);
                $dataAccess->range_from = $rangeFrom;
                $dataAccess->range_to = $rangeTo;
                $dataAccess->include = $include;
                $dataAccess->exclude = $exclude;
                $dataAccess->save();
            } else {
                //if no,insert new record
                $dataAccess = new DataAccess();
                $dataAccess->user_id = $target_user_id;
                $dataAccess->type = DataAccess::LOCATION_TYPE;
                $dataAccess->range_from = $rangeFrom;
                $dataAccess->range_to = $rangeTo;
                $dataAccess->include = $include;
                $dataAccess->exclude = $exclude;
                $dataAccess->save();
            }
            $statusCode = 200;
            $responseMessage->setData('success', true);
            $result = $responseMessage->getMessage();

            return response()->json($result, $statusCode);
        } catch (\Exception $exception) {
            \Log::error("Exception " . $exception);
            return $this->responseError(ErrorCodes::SYS_ERR);
        }
    }

    public function getDataAccessByUser(Request $request, $userId)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        //check permission to view data access
        $apiPermission = array(Permissions::DATA_ACCESS_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if (!$valuationResult) {

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        try {
            $validationData = [
                'token' => $token,
                'user_id' => $userId,

            ];
            $v = Validator::make($validationData, [
                'token' => 'required|alpha_num|exists:mysql.user,token',
                'user_id' => 'required|alpha_num|exists:mysql.data_access,user_id',

            ], [
                'required' => ErrorCodes::REQUIRED,
                'alpha_num' => ErrorCodes::ALPHA_NUM,
                'exists' => ErrorCodes::NOT_EXISTS,
                'numeric' => ErrorCodes::NUMERIC,
            ]);
            if ($v->fails()) {
                $errorMessages = $v->errors()->getMessages();
                return $this->responseError(ErrorCodes::INVALID, $errorMessages);
            }
            $responseMessage = new ResponseMessage();
            $dataAccessInfo = DataAccess::getByUserId($userId);

            $statusCode = 200;
            $responseMessage->setData('dataAccessInfo', $dataAccessInfo);

            $result = $responseMessage->getMessage();

            return response()->json($result, $statusCode);
        } catch (\Exception $exception) {
            \Log::error("Exception " . $exception);
            return $this->responseError(ErrorCodes::SYS_ERR);
        }

    }

    public function updateDataAccessByUser(Request $request, $userId){
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        //check permission to edit data access
        $apiPermission = array(Permissions::DATA_ACCESS_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if (!$valuationResult) {

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        try {
            $validationData = [
                'token' => $token,
                'user_id' => $userId,

            ];
            $v = Validator::make($validationData, [
                'token' => 'required|alpha_num|exists:mysql.user,token',
                'user_id' => 'required|alpha_num|exists:mysql.data_access,user_id',
            ], [
                'required' => ErrorCodes::REQUIRED,
                'alpha_num' => ErrorCodes::ALPHA_NUM,
                'exists' => ErrorCodes::NOT_EXISTS,
                'numeric' => ErrorCodes::NUMERIC,
                'digits_between' => ErrorCodes::OUT_SIDE_VALID_RANGE,
            ]);
            if ($v->fails()) {
                $errorMessages = $v->errors()->getMessages();
                return $this->responseError(ErrorCodes::INVALID, $errorMessages);
            }
            $responseMessage = new ResponseMessage();
            $input = $request->json();

            $dataAccess = DataAccess::getByUserId($userId);
            if(!$dataAccess){
                return $this->responseError(ErrorCodes::NOT_FOUND);
            }
            $dataAccess = DataAccess::find($dataAccess->id);
            $rangeFrom = $input->get('range_from');
            $rangeTo = $input->get('range_to');
            $include = $input->get('include');
            $exclude = $input->get('exclude');
            if ($rangeFrom < 0 || $rangeFrom > 100 || $rangeTo < 0 || $rangeTo > 100) {
                return $this->responseError(ErrorCodes::OUT_SIDE_VALID_RANGE);
            }

            $dataAccess->range_from = $rangeFrom;
            $dataAccess->range_to = $rangeTo;
            $dataAccess->include = $include;
            $dataAccess->exclude = $exclude;
            $dataAccess->save();

            $statusCode = 200;
            $responseMessage->setData('success', true);
            $result = $responseMessage->getMessage();

            return response()->json($result, $statusCode);
        } catch (\Exception $exception) {
            \Log::error("Exception " . $exception);
            return $this->responseError(ErrorCodes::SYS_ERR);
        }
    }

}
