<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Libraries\ErrorCodes;
use App\Libraries\Event;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\Libraries\ResponseMessage;
use App\Permission;
use App\RolePermission;
use App\UserRole;
use Illuminate\Http\Request;
use App\Role;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getOne(Request $request, $role_id)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::ROLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $role = Role::find($role_id);
        if (!$role) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        return $this->responseData($role);
    }
    public function delete(Request $request, $role_id)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::CUSTOM_ROLE_DELETE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $role = Role::find($role_id);
        if (!$role) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        if($role->is_default){
            $valuationResult = Helper::valuatePermission($user->id, array(Permissions::SYSTEM_ROLE_DELETE));
            if(!$valuationResult){

                return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
            }
        }
        RolePermission::where('role_id', $role->id)->delete();
        UserRole::where('role_id', $role->id)->delete();

        $role->delete();

        return $this->responseData($role);
    }


    public function update(Request $request, $role_id)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        $v = Validator::make($request->all(), [
            'name' => 'required|string|min:1',
            'permissions' => 'required|array'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::ROLE_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $role = Role::find($role_id);
        if (!$role) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        if($role->is_default){
            $valuationResult = Helper::valuatePermission($user->id, array(Permissions::SYSTEM_ROLE_EDIT));
            if(!$valuationResult){

                return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
            }
        }

        DB::select("DELETE FROM role_permission WHERE role_id=:role_id", ['role_id' => $role_id]);

        $input = $request->json();
        $permission = $input->get('permissions');

        foreach ($permission as $p) {
            if (Permission::find($p)) {
                $rp = new RolePermission();
                $rp->role_id = $role->id;
                $rp->permission_id = $p;
                $rp->save();
            } else {
                \LOG::warn("There is no permission_id[$p]");
            }
        }

        if ($input->has('name')) {
            $name = $input->get('name');
            $role->name = $name;
            // generate code for role

            $oldCode = $role->code;

            $code = strtolower($name);
            $code = str_replace(" ","_",$code);
            $company_group_id = $role->company_group_id;
            $code = $company_group_id . "_" . $code;
            if($code != $oldCode){
                //check if new code is duplicate with other
                $validateCode = DB::select("SELECT * from role where (`code` = '$code' OR `name` = '$name') AND `company_group_id` = $company_group_id");
                if($validateCode && $validateCode[0]->id != $role_id) {
                    return $this->responseError(ErrorCodes::ROLE_CODE_DUPLICATE);
                }
            }
            $role->code = $code;
            if($input->has('is_default')){
                $role->is_default = $input->get('is_default');
            }
            $role->save();

            // update channel with new code
            $channel = Channel::updateByName($oldCode,$code);

        }

        return $this->responseData($role);
    }

    public function getAll(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        //check only user is admin to view roles
        $apiPermission = array(Permissions::ROLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $roles = Role::listAll();
        return $this->responseData($roles);


    }

    public function create(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::ROLE_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'name' => 'required|string|min:1',
            'permissions' => 'required|array'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $input = $request->json();

        $permissions = $input->get('permissions');

        $role = new Role();
        $name = $input->get('name');
        $role->name = $name;
        // generate code for new role
        $code = strtolower($name);
        $code = str_replace(" ","_",$code);
        $company_group_id = $user->company_group_id;
        $code = $company_group_id . "_" . $code;
        $validateCode = DB::select("SELECT * from role where (`code` = '$code' OR `name` = '$name') AND `company_group_id` = $company_group_id  ");

        if($validateCode){
            return $this->responseError(ErrorCodes::ROLE_CODE_DUPLICATE);
        }
        $role->code = $code;
        $role->company_group_id = $user->company_group_id;
        if($input->has('is_default')){

            $valuationResult = Helper::valuatePermission($user->id, array(Permissions::SYSTEM_ROLE_CREATE));
            if(!$valuationResult){

                return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
            }

            $role->is_default = $input->get('is_default');
        }
        $role->save();

        // add new role to channel
        $channel = new Channel();
        $channel->name = $code;
        $channel->type = "role";
        $channel->save();

        foreach ($permissions as $p) {
            if (Permission::find($p)) {
                $rp = new RolePermission();
                $rp->role_id = $role->id;
                $rp->permission_id = $p;
                $rp->save();
            } else {
                \LOG::warn("There is no permission_id[$p]");
            }
        }
        return $this->responseData($role);
    }

    public function getRolePermissions(Request $request, $role_id)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::ROLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $role = Role::find($role_id);
        if (!$role) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        $r = RolePermission::getListPerMissionByRole($role_id);
        return $this->responseData($r);
    }

    public function searchRolesList(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;


        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        //check permission to view roles
        $apiPermission = array(Permissions::ROLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $v = Validator::make($request->all(), [

            'query' => 'max:100',
            'limit' => 'required|integer|min:1|max:100',
            'offset' => 'required|integer|min:0',
            'orderBy' => 'in:first_name,last_name',
            'orderDesc' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $input = $request->json();
        $responseMessage = new ResponseMessage();

        $roles = new Role();
        //Get list user with pagination and full size
        $company_group_id = $request->json()->get('company_group_id');
        if(!$company_group_id){
            $company_group_id = $user->company_group_id;
        }
        $rolesList = $roles->searchAllRoles($input, $company_group_id);

        $statusCode = 200;

        $responseMessage->setData('list_size', $rolesList[0]);
        $responseMessage->setData('roleList', $rolesList['rolesList']);

        $result = $responseMessage->getMessage();

        return response()->json($result, $statusCode);

    }
}
