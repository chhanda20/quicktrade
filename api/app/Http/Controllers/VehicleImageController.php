<?php

namespace App\Http\Controllers;

use App\Libraries\ErrorCodes;
use App\User;
use App\Vehicle;
use App\VehicleImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DateTime;

class VehicleImageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getPrimary(Request $request, $vehicle_id)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $vehicle = Vehicle::find($vehicle_id);
        if (!$vehicle) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, [], "No such vehicle");
        }

        $result = VehicleImage::where('vehicle_id', $vehicle_id)->where('is_primary', TRUE)->first();

        if ($result == NULL) {
            return $this->responseData([]);
        }

        $result->image = env('VEHICLE_IMAGE_URL') . $result->image;
        return $this->responseData($result);
    }

    public function create(Request $request, $vehicle_id)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $vehicle = Vehicle::find($vehicle_id);
        if (!$vehicle) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, [], "No such vehicle");
        }

        $v = Validator::make($request->all(), [
            'image' => 'required|max:50',
            'seq_no' => 'integer',
            'is_primary' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $json = $request->json();

        $image = new VehicleImage();
        $image->vehicle_id = $vehicle_id;

        $image->image = $json->get('image');
        $image->image = substr($image->image, strrpos($image->image, '/') + 1);

//        if ($json->has('is_primary')) {
//            $image->is_primary = $json->get('is_primary') || TRUE;
//        } else {
//            $image->is_primary = true;
//        }


        $image->is_primary = $json->get('is_primary') || TRUE;
        $image->seq_no = $json->get('seq_no');
        $image->creator_id = $image->updater_id = $user->id;
        $image->create_date = $image->update_date = (new DateTime('NOW'))->format('Y-m-d H:i:s');

        $image->save();

        return $this->responseData($image);
    }

    public function get(Request $request, $vehicle_id, $image_id)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $vehicle = Vehicle::find($vehicle_id);
        if (!$vehicle) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, [], "No such vehicle");
        }

        $result = VehicleImage::find($image_id);

        if (!$result) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, [], "No such image");
        }

        $result->image = config('VEHICLE_IMAGE_URL') + $result->image;

        return $this->responseData($result);
    }

    public function update(Request $request, $vehicle_id, $image_id)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $vehicle = Vehicle::find($vehicle_id);
        if (!$vehicle) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, [], "No such vehicle");
        }

        $image = VehicleImage::find($image_id);

        if (!$image) {
            return $this->responseError(ErrorCodes::NOT_EXISTS, [], "No such image");
        }

        $json = $request->json();

        if ($json->get('image')) {
            $image->image = $json->get('image');
            $image->image = substr($image->image, strrpos($image->image, '/') + 1);
        }
        if ($json->get('is_primary')) {
            $image->is_primary = $json->get('is_primary');
        }
        if ($json->get('seq_no')) {
            $image->seq_no = $json->get('seq_no');
        }
        $image->creator_id = $image->updater_id = $user->id;
        $image->create_date = $image->update_date = (new DateTime('NOW'))->format('Y-m-d H:i:s');
        $image->save();
    }
}
