<?php

namespace App\Http\Controllers;



use App\AuctionSession;
use App\Libraries\ErrorCodes;
use App\Libraries\Event;
use App\Libraries\Helper;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;
use WindowsAzure\Common\ServicesBuilder;
use MicrosoftAzure\Storage\Blob\Models\CreateContainerOptions;
use MicrosoftAzure\Storage\Blob\Models\PublicAccessType;
use MicrosoftAzure\Storage\Common\ServiceException;


class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function hello()
    {

    }

    public function exec()
    {
        AuctionSession::autoCreateTransactions();
    }
    public function pushnoti(Request $request) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }


        $v = Validator::make($request->all(), [
            'title' => 'required|string',
            'message' => 'required|string',
            'data' => 'required|Array'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);
        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        try{
            $input = $request->json();

            $title = $input->get('title');
            $message = $input->get('message');
            $customData = $input->get('data');
            Helper::notify(Event::TRANSACTION_NEW, $user->id, $title, $message, $customData, Event::PARAMS_TYPE_USER);
            //test
        }catch (Exception $ex){
            \LOG::error("Exception ". $ex);
        }

        return $this->responseData([]);
    }
}
