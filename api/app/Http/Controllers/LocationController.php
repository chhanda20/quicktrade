<?php

namespace App\Http\Controllers;

use App\Libraries\ErrorCodes;
use App\Libraries\Event;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\Libraries\ResponseMessage;
use App\Location;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LocationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::LOCATION_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'name' => 'required|min:1|max:256',
            'location_code' => 'numeric',
            'status_code' => 'required|min:1|max:2',
            'company_id' => 'required|min:1|max:256',
            'address' => 'required|min:1|max:256',
            'lmct' => 'min:1|max:256',
            'suburb' => 'required',
            'country' => 'required|min:1|max:256',
            'state' => 'required|min:1|max:256',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        //check location code is unique in company group 
        $locationCode = $request->json()->get('location_code');
        $companyId = $request->json()->get('company_id');
        $checkUniqueLocationCode = Location::getLocationCodeInCompanyGroup($locationCode,$companyId);
        if($checkUniqueLocationCode){
            return $this->responseError(ErrorCodes::LOCATION_CODE_DUPLICATE);
        }

        $location = new Location();

        $input = $request->toArray();
        $input = array_filter($input, 'strlen'); // remove null values

        $location->fill($input);

        $location->save();

        return $this->responseData($location);
    }

    public function find(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::LOCATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'query' => 'max:100',
            'company_id' => 'required|integer|min:0',
            'limit' => 'required|integer|min:1',
            'offset' => 'required|integer|min:0',
            'orderBy' => 'in:name',
            'orderDesc' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $input = $request->json();
        $company_id = $input->get('company_id');
        $offset = $input->get('offset');
        $limit = $input->get('limit');

        $query = '';
        $orderBy = '';

        if ($input->has('query')) {
            $query = " AND location.name LIKE '%" . $input->get('query') . "%' ";
        }

        if ($input->has('orderBy')) {
            $orderBy = " ORDER BY " . $input->get('orderBy');

            if ($input->has('orderDesc')) {
                $orderBy .= ' DESC ';
            }
        }

        $accessibleLocationIds = implode(',', $user->getAccessableLocationIds());
        $limitAccessQuery = $user->isMo() ? " " : " AND location.id IN ($accessibleLocationIds) ";

        $r = DB::select("
            SELECT location.*, 
                    country_state.name as state_text
            FROM location 
            LEFT JOIN country_state 
              ON location.state = country_state.id
            WHERE company_id=$company_id 
              $limitAccessQuery
            $query
            $orderBy
            LIMIT $offset, $limit    
        ");

        foreach ($r as $item) {
        $item->quick_seller   = Location::isSpeedySeller($item->id);
        $item->quick_buyer    = Location::isSpeedyBuyer($item->id);
    }

        return $this->responseData($r);
    }

    public function findCount(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::LOCATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'query' => 'max:100',
            'company_id' => 'required|integer|min:0'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $input = $request->json();
        $company_id = $input->get('company_id');
        $query = '';
        if ($input->has('query')) {
            $query = " AND name LIKE '%" . $input->get('query') . "%' ";
        }


        $accessibleLocationIds = implode(',', $user->getAccessableLocationIds());

        $r = DB::select("
            SELECT COUNT(*) AS count FROM location WHERE company_id=$company_id AND location.id IN ($accessibleLocationIds) 
            $query 
        ");
        return $this->responseData($r[0]);
    }

    public function update(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::LOCATION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'name' => 'min:1|max:256',
            'location_code' => 'numeric',
            'status_code' => 'min:1|max:2',
            'company_id' => 'min:1|max:256',
            'address' => 'required|min:1|max:256',
            'lmct' => 'min:1|max:256',
            'suburb' => 'required',
            'country' => 'required|min:1|max:256',
            'state' => 'required|min:1|max:256',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $location = Location::find($id);

        //check location code is unique in company group
        $locationCode = $request->json()->get('location_code');
        $companyId = $request->json()->get('company_id');
        $checkUniqueLocationCode = Location::getLocationCodeInCompanyGroup($locationCode,$companyId);
        
        if($checkUniqueLocationCode !=null && $checkUniqueLocationCode[0]->id != $id){
            return $this->responseError(ErrorCodes::LOCATION_CODE_DUPLICATE);
        }

        if (!$location) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        $input = $request->toArray();
        $input = array_filter($input, 'strlen'); // remove null values

        $location->fill($input);
        $location->save();

        return $this->responseData($location);
    }

    public function getOne(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::LOCATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $location = Location::getLocationById($id);

        if (!$location) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        $location[0]->quick_seller   = Location::isSpeedySeller($id);
        $location[0]->quick_buyer    = Location::isSpeedyBuyer($id);

        return $this->responseData($location);
    }

    public function getListLocationAccess(Request $request){
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $apiPermission = array(Permissions::LOCATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $accessibleLocationIds = $user->getAccessableLocationIds();

        \Log::info('$accessibleLocationIds=' . print_r($accessibleLocationIds, true));

        $listLocation = Location::getListLocation($user->company_group_id, $accessibleLocationIds,null);

        \Log::info('$listLocation=' . print_r($listLocation, true));
        return $this->responseData($listLocation['locationList']);
    }

    public function getListAll(Request $request){
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $apiPermission = array(Permissions::LOCATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $input = $request->json();
        $listLocation = Location::getListLocation($user->company_group_id, null,$input);
        $responseMessage = new ResponseMessage();

        $responseMessage->setData('list_size', $listLocation[0]);
        $responseMessage->setData('locationList', $listLocation['locationList']);

        foreach ($listLocation['locationList'] as $item) {
            $item->quick_seller   = Location::isSpeedySeller($item->id);
            $item->quick_buyer    = Location::isSpeedyBuyer($item->id);
        }

        $result = $responseMessage->getMessage();
        return $this->responseData($result);
    }
}
