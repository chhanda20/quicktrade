<?php

namespace App\Http\Controllers;


use App\AppProperty;
use App\Auction;
use App\AuctionSession;
use App\Libraries\AuctionBidHelper;
use App\Libraries\ErrorCodes;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\Libraries\ResponseMessage;
use App\Menu;
use App\RoleHasMenu;
use App\SessionHasItem;
use App\TestDrive;
use App\User;
use App\WishList;
use DateTime;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MenuController extends Controller
{
    public function __construct()
    {
    }


    public function getListMenu(Request $request){
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }


        //check permission to View Menu
        $apiPermission = array(Permissions::MENU_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        try {
            $validationData = [
                'token' => $token,

            ];
            $v = Validator::make($validationData, [
                'token' => 'required|alpha_num|exists:mysql.user,token'


            ], [
                'required' => ErrorCodes::REQUIRED,
                'alpha_num' => ErrorCodes::ALPHA_NUM,
                'exists' => ErrorCodes::NOT_EXISTS,
                'numeric' => ErrorCodes::NUMERIC,
            ]);
            if ($v->fails()) {
                $errorMessages = $v->errors()->getMessages();
                return $this->responseError(ErrorCodes::INVALID, $errorMessages);
            }
            $responseMessage = new ResponseMessage();

            $menuListDB = Menu::getListMenu();

            $menuList = $this->convertMenuFromDBToJSonFormat($menuListDB);


            $statusCode = 200;

            $responseMessage->setData('menuList',$menuList);
            $result = $responseMessage->getMessage();

            return response()->json($result, $statusCode);
        } catch (\Exception $exception) {
            \Log::error("Exception " . $exception);
            return $this->responseError(ErrorCodes::SYS_ERR);
        }
    }

   public function setMenuForRole(Request $request){
       $token = $request->header("token");
       $user = NULL;

       if ($token) {
           $user = User::where('token', $token)->first();
       }

       if (!$user) {
           return $this->responseError(ErrorCodes::INVALID_TOKEN);
       }


       //check permission to set Menu
        $apiPermission = array(Permissions::MENU_SET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
       try {
           $roleId = $request->json()->get('role_id');
           $menuList = $request->json()->get('menu_id');
           $updateMoMenu = $request->json()->get('update_dts_menu');

           $validationData = [
               'token' => $token,
               'role_id' => $roleId,
               'menu_id' => $menuList
           ];
           $v = Validator::make($validationData, [
               'token' => 'required|alpha_num|exists:mysql.user,token',
               'role_id' => 'required|alpha_num|exists:mysql.role,id',
               'menu_id' => 'required|array'
           ], [
               'required' => ErrorCodes::REQUIRED,
               'alpha_num' => ErrorCodes::ALPHA_NUM,
               'exists' => ErrorCodes::NOT_EXISTS,
               'numeric' => ErrorCodes::NUMERIC,
           ]);
           if ($v->fails()) {
               $errorMessages = $v->errors()->getMessages();
               return $this->responseError(ErrorCodes::INVALID, $errorMessages);
           }
           $responseMessage = new ResponseMessage();

           $currentMoIds = [];
           if (!$updateMoMenu) {
               $currentIds      = RoleHasMenu::where('role_id', $roleId)->pluck('menu_id')->toArray();
               $dtsMenuIds      = Menu::where('site_menu', 'dts_admin')->pluck('code')->toArray();
               $currentMoIds   = array_intersect($currentIds, $dtsMenuIds);
           }

           //remove role-menu
           DB::statement("delete from role_has_menu where role_id = $roleId");

           $menuList = array_unique(array_merge($menuList, $currentMoIds), SORT_REGULAR);

           $roleHasMenu = new RoleHasMenu();
           $data = array();
           foreach($menuList as $menuId){
               $currentDate = date('Y-m-d H:i:s');
               $temp = array('menu_id'=>$menuId, 'role_id'=> $roleId, 'creator_id' => $user->id, 'create_date' => $currentDate);
               array_push($data,$temp);
           }

           $roleHasMenu->insert($data); // Query Builder


           $statusCode = 200;
           $responseMessage->setData('success', true);

           $result = $responseMessage->getMessage();

           return response()->json($result, $statusCode);
       } catch (\Exception $exception) {
           \Log::error("Exception " . $exception);
           return $this->responseError(ErrorCodes::SYS_ERR);
       }
   }

   public function getRoleMenu(Request $request, $roleId){
       $token = $request->header("token");
       $user = NULL;

       if ($token) {
           $user = User::where('token', $token)->first();
       }

       if (!$user) {
           return $this->responseError(ErrorCodes::INVALID_TOKEN);
       }


       //check permission to view Menu for each role
        $apiPermission = array(Permissions::MENU_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
       try{
           $validationData = [
               'token' => $token,
               'role_id' => $roleId,

           ];
           $v = Validator::make($validationData, [
               'token' => 'required|alpha_num|exists:mysql.user,token',
               'role_id' => 'required|alpha_num|exists:mysql.role,id',

           ], [
               'required' => ErrorCodes::REQUIRED,
               'alpha_num' => ErrorCodes::ALPHA_NUM,
               'exists' => ErrorCodes::NOT_EXISTS,
               'numeric' => ErrorCodes::NUMERIC,
           ]);
           if ($v->fails()) {
               $errorMessages = $v->errors()->getMessages();
               return $this->responseError(ErrorCodes::INVALID, $errorMessages);
           }
           $responseMessage = new ResponseMessage();

           $menuListDB = Menu::getMenuListForRole($roleId);
           $menuList = $this->convertMenuFromDBToJSonFormat($menuListDB);


           $statusCode = 200;

           $responseMessage->setData('menuList',$menuList);

           $result = $responseMessage->getMessage();

           return response()->json($result, $statusCode);
       } catch (\Exception $exception) {
           \Log::error("Exception " . $exception);
           return $this->responseError(ErrorCodes::SYS_ERR);
       }

   }

    /**
     * @param $menuListDB
     * @return array
     */
    public function convertMenuFromDBToJSonFormat($menuListDB)
    {
//loop to group all menu item to correct group

        $newArray = array();
        $menuList = array();
        foreach ($menuListDB as $row) {
            $newArray[$row->site_menu][$row->group_name][] = $row;
        }

        //loop to set correct format and return to FE
        foreach ($newArray as $key => $arr) {

            $siteArray = array('site_menu' => $key);
            $groupArray = array();
            $groupItem = array();
            foreach ($arr as $key1 => $m) {
                $groupItem['group_name'] = $key1;
                $groupItem['menu_items'] = $m;
                array_push($groupArray, $groupItem);

            }
            $siteArray['group'] = $groupArray;
            array_push($menuList, $siteArray);
        }
        return $menuList;
    }
}
