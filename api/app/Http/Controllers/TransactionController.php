<?php

namespace App\Http\Controllers;

use App\Country;
use App\CountryState;
use App\Dispute;
use App\DisputeItem;
use App\DisputeNote;
use App\Document;
use App\ExternalOffer;
use App\InspectionItem;
use App\Location;
use App\Libraries\ErrorCodes;
use App\Libraries\Event;
use App\Libraries\Helper;
use App\Libraries\IdTransformer;
use App\Libraries\PDFGeneratorService;
use App\Libraries\Permissions;
use App\Transaction;
use App\User;
use App\Valuation;
use App\ValuationInspectionItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use WindowsAzure\Common\ServiceException;
use WindowsAzure\Common\ServicesBuilder;
use App\Http\Controllers\Log;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function setStatusReturned(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $t = Transaction::find($id);
        if (!$t) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

//        if ($t->status != Transaction::STATUS_IN_TRANSIT) {
//            return $this->responseError(ErrorCodes::INVALID, 'Not in transport booked');
//        }
        $t->status = Transaction::STATUS_DOCUMENTING;
        $t->save();

//        $title = 'TRANSACTION HAS CANCELED';
//        $message = 'The vehicle is returning';
//        $customData = ['transactionId' => $t->id, 'transactionRid' => $t->rid];
//        Helper::notify(Event::TRANSACTION_NOTIFY, $t->seller_id, $title, $message, $customData);

        return $this->responseData($t);
    }

    public function setStatusReturning(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $t = Transaction::find($id);
        if (!$t) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

//        if ($t->status != Transaction::STATUS_IN_TRANSIT) {
//            return $this->responseError(ErrorCodes::INVALID, 'Not in transport booked');
//        }
        $t->status = Transaction::STATUS_RETURNING;
        $t->save();

        $title = 'TRANSACTION HAS CANCELED';
        $message = 'The vehicle is returning';
        $customData = ['transactionId' => $t->id, 'transactionRid' => $t->rid];
        // Helper::notify(Event::TRANSACTION_NOTIFY, $t->seller_id, $title, $message, $customData, Event::PARAMS_TYPE_USER);
        User::notice($t->seller_id, Event::TRANSACTION_NEW, $t->id, $customData);

        return $this->responseData($t);
    }

    public function shipped(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $t = Transaction::find($id);
        if (!$t) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        if ($t->status != Transaction::STATUS_IN_TRANSIT) {
            return $this->responseError(ErrorCodes::INVALID, 'Not in transport booked');
        }

        $t->status = Transaction::STATUS_ARRIVED;
        $t->save();

        if ($t->source == Transaction::SOURCE_EXTERNAL_OFFER) {
            $externalOffer = ExternalOffer::find($t->source_id);
            $externalOffer->status = ExternalOffer::STATUS_DELIVERED;
            $externalOffer->save();
        }

        $event = Event::TRANSACTION_VEHICLE_ARRIVED;
        $buyerData  = ['transactionId' => $t->id, 'transactionRid' => $t->rid];
        $sellerData = ['transactionId' => $t->id, 'transactionRid' => $t->rid];

        User::notice($t->seller_id, $event, $t->id, $sellerData);
        User::notice($t->buyer_id, $event, $t->id, $buyerData);

        return $this->responseData($t);
    }

    public function inTransit(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $t = Transaction::find($id);
        if (!$t) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        if ($t->status != Transaction::STATUS_TRANSPORT_BOOKED) {
            return $this->responseError(ErrorCodes::INVALID, 'Not in transport booked');
        }
        $t->status = Transaction::STATUS_IN_TRANSIT;
        $t->save();

        $event = Event::TRANSACTION_VEHICLE_PICKED_UP;
        $sellerData = [];
        $buyerData  = [];

        User::notice($t->seller_id, $event, $t->id, $sellerData);
        User::notice($t->buyer_id, $event, $t->id, $buyerData);

        return $this->responseData($t);
    }

    public function transportBooked(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $t = Transaction::find($id);
        if (!$t) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        if ($t->status != Transaction::STATUS_BOOKING_TRANSPORT) {
            return $this->responseError(ErrorCodes::INVALID, 'Not in booking transport status');
        }
        $t->status = Transaction::STATUS_TRANSPORT_BOOKED;
        $t->transport_no = $request->json()->get('transport_no');
        $t->save();

        if ($t->source == Transaction::SOURCE_EXTERNAL_OFFER) {
            $externalOffer = ExternalOffer::find($t->source_id);
            $externalOffer->status = ExternalOffer::STATUS_SHIPPED;
            $externalOffer->save();
        }

        return $this->responseData($t);
    }

    public function readyForTransport(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $t = Transaction::find($id);
        if (!$t) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        if ($t->status != Transaction::STATUS_PAYMENT_RECEIVED) {
            return $this->responseError(ErrorCodes::INVALID, 'Not in payment received status');
        }
        $t->status = Transaction::STATUS_BOOKING_TRANSPORT;
        $t->save();

        if ($t->source == Transaction::SOURCE_EXTERNAL_OFFER) {
            $externalOffer = ExternalOffer::find($t->source_id);
            $externalOffer->status = ExternalOffer::STATUS_TRANSPORT_TRIGGERED;
            $externalOffer->save();
        }

        return $this->responseData($t);
    }

    public function paymentReceived(Request $request) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'payments' => 'required|array',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $input = $request->json();
        $payments = $input->get('payments');

        $errors = [];

        foreach($payments as $payment) {
            if ($payment['rid']) {
                $t = Transaction::where('rid', $payment['rid'])->first();
                if (!$t) {
                    $errors[$payment['rid']] = [
                        'error' => ErrorCodes::NOT_FOUND
                    ];
                    \Log::debug("sellerId: ErrorCodes::NOT_FOUND");
                } else {
                    $disputeId = null;
                    $listDispute = Dispute::getListDisputeByTransactionId($t->id, Dispute::STATUS_RESOLVE);
                    $disputeAdjustCost = 0;
                    foreach ($listDispute as $dispute){
                        $listDisputeItems = DisputeItem::getListItemByDisputeId($dispute->id);
                        $costDispute = $dispute->general_recondition_cost + $dispute->general_recondition_cost_variance;
                        foreach ($listDisputeItems as $disputeItem){
                            $cost = 0;
                            if($disputeItem->type == DisputeItem::TYPE_INSPECTION){
                                $cost = $disputeItem->repair_cost + $disputeItem->cost_variance;
                            }else{
                                $cost = $disputeItem->repair_cost - $disputeItem->cost_variance;
                            }
                            $costDispute += $cost;
                        }
                        $disputeAdjustCost += $costDispute;
                    }

                    $totalPrice = $disputeAdjustCost+$t->vehicle_price + $t->buyer_fee + $t->shipping_price;
                    // if($payment['amount'] < $totalPrice){
                    //     $errors[$payment['rid']] = [
                    //         'error' => ErrorCodes::PAYMENT_NOT_ENOUGH
                    //     ];
                    //     return $this->responseData($errors);
                    // }

                    $err = $t->setPaymentReceived($payment['amount']);
                    if ($err) {
                        $errors[$t->rid] = $err;
                    }

                    $event = Event::TRANSACTION_PAYMENT_RECEIVE;
                    $data = [];
                    User::notice($t->buyer_id, $event, $t->id, $data);
                }
            }
        }



        return $this->responseData($errors);
    }

    public function find(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'offset' => 'required',
            'limit' => 'required',
            'status' => 'array',
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $apiPermission = array(Permissions::TRANSACTION_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $json = $request->json();

        $offset = $json->get('offset');
        $limit = $json->get('limit');
        $status = $json->get('status');

        $locations = $user->getAccessableLocationIds();
        $locations = implode(',', $locations);

        $transaction_type = $json->get('transaction_type');


        $transaction_type_select = '';
        $location_query = '';
        if($request->path() != 'transaction/admin/_find'){
            if($transaction_type){
                if($transaction_type == "buy"){
                    $location_query .= " t.buyer_location IN ($locations) ";
                    $transaction_type_select = ", 'In' as transaction_type ";
                }

                else if($transaction_type == "sell"){
                    $location_query .= " t.seller_location IN ($locations) ";
                    $transaction_type_select = ", 'Out' as transaction_type ";
                }



            }else{
                $transaction_type_select =", IF(t.seller_location In ($locations), 'Out','In') as transaction_type";
                $location_query .= " (t.buyer_location IN ($locations) OR t.seller_location IN ($locations)) ";
            }
        }


        if ($status) {
            if($location_query)
                $location_query .= ' AND ' ;
            $location_query .= ' t.status IN (' . implode(',', $status) . ") ";
        }
        $where = ' WHERE t.valuation_id IS NOT NULL';
        if($location_query)
            $where .= ' AND '.$location_query;

        // apply filter by status, buyer/seller, year,make,model
        $input = $request->json();
        //filter by buyer/seller
        if($input->has('region')){

        }



        if ($input->has("year")) {
            $year = $input->get('year');
            if (sizeof($year) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'year must be an array contains 2 number');
            }
            $where .= " AND (year(ve.build_date) between $year[0] AND $year[1])";
        }

        $make = collect();
        $model = collect();
        $variant = collect();

        if ($input->has("makeInfo")) {
            $arr = $input->get('makeInfo');
            foreach ($arr as $i) {
                $i = (object) $i;

                $i->make = str_replace('\'', '"', $i->make);
                $i->make = trim($i->make);

                $i->model = str_replace('\'', '"', $i->model);
                $i->model = trim($i->model);

                $i->variant = str_replace('\'', '"', $i->variant);
                $i->variant = trim($i->variant);

                if ($i->make) {
                    $make->push("'" . $i->make. "'" );
                }
                if ($i->model) {
                    $model->push("'" . $i->model. "'" );
                }
                if ($i->variant) {
                    $variant->push("'" . $i->variant. "'" );
                }
            }
        }

        $make = $make->unique()->values()->all();
        $model = $model->unique()->values()->all();
        $variant = $variant->unique()->values()->all();

        if (sizeof($make) > 0) {
            $make = implode(",", $make);
            $where .= " AND (make IN ($make))";
        }
        if (sizeof($model) > 0) {
            $model = implode(",", $model);
            $where .= " AND (model_family IN ($model))";
        }
        if (sizeof($variant) > 0) {
            $variant = implode(",", $variant);
            $where .= " AND (variant IN ($variant))";
        }

        $result = DB::select("
          SELECT ve.*,ve.id vehicle_id, valuer.id valuer_id, valuer_contact.first_name valuer_first_name, valuer_contact.last_name valuer_last_name,
           salesperson.id salesperson_id, salesperson_contact.first_name salesperson_first_name, salesperson_contact.last_name salesperson_last_name, va.overall_rating, 
           buy_dealer.name location_name_buyer,
           sell_dealer.name location_name_seller,   
           CONCAT_WS(' ',buy_dealer.name,buy_dealer.company_name) buying_dealer,
           CONCAT_WS(' ',sell_dealer.name,sell_dealer.company_name) selling_dealer,
           t.* $transaction_type_select
           FROM transaction t 
            LEFT JOIN valuation va ON (va.version=t.valuation_version AND (va.id=t.valuation_id))
            LEFT JOIN vehicle ve ON ve.id=va.vehicle_id
            LEFT JOIN `user` valuer ON valuer.id=va.valuer_id
            LEFT JOIN contact valuer_contact ON valuer_contact.id=valuer.contact_id
            LEFT JOIN `user` salesperson ON salesperson.id=va.user_id
            LEFT JOIN contact salesperson_contact ON salesperson_contact.id=salesperson.contact_id
            LEFT JOIN (select lo.*, contact.company_name from `location` lo JOIN company on lo.company_id = company.id JOIN contact on company.contact_id = contact.id) as buy_dealer ON buy_dealer.id = t.buyer_location
LEFT JOIN (select lo.*, contact.company_name from `location` lo JOIN company on lo.company_id = company.id JOIN contact on company.contact_id = contact.id) as sell_dealer ON sell_dealer.id = t.seller_location            
              $where
            ORDER BY t.create_date DESC
            LIMIT $offset, $limit
        ");

        foreach($result as $r) {
            $details = [];
            if ($r->make) {
                $details[] = trim($r->make);
            }
            if ($r->family) {
                $details[] = trim($r->family);
            }
            if ($r->series) {
                $details[] = trim($r->series);
            }
            if ($r->style) {
                $details[] = trim($r->style);
            }
            if ($r->engine_type) {
                $details[] = trim($r->engine_type);
            }
            if ($r->engine_capacity) {
                $details[] = trim($r->engine_capacity);
            }
            if ($r->engine_size) {
                $details[] = trim($r->engine_size);
            }
            if ($r->variant) {
                $details[] = trim($r->variant);
            }
            if ($r->cylinder) {
                $details[] = trim($r->cylinder);
            }
            if ($r->transmission) {
                $details[] = trim($r->transmission);
            }
            if ($r->build_date) {
                $array = explode('-', $r->build_date);
                if (count($array) > 0) {
                    $details[] = $array[0];
                }
            }
            $r->vehicle_name = implode(' ', $details);
        }

        return $this->responseData($result);
    }

    public function findCount(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'status' => 'array'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $apiPermission = array(Permissions::TRANSACTION_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $json = $request->json();

        $status = $json->get('status');
        $transaction_type = $json->get('transaction_type');

        $locations = $user->getAccessableLocationIds();
        $locations = implode(',', $locations);


        $location_query = '';
        if($request->path() != 'transaction/admin/_findCount'){
            if($transaction_type){
                if($transaction_type == "buy")
                    $location_query .= " t.buyer_location IN ($locations) ";
                else if($transaction_type == "sell")
                    $location_query .= " t.seller_location IN ($locations) ";
            }else{
                $location_query .= " (t.buyer_location IN ($locations) OR t.seller_location IN ($locations) )";
            }
        }


        if ($status) {
            if($location_query)
                $location_query .= ' AND ' ;
            $location_query .= ' t.status IN (' . implode(',', $status) . ") ";
        }

        $where = ' WHERE t.valuation_id IS NOT NULL';
        if($location_query)
            $where .= ' AND '.$location_query;

        // apply filter by status, buyer/seller, year,make,model
        $input = $request->json();
        //filter by buyer/seller
        if($input->has('region')){

        }



        if ($input->has("year")) {
            $year = $input->get('year');
            if (sizeof($year) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'year must be an array contains 2 number');
            }
            $where .= " AND (year(ve.build_date) between $year[0] AND $year[1])";
        }

        $make = collect();
        $model = collect();
        $variant = collect();

        if ($input->has("makeInfo")) {
            $arr = $input->get('makeInfo');
            foreach ($arr as $i) {
                $i = (object) $i;

                $i->make = str_replace('\'', '"', $i->make);
                $i->make = trim($i->make);

                $i->model = str_replace('\'', '"', $i->model);
                $i->model = trim($i->model);

                $i->variant = str_replace('\'', '"', $i->variant);
                $i->variant = trim($i->variant);

                if ($i->make) {
                    $make->push("'" . $i->make. "'" );
                }
                if ($i->model) {
                    $model->push("'" . $i->model. "'" );
                }
                if ($i->variant) {
                    $variant->push("'" . $i->variant. "'" );
                }
            }
        }

        $make = $make->unique()->values()->all();
        $model = $model->unique()->values()->all();
        $variant = $variant->unique()->values()->all();

        if (sizeof($make) > 0) {
            $make = implode(",", $make);
            $where .= " AND (make IN ($make))";
        }
        if (sizeof($model) > 0) {
            $model = implode(",", $model);
            $where .= " AND (model_family IN ($model))";
        }
        if (sizeof($variant) > 0) {
            $variant = implode(",", $variant);
            $where .= " AND (variant IN ($variant))";
        }
        $r = DB::select("
            SELECT COUNT(*) AS count
            FROM transaction t
                LEFT JOIN valuation va ON (va.version=t.valuation_version AND (va.id=t.valuation_id))
                LEFT JOIN vehicle ve ON ve.id=va.vehicle_id
                LEFT JOIN `user` valuer ON valuer.id=va.valuer_id
                LEFT JOIN contact valuer_contact ON valuer_contact.id=valuer.contact_id
                LEFT JOIN `user` salesperson ON salesperson.id=va.user_id
                LEFT JOIN contact salesperson_contact ON salesperson_contact.id=salesperson.contact_id
                LEFT JOIN (select lo.*, contact.company_name from `location` lo JOIN company on lo.company_id = company.id JOIN contact on company.contact_id = contact.id) as buy_dealer ON buy_dealer.id = t.buyer_location
            LEFT JOIN vehicle vh ON vh.id=va.vehicle_id
            $where
" );
        return $this->responseData($r[0]);
    }

    public function doInspect(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        //check permission
        $apiPermission = array(Permissions::DISPUTE_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $t = Transaction::find($id);
        if (!$t) {
            return $this->responseError(ErrorCodes::NOT_EXISTS);
        }

        $t->inspect_time =  date('Y-m-d H:i:s');

        $t->status = Transaction::STATUS_IN_INSPECTION;

        $t->save();
        return $this->responseData($t);
    }

    public function addItem(Request $request) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
    }

    public function getOne(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $apiPermission = array(Permissions::TRANSACTION_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
//        $t = Transaction::find($id);
        $t = Transaction::getOneById($id);
        //todo check if user can access in this transaction
        $locations = $user->getAccessableLocationIds();

        if(in_array($t->seller_location, $locations)){
            $t->transaction_type = "Out";
        }else if(in_array($t->buyer_location, $locations)){
            $t->transaction_type = "In";
        }
        // add sell_dispute_adjust,buy_dispute_adjust, buy_transport. BE only return positive number, FE must format and add negative wherever it needs
        $disputeId = null;
        $listDispute = Dispute::getListDisputeByTransactionId($id, Dispute::STATUS_RESOLVE);
        $disputeAdjustCost = 0;
        foreach ($listDispute as $dispute){
            $listDisputeItems = DisputeItem::getListItemByDisputeId($dispute->id);
            $costDispute = $dispute->general_recondition_cost + $dispute->general_recondition_cost_variance;
            foreach ($listDisputeItems as $disputeItem){
                $cost = 0;
                if($disputeItem->type == DisputeItem::TYPE_INSPECTION){
                    $cost = $disputeItem->repair_cost + $disputeItem->cost_variance;
                }else{
                    $cost = $disputeItem->repair_cost - $disputeItem->cost_variance;
                }
                $costDispute += $cost;
            }
            $disputeAdjustCost += $costDispute;
            $disputeId = $dispute->id;
        }
        $t->dispute_id          = $disputeId;

        $sell_dispute_adjust    = $disputeAdjustCost;
        $buy_dispute_adjust     = $disputeAdjustCost;
        $t->sell_dispute_adjust = $sell_dispute_adjust;
        $t->buy_dispute_adjust  = $buy_dispute_adjust;
        $sellerLocation         = Location::find($t->seller_location);
        $buyerLocation          = Location::find($t->buyer_location);
        $t->seller_address      = $sellerLocation->name.' - '.$sellerLocation->address;
        $t->buyer_address       = $buyerLocation->name.' - '.$buyerLocation->address;

        if (!$t) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        return $this->responseData($t);
    }

    public function getAllDispute(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $apiPermission = array(Permissions::DISPUTE_INFO_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        return Dispute::where('transaction_id', $id)->get();
    }

    public function getAllDisputeItem(Request $request, $id, $dispute_id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $apiPermission = array(Permissions::DISPUTE_INFO_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $transaction = Transaction::find($id);
        if(!$transaction) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }


        $rows = DB::select("
        SELECT tc.type section_type_name, tc.value section_name, ii.*, vii.note, vii.repair_cost, di.create_date, di.update_date, di.id, di.inspection_item_id, di.cost_variance FROM dispute_item di
            LEFT JOIN valuation_inspection_item vii ON (vii.inspection_item_id=di.inspection_item_id AND vii.valuation_id=:valuationId)
            LEFT JOIN inspection_item ii ON ii.id=vii.inspection_item_id
            LEFT JOIN type_code tc ON (tc.code=ii.section_type AND tc.type IN ('EXTERIOR_SECTION','INTERIOR_SECTION','MECHANICAL_SECTION'))
            WHERE di.type=:disputeItemType AND di.dispute_id=$dispute_id  AND vii.inspection_item_id IS NOT NULL
        
        ", [
            'valuationId' => $transaction->valuation_id,
            'disputeItemType' => DisputeItem::TYPE_INSPECTION
        ]);

        $original =  new \stdClass();
        foreach($rows as $row) {
            $name = $row->section_type_name;
            if ($name) {
                if (!property_exists($original, $name)) {
                    $original->$name = [];
                }
                array_push($original->$name, $row);
            }
        }
 

        $rows = DB::select("
        SELECT tc.type section_type_name, tc.value section_name, ii.*, di.repair_cost, di.create_date, di.update_date, di.id, di.inspection_item_id, di.cost_variance, di.note FROM dispute_item di
            LEFT JOIN inspection_item ii ON ii.id=di.inspection_item_id
            LEFT JOIN type_code tc ON (tc.code=ii.section_type AND tc.type IN ('EXTERIOR_SECTION','INTERIOR_SECTION','MECHANICAL_SECTION'))
            WHERE di.type=:disputeItemType AND di.dispute_id=$dispute_id  AND di.inspection_item_id IS NOT NULL
        
        ", [
            'disputeItemType' => DisputeItem::TYPE_DISPUTE
        ]);

        $additional = new \stdClass();
        foreach($rows as $row) {
            $name = $row->section_type_name;
            if ($name) {
                if (!property_exists($additional, $name)) {
                    $additional->$name = [];
                }
                array_push($additional->$name, $row);
            }
        }

        $result = [
            'original' => $original,
            'additional' => $additional
        ];

        return $this->responseData($result);
    }

    public function getAllAvailableDisputeItem(Request $request, $id, $dispute_id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $apiPermission = array(Permissions::DISPUTE_INFO_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $transaction = Transaction::find($id);
        if(!$transaction) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }


        $rows = DB::select("
        
        SELECT tc.type section_type_name, ii.*, di.repair_cost, di.create_date, di.update_date, di.id, di.type, di.inspection_item_id FROM dispute_item di
            LEFT JOIN inspection_item ii ON ii.id=di.inspection_item_id
            LEFT JOIN type_code tc ON (tc.code=ii.section_type AND tc.type IN ('EXTERIOR_SECTION','INTERIOR_SECTION','MECHANICAL_SECTION'))
            WHERE di.inspection_item_id IS NOT NULL AND  di.dispute_id=$dispute_id 
        
        ", [
        ]);

        $result = new \stdClass();
        $selected = [];

        foreach($rows as $row) {
            unset($row->id);
            if ($row->type == DisputeItem::TYPE_DISPUTE) {
                $name = $row->section_type_name;
                if ($name) {
                    if (!property_exists($result, $name)) {
                        $result->$name = [];
                    }
                    $row->selected = true;
                    $row->isOriginal = false;
                    unset($row->section_type_name);
                    array_push($result->$name, $row);
                }
            } else if ($row->type == DisputeItem::TYPE_INSPECTION) {
                array_push($selected, $row->inspection_item_id);
            } else {
                \Log::warn("Unhandled dispute type");
            }
        }

        $rows = DB::select("
        SELECT tc.type section_type_name, ii.*, ii.description, vii.* FROM
            valuation_inspection_item vii
            LEFT JOIN inspection_item ii ON ii.id=vii.inspection_item_id
            LEFT JOIN type_code tc ON (tc.code=ii.section_type AND tc.type IN ('EXTERIOR_SECTION','INTERIOR_SECTION','MECHANICAL_SECTION'))
          WHERE vii.valuation_id=:valuationId
        ", [
            'valuationId' => $transaction->valuation_id,
        ]);

        foreach($rows as $row) {
            $name = $row->section_type_name;
            if (!property_exists($result, $name)) {
                $result->$name = [];
            }
            $row->selected = in_array($row->inspection_item_id, $selected);
            $row->isOriginal = true;
            unset($row->id);
            unset($row->section_type_name);
            array_push($result->$name, $row);
        }

        return $this->responseData($result);
    }


    public function createDispute(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'inspectionItems' => 'array',
            'disputeItems' => 'array',
            'comment' => 'required|string',
            'contact_person_first_name' => 'string',
            'contact_person_last_name' => 'string',
            'contact_person_email' => 'string',
            'contact_person_phone' => 'string',
            'general_recondition_cost' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $apiPermission = array(Permissions::DISPUTE_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){
            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $t = Transaction::find($id);
        if (!$t) {
            return $this->responseError(ErrorCodes::NOT_FOUND, "No such transaction $id");
        }

        $json = $request->json();
        $inspectionItems = $json->get('inspectionItems');
        $disputeItems = $json->get('disputeItems');

        $dispute = new Dispute();
        $dispute->transaction_id = $id;
        $dispute->status = Dispute::STATUS_NEW;
        $dispute->creator_id = $user->id;
        $dispute->comment = $json->get('comment');
        $dispute->contact_person_first_name = $json->get('contact_person_first_name');
        $dispute->contact_person_last_name = $json->get('contact_person_last_name');
        $dispute->contact_person_email = $json->get('contact_person_email');
        $dispute->contact_person_phone = $json->get('contact_person_phone');
        $dispute->general_recondition_cost = ($json->get('general_recondition_cost') === TRUE);
        $dispute->save();

        $dispute->rid = IdTransformer::toClientDisputeId($dispute->id);
        $dispute->save();

        //create the default note;
        if ($dispute->comment) {
            $a = new DisputeNote();
            $a->creator_id = $user->id;
            $a->dispute_id = $dispute->id;
            $a->note = $dispute->comment;
            $a->save();
        }

        if ($inspectionItems) {
            foreach($inspectionItems as $inspectionItemId) {
                $a = new DisputeItem();
                $a->dispute_id = $dispute->id;
                $a->inspection_item_id = $inspectionItemId;
                $a->type = DisputeItem::TYPE_INSPECTION;
                $a->save();
            }
        }

        if ($disputeItems) {
            foreach($disputeItems as $disputeItem) {
                $a = new DisputeItem();
                $a->fill($disputeItem);
                $a->dispute_id = $dispute->id;
                $a->type = DisputeItem::TYPE_DISPUTE;
                $a->save();
            }
        }

        $t->status = Transaction::STATUS_IN_DISPUTE;
        $t->save();

        $sellerData = [];
        $event = Event::DISPUTE_RAISED;
        User::notice($t->seller_id, $event, $t->id, $sellerData);

        return $this->responseData($dispute);
    }
    
    public function accept(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;
        
        try {
            $user = $this->currentUser($request, [Permissions::VEHICLE_ACCEPT]);
        } catch (Exception $e) {
            return $this->responseError($e->getMessage());
        }

        $v = Validator::make($request->all(), [
            'inspectionItems' => 'array',
            'disputeItems' => 'array',
            'comment' => 'string'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $t = Transaction::find($id);
        if (!$t) {
            return $this->responseError(ErrorCodes::NOT_FOUND, "No such transaction $id");
        }

        if ($t->status == Transaction::STATUS_DOCUMENTING) {
            return $this->responseError(ErrorCodes::INVALID, 'Transaction had been accepted ');
        }
        $t->status = Transaction::STATUS_DOCUMENTING;
        $t->save();

        $vehicle = $t->getVehicle();
        $n = $vehicle->replicate();

        $n->company_group_id = $user->company_group_id;
        $n->creator_id = $user->id;
        $n->create_date = date("Y/m/d H:i:s");
        $n->update_date = date("Y/m/d H:i:s");
        $n->location_id = $user->location_id;
        $n->trade_in_location = $user->location_id;
        $n->save();

        $vehicle->cloneImage($n->id);

        $valuation = $t->getValuation();
        $v = $valuation->replicate();
        $v->user_id = $user->id;
        $v->vehicle_id = $n->id;
        $v->company_group_id = $user->company_group_id;
        $v->status = Valuation::STATUS_COMPLETE_TRADED_IN;
        $v->estimated_recon_cost = 0;
        $v->requested_approve_price = 0;
        $v->negotiating_price = 0;
        $v->max_price = 0;
        $v->offer_value = 0;
        $v->valuation_value = 0;
        $v->requested_price = 0;
        $v->trade_price = 0;
        $v->offer_price = 0;
        $v->total_recon_cost = 0;
        $v->create_date = date("Y-m-d H:i:s");
        $v->update_date = date("Y-m-d H:i:s");
        $v->save();

        //todo: feature/865 + feature/36: missing send notification to company channel
        // $userWishlist = $valuation->usersHaveMatchWishlist();
        // foreach ($userWishlist as $user) {
            //Log::debug("sending mail to: $user->email\n");
            // MailServiceController::sendWishlistAppear($user, '');
        // }

        ValuationInspectionItem::where('valuation_id', $valuation->id)
            ->get()
            ->map(function($item) use ($v, $valuation) {
                $newItem = $item->replicate();
                $newItem->valuation_id = $v->id;
                $newItem->save();
                ValuationInspectionItem::cloneImages($item->id, $newItem->id);
            });

        $sellerData = [];
        $event = Event::TRANSACTION_VEHICLE_ACCEPT;
        \LOG::debug("start generate document for event: [$event]");

        Document::generatorPDFAndUploadAzure($t->seller_id, $event, $t->id, 'seller');
        Document::generatorPDFAndUploadAzure($t->seller_id, $event, $t->id, 'buyer');
        User::notice($t->seller_id, $event, $t->id, $sellerData);

        return $this->responseData($n);
    }

    public function setStatusDocumenting(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $t = Transaction::find($id);
        if (!$t) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        if ($t->status != Transaction::STATUS_RETURNING) {
            return $this->responseError(ErrorCodes::INVALID, 'Not in status accepted or returning');
        }
        $t->status = Transaction::STATUS_DOCUMENTING;
        $t->save();

        $event = Event::TRANSACTION_WAITING_RETURN;
        $buyerData = [];

        User::notice($t->buyer_id, $event, $t->id, $buyerData);

        return $this->responseData($t);
    }

    public function setTransactionStatus(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $t = Transaction::find($id);
        if (!$t) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        $statusToChange = $request->json()->get('status');
        switch ($statusToChange){
            case Transaction::STATUS_DOCUMENTED:
                if ($t->status != Transaction::STATUS_DOCUMENTING) {
                    return $this->responseError(ErrorCodes::INVALID, 'Not in status documenting');
                }
                $t->status = Transaction::STATUS_DOCUMENTED;
                $t->save();
                break;
            case Transaction::STATUS_COMPLETE:
                if ($t->status != Transaction::STATUS_DOCUMENTED) {
                    return $this->responseError(ErrorCodes::INVALID, 'Not in status documented');
                }
                $t->status = Transaction::STATUS_COMPLETE;
                $t->save();

                $sellerData = [];
                $buyerData  = [];
                $event = Event::TRANSACTION_COMPLETE;

                User::notice($t->seller_id, $event, $t->id, $sellerData);
                User::notice($t->buyer_id, $event, $t->id, $buyerData);
                break;
        }

//        $title = 'TRANSACTION HAS CANCELED';
//        $message = 'The vehicle is returning';
//        $customData = ['transactionId' => $t->id, 'transactionRid' => $t->rid];
//        Helper::notify(Event::TRANSACTION_NOTIFY, $t->seller_id, $title, $message, $customData);

        return $this->responseData($t);
    }

    public function receiveSellerPayment(Request $request, $id){
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $t = Transaction::find($id);
        if (!$t) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        $t->seller_pay_receive = true;
        $t->save();
    }

    /**
     * @param $transaction need create receipt
     * @return url of receipt pdf file on azure
     */
    static function createAndUploadReceiptToAzure($transaction) {
        $receipt = TransactionController::receiptPDF($transaction);
        return Document::uploadToAzure(Document::DOCUMENT_TYPE_PAYMENT_RECEIPT, $receipt, $transaction->id);
    }

    /**
     * @param $transaction need create receipt
     * @return string output of pdf file generated
     */
    private static function receiptPDF($transaction) {
        $html = "<h1>Transaction receipt $transaction->id</h1></br>
                <h3>This is Payment Receipt</h3></br>
                <h4>buyer id $transaction->buyer_id</h4></br>
                <h4>seller id $transaction->seller_id</h4></br>
                <h4>valuation id $transaction->valuation_id</h4></br>
                Please contact amdin for more info";

        return PDFGeneratorService::generate($html, 'A4', 'landscape', false);
    }

    public function documents(Request $request, $id) {
        $user = NULL;
        try {
            $user = $this->currentUser($request, [Permissions::DEALER_SETTINGS_EDIT]);
        } catch (Exception $e) {
            return $this->responseError($e->getMessage());
        }
        $types = $request->json()->get('types');

        $docs = [];
        if (is_array($types)) {
            foreach ($types as $documentType) {
                $doc = Document::findBySourceId($documentType, $id);
                if ($doc) {
                    $docs[] = Document::findBySourceId($documentType, $id);
                }
            }
        }
        return $this->responseData($docs);
    }
}
