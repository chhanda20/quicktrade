<?php

namespace App\Http\Controllers;

use App\AppProperty;
use App\Auction;
use App\Company;
use App\Document;
use App\ExternalOffer;
use App\Libraries\FeatureCode;
use App\Libraries\Helper;
use App\Libraries\IdTransformer;
use App\Libraries\Permissions;
use App\Libraries\ResponseCodes;
use App\Libraries\VehicleService;
use App\Libraries\ErrorCodes;
use App\Libraries\Notification;
use App\Libraries\Event;

use App\Contact;
use App\Location;
use App\OfferBid;
use App\Role;
use App\Transaction;
use App\TypeCode;
use App\User;
use App\Valuation;
use App\Vehicle;
use App\ModelOption;
use App\Option;
use App\ValuationInspectionItem;
use App\InspectionItemImage;
use App\Channel;
use App\Make;


use App\VehicleForTestDrive;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\CountValidator\Exception;

class VehicleForTestDriveController extends Controller
{
    public function create(Request $request) {
        $input = $request->json();

        $vehicle = new VehicleForTestDrive();
        if ($input->has('vin')) {
            $vehicle->vin = $input->get('vin');
        }

        if ($input->has('build_date')) {
            $vehicle->build_date = $input->get('build_date');
        }

        if ($input->has('registration_number')) {
            $vehicle->registration_number = $input->get('registration_number');
        }

        if ($input->has('make')) {
            $vehicle->make = $input->get('make');
        }

        if ($input->has('model_family')) {
            $vehicle->model_family = $input->get('model_family');
        }

        if ($input->has('variant')) {
            $vehicle->variant = $input->get('variant');
        }

        if ($input->has('colour')) {
            $vehicle->colour = $input->get('colour');
        }

        if ($input->has('registration_expiry_date')) {
            $vehicle->registration_expiry_date = $input->get('registration_expiry_date');
        }

        if ($input->has('keys_location')) {
            $vehicle->keys_location = $input->get('keys_location');
        }

        if ($input->has('compliance_date')) {
            $vehicle->compliance_date = $input->get('compliance_date');
        }

        if ($input->has('odometer')) {
            $vehicle->odometer = $input->get('odometer');
        }

        if ($input->has('engine_number')) {
            $vehicle->engine_number = $input->get('engine_number');
        }

        /*if ($input->has('trade_no')) {
            $vehicle->trade_no = $input->get('trade_no');
        }*/

        if ($input->has('stock_number')) {
            $vehicle->stock_number = $input->get('stock_number');
        }

        $vehicle->save();

        return $this->responseData($vehicle);
    }
}