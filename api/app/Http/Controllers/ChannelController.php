<?php

namespace App\Http\Controllers;

use App\Channel;

class ChannelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getMyChannel()
    {
        $channels = Channel::all();
        return $this->responseData($channels);
    }
}