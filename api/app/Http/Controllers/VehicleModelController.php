<?php

namespace App\Http\Controllers;

use App\Libraries\ErrorCodes;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class VehicleModelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function find(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VEHICLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'makeId' => 'required|max10',
            'query' => 'max:50',
            'limit' => 'integer|min:1|max:100',
            'offset' => 'integer|min:0'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $makeId = $request->json()->get('makeId');
        $query = $request->json()->get('query');
        $limit = $request->json()->get('limit');
        $offset = $request->json()->get('offset');
    }
}
