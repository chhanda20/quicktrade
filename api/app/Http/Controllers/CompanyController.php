<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Company;
use App\CompanyGroup;
use App\CompanySetting;
use App\Contact;
use App\Libraries\ErrorCodes;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\Libraries\ResponseMessage;
use App\Location;
use App\User;
use App\UserRole;
use GuzzleHttp\Client;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getCompany(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $apiPermission = array(Permissions::COMPANY_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        // Be careful with: contact.address1 AS address
        $r = DB::select('SELECT c.*, con.company_name name, con.address1 AS address ,
                          con.suburb,con.country,con.state,con.mobile_phone,con.privacy_flag
                          FROM company c LEFT JOIN contact con on c.contact_id=con.id  WHERE c.id=:company_id LIMIT 1', ['company_id' => $id]);

        $company = $r ? $r[0]: null;

        if (!$company) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        return $this->responseData($company);
    }

    public function updateCompany(Request $request, $id)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $apiPermission = array(Permissions::COMPANY_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $v = Validator::make($request->all(), [
            'company_name' => 'min:1|max:256',
            'address1' => 'max:256',
            'status_code' => 'min:1|max:2',
            'company_group_id' => 'required|numeric',
            'first_name' => '',
            'last_name' => '',
            'suburb' => 'required',
            'country' => 'required',
            'state' => 'required',
//            'email' => 'required|email|unique:mysql.user,email'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $json = $request->json();

        $company = Company::find($id);

        if (!$company) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

//        $group = CompanyGroup::find($company->company_group_id);
//        if (!$group) {
//            return $this->responseError(ErrorCodes::INVALID, "No such company group id [$company->company_group_id]");
//        }

        $company->status_code = $json->get('status_code');
        $company->company_group_id = $json->get('company_group_id');
        $company->save();

        $contact = Contact::find($company->contact_id);

        if ($json->has('company_name')) {
            $contact->company_name = $json->get('company_name');
        }
        if ($json->has('address1')) {
            $contact->address1 = $json->get('address1');
        }

        $contact->suburb = $json->get('suburb');
        $contact->country = $json->get('country');
        $contact->state = $json->get('state');
        $contact->save();

        return $this->responseData($company);
    }

    public function createCompany(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::COMPANY_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $v = Validator::make($request->all(), [
            'company_name' => 'required|min:1|max:256',
            'address1' => 'max:256',
            'status_code' => 'required|min:1|max:2',
            'company_group_id' => 'required|numeric',
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile_phone' => 'required',
            'suburb' => 'required',
            'country' => 'required',
            'state' => 'required',
            'email' => 'required|email|unique:mysql.user,email|unique:mysql.contact,email'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $json = $request->json();

        $group = CompanyGroup::find($json->get('company_group_id'));
        if (!$group) {
            return $this->responseError(ErrorCodes::INVALID, "No such company group id [$json->get('company_group_id')]");
        }

        $contact = new Contact();
        if ($json->has('company_name')) {
            $contact->company_name = $json->get('company_name');
        }
        if ($json->has('address1')) {
            $contact->address1 = $json->get('address1');
        }
        if ($json->has('first_name')) {
            $contact->first_name = $json->get('first_name');
        }
        if ($json->has('last_name')) {
            $contact->last_name = $json->get('last_name');
        }
        if ($json->has('email')) {
            $contact->email = $json->get('email');
        }

        $contact->company_group_id = $json->get('company_group_id');

        $contact->mobile_phone = $json->get('mobile_phone');
        $contact->suburb = $json->get('suburb');
        $contact->country = $json->get('country');
        $contact->state = $json->get('state');
        $contact->privacy_flag = $json->get('privacy_flag');


        $contact->status_code = Contact::STATUS_CODE_ACTIVE;

        $contact->save();

        $company = new Company();

        $input = $request->toArray();
        $input = array_filter($input, 'strlen'); // remove null values

        $company->fill($input);
        $company->status_code = $json->get('status_code');
        $company->company_group_id = $json->get('company_group_id');

        $company->contact_id = $contact->id;

        $company->save();


//        $client = new Client(['base_uri' => env('APP_MAIL_URL')]);
//        $r = $client->post('company/welcome', [
//            'json' => [
//                'to_email' => $contact->email,
//                'fullname' => $contact->first_name . " " . $contact->last_name
//            ]
//        ]);
//        \LOG::info((string) $r);

        $client = new Client(['base_uri' => env('APP_MAIL_URL')]);
        $response = $client->post('company/welcome', [
            'json' => [
                'to_email' => $json->get('email'),
                'fullname' => $json->get('last_name'),
            ]
        ]);

        //todo : insert default value to company setting
        CompanySetting::createDefaultSetting($company->id, $user->id);

        $channel = new Channel();
        $channel->name = $company->channelName();
        $channel->type = 'company';
        $channel->save();

        return $this->responseData($company);
    }

    public function createDefaultUser(Request $request, $id)
    {
        $v = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:mysql.user,email'
        ], [
            'required' => ErrorCodes::REQUIRED,
            'email' => ErrorCodes::INVALID,
            'unique' => ErrorCodes::UNIQUE,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'min' => ErrorCodes::LENGTH_MIN_INVALID
        ]);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $company = Company::find($id);
        if (!$company) {
            return $this->responseError(ErrorCodes::INVALID, "No such company group id [$company->company_group_id]");
        }

        $u = User::where('email', $request->json()->get('email'))->first();

        if ($u) {
            return $this->responseError(ErrorCodes::EXISTED);
        }

        $location = Location::where('company_id', $company->id)->first();

        $contact = new Contact();
        $contact->first_name = $request->json()->get('first_name');
        $contact->last_name = $request->json()->get('last_name');
        $contact->status_code = Contact::STATUS_CODE_ACTIVE;
        $contact->email = $request->json()->get('email');
        $contact->save();

        $password = '123456';
        $user = new User();
        $user->user_code = NULL;
        $user->active = 1;
        $user->suspend = 0;
        $user->cause_of_concern = 0;
        $user->location_id = $location->id;
        $user->contact_id = $contact->id;
        $user->email = $request->json()->get('email');
        $user->password = Helper::hash($password);
        $user->token = Helper::generateGuid();
        $user->change_password_token = NULL;
        $user->login_counter = 0;
        $user->create_date = date('Y-m-d H:i:s');
        $user->update_date = date('Y-m-d H:i:s');
        $user->company_group_id = $company->company_group_id;

        $user->save();

        $userRole = new UserRole();
        $userRole->user_id = $user->id;
        $userRole->role_id = 7;
        $userRole->save();

        $company->contact_id = $contact->id;
        $company->save();

        $client = new Client(['base_uri' => env('APP_MAIL_URL')]);
        $response = $client->post('company/createDefaultUser', [
            'json' => [
                'to_email' => $request->json()->get('email'),
                'fullname' => $request->json()->get('last_name'),
            ]
        ]);

        return $this->responseData($user);
    }

    public function find(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $apiPermission = array(Permissions::COMPANY_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $apiPermission = array(Permissions::COMPANY_GET);

        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){
            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $v = Validator::make($request->all(), [
            'status_code' => 'required|array',
            'query' => 'max:100',
            'limit' => 'required|integer|min:1',
            'offset' => 'required|integer|min:0',
            'company_group_id' => 'integer',
            'orderBy' => 'in:first_name,last_name',
            'orderDesc' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $input = $request->json();
        $status_code = $input->get('status_code');
        $limit = $input->get('limit');
        $offset = $input->get('offset');

        $order = "";
        if ($input->has("orderBy")) {
            $order = "ORDER BY " . $input->get("orderBy");
            if ($input->has("orderDesc") && $input->get("orderDesc")) {
                $order .= ' DESC ';
            }
        }

        $query = "";
        if ($input->has("query")) {
            $query_input = $input->get('query');
            $query_input = str_replace('\'', '"', $query_input);
            if ($query_input) {
                $query = " AND (con.first_name LIKE '%$query_input%' OR con.last_name LIKE '%$query_input%' OR con.company_name LIKE '%$query_input%' OR con.email LIKE '%$query_input%')";
            }
        }

        if ($input->has("company_group_id")) {
            $query .= ' AND (cg.id=' . $input->get('company_group_id') .')';

        }


//        $result = DB::select("
//            SELECT c.id, c.name, c.address, c.status_code, con.first_name, con.last_name, cg.id company_group_id, cg.name company_group_name
//            FROM company c
//                LEFT JOIN company_group cg ON c.company_group_id=cg.id
//                LEFT JOIN contact con ON c.contact_id=con.id
//            WHERE
//                c.status_code IN (:status_code)
//                $query
//            $order
//            LIMIT $offset, $limit
//        ", [
//            'status_code' => implode(",",$status_code)
//        ]);

        //nndhuy 21 Mar 2017: it seems like  c.status_code IN (:status_code) is not working properly.
        // Acoording to http://stackoverflow.com/questions/22387487/laravel-4-db-raw-query-with-in-in-the-where-clause-doesnt-work-with-parameter-w
        //I replaced with a String

        $stringList = implode(",", $status_code);

        $result = DB::select("
            SELECT c.id, con.company_name, con.address1, c.status_code, con.email, con.first_name, con.last_name,
             cg.id company_group_id, cg.name company_group_name,
             con.suburb,con.country,con.state,con.mobile_phone,con.privacy_flag
            FROM company c 
                LEFT JOIN company_group cg ON c.company_group_id=cg.id
                LEFT JOIN contact con ON c.contact_id=con.id
            WHERE 
                c.status_code IN (".$stringList.")
                $query
            $order
            LIMIT $offset, $limit
        ");



        return $this->responseData($result);
    }

    public function findCount(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::COMPANY_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'status_code' => 'required|array',
            'company_group_id' => 'integer',
            'query' => 'max:100'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $input = $request->json();
        $status_code = $input->get('status_code');

        $query = "";
        if ($input->has("query")) {
            $query_input = $input->get('query');
            $query_input = str_replace('\'', '"', $query_input);
            if ($query_input) {
                $query = " AND (con.first_name LIKE '%$query_input%' OR con.last_name LIKE '%$query_input%' OR con.company_name LIKE '%$query_input%' OR con.email LIKE '%$query_input%')";
            }
        }

        if ($input->has("company_group_id")) {
            $query .= ' AND (cg.id=' . $input->get('company_group_id') .')';

        }

        $stringList = implode(",", $status_code);
        $result = DB::select("
            SELECT COUNT(*) count
            FROM company c 
                LEFT JOIN company_group cg ON c.company_group_id=cg.id
                LEFT JOIN contact con ON c.contact_id=con.id
            WHERE 
                c.status_code IN (".$stringList.")
                $query
        ");

        return $this->responseData($result[0]);
    }
    public function searchDealerLocation(Request $request){
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::LOCATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'query' => 'max:100',
            'orderBy' => 'in:first_name,last_name',
            'orderDesc' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $input = $request->json();


        $query = "";
        $queryLocation = "";
        if ($input->has("query")) {
            $query_input = $input->get('query');
            if ($query_input) {
                $query = " AND  con.company_name LIKE '%$query_input%'";
                $queryLocation = " AND location.name LIKE '%$query_input%' ";
            }
        }

        $order = "";
        if ($input->has("orderBy")) {
            $order = "ORDER BY con." . $input->get("orderBy");
            if ($input->has("orderDesc") && $input->get("orderDesc")) {
                $order .= ' DESC ';
            }
        }
        $company_status_alive = Company::STATUS_ACTIVE;
        $listDealer = DB::select("
            SELECT
                con.`company_name` name,
                company.id,
                con.first_name first_name,
                con.last_name last_name
    
                FROM
                company
                INNER JOIN company_group ON company.company_group_id = company_group.id
                LEFT JOIN `contact` AS con ON con.id=company.contact_id
                where company.id not IN 
                    (SELECT
                            company.id 
                            FROM
                            company
                            INNER JOIN company_group ON company.company_group_id = company_group.id
                            INNER JOIN `user` as u ON u.company_group_id = company_group.id                            
                            where u.id = $user->id)
                and company.status_code = $company_status_alive  and company_group.id<>1
                GROUP BY company.id
                $order
              
        ");
        $result = array();$i = 0;
        $locationStatus = Location::STATUS_ACTIVE;
        foreach ($listDealer as $dealer){
            $temp = array("dealerName" => $dealer->name,"dealerId" => $dealer->id);
            $locationList = DB::select("
            SELECT
                location.`name` as locationName,
                location.id as locationId
                FROM
                company
                INNER JOIN location ON location.company_id = company.id
                LEFT JOIN `contact` AS con ON con.id=company.contact_id
                WHERE company.id = $dealer->id
                AND location.status_code = $locationStatus
                $queryLocation
                $order
              
            ");
            if($locationList){
                $temp['locationList'] = $locationList;
                array_push($result,$temp);
                unset($temp);
            }


            $i++;

        }



//        $result = array("list_size" => $countAll[0], "dealerList" => $listDealer);
        return $this->responseData($result);
    }

    public function updateMoTermAndCondition(Request $request) {
        $user = NULL;
        try {
            $user = $this->currentUser($request, [Permissions::COMPANY_EDIT]);
        } catch(Exception $e) {
            return $this->responseError($e->getMessage());
        }
        if (!$user->isMo()) {
            return $this->responseError("Only MO admin can update term and condition ");
        }

        $input                      = $request->json();
        $termAndCondition           = $input->get('term_and_condition');
        $companySetting             = CompanySetting::firstOrNew(['key' => CompanySetting::MO_TERM_AND_CONDITION]);
        $companySetting->value      = $termAndCondition ? $termAndCondition : "";
        $companySetting->creator_id = $user->id;
        $companySetting->updater_id = $user->id;

        $companySetting->save();

        User::where('id', '<>', $user->id)->update(['need_accept_term_and_condition' => true]);

        return $this->responseData(['success' => true]);
    }

//    public function updateTermAndCondition(Request $request, $id) {
//        //todo: this feature set term and condition for each company is block
//        return;
//        $user = NULL;
//        try {
//            $user = $this->currentUser($request, [Permissions::COMPANY_EDIT]);
//        } catch(Exception $e) {
//            return $this->responseError($e->getMessage());
//        }
//        if (Location::find($user->location_id)->company_id != $id) {
//            return $this->responseError("Can't update term and condition of other company ");
//        }
//
//        $input                      = $request->json();
//        $termAndCondition           = $input->get('term_and_condition');
//        $companySetting             = CompanySetting::firstOrNew(['company_id' => $id, 'key' => CompanySetting::TERM_AND_CONDITION]);
//        $companySetting->value      = $termAndCondition ? $termAndCondition : "";
//        $companySetting->creator_id = $user->id;
//        $companySetting->updater_id = $user->id;
//
//        $companySetting->save();
//
//        User::where('location_id', $user->location_id)->update(['need_accept_term_and_condition' => true]);
//
//        return $this->responseData(['success' => true]);
//    }

    public function getTermAndCondition(Request $request) {
        $termAndCondition = CompanySetting::firstOrNew(['key' => CompanySetting::MO_TERM_AND_CONDITION])->value;
        return $this->responseData(['term_and_condition' => $termAndCondition]);
    }
}
