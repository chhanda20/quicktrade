<?php

namespace App\Http\Controllers;

use App\Country;
use App\CountryState;
use App\Libraries\ErrorCodes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CountryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getAll(Request $request)
    {
        $token = $request->header('token');
        $validationData = [
            'token' => $token,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }
        $countries = Country::all()->toArray();
        $countryList = [];
        foreach ($countries as $country) {
            $states = CountryState::where('country_id', $country['id'])->get();
            $countryItem = $country;
            $countryItem['stateList'] = $states;
            $countryList[] = $countryItem;
        }
        return $this->responseData($countryList);
    }

    public function getByCode(Request $request)
    {
        $token = $request->header('token');
        $code = $request->json()->get('code');
        $validationData = [
            'token' => $token,
            'code' => $code
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'code' => 'required|array'
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'array' => ErrorCodes::ARRAY_TYPE
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }
        foreach ($code as $countryCode) {
            $validationItem = [
                'countryCode' => $countryCode
            ];
            $vItem = Validator::make($validationItem, [
                'countryCode' => 'string|required'
            ], [
                'required' => ErrorCodes::REQUIRED,
                'string' => ErrorCodes::STRING
            ]);
            if ($vItem->fails()) {
                $errorMessages = $vItem->errors()->getMessages();
                return $this->responseError(ErrorCodes::INVALID, $errorMessages);
            }

            $countries = Country::whereIn('code', $code)->get()->toArray();
            $countryList = [];
            foreach ($countries as $country) {
                $states = CountryState::where('country_id', $country['id'])->get();
                $countryItem = $country;
                $countryItem['stateList'] = $states;
                $countryList[] = $countryItem;
            }
            return $this->responseData($countryList);
        }
    }
}
