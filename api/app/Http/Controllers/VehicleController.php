<?php

namespace App\Http\Controllers;

use App\Auction;
use App\ExternalOffer;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\Libraries\VehicleService;
use App\Libraries\ErrorCodes;
use App\ModelOption;
use App\Option;
use App\TestDrive;
use App\User;
use App\Valuation;
use App\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class VehicleController extends Controller
{
    public function __construct()
    {
    }

    public function getVehicle(Request $request, $vin, $vehicleYear)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        //checking permission
        $apiPermission = array(Permissions::VEHICLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'vin' => $vin,
            'vehicleYear' => $vehicleYear,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'vin' => 'alpha_num',
            'vehicleYear' => 'numeric',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $vehicle = VehicleService::getVehicle($vin, $vehicleYear);
        if ($vehicle) {
            return $this->responseData($vehicle);
        }

        // Look up VIN in Vindicator service

        // Hard code for testing
//        $vinVehicle = [
//            'vin'                 => $vin,
//            'colour'              => 2,
//            'registration_number' => 'abc',
//        ];

        // Insert into vehicle table
//        $vehicleObj = new Vehicle();
//        $vehicleObj->vin = $vinVehicle['vin'];
//        $vehicleObj->colour = $vinVehicle['colour'];
//        $vehicleObj->registration_number = $vinVehicle['registration_number'];
//        $vehicleObj->save();
//        return $this->responseData($vehicleObj);
        return $this->responseError(ErrorCodes::NOT_FOUND);
    }

    public function getVehicleDiagram(Request $request, $bodyType) {
        $token = $request->header('token');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        //checking permission
        $apiPermission = array(Permissions::VEHICLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'bodyType' => $bodyType,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'bodyType' => 'required',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $diagram = new \stdClass();
        $diagram->image = 'http://moto-backend-dev.mmsoft.vn/example/vehicle_diagram@3x.png';
        $diagram->width = 359;
        $diagram->height = 728;

        $frame1 = new \stdClass();
        $frame1->x = 0;
        $frame1->y = 0;
        $frame1->width = 359;
        $frame1->height = 104;
        $frame1->section_id = 1;
        $diagram->section_frames[] = $frame1;

        $frame2 = new \stdClass();
        $frame2->x = 0;
        $frame2->y = 103;
        $frame2->width = 180;
        $frame2->height = 144;
        $frame2->section_id = 11;
        $diagram->section_frames[] = $frame2;

        $frame3 = new \stdClass();
        $frame3->x = 179;
        $frame3->y = 103;
        $frame3->width = 180;
        $frame3->height = 144;
        $frame3->section_id = 2;
        $diagram->section_frames[] = $frame3;

        $frame4 = new \stdClass();
        $frame4->x = 0;
        $frame4->y = 248;
        $frame4->width = 135;
        $frame4->height = 141; 
        $frame4->section_id = 9;
        $diagram->section_frames[] = $frame4;

        $frame5 = new \stdClass();
        $frame5->x = 134;
        $frame5->y = 248;
        $frame5->width = 91;
        $frame5->height = 268;
        $frame5->section_id = 10;
        $diagram->section_frames[] = $frame5;

        $frame6 = new \stdClass();
        $frame6->x = 224;
        $frame6->y = 247;
        $frame6->width = 135;
        $frame6->height = 141;
        $frame6->section_id = 3;
        $diagram->section_frames[] = $frame6;

        $frame7 = new \stdClass();
        $frame7->x = 0;
        $frame7->y = 386;
        $frame7->width = 135;
        $frame7->height = 128;
        $frame7->section_id = 8;
        $diagram->section_frames[] = $frame7;

        $frame8 = new \stdClass();
        $frame8->x = 224;
        $frame8->y = 386;
        $frame8->width = 135;
        $frame8->height = 128;
        $frame8->section_id = 4;
        $diagram->section_frames[] = $frame8;

        $frame9 = new \stdClass();
        $frame9->x = 0;
        $frame9->y = 513;
        $frame9->width = 180;
        $frame9->height = 144;
        $frame9->section_id = 7;
        $diagram->section_frames[] = $frame9;

        $frame10 = new \stdClass();
        $frame10->x = 179;
        $frame10->y = 513;
        $frame10->width = 180;
        $frame10->height = 144;
        $frame10->section_id = 5;
        $diagram->section_frames[] = $frame10;

        $frame11 = new \stdClass();
        $frame11->x = 0;
        $frame11->y = 656;
        $frame11->width = 359;
        $frame11->height = 72;
        $frame11->section_id = 6;
        $diagram->section_frames[] = $frame11;

        return $this->responseData($diagram);
    }

    
    public function stockFind(Request $request) {
    	$token = $request->header("token");
    	$user = NULL;
    	
    	if ($token) {
    		$user = User::where('token', $token)->first();
    	}
    	
    	if (!$user) {
    		return $this->responseError(ErrorCodes::INVALID_TOKEN);
    	}
    	
    	$v = Validator::make($request->all(), [
    			'offset' => 'required|integer|min:0',
    			'limit' => 'required|integer',
    			'query' => '',
    			'orderBy' => 'in:trade_price,registration_number,day_in_stock,location_name,make,model_family,build_date,stock_number',
    			'orderDesc' => 'boolean',
    			'makeInfo' => 'array',
    			'odometer' => 'array',
    			'year' => 'array',
    			'location' => 'array',
                'transmissionType' => 'array',
                'trade_price' => 'array'

    	], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);
    	
    	if ($v->fails()) {
    		return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
    	}
        $apiPermission = array(Permissions::VEHICLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $json = $request->json();
        $offset = $json->get('offset');
        $limit = $json->get('limit');
        $location = [];
        $where = "";

        $selectedLocations = $json->get('location');

        if ($selectedLocations && sizeof($selectedLocations) != 0) {
            $a = $user->getAccessableLocationIds();
            foreach ($selectedLocations as $s) {
                if (in_array($s, $a)) {
                    array_push($location, $s);
                }
            }
        } else {
            $location = $user->getAccessableLocationIds();
        }

        if (sizeof($location) == 0) {
            return $this->responseError(ErrorCodes::INVALID, "User can't access any locations");
        }

        $location = implode(',', $location);

        if ($json->has("odometer")) {
            $odometer = $json->get('odometer');
            if (sizeof($odometer) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'odometer must be an array contains 2 number');
            }
            $where .= " AND (odometer between $odometer[0] AND $odometer[1])";
        }

        if ($json->has("year")) {
            $year = $json->get('year');
            if (sizeof($year) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'year must be an array contains 2 number');
            }
            $where .= " AND (year(v.build_date) between $year[0] AND $year[1])";
        }

        $transmission = collect();

        if ($json->has("transmissionType")) {
            foreach ($json->get('transmissionType') as $type) {
                $x =  str_replace('\'', '"', $type);
                $x =  trim($x);
                if ($x) {
                    $transmission->push("'" . $x . "'");
                }
            }

            $transmission = $transmission->unique()->values()->all();

            $transmission = implode(",", $transmission);
            if($transmission) {
                $where .= " AND (v.transmission IN ($transmission))";
            }
        }

        $make = collect();
        $model = collect();
        $variant = collect();

        if ($json->has("makeInfo")) {
            $arr = $json->get('makeInfo');
            foreach ($arr as $i) {
                $i = (object) $i;

                $i->make = str_replace('\'', '"', $i->make);
                $i->make = trim($i->make);

                $i->model = str_replace('\'', '"', $i->model);
                $i->model = trim($i->model);

                $i->variant = str_replace('\'', '"', $i->variant);
                $i->variant = trim($i->variant);

                if ($i->make) {
                    $make->push("'" . $i->make. "'" );
                }
                if ($i->model) {
                    $model->push("'" . $i->model. "'" );
                }
                if ($i->variant) {
                    $variant->push("'" . $i->variant. "'" );
                }
            }
        }

        $make = $make->unique()->values()->all();
        $model = $model->unique()->values()->all();
        $variant = $variant->unique()->values()->all();

        if (sizeof($make) > 0) {
            $make = implode(",", $make);
            $where .= " AND (make IN ($make))";
        }
        if (sizeof($model) > 0) {
            $model = implode(",", $model);
            $where .= " AND (model_family IN ($model))";
        }
        if (sizeof($variant) > 0) {
            $variant = implode(",", $variant);
            $where .= " AND (variant IN ($variant))";
        }

        if ($json->has("query")) {
            $text = $json->get('query');
            $text = str_replace('\'', '"', $text); // for safety reasons;
            if ($text) {
                $query = "(YEAR(build_date) LIKE '$text%') OR (CAST(va.stock_number as CHAR) LIKE '$text%')";
                $query .= "OR MATCH(make, family, series, style, engine_type, engine_size, variant, cylinder, transmission, registration_number, body_type) AGAINST ('$text*' IN BOOLEAN MODE)";
                $where .= " AND ($query) ";
            }
        }
        if ($json->has("trade_price")) {
            $trade_price = $json->get('trade_price');
            if (sizeof($trade_price) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'trade_price must be an array contains 2 number');
            }
            $where .= " AND (trade_price between $trade_price[0] AND $trade_price[1])";
        }

        $order = "";
        if ($json->has("orderBy")) {
            $order = " ORDER BY " . $json->get('orderBy');
            if ($json->get("orderDesc")) {
                $order .= " DESC ";
            }
        }
        //implement search by vehicle id
        $statusAuctionReject = Auction::STATUS_CODE_REJECT;
        if($json->has("vehicle_id")){
            $vehicle_id = $json->get('vehicle_id');
            $where .= " AND v.id = $vehicle_id ";
        }else{
            $where .= "AND (sesItem.status <> $statusAuctionReject OR sesItem.status IS NULL)";
        }
        /**
         * Vehicle in stock are ones in valuation.vehicle_id, valuation.status=TRADED_IN, valuation has lastest traded in date
         */

        $q = "
            SELECT v.*, va.trade_price, va.id valuation_id, va.proposed_arrival_date, va.stock_number, datediff(:backend_date, v.trade_in) day_in_stock, lo.name location_name,va.ownership_type, va.special_disclosures 
              FROM vehicle v 
              RIGHT JOIN (select vehicle_id, max(proposed_arrival_date) arrival_date from valuation where proposed_arrival_date IS NOT NULL AND status IN (:valuation_status, :status_listed_for_auction) AND  company_group_id=:company_group_id group by vehicle_id) x
                ON v.id = x.vehicle_id
              LEFT JOIN (SELECT * FROM valuation WHERE status IN (:valuation_status_2, :status_listed_for_auction_2)) va ON (va.proposed_arrival_date = x.arrival_date AND va.vehicle_id = x.vehicle_id)
              LEFT JOIN location lo ON lo.id = v.trade_in_location
              LEFT  JOIN  auction_item AS auc ON auc.valuation_id = va.id
              LEFT JOIN session_has_item sesItem ON sesItem.auction_item_id =  auc.id
            WHERE v.trade_in_location IN ($location) 
                  AND v.id NOT IN (SELECT vehicle_id FROM `test_drive` WHERE status=:activeTestDrive)
                  AND va.id NOT IN (select valuation_id from (select valuation_id, `status` from external_offer eo ORDER BY id DESC limit 1 ) a WHERE `status` <>:external_offer_passed)
                  
             $where $order LIMIT  $offset, $limit ";

        \LOG::debug($q);

        $result = DB::select($q,
            [
                'valuation_status' => Valuation::STATUS_COMPLETE_TRADED_IN,
                'valuation_status_2' => Valuation::STATUS_COMPLETE_TRADED_IN,
                'status_listed_for_auction' => Valuation::STATUS_LISTED_FOR_AUCTION,
                'status_listed_for_auction_2' => Valuation::STATUS_LISTED_FOR_AUCTION,
                'company_group_id' => $user->company_group_id,
                'backend_date' => date('Y-m-d H:i:s'),
                'activeTestDrive' => TestDrive::STATUS_CODE_IN_PROGRESS,
                'external_offer_passed' => ExternalOffer::STATUS_PASSED
            ]);

        foreach($result as $r) {
            $details = [];
            if ($r->make) {
                $details[] = trim($r->make);
            }
            if ($r->family) {
                $details[] = trim($r->family);
            }
            if ($r->series) {
                $details[] = trim($r->series);
            }
            if ($r->style) {
                $details[] = trim($r->style);
            }
            if ($r->engine_type) {
                $details[] = trim($r->engine_type);
            }
            if ($r->engine_capacity) {
                $details[] = trim($r->engine_capacity);
            }
            if ($r->engine_size) {
                $details[] = trim($r->engine_size);
            }
            if ($r->variant) {
                $details[] = trim($r->variant);
            }
            if ($r->cylinder) {
                $details[] = trim($r->cylinder);
            }
            if ($r->transmission) {
                $details[] = trim($r->transmission);
            }
            $r->name = implode(' ', $details);
            $r->ownership_type = json_decode($r->ownership_type, true);
        }

        return $this->responseData($result);
    }
    
    
    public function stockCount(Request $request) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'query' => '',
            'makeInfo' => 'array',
            'odometer' => 'array',
            'year' => 'array',
            'location' => 'array',
            'transmissionType' => '',
            'trade_price' => 'array'

        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $apiPermission = array(Permissions::VEHICLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $json = $request->json();
        $location = null;
        $where = "";

        $selectedLocations = $json->get('location');

        if ($selectedLocations && sizeof($selectedLocations) != 0) {
            $a = $user->getAccessableLocationIds();
            foreach ($selectedLocations as $s) {
                if (in_array($s, $a)) {
                    array_push($location, $s);
                }
            }
        } else {
            $location = $user->getAccessableLocationIds();
        }

        if (sizeof($location) == 0) {
            return $this->responseError(ErrorCodes::INVALID, "User can't access any locations");
        }

        $location = implode(',', $location);

        if ($json->has("odometer")) {
            $odometer = $json->get('odometer');
            if (sizeof($odometer) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'odometer must be an array contains 2 number');
            }
            $where .= " AND (odometer between $odometer[0] AND $odometer[1])";
        }

        if ($json->has("year")) {
            $year = $json->get('year');
            if (sizeof($year) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'year must be an array contains 2 number');
            }
            $where .= " AND (year(v.build_date) between $year[0] AND $year[1])";
        }

        $transmission = collect();

        if ($json->has("transmissionType")) {
            foreach ($json->get('transmissionType') as $type) {
                $x =  str_replace('\'', '"', $type);
                $x =  trim($x);
                if ($x) {
                    $transmission->push("'" . $x . "'");
                }
            }

            $transmission = $transmission->unique()->values()->all();

            $transmission = implode(",", $transmission);
            if($transmission) {
                $where .= " AND (v.transmission IN ($transmission))";
            }
        }

        $make = collect();
        $model = collect();
        $variant = collect();

        if ($json->has("makeInfo")) {
            $arr = $json->get('makeInfo');
            foreach ($arr as $i) {
                $i = (object) $i;

                $i->make = str_replace('\'', '"', $i->make);
                $i->make = trim($i->make);

                $i->model = str_replace('\'', '"', $i->model);
                $i->model = trim($i->model);

                $i->variant = str_replace('\'', '"', $i->variant);
                $i->variant = trim($i->variant);

                if ($i->make) {
                    $make->push("'" . $i->make. "'" );
                }
                if ($i->model) {
                    $model->push("'" . $i->model. "'" );
                }
                if ($i->variant) {
                    $variant->push("'" . $i->variant. "'" );
                }
            }
        }

        $make = $make->unique()->values()->all();
        $model = $model->unique()->values()->all();
        $variant = $variant->unique()->values()->all();

        if (sizeof($make) > 0) {
            $make = implode(",", $make);
            $where .= " AND (make IN ($make))";
        }
        if (sizeof($model) > 0) {
            $model = implode(",", $model);
            $where .= " AND (model_family IN ($model))";
        }
        if (sizeof($variant) > 0) {
            $variant = implode(",", $variant);
            $where .= " AND (variant IN ($variant))";
        }

        if ($json->has("query")) {
            $text = $json->get('query');
            $text = str_replace('\'', '"', $text); // for safety reasons;
            if ($text) {
                $query = "(YEAR(build_date) LIKE '$text%') OR (CAST(va.stock_number as CHAR) LIKE '$text%')";
                $query .= "OR MATCH(make, family, series, style, engine_type, engine_size, variant, cylinder, transmission, registration_number, body_type) AGAINST ('$text*' IN BOOLEAN MODE)";
                $where .= " AND ($query) ";
            }
        }
        if ($json->has("trade_price")) {
            $trade_price = $json->get('trade_price');
            if (sizeof($trade_price) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'trade_price must be an array contains 2 number');
            }
            $where .= " AND (trade_price between $trade_price[0] AND $trade_price[1])";
        }

        $statusAuctionReject = Auction::STATUS_CODE_REJECT;
        $q = "
            SELECT count(*) count FROM vehicle v 
              RIGHT JOIN (select vehicle_id, max(proposed_arrival_date) arrival_date from valuation where proposed_arrival_date IS NOT NULL AND status=:valuation_status AND  company_group_id=:company_group_id group by vehicle_id) x
                ON v.id = x.vehicle_id
              LEFT JOIN (SELECT * FROM valuation WHERE status=:valuation_status2) va ON (va.proposed_arrival_date = x.arrival_date AND va.vehicle_id = x.vehicle_id)
              LEFT  JOIN  auction_item AS auc ON auc.valuation_id = va.id
              LEFT JOIN session_has_item sesItem ON sesItem.auction_item_id =  auc.id
            WHERE v.trade_in_location IN ($location) 
            AND v.id NOT IN (SELECT vehicle_id FROM `test_drive` WHERE status=:activeTestDrive)
            AND va.id NOT IN (select valuation_id from (select valuation_id, `status` from external_offer eo ORDER BY id DESC limit 1 ) a WHERE `status` <>:external_offer_passed)
            AND (sesItem.status <> $statusAuctionReject OR sesItem.status IS NULL)
            $where ";


        $r = DB::select($q,
            [
                'valuation_status' => Valuation::STATUS_COMPLETE_TRADED_IN,
                'valuation_status2' => Valuation::STATUS_COMPLETE_TRADED_IN,
                'company_group_id' => $user->company_group_id,
                'activeTestDrive' => TestDrive::STATUS_CODE_IN_PROGRESS,
                'external_offer_passed' => ExternalOffer::STATUS_PASSED
            ]);
        return $this->responseData($r[0]);
    }

    public function getOne(Request $request, $id) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VEHICLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $vehicle = DB::Select('SELECT *,datediff(:backend_date, trade_in) day_in_stock FROM vehicle WHERE id=:id LIMIT 1', ['id' => $id, 'backend_date' => date('Y-m-d H:i:s')]);
        if (sizeof($vehicle) > 0 ) {
            $vehicle = $vehicle[0];
        } else {
            $vehicle = null;
        }

        if (!$vehicle) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        $glassCode = $vehicle->vehicle_model_code;
        $modelOptionModel = new ModelOption();
        $optionGroup = $modelOptionModel->getOptionByCode($glassCode);
        $optionList = [];
        if ($optionGroup && $optionGroup->option_group) {
            $options = explode(',', $optionGroup->option_group);
            foreach ($options as $option) {
                $optionArr[] = trim($option);
            }
            $optionStr = "'" . implode("','", $optionArr) . "'";
            $optionModel = new Option();
            $optionList = $optionModel->getOption($optionStr);
        }
        $vehicle->option_list = $optionList;

        $details = [];
        if ($vehicle->make) {
            $details[] = trim($vehicle->make);
        }
        if ($vehicle->family) {
            $details[] = trim($vehicle->family);
        }
        if ($vehicle->series) {
            $details[] = trim($vehicle->series);
        }
        if ($vehicle->style) {
            $details[] = trim($vehicle->style);
        }
        if ($vehicle->engine_type) {
            $details[] = trim($vehicle->engine_type);
        }
        if ($vehicle->engine_capacity) {
            $details[] = trim($vehicle->engine_capacity);
        }
        if ($vehicle->engine_size) {
            $details[] = trim($vehicle->engine_size);
        }
        if ($vehicle->variant) {
            $details[] = trim($vehicle->variant);
        }
        if ($vehicle->cylinder) {
            $details[] = trim($vehicle->cylinder);
        }
        if ($vehicle->transmission) {
            $details[] = trim($vehicle->transmission);
        }
        $vehicle->name = implode(' ', $details);

        return $this->responseData($vehicle);
    }

    public function testDrivesFind(Request $request) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        //check permission to get list vehicle
        $apiPermission = array(Permissions::VEHICLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $v = Validator::make($request->all(), [
            'offset' => 'required|integer|min:0',
            'limit' => 'required|integer',
            'query' => '',
            'orderBy' => 'in:trade_price,registration_number,day_in_stock,location_name,make,model_family,build_date,stock_number',
            'orderDesc' => 'boolean',
            'make' => '',
            'variant' => '',
            'odometer' => 'array',
            'year' => 'array',
            'location' => 'array',
            'transmissionType' => ''

        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $json = $request->json();
        $offset = $json->get('offset');
        $limit = $json->get('limit');
        $location = null;
        $where = "";

        if ($json->has('location')) {
            if (sizeof($json->get('location')) == 0) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'location must contain at least 1 number');
            }
            $location = implode(",", $json->get('location'));
        } else {
            if (!$user->location_id) {
                \LOG::warn("User[$user->id] doesn't have location_id");
            }
            $location = $user->location_id;
        }

        if ($json->has("odometer")) {
            $odometer = $json->get('odometer');
            if (sizeof($odometer) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'odometer must be an array contains 2 number');
            }
            $where .= " AND (odometer between $odometer[0] AND $odometer[1])";
        }

        if ($json->has("year")) {
            $year = $json->get('year');
            if (sizeof($year) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'year must be an array contains 2 number');
            }
            $where .= " AND (year(v.build_date) between $year[0] AND $year[1])";
        }


        $transmission = collect();

        if ($json->has("transmissionType")) {
            foreach ($json->get('transmissionType') as $type) {
                $x =  str_replace('\'', '"', $type);
                $x =  trim($x);
                if ($x) {
                    $transmission->push("'" . $x . "'");
                }
            }

            $transmission = $transmission->unique()->values()->all();

            $transmission = implode(",", $transmission);
            if($transmission) {
                $where .= " AND (v.transmission IN ($transmission))";
            }
        }

        $make = collect();
        $model = collect();
        $variant = collect();

        if ($json->has("makeInfo")) {
            $arr = $json->get('makeInfo');
            foreach ($arr as $i) {
                $i = (object) $i;

                $i->make = str_replace('\'', '"', $i->make);
                $i->make = trim($i->make);

                $i->model = str_replace('\'', '"', $i->model);
                $i->model = trim($i->model);

                $i->variant = str_replace('\'', '"', $i->variant);
                $i->variant = trim($i->variant);

                if ($i->make) {
                    $make->push("'" . $i->make. "'" );
                }
                if ($i->model) {
                    $model->push("'" . $i->model. "'" );
                }
                if ($i->variant) {
                    $variant->push("'" . $i->variant. "'" );
                }
            }
        }

        $make = $make->unique()->values()->all();
        $model = $model->unique()->values()->all();
        $variant = $variant->unique()->values()->all();

        if (sizeof($make) > 0) {
            $make = implode(",", $make);
            $where .= " AND (make IN ($make))";
        }
        if (sizeof($model) > 0) {
            $model = implode(",", $model);
            $where .= " AND (model_family IN ($model))";
        }
        if (sizeof($variant) > 0) {
            $variant = implode(",", $variant);
            $where .= " AND (variant IN ($variant))";
        }


        if ($json->has("query")) {
            $text = $json->get('query');
            $text = str_replace('\'', '"', $text); // for safety reasons;
            if ($text) {
                $query = "(YEAR(build_date) LIKE '$text*')";
                $query .= "OR MATCH(make, family, series, style, engine_type, engine_size, variant, cylinder, transmission, registration_number, body_type) AGAINST ('$text*' IN BOOLEAN MODE)";
                $where .= " AND ($query) ";
            }
        }

        $order = "";
        if ($json->has("orderBy")) {
            $order = " ORDER BY " . $json->get('orderBy');
            if ($json->get("orderDesc")) {
                $order .= " DESC ";
            }
        }

        /**
         * Vehicle in stock are ones in valuation.vehicle_id, valuation.status=TRADED_IN, valuation has lastest traded in date
         */
        $q = "
            SELECT v.*, va.id valuation_id, va.proposed_arrival_date, va.stock_number, datediff(:backend_date, v.trade_in) day_in_stock, lo.name location_name FROM vehicle v 
              RIGHT JOIN (select vehicle_id, max(proposed_arrival_date) arrival_date from valuation where proposed_arrival_date IS NOT NULL AND status=:valuation_status AND  company_group_id=:company_group_id group by vehicle_id) x
                ON v.id = x.vehicle_id
              LEFT JOIN (SELECT * FROM valuation WHERE status=:valuation_status_2) va ON (va.proposed_arrival_date = x.arrival_date AND va.vehicle_id = x.vehicle_id)
              LEFT JOIN location lo ON lo.id = v.location_id
            WHERE v.location_id IN ($location) AND v.id NOT IN (SELECT vehicle_id FROM `test_drive` WHERE status=:activeTestDrive) $where $order LIMIT  $offset, $limit ";


        $result = DB::select($q,
            [
                'valuation_status' => Valuation::STATUS_COMPLETE_TRADED_IN,
                'valuation_status_2' => Valuation::STATUS_COMPLETE_TRADED_IN,
                'company_group_id' => $user->company_group_id,
                'backend_date' => date('Y-m-d H:i:s'),
                'activeTestDrive' => TestDrive::STATUS_CODE_IN_PROGRESS
            ]);

        foreach($result as $r) {
            $details = [];
            if ($r->make) {
                $details[] = trim($r->make);
            }
            if ($r->family) {
                $details[] = trim($r->family);
            }
            if ($r->series) {
                $details[] = trim($r->series);
            }
            if ($r->style) {
                $details[] = trim($r->style);
            }
            if ($r->engine_type) {
                $details[] = trim($r->engine_type);
            }
            if ($r->engine_capacity) {
                $details[] = trim($r->engine_capacity);
            }
            if ($r->engine_size) {
                $details[] = trim($r->engine_size);
            }
            if ($r->variant) {
                $details[] = trim($r->variant);
            }
            if ($r->cylinder) {
                $details[] = trim($r->cylinder);
            }
            if ($r->transmission) {
                $details[] = trim($r->transmission);
            }
            $r->name = implode(' ', $details);
        }

        return $this->responseData($result);
    }


    public function testDrivesCount(Request $request) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        //check permission to get list vehicle
        $apiPermission = array(Permissions::VEHICLE_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $v = Validator::make($request->all(), [
            'query' => '',
            'odometer' => 'array',
            'year' => 'array',
            'location' => 'array',
            'transmissionType' => '',
            'trade_price' => 'array'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $json = $request->json();
        $location = null;
        $where = "";

        if ($json->has('location')) {
            if (sizeof($json->get('location')) == 0) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'location must contain at least 1 number');
            }
            $location = implode(",", $json->get('location'));
        } else {
            if (!$user->location_id) {
                \LOG::warn("User[$user->id] doesn't have location_id");
            }
            $location = $user->location_id;
        }


        if ($json->has("odometer")) {
            $odometer = $json->get('odometer');
            if (sizeof($odometer) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'odometer must be an array contains 2 number');
            }
            $where .= " AND (odometer between $odometer[0] AND $odometer[1])";
        }

        if ($json->has("year")) {
            $year = $json->get('year');
            if (sizeof($year) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'year must be an array contains 2 number');
            }
            $where .= " AND (year(v.build_date) between $year[0] AND $year[1])";
        }

        $transmission = collect();

        if ($json->has("transmissionType")) {
            foreach ($json->get('transmissionType') as $type) {
                $x =  str_replace('\'', '"', $type);
                $x =  trim($x);
                if ($x) {
                    $transmission->push("'" . $x . "'");
                }
            }

            $transmission = $transmission->unique()->values()->all();

            $transmission = implode(",", $transmission);
            if($transmission) {
                $where .= " AND (v.transmission IN ($transmission))";
            }
        }

        $make = collect();
        $model = collect();
        $variant = collect();

        if ($json->has("makeInfo")) {
            $arr = $json->get('makeInfo');
            foreach ($arr as $i) {
                $i = (object) $i;

                $i->make = str_replace('\'', '"', $i->make);
                $i->make = trim($i->make);

                $i->model = str_replace('\'', '"', $i->model);
                $i->model = trim($i->model);

                $i->variant = str_replace('\'', '"', $i->variant);
                $i->variant = trim($i->variant);

                if ($i->make) {
                    $make->push("'" . $i->make. "'" );
                }
                if ($i->model) {
                    $model->push("'" . $i->model. "'" );
                }
                if ($i->variant) {
                    $variant->push("'" . $i->variant. "'" );
                }
            }
        }

        $make = $make->unique()->values()->all();
        $model = $model->unique()->values()->all();
        $variant = $variant->unique()->values()->all();

        if (sizeof($make) > 0) {
            $make = implode(",", $make);
            $where .= " AND (make IN ($make))";
        }
        if (sizeof($model) > 0) {
            $model = implode(",", $model);
            $where .= " AND (model_family IN ($model))";
        }
        if (sizeof($variant) > 0) {
            $variant = implode(",", $variant);
            $where .= " AND (variant IN ($variant))";
        }

        if ($json->has("query")) {
            $text = $json->get('query');
            $text = str_replace('\'', '"', $text); // for safety reasons;
            if ($text) {
                $query = "(YEAR(build_date) LIKE '$text*')";
                $query .= "OR MATCH(make, family, series, style, engine_type, engine_size, variant, cylinder, transmission, registration_number, body_type) AGAINST ('$text*' IN BOOLEAN MODE)";
                $where .= " AND ($query) ";
            }
        }
        if ($json->has("trade_price")) {
            $trade_price = $json->get('trade_price');
            if (sizeof($trade_price) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'trade_price must be an array contains 2 number');
            }
            $where .= " AND (trade_price between $trade_price[0] AND $trade_price[1])";
        }

        $q = "
            SELECT count(*) count FROM vehicle v 
              RIGHT JOIN (select vehicle_id, max(proposed_arrival_date) arrival_date from valuation where proposed_arrival_date IS NOT NULL AND status=:valuation_status AND  company_group_id=:company_group_id group by vehicle_id) x
                ON v.id = x.vehicle_id
              LEFT JOIN (SELECT * FROM valuation WHERE status=:valuation_status2) va ON (va.proposed_arrival_date = x.arrival_date AND va.vehicle_id = x.vehicle_id)
            WHERE v.location_id IN ($location) AND v.id NOT IN (SELECT vehicle_id FROM `test_drive` WHERE status=:activeTestDrive) $where";


        $r = DB::select($q,
            [
                'valuation_status' => Valuation::STATUS_COMPLETE_TRADED_IN,
                'valuation_status2' => Valuation::STATUS_COMPLETE_TRADED_IN,
                'company_group_id' => $user->company_group_id,
                'activeTestDrive' => TestDrive::STATUS_CODE_IN_PROGRESS
            ]);
        return $this->responseData($r[0]);
    }
}
