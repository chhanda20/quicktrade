<?php

namespace App\Http\Controllers;


use App\AppProperty;
use App\Auction;
use App\AuctionSession;
use App\Libraries\AuctionBidHelper;
use App\Libraries\ErrorCodes;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\Libraries\ResponseMessage;
use App\Location;
use App\SessionHasItem;
use App\TestDrive;
use App\User;
use DateTime;
use http\Exception;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AuctionSessionController extends Controller
{
    public function __construct()
    {
    }

    public function createAuctionSession(Request $request)
    {
        $token                      = $request->header("token");
        $name                       = $request->json()->get('name');
        $sessionType                = $request->json()->get('sessionType');
        $startDate                  = $request->json()->get('startDate');
        $endDate                    = $request->json()->get('endDate');
        $time                       = $request->json()->get('time');
        $duration                   = $request->json()->get('duration');
        $recurringDate              = $request->json()->get('recurringDate');
        $customBidPriceIncrement    = $request->json()->get('bid_price_increment');

        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        //check permission to create auction session
        $apiPermission = array(Permissions::AUCTION_SESSION_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        try {
            $validationData = [
                'token' => $token,
                'sessionType' => $sessionType,
                'time' => $time,
                'duration' => $duration,
                'startDate' => $startDate,

            ];
            $v = Validator::make($validationData, [
                'token' => 'required|alpha_num|exists:mysql.user,token',
                'sessionType' => 'required',
                'time' => 'required',
                'duration' => 'required|numeric',
                'startDate' => 'required',

            ], [
                'required' => ErrorCodes::REQUIRED,
                'alpha_num' => ErrorCodes::ALPHA_NUM,
                'exists' => ErrorCodes::NOT_EXISTS,
                'numeric' => ErrorCodes::NUMERIC,
            ]);
            if ($v->fails()) {
                $errorMessages = $v->errors()->getMessages();
                return $this->responseError(ErrorCodes::INVALID, $errorMessages);
            }
            $responseMessage = new ResponseMessage();

            // handle base on session type

            $auctionSessionList = array();
            if ($sessionType == 'recurring') {
                // if recurring, calculate list of session with start time and end time
                if(!$endDate || !$recurringDate || sizeof($recurringDate)<1){
                    return $this->responseError(ErrorCodes::INVALID);
                }
                $endDate = strtotime($endDate);
                if(strtotime($startDate)> $endDate){
                    return $this->responseError(ErrorCodes::INVALID);
                }
                $days = [
                    1 => 'Sunday',
                    2 => 'Monday',
                    3 => 'Tuesday',
                    4 => 'Wednesday',
                    5 => 'Thursday',
                    6 => 'Friday',
                    7 => 'Saturday'
                ];
                foreach ($recurringDate as $dayOfWeek) {
                    for ($i = strtotime($days[$dayOfWeek], strtotime($startDate)); $i <= $endDate; $i = strtotime('+1 week', $i)) {
                        $newAuctionSession = new AuctionSession();
                        $nextStartDate = date("Y/m/d",$i);
                        $newAuctionSession = $this->convertAndMapDataToSession($name, $nextStartDate, $time, $duration, $newAuctionSession);
                        array_push($auctionSessionList, $newAuctionSession);
                    }

                }
            } else {
                //if ad-hoc, calculate start time and end time
                $newAuctionSession = new AuctionSession();
                $newAuctionSession = $this->convertAndMapDataToSession($name, $startDate, $time, $duration, $newAuctionSession);
                array_push($auctionSessionList, $newAuctionSession);
            }

            // validate if prepared insert session is not overlap with db session
            //loop all pre-insert session,compare start date with db. If start time is between

            foreach ($auctionSessionList as $item) {
                $checkResult = AuctionSession::validateDuplicateSessionTime($item->startAt, $item->endAt);
                if($checkResult){
                    return $this->responseError(ErrorCodes::AUCTION_SESSION_DUPLICATE);
                }
            }
            $createResult = 0;
            foreach ($auctionSessionList as $item) {
                $currentDate = date("Y/m/d H:i:s");
                if(strtotime($item->startAt) < strtotime($currentDate)){
                    return $this->responseError(ErrorCodes::TIME_IN_PAST);
                    break;
                }
                $auctionSession                             = new AuctionSession();
                $auctionSession->name                       = $item->name;
                $auctionSession->startDate                  = $item->startAt;
                $auctionSession->endDate                    = $item->endAt;
                $auctionSession->duration                   = $item->durationRq;
                $auctionSession->custom_bid_price_increment = $customBidPriceIncrement;
                $createResult                               = $auctionSession->save();
            }

            if ($createResult == 1 || sizeof($auctionSessionList) ==0) {
                $statusCode = 200;
                $responseMessage->setData('success', true);
                $result = $responseMessage->getMessage();

                return response()->json($result, $statusCode);
            } else {
                \Log::info("Cannot create Auction Session");
                return $this->responseError(ErrorCodes::CREATE_FAILED);
            }
        } catch (\Exception $exception) {
            \Log::error("Exception " . $exception);
            return $this->responseError(ErrorCodes::SYS_ERR);
        }


    }

    public function getAuctionSession(Request $request)
    {
        $token = $request->header("token");

        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        //check permission
        $apiPermission = array(Permissions::AUCTION_SESSION_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }


        $v = Validator::make($request->all(), [

            'limit' => 'integer|min:1|max:100',
            'offset' => 'integer|min:0',
            'orderDesc' => 'boolean',
            'filterEditable' => 'boolean'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $input = $request->json();
        $responseMessage = new ResponseMessage();

        $filterEditable = $input->get('filterEditable');
        $history =  $input->get('history');

        //Get list auction session with pagination and full size
        $listAuctionSession = AuctionSession::getAll($input, $history, $filterEditable);
        if (!$listAuctionSession) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        $statusCode = 200;

        $responseMessage->setData('list_size', $listAuctionSession[0]);
        $responseMessage->setData('sessionList', $listAuctionSession['sessionList']);

        $result = $responseMessage->getMessage();

        return response()->json($result, $statusCode);

    }

    public function updateAuctionSession(Request $request, $sessionId)
    {
        $token = $request->header("token");

        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // check permission
        $apiPermission = array(Permissions::AUCTION_SESSION_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }

        $startDate  = $request->json()->get('startDate');
        $time       = $request->json()->get('time');
        $duration   = $request->json()->get('duration');
        $name       = $request->json()->get('name');

        $validationData = [
            'token' => $token,
            'time' => $time,
            'name' => $name,
            'duration' => $duration,
            'startDate' => $startDate,

        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'name' => 'required',
            'time' => 'required',
            'duration' => 'required|numeric',
            'startDate' => 'required',

        ], [
            'required' => ErrorCodes::REQUIRED,
            'alpha_num' => ErrorCodes::ALPHA_NUM,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        $auctionSession = AuctionSession::find($sessionId);
        if (!$auctionSession) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        $customBidPriceIncrement = $request->json()->get('bid_price_increment');

        $sessionStartDate = date($startDate . ' ' . $time);
        $sessionEndDate = date("Y/m/d H:i", strtotime(" +$duration minutes", strtotime($sessionStartDate)));

        $checkResult = AuctionSession::validateDuplicateSessionTime($sessionStartDate, $sessionEndDate, $sessionId);
        if($checkResult){
            return $this->responseError(ErrorCodes::AUCTION_SESSION_DUPLICATE);
        }
        $currentDate = date("Y/m/d H:i:s");
        if(strtotime($sessionStartDate) < strtotime($currentDate)){
            return $this->responseError(ErrorCodes::TIME_IN_PAST);
        }
        $auctionSession->name                       = $name;
        $auctionSession->startDate                  = $sessionStartDate;
        $auctionSession->endDate                    = $sessionEndDate;
        $auctionSession->duration                   = $duration;
        $auctionSession->custom_bid_price_increment = $customBidPriceIncrement;

        $updateResult = $auctionSession->save();

        $responseMessage = new ResponseMessage();
        if ($updateResult == 1) {
            $statusCode = 200;
            $responseMessage->setData('success', true);
            $result = $responseMessage->getMessage();

            return response()->json($result, $statusCode);
        } else {
            \Log::info("Cannot update Auction Session");
            return $this->responseError(ErrorCodes::UPDATE_FAILED);
        }


    }

    public function deleteAuctionSession(Request $request, $sessionId){

        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        //check permission
        $apiPermission = array(Permissions::AUCTION_SESSION_DELETE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $auctionSession = AuctionSession::find($sessionId);
        if (!$auctionSession) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }
        //check if auction session must not have any vehicle and on live
        if(AuctionSession::checkAuctionSessionHaveVehicle($sessionId)){
            \Log::info("Cannot delete Auction Session has vehicle");
            return $this->responseError(ErrorCodes::AUCTION_SESSION_HAS_VEHICLE);
        }
        $deleteResult = $auctionSession->delete();
        $responseMessage = new ResponseMessage();
        if ($deleteResult == 1) {
            $statusCode = 200;
            $responseMessage->setData('success', true);
            $result = $responseMessage->getMessage();

            return response()->json($result, $statusCode);
        } else {
            \Log::info("Cannot delete Auction Session");
            return $this->responseError(ErrorCodes::UPDATE_FAILED);
        }
    }

    public function getAuctionSessionById(Request $request, $sessionId){
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        //check permission
        $apiPermission = array(Permissions::AUCTION_SESSION_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $auctionSession = AuctionSession::getById($sessionId);

        if (!$auctionSession[0]->id) {
            return $this->responseError(ErrorCodes::NOT_FOUND);
        }

        return response()->json($auctionSession);

    }

    /**
     * @param $startDate
     * @param $time
     * @param $duration
     * @param $auctionSession
     * @return AuctionSession
     */
    public function convertAndMapDataToSession($name, $startDate, $time, $duration, $auctionSession)
    {
        $sessionStartDate = date($startDate . ' ' . $time);
        $sessionEndDate = date("Y/m/d H:i", strtotime(" +$duration minutes", strtotime($sessionStartDate)));

        $auctionSession->name = $name;
        $auctionSession->startAt = $sessionStartDate;
        $auctionSession->endAt = $sessionEndDate;
        $auctionSession->durationRq = $duration;
        return $auctionSession;
    }


    public function getActiveSession(Request $request) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // check permission
        $apiPermission = array(Permissions::AUCTION_SESSION_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
//        $v = Validator::make($request->all(), [
//        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);
//
//        if ($v->fails()) {
//            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
//        }

//        $r = DB::select("SELECT * FROM auction_session WHERE :backend_time BETWEEN startDate and endDate", ['backend_time' => date('Y-m-d H:i:s')]);
        return $this->responseData(AuctionSession::getActiveSessions());
    }


    public function getNextSession(Request $request) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::AUCTION_SESSION_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        return $this->responseData(AuctionSession::getNextSessions());
    }

    public function findVehicleInSession(Request $request, $sessionId) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'offset' => 'required|integer|min:0',
            'limit' => 'required|integer',
            'query' => '',
            //'orderBy' => 'in:odometer,location_text,overall_rating',
            'orderBy' => 'in:trade_price,registration_number,day_in_stock,location_name,make,model_family,build_date,stock_number,vehicle_name',
            'orderDesc' => 'boolean',
            'make' => '',
            'variant' => '',
            'odometer' => 'array',
            'year' => 'array',
            'location' => 'array',
            'transmissionType' => ''

        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }
        $apiPermission = array(Permissions::AUCTION_ITEM_LISTING_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $json = $request->json();
        $offset = $json->get('offset');
        $limit = $json->get('limit');
        $location = null;
        $where = "";

        if ($json->has('location')) {
            if (sizeof($json->get('location')) == 0) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'location must contain at least 1 number');
            }
            $location = implode(",", $json->get('location'));
            $where .= " AND v.trade_in_location IN ($location) ";
        } else {
            if (!$user->location_id) {
                \LOG::warn("User[$user->id] doesn't have location_id");
            }
            $location = $user->location_id;
        }

        if ($json->has("odometer")) {
            $odometer = $json->get('odometer');
            if (sizeof($odometer) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'odometer must be an array contains 2 number');
            }
            $where .= " AND (odometer between $odometer[0] AND $odometer[1])";
        }

        if ($json->has("year")) {
            $year = $json->get('year');
            if (sizeof($year) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'year must be an array contains 2 number');
            }
            $where .= " AND (year(v.build_date) between $year[0] AND $year[1])";
        }


        $transmission = collect();

        if ($json->has("transmissionType")) {
            foreach ($json->get('transmissionType') as $type) {
                $x =  str_replace('\'', '"', $type);
                $x =  trim($x);
                if ($x) {
                    $transmission->push("'" . $x . "'");
                }
            }

            $transmission = $transmission->unique()->values()->all();

            $transmission = implode("','", $transmission);
            if($transmission) {
                $where .= " AND (v.transmission IN ($transmission))";
            }
        }

        $make = collect();
        $model = collect();
        $variant = collect();

        if ($json->has("makeInfo")) {
            $arr = $json->get('makeInfo');
            foreach ($arr as $i) {
                $i = (object) $i;

                $i->make = str_replace('\'', '"', $i->make);
                $i->make = trim($i->make);

                $i->model = str_replace('\'', '"', $i->model);
                $i->model = trim($i->model);

                $i->variant = str_replace('\'', '"', $i->variant);
                $i->variant = trim($i->variant);

                if ($i->make) {
                    $make->push("'" . $i->make. "'" );
                }
                if ($i->model) {
                    $model->push("'" . $i->model. "'" );
                }
                if ($i->variant) {
                    $variant->push("'" . $i->variant. "'" );
                }
            }
        }

        $make = $make->unique()->values()->all();
        $model = $model->unique()->values()->all();
        $variant = $variant->unique()->values()->all();

        if (sizeof($make) > 0) {
            $make = implode(",", $make);
            $where .= " AND (make IN ($make))";
        }
        if (sizeof($model) > 0) {
            $model = implode(",", $model);
            $where .= " AND (model_family IN ($model))";
        }
        if (sizeof($variant) > 0) {
            $variant = implode(",", $variant);
            $where .= " AND (variant IN ($variant))";
        }


        if ($json->has("query")) {
            $text = $json->get('query');
            $text = str_replace('\'', '"', $text); // for safety reasons;
            if ($text) {
                $query = "(YEAR(build_date) LIKE '$text*')";
                $query .= "OR MATCH(make, family, series, style, engine_type, engine_size, variant, cylinder, transmission, registration_number, body_type) AGAINST ('$text*' IN BOOLEAN MODE)";
                $where .= " AND ($query) ";
            }
        }

        $seller_rating = $json->get('seller_rating');
        if ($seller_rating) {
            $ratings = implode(",", $seller_rating);
            $where .= " AND find_in_set(l.seller_rating, '$ratings') ";
        }

        $order = "";
        if ($json->has("orderBy")) {
            $order = " ORDER BY " . $json->get('orderBy');
            if ($json->get("orderDesc")) {
                $order .= " DESC ";
            }
        }

        $q = "
            SELECT v.*, v.id vehicle_id, va.id valuation_id, va.proposed_arrival_date, va.stock_number, va.total_recon_cost, type_code.value as overall_rating, au.*, shi.bid_price, shi.bid_user_id, l.id as location_id,
             l.name AS location_text, l.buyer_rating, l.seller_rating, CONCAT_WS(' ', trim(make), trim(family), trim(series), trim(style), trim(engine_type), trim(engine_capacity), trim(engine_size), trim(variant), trim(cylinder), trim(transmission)) vehicle_name
            FROM 
              session_has_item shi 
              LEFT JOIN auction_item au ON shi.auction_item_id = au.id
              LEFT JOIN valuation va ON au.valuation_id = va.id
              LEFT JOIN vehicle v ON va.vehicle_id = v.id
              LEFT JOIN location l ON l.id = v.trade_in_location
              INNER JOIN type_code ON type_code.code = va.overall_rating AND type_code.type = 'OVERALL_RATING'  
            WHERE shi.status=:status AND shi.auction_session_id = $sessionId
            $where 
            $order LIMIT  $offset, $limit ";


        $result = DB::select($q,
            [
                'status' => Auction::STATUS_CODE_APPROVE, // acepted status
//                'company_group_id' => $user->company_group_id,
//                'backend_date' => date('Y-m-d H:i:s')
            ]);

        foreach ($result as $item) {
            $item->quick_seller   = Location::isSpeedySeller($item->location_id);
            $item->quick_buyer    = Location::isSpeedyBuyer($item->location_id);
        }

//        foreach($result as $r) {
//            $details = [];
//            if ($r->make) {
//                $details[] = trim($r->make);
//            }
//            if ($r->family) {
//                $details[] = trim($r->family);
//            }
//            if ($r->series) {
//                $details[] = trim($r->series);
//            }
//            if ($r->style) {
//                $details[] = trim($r->style);
//            }
//            if ($r->engine_type) {
//                $details[] = trim($r->engine_type);
//            }
//            if ($r->engine_capacity) {
//                $details[] = trim($r->engine_capacity);
//            }
//            if ($r->engine_size) {
//                $details[] = trim($r->engine_size);
//            }
//            if ($r->variant) {
//                $details[] = trim($r->variant);
//            }
//            if ($r->cylinder) {
//                $details[] = trim($r->cylinder);
//            }
//            if ($r->transmission) {
//                $details[] = trim($r->transmission);
//            }
//            $r->vehicle_name = implode(' ', $details);
//        }

        return $this->responseData($result);
    }


    public function countVehicleInSession(Request $request, $sessionId) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $v = Validator::make($request->all(), [
            'offset' => 'required|integer|min:0',
            'limit' => 'required|integer',
            'query' => '',
            'make' => '',
            'variant' => '',
            'odometer' => 'array',
            'year' => 'array',
            'location' => 'array',
            'transmissionType' => ''

        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);

        if ($v->fails()) {
            return $this->responseError(ErrorCodes::INVALID_INPUT, $v->errors()->getMessages());
        }

        $apiPermission = array(Permissions::AUCTION_ITEM_LISTING_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $json = $request->json();
        $offset = $json->get('offset');
        $limit = $json->get('limit');
        $location = null;
        $where = "";

        if ($json->has('location')) {
            if (sizeof($json->get('location')) == 0) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'location must contain at least 1 number');
            }
            $location = implode(",", $json->get('location'));
            $where .= " AND v.trade_in_location IN ($location) ";
        } else {
            if (!$user->location_id) {
                \LOG::warn("User[$user->id] doesn't have location_id");
            }
            $location = $user->location_id;
        }

        if ($json->has("odometer")) {
            $odometer = $json->get('odometer');
            if (sizeof($odometer) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'odometer must be an array contains 2 number');
            }
            $where .= " AND (odometer between $odometer[0] AND $odometer[1])";
        }

        if ($json->has("year")) {
            $year = $json->get('year');
            if (sizeof($year) != 2) {
                return $this->responseError(ErrorCodes::INVALID_INPUT, 'year must be an array contains 2 number');
            }
            $where .= " AND (year(v.build_date) between $year[0] AND $year[1])";
        }


        $transmission = collect();

        if ($json->has("transmissionType")) {
            foreach ($json->get('transmissionType') as $type) {
                $x =  str_replace('\'', '"', $type);
                $x =  trim($x);
                if ($x) {
                    $transmission->push("'" . $x . "'");
                }
            }

            $transmission = $transmission->unique()->values()->all();

            $transmission = implode("','", $transmission);
            if($transmission) {
                $where .= " AND (v.transmission IN ($transmission))";
            }
        }

        $make = collect();
        $model = collect();
        $variant = collect();

        if ($json->has("makeInfo")) {
            $arr = $json->get('makeInfo');
            foreach ($arr as $i) {
                $i = (object) $i;

                $i->make = str_replace('\'', '"', $i->make);
                $i->make = trim($i->make);

                $i->model = str_replace('\'', '"', $i->model);
                $i->model = trim($i->model);

                $i->variant = str_replace('\'', '"', $i->variant);
                $i->variant = trim($i->variant);

                if ($i->make) {
                    $make->push("'" . $i->make. "'" );
                }
                if ($i->model) {
                    $model->push("'" . $i->model. "'" );
                }
                if ($i->variant) {
                    $variant->push("'" . $i->variant. "'" );
                }
            }
        }

        $make = $make->unique()->values()->all();
        $model = $model->unique()->values()->all();
        $variant = $variant->unique()->values()->all();

        if (sizeof($make) > 0) {
            $make = implode(",", $make);
            $where .= " AND (make IN ($make))";
        }
        if (sizeof($model) > 0) {
            $model = implode(",", $model);
            $where .= " AND (model_family IN ($model))";
        }
        if (sizeof($variant) > 0) {
            $variant = implode(",", $variant);
            $where .= " AND (variant IN ($variant))";
        }


        if ($json->has("query")) {
            $text = $json->get('query');
            $text = str_replace('\'', '"', $text); // for safety reasons;
            if ($text) {
                $query = "(YEAR(build_date) LIKE '$text*')";
                $query .= "OR MATCH(make, family, series, style, engine_type, engine_size, variant, cylinder, transmission, registration_number, body_type) AGAINST ('$text*' IN BOOLEAN MODE)";
                $where .= " AND ($query) ";
            }
        }

        $q = "
            SELECT COUNT(*) AS count FROM 
              session_has_item shi 
              LEFT JOIN auction_item au ON shi.auction_item_id = au.id
              LEFT JOIN valuation va ON au.valuation_id = va.id
              LEFT JOIN vehicle v ON va.vehicle_id = v.id
            WHERE shi.status=:status AND shi.auction_session_id = $sessionId $where LIMIT  $offset, $limit ";


        $result = DB::select($q,
            [
                'status' => Auction::STATUS_CODE_APPROVE, // acepted status
//                'company_group_id' => $user->company_group_id,
//                'backend_date' => date('Y-m-d H:i:s')
            ]);
        return $this->responseData($result[0]);
    }

    public function history(Request $request, $sessionId) {
        //todo: need define permission for this api - update to jira for log.
        $user = NULL;
        try {
            $user = $this->currentUser($request, NULL);
        } catch(Exception $e) {
            return $this->responseError($e->getMessage());
        }

        $offset = $request->json()->get('offset') ? $request->json()->get('offset') : 0;
        $limit  = $request->json()->get('limit') ? $request->json()->get('limit') : 20;

        $result['value'] = Auction::history($sessionId, $offset, $limit);
        $result['count'] = Auction::historyCount($sessionId);

        return response()->json($result);
    }
}
