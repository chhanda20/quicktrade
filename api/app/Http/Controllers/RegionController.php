<?php

namespace App\Http\Controllers;



use App\AuctionSession;
use App\Libraries\ErrorCodes;
use App\Libraries\Event;
use App\Libraries\Helper;
use App\NotificationMessage;
use App\User;
use App\UserRole;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;



class RegionController extends Controller
{


    public function getRegionLists(Request $request) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        $result = DB::select("SELECT * FROM region");

        return $this->responseData($result);
    }
}
