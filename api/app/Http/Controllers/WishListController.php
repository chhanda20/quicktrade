<?php

namespace App\Http\Controllers;


use App\AppProperty;
use App\Auction;
use App\AuctionSession;
use App\Libraries\AuctionBidHelper;
use App\Libraries\ErrorCodes;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\Libraries\ResponseMessage;
use App\SessionHasItem;
use App\TestDrive;
use App\User;
use App\WishList;
use DateTime;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class WishListController extends Controller
{
    public function __construct()
    {
    }

    public function createWishList(Request $request)
    {
        $token = $request->header("token");
        $makeInfo = $request->json()->get('makeInfo');
        $location = $request->json()->get('location');
        $price = $request->json()->get('price');
        $year = $request->json()->get('year');
        $odometer = $request->json()->get('odometer');
        $transmission = $request->json()->get('transmission');


        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }


        //check permission create Wish List
        $apiPermission = array(Permissions::WISH_LIST_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        try {
            $validationData = [
                'token' => $token,
                'price' => $price,
                'year' => $year,
                'odometer' => $odometer

            ];
            $v = Validator::make($validationData, [
                'token' => 'required|alpha_num|exists:mysql.user,token',
                'year' => 'array',
                'price' => 'array',
                'odometer' => 'array',


            ], [
                'required' => ErrorCodes::REQUIRED,
                'alpha_num' => ErrorCodes::ALPHA_NUM,
                'exists' => ErrorCodes::NOT_EXISTS,
                'numeric' => ErrorCodes::NUMERIC,
            ]);
            if ($v->fails()) {
                $errorMessages = $v->errors()->getMessages();
                return $this->responseError(ErrorCodes::INVALID, $errorMessages);
            }
            $responseMessage = new ResponseMessage();
            //6 Jul 2017: According to Kim, change wish list logic: wishlist now marked with user location,not by user id. Therefore wishlist will be seen by location
            //This will cause effect to search auction wishlist tab

            $wishList = new WishList();
            $wishList->user_id = $user->id;
            $wishList->dealer_location = $user->location_id;
            $strFromArr = json_encode($makeInfo);
            $wishList->makeInfo = $strFromArr;
            if($location){
                $wishList->locationId = implode(",",$location);
            }

            $wishList->price_start = $price[0];
            $wishList->price_end = $price[1];
            $wishList->year_start = $year[0];
            $wishList->year_end = $year[1];
            $wishList->odometer_start = $odometer[0];
            $wishList->odometer_end = $odometer[1];
            $wishList->transmission = $transmission ? json_encode($transmission): null;
            $createResult = $wishList->save();

            if ($createResult == 1) {
                $statusCode = 200;
                $responseMessage->setData('success', true);
                $result = $responseMessage->getMessage();

                return response()->json($result, $statusCode);
            } else {
                \Log::info("Cannot create Wish List");
                return $this->responseError(ErrorCodes::CREATE_FAILED);
            }
        } catch (\Exception $exception) {
            \Log::error("Exception " . $exception);
            return $this->responseError(ErrorCodes::SYS_ERR);
        }


    }

    public function searchWishList(Request $request){
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }


        //check permission to view Wish List
        $apiPermission = array(Permissions::WISH_LIST_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        try {
            $validationData = [
                'token' => $token,

            ];
            $v = Validator::make($validationData, [
                'token' => 'required|alpha_num|exists:mysql.user,token'


            ], [
                'required' => ErrorCodes::REQUIRED,
                'alpha_num' => ErrorCodes::ALPHA_NUM,
                'exists' => ErrorCodes::NOT_EXISTS,
                'numeric' => ErrorCodes::NUMERIC,
            ]);
            if ($v->fails()) {
                $errorMessages = $v->errors()->getMessages();
                return $this->responseError(ErrorCodes::INVALID, $errorMessages);
            }
            $responseMessage = new ResponseMessage();

            //search wishList
            $input = $request->json();
            $result = WishList::search($input, $user->location_id);
            foreach ($result['wishList'] as $wishList){
                $wishList->makeInfo = json_decode($wishList->makeInfo, true);

                $wishList->transmission = $wishList->transmission ? json_decode($wishList->transmission, true) : null;

            }

            $statusCode = 200;
            $responseMessage->setData('countList', $result[0]);
            $responseMessage->setData('wishList', $result['wishList']);
            $result = $responseMessage->getMessage();

            return response()->json($result, $statusCode);
        } catch (\Exception $exception) {
            \Log::error("Exception " . $exception);
            return $this->responseError(ErrorCodes::SYS_ERR);
        }
    }

    public function getWishListById(Request $request, $id){
        $token = $request->header("token");
        $user = NULL;
        if ($token) {
            $user = User::where('token', $token)->first();
        }
        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::WISH_LIST_VIEW);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        try {
            //search wishList
            $result = WishList::getById($id, $user->location_id);


            if($result){
                $result[0]->makeInfo = json_decode($result[0]->makeInfo, true);

                $result[0]->transmission = $result[0]->transmission ? json_decode($result[0]->transmission, true) : null;
                $statusCode = 200;
                return response()->json($result[0], $statusCode);
            }else{
                return $this->responseError(ErrorCodes::NOT_FOUND);
            }

        } catch (\Exception $exception) {
            \Log::error("Exception " . $exception);
            return $this->responseError(ErrorCodes::SYS_ERR);
        }
    }

    public function updateWishList(Request $request, $id){
        $token = $request->header("token");
        $makeInfo = $request->json()->get('makeInfo');
        $location = $request->json()->get('location');
        $price = $request->json()->get('price');
        $year = $request->json()->get('year');
        $odometer = $request->json()->get('odometer');
        $transmission = $request->json()->get('transmission');


        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }


        //check permission to update Wish List
        $apiPermission = array(Permissions::WISH_LIST_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        try {
            $validationData = [
                'token' => $token,
                'price' => $price,
                'year' => $year,
                'odometer' => $odometer

            ];
            $v = Validator::make($validationData, [
                'token' => 'required|alpha_num|exists:mysql.user,token',
                'year' => 'array',
                'price' => 'array',
                'odometer' => 'array',


            ], [
                'required' => ErrorCodes::REQUIRED,
                'alpha_num' => ErrorCodes::ALPHA_NUM,
                'exists' => ErrorCodes::NOT_EXISTS,
                'numeric' => ErrorCodes::NUMERIC,
            ]);
            if ($v->fails()) {
                $errorMessages = $v->errors()->getMessages();
                return $this->responseError(ErrorCodes::INVALID, $errorMessages);
            }
            $responseMessage = new ResponseMessage();

            $wishList = WishList::find($id);
            if(!$wishList){
                return $this->responseError(ErrorCodes::NOT_FOUND);
            }
            $strFromArr = json_encode($makeInfo);
            $wishList->makeInfo = $strFromArr;
            if($location){
                $wishList->locationId = implode(",",$location);
            }else{
                $wishList->locationId = null;
            }
            $wishList->price_start = $price[0];
            $wishList->price_end = $price[1];
            $wishList->year_start = $year[0];
            $wishList->year_end = $year[1];
            $wishList->odometer_start = $odometer[0];
            $wishList->odometer_end = $odometer[1];
            $wishList->transmission = $transmission ? json_encode($transmission) : null;
            $createResult = $wishList->save();

            if ($createResult == 1) {
                $statusCode = 200;
                $responseMessage->setData('success', true);
                $result = $responseMessage->getMessage();

                return response()->json($result, $statusCode);
            } else {
                \Log::info("Cannot create Wish List");
                return $this->responseError(ErrorCodes::CREATE_FAILED);
            }
        } catch (\Exception $exception) {
            \Log::error("Exception " . $exception);
            return $this->responseError(ErrorCodes::SYS_ERR);
        }
    }
}
