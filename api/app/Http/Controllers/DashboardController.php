<?php

namespace App\Http\Controllers;


use App\AppProperty;
use App\Auction;
use App\AuctionSession;
use App\BidHistory;
use App\Dispute;
use App\Document;
use App\ExternalOffer;
use App\Libraries\AuctionBidHelper;
use App\Libraries\Dashboard;
use App\Libraries\ErrorCodes;
use App\Libraries\Permissions;
use App\Libraries\ResponseMessage;
use App\Location;
use App\Menu;
use App\RoleHasMenu;
use App\SessionHasItem;
use App\TestDrive;
use App\Transaction;
use App\User;
use App\Valuation;
use App\Vehicle;
use App\WishList;
use DateTime;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use stdClass;
use Exception;

class DashboardController extends Controller
{
    public function __construct()
    {
    }

    public function getAccessableLocations(Request $request) {
        $user = NULL;
        try {
            $user = $this->currentUser($request, [Permissions::AUCTION_SESSION_CREATE]);
        } catch(Exception $e) {
            return $this->responseError($e->getMessage());
        }
        return Location::accessableLocationsQuery($user->id)->select('location.id')->get();
    }

    /***
     * get list dashboard
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getListDashBoard(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }
        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        // get list dashboard by Category
        $listDashboard = DB::select("SELECT * FROM dashboard");

        $newArray = array();
        $dashboardList = array();
        foreach ($listDashboard as $row) {
            $newArray[$row->dashboard_category][] = $row;
        }
//        var_dump($newArray);

        //loop to set correct format and return to FE
        foreach ($newArray as $key => $arr) {

            $siteArray = array('dashboard_category' => $key);
            $groupArray = array();
            foreach ($arr as $key1 => $m) {
                $dashboardDetail = new stdClass();
                $dashboardDetail->name = $m->dashboard_name;
                $dashboardDetail->code = $m->dashboard_code;
                $dashboardDetail->label = $m->label;

                $groupArray[] = $dashboardDetail;
            }
            $siteArray['dashboard_items'] = $groupArray;
            array_push($dashboardList, $siteArray);
        }


        $responseMessage = new ResponseMessage();
        $statusCode = 200;

        $responseMessage->setData('listDashboard', $dashboardList);

        $result = $responseMessage->getMessage();

        return response()->json($result, $statusCode);
    }

    public function getDashboardDetail(Request $request)
    {
        $user = NULL;
        try {
            $user = $this->currentUser($request);
        } catch (Exception $e) {
            return $this->responseError($e->getMessage());
        }

        $company_group_id = $user->company_group_id;
        $input = $request->json();
        $dashboardList = $input->get('dashboard_list');

        $filter = $this->getFilterInput($request, $user);

        $createDate     = $filter['createDate'];
        $locationIds    = $filter['locationIds'];
        $ownStat        = $filter['ownStat'];

        if (!$dashboardList) {
            return response()->json();
        }

        $result = [];

        foreach ($dashboardList as $dashboard) {

            switch ($dashboard) {
                //Test Drive
                case Dashboard::TOTAL_TEST_DRIVE:
                    $result[$dashboard] = TestDrive::filterQuery(
                        NULL,
                        $user,
                        $ownStat,
                        $locationIds,
                        $createDate)->count(DB::raw('distinct test_drive.id'));
                    break;

                //Valuation
                case Dashboard::TOTAL_VALUATIONS:
                case Dashboard::TOTAL_TRADE_INS_WON:
                    $result[$dashboard] = Valuation::filterQuery($dashboard,
                        $user,
                        $ownStat,
                        $locationIds,
                        $createDate)->count(DB::raw('distinct valuation.id'));
                    \LOG::debug("TOTAL_VALUATIONS [$dashboard]:[$result[$dashboard]] $user->id");
                    break;
                case Dashboard::VALUATION_TO_TRADE_IN_PERCENTAGE:
                    $result[$dashboard] = Valuation::percentageValuationWon(
                        $user,
                        $ownStat,
                        $locationIds,
                        $createDate);
                    break;

                //Place my car
                case Dashboard::TOTAL_OUTBOUND_OFFER_REQUESTS:
                case Dashboard::TOTAL_OUTBOUND_OFFER_REQUESTS_ACCEPTED:
                case Dashboard::TOTAL_OUTBOUND_CARS_SOLD:
                case Dashboard::TOTAL_BIDS_WON:
                case Dashboard::TOTAL_BID_ACCEPTED:
                case Dashboard::TOTAL_INBOUND_OFFERS_BOUGHT:
                    $result[$dashboard] = ExternalOffer::filterQuery($dashboard,
                        $user,
                        $ownStat,
                        $locationIds,
                        $createDate)->count(DB::raw('distinct external_offer.id'));
                    break;
                case Dashboard::TOTAL_BIDS_MADE:
                    $result[$dashboard] = ExternalOffer::filterQuery($dashboard,
                        $user,
                        $ownStat,
                        $locationIds,
                        $createDate)->count(DB::raw('distinct bid_history.id'));
                    break;
                case Dashboard::PERCENTAGE_BIDS_WON:
                    $result[$dashboard] = ExternalOffer::percentageBidWon($user, $ownStat, $locationIds, $createDate);
                    break;

                //Auction
                case Dashboard::CARS_WIN_BOUGHT:
                case Dashboard::CARS_LISTED:
                case Dashboard::CARS_SOLD_AT_AUCTION:
                case Dashboard::CARS_ON_WISHLIST_APPEARING_ON_NEXT_AUCTION:
                    $query = Vehicle::filterQuery($dashboard,
                        $user,
                        $ownStat,
                        $locationIds,
                        $createDate);
                    $result[$dashboard] = $query ? $query->count(DB::raw('distinct vehicle.id')) : 0;
                    break;
                case Dashboard::PERCENTAGE_CARS_LISTED_V_SOLD:
                    $result[$dashboard] = Vehicle::percentageCarsListedVsSold($user, $ownStat, $locationIds, $createDate);
                    break;

                //Dispute
                case Dashboard::DISPUTES_RAISED_AGAINST_YOU:
                case Dashboard::DISPUTES_FOUND_AT_FAULT:
                    $result[$dashboard] = Dispute::filterQuery(
                        $dashboard,
                        $user,
                        $ownStat,
                        $locationIds,
                        $createDate)->count(DB::raw('distinct dispute.id'));
                    break;
                case Dashboard::PERCENTAGE_DISPUTES_RAISED_V_TOTAL_CARS_SOLD:
                case Dashboard::PERCENTAGE_DISPUTES_AT_FAULTS_V_TOTAL_CARS_SOLD:
                    $result[$dashboard] = Dispute::percentage($dashboard,
                        $user,
                        $ownStat,
                        $locationIds,
                        $createDate);
                    break;
            }
        }
        $responseMessage = new ResponseMessage();
        $statusCode = 200;
        $responseMessage->setData('data', $result);
        $result = $responseMessage->getMessage();
        return response()->json($result, $statusCode);
    }

    public function testDrivesInProgress(Request $request) {
        $user = NULL;
        try {
            $user = $this->currentUser($request);
        } catch (Exception $e) {
            return $this->responseError($e->getMessage());
        }

        $filter = $this->getFilterInput($request, $user);

        $createDate     = $filter['createDate'];
        $locationIds    = $filter['locationIds'];
        $ownStat        = $filter['ownStat'];

        $result = TestDrive::filterQuery(
            [TestDrive::STATUS_CODE_IN_PROGRESS],
            $user,
            $ownStat,
            $locationIds,
            $createDate
        )->get();

        return $this->responseData($result);
    }

    public function valuations(Request $request) {
        $user = NULL;
        try {
            $user = $this->currentUser($request);
        } catch (Exception $e) {
            return $this->responseError($e->getMessage());
        }
        $filter = $this->getFilterInput($request, $user);

        $createDate     = $filter['createDate'];
        $locationIds    = $filter['locationIds'];
        $ownStat        = $filter['ownStat'];

        $result = TestDrive::filterQuery(
            [TestDrive::STATUS_CODE_IN_PROGRESS],
            $user,
            $ownStat,
            $locationIds,
            $createDate
        )->get();

        return $this->responseData($result);
    }

    public function placeMyCars(Request $request) {
        $user = NULL;
        try {
            $user = $this->currentUser($request);
        } catch (Exception $e) {
            return $this->responseError($e->getMessage());
        }
        $filter = $this->getFilterInput($request, $user);

        $createDate     = $filter['createDate'];
        $locationIds    = $filter['locationIds'];
        $ownStat        = $filter['ownStat'];

        $result = Valuation::filterQuery(
            $user,
            [TestDrive::STATUS_CODE_IN_PROGRESS],
            $ownStat,
            $locationIds,
            $createDate
        )->get();

        return $this->responseData($result);
    }

    public function auctions() {

    }

    public function disputes() {

    }

    private function getFilterInput($request, $user) {
        $input = $request->json();

        $createDate = NULL;
        if ($input->get('range_from') && $input->get('range_to')) {
            $createDate['start'] = $input->get('range_from'). ' 0:0:0';
            $createDate['end'] = $input->get('range_to') . ' 23:59:59';
        }

        //prepare query
        $inputLocationIds = $request->json()->get('location');
        $locationIds = $inputLocationIds ? $inputLocationIds : $user->getAccessableLocationIds();
        $ownStat = $request->json()->get('own_stat');

        $filter['createDate']   = $createDate;
        $filter['locationIds']  = $locationIds;
        $filter['ownStat']      = $ownStat;

        return $filter;
    }
}



