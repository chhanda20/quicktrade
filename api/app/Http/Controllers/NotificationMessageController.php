<?php

namespace App\Http\Controllers;



use App\AuctionSession;
use App\Libraries\ErrorCodes;
use App\Libraries\Event;
use App\Libraries\Helper;
use App\NotificationMessage;
use App\User;
use App\UserRole;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;



class NotificationMessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function getNotificationByUser(Request $request) {
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $roles = DB::select("SELECT DISTINCT(name) FROM `role` r INNER JOIN `user_role` ur ON r.id=ur.role_id WHERE ur.user_id = $user->id");
        $result = "";
        foreach ($roles as $role) {
            if($result)
                $result .=",";
            $result.= "'" .Helper::channelName(strtolower($role->name),"role",$user->location_id)."'";

        }

        $listNotification = NotificationMessage::getListNotificationMessage($user->id, $result);
          //  dd($listNotification);
        return $this->responseData($listNotification);
    }
}
