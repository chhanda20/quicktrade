<?php

namespace App\Http\Controllers;


use App\AppProperty;
use App\Auction;
use App\AuctionSession;
use App\Document;
use App\Libraries\AuctionBidHelper;
use App\Libraries\ErrorCodes;
use App\Libraries\Permissions;
use App\Libraries\ResponseMessage;
use App\Menu;
use App\RoleHasMenu;
use App\SessionHasItem;
use App\TestDrive;
use App\Transaction;
use App\User;
use App\WishList;
use DateTime;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AccountingController extends Controller
{
    public function __construct()
    {
    }


    public function uploadDocument(Request $request, $transactionId){
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        //todo check permission
        //check permission to view roles
//        $apiPermission = array(Permissions::ROLE_GET);
//        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
//        if(!$valuationResult){
//
//            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
//        }

        // get document name/id, get document type
        $input = $request->json();
        $documents = $input->get('document_name');

        //get from db
        $documentType = Document::DOCUMENT_TYPE_TRANSACTION;
        $documentDB = Document::findBySourceId($documentType,$transactionId);
        if($documentDB){
            //If exist: merge with uploaded document and store.
            $uploadedDocument = explode(',',$documentDB->document_uploaded);
            $documentListToInsert = array_unique(array_merge($uploadedDocument,$documents));
            $stringToInsert = implode(",",$documentListToInsert);
            Document::updateDocument($stringToInsert, $documentType, $transactionId);
        }else{
            //if not exist: create new
            $newDocumentDto = new Document();
            $newDocumentDto->document_type = $documentType;
            $newDocumentDto->source_id = $transactionId;
            $newDocumentDto->document_uploaded = implode(',',$documents);
            $newDocumentDto->save();
        }
        $responseMessage = new ResponseMessage();
        $statusCode = 200;
        $responseMessage->setData('success', true);
        $result = $responseMessage->getMessage();

        return response()->json($result, $statusCode);
    }

    public function getDocument(Request $request, $transactionId){
        $token = $request->header("token");
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        //todo check permission
        //check permission to view roles
//        $apiPermission = array(Permissions::ROLE_GET);
//        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
//        if(!$valuationResult){
//
//            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
//        }
        $documentType = Document::DOCUMENT_TYPE_TRANSACTION;
        $documentDB = Document::findBySourceId($documentType,$transactionId);
        $uploadedDocument = null;
        if ($documentDB){
            $uploadedDocument = explode(',',$documentDB->document_uploaded);
        }

        $responseMessage = new ResponseMessage();
        $responseMessage->setData("uploadedDocuments",$uploadedDocument);
        $result = $responseMessage->getMessage();
        $statusCode = 200;
        return response()->json($result, $statusCode);
    }



}
