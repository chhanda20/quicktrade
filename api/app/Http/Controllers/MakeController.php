<?php

namespace App\Http\Controllers;

use App\Libraries\ErrorCodes;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MakeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function find(Request $request)
    {
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::VALUATION_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $result = DB::select("SELECT glass_make_id `id`, RTRIM(description) `name` FROM glass_make");
        return $this->responseData($result);
    }
}
