<?php

namespace App\Http\Controllers;

use App\ExternalGroup;
use App\ExternalGroupLocation;
use App\Libraries\ErrorCodes;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\Libraries\ResponseMessage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ExternalGroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function create(Request $request)
    {
        $token = $request->header('token');
        $name = $request->json()->get('name');
        $companyId = $request->json()->get('companyId');
        $description = $request->json()->get('description');
        $locationList = $request->json()->get('location');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::EXTERNAL_GROUP_CREATE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'name' => $name,
            'companyId' => $companyId,
            'locationList' => $locationList,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'name' => 'required|string',
            'companyId' => 'numeric|required|exists:mysql.company,id',
            'description' => 'string',
            'locationList' => 'array|required',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
            'array' => ErrorCodes::ARRAY_TYPE,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }
        $externalGroupModel = new ExternalGroup();
        $externalGroupModel->name = $name;
        $externalGroupModel->company_id = $companyId;
        $externalGroupModel->description = $description;
        $externalGroupModel->save();

        foreach ($locationList as $locationId) {
            $externalGLocationModel = new ExternalGroupLocation();
            $externalGLocationModel->external_group_id = $externalGroupModel->id;
            $externalGLocationModel->location_id = $locationId;
            $externalGLocationModel->save();
        }

        return $this->responseData(['success' => TRUE]);
    }

    public function getExternalGroupById(Request $request, $id){
        $token = $request->header("token");
        $user = NULL;
        $company_id = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }
        $apiPermission = array(Permissions::EXTERNAL_GROUP_GET);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $externalGroup = ExternalGroup::find($id);
        if(!$externalGroup){
            return $this->responseError(ErrorCodes::EXTERNAL_GROUP_NOT_FOUND);
        }


        $listDealer = ExternalGroup::getListDealer($id);
        $result = array();$i = 0;
        $groupName = "";
        $groupDesc = "";
        foreach ($listDealer as $dealer){
            $groupDesc = $dealer->group_description;
            $groupName = $dealer->group_name;
            $temp = array("dealerName" => $dealer->company_name,"dealerId" => $dealer->company_id);
            $locationList = ExternalGroup::getListLocationByDealer($id, $dealer->company_id);
            $temp['locationList'] = $locationList;
            array_push($result,$temp);
            unset($temp);
            $i++;

        }
        $responseMessage = new ResponseMessage();
        $responseMessage->setData("groupName",$groupName);
        $responseMessage->setData("groupDescription",$groupDesc);
        $responseMessage->setData("externalGroupId",$id);
        $responseMessage->setData("listLocation",$result);

        $result = $responseMessage->getMessage();
        $statusCode = 200;
        return response()->json($result, $statusCode);


    }

    public function updateExternalGroup(Request $request, $id){
        $token = $request->header('token');
        $name = $request->json()->get('name');
        $companyId = $request->json()->get('companyId');
        $description = $request->json()->get('description');
        $locationList = $request->json()->get('location');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::EXTERNAL_GROUP_EDIT);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'name' => $name,
            'companyId' => $companyId,
            'locationList' => $locationList,
            'externalGroupId' => $id,
            'description' => $description,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'name' => 'required|string',
            'companyId' => 'numeric|required|exists:mysql.company,id',
            'description' => 'string',
            'locationList' => 'array|required',
            'externalGroupId' => 'required|exists:mysql.external_group,id',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
            'array' => ErrorCodes::ARRAY_TYPE,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        // find external group record
        $externalGroup = ExternalGroup::find($id);
        if(!$externalGroup){
            return $this->responseError(ErrorCodes::EXTERNAL_GROUP_NOT_FOUND);
        }
        //update external group info
        $externalGroup->name = $name;
        $externalGroup->company_id = $companyId;
        $externalGroup->description = $description;
        $externalGroup->save();


        //update external - location relation
        //remove all link between location and external group
        DB::select("delete from external_group_location where external_group_id = $id");

        //add new link location - external group
        foreach ($locationList as $locationId) {
            $externalGLocationModel = new ExternalGroupLocation();
            $externalGLocationModel->external_group_id = $id;
            $externalGLocationModel->location_id = $locationId;
            $externalGLocationModel->save();
        }

        return $this->responseData(['success' => TRUE]);
    }

    public function deleteExternalGroup(Request $request, $id){

        $token = $request->header('token');
        $user = NULL;

        if ($token) {
            $user = User::where('token', $token)->first();
        }

        if (!$user) {
            return $this->responseError(ErrorCodes::INVALID_TOKEN);
        }

        // CHECKING PERMISSION
        $apiPermission = array(Permissions::EXTERNAL_GROUP_DELETE);
        $valuationResult = Helper::valuatePermission($user->id, $apiPermission);
        if(!$valuationResult){

            return $this->responseError(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
        $validationData = [
            'token' => $token,
            'id' => $id,
        ];
        $v = Validator::make($validationData, [
            'token' => 'required|alpha_num|exists:mysql.user,token',
            'id' => 'required|numeric|exists:mysql.external_group,id'

        ], [
            'required' => ErrorCodes::REQUIRED,
            'exists' => ErrorCodes::NOT_EXISTS,
            'numeric' => ErrorCodes::NUMERIC,
            'array' => ErrorCodes::ARRAY_TYPE,
            'alpha_num' => ErrorCodes::ALPHA_NUM
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            return $this->responseError(ErrorCodes::INVALID, $errorMessages);
        }

        DB::table('external_group')->where('id', '=', $id)->delete();

        return $this->responseData(['success' => TRUE]);
    }
}
