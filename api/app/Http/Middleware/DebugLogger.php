<?php

namespace App\Http\Middleware;

use Closure;

class DebugLogger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (env('APP_DEBUG') === true) {

            $response =  $next($request);
            if ($request->isJson()) {
                $log_array = json_decode($request->getContent(), true);
                $json_pretty_string = json_encode($log_array, JSON_PRETTY_PRINT);
                \LOG::debug($request->header('User-Agent'));
                \LOG::debug(sprintf("%s %s [%s] %s", $request->header('token'), $request->decodedPath(), $request->method(), $json_pretty_string));
            } else {
                \LOG::debug(sprintf("%s %s [%s] [NOT JSON BODY]", $request->header('token'), $request->decodedPath(), $request->method()));
            }

            if (method_exists($response, 'status')) {
                $content = $response->content();
                $log_array = json_decode($content, true);
                if ($log_array) {
                    $content = json_encode($log_array, JSON_PRETTY_PRINT);
                }
                \LOG::debug(sprintf("%s \t--> %3d %.2000s", $request->header('token'), $response->status(), $content));
            } else {
                \LOG::debug(sprintf("%s \t--> UNKNOWN", $request->header('token')));
            }

            return $response;
        }

        return $next($request);
    }
}
