<?php

namespace App\Http\Middleware;

use Closure;

class RequestLoggerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Log::debug('[requests]', [
            'path' => $request->path(),
            'request' => $request->all()
        ]);

        return $next($request);
    }

    public function terminate($request, $response)
    {
        Log::debug($request->header('User-Agent'));
        Log::debug('[response]', [
            'path' => $request->path(),
            'response' => $response
        ]);
    }
}
