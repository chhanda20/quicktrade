<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$app->get('/', function () use ($app) {
    return $app->version();
});


$app->post('tests/exec', 'ExampleController@exec');
$app->post('tests/noti', 'ExampleController@pushnoti');

$app->post('users', 'UserController@addUser');
$app->post('admin/createUser', 'UserController@addUserByAdmin');
$app->put('admin/users/{userId}', 'UserController@updateUserByAdmin');

$app->post('users/login', 'UserController@login');
$app->post('users/resetPassword', 'UserController@resetPassword');
$app->post('users/updatePassword', 'UserController@updatePassword');
$app->get('users/getChannel/{userId}', 'UserController@getChannel');
$app->get('users/getChannelByContactId/{contactId}', 'UserController@getChannelByContactId');
$app->get('users/_me/roles', 'UserController@getMyRoles');
$app->get('users/_me/permissions', 'UserController@getMyPermissions');
$app->put('users/_me/updatePassword', 'UserController@updateMyPassword');
$app->post('users/logout', 'UserController@logout');
$app->post('users/doesEmailExist', 'UserController@doesEmailExist');
$app->post('users/acceptTermAndCondition', 'UserController@acceptTermAndCondition');
$app->get('users/event', 'UserController@getListEventCanConfig');
$app->put('users/updateEvent', 'UserController@configEventCanConfig');

$app->put('users/quickCode', 'UserController@setQuickCode');
$app->post('users/isCorrectQuickCode', 'UserController@isCorrectQuickCode');

$app->put('users/{id}', 'UserController@updateUser');
$app->get('users/{id}', 'UserController@getUserById');
$app->post('type_codes/_find', 'TypeCodeController@find');

$app->get('users/_me/config', 'UserController@getMyConfig');
$app->put('users/_me/config', 'UserController@setMyConfig');
$app->get('users/_me/locations', 'UserController@getMyLocations');


$app->post('users/search', 'UserController@getListUser');
$app->post('users/admin/search', 'UserController@getListUser');
$app->post('users/status', 'UserController@updateUserStatus');
$app->post('contacts', 'ContactController@create');
$app->post('contacts/_find', 'ContactController@find');
$app->post('contacts/_findCount', 'ContactController@findCount');
$app->get('contacts/_me', 'ContactController@getCurrentUserContact');
$app->put('contacts/_me', 'ContactController@updateCurrentUserContact');
$app->get('contacts/{id}', 'ContactController@getById');
$app->put('contacts/{id}', 'ContactController@update');

$app->post('contacts/admin/_find', 'ContactController@find');
$app->post('contacts/admin/_findCount', 'ContactController@findCount');

//todo: set permission
$app->post('contacts/{contact_id}/interaction', 'InteractionController@create');
$app->post('contacts/{contact_id}/interaction/_find', 'InteractionController@find');
$app->post('contacts/{contact_id}/interaction/_findCount', 'InteractionController@findCount');
$app->get('contacts/{contact_id}/interaction/{id}', 'InteractionController@getOne');
$app->put('contacts/{contact_id}/interaction/{id}', 'InteractionController@update');
$app->delete('contacts/{contact_id}/interaction/{id}', 'InteractionController@delete');

$app->post('valuations', 'ValuationController@create');
$app->post('valuations/_begin', 'ValuationController@begin');
$app->post('valuations/_find', 'ValuationController@find');
$app->post('valuations/_findCount', 'ValuationController@findCount');
$app->post('valuations/_cancel', 'ValuationController@cancel');
$app->get('testNotification/{userId}', 'TestController@testNotification');
$app->post('valuations/addInspectionItem', 'ValuationController@addInspectionItem');
$app->post('valuations/{valuationId}/{sectionType}/inspectionItemGroup', 'ValuationController@addInspectionItemGroup');
$app->put('valuations/{id}/updateVehicle', 'ValuationController@updateVehicle');
$app->post('valuations/{id}/version', 'ValuationController@createNewVersion');
$app->post('valuations/{id}/releaseVersion', 'ValuationController@releaseVersion');
$app->post('valuations/{id}/cancelModifyingVersion', 'ValuationController@cancelModifyingVersion');
$app->get('valuations/{id}/version', 'ValuationController@getAllVersion');
$app->get('valuations/{id}/version/{version}', 'ValuationController@getValuationVersion');
$app->get('valuations/{id}/version/{version}/inspectionItems', 'ValuationController@getInspectionItemOfOldVersion');


$app->put('valuations/updateGeneralReconditionCost/{valuationId}', 'ValuationController@updateGeneralReconditionCost');
$app->get('valuations/getGeneralReconditionCost/{valuationId}', 'ValuationController@getGeneralReconditionCost');

$app->post('valuations/{id}/_accept', 'ValuationController@accept');

$app->put('valuations/{id}/requestValuation', 'ValuationController@requestValuation');

$app->get('valuations/{id}/wholesaleValues', 'ValuationController@getWholesaleValues');
$app->put('valuations/{id}/wholesaleValues', 'ValuationController@updateWholesaleValues');

$app->get('valuations/{id}', 'ValuationController@getValuation');

$app->get('valuations/{id}/reconditioningCost', 'ValuationController@getReconditioningCost');
$app->put('valuations/{id}/reconditioningCost', 'ValuationController@updateReconditioningCost');

$app->get('valuations/{id}/salesInfo', 'ValuationController@getSalesInfo');
$app->put('valuations/{id}/salesInfo', 'ValuationController@updateSalesInfo');

$app->get('valuations/getInspectionItem/{valuationId}/{sectionGroup}', 'ValuationController@getInspectionItem');
$app->get('valuations/getInspectionItem/{valuationId}', 'ValuationController@getAllInspectionItem');

$app->get('valuations/getValuationDetail/{valuationId}', 'ValuationController@getValuationDetail');
$app->get('valuations/getVehicleDetail/{valuationId}', 'ValuationController@getVehicleDetail');

$app->put('valuations/{id}', 'ValuationController@update');
$app->put('valuations/{valuationId}/inspectionItem/{inspectionItemId}', 'ValuationController@updateInspectionItem');

$app->delete('valuations/{valuationId}/inspectionItem/{inspectionItemId}', 'ValuationController@deleteInspectionItem');
$app->delete('valuations/{valuationId}/{sectionType}/inspectionItemGroup', 'ValuationController@deleteInspectionItemGroup');

$app->get('valuations/{id}/mechanicalDetails', 'ValuationController@getMechanicalDetail');
$app->put('valuations/{id}/mechanicalDetails', 'ValuationController@updateMechanicalDetail');

$app->post('valuations/_findByCustomerId', 'ValuationController@findByCustomerId');
$app->post('valuations/_countByCustomerId', 'ValuationController@countByCustomerId');

$app->get('inspectionItems/{sectionType}', 'InspectionItemController@getAll');
$app->get('inspectionItems/{valuationId}/{inspectionItemId}', 'InspectionItemController@getItem');

$app->post('makes/_find', 'MakeController@find');

$app->post('vehicleModels/_find', 'VehicleModelController@find');

$app->post('vehicleStock/_find', 'VehicleController@stockFind');
$app->post('vehicleStock/_findCount', 'VehicleController@stockCount');

//$app->post('vehicleStock/_find', 'VehicleController@stockFind');
//$app->post('vehicleStock/_findCount', 'VehicleController@stockCount');

$app->get('vehicles/getVehicle/{vin}/{vehicleYear}', 'VehicleController@getVehicle');
$app->get('vehicles/getVehicleDiagram/{bodyType}', 'VehicleController@getVehicleDiagram');
$app->get('vehicles/{id}', 'VehicleController@getOne');
$app->get('vehicles/{id}/options', 'VehicleOptionController@getOptions');
$app->put('vehicles/{id}/options', 'VehicleOptionController@updateOptions');

$app->get('vehicles/{id}/extraOptions', 'VehicleOptionController@getAllExtraOption');
$app->post('vehicles/{id}/extraOptions', 'VehicleOptionController@createExtraOption');
$app->delete('vehicles/{vehicle_id}/extraOptions/{id}', 'VehicleOptionController@deleteExtraOption');
$app->delete('vehicles/{vehicle_id}/extraOptions', 'VehicleOptionController@deleteMultiExtraOption');

$app->get('vehicles/{vehicle_id}/images', 'VehicleImageController@getPrimary');
$app->post('vehicles/{vehicle_id}/images', 'VehicleImageController@create');
$app->get('vehicles/{vehicle_id}/images/{image_id}', 'VehicleImageController@get');
$app->put('vehicles/{vehicle_id}/images/{image_id}', 'VehicleImageController@update');

$app->post('manualVehicle/create', 'VehicleForTestDriveController@create');

$app->put('valuations/{id}/complete', 'ValuationController@complete');
$app->put('valuations/{id}/approve', 'ValuationController@approve');
$app->put('valuations/{id}/reprice', 'ValuationController@reprice');
$app->put('valuations/{id}/approveReprice', 'ValuationController@approveReprice');
$app->put('valuations/{id}/rejectReprice', 'ValuationController@rejectReprice');
$app->put('valuations/{id}/trade', 'ValuationController@trade');
$app->put('valuations/{id}/tradein', 'ValuationController@tradein');

$app->post('valuations/_sync', 'ValuationController@sync');//not used yet

$app->delete('valuations/{id}', 'ValuationController@delete');

$app->put('valuations/{id}/overallRating', 'ValuationController@updateOverallRating');
//some day it will be merged
$app->put('valuations/{id}/offerPrice', 'ValuationController@updateOverallRating');

$app->get('valuations/{id}/vehicle', 'ValuationController@getValuationVehicle');

$app->get('country', 'CountryController@getAll');
$app->post('country/getByCode', 'CountryController@getByCode');

//$app->get('channel', 'ChannelController@getAll');
$app->get('channel', 'ChannelController@getMyChannel');

$app->post('glasses/export', 'GlassDataController@export');
$app->get('glasses/info', 'GlassDataController@info');
$app->get('glasses/download/{file_name}', 'GlassDataController@download');
$app->post('glasses/code/{fieldName}', 'GlassModelController@findGlassCode');
$app->get('glasses/modelDetail/{glassCode}', 'GlassModelController@getModelDetailByGlassCode');

$app->get('glasses/optionList/{glassCode}', 'GlassModelController@getOptionList');
$app->get('glasses/transmissionType', 'GlassTransmissionTypeController@getTransmissionType');

$app->get('glassModel/getFamily/{makeName}', 'GlassModelController@getFamily');
$app->get('glassModel/getVariant/{makeName}/{family}', 'GlassModelController@getVariant');
$app->get('glassModel/getReleaseYear/{makeName}/{family}', 'GlassModelController@getReleaseYear');
$app->get('glassModel/getReleaseYear/{makeName}/{family}/{variant}', 'GlassModelController@getReleaseYear');
$app->get('glassModel/getReleaseMonth/{makeName}/{family}/{releaseYear}', 'GlassModelController@getReleaseMonth');
$app->get('glassModel/getReleaseMonth/{makeName}/{family}/{releaseYear}/{variant}', 'GlassModelController@getReleaseMonth');
$app->get('glassModel/getModelDetail/{valuationId}/{makeName}/{family}/{releaseYear}/{releaseMonth}', 'GlassModelController@getModelDetail');
$app->get('glassModel/getModelDetail/{valuationId}/{makeName}/{family}/{releaseYear}/{releaseMonth}/{variant}', 'GlassModelController@getModelDetail');

$app->post('ppsr', 'PpsrController@sendEmail');

$app->post('admin/login', 'UserController@adminLogin');

$app->post('company', 'CompanyController@createCompany');
$app->post('company/updateTermAndCondition', 'CompanyController@updateMoTermAndCondition');
$app->get('company/getTermAndCondition', 'CompanyController@getTermAndCondition');
$app->put('company/{id}', 'CompanyController@updateCompany');
$app->get('company/{id}', 'CompanyController@getCompany');
$app->post('company/{id}/setAdmin', 'CompanyController@createDefaultUser');//unused

$app->post('company/_find', 'CompanyController@find');
$app->post('company/_findCount', 'CompanyController@findCount');

$app->get('companyGroups', 'CompanyGroupController@getAll');// not used
$app->post('companyGroup', 'CompanyGroupController@create');
$app->post('companyGroup/_find', 'CompanyGroupController@find');
$app->post('companyGroup/_findCount', 'CompanyGroupController@findCount');
$app->put('companyGroup/{id}', 'CompanyGroupController@update');
$app->get('companyGroup/{id}', 'CompanyGroupController@getOne');
$app->get('companyGroup/{id}/roles', 'CompanyGroupController@getRoleByCompanyGroup');


$app->get('permission', 'PermissionController@getAll');
$app->get('permissionByGroup', 'PermissionController@getAllPermissionSortByGroup');
$app->get('permission/user/{userId}', 'PermissionController@getUserPermissions');
$app->put('permission/user/{userId}', 'PermissionController@setUserPermissions');
$app->put('permission/{id}','PermissionController@updatePermission');
$app->get('role', 'RoleController@getAll');
$app->post('role', 'RoleController@create');
$app->get('role/{role_id}', 'RoleController@getOne');
$app->put('role/{role_id}', 'RoleController@update');
$app->get('role/{role_id}/permissions', 'RoleController@getRolePermissions');
$app->delete('role/{role_id}', 'RoleController@delete');
$app->post('role/search', 'RoleController@searchRolesList');


$app->post('location', 'LocationController@create');

$app->post('location/_find', 'LocationController@find');
$app->post('location/_findCount', 'LocationController@findCount');

$app->get('location', 'LocationController@getListLocationAccess');
$app->post('location/all', 'LocationController@getListAll');
$app->get('location/{id}', 'LocationController@getOne');
$app->put('location/{id}', 'LocationController@update');

$app->post('externalGroup', 'ExternalGroupController@create');
$app->get('externalGroup/{id}','ExternalGroupController@getExternalGroupById');
$app->put('externalGroup/{id}', 'ExternalGroupController@updateExternalGroup');
$app->delete('externalGroup/{id}', 'ExternalGroupController@deleteExternalGroup');


$app->get('appProperty', 'AppPropertyController@getServerProperty');

$app->put('appProperty/externalOffer', 'AppPropertyController@updateExternalOffer');
$app->get('appProperty/externalOffer', 'AppPropertyController@getExternalOffer');

$app->put('appProperty/valuationTimeout', 'AppPropertyController@updateValuationTimeout');
$app->get('appProperty/valuationTimeout', 'AppPropertyController@getValuationTimeout');

$app->put('appProperty/optionSetting', 'AppPropertyController@updateOptionSetting');
$app->get('appProperty/optionSetting', 'AppPropertyController@getOptionSetting');
$app->post('appProperty/getSetting', 'AppPropertyController@getSetting');



$app->put('appProperty/updateSpeedySetting', 'AppPropertyController@updateSpeedySetting');
$app->get('appProperty/speedySetting', 'AppPropertyController@getSpeedySetting');

$app->put('appProperty/update', 'AppPropertyController@updateAppProperty');
$app->post('appProperty/get', 'AppPropertyController@getAppProperty');

$app->post('externalOffer', 'ExternalOfferController@add');
$app->post('externalOffer/outgoing', 'ExternalOfferController@getOutgoing');
$app->post('externalOffer/bid', 'ExternalOfferController@bid');
$app->post('externalOffer/accept', 'ExternalOfferController@accept');
$app->post('externalOffer/reject', 'ExternalOfferController@reject');
$app->post('externalOffer/search', 'ExternalOfferController@searchExternalOfferGroup');

$app->get('externalOffer/group', 'ExternalOfferController@getGroup');
$app->get('externalOffer/{valuationId}', 'ExternalOfferController@getOffer');
$app->get('externalOffer/getById/{id}', 'ExternalOfferController@getById');

$app->post('externalOffer/inComing', 'ExternalOfferController@getInComing');
$app->post('externalOffer/blockVehicle', 'ExternalOfferController@blockVehicle');
$app->post('externalOffer/setLocation', 'ExternalOfferController@setLocation');
$app->post('externalOffer/setVehicleReadyToShip', 'ExternalOfferController@setVehicleReadyToShip');
$app->post('externalOffer/setTransportTriggered', 'ExternalOfferController@setTransportTriggered');
$app->post('externalOffer/setShipped', 'ExternalOfferController@setShipped');
$app->post('externalOffer/setDelivered', 'ExternalOfferController@setDelivered');
$app->post('externalOffer/offerWon', 'ExternalOfferController@getOfferWon');
$app->post('externalOffer/purchaseHistory', 'ExternalOfferController@getPurchaseHistory');
$app->post('externalOffer/soldHistory', 'ExternalOfferController@getSoldHistory');

$app->post('dealer/search','CompanyController@searchDealerLocation');

$app->post('testDrives/create', 'TestDriveController@create');
$app->get('testDrives', 'TestDriveController@getListTestDrive');
$app->post('testDrives/search', 'TestDriveController@searchTestDrive');
$app->post('testDrives/searchCount', 'TestDriveController@searchTestDriveCount');
$app->put('testDrives/complete/{testDriveId}', 'TestDriveController@completeTestDrive');
$app->get('testDrives/{id}', 'TestDriveController@getById');


$app->post('testDrives/vehicle/_find', 'VehicleController@testDrivesFind');
$app->post('testDrives/vehicle/_findCount', 'VehicleController@testDrivesCount');

//API: Auction Session
$app->post('auctions/session', 'AuctionSessionController@createAuctionSession');
$app->post('auctions/session/list', 'AuctionSessionController@getAuctionSession');
$app->put('auctions/session/{sessionId}', 'AuctionSessionController@updateAuctionSession');
$app->delete('auctions/session/{sessionId}', 'AuctionSessionController@deleteAuctionSession');
$app->get('auctions/session/{sessionId}', 'AuctionSessionController@getAuctionSessionById');
$app->get('auctions/active', 'AuctionSessionController@getActiveSession');
$app->get('auctions/nextSession', 'AuctionSessionController@getNextSession');
$app->post('auctions/session/{sessionId}/_findVehicle', 'AuctionSessionController@findVehicleInSession');
$app->post('auctions/session/{sessionId}/_countVehicle', 'AuctionSessionController@countVehicleInSession');
$app->post('auctions/session/{sessionId}/history', 'AuctionSessionController@history');

$app->post('auctions/session/{sessionId}/item/{itemId}/bid', 'BidController@bid');
$app->post('auctions/autoBid', 'BidController@autoBid');//call locally

//API: Auction
$app->post('auctions/sendToAuction', 'AuctionController@submitAuction');
$app->put('auctions/editAuctionListing/{auctionItemId}','AuctionController@editAuctionListing');
$app->post('auctions/search','AuctionController@searchByListType');
$app->get('auctions/searchItem/{auction_item}','AuctionController@searchItem');

$app->post('auctions/watchList','AuctionController@addWatchList');
$app->delete('auctions/watchList','AuctionController@removeWatchList');
 
$app->post('auctions/requestList','AuctionController@getRequestList');

$app->post('auctions/request/approval','AuctionController@approveOrRejectRequestAuction');

$app->post('auctions/recentView','AuctionController@addRecentView');

$app->post('auctions/trading/buyNow','AuctionController@buyNow');

//API: Auction Setting
$app->put('auctions/settings','AppPropertyController@changeAuctionSettings');
$app->get('auctions/settings','AppPropertyController@getAuctionSettings');

$app->put('auctions/session/{session_id}/items/{item_id}/userSetting', 'BidController@updateUserSetting');
$app->get('auctions/session/{session_id}/items/{item_id}/userSetting', 'BidController@getUserSetting');
$app->put('auctions/{auctionId}','AuctionController@updateAuctionItem');
$app->post('auctions/request/cancel','AuctionController@cancelAuctionRequest');
$app->get('auctions/{auctionId}','AuctionController@getAuctionById');
$app->post('auctions/resend','AuctionController@resendAuction');

//API: Wish List Management
$app->post('wishLists','WishListController@createWishList');
$app->post('wishLists/search','WishListController@searchWishList');
$app->get('wishLists/{id}','WishListController@getWishListById');
$app->put('wishLists/{id}','WishListController@updateWishList');

$app->put('transaction/_paymentReceived', 'TransactionController@paymentReceived');
$app->post('transaction/_find','TransactionController@find');
$app->post('transaction/admin/_find','TransactionController@find');
$app->post('transaction/_findCount','TransactionController@findCount');
$app->post('transaction/admin/_findCount','TransactionController@findCount');
//$app->post('transaction/{id}/dispute/item','TransactionController@addItem');
$app->get('transaction/{id}','TransactionController@getOne');
$app->put('transaction/{id}/inspect','TransactionController@doInspect');
$app->post('transaction/{id}/dispute','TransactionController@createDispute');
$app->get('transaction/{id}/dispute','TransactionController@getAllDispute');
$app->get('transaction/{id}/dispute/{dispute_id}/items','TransactionController@getAllDisputeItem');
$app->get('transaction/{id}/dispute/{dispute_id}/availableItems','TransactionController@getAllAvailableDisputeItem');
$app->put('transaction/{id}/accept', 'TransactionController@accept');
$app->put('transaction/{id}/readyForTransport', 'TransactionController@readyForTransport');
$app->put('transaction/{id}/transportBooked', 'TransactionController@transportBooked');
$app->put('transaction/{id}/inTransit', 'TransactionController@inTransit');
$app->put('transaction/{id}/shipped', 'TransactionController@shipped');
$app->put('transaction/{id}/setStatusReturning', 'TransactionController@setStatusReturning');
$app->put('transaction/{id}/setStatusReturned', 'TransactionController@setStatusReturned');
$app->put('transaction/{id}/setStatusDocumenting', 'TransactionController@setStatusDocumenting');
$app->put('transaction/{id}/changeStatus', 'TransactionController@setTransactionStatus');
$app->put('transaction/{id}/receiveSellerPayment','TransactionController@receiveSellerPayment');
$app->post('transaction/{id}/documents','TransactionController@documents');

$app->post('dispute/_findCompanyDispute','DisputeController@findCompanyDispute');
$app->post('dispute/_countCompanyDispute','DisputeController@countCompanyDispute');

$app->post('dispute/_find','DisputeController@find');
$app->post('dispute/_findCount','DisputeController@findCount');

$app->get('dispute/{id}','DisputeController@getOne');
$app->put('dispute/{id}/resolve','DisputeController@resolve');
$app->put('dispute/{id}/accept','DisputeController@accept');
$app->put('dispute/{id}/underReview','DisputeController@setUnderReview');

$app->post('dispute/{id}/note','DisputeController@createNote');
$app->delete('dispute/{id}/note/{note_id}','DisputeController@deleteNote');
$app->get('dispute/{id}/note','DisputeController@getAllNote');

//API: Menu Permission
$app->get('menu','MenuController@getListMenu');
$app->post('menu/setRole','MenuController@setMenuForRole');
$app->get('role/{roleId}/menu/','MenuController@getRoleMenu');

//API: Accounting
$app->post('accounting/{transactionId}/documents','AccountingController@uploadDocument');
$app->get('accounting/{transactionId}/documents','AccountingController@getDocument');


//API: Data Access Management (Location)
$app->post('dataAccess','DataAccessController@setDataAccessForUser');
$app->get('dataAccess/{userId}','DataAccessController@getDataAccessByUser');
$app->put('dataAccess/{userId}','DataAccessController@updateDataAccessByUser');


$app->post('notifications','NotificationMessageController@getNotificationByUser');
$app->put('appProperty/transactionFees','AppPropertyController@changeTransactionFees');
$app->get('appProperty/transactionFees', 'AppPropertyController@getTransactionFees');
$app->get('regions', 'RegionController@getRegionLists');


//todo
$app->get('dashboard','DashboardController@getListDashBoard');
$app->get('dashboard/locations','DashboardController@getAccessableLocations');
$app->post('dashboard','DashboardController@getDashboardDetail');

//test
$app->get('test','TestController@test');
$app->get('test/noti','TestController@sendTest');
$app->get('test/export','TestController@export');
$app->get('test/sendTest/{user_id}','TestController@sendTest');
$app->get('test/pdf','TestController@pdf');
$app->get('test/notice/{userId}','TestController@notice');