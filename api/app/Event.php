<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Event extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'event';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public function getByCode($code)
    {
        $sql = '
            SELECT c.name, c.type, c.id
            FROM `event` e
                INNER JOIN channel_event ce ON ce.event_id = e.id
                INNER JOIN channel c ON c.id = ce.channel_id
            WHERE e.code = :code';
        $params = ['code' => $code];
        $data = DB::select($sql,$params);
        return $data;
    }
}
