<?php

namespace App;


use App\Libraries\Event as NotiEvent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class NotificationMessage extends Backend
{

    const IS_READ = 1;//true
    const NOT_READ = 0;//false

    protected $table = 'notification_message';

    public static function getListNotificationMessage($userId, $roles)
    {

        $paramTypeUser = NotiEvent::PARAMS_TYPE_USER;
        $paramTypeLocation = NotiEvent::PARAMS_TYPE_LOCATION;
        $listNotification = DB::select("
            select * from (
                select *
                from notification_message
                where param = $userId and param_type ='$paramTypeUser'

                UNION

                select notification_message.*
                from notification_message JOIN `user` ON notification_message.param = `user`.location_id
                    AND param_type = '$paramTypeLocation'
                    AND notification_message.channel_name IN ($roles)
                where `user`.id = $userId) a
            ORDER BY create_date DESC limit 20");
        foreach ($listNotification as $notification ){
            $notification->custom_data = json_decode($notification->custom_data);
            DB::statement("UPDATE notification_message SET is_read=TRUE WHERE id = $notification->id");
        }
        return $listNotification;
    }


}
