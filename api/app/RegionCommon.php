<?php

namespace App;


use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class RegionCommon extends Backend
{

    public static function getRegionByPostcode($postcode){
        $result = DB::select("SELECT * FROM region_postcode where postcode_from <= $postcode AND $postcode <= postcode_to");
        if($result)
            return $result[0]->region_id;
        else
            return null;
    }

    public static function getRegionNameByPostcode($postcode){
        $result = DB::select("SELECT region.* FROM region 
                              JOIN region_postcode on region_postcode.region_id = region.id
                              where postcode_from <= $postcode AND $postcode <= postcode_to");
        if($result)
            return $result[0]->region_name;
        else
            return null;
    }

    public static function updateTransportPrice($transportPrices){
        if(sizeof($transportPrices) > 0){
            //delete all from transport price
            DB::statement("delete from transport_price");
            //insert new
            $dataSet = [];
            foreach ($transportPrices as $transportPrice) {

                $dataSet[] = [
                    'from_region'  => $transportPrice['from_region'],
                    'to_region'    => $transportPrice['to_region'],
                    'price'       => $transportPrice['price']
                ];
            }

            DB::table('transport_price')->insert($dataSet);
        }
    }
}
