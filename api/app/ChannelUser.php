<?php

namespace App;

use Illuminate\Support\Facades\DB;

class ChannelUser extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'channel_user';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public function updateChannelUser($id, $listRole)
    {
        //delete old channel,except user type
        $deleteChannelUserQuery = "delete T FROM channel_user T
                                    INNER JOIN channel ON T.channel_id = channel.id
                                    WHERE
                                      channel.type = 'role'
                                      AND T.user_id =$id
                                    ";

        $status = \DB::delete($deleteChannelUserQuery);

        //add new channel to user
        $channel = new Channel();

        $channelInfos = $channel->getByTypeAndRole('',$listRole);

        if($channelInfos){

            $prepareDataForInsert = array();
            foreach ($channelInfos as $info){
                $temp = array('user_id'=>$id, 'channel_id'=> $info->id);
                array_push($prepareDataForInsert,$temp);

            }
            $this->insert($prepareDataForInsert);
        }
    }


}
