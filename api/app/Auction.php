<?php

namespace App;


use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use stdClass;


class Auction extends Backend
{

    const STATUS_CODE_INACTIVE = 0;
    const STATUS_CODE_PENDING = 1;
    const STATUS_CODE_APPROVE = 2;
    const STATUS_CODE_REJECT = 3;
    const STATUS_CODE_BUY_OUT = 4;
    const STATUS_CODE_CANCEL = 5;

    const SOURCE_LIVE       = 'live_auction';
    const SOURCE_BUY_NOW    = 'auction_buy_now';

    protected $table = 'auction_item';

    public static function getAll($input, $userId, $where, $userLocationAccess)
    {
        $limit = $input->get('limit');
        $offset = $input->get('offset');
        $type = $input->get('viewType');
        $currentDate = date("Y/m/d H:i:s");

        $order = "";
        if ($input->has("orderBy")) {
            $order = " ORDER BY " . $input->get("orderBy");
            if ($input->has("orderDesc") && $input->get("orderDesc")) {
                $order .= ' DESC ';
            }
        }

        $query = "";
        $paramReturn = "
                lo.seller_rating, 
                auc.id,
                auc.creator_id,
                veh.trade_in_location,
                auc.start_price,
                auc.buy_now_price, auc.status, auc.valuation_id, auc.comment,
                veh.odometer,
                val.overall_rating,                
                lo.name AS location_text,
                lo.postcode,
                CONCAT_WS(' ',veh.make,veh.model_family,veh.series,veh.style,veh.engine_type,
					veh.engine_capacity,veh.engine_size,veh.variant,veh.cylinder,veh.transmission) AS 'vehicle_name'
                , veh.id AS vehicle_id,
                 sesItem.auction_session_id AS auction_session_id,DATE(startDate) session_date, DATE_FORMAT(startDate,'%H:%i') session_time,
                 w.auction_item_id IS NOT NULL AS watched,0 as delivery_cost";

        $joinTables ="auction_item AS auc
                            INNER JOIN valuation AS val ON auc.valuation_id = val.id
                            INNER JOIN vehicle AS veh ON val.vehicle_id = veh.id
                            LEFT JOIN location lo ON lo.id = veh.trade_in_location
                            INNER JOIN session_has_item sesItem ON sesItem.auction_item_id =  auc.id
                            INNER JOIN auction_session ses ON ses.id = sesItem.auction_session_id";



//            INNER JOIN `user` ON val.user_id = `user`.id
//            INNER JOIN contact ON `user`.contact_id = contact.id


        if ($type) {
            if ($type == 'watching') {
                $status = Auction::STATUS_CODE_APPROVE;
                $auction = DB::select("
                        select SQL_CALC_FOUND_ROWS t.*, type.`value` AS rating from
                        (SELECT $paramReturn                                                        
                            FROM
                            $joinTables
                            INNER JOIN auction_watch_list w ON auc.id = w.auction_item_id and w.user_id = $userId  
                            WHERE auc.`status` = $status AND sesItem.status = $status AND ses.startDate >= '$currentDate' 
                            AND veh.trade_in_location NOT IN ($userLocationAccess)
                              $query   $where
                        ) as t
                        LEFT JOIN type_code type ON type.code = t.overall_rating AND type.type = 'OVERALL_RATING'
                        $order
                        LIMIT $offset, $limit 
                         ");


                $auctionListSize = DB::select("select FOUND_ROWS() as count");

                $result = array($auctionListSize[0], "auctionList" => $auction);
                return $result;
            } else if ($type == 'recent_view') {
                $status = Auction::STATUS_CODE_APPROVE;
                $auction = DB::select("
                        select SQL_CALC_FOUND_ROWS t.*, type.`value` AS rating from
                        (SELECT $paramReturn, rec.view_date                                                        
                            FROM
                            $joinTables
                            INNER JOIN auction_recent_view rec ON auc.id = rec.auction_item_id and rec.user_id = $userId
                            LEFT JOIN auction_watch_list w ON auc.id = w.auction_item_id and w.user_id = $userId  
                            WHERE auc.`status` = $status AND sesItem.status = $status AND ses.startDate >= '$currentDate'
                            AND veh.trade_in_location NOT IN ($userLocationAccess)
                              $query   $where
                              
                        ) as t
                        LEFT JOIN type_code type ON type.code = t.overall_rating AND type.type = 'OVERALL_RATING'
                        ORDER BY t.view_date DESC 
                        LIMIT $offset, $limit 
                         ");

                $auctionListSize = DB::select("select FOUND_ROWS() as count");
                $result = array($auctionListSize[0], "auctionList" => $auction);
                return $result;

            } else if ($type == 'selling' || $type == 'submitted') {
                if ($type == 'selling')
                    $status = Auction::STATUS_CODE_APPROVE;
                else
                    $status = Auction::STATUS_CODE_PENDING . "," . Auction::STATUS_CODE_REJECT . "," . Auction::STATUS_CODE_APPROVE;

                //search list vehicle base on auction status
                $testDriveInProgressStatus = TestDrive::STATUS_CODE_IN_PROGRESS;
                $auction = DB::select("
                        select SQL_CALC_FOUND_ROWS t.*, type.`value` AS rating,t1.value as reject_reason from
                        (SELECT $paramReturn    , reject_code                                                    
                            FROM
                            $joinTables
                            INNER JOIN `user` ON val.user_id = `user`.id AND  val.company_group_id = `user`.company_group_id
                            LEFT JOIN auction_watch_list w ON auc.id = w.auction_item_id and w.user_id = $userId                            
                            WHERE  sesItem.status IN ($status) AND ses.startDate >= '$currentDate'
                            AND veh.trade_in_location IN ($userLocationAccess)
                            AND veh.id NOT IN (SELECT vehicle_id FROM `test_drive` WHERE status = $testDriveInProgressStatus)
                              $query   $where
                        ) as t
                        LEFT JOIN type_code type ON type.code = t.overall_rating AND type.type = 'OVERALL_RATING'
                        LEFT JOIN type_code AS t1 ON t.reject_code = t1.`code` AND t1.type = 'AUCTION_REJECT_REASON'
                        $order
                        LIMIT $offset, $limit 
                         ");

                $auctionListSize = DB::select("select FOUND_ROWS() as count");
                $result = array($auctionListSize[0], "auctionList" => $auction);
                return $result;
            } else if ($type == 'wish_list') {
                // get all wish list of current user
                $user = User::find($userId);
                $wishListArr = WishList::getByDealerLocationId($user->location_id);
                $status = Auction::STATUS_CODE_PENDING . "," . Auction::STATUS_CODE_REJECT . "," . Auction::STATUS_CODE_APPROVE;

                $listAuction = array();
                foreach ($wishListArr as $k => $wishList) {
                    // loop through wish list

                    $location = $wishList->locationId;

                    if ($location) {
                        $where .= " AND veh.trade_in_location IN ($location) ";
                    }

                    if ($wishList->odometer_start != null && $wishList->odometer_end != null) {
                        $where .= " AND (veh.odometer between $wishList->odometer_start AND $wishList->odometer_end)";
                    }

                    if ($wishList->year_start != null && $wishList->year_end != null) {
                        $where .= " AND (year(veh.build_date) between $wishList->year_start AND $wishList->year_end)";
                    }

                    $transmission = collect();

                    $transmissionArr = json_decode($wishList->transmission, true);
                    if ($transmissionArr) {
                        foreach ($transmissionArr as $type) {
                            $x = str_replace('\'', '"', $type);
                            $x = trim($x);
                            if ($x) {
                                $transmission->push("'" . $x . "'");
                            }
                        }

                        $transmission = $transmission->unique()->values()->all();

                        $transmission = implode(",", $transmission);
                        if ($transmission) {
                            $where .= " AND (veh.transmission IN ($transmission))";
                        }
                    }


                    $make = collect();
                    $model = collect();
                    $variant = collect();

                    if ($wishList->makeInfo) {
                        $arr = json_decode($wishList->makeInfo, true);
                        if ($arr) {
                            foreach ($arr as $i) {

                                $i = (object)$i;
                                if (property_exists($i, "make")) {
                                    $i->make = str_replace('\'', '"', $i->make);
                                    $i->make = trim($i->make);
                                } else
                                    $i->make = "";
                                if (property_exists($i, "model")) {
                                    $i->model = str_replace('\'', '"', $i->model);
                                    $i->model = trim($i->model);
                                } else
                                    $i->model = "";


                                if (property_exists($i, "variant")) {
                                    $i->variant = str_replace('\'', '"', $i->variant);
                                    $i->variant = trim($i->variant);
                                } else {
                                    $i->variant = "";
                                }
                                if ($i->make) {
                                    $make->push("'" . $i->make . "'");
                                }
                                if ($i->model) {
                                    $model->push("'" . $i->model . "'");
                                }
                                if ($i->variant) {
                                    $variant->push("'" . $i->variant . "'");
                                }
                            }

                            $make = $make->unique()->values()->all();
                            $model = $model->unique()->values()->all();
                            $variant = $variant->unique()->values()->all();

                            if (sizeof($make) > 0) {
                                $make = implode(",", $make);
                                $where .= " AND (make IN ($make))";
                            }
                            if (sizeof($model) > 0) {
                                $model = implode(",", $model);
                                $where .= " AND (model_family IN ($model))";
                            }
                            if (sizeof($variant) > 0) {
                                $variant = implode(",", $variant);
                                $where .= " AND (variant IN ($variant))";
                            }
                        }
                    }

                    if ($input->has("query")) {
                        $text = $input->get('query');
                        $text = str_replace('\'', '"', $text); // for safety reasons;
                        if ($text) {
                            $query = "(YEAR(build_date) LIKE '$text*')";
                            $query .= "OR MATCH(make, family, series, style, engine_type, engine_size, variant, cylinder, transmission, registration_number, body_type) AGAINST ('$text*' IN BOOLEAN MODE)";
                            $where .= " AND ($query) ";
                        }
                    }
                    if ($wishList->price_start != null && $wishList->price_end != null)
                        $where .= " AND ((auc.start_price between $wishList->price_start AND $wishList->price_end) OR (auc.buy_now_price between $wishList->price_start AND $wishList->price_end))";


                    $auction = DB::select("
                        select SQL_CALC_FOUND_ROWS t.*, type.`value` AS rating,t1.value as reject_reason from
                        (SELECT $paramReturn    , reject_code
                            FROM
                            $joinTables
                            INNER JOIN `user` ON val.user_id = `user`.id AND  val.company_group_id = `user`.company_group_id
                            LEFT JOIN auction_watch_list w ON auc.id = w.auction_item_id and w.user_id = $userId
                            WHERE  sesItem.status IN ($status) AND ses.startDate >= '$currentDate'
                            $where
                            LIMIT $offset, $limit 
                        ) as t
                        LEFT JOIN type_code type ON type.code = t.overall_rating AND type.type = 'OVERALL_RATING'
                        LEFT JOIN type_code AS t1 ON t.reject_code = t1.`code` AND t1.type = 'AUCTION_REJECT_REASON'
                        $order                        
                         ");


                    foreach ($auction as $t) {
                        array_push($listAuction, $t);
                    }

                }
                //remove duplicate vehicle
                $listAuction = array_map("unserialize", array_unique(array_map("serialize", $listAuction)));

                //this is for sorting,working with sort one field
//                usort($listAuction, function ($a, $b) {
//                    return strcmp($a->id, $b->id);
//                });

                $result = array(array("count" => count($listAuction)), "auctionList" => $listAuction);
                return $result;
            }
        }
        $status = Auction::STATUS_CODE_APPROVE;

        $auction = DB::select("
            select t.*, type.`value` AS rating from
                (SELECT $paramReturn                                        
                    FROM
                    $joinTables
                    LEFT JOIN auction_watch_list w ON auc.id = w.auction_item_id and w.user_id = $userId 
                    WHERE auc.`status` = $status AND sesItem.status = $status AND ses.startDate >= '$currentDate'
                    AND veh.trade_in_location NOT IN ($userLocationAccess)
                      $query   $where
                ) as t
             LEFT JOIN type_code type ON type.code = t.overall_rating AND type.type = 'OVERALL_RATING'
             $order
             LIMIT $offset, $limit 
             ");


        $auctionListSize = DB::select("
            select count(*) as count 
            FROM
                $joinTables
                LEFT JOIN auction_watch_list w ON auc.id = w.auction_item_id and w.user_id = $userId                     
                    WHERE auc.`status` = $status AND sesItem.status = $status AND ses.startDate >= '$currentDate'
                    AND veh.trade_in_location NOT IN ($userLocationAccess)
                $query      $where             
            ");

        $result = array($auctionListSize[0], "auctionList" => $auction);
        return $result;

    }

    public static function validateDuplicateSessionTime($startDate, $endDate)
    {
        return DB::select("SELECT * from auction_session where 
                              (startDate <= '$startDate' AND endDate > '$startDate')
                               OR (startDate < '$endDate' AND endDate >= '$endDate')");
    }

    public static function linkAuctionToSession($sessionId, $auctionItemId, $startPrice)
    {
        $status = Auction::STATUS_CODE_PENDING;
        return DB::statement("Insert into session_has_item(`auction_session_id`,`auction_item_id`,`status`,`bid_price`) VALUES ($sessionId,$auctionItemId,$status,$startPrice)");
    }


    public static function addToWatchList($auctionId, $userId)
    {
        $currentDate = date("Y/m/d H:i:s");
        try {
            $result = DB::statement("Insert into auction_watch_list(auction_item_id,user_id,create_date,update_date) VALUES ($auctionId,$userId,'$currentDate','$currentDate')");
            return $result;
        } catch (\Exception $ex) {
            Log::error("Error in add auction to Watch List " . $ex->getMessage());
            return 0;
        }

    }

    public static function removeFromWatchList($auctionItemId, $userId)
    {
        return DB::statement("delete from auction_watch_list where auction_item_id = $auctionItemId AND user_id = $userId");
    }

    public static function getRequestList($input, $where)
    {
        try {
            $status = Auction::STATUS_CODE_PENDING;
            $limit = $input->get('limit');
            $offset = $input->get('offset');
            $currentDate = date("Y/m/d H:i:s");
            $order = "";
            if ($input->has("orderBy")) {
                $order = "ORDER BY " . $input->get("orderBy");
                if ($input->has("orderDesc") && $input->get("orderDesc")) {
                    $order .= ' DESC ';
                }
            }


            $query = "";
            if ($input->has("query")) {
                $str = $input->get("query");
                $query = " where (t.vehicle_name LIKE '%$str%' OR c.company_name LIKE '%$str%' OR t.location_text LIKE '%$str%') ";
            }
            $auction = DB::select("
                select SQL_CALC_FOUND_ROWS t.*,c.company_name AS 'dealer_name',type.`value` AS rating from
                    (SELECT auc.id,
                                    auc.start_price,
                                    auc.buy_now_price,
                                    veh.odometer,
                                    val.overall_rating,
                                    val.id valuation_id,
                                    contact.state as location, lo.name AS location_text, sesItem.auction_session_id AS session_id,
                                    
                                    CONCAT_WS(' ',veh.make,veh.model_family,veh.series,veh.style,veh.engine_type,
					                    veh.engine_capacity,veh.engine_size,veh.variant,veh.cylinder,veh.transmission) AS 'vehicle_name'
                                    ,auc.location_id as dealer_location, veh.id AS vehicle_id
                                    
                                    FROM
                                    auction_item AS auc
                                    INNER JOIN valuation AS val ON auc.valuation_id = val.id
                                    INNER JOIN vehicle AS veh ON val.vehicle_id = veh.id
                                    INNER JOIN `user` ON val.user_id = `user`.id
                                    INNER JOIN contact ON `user`.contact_id = contact.id
                                    LEFT JOIN country_state state ON state.id = contact.state
                                    INNER JOIN session_has_item sesItem ON sesItem.auction_item_id =  auc.id
                                    INNER JOIN auction_session ses ON ses.id = sesItem.auction_session_id
                                    LEFT JOIN location lo ON lo.id = veh.trade_in_location
                                    WHERE auc.`status` = $status AND sesItem.status = $status
                                    $where
                                    ) as t
                    inner join location l on l.id = t.dealer_location
                    inner join company com on com.id = l.company_id
                    INNER join contact c on c.id = com.contact_id  
                    LEFT JOIN type_code type ON type.code = t.overall_rating AND type.type = 'OVERALL_RATING' 
                      $query
                      $order
                LIMIT $offset, $limit  ");


            $auctionListSize = DB::select("select FOUND_ROWS() as count");
            if ($auction) {
                $result = array($auctionListSize[0], "requestList" => $auction);
                return $result;
            }
        } catch (Exception $e) {
            Log::error("Error in getRequestList " . $e->getMessage());
            return NULL;
        }
        return NULL;
    }


    public static function updateAuctionItemToSession($auctionItemId, $sessionId, $status)
    {
        $pendingStatus = Auction::STATUS_CODE_PENDING;

        return DB::statement("UPDATE session_has_item Set auction_session_id = $sessionId, status = $status 
                                WHERE auction_item_id = $auctionItemId and status = $pendingStatus");
    }

    public static function addToRecentList($auctionItemId, $userId)
    {
        $currentDate = date("Y/m/d H:i:s");
        try {
            DB::statement("delete from auction_recent_view where auction_item_id = $auctionItemId AND user_id = $userId");
            $result = DB::statement("Insert into auction_recent_view(auction_item_id,user_id,view_date) VALUES ($auctionItemId,$userId,'$currentDate')");
            return $result;
        } catch (\Exception $ex) {
            Log::error("Error in add auction to Watch List " . $ex->getMessage());
            return 0;
        }
    }

    public static function checkItemInLiveAuction($auctionItemId)
    {
        $currentDate = date("Y/m/d H:i:s");
        try {
            $result = DB::select("SELECT
                                        auc.id,
                                        auc.valuation_id,ses.startDate
                                        FROM
                                        auction_item AS auc
                                        
                                        INNER JOIN session_has_item ON auc.id = session_has_item.auction_item_id 
                                        INNER JOIN auction_session AS ses ON ses.id = session_has_item.auction_session_id
                                        WHERE 
                                        auc.id = $auctionItemId and
                                        ses.startDate <= '$currentDate' and ses.endDate >= '$currentDate'");
            return $result;
        } catch (\Exception $ex) {
            Log::error("Error in add auction to Watch List " . $ex->getMessage());
            return NULL;
        }
    }

    public static function updateAuctionItemStatus($auctionItemId, $status_code)
    {
        try {
            $approveStatus = Auction::STATUS_CODE_APPROVE;
            DB::statement("UPDATE session_has_item SET status = $status_code WHERE auction_item_id = $auctionItemId AND status = $approveStatus");
            return DB::statement("UPDATE auction_item SET status = $status_code WHERE id = $auctionItemId");
        } catch (\Exception $ex) {
            Log::error("Error in updateAuctionItemStatus " . $ex->getMessage());
            return NULL;
        }
    }

    public static function isValuationCanSubmit($valuationId) {
        $valuation = Valuation::find($valuationId);
        return $valuation ? $valuation->status == Valuation::STATUS_COMPLETE_TRADED_IN : false;
    }

    public static function removeFromAuction($auctionItemId)
    {
        try {
            DB::statement("DELETE From session_has_item WHERE auction_item_id = $auctionItemId");
            $result = DB::statement("DELETE From auction_item WHERE id = $auctionItemId");

            return $result;

        } catch (\Exception $ex) {
            Log::error("Error in removeFromAuction " . $ex->getMessage());
            return NULL;
        }
    }

    public static function getAuctionById($auctionId, $userId)
    {
        try {
            $currentDate = date("Y/m/d H:i:s");
            $paramReturn = "auc.id,
                auc.start_price,
                auc.buy_now_price, auc.status, auc.valuation_id, auc.comment,
                auc.creator_id,
                veh.odometer,
                val.overall_rating, val.stock_number, datediff('$currentDate', veh.trade_in) day_in_stock,
                contact.state as location, lo.name AS location_text,
                CONCAT_WS(' ',veh.make,veh.model_family,veh.series,veh.style,veh.engine_type,
					veh.engine_capacity,veh.engine_size,veh.variant,veh.cylinder,veh.transmission) AS 'vehicle_name'
                , veh.id AS vehicle_id,veh.registration_number,val.trade_price,
                 sesItem.auction_session_id AS auction_session_id,DATE(startDate) session_date, DATE_FORMAT(startDate,'%H:%i') session_time,
                 w.auction_item_id IS NOT NULL AS watched,0 as delivery_cost";

            $auction = DB::select("
                        select t.*, type.`value` AS rating from
                        (SELECT $paramReturn                                                        
                            FROM
                            auction_item AS auc
                            INNER JOIN valuation AS val ON auc.valuation_id = val.id
                            INNER JOIN vehicle AS veh ON val.vehicle_id = veh.id
                            INNER JOIN `user` ON val.user_id = `user`.id
                            INNER JOIN contact ON `user`.contact_id = contact.id
                            LEFT JOIN auction_watch_list w ON auc.id = w.auction_item_id and w.user_id = $userId  
                            LEFT JOIN country_state state ON state.id = contact.state
                            INNER JOIN session_has_item sesItem ON sesItem.auction_item_id =  auc.id
                            INNER JOIN auction_session ses ON ses.id = sesItem.auction_session_id
                            LEFT JOIN location lo ON lo.id = veh.trade_in_location
                            WHERE auc.id = $auctionId                            
                        ) as t
                        LEFT JOIN type_code type ON type.code = t.overall_rating AND type.type = 'OVERALL_RATING'

                         ");

            return $auction;

        } catch (\Exception $ex) {
            Log::error("Error in getAuctionById " . $ex->getMessage());
            return NULL;
        }
    }

    static function historyQuery($sessionId) {
        $query = Auction::join('session_has_item', 'session_has_item.auction_item_id', 'auction_item.id')
            ->join('auction_session', 'auction_session.id', 'session_has_item.auction_session_id')
            ->join('valuation', 'valuation.id', 'auction_item.valuation_id')
            ->join('vehicle', 'vehicle.id', 'valuation.vehicle_id')
            ->leftJoin('bid_log', function ($join) {
                $join->on('bid_log.auction_session_id', '=', 'auction_session.id')
                    ->on('bid_log.auction_item_id', '=', 'auction_item.id');
            })
            ->leftJoin('user as bider', 'bider.id', 'bid_log.user_id')
            ->leftJoin('location', 'bider.location_id', 'location.id')
            ->leftJoin('contact', 'bider.contact_id', 'contact.id')
            ->leftJoin('transaction', 'transaction.valuation_id', 'valuation.id')
            ->leftJoin('user as buyer', 'buyer.id', 'transaction.buyer_id')
            ->leftJoin('contact as buyer_contact', 'buyer_contact.id', 'buyer.contact_id')
            ->where('session_has_item.auction_session_id', $sessionId);

        return $query;
    }

    static function historyCount($sessionId) {
        return Auction::selectRaw('count(*) as count')
            ->join('session_has_item', 'session_has_item.auction_item_id', 'auction_item.id')
            ->join('auction_session', 'auction_session.id', 'session_has_item.auction_session_id')
            ->join('valuation', 'valuation.id', 'auction_item.valuation_id')
            ->join('vehicle', 'vehicle.id', 'valuation.vehicle_id')
            ->where('session_has_item.auction_session_id', $sessionId)
            ->first()
            ->count;
    }

    static function history($sessionId, $offset = NULL, $limit = NULL) {
        $auctions   = Auction::historyQuery($sessionId, $offset, $limit)
            ->select(
                'buyer_contact.first_name as buyer_first_name',
                'buyer_contact.last_name as buyer_last_name',
                'transaction.vehicle_price as final_price',
                'auction_item.id as auction_item_id',
                'vehicle.make as vehicle_make',
                'vehicle.model_family as vehicle_model_family',
                'vehicle.build_date as vehicle_build_date',
                'vehicle.variant as vehicle_variant',
                'vehicle.id as vehicle_id',
                'auction_item.buy_now_price',
                'location.name as bid_location',
                'bid_log.create_date as bid_time',
                'bid_log.bid_price',
                'contact.first_name as bid_first_name',
                'contact.last_name as bid_last_name')
            ->get();
        $result     = [];
        $array      = [];

        foreach ($auctions as $auction) {
            if (!isset($result[$auction->auction_item_id])) {
                $result[$auction->auction_item_id] = new stdClass();
            }
            $result[$auction->auction_item_id]->auction_item_id         = $auction->auction_item_id;
            $result[$auction->auction_item_id]->buyer_first_name        = $auction->buyer_first_name;
            $result[$auction->auction_item_id]->buyer_last_name         = $auction->buyer_last_name;
            $result[$auction->auction_item_id]->final_price             = $auction->final_price;
            $result[$auction->auction_item_id]->vehicle_make            = $auction->vehicle_make;
            $result[$auction->auction_item_id]->vehicle_model_family    = $auction->vehicle_model_family;
            $result[$auction->auction_item_id]->vehicle_build_date      = $auction->vehicle_build_date;
            $result[$auction->auction_item_id]->vehicle_variant         = $auction->vehicle_variant;
            $result[$auction->auction_item_id]->vehicle_id              = $auction->vehicle_id;
            $result[$auction->auction_item_id]->buy_now_price           = $auction->buy_now_price;
            $result[$auction->auction_item_id]->final_price             = $auction->final_price;

            if (!$auction->bid_location) {
                $result[$auction->auction_item_id]->bids = NULL;
                continue;
            }

            $result[$auction->auction_item_id]->bids[] = [
                'location'      => $auction->bid_location,
                'time'          => $auction->bid_time,
                'price'         => $auction->bid_price,
                'first_name'    => $auction->bid_first_name,
                'last_name'     => $auction->bid_last_name,
            ];
        }

        $result = array_slice($result, $offset * $limit, $limit);

        foreach ($result as $key => $value) {
            $array[] = $value;
        }

        return $array;
    }
}
