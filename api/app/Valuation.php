<?php

namespace App;

use App\Libraries\Dashboard;
use App\Libraries\WishlistHelper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use WindowsAzure\Common\ServicesBuilder;
use MicrosoftAzure\Storage\Blob\Models\ListBlobsOptions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;

use App\User;
use App\Vehicle;

class Valuation extends Backend
{
    const STATUS_WAITING            = 1;
    const STATUS_IN_PROCESS         = 2;
    const STATUS_COMPLETE           = 3;
    const STATUS_PENDING            = 4;
    const STATUS_PENDING_REEVALUATE = 5;
    const STATUS_COMPLETE_TRADED    = 6;
    const STATUS_COMPLETE_TRADED_IN = 7;
    const STATUS_DELETED            = 8;
    const STATUS_VERSION            = 9;
    const STATUS_BEGIN              = 10;
    const STATUS_VERSION_EDITING    = 11;
    const STATUS_SOLD_OUT           = 12;
    const STATUS_SUBMIT_TO_AUCTION  = 13;
    const STATUS_LISTED_FOR_AUCTION = 14;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 'vehicle_id', 'trade_in_location', 'keys_location', 'has_one_owner',
        'has_service_book', 'has_service_history'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];


    protected $table = 'valuation';

    const CREATED_AT = "create_date";
    const UPDATED_AT = "update_date";

    public $timestamps = true;

    public function getVehicle() {
        return Vehicle::find($this->vehicle_id);
    }

    public static function filterQuery($type,
                                       $user,
                                       $ownStat = NULL,
                                       $locationIds = NULL,
                                       $createDate = NULL) {

        $query = Valuation::select('valuation.*');
        switch ($type) {
            case Dashboard::TOTAL_TRADE_INS_WON:
                $query->where('valuation.status', '>=', Valuation::STATUS_COMPLETE_TRADED_IN);
            case Dashboard::TOTAL_VALUATIONS:
                $query->join('vehicle', 'vehicle.id', 'valuation.vehicle_id')
                    ->groupBy('valuation.version');
                break;
            default:
                \Log::warning("undefined type $type send to filter of Valuation");
                break;
        }

        if ($ownStat) {
            $query->where('valuation.valuer_id', $user->id);
        }
//        else {
//            $ids = User::usersSameCompanyQuery($user->company_group_id)->select('user.id')->get();
//            $query->whereIn('valuation.valuer_id', $ids);
//        }

        if ($locationIds) {
            $query->join('user as creator', 'creator.id', '=', 'valuation.user_id')
                ->whereIn('creator.location_id', $locationIds);
        }

        if ($createDate && $createDate['start'] && $createDate['end']) {
            $times = [$createDate['start'], $createDate['end']];
            $query->whereBetween('valuation.create_date', $times);
        }

        return $query;
    }

    public static function percentageValuationWon($user, $ownStat = NULL, $locationId = NULL, $createDate = NULL) {
        $valuationTotal    = Valuation::filterQuery(Dashboard::TOTAL_VALUATIONS,
            $user,
            $ownStat,
            $locationId,
            $createDate)->count(DB::raw('distinct valuation.id'));
        $valuationTradeInWon     = Valuation::filterQuery(Dashboard::TOTAL_TRADE_INS_WON,
            $user,
            $ownStat,
            $locationId,
            $createDate)->count(DB::raw('distinct valuation.id'));
        return $valuationTotal > 0 ? floatval(number_format($valuationTradeInWon / $valuationTotal * 100, 2))."%" : "0%";
    }

    public static function findByIdAndVersion($id, $version) {
        return Valuation::orWhere(function ($query) use (&$id, &$version) {
            $query->where('origin_id', $id)
                ->where('version', $version);
        })->orWhere(function ($query) use (&$id, &$version)  {
            $query->where('id', $id)
                ->where('version', $version);
        })->first();
    }

    public static function searchPending($company_group_id, $offset, $limit, $order_field, $desc, $isHeadValuer, $user_id, $query = NULL, $isMo = false)
    {
        $order = '';
        if ($order_field) {
            if ($desc) {
                $order = " ORDER BY $order_field DESC ";
            } else {
                $order = " ORDER BY $order_field ";
            }
        }

        $status_filter = "(v.status=5 AND v.valuer_id = $user_id)";
        if ($isHeadValuer) {
            $status_filter = "(v.status=4) OR (v.status=5 AND v.valuer_id = $user_id)";
        }

        $year = $query;
        if ($query) {
            $query = preg_replace("/[^A-Za-z0-9]/", ' ', $query);
            $query = trim($query);
        }
        if ($query) {
            $queryMo = $isMo ? " OR location.name like '%$query%' OR company_contact.company_name like '%$query%' OR company_group.name like '%$query%' " : " ";
            $joinMo  = $isMo ? " LEFT JOIN location as valuer_location on valuer.location_id = valuer_location.id 
                                    LEFT JOIN company on company.id = valuer_location.company_id
                                    LEFT JOIN contact as company_contact on company.contact_id = company_contact.id
                                    LEFT JOIN company_group on company.company_group_id = company_group.id " : " ";

            $sql = "
                SELECT DISTINCT
                vehicle.vin, vehicle.colour, vehicle.registration_number, vehicle.engine_number,vehicle.build_date,
                vehicle.has_one_owner,vehicle.has_service_book,vehicle.has_service_history, vehicle.model_family,vehicle.condition,
            	vehicle.make,
                v.*, v.create_date 'valuation_create_date', vehicle.registration_expiry_date,
                vehicle.id 'vehicle_id',
                valuer_contact.id 'valuer_id', valuer_contact.first_name 'valuer_first_name', valuer_contact.last_name 'valuer_last_name',
                seller_contact.id 'salesperson_id', seller_contact.first_name 'salesperson_first_name', seller_contact.last_name 'salesperson_last_name',
                customer.first_name 'customer_first_name', customer.last_name 'customer_last_name',
                location.id 'location_id', location.name 'location_name', location.location_code 'location_code'
                FROM valuation v
                LEFT JOIN `user` valuer ON v.valuer_id=valuer.id
                LEFT JOIN `user` seller ON v.user_id=seller.id
                LEFT JOIN `location` location ON seller.location_id=location.id
                LEFT JOIN `contact` customer ON v.customer_id=customer.id
                LEFT JOIN `contact` seller_contact ON seller.contact_id=seller_contact.id
                LEFT JOIN `contact` valuer_contact ON valuer.contact_id=valuer_contact.id
                LEFT JOIN `vehicle` vehicle ON v.vehicle_id=vehicle.id
                
                $joinMo
                 
                WHERE
                (
                MATCH(seller_contact.first_name, seller_contact.last_name) AGAINST (?  IN BOOLEAN MODE)
                OR MATCH(valuer_contact.first_name, valuer_contact.last_name) AGAINST (?  IN BOOLEAN MODE)
                OR MATCH(customer.first_name, customer.last_name) AGAINST (?  IN BOOLEAN MODE)
                OR MATCH(make, model_family, registration_number) AGAINST (?  IN BOOLEAN MODE)
                
                $queryMo
                
                OR YEAR(vehicle.build_date) LIKE ?
                )
                AND ($status_filter) AND v.company_group_id=?
                $order
                LIMIT ?, ?
            ";

            $query = $query . '*';
            return DB::select($sql, [$query, $query, $query, $query, $year, $company_group_id, $offset, $limit]);
        }

        $sql = "
                SELECT DISTINCT
                vehicle.vin, vehicle.colour, vehicle.registration_number, vehicle.engine_number,vehicle.build_date,
                vehicle.has_one_owner,vehicle.has_service_book,vehicle.has_service_history, vehicle.model_family,vehicle.condition,
            	vehicle.make,
                v.*, v.create_date 'valuation_create_date', vehicle.registration_expiry_date,
                vehicle.id 'vehicle_id',
                valuer_contact.id 'valuer_id', valuer_contact.first_name 'valuer_first_name', valuer_contact.last_name 'valuer_last_name',
                seller_contact.id 'salesperson_id', seller_contact.first_name 'salesperson_first_name', seller_contact.last_name 'salesperson_last_name',
                customer.first_name 'customer_first_name', customer.last_name 'customer_last_name',
                location.id 'location_id', location.name 'location_name', location.location_code 'location_code'
                FROM valuation v
                LEFT JOIN `user` valuer ON v.valuer_id=valuer.id
                LEFT JOIN `user` seller ON v.user_id=seller.id
                LEFT JOIN `location` location ON seller.location_id=location.id
                LEFT JOIN `contact` customer ON v.customer_id=customer.id
                LEFT JOIN `contact` seller_contact ON seller.contact_id=seller_contact.id
                LEFT JOIN `contact` valuer_contact ON valuer.contact_id=valuer_contact.id
                LEFT JOIN `vehicle` vehicle ON v.vehicle_id=vehicle.id
                WHERE
                 ((v.status=4) OR (v.status=5 AND v.valuer_id = $user_id)) AND v.company_group_id=?
                $order
                LIMIT ?, ?
            ";
        return DB::select($sql, [$company_group_id, $offset, $limit]);
    }


    public static function searchPendingCount($company_group_id, $status, $isHeadValuer, $user_id, $query = NULL, $isMo = false)
    {
        if ($query) {
            $query = preg_replace("/[^A-Za-z0-9]/", ' ', $query);
            $query = trim($query);
        }

        $status_filter = "(v.status=5 AND v.valuer_id = $user_id)";
        if ($isHeadValuer) {
            $status_filter = "(v.status=4) OR (v.status=5 AND v.valuer_id = $user_id)";
        }

        $year = $query;
        if ($query) {
            $queryMo = $isMo ? " OR location.name like '%$query%' OR company_contact.company_name like '%$query%' OR company_group.name like '%$query%' " : " ";
            $joinMo  = $isMo ? " LEFT JOIN location as valuer_location on valuer.location_id = valuer_location.id 
                                    LEFT JOIN company on company.id = valuer_location.company_id
                                    LEFT JOIN contact as company_contact on company.contact_id = company_contact.id
                                    LEFT JOIN company_group on company.company_group_id = company_group.id " : " ";

            $query = $query . '*';

            $sql = "
                SELECT 
                COUNT(*) AS `count`
                FROM valuation v
                LEFT JOIN `user` valuer ON v.valuer_id=valuer.id
                LEFT JOIN `user` seller ON v.user_id=seller.id
                LEFT JOIN `location` location ON seller.location_id=location.id
                LEFT JOIN `contact` customer ON v.customer_id=customer.id
                LEFT JOIN `contact` seller_contact ON seller.contact_id=seller_contact.id
                LEFT JOIN `contact` valuer_contact ON valuer.contact_id=valuer_contact.id
                LEFT JOIN `vehicle` vehicle ON v.vehicle_id=vehicle.id
                
                $joinMo
                
                WHERE
                (
                MATCH(seller_contact.first_name, seller_contact.last_name) AGAINST (?  IN BOOLEAN MODE)
                OR MATCH(valuer_contact.first_name, valuer_contact.last_name) AGAINST (?  IN BOOLEAN MODE)
                OR MATCH(customer.first_name, customer.last_name) AGAINST (?  IN BOOLEAN MODE)
                OR MATCH(make, model_family, registration_number) AGAINST (?  IN BOOLEAN MODE)
                OR YEAR(vehicle.build_date) LIKE ?
                
                $queryMo
                )
                AND ($status_filter) AND v.company_group_id=?
            ";
            return DB::select($sql, [$query, $query, $query, $query, $year, $status, $company_group_id])[0];
        }

        $sql = "
                SELECT 
                COUNT(*) AS `count`
                FROM valuation v
                LEFT JOIN `user` valuer ON v.valuer_id=valuer.id
                LEFT JOIN `user` seller ON v.user_id=seller.id
                LEFT JOIN `location` location ON seller.location_id=location.id
                LEFT JOIN `contact` customer ON v.customer_id=customer.id
                LEFT JOIN `contact` seller_contact ON seller.contact_id=seller_contact.id
                LEFT JOIN `contact` valuer_contact ON valuer.contact_id=valuer_contact.id
                LEFT JOIN `vehicle` vehicle ON v.vehicle_id=vehicle.id
                WHERE
                ($status_filter) AND v.company_group_id=?
            ";
        return DB::select($sql, [$status, $company_group_id])[0];
    }

    public static function search($company_group_id, $offset, $limit, $status_filter, $order_field, $desc, $query, $userLocationAccess, $limitUserId, $userLocationId, $isMo = false)
    {
        $order = '';
        if ($order_field) {
            if ($desc) {
                $order = " ORDER BY $order_field DESC ";
            } else {
                $order = " ORDER BY $order_field ";
            }
        }

        $status = NULL;
        if (is_array($status_filter)) {
            foreach ($status_filter as $item) {
                if ($status) {
                    $status = $status . ',' . $item;
                } else {
                    $status = $item;
                }
            }
        } else {
            $status = $status_filter;
        }
        if ($status) {
            $status = '(' . $status . ')';
        }

        $colorIds = TypeCode::where('type', 'COLOR_TYPE')->where('value', $query)->pluck('code')->toArray();
        $colorIds = strtolower(implode(',', $colorIds));
        \Log::debug('colorIds-to-find: '.$colorIds);

        $year = $query;
        if ($query) {
            $query = preg_replace("/[^A-Za-z0-9]/", ' ', $query);
            $query = trim($query);
        }

        $limitAccess = $isMo ? " " : " AND v.company_group_id = $company_group_id AND creator.location_id IN ($userLocationAccess) ";

        $sqlQueryCondition = " ";
        if ($query) {
            $queryMo = $isMo ? " OR location.name like '%$query%' OR company_contact.company_name like '%$query%' OR company_group.name like '%$query%' " : " ";
            $sqlQueryCondition = " 
                (
                MATCH(seller_contact.first_name, seller_contact.last_name) AGAINST (?  IN BOOLEAN MODE)
                OR MATCH(valuer_contact.first_name, valuer_contact.last_name) AGAINST (?  IN BOOLEAN MODE)
                OR MATCH(customer.first_name, customer.last_name) AGAINST (?  IN BOOLEAN MODE)
                OR MATCH(make, model_family, registration_number) AGAINST (?  IN BOOLEAN MODE)                
                OR YEAR(vehicle.build_date) LIKE ?
                OR find_in_set(vehicle.colour, '$colorIds')
                
                $queryMo
                
                )
                AND ";
        }

        $joinMo  = $isMo ? " LEFT JOIN location as valuer_location on valuer.location_id = valuer_location.id 
                                    LEFT JOIN company on company.id = valuer_location.company_id
                                    LEFT JOIN contact as company_contact on company.contact_id = company_contact.id
                                    LEFT JOIN company_group on company.company_group_id = company_group.id " : " ";

        $limitOwnStat = $limitUserId ? " v.valuer_id = $limitUserId AND " : " ";

        $wishlists      = WishList::getByDealerLocationId($userLocationId);
        $wishlistLabel  = WishlistHelper::wishlistLabelQuery($wishlists, 'vehicle', NULL);

        $sqlQuery = "
                SELECT DISTINCT
                vehicle.vin, vehicle.colour, vehicle.registration_number, vehicle.engine_number,vehicle.build_date,
                vehicle.has_one_owner,vehicle.has_service_book,vehicle.has_service_history, vehicle.model_family,vehicle.condition,
                vehicle.make,
                v.*, v.create_date 'valuation_create_date', vehicle.registration_expiry_date,
                vehicle.id 'vehicle_id',
                valuer_contact.id 'valuer_id', valuer_contact.first_name 'valuer_first_name', valuer_contact.last_name 'valuer_last_name',
                seller_contact.id 'salesperson_id', seller_contact.first_name 'salesperson_first_name', seller_contact.last_name 'salesperson_last_name',
                customer.first_name 'customer_first_name', customer.last_name 'customer_last_name',
                location.id 'location_id', location.name 'location_name', location.location_code 'location_code'
                $wishlistLabel
                
                FROM valuation v
                LEFT JOIN `user` valuer ON v.valuer_id=valuer.id
                LEFT JOIN `user` seller ON v.user_id=seller.id
                LEFT JOIN `location` location ON seller.location_id=location.id
                LEFT JOIN `contact` customer ON v.customer_id=customer.id
                LEFT JOIN `contact` seller_contact ON seller.contact_id=seller_contact.id
                LEFT JOIN `contact` valuer_contact ON valuer.contact_id=valuer_contact.id
                LEFT JOIN `vehicle` vehicle ON v.vehicle_id=vehicle.id
                LEFT JOIN `user` creator ON v.user_id = creator.id
                
                $joinMo
                
                WHERE 
                $sqlQueryCondition 
                $limitOwnStat
                (v.status IN $status)
                $limitAccess
                $order
                LIMIT ?, ?
            ";

        if ($query) {
            $query = $query . '*';
            return DB::select($sqlQuery, [$query, $query, $query, $query, $year, $offset, $limit]);
        }
        return DB::select($sqlQuery, [$offset, $limit]);
    }


    public static function searchCount($company_group_id, $status_filter, $query = NULL, $userLocationAccess, $isMo = false)
    {
        if ($query) {
            $query = preg_replace("/[^A-Za-z0-9]/", ' ', $query);
            $query = trim($query);
        }

        $status = NULL;
        if (is_array($status_filter)) {
            foreach ($status_filter as $item) {
                if ($status) {
                    $status = $status . ',' . $item;
                } else {
                    $status = $item;
                }
            }
        } else {
            $status = $status_filter;
        }
        if ($status) {
            $status = '(' . $status . ')';
        }

        $limitAccess = $isMo ? " " : " AND v.company_group_id = $company_group_id AND creator.location_id IN ($userLocationAccess) ";

        $year = $query;
        $joinMo  = $isMo ? " LEFT JOIN location as valuer_location on valuer.location_id = valuer_location.id 
                                    LEFT JOIN company on company.id = valuer_location.company_id
                                    LEFT JOIN contact as company_contact on company.contact_id = company_contact.id
                                    LEFT JOIN company_group on company.company_group_id = company_group.id " : " ";
        if ($query) {
            $queryMo = $isMo ? " OR location.name like '%$query%' 
                                    OR company_contact.company_name like '%$query%' 
                                    OR company_group.name like '%$query%' " : " ";

            $query = $query . '*';
            $sql = "
                SELECT 
                COUNT(DISTINCT v.id) AS `count`
                FROM valuation v
                LEFT JOIN `user` valuer ON v.valuer_id=valuer.id
                LEFT JOIN `user` seller ON v.user_id=seller.id
                LEFT JOIN `location` location ON seller.location_id=location.id
                LEFT JOIN `contact` customer ON v.customer_id=customer.id
                LEFT JOIN `contact` seller_contact ON seller.contact_id=seller_contact.id
                LEFT JOIN `contact` valuer_contact ON valuer.contact_id=valuer_contact.id
                LEFT JOIN `vehicle` vehicle ON v.vehicle_id=vehicle.id
                LEFT JOIN `user` creator ON v.user_id = creator.id
                
                $joinMo
                
                WHERE
                (
                MATCH(seller_contact.first_name, seller_contact.last_name) AGAINST (?  IN BOOLEAN MODE)
                OR MATCH(valuer_contact.first_name, valuer_contact.last_name) AGAINST (?  IN BOOLEAN MODE)
                OR MATCH(customer.first_name, customer.last_name) AGAINST (?  IN BOOLEAN MODE)
                OR MATCH(make, model_family, registration_number) AGAINST (?  IN BOOLEAN MODE)
                OR YEAR(vehicle.build_date) LIKE ?
                
                $queryMo
                )
                AND (v.status IN $status) $limitAccess
            ";
            return DB::select($sql, [$query, $query, $query, $query, $year, $company_group_id])[0];
        }

        $sql = "
                SELECT 
                COUNT(DISTINCT v.id) AS `count`
                FROM valuation v
                LEFT JOIN `user` valuer ON v.valuer_id=valuer.id
                LEFT JOIN `user` seller ON v.user_id=seller.id
                LEFT JOIN `location` location ON seller.location_id=location.id
                LEFT JOIN `contact` customer ON v.customer_id=customer.id
                LEFT JOIN `contact` seller_contact ON seller.contact_id=seller_contact.id
                LEFT JOIN `contact` valuer_contact ON valuer.contact_id=valuer_contact.id
                LEFT JOIN `vehicle` vehicle ON v.vehicle_id=vehicle.id
                LEFT JOIN `user` creator ON v.user_id = creator.id
                
                $joinMo
                
                WHERE
                v.status IN $status $limitAccess
            ";
        return DB::select($sql, [$company_group_id])[0];
    }

    public function getChannel($valuationId)
    {
        $channels = DB::select('
            SELECT c.`name`
            FROM channel_valuation cv INNER JOIN channel c ON c.id = cv.channel_id
            WHERE cv.valuation_id = :valuationId',
            ['valuationId' => $valuationId]);
        return $channels;
    }

    public function getVehicleDetail($valuationId)
    {
       // DB::EnableQueryLog();
        $data = DB::select("
            SELECT va.id valuation_id, va.vehicle_id, '' badge, va.has_one_owner, va.has_service_history, va.has_service_book, va.status, va.overall_rating, t.value overall_rating_text, va.offer_price,
                ve.engine_number, ve.model_family, ve.registration_number, ve.colour, ve.drive_train, ve.registration_expiry_date, ve.vin, ve.chassis_number, ve.build_date, ve.trim, ve.compliance_date, ve.glass_guide_code, ve.registered_state, ve.fuel_type, ve.manufacturer_color, YEAR(ve.first_reg_date) year_first_registered, ve.body_no, ve.trim_color, ve.odometer,
                RTRIM(ve.make) make, RTRIM(ve.family) family, RTRIM(ve.series) series, RTRIM(ve.style) style, RTRIM(ve.engine_type) engine,
                RTRIM(ve.engine_capacity) engine_capacity, RTRIM(ve.engine_size) engine_size, RTRIM(ve.variant) variant, RTRIM(ve.cylinder) cylinder, RTRIM(ve.transmission) transmission, RTRIM(ve.body_type) body_type, va.ownership_type, va.special_disclosures, va.repairable_write_off,
                location.company_id 
            FROM valuation va
                INNER JOIN vehicle ve ON ve.id = va.vehicle_id
                LEFT JOIN type_code t ON t.code = va.overall_rating AND t.type='OVERALL_RATING'
                INNER JOIN user seller on seller.id = va.user_id
                INNER JOIN location on seller.location_id = location.id
            WHERE va.id = :valuationId",
            ['valuationId' => $valuationId]);

           // dd(DB::getQueryLog());
        if ($data) {
            $data[0]->ownership_type = json_decode($data[0]->ownership_type, true);
            return $data[0];
        }
        return NULL;
    }

    public function getRequestedValuation()
    {

       /* $sql = "
            SELECT va.id, va.user_id, va.update_date, u.location_id, ve.model_family, colour.`value` colour, YEAR(ve.build_date) year,
                RTRIM(ve.make) make, va.notify_sent_counter, va.create_date, va.`status`, ve.registration_number
            FROM valuation va
                INNER JOIN user u ON u.id = va.user_id
                INNER JOIN vehicle ve ON ve.id = va.vehicle_id
                LEFT JOIN type_code colour ON colour.code = ve.colour AND colour.type = 'COLOR_TYPE'
            WHERE va.status = 1" ;*/
       // echo $sql ;
        $data = DB::select("
            SELECT va.id, va.user_id, va.update_date, u.location_id, ve.model_family, colour.`value` colour, YEAR(ve.build_date) year,
                RTRIM(ve.make) make, va.notify_sent_counter, va.create_date, va.`status`, ve.registration_number
            FROM valuation va
                INNER JOIN user u ON u.id = va.user_id
                INNER JOIN vehicle ve ON ve.id = va.vehicle_id
                LEFT JOIN type_code colour ON colour.code = ve.colour AND colour.type = 'COLOR_TYPE'
            WHERE va.status = 1");
        return $data;
    }

    public function getValuationById($valuationId)
    {
        $sql = "
            SELECT DISTINCT va.id , ve.model_family, colour.`value` colour, YEAR(ve.build_date) year, va.requested_approve_price, va.negotiating_price, va.max_price, va.user_id,
                RTRIM(ve.make) make, va.`status`, ve.registration_number, va.company_group_id, va.valuer_id, ve.make_id, va.offer_value, va.valuation_value, va.requested_price
            FROM valuation va
                INNER JOIN vehicle ve ON ve.id = va.vehicle_id
                LEFT JOIN type_code colour ON colour.code = ve.colour AND colour.type = 'COLOR_TYPE'
            WHERE va.id = :valuationId";
        $params = ['valuationId' => $valuationId];
        $data = DB::select($sql,$params);
        if ($data) {
            return $data[0];
        }
        return NULL;
    }

    public static function removeLastVersion($valuationId)
    {
    	if ($valuationId == null) {
    		throw new Exception('params can not be empty');
    	}
    	
    	$v = Valuation::find($valuationId);
    	if (!$v) {
    		return -1; // no such valuationId
    	}
    	
    	$v2 = $v->getPreviousVersion();
    	if (!$v2) {
    		return -2; // there is no other version.
    	}
    	
    	//Valuation::deleteImages($v->id, $v->version);

        DB::select("DELETE FROM valuation WHERE id=$v->id");

        DB::select("UPDATE valuation SET id=?, status=? WHERE id=?", [$v->id, $v->status, $v2->id]);


        DB::select("DELETE FROM valuation_inspection_item WHERE valuation_id=$v->id");

        DB::select("UPDATE valuation_inspection_item set valuation_id=? WHERE valuation_id=?", [$v->id, $v2->id]);


//    	$new = $v2->replicate();
//    	$new->status = $v->status;
//    	$new->id = $v->id;
//        $new->create_date = $v->create_date;
//        $new->update_date = $v->update_date;
//    	$v->delete();
//    	$v2->delete();
//    	$new->save();
    	
    	return 0;
    }

    /**
     *  0: success
     * -1: no such valuationId & version (fromValuation)
     * -2: version already exists
     * -3: invalid valuation status
     * -4: is modifying by someone
     * -5: not a lastest valuation
     * @param $valuationId
     * @param $version
     * @param null $fromVersion
     * @return int
     */
    public static function createVersion($version_creator, $valuationId, $version, $fromVersion=null)
    {
        echo "<br>". $version_creator . " - ".$valuationId . " - ".$version ;

        if ($valuationId == null || $version == null || $version_creator == null) {
            throw new Exception('params can not be empty');
        }

        if (Valuation::where('origin_id', $valuationId)->where('version', $version)->first()) {
            return -2; // version existed;
        }

        $v = Valuation::find($valuationId);
        if (!$v) {
            return -1; // no such valuationId
        }
        if ($v->origin_id != 0 && $v->origin_id!= $v->id || $v->status == Valuation::STATUS_VERSION) {
        	return -5; // This is not a root valuation 
        }
        if ($v->version == $version) {
            return -2; // version existed;
        }

        if (!in_array($v->status, [Valuation::STATUS_COMPLETE, Valuation::STATUS_COMPLETE_TRADED,Valuation::STATUS_COMPLETE_TRADED_IN, Valuation::STATUS_LISTED_FOR_AUCTION])) {
//        if (!in_array($v->status, [Valuation::STATUS_COMPLETE_TRADED])) {
            return -3;
        }

        if (!Valuation::setLocker($valuationId, $version_creator)) {
        	return -4;
        }

        $oldValuation = null; // new valuation version will be copied from this.
        if ($fromVersion) {
            $oldValuation = Valuation::orWhere(function ($query) use (&$valuationId, &$fromVersion) {
                $query->where('origin_id', $valuationId)
                        ->where('version', $fromVersion);
            })->orWhere(function ($query) use (&$valuationId, &$fromVersion)  {
                $query->where('id', $valuationId)
                        ->where('version', $fromVersion);
            })->first();
            if (!$oldValuation) {
                return -1;
            }
            if ($oldValuation->version == $v->version) {
                // Old version is current newest version.
                $oldValuation = null;
            }
        }


        if ($oldValuation) {
            \LOG::debug("Create new valuation of [$v->id] from version[$fromVersion]");
        	Valuation::cloneImages($v->id, $fromVersion, $version);
        } else {
            \LOG::debug("Create new valuation of [$v->id] from version[$v->version]");
        	Valuation::cloneImages($v->id, $v->version, $version);
        }
        
        //clone valuation
        $old = $v->replicate();
        $old->origin_id = $v->id;
        $old->status = Valuation::STATUS_VERSION;
        $old->version_locker_time = null;
        $old->version_locker_user_id = 0;
        $old->create_date = $v->create_date;
        $old->save();

        \LOG::debug("Current valuation[$v->id] will be saved as [$old->id]");

        // clone valuation's related info: inspection items

        $items = ValuationInspectionItem::where('valuation_id', $valuationId)->get();
        foreach ($items as $i) {
            $newItem = $i->replicate();
            $newItem->valuation_id = $old->id;

            $newItem->save();

            \LOG::debug("Crete ValuationInspectionItem(id=$newItem->id, valuation_id=$newItem->valuation_id)");
        }

        if ($oldValuation) {
            $current_valuation_status = $v->status;
            ValuationInspectionItem::where('valuation_id', $valuationId)->delete();
            \LOG::debug("Delete all inspection item of valuation[$valuationId]");

            // clone valuation's related info: inspection items
            $items = ValuationInspectionItem::where('valuation_id', $oldValuation->id)->get();
            foreach ($items as $i) {
                //unset($i->id);
                $newItem = $i->replicate();
                $newItem->valuation_id = $valuationId;

                $newItem->save();

                \LOG::debug("Crete ValuationInspectionItem(id=$newItem->id, valuation_id=$newItem->valuation_id)");
            }

            $v->delete();
            $v = $oldValuation->replicate();
            $v->status = $current_valuation_status;
            $v->from_version = $oldValuation->version;
            $v->id = $valuationId;
            $v->version_creator = $version_creator;
            $v->version = $version;
            $v->version_locker_time = date('Y-m-d H:i:s');
            $v->version_locker_user_id = $version_creator;
            $v->create_date = $v->update_date = date('Y-m-d H:i:s');
            $v->save();
            \LOG::debug("Recreate valuation[$oldValuation->id] with version[$v->version]");
        } else {
            $v->from_version = $v->version;
            $v->version = $version;
            $v->version_creator = $version_creator;
            $v->create_date = $v->update_date = date('Y-m-d H:i:s');
            $v->save();
            \LOG::debug("new valuation version[$v->version] was saved");
        }
        
        
        return $old->id;
    }
    
    public function getFirstDifferentValueField($v, $fields) 
    {
    	foreach($fields as $field) {
    		if ($this->$field != $v->$field){
    			return $field;
    		}
    	}
    	return null;
    }
    
    public function hasSameValues($v) {
    	if (!$v) return '_object';
    	return $this->getFirstDifferentValueField($v, ['valuer_id','customer_id','user_id','vehicle_id','trade_in_location','keys_location','company_group_id',
    			'estimated_recon_cost', 'requested_approve_price', 'negotiating_price','max_price','has_one_onwer','has_service_history',
    			'has_service_book','notify_sent_counter','test_driven','mechanical_additional_detail','offer_value','valuation_value',
    			'requested_price','trade_price','proposed_arrival_date','note','value_of_options','overall_rating','general_recondition_cost',
    			'total_recon_cost','location_id','offer_price'
    	]) == null;
    }
    
    public function getPreviousVersion() {
    	if ($this->origin_id) {
    		$origin_id = $this->origin_id;
    	} else {
    		$origin_id = $this->id;
    	}
        \LOG::debug("checking previous version: origin: " .$origin_id." this->id" . $this->id);
        //\Log::info("checking previous version: origin: " .$origin_id." this->id" , $this->id);
/*    	echo "origin : ".$origin_id;
    	echo '<\n> id '.$this->id;
    	echo '<\n> version '.$this->version;*/

    	return Valuation::where('origin_id', $origin_id)->where('id', '!=', $this->id)->where('version', '<', $this->version)->orderBy('version', 'desc')->first();
    }

    public function getParentVersion() {
        if ($this->origin_id) {
            $origin_id = $this->origin_id;
        } else {
            $origin_id = $this->id;
        }
        return Valuation::where('origin_id', $origin_id)->where('version', $this->from_version)->first();
    }


    function azure_listBlogByPrefix($client, $container, $prefix) {
        $ops = new ListBlobsOptions();
        $ops->setPrefix($prefix);
        $blob_list = $client->listBlobs($container, $ops);
        return $blob_list->getBlobs();
    }

    function azure_getBlogProperties($client, $container, $blob) {
        $result = $client->getBlobProperties($container, $blob);
        return $result->getProperties();
    }

//    function hasSameImages($valuationId, $v1, $v2) {
//    }
    
    public function hasSamePictures($v) {
        if ($this->origin_id) {
            $valuationId = $this->origin_id;
        } else {
            $valuationId =  $this->id;
        }
        $v1 = $this->version;
        $v2 = $v->version;

        \LOG::debug("Comparing images of valuation[$valuationId] version $v1 and $v2 || $this->origin_id");

        $accountKey= env('azure_account_key');
        $accountName=env('azure_account_name');

        $connectionString = "DefaultEndpointsProtocol=https;AccountName=$accountName;AccountKey=$accountKey";
        $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);

        $container = "inspection-item";

        $blobs = $this->azure_listBlogByPrefix($blobRestProxy, $container, $valuationId . "_" . $v1);
        $blobs2 = $this->azure_listBlogByPrefix($blobRestProxy, $container, $valuationId . "_" . $v2);


        if (sizeof($blobs) != sizeof($blobs2)) {
            \LOG::debug("\thas diff number of images " . sizeof($blobs) . ' != ' . sizeof($blobs2));
            return false;
        }

        foreach($blobs as $blob)
        {
            $parts = explode("_", $blob->getName());
            array_shift($parts);
            array_shift($parts);

            $b1 = $this->azure_getBlogProperties($blobRestProxy, $container, $blob->getName());
            $b2 = $this->azure_getBlogProperties($blobRestProxy, $container, $valuationId . '_' . $v2 . '_' . implode('_', $parts));

            if ($b1->getContentLength() != $b2->getContentLength() || $b1->getContentMD5() != $b2->getContentMD5()) {
                \LOG::debug("\thas diff image: " . $blob->getName() . ' != ' .  $valuationId . '_' . $v2 . '_' . implode('_', $parts));
                \LOG::debug("\t\t length: " . $b1->getContentLength() . ' ?? ' .  $b2->getContentLength());
                \LOG::debug("\t\t md5: " . $b1->getContentMD5() . ' ?? ' .  $b2->getContentMD5());
                return false;
            }
        }

        \LOG::debug("\t same!");
        return true;
    }
    
    public static function setLocker($id, $user_id)
    {
      //  dd($request->session()->get);
      echo "<br>".$id ." - ". $user_id ;
    	$affected = Valuation::where('id', $id)->whereNull('version_locker_time')->update(['version_locker_time' => date('Y-m-d H:i:s'), 'version_locker_user_id' => $user_id]);

    	return $affected > 0;
    }
    
    public static function unsetLocker($id)
    {
      //  echo "unset locker";
    	$affected = Valuation::where('id', $id)->update(['version_locker_time' => null, 'version_locker_user_id' => 0]);
    	return $affected > 0;
    }
    
    public static function cloneImagesNewValuation($valuationId, $newValuationId) {
    	
    	$accountKey= env('azure_account_key');
    	$accountName=env('azure_account_name');
    	
    	$connectionString = "DefaultEndpointsProtocol=https;AccountName=$accountName;AccountKey=$accountKey";
    	$blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);
    	
    	$container = "inspection-item";
    	
    	
        $ops = new ListBlobsOptions();
    	$ops->setPrefix($valuationId . "_" . Valuation::find($valuationId)->version);
    	$blob_list = $blobRestProxy->listBlobs($container, $ops);
        $blobs = $blob_list->getBlobs();
        
        $valuationLenght = strlen($valuationId);
    	
    	foreach($blobs as $blob)
    	{
            $parts = explode("_", $blob->getName());
    		array_shift($parts);
    		array_shift($parts);
            
    		$newName =  $newValuationId . '_' . 1 . '_' . implode('_', $parts);
    		\LOG::debug( "COPY ". $blob->getName() ." TO $newName <br /> \n");
    		$blobRestProxy->copyBlob($container, $newName, $container, $blob->getName());
    	}
    }
    
    public static function cloneImages($valuationId, $v1, $v2) {
    	
    	$accountKey= env('azure_account_key');
    	$accountName=env('azure_account_name');
    	
    	$connectionString = "DefaultEndpointsProtocol=https;AccountName=$accountName;AccountKey=$accountKey";
    	$blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);
    	
    	$container = "inspection-item";
    	
    	
    	$ops = new ListBlobsOptions();
    	$ops->setPrefix($valuationId . "_" . $v1);
    	$blob_list = $blobRestProxy->listBlobs($container, $ops);
    	$blobs = $blob_list->getBlobs();
    	
    	foreach($blobs as $blob)
    	{
    		$parts = explode("_", $blob->getName());
    		array_shift($parts);
    		array_shift($parts);
    		
    		$newName =  $valuationId . '_' . $v2 . '_' . implode('_', $parts);
    		/**
    		 *         $destinationContainer,
    		 $destinationBlob,
    		 $sourceContainer,
    		 $sourceBlob,
    		 $options = null
    		 */
    		\LOG::debug( "COPY ". $blob->getName() ." TO $newName <br /> \n");
    		$blobRestProxy->copyBlob($container, $newName, $container, $blob->getName());
    		// 		echo $blob->getName().": ".$blob->getUrl()."<br /> \n";
    	}
    }
    
    public static function deleteImages($valuationId, $v1) {
    	
    	$accountKey= env('azure_account_key');
    	$accountName=env('azure_account_name');

        $connectionString = "DefaultEndpointsProtocol=https;AccountName=$accountName;AccountKey=$accountKey";
    	$blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);
    	
    	$container = "inspection-item";
    	
    	
    	$ops = new ListBlobsOptions();
    	$ops->setPrefix($valuationId . "_" . $v1);
    	$blob_list = $blobRestProxy->listBlobs($container, $ops);
    	$blobs = $blob_list->getBlobs();
    	
    	foreach($blobs as $blob)
    	{
    		\LOG::debug("DELETE " . $blob->getName(). "<br /> \n");
    		/**
    		 *         $destinationContainer,
    		 $destinationBlob,
    		 $sourceContainer,
    		 $sourceBlob,
    		 $options = null
    		 */
    		$blobRestProxy->deleteBlob($container, $blob->getName());
    		// 		echo $blob->getName().": ".$blob->getUrl()."<br /> \n";
    	}
    }

    public static function releaseOutOfTimeValuation() {
        \LOG::info('Start automatic release version clock task.');

        $list = DB::select("SELECT id, version, version_locker_time FROM valuation WHERE version_locker_time IS NOT NULL AND (version_locker_time + INTERVAL 20 MINUTE) < :date", ['date' => date('Y-m-d H:i:s')]);

        foreach($list as $item) {
            \LOG::info("Checking valuation[$item->id]");

            $v = Valuation::find($item->id);

            $pv = $v->getPreviousVersion();

            if (!$pv) {
                echo "valuation[$v->id] has no previous version => release" ;
                \LOG::info("valuation[$v->id] has no previous version => release");
                continue;
            }
/* Commenting pictures condition temperory by SS-E0033
  Dated:6th Sept
    Reason : azure is not working so we will skip image checking for now.
OLD code :  if ($v->hasSameValues($pv) && $v->hasSamePictures($pv)) {
 */
            if ($v->hasSameValues($pv)) {
                echo "valuation[$v->id] remove last version" ;
                \LOG::info("valuation[$v->id] remove last version");
                Valuation::removeLastVersion($item->id);
            }

            Valuation::unsetLocker($item->id);

        }
    }

    public function usersHaveMatchWishlist() {

        $vehicle = Vehicle::find($this->vehicle_id);
        $sellerLocation = User::find($this->user_id)->location_id;
        $wishLists = WishList::getByDealerLocationId($sellerLocation);
        $deallerLocationIds = [];

        if (!$vehicle || !$sellerLocation) {
            return NULL;
        }

        foreach ($wishLists as $k => $wishList) {
            $location = $wishList->locationId;
            if ($location) {
                if (array_search($vehicle->trade_in_location, explode(",", $location)) === false) {
                    continue;
                }
            }

            if ($wishList->odometer_start != null && $wishList->odometer_end != null) {
                if (!($wishList->odometer_start <= $vehicle->odometer && $vehicle->odometer <= $wishList->odometer_end)) {
                    continue;
                }
            }

            if ($wishList->year_start != null && $wishList->year_end != null) {
                if (!($wishList->year_start <= $vehicle->build_date && $vehicle->build_date <= $wishList->year_end)) {
                    continue;
                }
            }

            $transmission = collect();

            $transmissionArr = json_decode($wishList->transmission, true);
            if ($transmissionArr) {
                foreach ($transmissionArr as $type) {
                    $x = str_replace('\'', '"', $type);
                    $x = trim($x);
                    if ($x) {
                        $transmission->push("'" . $x . "'");
                    }
                }

                $transmission = $transmission->unique()->values()->all();

                if ($transmission) {
                    if (!array_search($vehicle->transmission, $transmission) === false) {
                        continue;
                    }
                }
            }

            $make = collect();
            $model = collect();
            $variant = collect();

            if ($wishList->makeInfo) {
                $arr = json_decode($wishList->makeInfo, true);
                if ($arr) {
                    foreach ($arr as $i) {

                        $i = (object)$i;
                        if (property_exists($i, "make")) {
                            $i->make = str_replace('\'', '"', $i->make);
                            $i->make = trim($i->make);
                        } else
                            $i->make = "";
                        if (property_exists($i, "model")) {
                            $i->model = str_replace('\'', '"', $i->model);
                            $i->model = trim($i->model);
                        } else {
                            $i->model = "";
                        }

                        if (property_exists($i, "variant")) {
                            $i->variant = str_replace('\'', '"', $i->variant);
                            $i->variant = trim($i->variant);
                        } else {
                            $i->variant = "";
                        }
                        if ($i->make) {
                            $make->push("'" . $i->make . "'");
                        }
                        if ($i->model) {
                            $model->push("'" . $i->model . "'");
                        }
                        if ($i->variant) {
                            $variant->push("'" . $i->variant . "'");
                        }
                    }

                    $make = $make->unique()->values()->all();
                    $model = $model->unique()->values()->all();
                    $variant = $variant->unique()->values()->all();

                    if (sizeof($make) > 0) {
                        if (array_search($vehicle->make, $make) === false) {
                            continue;
                        }
                    }
                    if (sizeof($model) > 0) {
                        if (array_search($vehicle->model_family, $model) === false) {
                            continue;
                        }
                    }
                    if (sizeof($variant) > 0) {
                        if (array_search($vehicle->variant, $variant) === false) {
                            continue;
                        }
                    }
                }
            }

            if ($wishList->price_start != null && $wishList->price_end != null) {

                if (($wishList->price_start > $this->start_price && $wishList->price_end > $this->start_price) &&
                    ($wishList->price_start > $this->buy_now_price && $wishList->buy_now_price > $this->start_price)) {
                    continue;
                }
            }

            $locationId = Location::find($wishList->dealer_location)->id;

            if (!in_array($location, $deallerLocationIds)) {
                $deallerLocationIds[] = $locationId;
            }
        }

        $companyIds = Location::whereIn('location.id', $deallerLocationIds)
            ->groupBy('company_id')
            ->pluck('company_id')
            ->all();

        return User::whereIn('location_id', $companyIds)->get();
    }
}
