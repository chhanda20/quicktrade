<?php

namespace App;

class InspectionItem extends Backend
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['valuation_id', 'inspection_item_id', 'repair_cost', 'note' , 'creator_id', 'updater_id', 'create_date', 'update_date'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'inspection_item';

}
