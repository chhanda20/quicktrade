<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
class UserRole extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'user_role';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;
    public function updateRoleUserInfo($userId,$newRoleId)
    {
        return DB::table('user_role')
            ->where('user_id', $userId)
            ->update(['role_id' => $newRoleId]);
    }

    public function deleteAllRole($userId)
    {
        return DB::table('user_role')
            ->where('user_id', $userId)
            ->delete();
    }


    public function getRoles()
    {

    }
}
