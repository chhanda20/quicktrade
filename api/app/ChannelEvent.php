<?php

namespace App;

use App\Libraries\ErrorCodes;
use Exception;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class ChannelEvent extends Backend
{
    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;


    protected $table = 'channel_event';
}