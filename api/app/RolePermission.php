<?php

namespace App;

use Illuminate\Support\Facades\DB;

class RolePermission extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'role_permission';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public static function getListPerMissionByRole($role_id){
        return DB::select("SELECT p.* FROM permission p LEFT JOIN role_permission rp ON p.id=rp.permission_id WHERE role_id = :role_id", ['role_id' => $role_id]);
    }
}
