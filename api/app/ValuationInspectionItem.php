<?php

namespace App;
use WindowsAzure\Common\ServicesBuilder;
use MicrosoftAzure\Storage\Blob\Models\ListBlobsOptions;
use Illuminate\Support\Facades\DB;

class ValuationInspectionItem extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];
    protected $table = 'valuation_inspection_item';

    const EXTERIOR_SECTION = 'EXTERIOR_SECTION';
    const INTERIOR_SECTION = 'INTERIOR_SECTION';
    const MECHANICAL_SECTION = 'MECHANICAL_SECTION';

    public function getItems($valuationId, $sectionType)
    {
        $sql = '
            SELECT v.inspection_item_id, i.item, v.repair_cost, v.note, i.section_type
            FROM valuation_inspection_item v INNER JOIN inspection_item i ON i.id = v.inspection_item_id
            WHERE v.valuation_id = :valuationId';
        $params = ['valuationId' => $valuationId];
        if ($sectionType) {
            $sql.= ' AND i.section_type = :sectionType';
            $params['sectionType'] = $sectionType;
        }
        $sql .= ' ORDER BY i.id';
        $data = DB::select($sql,$params);
        return $data;
    }

    public function getSection($valuationId, $sectionGroup)
    {
        $sql = "
            SELECT DISTINCT i.section_type, t.value section_name
            FROM valuation_inspection_item v
                    INNER JOIN inspection_item i ON i.id = v.inspection_item_id
                    INNER JOIN type_code t ON t.code = i.section_type AND t.type = :sectionGroup
            WHERE v.valuation_id = :valuationId";
        $params = [
            'valuationId' => $valuationId,
            'sectionGroup' => $sectionGroup,
        ];
        $sql .= ' ORDER BY i.section_type';
        $data = DB::select($sql,$params);
        return $data;
    }

    public static function deleteBySection($valuationId, $sectionType)
    {
        $sql = '
            DELETE FROM valuation_inspection_item WHERE valuation_id = :valuationId AND inspection_item_id IN (
                SELECT i.id
                FROM inspection_item i INNER JOIN type_code t ON t.code = i.section_type
                WHERE t.type = :sectionType
            )';
        $params = [
            'valuationId' => $valuationId,
            'sectionType' => $sectionType,
        ];

        $deleted = DB::delete($sql,$params);
        return $deleted;
    }

    public function getItemsBySectionGroup($valuationId, $sectionGroup)
    {
        $sql = '
            SELECT v.inspection_item_id, i.item, v.repair_cost, i.section_type
            FROM valuation_inspection_item v
                INNER JOIN inspection_item i ON i.id = v.inspection_item_id
                INNER JOIN type_code t ON t.code = i.section_type AND t.type = :sectionGroup
            WHERE v.valuation_id = :valuationId';
        $params = [
            'valuationId' => $valuationId,
            'sectionGroup' => $sectionGroup,
        ];

        $data = DB::select($sql,$params);
        return $data;
    }
}
