<?php

namespace App;

use Illuminate\Support\Facades\DB;

class NotificationStatus extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['user_id', 'event_code', 'source_id', 'is_ready'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];
    protected $table = 'notification_status';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public static function readyReceiveNotification($userId, $eventCode, $sourceId) {
        $status = NotificationStatus::where('user_id', $userId)
            ->where('event_code', $eventCode)
            ->where('source_id', $sourceId)
            ->first();
        \LOG::debug($status);
        return $status ? $status->is_ready : true;
    }
}
