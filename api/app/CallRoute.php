<?php

namespace App;

use App\Http\Controllers\GlassDataController;
use App\Http\Controllers\MailServiceController;
use App\Http\Controllers\TransactionController;
use app\Libraries\Dashboard;
use App\Libraries\Helper;
use App\Libraries\PDFGeneratorService;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use App\Libraries;
use Illuminate\Support\Facades\Log;
use App\Event;
use App\EventPermission;

class CallRoute extends Backend
{
    static function genPdf($html) {
        return PDFGeneratorService::generate($html, 'A4', 'landscape', false);
    }

    static function dispute($seller, $transaction) {
        $html = "<h1>DISPUTE $transaction->id</h1></br>
                <h4>buyer id $transaction->buyer_id</h4></br>
                <h4>seller id $transaction->seller_id</h4></br>
                <h4>valuation id $transaction->valuation_id</h4></br>
                Please contact amdin for more info";

        return PDFGeneratorService::generate($html);
    }

    static function sendMail($user, $attach) {
        return MailServiceController::sendDispute($user, $attach);
    }

    static function sendTest($user_id) {
        $customData = [];
        $message    = 'message';
        $title      = 'title';

        // Helper::notify('transaction_notify', $user_id, $title, $message, $customData, 'user_id');

        return 'push to '.$user_id;
    }

    static function test() {
        User::notice(116, 'auction_session_outcome', 0, ['body' => [10, 11, 23]]);
    }

    static function callApi() {
        echo 'start init client';
        $client = new Client(['base_uri' => 'localhost:8080',
                              'headers' => ['token', 'qs0j1x9E1mZdphykHAg7RAB2nrY5qmaW57I35vbk']
                            ]);
        echo 'end init client';
        $response = $client->post('manualVehicle/create', [
            'json' => [
                'limit'  => 20,
                'offset' => 0,
                'status' => [1],
                'query'  => ''
            ]
        ]);
        return $response;
    }

    static function export() {
        $class = new GlassDataController();
        return $class->export();
    }

    static function sendAll() {
        $users = User::all();

        foreach ($users as $user) {
            \LOG::debug('send_notify_'.$user->id);
            // Helper::notify('transaction_notify', $user->id, 'title', 'message', 'custom-data', 'user_id');
        }
    }

    //This function is POWERFULL, call when alter/add new table to database
    static function updateDefaultValueDueChangeDataBase() {
        //update default postcode for location
        {
            echo 'start update default postcode for location --- ';
            Location::whereNull('postcode')
                ->update(
                    ['postcode' => 400]
            );
        }

        //update default event when update table event_permission
        {
            echo 'start update due update value of table event_permission --- ';

            $eventPermission = EventPermission::all();
            $defaultCategory = 'Default-Category-Need-Update-Manually';
            foreach ($eventPermission as $ep) {
                $code = $ep->event;
                $even = Event::where('code', $code)->first();
                if ($even == NULL) {
                    $event              = new Event();
                    $event->code        = $code;
                    $event->category    = $defaultCategory;
                    $event->save();
                }
            }

            $missingCategory = "Missing-Permission-Category-Need-Remote";
            $e2 = EventPermission::select('event')->groupBy('event')->pluck('event')->toArray();
            Event::whereNotIn('code', $e2)->update(['category' => $missingCategory]);
        }

        //update default setting email + notification of user to enable
        {
            echo 'update default setting email + notification of user to enable';
            $users   = User::all();
            $events  = Event::all();

            $total = User::count() * Event::count();
            $count = 0;
            foreach ($users as $user) {
                foreach ($events as $event) {
                    $count += 1;

                    echo $count.'/'.$total.'+++';
                    if (ConfigEmailNotification::where([['user_id', '=', $user->id],
                                                        ['event', '=', $event->code]])->first() != NULL) {
                        continue;
                    }
                    $config             = new ConfigEmailNotification();
                    $config->user_id    = $user->id;
                    $config->event      = $event->code;
                    $config->config     = ConfigEmailNotification::CONFIG_BOTH;
                    $config->save();
                }
            }
        }

        //update permission new category
        Permission::whereIn('name', [
            'vehicle_trade', 
            'vehicle_create', 
            'vehicle_update', 
            'valuation_create', 
            'valuation_edit', 
            'valuation_perform', 
            'valuation_approve', 
            'valuation_reprice', 
            'valuation_delete', 
            'wholesale_value_update', 
            'sales_info_update'
        ])->update(['category' => 2]);

        //remove useless event
        {
            DB::table('event')->whereIn('code', [
                'external_best_offer',
                'transaction_dispute',
                'transaction_resolve_dispute'
            ])->delete();
        }
    }

    static function disableEmail() {
        //update default setting email + notification of user to enable
        {
            echo 'update default setting email + notification of user to enable';
            $users   = User::all();
            $events  = Event::all();

            $total = User::count() * Event::count();
            $count = 0;
            foreach ($users as $user) {
                foreach ($events as $event) {
                    $count += 1;

                    echo ($count / $total * 100)."%\n";
                    $config = ConfigEmailNotification::where([['user_id', '=', $user->id], ['event', '=', $event->code]])->first();

                    if ($config != NULL) {
                        $notifyOnly = ConfigEmailNotification::CONFIG_NOTI_ONLY;
                        DB::statement("UPDATE config_email_notification SET config = '$notifyOnly' where (`user_id` = $user->id and `event` = '$event->code')");
                        continue;
                    }
                    $config             = new ConfigEmailNotification();
                    $config->user_id    = $user->id;
                    $config->event      = $event->code;
                    $config->config     = ConfigEmailNotification::CONFIG_NOTI_ONLY;
                    $config->save();
                }
            }
        }
    }

    public static function createNewEvent($code, $category, $permissionIds) {
        $event              = new Event();
        $event->category    = $category;
        $event->code        = $code;
        $event->save();
        
        foreach ($permissionIds as $id) {
            $eventPermission                = new EventPermission();
            $eventPermission->event         = $code;
            $eventPermission->permission_id = $id;
            $eventPermission->save();
        }
    }
}