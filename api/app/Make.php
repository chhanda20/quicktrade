<?php

namespace App;

use App\Libraries\ErrorCodes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Make extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    protected $table = 'glass_make';


}
