<?php

namespace App;

use App\Libraries\Dashboard;
use App\Libraries\WishlistHelper;
use DateTime;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Tags\Example;

class ExternalOffer extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];
    protected $table = 'external_offer';

    const CREATED_AT = "create_date";
    const UPDATED_AT = NULL;
    public $timestamps = true;

    const STATUS_PENDING = 1;
    const STATUS_OFFER_MADE = 2;
    const STATUS_AWAITING_RESPONSE = 3;
    const STATUS_ACCEPTED = 4;
    const STATUS_LOCATION_SET = 5;
    const STATUS_VEHICLE_READY_TO_SHIP = 6;
    const STATUS_TRANSPORT_TRIGGERED = 7;
    const STATUS_SHIPPED = 8;
    const STATUS_DELIVERED = 9;
    const STATUS_PASSED = 10;

    /**
     * @param $status
     * @param $userId
     * @param $limit
     * @param $offset
     * @param bool $orderDesc
     * @param array $filter
     * @return mixed
     */
    public static function getOutgoing($status, $userId, $limit, $offset, $orderDesc = FALSE, $filter = [])
    {
        $statusStr = implode(',', $status);
        $submittedOffer = [ExternalOffer::STATUS_PENDING,
                           ExternalOffer::STATUS_OFFER_MADE];

        $groupBy = $submittedOffer == $status ? " GROUP BY external_offer_id " : " ";

        $query = "
            SELECT CONCAT_WS(' ', ve.make,ve.model_family,
                                  ve.series,
                                  ve.style,
                                  ve.engine_type,
					              ve.engine_capacity,
					              ve.engine_size,
					              ve.variant,
					              ve.cylinder,
					              ve.transmission) offer_name,  
                                  ve.model_family,
                                  colour.value colour, 
                                  YEAR(ve.build_date) year_build, 
                                  ve.odometer,
                                  RTRIM(ve.make) make, 
                                  RTRIM(ve.family) family, 
                                  ve.id vehicle_id, 
                                  ve.variant, 
                                  ve.transmission,
                                  eo.id external_offer_id, 
                                  eo.create_date, 
                                  eo.arrival_date, 
                                  eo.`status` offer_status, 
                                  va.estimated_recon_cost total_recon_cost, 
                                  va.id valuation_id, 
                                  c.state location, 
                                  t.name location_text,
                                  ob.bid_date, 
                                  ob.bid_price, 
                                  ob.expiry_date,
                                  CONCAT_WS(' ', c.first_name, c.last_name) buyer_name
                FROM valuation va
                INNER JOIN vehicle ve ON ve.id = va.vehicle_id
                INNER JOIN external_offer eo ON eo.valuation_id = va.id 
                                             AND eo.creator_id = :userId 
                                             AND eo.`status` IN ($statusStr)
                LEFT JOIN type_code colour ON colour.type = 'COLOR_TYPE' 
                                           AND colour.`code` = ve.colour
                INNER JOIN `user` u ON u.id = va.user_id
		        INNER JOIN contact c ON c.id = u.contact_id
		        LEFT JOIN offer_bid ob ON ob.external_offer_id = eo.id
		        LEFT JOIN country_state t ON t.id = c.state";

        $query = $query.$groupBy;
        $where = [];

        if (isset($filter['makeInfo'])) {
            $makeAndWhere = [];
            foreach ($filter['makeInfo'] as $makeInfo) {
                $makeWhere = [];
                if (isset($makeInfo['make']) && $makeInfo['make']) {
                    $makeWhere[] = "ve.make = '" . $makeInfo['make']."'";
                }
                if (isset($makeInfo['model']) && $makeInfo['model']) {
                    $makeWhere[] = "ve.model_family = '" . $makeInfo['model']."'";
                }
                if (isset($makeInfo['variant']) && $makeInfo['variant']) {
                    $makeWhere[] = "ve.variant = '" . $makeInfo['variant']."'";
                }
                if ($makeWhere) {
                    $makeAndWhere[] = '(' . implode(' AND ', $makeWhere) . ')';
                }
            }
            if ($makeAndWhere) {
                $where[] = '(' . implode(' OR ', $makeAndWhere) . ')';
            }
        }

        if (isset($filter['odoRange'])) {
            if ($filter['odoRange']['from'] && $filter['odoRange']['to']) {
                $where[] = 've.odometer BETWEEN '.$filter['odoRange']['from'].' AND '.$filter['odoRange']['to'];
            }
        }

        if (isset($filter['yearRange'])) {
            if ($filter['yearRange']['from'] && $filter['yearRange']['to']) {
                $where[] = 'YEAR(ve.build_date) BETWEEN '.$filter['yearRange']['from'].' AND '.$filter['yearRange']['to'];
            }
        }

        if (isset($filter['vehicleLocation']) && $filter['vehicleLocation']) {
            $where[] = "c.state IN ('".implode("','", $filter['vehicleLocation'])."')";
        }

        if (isset($filter['transmisionType']) && $filter['transmisionType']) {
            $where[] = "ve.transmission IN ('".implode("','", $filter['transmisionType'])."')";
        }

        if (isset($filter['lastDay']) && $filter['lastDay']) {
            $where[] = "eo.create_date BETWEEN DATE_SUB(NOW(),INTERVAL ".$filter['lastDay']." DAY) AND NOW()";
        }

        if (isset($filter['dateRange'])) {
            if ($filter['dateRange']['from'] && $filter['dateRange']['to']) {
                $where[] = "eo.create_date BETWEEN '".$filter['dateRange']['from']."' AND ('".$filter['dateRange']['to'] ."' + INTERVAL 1 DAY)";
            }
        }

        $whereStr = '';
        if ($where) {
            $whereStr = 'WHERE (' . implode(' AND ', $where) . ')';
        }

        if ($orderDesc) {
            $orderBy = "ORDER BY eo.id DESC";
        } else {
            $orderBy = "ORDER BY eo.id";
        }
        $limitQuery = "LIMIT :offset, :limit";
        $sql = $query . ' ' . $whereStr . ' ' . $orderBy . ' ' . $limitQuery;
        $params = [
            'userId' => $userId,
            'limit' => $limit,
            'offset' => $offset,
        ];
        $data = DB::select($sql, $params);

        $str = implode(",", $params);
        \LOG::debug("sql: $sql, param: $str");
        return $data;
    }


    public static function getIncoming($status, $userId, $locationId, $companyId, $limit, $offset, $orderDesc = FALSE, $filter = [])
    {
        $wishlists      = WishList::getByDealerLocationId($locationId);
        $wishlistLabel  = WishlistHelper::wishlistLabelQuery($wishlists, 've', NULL);

        $statusStr = implode(',', $status);
        $selectField = "DISTINCT CONCAT_WS(' ', ve.make,ve.model_family,ve.series,ve.style,ve.engine_type,
					ve.engine_capacity,ve.engine_size,ve.variant,ve.cylinder,ve.transmission) offer_name, 
        ve.model_family, colour.value colour, YEAR(ve.build_date) year_build, ve.odometer,
            RTRIM(ve.make) make, ve.id vehicle_id, ve.variant, ve.transmission,
            eo.id external_offer_id, eo.create_date, eo.arrival_date, eo.`status` offer_status, va.estimated_recon_cost total_recon_cost, va.id valuation_id, c.state location, t.name location_text,
            CONCAT_WS(' ', c.first_name, c.last_name) buyer_name";
        $query = "
            SELECT *
            FROM (
                SELECT $selectField $wishlistLabel
                FROM valuation va
                    INNER JOIN vehicle ve ON ve.id = va.vehicle_id AND va.vehicle_id NOT IN (
                            SELECT vehicle_id
                            FROM vehicle_blocked vb
                            where vb.location_id = :locationId
                    )
                    INNER JOIN external_offer eo ON eo.valuation_id = va.id AND eo.`status` IN ($statusStr) AND eo.creator_id != :userId AND eo.public_group = 1
                    LEFT JOIN type_code colour ON colour.type = 'COLOR_TYPE' AND colour.`code` = ve.colour
                    INNER JOIN `user` u ON u.id = va.user_id
                    INNER JOIN contact c ON c.id = u.contact_id
                    LEFT JOIN offer_bid ob ON ob.external_offer_id = eo.id
                    INNER JOIN user offer_creator ON offer_creator.id = eo.creator_id
                    INNER JOIN location l ON l.id = offer_creator.location_id
                    INNER JOIN company com ON com.id = l.company_id AND com.id != :companyId
                    LEFT JOIN country_state t ON t.id = c.state
                UNION

                SELECT $selectField $wishlistLabel
                FROM valuation va
                    INNER JOIN vehicle ve ON ve.id = va.vehicle_id AND va.vehicle_id NOT IN (
                            SELECT vehicle_id
                            FROM vehicle_blocked vb
                            where vb.location_id = :locationId2
                    )
                    INNER JOIN external_offer eo ON eo.valuation_id = va.id AND eo.`status` IN ($statusStr) AND eo.creator_id != :userId2 AND eo.public_group = 0
                    LEFT JOIN type_code colour ON colour.type = 'COLOR_TYPE' AND colour.`code` = ve.colour
                    INNER JOIN `user` u ON u.id = va.user_id
                    INNER JOIN contact c ON c.id = u.contact_id
                    LEFT JOIN offer_bid ob ON ob.external_offer_id = eo.id
                    INNER JOIN external_group_offer go ON go.external_offer_id = eo.id
                    INNER JOIN external_group eg ON eg.id = go.external_group_id
                    INNER JOIN external_group_location el ON el.external_group_id = eg.id AND el.location_id = :locationId3
                    LEFT JOIN country_state t ON t.id = c.state
            ) a";

        $where = [];
        if (isset($filter['makeInfo'])) {
            $makeAndWhere = [];
            foreach ($filter['makeInfo'] as $makeInfo) {
                $makeWhere = [];
                if (isset($makeInfo['make']) && $makeInfo['make']) {
                    $makeWhere[] = "a.make = '" . $makeInfo['make']."'";
                }
                if (isset($makeInfo['model']) && $makeInfo['model']) {
                    $makeWhere[] = "a.model_family = '" . $makeInfo['model']."'";
                }
                if (isset($makeInfo['variant']) && $makeInfo['variant']) {
                    $makeWhere[] = "a.variant = '" . $makeInfo['variant']."'";
                }
                if ($makeWhere) {
                    $makeAndWhere[] = '(' . implode(' AND ', $makeWhere) . ')';
                }
            }
            if ($makeAndWhere) {
                $where[] = '(' . implode(' OR ', $makeAndWhere) . ')';
            }
        }

        if (isset($filter['odoRange'])) {
            if ($filter['odoRange']['from'] && $filter['odoRange']['to']) {
                $where[] = 'a.odometer BETWEEN '.$filter['odoRange']['from'].' AND '.$filter['odoRange']['to'];
            }
        }

        if (isset($filter['yearRange'])) {
            if ($filter['yearRange']['from'] && $filter['yearRange']['to']) {
                $where[] = 'a.year_build BETWEEN '.$filter['yearRange']['from'].' AND '.$filter['yearRange']['to'];
            }
        }

        if (isset($filter['vehicleLocation']) && $filter['vehicleLocation']) {
            $where[] = "a.location IN ('".implode("','", $filter['vehicleLocation'])."')";
        }

        if (isset($filter['transmisionType']) && $filter['transmisionType']) {
            $where[] = "a.transmission IN ('".implode("','", $filter['transmisionType'])."')";
        }

        if (isset($filter['lastDay']) && $filter['lastDay']) {
            $where[] = "a.create_date BETWEEN DATE_SUB(NOW(),INTERVAL ".$filter['lastDay']." DAY) AND NOW()";
        }

        if (isset($filter['dateRange'])) {
            if ($filter['dateRange']['from'] && $filter['dateRange']['to']) {
                $where[] = "a.create_date BETWEEN '".$filter['dateRange']['from']."' AND ('".$filter['dateRange']['to'] ."' + INTERVAL 1 DAY)";
            }
        }
        $whereStr = '';
        if ($where) {
            $whereStr = 'WHERE (' . implode(' AND ', $where) . ')';
        }

        if ($orderDesc) {
            $orderBy = "ORDER BY a.external_offer_id DESC";
        } else {
            $orderBy = "ORDER BY a.external_offer_id";
        }
        $limitQuery = "LIMIT :offset, :limit";
        $sql = $query . ' ' . $whereStr . ' ' . $orderBy . ' ' . $limitQuery;
        $params = [
            'userId' => $userId,
            'userId2' => $userId,
            'limit' => $limit,
            'offset' => $offset,
            'locationId' => $locationId,
            'locationId2' => $locationId,
            'locationId3' => $locationId,
            'companyId' => $companyId
        ];
        $data = DB::select($sql, $params);
        return $data;
    }

    public static function getOfferWon($status, $locationId, $companyId, $limit, $offset, $orderDesc = FALSE, $filter = [])
    {
        $statusStr = implode(',', $status);
        $query = "
                SELECT DISTINCT CONCAT_WS(' ', ve.make,ve.model_family,ve.series,ve.style,ve.engine_type,
					                           ve.engine_capacity,ve.engine_size,ve.variant,ve.cylinder,ve.transmission) offer_name, 
                 ve.model_family, colour.value colour, YEAR(ve.build_date) year_build,
                    RTRIM(ve.make) make, RTRIM(ve.family) family, ve.id vehicle_id, va.id valuation_id, ve.odometer, ve.transmission, ve.variant,
                    eo.id external_offer_id, eo.`status` offer_status, c.state location, t.name location_text, eo.create_date,
                    ob.bid_date, ob.bid_price, ob.expiry_date, ob.user_id buyer_id, CONCAT_WS(' ', c.first_name, c.last_name) buyer_name
                FROM valuation va
                    INNER JOIN vehicle ve ON ve.id = va.vehicle_id AND va.vehicle_id NOT IN (
                            SELECT vehicle_id
                            FROM vehicle_blocked vb
                            where vb.location_id = :locationId
                    )
                    INNER JOIN external_offer eo ON eo.valuation_id = va.id AND eo.`status` IN ($statusStr)
                    LEFT JOIN type_code colour ON colour.type = 'COLOR_TYPE' AND colour.`code` = ve.colour
                    INNER JOIN offer_bid ob ON ob.external_offer_id = eo.id
                    INNER JOIN user u ON u.id = ob.user_id
                    INNER JOIN contact c ON c.id = u.contact_id
                    INNER JOIN location l ON l.id = u.location_id AND l.company_id = :companyId
                    INNER JOIN `user` valuation_user ON valuation_user.id = va.user_id
                    INNER JOIN contact valuation_contact ON valuation_contact.id = valuation_user.contact_id
                    LEFT JOIN country_state t ON t.id = c.state";
        $where = [];
        if (isset($filter['makeInfo'])) {
            $makeAndWhere = [];
            foreach ($filter['makeInfo'] as $makeInfo) {
                $makeWhere = [];
                if (isset($makeInfo['make']) && $makeInfo['make']) {
                    $makeWhere[] = "ve.make = '" . $makeInfo['make']."'";
                }
                if (isset($makeInfo['model']) && $makeInfo['model']) {
                    $makeWhere[] = "ve.model_family = '" . $makeInfo['model']."'";
                }
                if (isset($makeInfo['variant']) && $makeInfo['variant']) {
                    $makeWhere[] = "ve.variant = '" . $makeInfo['variant']."'";
                }
                if ($makeWhere) {
                    $makeAndWhere[] = '(' . implode(' AND ', $makeWhere) . ')';
                }
            }
            if ($makeAndWhere) {
                $where[] = '(' . implode(' OR ', $makeAndWhere) . ')';
            }
        }

        if (isset($filter['odoRange'])) {
            if ($filter['odoRange']['from'] && $filter['odoRange']['to']) {
                $where[] = 've.odometer BETWEEN '.$filter['odoRange']['from'].' AND '.$filter['odoRange']['to'];
            }
        }

        if (isset($filter['yearRange'])) {
            if ($filter['yearRange']['from'] && $filter['yearRange']['to']) {
                $where[] = 'YEAR(ve.build_date) BETWEEN '.$filter['yearRange']['from'].' AND '.$filter['yearRange']['to'];
            }
        }

        if (isset($filter['vehicleLocation']) && $filter['vehicleLocation']) {
            $where[] = "c.state IN ('".implode("','", $filter['vehicleLocation'])."')";
        }

        if (isset($filter['transmisionType']) && $filter['transmisionType']) {
            $where[] = "ve.transmission IN ('".implode("','", $filter['transmisionType'])."')";
        }

        if (isset($filter['lastDay']) && $filter['lastDay']) {
            $where[] = "eo.create_date BETWEEN DATE_SUB(NOW(),INTERVAL ".$filter['lastDay']." DAY) AND NOW()";
        }

        if (isset($filter['dateRange'])) {
            if ($filter['dateRange']['from'] && $filter['dateRange']['to']) {
                $where[] = "eo.create_date BETWEEN '".$filter['dateRange']['from']."' AND ('".$filter['dateRange']['to'] ."' + INTERVAL 1 DAY)";
            }
        }

        $whereStr = '';
        if ($where) {
            $whereStr = 'WHERE (' . implode(' AND ', $where) . ')';
        }

        if ($orderDesc) {
            $orderBy = "ORDER BY eo.id DESC";
        } else {
            $orderBy = "ORDER BY eo.id";
        }
        $limitQuery = "LIMIT :offset, :limit";
        $sql = $query . ' ' . $whereStr . ' ' . $orderBy . ' ' . $limitQuery;
        $params = [
            'locationId' => $locationId,
            'companyId' => $companyId,
            'limit' => $limit,
            'offset' => $offset,
        ];
        $data = DB::select($sql, $params);
        return $data;
    }

    public static function getPurchaseHistory($status, $companyGroupId, $limit, $offset, $orderDesc = FALSE, $filter = [])
    {
        $statusStr = implode(',', $status);
        $query = "
                SELECT DISTINCT CONCAT_WS(' ', ve.make,ve.model_family,ve.series,ve.style,ve.engine_type,
					                           ve.engine_capacity,ve.engine_size,ve.variant,ve.cylinder,ve.transmission) offer_name,  
                 ve.model_family, YEAR(ve.build_date) year_build, ve.odometer,
                    RTRIM(ve.make) make, ve.id vehicle_id, ve.variant, ve.transmission,
                    eo.id external_offer_id, eo.create_date, eo.arrival_date, eo.`status` offer_status, va.estimated_recon_cost total_recon_cost, va.id valuation_id, c.state location, t.name location_text,
                    ob.bid_date, ob.bid_price, ob.expiry_date, ob.user_id buyer_id, CONCAT_WS(' ', c.first_name, c.last_name) buyer_name
		FROM valuation va
				INNER JOIN vehicle ve ON ve.id = va.vehicle_id
				INNER JOIN external_offer eo ON eo.valuation_id = va.id AND eo.`status` IN ($statusStr)
				LEFT JOIN offer_bid ob ON ob.external_offer_id = eo.id
				INNER JOIN `user` u ON u.id = ob.user_id
				INNER JOIN contact c ON c.id = u.contact_id
				INNER JOIN location l ON l.id = u.location_id
				INNER JOIN company com ON com.id = l.company_id AND com.company_group_id = :companyGroupId
				LEFT JOIN country_state t ON t.id = c.state";
        $where = [];
        if (isset($filter['makeInfo'])) {
            $makeAndWhere = [];
            foreach ($filter['makeInfo'] as $makeInfo) {
                $makeWhere = [];
                if (isset($makeInfo['make']) && $makeInfo['make']) {
                    $makeWhere[] = "ve.make = '" . $makeInfo['make']."'";
                }
                if (isset($makeInfo['model']) && $makeInfo['model']) {
                    $makeWhere[] = "ve.model_family = '" . $makeInfo['model']."'";
                }
                if (isset($makeInfo['variant']) && $makeInfo['variant']) {
                    $makeWhere[] = "ve.variant = '" . $makeInfo['variant']."'";
                }
                if ($makeWhere) {
                    $makeAndWhere[] = '(' . implode(' AND ', $makeWhere) . ')';
                }
            }
            if ($makeAndWhere) {
                $where[] = '(' . implode(' OR ', $makeAndWhere) . ')';
            }
        }

        if (isset($filter['odoRange'])) {
            if ($filter['odoRange']['from'] && $filter['odoRange']['to']) {
                $where[] = 've.odometer BETWEEN '.$filter['odoRange']['from'].' AND '.$filter['odoRange']['to'];
            }
        }

        if (isset($filter['yearRange'])) {
            if ($filter['yearRange']['from'] && $filter['yearRange']['to']) {
                $where[] = 'YEAR(ve.build_date) BETWEEN '.$filter['yearRange']['from'].' AND '.$filter['yearRange']['to'];
            }
        }

        if (isset($filter['vehicleLocation']) && $filter['vehicleLocation']) {
            $where[] = "c.state IN ('".implode("','", $filter['vehicleLocation'])."')";
        }

        if (isset($filter['transmisionType']) && $filter['transmisionType']) {
            $where[] = "ve.transmission IN ('".implode("','", $filter['transmisionType'])."')";
        }

        if (isset($filter['lastDay']) && $filter['lastDay']) {
            $where[] = "eo.create_date BETWEEN DATE_SUB(NOW(),INTERVAL ".$filter['lastDay']." DAY) AND NOW()";
        }

        if (isset($filter['dateRange'])) {
            if ($filter['dateRange']['from'] && $filter['dateRange']['to']) {
                $where[] = "eo.create_date BETWEEN '".$filter['dateRange']['from']."' AND ('".$filter['dateRange']['to'] ."' + INTERVAL 1 DAY)";
            }
        }

        $whereStr = '';
        if ($where) {
            $whereStr = 'WHERE (' . implode(' AND ', $where) . ')';
        }

        if ($orderDesc) {
            $orderBy = "ORDER BY eo.id DESC";
        } else {
            $orderBy = "ORDER BY eo.id";
        }
        $limitQuery = "LIMIT :offset, :limit";
        $sql = $query . ' ' . $whereStr . ' ' . $orderBy . ' ' . $limitQuery;
        $params = [
            'companyGroupId' => $companyGroupId,
            'limit' => $limit,
            'offset' => $offset,
        ];
        $data = DB::select($sql, $params);
        return $data;
    }

    public static function getSoldHistory($status, $companyGroupId, $limit, $offset, $orderDesc = FALSE, $filter = [])
    {
        $statusStr = implode(',', $status);
        $query = "
            SELECT DISTINCT CONCAT_WS(' ', ve.make,ve.model_family,ve.series,ve.style,ve.engine_type,
					                       ve.engine_capacity,ve.engine_size,ve.variant,ve.cylinder,ve.transmission) offer_name, 
                ve.model_family, YEAR(ve.build_date) year_build, ve.odometer,
                RTRIM(ve.make) make, ve.id vehicle_id, ve.variant, ve.transmission,
                eo.id external_offer_id, eo.create_date, eo.arrival_date, eo.`status` offer_status, va.estimated_recon_cost total_recon_cost, va.id valuation_id, c.state location, t.name location_text,
                ob.bid_date, ob.bid_price, ob.expiry_date, ob.user_id buyer_id, CONCAT_WS(' ', c.first_name, c.last_name) buyer_name
            FROM valuation va
                INNER JOIN vehicle ve ON ve.id = va.vehicle_id
                INNER JOIN external_offer eo ON eo.valuation_id = va.id AND eo.`status` IN ($statusStr)
                INNER JOIN offer_bid ob ON ob.external_offer_id = eo.id
                INNER JOIN user buyer ON buyer.id = ob.user_id
                INNER JOIN contact c ON c.id = buyer.contact_id
                INNER JOIN user seller ON seller.id = eo.creator_id
                INNER JOIN location l ON l.id = seller.location_id
                INNER JOIN company com ON com.id = l.company_id AND com.company_group_id = :companyGroupId
                LEFT JOIN country_state t ON t.id = c.state";
        $where = [];
        if (isset($filter['makeInfo'])) {
            $makeAndWhere = [];
            foreach ($filter['makeInfo'] as $makeInfo) {
                $makeWhere = [];
                if (isset($makeInfo['make']) && $makeInfo['make']) {
                    $makeWhere[] = "ve.make = '" . $makeInfo['make']."'";
                }
                if (isset($makeInfo['model']) && $makeInfo['model']) {
                    $makeWhere[] = "ve.model_family = '" . $makeInfo['model']."'";
                }
                if (isset($makeInfo['variant']) && $makeInfo['variant']) {
                    $makeWhere[] = "ve.variant = '" . $makeInfo['variant']."'";
                }
                if ($makeWhere) {
                    $makeAndWhere[] = '(' . implode(' AND ', $makeWhere) . ')';
                }
            }
            if ($makeAndWhere) {
                $where[] = '(' . implode(' OR ', $makeAndWhere) . ')';
            }
        }

        if (isset($filter['odoRange'])) {
            if ($filter['odoRange']['from'] && $filter['odoRange']['to']) {
                $where[] = 've.odometer BETWEEN '.$filter['odoRange']['from'].' AND '.$filter['odoRange']['to'];
            }
        }

        if (isset($filter['yearRange'])) {
            if ($filter['yearRange']['from'] && $filter['yearRange']['to']) {
                $where[] = 'YEAR(ve.build_date) BETWEEN '.$filter['yearRange']['from'].' AND '.$filter['yearRange']['to'];
            }
        }

        if (isset($filter['vehicleLocation']) && $filter['vehicleLocation']) {
            $where[] = "c.state IN ('".implode("','", $filter['vehicleLocation'])."')";
        }

        if (isset($filter['transmisionType']) && $filter['transmisionType']) {
            $where[] = "ve.transmission IN ('".implode("','", $filter['transmisionType'])."')";
        }

        if (isset($filter['lastDay']) && $filter['lastDay']) {
            $where[] = "eo.create_date BETWEEN DATE_SUB(NOW(),INTERVAL ".$filter['lastDay']." DAY) AND NOW()";
        }

        if (isset($filter['dateRange'])) {
            if ($filter['dateRange']['from'] && $filter['dateRange']['to']) {
                $where[] = "eo.create_date BETWEEN '".$filter['dateRange']['from']."' AND ('".$filter['dateRange']['to'] ."' + INTERVAL 1 DAY)";
            }
        }

        $whereStr = '';
        if ($where) {
            $whereStr = 'WHERE (' . implode(' AND ', $where) . ')';
        }

        if ($orderDesc) {
            $orderBy = "ORDER BY eo.id DESC";
        } else {
            $orderBy = "ORDER BY eo.id";
        }
        $limitQuery = "LIMIT :offset, :limit";
        $sql = $query . ' ' . $whereStr . ' ' . $orderBy . ' ' . $limitQuery;
        $params = [
            'companyGroupId' => $companyGroupId,
            'limit' => $limit,
            'offset' => $offset,
        ];
        $data = DB::select($sql, $params);
        return $data;
    }

    public static function filterQuery($types,
                                       $user,
                                       $ownStat = NULL,
                                       $locationIds = NULL,
                                       $createDate = NULL) {

        $customerId = $user->id;
        $query = ExternalOffer::select('external_offer.*');
//        $ids = User::usersSameCompanyQuery($user->company_group_id)->select('user.id')->get();

        //filter by status
        $status = NULL;
        switch ($types) {
            case Dashboard::TOTAL_OUTBOUND_OFFER_REQUESTS:
                $status = [
                    ExternalOffer::STATUS_PENDING,
                    ExternalOffer::STATUS_OFFER_MADE,
                    ExternalOffer::STATUS_AWAITING_RESPONSE,
                    ExternalOffer::STATUS_ACCEPTED,
                    ExternalOffer::STATUS_LOCATION_SET,
                    ExternalOffer::STATUS_VEHICLE_READY_TO_SHIP,
                    ExternalOffer::STATUS_TRANSPORT_TRIGGERED,
                    ExternalOffer::STATUS_SHIPPED,
                    ExternalOffer::STATUS_DELIVERED
                ];
                $query->whereIn('external_offer.status', $status);
                break;
            case Dashboard::TOTAL_OUTBOUND_OFFER_REQUESTS_ACCEPTED:
                $status = [ExternalOffer::STATUS_ACCEPTED];
                $query->whereIn('external_offer.status', $status);
                break;
            case Dashboard::TOTAL_OUTBOUND_CARS_SOLD:
                $query->join('transaction', 'transaction.source_id', '=', "external_offer.id")
                      ->where('transaction.source', '=', Transaction::SOURCE_EXTERNAL_OFFER)
                      ->where('transaction.status', '=', Transaction::STATUS_COMPLETE);
//                      ->whereIn('transaction.seller_id', $ids);
                break;
            case Dashboard::TOTAL_BIDS_MADE:
                $query->rightJoin('bid_history', 'external_offer.id', '=', 'bid_history.trade_id')
                      ->where('bid_history.trade_type', '=', BidHistory::EXTERNAL_OFFER);
//                      ->whereIn('bid_history.user_id', $ids);
                break;
            case Dashboard::TOTAL_BIDS_WON:
                $externalOfferTimeout = AppProperty::where('id', AppProperty::EXTERNAL_OFFER_TIMEOUT)->first()->value;
                $dateEndOffer = (new DateTime)->modify("-$externalOfferTimeout minutes")->format('Y-m-d H:i:s');

                $bidHistoryType = BidHistory::EXTERNAL_OFFER;
                    $bidHistoryMaxPrice = DB::raw("(Select bid_history.*,
                                                           MAX(bid_history.bid_price) max_price
                                                    From bid_history
                                                    Where trade_type = '$bidHistoryType'
                                                    Group By bid_history.trade_id)
                                                    bid_max_price");

                $query->where('external_offer.create_date', '<', $dateEndOffer)
                      ->join($bidHistoryMaxPrice, function($join) {
                        $join->on('bid_max_price.trade_id', '=', 'external_offer.id');
                      });
//                      ->whereIn('bid_max_price.user_id', $ids);

                break;
            case Dashboard::TOTAL_BID_ACCEPTED:
                $status = [
                    ExternalOffer::STATUS_ACCEPTED,
                    ExternalOffer::STATUS_LOCATION_SET,
                    ExternalOffer::STATUS_VEHICLE_READY_TO_SHIP,
                    ExternalOffer::STATUS_TRANSPORT_TRIGGERED,
                    ExternalOffer::STATUS_SHIPPED,
                    ExternalOffer::STATUS_DELIVERED
                ];
                $query->whereIn('external_offer.status', $status)
                      ->join('offer_bid', 'external_offer.id', 'offer_bid.external_offer_id');
//                      ->whereIn('offer_bid.user_id', $ids);
                break;
            case Dashboard::TOTAL_INBOUND_OFFERS_BOUGHT:
                $query->join('transaction', 'transaction.source_id', '=', "external_offer.id")
                    ->where('transaction.source', '=', Transaction::SOURCE_EXTERNAL_OFFER)
                    ->where('transaction.status', '=', Transaction::STATUS_COMPLETE);
//                    ->whereIn('transaction.buyer_id', $ids);
                break;
        }

        //filter by location
        if ($locationIds) {
            switch($types) {
                case Dashboard::TOTAL_OUTBOUND_OFFER_REQUESTS:
                case Dashboard::TOTAL_OUTBOUND_OFFER_REQUESTS_ACCEPTED:
                    $query->join('user as creator', 'creator.id', '=', 'external_offer.creator_id')
                        ->whereIn('creator.location_id', $locationIds);
                    break;
                case Dashboard::TOTAL_OUTBOUND_CARS_SOLD:
                    $query->join('user as creator', 'creator.id', '=', 'transaction.seller_id')
                        ->whereIn('creator.location_id', $locationIds);
                    break;
                case Dashboard::TOTAL_BIDS_MADE:
                    $query->join('user as creator', 'creator.id', '=', 'bid_history.user_id')
                        ->whereIn('creator.location_id', $locationIds);
                    break;
                case Dashboard::TOTAL_BIDS_WON:
                    $query->join('user as creator', 'creator.id', '=', 'bid_max_price.user_id')
                        ->whereIn('creator.location_id', $locationIds);
                    break;
                case Dashboard::TOTAL_BID_ACCEPTED:
                $query->join('user as creator', 'creator.id', '=', 'offer_bid.user_id')
                    ->whereIn('creator.location_id', $locationIds);
                    break;
            }
        }

        //filter ownStat
        if ($ownStat) {
            $query->where('external_offer.creator_id', $user->id);
        }

        //filter date range
        if ($createDate && $createDate['start'] && $createDate['end']) {
            $times = [$createDate['start'], $createDate['end']];
            $query->whereBetween('external_offer.create_date', $times);
        }

        return $query;
    }

    public static function percentageBidWon($user, $ownStat = NULL, $locationId = NULL, $createDate = NULL) {
        $bidMade    = ExternalOffer::filterQuery(Dashboard::TOTAL_BIDS_MADE,
                                                 $user,
                                                 $ownStat,
                                                 $locationId,
                                                 $createDate)->count(DB::raw('distinct external_offer.id'));
        $bidWon     = ExternalOffer::filterQuery(Dashboard::TOTAL_BIDS_WON,
                                                 $user,
                                                 $ownStat,
                                                 $locationId,
                                                 $createDate)->count(DB::raw('distinct external_offer.id'));
        return $bidMade > 0 ? floatval(number_format($bidWon / $bidMade * 100, 2))."%" : "0%";
    }
}
