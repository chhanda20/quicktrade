<?php

namespace App;

use App\Libraries\ErrorCodes;
use App\Libraries\Event;
use App\Libraries\Helper;
use App\Libraries\PDFGeneratorService;
use Illuminate\Support\Facades\DB;
use WindowsAzure\Common\ServicesBuilder;
use App\RegionCommon;
use App\Location;

class Transaction extends Backend
{

    const STATUS_WAITING_PAYMENT = 1;
    const STATUS_WAITING_TRANSPORT = 2;
//    const STATUS_WAITING_ARRIVAL = 3;
    const STATUS_IN_DISPUTE = 4;
    const STATUS_COMPLETE = 5;
    const STATUS_DISPUTE_RESOLVE = 6;
    const STATUS_ACCEPTED = 7;
    const STATUS_IN_INSPECTION = 8;

    const STATUS_LOCATION_SET = 10;

    const STATUS_PAYMENT_RECEIVED = 20;
//    const STATUS_PREPARING_VEHICLE = 21;
    const STATUS_BOOKING_TRANSPORT = 22;
    const STATUS_TRANSPORT_BOOKED = 23;
    const STATUS_IN_TRANSIT = 24;
//    const STATUS_SHIPPED = 25;
    const STATUS_ARRIVED = 3;

    const STATUS_DOCUMENTING = 26;
    const STATUS_DOCUMENTED = 27;
    const STATUS_RETURNING = 28;
    const STATUS_RETURNED = 29;


    const SOURCE_EXTERNAL_OFFER = "external_offer";
    const SOURCE_LIVE_AUCTION = "live_auction";//In transaction: store auction Session id as source id
    const SOURCE_AUCTION_BUY_NOW = "auction_buy_now";//In transaction: store auction item id as source id

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'transaction';


    public static function updateStatus($id, $status)
    {
        if ($status == Transaction::STATUS_DOCUMENTING) {
            $unsolved = Dispute::where('transaction_id', $id)->where('status', '<>', Dispute::STATUS_RESOLVE)->count();
            if ($unsolved == 0) {
                $updated = Transaction::where('id', $id)->update(['status' => $status]);
                return $updated;
            }
        }
    }

    public function getValuation()
    {
        if ($this->valuation_id && $this->valuation_version) {
            return Valuation::findByIdAndVersion($this->valuation_id, $this->valuation_version);
        }
        throw new \ErrorException("No valuation_id || valuation_version");
    }

    public function getVehicle()
    {
        $v = $this->getValuation();
        if (!$v) {
            throw new \ErrorException("Can't find valuation");
        }
        return $v->getVehicle();
    }

    public function setPaymentReceived($amount) {
        if ($this->status != Transaction::STATUS_WAITING_PAYMENT) {
            return [
                'error' => ErrorCodes::INVALID,
                'msg' => 'No in waiting payment status'
            ];
        }

        $this->status               = Transaction::STATUS_PAYMENT_RECEIVED;
        $this->payment_receive      = $amount;
        $this->payment_receive_time = date('Y-m-d H:i:s');
        $this->save();

        return null;
    }


    public static function updateTransactionStatusBySourceId($source_type, $sourceId, $status)
    {
        return DB::statement("update transaction set status=$status WHERE source = '$source_type' AND source_id = $sourceId");
    }


    public static function getOneById($transactionId){
        $transactionLiveAuction = Transaction::SOURCE_LIVE_AUCTION;
        $transactionBuyNow = Transaction::SOURCE_AUCTION_BUY_NOW;

        $result = DB::select("select t.id, t.rid, t.valuation_id,t.valuation_version,
		CONCAT_WS(' ',veh.make,veh.model_family,veh.series,veh.style,veh.engine_type,
																			veh.engine_capacity,veh.engine_size,veh.variant,veh.cylinder,veh.transmission) AS 'vehicle_name',
		 veh.registration_number as rego,t.transport_no as transportNo,t.status,t.seller_location,t.buyer_location,

          CONCAT(if(t.source IN ('$transactionBuyNow','$transactionLiveAuction'),'A','P'),t.source_id) as auctionNo,

                            'buying' as buying_dealer, 'selling' as selling_dealer, 
                            t.vehicle_price sell_vehicle_price, t.seller_fee as sell_moto_fee, 
                            t.vehicle_price buy_vehicle_price, t.buyer_fee as buy_moto_fee, t.shipping_price as buy_transport, t.payment_receive as buy_payment_made
                            from `transaction` t 

                            INNER JOIN type_code ON t.`status` = type_code.`code` AND type_code.type = 'TRANSACTION_STATUS'
                            LEFT JOIN valuation va ON va.id=t.valuation_id
                            LEFT JOIN vehicle veh ON veh.id=va.vehicle_id
                            
                            where t.id = $transactionId           
              ");
        if($result){
            return $result[0];
        }else{
            return null;
        }
    }

    public static function setMotoFee($transaction){
        $appPropertyModel = new AppProperty();
        $motoFeeSettings = [
            AppProperty::BUYER_MOTO_FEE,
            AppProperty::SELLER_MOTO_FEE
        ];
        $motoFees = $appPropertyModel->getAppProperty($motoFeeSettings);

        foreach ($motoFees as $motoFee){
            if($motoFee->id == AppProperty::SELLER_MOTO_FEE){
                $transaction->seller_fee = $motoFee->value;
            }else{
                $transaction->buyer_fee = $motoFee->value;
            }
        }

        //todo calculate transport fee
        $buyerLocation = $transaction->buyer_location;
        $sellerLocation = $transaction->seller_location;
        //get region id of buyer/seller location
        $buyerPostcode = Location::find($buyerLocation)->postcode;
        $sellerPostcode = Location::find($sellerLocation)->postcode;
        if($buyerPostcode && $sellerPostcode){
            $buyerRegion = RegionCommon::getRegionByPostcode($buyerPostcode);
            $sellerRegion = RegionCommon::getRegionByPostcode($sellerPostcode);
            //get transport price
            $min = min($buyerRegion, $sellerRegion);
            $max = max($sellerRegion, $buyerRegion);
            $result = DB::select("SELECT * FROM transport_price WHERE from_region = $min AND to_region = $max");
            $transaction->shipping_price = $result[0]->price;
        }
    }

    public function transportPrice() {
        //todo calculate transport fee
        $buyerLocation = $this->buyer_location;
        $sellerLocation = $this->seller_location;
        //get region id of buyer/seller location
        $buyerPostcode = Location::find($buyerLocation)->postcode;
        $sellerPostcode = Location::find($sellerLocation)->postcode;
        if($buyerPostcode && $sellerPostcode){
            $buyerRegion = RegionCommon::getRegionByPostcode($buyerPostcode);
            $sellerRegion = RegionCommon::getRegionByPostcode($sellerPostcode);
            //get transport price
            $min = min($buyerRegion, $sellerRegion);
            $max = max($sellerRegion, $buyerRegion);
            $result = DB::select("SELECT * FROM transport_price WHERE from_region = $min AND to_region = $max");
            return $result[0]->price;
        } else {
            return null;
        }
    }
}
