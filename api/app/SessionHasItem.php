<?php

namespace App;

use Illuminate\Support\Facades\DB;

class SessionHasItem extends Backend
{
    const STATUS_PENDING = 1;
    const STATUS_APPROVED = 2;
    const STATUS_REJECTED = 3;
    const STATUS_SOLD_OUT = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    const CREATED_AT =  null;
    const UPDATED_AT = null;

    protected $hidden = [];

    protected $table = 'session_has_item';
}
