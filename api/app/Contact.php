<?php
/**
 * Created by PhpStorm.
 * User: ntnam
 * Date: 11/15/16
 * Time: 11:41
 */

namespace App;


use App\Libraries\ErrorCodes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Contact extends Backend
{
    const STATUS_CODE_INACTIVE = 1;
    const STATUS_CODE_ACTIVE = 2;

    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;
    const GENDER_UNKNOWN = 3;

    const COMPANY_TYPE_COMPANY = 1;
    const COMPANY_TYPE_NON_PROFIT = 2;
    const COMPANY_TYPE_GOVERNMENT = 3;
    const COMPANY_TYPE_FLEET = 4;
    const COMPANY_TYPE_DEALER = 5;
    const COMPANY_TYPE_WHOLESALER = 6;
    const COMPANY_TYPE_FINANCE_INSURANCE_WARRANTY = 7;

    const TAX_TYPE_TAXABLE = 1;
    const TAX_TYPE_EXEMPTED = 2;
    const TAX_TYPE_TOTAL_PERMANENT_INJURY = 3;

    const PREFERRED_CONTACT_METHOD_EMAIL = 1;
    const PREFERRED_CONTACT_METHOD_MOBILE = 2;
    const PREFERRED_CONTACT_METHOD_HOME_PHONE = 3;
    const PREFERRED_CONTACT_METHOD_WORK_PHONE = 4;
    const PREFERRED_CONTACT_METHOD_POSTAL = 5;
    const PREFERRED_CONTACT_METHOD_FAX = 6;

    const PROSPECT_LEVEL_HOT = 1;
    const PROSPECT_LEVEL_WARM = 2;
    const PROSPECT_LEVEL_COLD = 3;

    public function full_name() {
        return $this->first_name." ".$this->last_name;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'gender', 'first_name', 'last_name', 'status_code', 'privacy_flag',
        'privacy_flag_update_time', 'company_name', 'company_type', 'ABN', 'ACN',
        'tax_type', 'address1', 'address2', 'suburb', 'state', 'postcode', 'country',
        'postal_address1', 'postal_address2', 'postal_state', 'postal_suburb', 'postal_postcode', 'postal_country',
        'email', 'email_alternate', 'mobile_phone', 'home_phone', 'work_phone', 'fax_number',
        'preferred_contact_method', 'driver_license_number', 'dealer_license_number', 'fleet_number',
        'bank_branch_name', 'bank_account_name', 'bank_bsb_number', 'bank_account_number', 'note', 'prospect_level',
        'salesperson_id', 'interested_vehicle'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    protected $table = 'contact';


    public static function validator($values)
    {
        return Validator::make($values, [
            'title' => 'integer',
            'gender' => 'integer',
            'first_name' => 'required|max:45',
            'last_name' => 'max:45',
            'status_code' => 'integer',
            'privacy_flag' => 'integer',
            'privacy_flag_update_time' => 'max:50',
            'company_name' => 'max:255',
            'company_type' => 'integer',
            'ABN' => 'max:20',
            'ACN' => 'max:20',
            'tax_type' => 'integer',
            'address1' => 'required|max:255',
            'address2' => 'max:255',
            'suburb' => 'required|max:45',
            'state' => 'required|max:45',
            'postcode' => 'max:10',
            'country' => 'max:45',
            'postal_address1' => 'max:255',
            'postal_address2' => 'max:255',
            'postal_state' => 'max:45',
            'postal_suburb' => 'max:45',
            'postal_postcode' => 'max:10',
            'postal_country' => 'max:45',
            'email' => 'max:255',
            'email_alternate' => 'max:45',
            'mobile_phone' => 'max:20',
            'home_phone' => 'max:20',
            'work_phone' => 'max:20',
            'fax_number' => 'max:20',
            'preferred_contact_method' => 'integer',
            'driver_license_number' => 'max:20',
            'dealer_license_number' => 'max:20',
            'fleet_number' => 'max:20',
            'bank_branch_name' => 'max:45',
            'bank_account_name' => 'max:255',
            'bank_bsb_number' => 'max:10',
            'bank_account_number' => 'max:20',
            'note' => '',
            'salesperson_id' => 'integer',
            'updater' => 'integer',
            'prospect_level' => 'integer',
//            'company_id' => 'integer',
            'interested_vehicle' => 'integer'
        ], ErrorCodes::VALIDATE_MESSAGE_CONVERTER);
    }

    public static function search($company_group_id, $offset, $limit, $order_field, $order_desc, $query = NULL, $contactNotLinkUser = false, $companyContractOnly = false)
    {
        $order = '';
        if ($order_field) {
            if ($order_desc) {
                $order = " ORDER BY $order_field ";
            } else {
                $order = " ORDER BY $order_field DESC ";
            }
        }




        if ($query) {
            $query = preg_replace("/[^A-Za-z0-9]/", ' ', $query);
            $query = trim($query);
        }

        $where = "";

        if($company_group_id){
            $where .= " AND c.company_group_id = $company_group_id ";
        }
        if($contactNotLinkUser){
            $where .= " AND (c.id not in (SELECT contact_id from `user`) AND c.id not in (SELECT contact_id from `company`))";
        }

        if ($companyContractOnly) {
            $where .= " AND (c.id in (SELECT contact_id from `company`))";
        }

        if ($query) {
            $query = $query . '*';
            $sql = DB::select("
                SELECT c.id,c.first_name,c.last_name,c.address1,c.address2,c.home_phone,c.work_phone,c.mobile_phone,c.email,c.salesperson_id,
                c.company_name,c.title,c.gender,c.suburb,c.postcode,c.country,c.state,c.privacy_flag,c.note,
                location.id 'location_id', location.name 'location_name',
                seller_contact.first_name 'salesperson_first_name', seller_contact.last_name 'salesperson_last_name',
                c.company_group_id 'company_group_id',location.company_id 'company_id'
                FROM contact c
                LEFT JOIN `user` u ON c.salesperson_id=u.id
                LEFT JOIN contact seller_contact ON u.contact_id=seller_contact.id
                LEFT JOIN location ON u.location_id=location.id
                WHERE
                MATCH(c.first_name, c.last_name, c.address1, c.address2, c.mobile_phone, c.home_phone, c.work_phone, c.company_name) against(? IN BOOLEAN MODE)
                AND c.status_code=? $where
                $order LIMIT ?, ?
            ",[$query,  Contact::STATUS_CODE_ACTIVE, $offset, $limit]) ;

            return $sql;
        }


        $sql = DB::select("
                SELECT c.id,c.first_name,c.last_name,c.address1,c.address2,c.home_phone,c.work_phone,c.mobile_phone,c.email,c.salesperson_id,
                c.company_name,c.title,c.gender,c.suburb,c.postcode,c.country,c.state,c.privacy_flag,c.note,
                location.id 'location_id', location.name 'location_name',
                seller_contact.first_name 'salesperson_first_name', seller_contact.last_name 'salesperson_last_name',
                c.company_group_id 'company_group_id',location.company_id 'company_id'
                FROM contact c
                LEFT JOIN `user` u ON c.salesperson_id=u.id
                LEFT JOIN contact seller_contact ON u.contact_id=seller_contact.id
                LEFT JOIN location ON u.location_id=location.id
                WHERE
                c.status_code=? $where
                $order LIMIT ?, ?
        ",
            [ Contact::STATUS_CODE_ACTIVE, $offset, $limit]);

        return $sql ;

    }

    public static function searchCount($company_group_id, $query = NULL, $contactNotLinkUser = false, $companyContractOnly = false)
    {
        if ($query) {
            $query = preg_replace("/[^A-Za-z0-9]/", ' ', $query);
            $query = trim($query);
        }
        $where = "";
        if($company_group_id){
            $where .= " AND contact.company_group_id = $company_group_id ";
        }
        if($contactNotLinkUser){
            $where .= " AND (contact.id not in (SELECT contact_id from `user`) AND contact.id not in (SELECT contact_id from `company`))";
        }

        if ($companyContractOnly) {
            $where .= " AND (contact.id in (SELECT contact_id from `company`))";
        }

        if ($query) {
            $query = $query . '*';
            return DB::select('SELECT COUNT(*) as `count` FROM contact '
                . ' WHERE MATCH(first_name, last_name, address1, address2, mobile_phone, home_phone, work_phone, company_name) against(? IN BOOLEAN MODE)'
                . ' AND status_code=? '.$where,
                [$query,  Contact::STATUS_CODE_ACTIVE])[0];
        }
        return DB::select('SELECT COUNT(*) as `count` FROM contact WHERE status_code=? '.$where,
            [ Contact::STATUS_CODE_ACTIVE])[0];

    }
}
