<?php

namespace App;

use Illuminate\Support\Facades\DB;

class CompanyGroup extends Backend
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 2;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['name', 'status_code', 'description'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'company_group';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;
}
