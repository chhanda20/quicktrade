<?php

namespace App;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use MicrosoftAzure\Storage\Common\ServicesBuilder;
use App\Libraries\PDFTemplate;
use App\Libraries\Event;
use App\Transaction;
use App\Valuation;
use App\Vehicle;
use Carbon\Carbon;

class Document extends Backend
{
    const TRANSPORT_FEE                 = 'transport-fee';
    const TRANSPORT_FEE_RETURN_VEHICLE  = 'transport-fee-return-vehicle';
    const FEE                           = 'fee';
    const CREDIT_NOTE_COST_VARIANCE     = 'credit-note-cost-variance';
    const CREDIT_NOTE_RETURN_VEHICLE    = 'credit-note-return-vehicle';
    const PAYMENT_RECEIPT               = 'payment-receipt';

    const DOCUMENT_TYPE_TRANSACTION     = "transaction";
    const DOCUMENT_TYPE_PAYMENT_RECEIPT = "payment-receipt";
    const DOCUMENT_TYPE_CREDIT_NOTE     = "credit-note";
    const DOCUMENT_TYPE_DEBIT_NOTE      = "debit-note";

    protected $table = 'document';

    protected $fillable = ['document_type', 'source_id', 'document_uploaded'];

    static public function generate($user, $documentType, $sourceId, $userType = null) {
        $data   = Document::documentData($documentType, $sourceId, $userType);
        $pdf    = PDFTemplate::generate($user, $documentType, $data);

        return $pdf;
    }

    static public function documentData($documentType, $sourceId, $userType = null) {
        $data       = [];
        $location   = '';
        $now        = Carbon::now();
        $dateTime   = $now->toDateString();

        switch ($documentType) {
            case Document::TRANSPORT_FEE:
            case Document::FEE:
            case Document::PAYMENT_RECEIPT:
                $transaction        = Transaction::find($sourceId);
                $valuation          = Valuation::find($transaction->valuation_id);
                $vehicle            = Vehicle::find($valuation->vehicle_id);
                $userId             = $userType === 'buyer' ? $transaction->buyer_id : $transaction->seller_id;
                $location           = Location::find(User::find($userId)->location_id)->address;
                $now                = Carbon::now();
                $dateTime           = $now->toDateString();
                $title              = strtoupper(str_replace("-", " ", $documentType));

                //Year + CATERHAM SEVEN MULTI POINT F/INJ 1999 2.0L SUPERLIGHT SV R300 4 + Color
                $year               = explode('-', $vehicle->build_date)[0];
                $color              = TypeCode::where('type', 'COLOR_TYPE')->where('code', $vehicle->colour)->first()->value;
                $offerName          = $vehicle->offerName();

                if ($userType === 'creator') {
                    $data[]         = ['item' => 'VEHICLE', 'description' => "$year $offerName $color", 'amount' => $valuation->trade_price];
                }
                if ($userType === 'creator' || $userType === 'buyer') {
                    $data[]         = ['item' => 'TRANSPORT', 'description' => "Transport", 'amount' => $transaction->transportPrice()];
                }
                if ($documentType == Document::FEE) {
                    $data[]         = ['item' => 'FEE', 'description' => "Moto Seller Fee", 'amount' => AppProperty::find('seller_moto_fee')->value];
                } else {
                    $data[]         = ['item' => 'FEE', 'description' => "Moto Buyer Fee", 'amount' => AppProperty::find('buyer_moto_fee')->value];
                }
                
                $gst                = 0;
                foreach ($data as $item) {
                    $gst += 0.1 * $item['amount'];
                }

                $data                           = ['items' => $data];
                $data['summary']                = ['gst' => $gst];
                $data['summary']['total']       = 11 * $gst;
                $data['summary']['amount paid'] = $documentType == Document::PAYMENT_RECEIPT ? 0 : 11 * $gst;
                $data['summary']['amount due']  = $documentType == Document::PAYMENT_RECEIPT ? 11 * $gst : 0;
                $data['title']                  = $title;
                break;
            case Document::CREDIT_NOTE_COST_VARIANCE:
            case Document::TRANSPORT_FEE_RETURN_VEHICLE:
                $title                          = $documentType === Document::CREDIT_NOTE_COST_VARIANCE ? 'CREDIT NOTE' : 'TRANSPORT FEE';
                $cost = DB::select("
                    SELECT SUM(repair_cost) as total_repair_cost
                    FROM dispute_item di
                    LEFT JOIN inspection_item ii ON ii.id=di.inspection_item_id
                    LEFT JOIN type_code tc ON (tc.code=ii.section_type AND tc.type IN ('EXTERIOR_SECTION','INTERIOR_SECTION','MECHANICAL_SECTION'))
                    WHERE di.inspection_item_id IS NOT NULL AND  di.dispute_id=$sourceId 
                ");
                $disputeCost = $cost && count($cost) ? $cost[0]->total_repair_cost : 0;

                $gst                            = 0;
                $data                           = [];
                $data[]                         = ['item' => 'DISPUTE', 'description' => "Description with data", 'amount' => $disputeCost];
                $ids                            = Dispute::join('transaction', 'dispute.transaction_id', 'transaction.id')
                                                    ->where('dispute.id', $sourceId)
                                                    ->select('transaction.buyer_id', 'transaction.seller_id', 'transaction.id as transaction_id')->first();
                $transaction                    = Transaction::find($ids->transaction_id);
                $userId                         = $userType === 'buyer' ? $ids->buyer_id : $ids->seller_id;
                $location                       = Location::find(User::find($userId)->location_id)->address;

                if ($documentType == Document::TRANSPORT_FEE_RETURN_VEHICLE) {
                    $data[] = ['item' => 'TRANSPORT', 'description' => "Transport", 'amount' => $transaction->transportPrice() * 2];
                }
                
                foreach ($data as $item) {
                    $gst += 0.1 * $item['amount'];
                }

                $data                           = ['items' => $data];
                $data['summary']                = ['gst' => $gst];
                $data['summary']['total']       = 11 * $gst;
                $data['summary']['amount paid'] = 0;
                $data['summary']['amount due']  = 11 * $gst;
                $data['title']                  = $title;
                
                break;
            case Document::CREDIT_NOTE_RETURN_VEHICLE:
                $title                          = 'CREDIT NOTE';
                $cost = DB::select("
                    SELECT SUM(repair_cost) as total_repair_cost
                    FROM dispute_item di
                    LEFT JOIN inspection_item ii ON ii.id=di.inspection_item_id
                    LEFT JOIN type_code tc ON (tc.code=ii.section_type AND tc.type IN ('EXTERIOR_SECTION','INTERIOR_SECTION','MECHANICAL_SECTION'))
                    WHERE di.inspection_item_id IS NOT NULL AND  di.dispute_id=$sourceId 
                ");
                $disputeCost = $cost && count($cost) ? $cost[0]->total_repair_cost : 0;
                
                $gst                            = 0;
                $data                           = [];
                $data[]                         = ['item' => 'DISPUTE', 'description' => "Description with data", 'amount' => $disputeCost];
                $ids                            = Dispute::join('transaction', 'dispute.transaction_id', 'transaction.id')
                                                    ->where('dispute.id', $sourceId)
                                                    ->select('transaction.buyer_id', 'transaction.seller_id', 'transaction.id as transaction_id')->first();
                $transaction                    = Transaction::find($ids->transaction_id);
                $userId                         = $userType === 'buyer' ? $ids->buyer_id : $ids->seller_id;
                $location                       = Location::find(User::find($userId)->location_id)->address;
                
                $data[]                         = ['item' => 'SELLER FEE', 'description' => "Moto Seller Fee", 'amount' => AppProperty::find('seller_moto_fee')->value];
                $data[]                         = ['item' => 'BUYER FEE', 'description' => "Moto Buyer Fee", 'amount' => AppProperty::find('buyer_moto_fee')->value];
                $data[]                         = ['item' => 'TRANSPORT', 'description' => "Transport", 'amount' => $transaction->transportPrice() * 2];
                
                foreach ($data as $item) {
                    $gst += 0.1 * $item['amount'];
                }

                $data                           = ['items' => $data];
                $data['summary']                = ['gst' => $gst];
                $data['summary']['total']       = 11 * $gst;
                $data['summary']['amount paid'] = 0;
                $data['summary']['amount due']  = 11 * $gst;
                $data['title']                  = $title;
                break;
            default:
                return null;
        }

        $data['bill to'] = $location;
        $data['date']    = $dateTime;

        return $data;
    }

    static public function documentType($event, $userType) {
        //Check user-type buyer/seller/creator... and map with event to specifile what document need to be created
        $documentType = null;
        switch ($event) {
            case Event::TRANSACTION_NEW:
                return Document::PAYMENT_RECEIPT;
            case Event::TRANSACTION_RESOLVE_DISPUTE_PAY_COST_VARIANT:
            case Event::TRANSACTION_RESOLVE_DISPUTE_NO_CHANGE:
            case Event::TRANSACTION_VEHICLE_ACCEPT:
                switch ($userType) {
                    case 'buyer':
                        return Document::TRANSPORT_FEE;
                    case 'seller':
                        return Document::FEE;
                }
                return Document::CREDIT_NOTE_COST_VARIANCE;
            case Event::TRANSACTION_RESOLVE_DISPUTE_RETURN_VEHICLE:
                switch ($userType) {
                    case 'buyer':
                        return Document::CREDIT_NOTE_RETURN_VEHICLE;
                    case 'seller':
                        return Document::TRANSPORT_FEE_RETURN_VEHICLE;
                }
        }

        return null;
    }

    public static function generatorPDFAndUploadAzure($userId, $event, $sourceId, $userType) {
        $user = User::find($userId);
        $documentType = Document::documentType($event, $userType);
        $pdf = Document::generate($user, $documentType, $sourceId, $userType);
        $attach = Document::uploadToAzure($documentType, $pdf, $sourceId);
        \LOG::debug('genrate document at: ['.$attach.']');
        return $attach;
    }

    public static function findBySourceId($documentType, $sourceId)
    {
        $result = DB::select("SELECT * from document where document_type = '$documentType' AND source_id = $sourceId order by update_date desc");
        if ($result) {
            return $result[0];
        } else {
            return NULL;
        }
    }

    public static function updateDocument($stringToInsert, $documentType, $transactionId)
    {
        DB::statement("UPDATE document SET document_uploaded = '$stringToInsert' where document_type = '$documentType' AND source_id = $transactionId");
    }

    public static function uploadToAzure($type, $receiptContent, $sourceId, $fileName = NULL, $saveDocumentToDataSource = true) {
        $accountKey= env('azure_account_key');
        $accountName=env('azure_account_name');

        $connectionString   = "DefaultEndpointsProtocol=https;AccountName=$accountName;AccountKey=$accountKey";
        $blobRestProxy      = ServicesBuilder::getInstance()->createBlobService($connectionString);
        $blob_name          = $fileName ? $fileName : uniqid().'.pdf';
        $blob_container     = $type;
        $url = "https://$accountName.blob.core.windows.net/$blob_container/$blob_name";

        try    {
            //Upload blob
            $blobRestProxy->createBlockBlob($blob_container, $blob_name, $receiptContent);
            if ($saveDocumentToDataSource) {
                $param = [
                    'document_type'     => $type,
                    'source_id'         => $sourceId,
                    'document_uploaded' => $url
                ];
                Document::create($param);
            }
        }
        catch(ServiceException $e) {
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();;
            \LOG::debug(sprintf('--> 998'.$code.": ".$error_message."<br />"));
        }
        return $url;
    }
}
