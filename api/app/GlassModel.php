<?php

namespace App;

use App\Libraries\ErrorCodes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class GlassModel extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    protected $table = 'glass_make';

    public function getFamilyList($makeName)
    {
        $sql = "
            SELECT DISTINCT RTRIM(family) family
            FROM glass_model
            WHERE make = :makeName
            ORDER BY family";
        $params = ['makeName' => $makeName];
        $data = DB::select($sql,$params);
        return $data;
    }

    public function getVariant($makeName, $family)
    {
        $sql = "
            SELECT DISTINCT RTRIM(variant) variant
            from glass_model
            WHERE make = :makeName AND family = :family
            ORDER BY variant";
        $params = [
            'makeName' => $makeName,
            'family' => $family,
        ];
        $data = DB::select($sql,$params);
        return $data;
    }

    public function getReleaseYear($makeName, $family, $variant)
    {
        $sql = "
            SELECT DISTINCT release_year
            FROM glass_model
            WHERE make = :makeName AND family = :family AND variant = :variant
            ORDER BY release_year";
        $params = [
            'makeName' => $makeName,
            'family' => $family,
            'variant' => $variant,
        ];
        $data = DB::select($sql,$params);
        return $data;
    }

    public function getReleaseMonth($makeName, $family, $variant, $releaseYear)
    {
        $sql = "
            SELECT DISTINCT release_month
            FROM glass_model
            WHERE make = :makeName AND family = :family AND variant = :variant AND release_year = :releaseYear
            ORDER BY release_month";
        $params = [
            'makeName' => $makeName,
            'family' => $family,
            'variant' => $variant,
            'releaseYear' => $releaseYear,
        ];
        $data = DB::select($sql,$params);
        return $data;
    }

    public function getModelDetail($makeName, $family, $releaseYear, $releaseMonth, $variant)
    {
        $params = [
            'makeName' => $makeName,
            'family' => $family,
            'releaseYear' => $releaseYear,
            'releaseMonth' => $releaseMonth,
            'variant' => $variant,
        ];
        $sql = "
            SELECT RTRIM(gm.style) body_type, RTRIM(gm.engine_size) engine_size, RTRIM(gm.engine_capacity) engine_capacity, RTRIM(gm.cylinder) cylinder, RTRIM(gm.transmission) transmission, glass_code
            FROM glass_model gm
            WHERE gm.make = :makeName AND gm.family = :family AND gm.release_year = :releaseYear AND gm.release_month = :releaseMonth AND gm.variant = :variant
            LIMIT 0,1";
        $data = DB::select($sql,$params);
        if ($data) {
            return $data[0];
        }
        return NULL;
    }

}
