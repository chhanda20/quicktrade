<?php

namespace App;


use Illuminate\Support\Facades\DB;

class DataAccess extends Backend
{
    protected $table = 'data_access';

    const LOCATION_TYPE = 2;
    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;
    public static function getByUserId($userId)
    {
        $result = DB::select("
            SELECT * FROM data_access WHERE user_id = $userId
               ");

        if (sizeof($result) > 0) {
            return $result[0];
        }

        return null;
    }
}
