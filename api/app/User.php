<?php

namespace App;

use App\Http\Controllers\MailServiceController;
use App\Http\Controllers\NotificationServiceController;
use App\Http\Controllers\PDFTemplate;
use App\Libraries\ErrorCodes;
use Exception;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use App\Event;
use App\EventPermission;
use Illuminate\Support\Facades\Log;
use App\Libraries\Helper;

class User extends Backend
{

    const STATUS_CODE_INACTIVE = 0;
    const STATUS_CODE_ACTIVE = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $table = 'user';

//    public static function find($id)
//    {
//        return DB::select('SELECT * FROM `user` WHERE id = ? LIMIT 0,1',
//            [$id])[0];
//    }

    public function getContact() {
        if (!$this->contact_id) {
            return null;
        }
        return Contact::find($this->contact_id);
    }

    public function getLoginInfo($email)
    {
        $user = DB::select('
            SELECT c.*, c.id contact_id, u.active, u.suspend, u.token, u.login_counter, u.password, u.quick_code, u.id as id, u.company_group_id, u.location_id, u.need_accept_term_and_condition
            FROM user u LEFT JOIN contact c ON c.id = u.contact_id
            WHERE u.email = :email', ['email' => $email]);
        if ($user) {
            return $user[0];
        }
        return NULL;
    }

    public function getContactFields($email, $fields = [])
    {
        if ($fields && is_array($fields)) {
            $selectedField = implode(',', $fields);
        } else {
            $selectedField = '*';
        }
        $user = DB::select("
            SELECT $selectedField
            FROM user u INNER JOIN contact c ON c.id = u.contact_id
            WHERE u.email = :email",
            [
                'email' => $email
            ]);
        if ($user) {
            return $user[0];
        }
        return NULL;
    }

    public function getContactFieldsByToken($token, $fields = [])
    {
        if ($fields && is_array($fields)) {
            $selectedField = implode(',', $fields);
        } else {
            $selectedField = '*';
        }
        $user = DB::select("
            SELECT $selectedField
            FROM user u INNER JOIN contact c ON c.id = u.contact_id
            WHERE u.token = :token",
            [
                'token' => $token
            ]);
        if ($user) {
            return $user[0];
        }
        return NULL;
    }

    public function getChannel($userId)
    {
        $channels = DB::select('
            SELECT c.name
            FROM `user` u
                INNER JOIN channel_user ch ON ch.user_id = u.id AND u.active = 1 AND u.suspend = 0
                INNER JOIN channel c ON c.id = ch.channel_id
            WHERE u.id = :userId',
            ['userId' => $userId]);
        return $channels;
    }

    public function getChannelByContactId($contactId)
    {
        $channels = DB::select('
            SELECT c.name, c.type
            FROM contact co
                INNER JOIN `user` u ON u.contact_id = co.id AND u.active = 1 AND u.suspend = 0
                INNER JOIN channel_user ch ON ch.user_id = u.id
                INNER JOIN channel c ON c.id = ch.channel_id
            WHERE co.id = :contact_id',
            ['contact_id' => $contactId]);

        $events = User::listEventCanConfigQuery(User::where('contact_id', $contactId)->first()->id);
        return $channels;
    }

    public function getListAllUser($input, $company_group_id){


        $limit = $input->get('limit');
        $offset = $input->get('offset');

        $order = "";
        if ($input->has("orderBy")) {
            $order = "ORDER BY " . $input->get("orderBy");
            if ($input->has("orderDesc") && $input->get("orderDesc")) {
                $order .= ' DESC ';
            }
        }

        $roleId = $input->get('roleId');
        $roleQuery ="";

        if($roleId){
            $roleIdList = implode(',', $roleId);
            $roleQuery = " AND ur.role_id IN (".$roleIdList.") ";
        }

        $query = "";
        if ($input->has("query")) {
            $query_input = $input->get('query');
            if ($query_input) {

                $query .= " AND  (c.first_name LIKE '%$query_input%' OR c.last_name LIKE '%$query_input%' )";
            }
        }

//        if($roleQuery && $query)
//            $roleQuery.=" AND ";

        $status = $input->get('status');
        $statusQuery = "";
        if($input->has("status")){
//            if($roleQuery){
//                $statusQuery = " AND ";
//            }
            if($status == 1){
                $statusQuery.=" AND suspend = 0 ";
            }else{
                $statusQuery.=" AND suspend = 1 ";
            }

        }
        if($company_group_id){
            $query .= " AND u.company_group_id = $company_group_id ";
        }

        $loginKey  = Activity::TYPE_LOGIN;
        $logoutKey = Activity::TYPE_LOGOUT;

        $user = DB::select("
            SELECT u.user_code, l.name, c.first_name,c.last_name,u.email,c.mobile_phone,u.id, GROUP_CONCAT(r.name SEPARATOR ', ') as role,if(u.active=TRUE AND u.suspend=TRUE,FALSE,TRUE) as active,
            cg.name 'company_group_name', c2.company_name,
            login.create_date as last_login, logout.create_date as last_logout
            
            FROM `user` u LEFT JOIN contact c ON c.id = u.contact_id
            LEFT JOIN user_role ur ON u.id = ur.user_id
            INNER JOIN role r ON ur.role_id = r.id
            INNER JOIN location l ON l.id = u.location_id
            INNER JOIN company com ON  com.id = l.company_id
            INNER JOIN contact c2 ON c2.id = com.contact_id
            INNER JOIN company_group cg ON cg.id = u.company_group_id
            LEFT JOIN (select max(activity.create_date) as create_date, source_id 
                       from activity
                       where type = '$loginKey'
                       group by source_id) login
                 ON u.id = login.source_id
            LEFT JOIN (select max(activity.create_date) as create_date, source_id 
                       from activity
                       where type = '$logoutKey'
                       group by source_id) logout
                 ON u.id = logout.source_id 
            
            WHERE active = 1 
            $roleQuery
            $query $statusQuery
            GROUP BY u.id
            LIMIT $offset, $limit $order ");


        $userListSize = DB::select("
            select count(*) as count from (SELECT u.user_code
            FROM `user` u LEFT JOIN contact c ON c.id = u.contact_id
            LEFT JOIN user_role ur ON u.id = ur.user_id
            INNER JOIN role r ON ur.role_id = r.id
            INNER JOIN location l ON l.id = u.location_id
            INNER JOIN company com ON  com.id = l.company_id
            INNER JOIN contact c2 ON c2.id = com.contact_id
            INNER JOIN company_group cg ON cg.id = u.company_group_id 
            WHERE active = 1 
            $roleQuery $query $statusQuery GROUP BY u.id) as y                       
            ");
        if ($user) {
            $result = array($userListSize[0], "userList" => $user);
            return $result;
        }
        return NULL;
    }
    public function getUserInfo($id){

        $user = DB::select("SELECT u.user_code, c.first_name,c.last_name,c.title,c.gender,c.company_name, c. address1,c.address2, u.company_group_id, u.location_id, l.company_id,
c.email,c.suburb,c.postcode, c.country,c.state, c.mobile_phone,c.privacy_flag,c.note,u.id, GROUP_CONCAT(r.name SEPARATOR ',') as role, GROUP_CONCAT(r.id SEPARATOR ',') as roleId
            FROM `user` u 
            LEFT JOIN contact c ON c.id = u.contact_id
            LEFT JOIN user_role ur ON u.id = ur.user_id
            INNER JOIN role r ON ur.role_id = r.id 
            LEFT JOIN location l ON u.location_id = l.id
            WHERE u.id = $id GROUP BY u.id");
        if ($user) {

            return $user;
        }
        return NULL;

    }

    public function getCompanyGroup()
    {
        return DB::select('
            SELECT cg.* FROM company_group cg RIGHT JOIN company c ON cg.id=c.company_group_id
            RIGHT JOIN location l ON c.id=l.company_id WHERE l.id=:location_id', ['location_id' => $this->location_id])->first();
    }

    public function validateContactAvailable($contactId)
    {
        $user = DB::select("
            SELECT count(*) as count
            FROM user u 
            WHERE contact_id = $contactId");
        if ($user) {
            return $user[0];
        }
        return NULL;
    }
    public function validateUserCodeAvailable($code, $userId)
    {
        $user = DB::select("
            SELECT count(*) as count
            FROM user u 
            WHERE user_code = '$code' AND id <>$userId ");
        if ($user) {
            return $user[0];
        }
        return NULL;
    }

    public static function getCompany($token)
    {
        $sql = "
            SELECT u.id, l.company_id, u.location_id, com.company_group_id
            FROM user u
                INNER JOIN location l ON l.id = u.location_id
                INNER JOIN company com ON com.id = l.company_id
            where u.token = :token";
        $params = ['token' => $token];
        $data = DB::select($sql, $params);
        if ($data) {
            return $data[0];
        }
        return NULL;
    }

    public function getAccessableLocationIds() {

        $conditions = DB::select("SELECT * FROM data_access WHERE user_id=:user_id", ['user_id' => $this->id]);

        $where = '';
        foreach($conditions as $con) {
            $x = '';

            // Build X expression: (  ((location_code is from FROM to TO) or location_code in INCLUDED) and location_code not in EXCLUDED

            $include = implode(',', array_filter(explode(",", $con->include)));
            $exclude = implode(',', array_filter(explode(",", $con->exclude)));

            ////////////////////////////////////////////////////////////
            $test = '';

            if($con->range_from && $con->range_to){
                $test = "(location_code >= $con->range_from AND location_code <= $con->range_to)";
            }

            if($include){
                $test .= ($test == '') ? '' : ' OR ';
                $test .= " location_code IN ($include)";
            }
            if($test){
                $where = "(".$test.")";
            }
            if($exclude){
                $where .= ($where == '') ? '' : ' AND ';
                $where .= "  (location_code NOT IN ($exclude))";
            }
            ////////////////////////////////////////////////////////////
//            if ($exclude) {
//                $x .= " (location_code NOT IN ($exclude)) ";
//            }
//
//            $y = '';
//            if ($con->range_from && $con->range_to) {
//                $y = "(location_code >= $con->range_from AND location_code <= $con->range_to)";
//            }
//            if ($include) {
//                if ($y) {
//                    $y .= ' OR ';
//                }
//                $y .= " (location_code IN ($include)) ";
//            }
//
//            if ($y) {
//                if ($x) {
//                    $x .= ' AND ';
//                }
//                $x .= " ($y)";
//            }
//
//            if ($x) {
//                if ($where) {
//                    $where .= ' OR ';
//                }
//                $where .= " ($x)";
//            }
        }

        \LOG::debug("GET ACCESS LOCATIONS:  SELECT l.id  FROM location l LEFT JOIN company c on l.company_id=c.id WHERE c.company_group_id=:company_group_id AND ($where)");

        if (!$where) {
            return [$this->location_id];
        }
           /* echo $sqlTest = "SELECT l.id  FROM location l LEFT JOIN company c on l.company_id=c.id WHERE c.company_group_id={$this->company_group_id} AND ($where)";
        exit;*/
        $r = DB::select("SELECT l.id  FROM location l LEFT JOIN company c on l.company_id=c.id WHERE c.company_group_id=:company_group_id AND ($where)", ['company_group_id' => $this->company_group_id]);
        if (sizeof($r) == 0) {
            return [$this->location_id];
        }

        $ids =  array_map(function ($i) {
            return $i->id;
        }, $r);
        
        if ($this->location_id && !in_array($this->location_id, $ids)) {
            array_push($ids, $this->location_id);
        }
        
        return $ids;
    }

    public static function usersSameCompanyQuery($companyGroupId) {
        return User::where('company_group_id', $companyGroupId);
    }

    public function headValuerApproval() {
        $setting = CompanySetting::join('location', function($join) {
            $join->on('location.company_id', 'company_setting.company_id')
                ->where('location.id', $this->location_id);
        })->where('key', 'head_valuer_approval')
        ->first();
        return $setting ? $setting->value : true;
    }

    public function usersHaveWishListMatchWithValuation($valuation) {
        return User::where('location_id', $valuation);
    }

    public function updateCompanyChannel($newCompanyId) {
        $companyId      = Location::find($this->location_id)->company_id;
        $channelId      = Company::getChannelName($companyId);
        $newChannelId   = Company::getChannelName($newCompanyId);

        ChannelUser::where('user_id', $this->id)
            ->where('channel_id', $channelId)
            ->update(['channel_id' => $newChannelId]);
    }

    public function isMo() {
        return $this->company_group_id == 1;
    }

    public function validateIsMo() {
        if (!$this->isMo()) {
            throw new Exception(ErrorCodes::PERMISSION_NOT_ALLOW);
        }
    }

    static function getPermission($userId) {
        $permissions = DB::select("
            SELECT * from permission WHERE id IN (
                SELECT id FROM permission p RIGHT JOIN user_permission up on p.id=up.permission_id WHERE up.user_id= ?
            ) OR id IN (
                SELECT rp.permission_id 'id' FROM role_permission rp RIGHT JOIN user_role ur on rp.role_id=ur.role_id WHERE ur.user_id= ?
            )
        ", [$userId, $userId]);
        return $permissions;
    }

    static function getPermissionIds($userId) {
        $result = [];
        $permissions = User::getPermission($userId);
        foreach ($permissions as $permission) {
            array_push($result, $permission->id);
        }
        return $result;
    }

    public static function listEventCanConfigQuery($userId) {
        $permissionIds = User::getPermissionIds($userId);
        $permissionIds = $permissionIds ? $permissionIds : [];

        //todo: diff event class

        return Event::select('event.*','config_email_notification.config')
            ->leftJoin('config_email_notification', 'config_email_notification.event', 'event.code')
            ->where(function($query) use ($userId) {
                $query->where('config_email_notification.user_id', $userId)
                    ->orWhere('config_email_notification.user_id', null);
            })
            ->whereIn('event.code', function($query) use ($permissionIds) {
                $query->select('event_permission.event')
                ->from(with(new EventPermission)->getTable())
                ->whereIn('event_permission.permission_id', $permissionIds);
            });
    }

    public static function listEventCanConfigGroupByCategory($userId) {
        $events     = User::listEventCanConfigQuery($userId)->get();
        $result     = [];
        $categories = [];
        foreach($events as $event) {
            $categories[$event->category][] = $event;
        }
        foreach ($categories as $category => $value) {
            $object                 = [];
            $object['category']     = $category;
            $object['events']       = $value;
            $result[] = $object;
        }
        return $result;
    }

    public static function getUserMenuAccessById($userId)
    {
        $listMenu = DB::select("SELECT
                                        menu.`code`, menu.`name`,
                                        type_code.`value` as group_name,`menu`.site_menu
                                        FROM
                                        user_role
                                        INNER JOIN role_has_menu ON user_role.role_id = role_has_menu.role_id
                                        INNER JOIN menu ON role_has_menu.menu_id = menu.id
                                        LEFT JOIN type_code ON menu.category = type_code.`code`
                                        where type_code.type = 'MENU_GROUP' and user_id = $userId
                                        GROUP BY menu.`code`");

        $menuList = array();
        $newArray = array();
        //loop to group all menu item to correct group
        foreach ($listMenu as $row) {
            $newArray[$row->site_menu][$row->group_name][] = $row;
        }

        //loop to set correct format and return to FE
        foreach ($newArray as $key => $arr) {

            $siteArray = array('site_menu' => $key);
            $groupArray = array();
            $groupItem = array();
            foreach ($arr as $key1 => $m) {
                $groupItem['group_name'] = $key1;
                $groupItem['menu_items'] = $m;
                array_push($groupArray, $groupItem);

            }
            $siteArray['group'] = $groupArray;
            array_push($menuList, $siteArray);
        }
        return $menuList;
    }

    static function enableForEvent($userId, $event, $type) {
        $config = ConfigEmailNotification::where([['user_id', '=', $userId], ['event', '=', $event]])->first();
        if (!$config) {
            return NULL;
        }
        $config = $config->config;
        switch ($type) {
            case "email":
                return in_array($config, [ConfigEmailNotification::CONFIG_EMAIL_ONLY, ConfigEmailNotification::CONFIG_BOTH], true);
            case "notification":
                return in_array($config, [ConfigEmailNotification::CONFIG_NOTI_ONLY, ConfigEmailNotification::CONFIG_BOTH], true);
        }
        return NULL;
    }

    /**
     * @param $userId
     * @param $event: namespace app\Libraries\Event;
     * @param $sourceId
     * @param null $data: custom data need pass to pdf/mail/notification
     */
    static function notice($userId, $event, $sourceId, $data = NULL) {
        \LOG::debug("send notification/email to user $userId event $event");
        $user = User::find($userId);

        //todo: uncomment and complete dat code :(
        //generate document and up to azure
//        $pdf            = PDFTemplate::pdf($user, $event, $sourceId, $data);
//        $attach         = Document::uploadToAzure(Document::DOCUMENT_TYPE_PAYMENT_RECEIPT, $pdf, $sourceId);
//        $data['attach'] = $attach;

        try {
           /* temperory commented by ss 0033 28th aug 2019
            *  NotificationServiceController::send($user, $event, $sourceId, $data);
            MailServiceController::send($user, $event, $sourceId, $data);*/
        } catch(Exception $e) {
            \LOG::debug('--> 999'.$e);
        }
    }

    /**
     * @param $userId
     * @param $event: namespace app\Libraries\Event;
     * @param $sourceId
     * @param null $data: custom data need pass to pdf/mail/notification
     */
    static function noticeLocation($locationId, $event, $sourceId, $data = NULL) {
        \LOG::debug("send notification/email to location $locationId event $event");
        $ids = User::where('location_id', $locationId)->pluck('id')->toArray();
        foreach ($ids as $id) {
            User::notice($id, $event, $sourceId, $data);
        }
    }

    static function noticePermission($locationId, $event, $sourceId, $permissions = [], $data = NULL) {
        $permissionStr = implode(", ", $permissions);
        \LOG::debug("send notification/email to user have permission $permissionStr of location $locationId event $event");

        try {
            $users = User::where('location_id', $locationId)->get();
            foreach ($users as $user) {
                if ($permissions != NULL && count($permissions) != 0 && !Helper::valuatePermission($user->id, $permissions)) {
                    continue;
                }
              /* commenting temperorily by ss 0033 26082019 */
               /* NotificationServiceController::send($user, $event, $sourceId, $data);
                MailServiceController::send($user, $event, $sourceId, $data);*/
            }
        } catch(Exception $e) {
            \LOG::debug('--> 999'.$e);
        }
    }
}