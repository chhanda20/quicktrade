<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Interaction extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['input_date', 'type', 'note'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'interaction';
}
