<?php

namespace App;


use App\Http\Controllers\MailServiceController;
use App\Http\Controllers\PDFTemplate;
use App\Libraries\Event as EventCode;
use App\Libraries\Helper;
use App\Libraries\IdTransformer;
use App\BidLog;
use Carbon\Carbon;
use App\NotificationStatus;
use App\Libraries\Event;
use App\User;
use Illuminate\Support\Facades\DB;

class AuctionSession extends Backend
{
    protected $table = 'auction_session';

    public $startAt;
    public $endAt;

    public $durationRq;

    //in stack, not start yet
    const STATUS_NORMAL = 1;
    const STATUS_TRANSACTION_MADE = 2;

    public static function getAll($input, $history = false, $filterEditable = false)
    {
        $limit = $input->get('limit');
        $offset = $input->get('offset');
        $sessionDate = $input->get('sessionDate');

        $order = "";
        if ($input->has("orderBy")) {
            $order = "ORDER BY " . $input->get("orderBy");
            if ($input->has("orderDesc") && $input->get("orderDesc")) {
                $order .= ' DESC ';
            }
        }else{
            $order = $history ? "ORDER BY date DESC" : "ORDER BY date ASC";
        }
        $limitQuery = "";
        if($limit){
            $limitQuery = "LIMIT $offset, $limit";
        }

        $query = $sessionDate ? " AND DATE(startDate) = '$sessionDate' " : " " ;

        $filterEditableQuery = " ";
        if ($filterEditable) {
            $limitTime = AppProperty::find(AppProperty::LIMIT_TIME_EDIT_AUCTION)->value;
            $validStartTime = Carbon::now()->addMinute($limitTime);

            $filterEditableQuery = $history ? " " : " AND auction_session.startDate > '$validStartTime' ";
        }

        $currentDate = date("Y/m/d H:i:s");
        $auctionItemApproveStatus = Auction::STATUS_CODE_APPROVE;

        $filterTime = $history ?  " '$currentDate' > endDate "  : " '$currentDate' <= startDate ";  

        $sqlQuert = "
            SELECT SQL_CALC_FOUND_ROWS auction_session.id, DATE(startDate) date, DATE_FORMAT(startDate,'%H:%i') time , duration, custom_bid_price_increment
                ,sum(if((se.`status` = $auctionItemApproveStatus),1,0)) as vehicle, name
            
            FROM auction_session
            left join session_has_item se on se.auction_session_id = auction_session.id
            left JOIN auction_item item on item.id = se.auction_item_id
            WHERE $filterTime $filterEditableQuery
            $query
            GROUP BY auction_session.id
            $order
            $limitQuery";

        $auctionSession = DB::select($sqlQuert);

        $sessionListSize = DB::select("
            SELECT FOUND_ROWS() as count                  
            ");
        if ($auctionSession) {
            $result = array($sessionListSize[0], "sessionList" => $auctionSession);
            return $result;
        }
        return NULL;
    }

    public static function validateDuplicateSessionTime($startDate, $endDate, $sessionId = NULL)
    {
        $query = "";
        if ($sessionId) {
            $query = " id <> $sessionId AND ";
        }

        return DB::select("SELECT * from auction_session where $query
                              ((startDate <= '$startDate' AND endDate > '$startDate')
                               OR (startDate < '$endDate' AND endDate >= '$endDate')
                               OR (startDate>= '$startDate' AND endDate <='$endDate'))");

    }

    public static function getById($sessionId)
    {
        $auctionItemApproveStatus = Auction::STATUS_CODE_APPROVE;
        $auctionSession = DB::select("
            SELECT auction_session.id, auction_session.name, DATE(startDate) date, DATE_FORMAT(startDate,'%H:%i') time , duration, custom_bid_price_increment,
            sum(if((se.`status` = $auctionItemApproveStatus),1,0)) as vehicle
            FROM auction_session
            left join session_has_item se on se.auction_session_id = auction_session.id
            left JOIN auction_item item on item.id = se.auction_item_id
            where auction_session.id = $sessionId            
            GROUP BY auction_session.id
            ");

        if (!$auctionSession) {
            return NULL;
        }
        return $auctionSession;
    }

    public static function validateSessionTime($sessionId){
        $currentDate = date('Y/m/d H:i:s');
        //todo : add 2 hours to current date and check with start date. Currently is not apply because of testing purpose

        return DB::select("SELECT id from auction_session WHERE id = $sessionId AND startDate > '$currentDate' ");
    }

    public static function updateSessionHasItemReject($requestId)
    {
        $pendingStatus = Auction::STATUS_CODE_PENDING;
        $rejectStatus = Auction::STATUS_CODE_REJECT;
        return DB::statement("Update session_has_item Set status = $rejectStatus WHERE auction_item_id = $requestId and status = $pendingStatus");
    }


    public static function getActiveSessions() {
        return DB::select("SELECT * FROM auction_session WHERE :backend_time BETWEEN startDate and endDate", ['backend_time' => date('Y-m-d H:i:s')]);
    }

    public static function getNextSessions() {
        $time = date('Y-m-d H:i:s');
        return DB::select("SELECT *,(:backend_time BETWEEN startDate and endDate) is_active 
                FROM auction_session WHERE
                (:backend_time2 BETWEEN startDate and endDate) 
                OR (:backend_time3 < startDate) ORDER BY startDate
                LIMIT 1
         ", [
            'backend_time' => $time,
            'backend_time2' => $time,
            'backend_time3' => $time
        ]);
    }

    public static function checkAuctionSessionHaveVehicle($auctionSessionId){
        $result = DB::select("select * from session_has_item where auction_session_id = $auctionSessionId");
        if($result){
            return true;
        }
        return false;
    }


    public static function autoCreateTransactions()
    {
        $outcomes = [];
        $time = date('Y-m-d H:i:s');
        $sessions = DB::select("
            SELECT * FROM auction_session WHERE endDate<:backend_date  AND status != :status_transaction_made
        ", [
            'backend_date' => $time,
            'status_transaction_made' => AuctionSession::STATUS_TRANSACTION_MADE
        ]);

        foreach ($sessions as $session) {
            $updatedRow = AuctionSession::where('id', $session->id)
                                        ->where('status', '!=', AuctionSession::STATUS_TRANSACTION_MADE)
                                        ->update(['status' => AuctionSession::STATUS_TRANSACTION_MADE]);
            if ($updatedRow > 0) {
                $items = DB::select("
                            SELECT shi.*, ai.valuation_id, v.version, v.user_id 
                              FROM session_has_item shi
                              LEFT JOIN auction_item ai ON ai.id=shi.auction_item_id
                              LEFT JOIN valuation v ON v.id = ai.valuation_id
                            WHERE auction_session_id = :session_id
                    ", [
                        'session_id' => $session->id
                    ]);

                //seller/buyer: array transactionIds each user invote in this auction session
                $outcome = [
                    'seller' => [],
                    'buyer' => [],
                    ];

                foreach($items as $item) {

                    $valuation = Valuation::find($item->valuation_id);
                    if (!$valuation) {
                        \LOG::error("Auction Session[$item->auction_session_id], item[$item->auction_item_id] => Can't get valuation[$item->valuation_id] ");
                    }

                    $buyer = User::find($item->bid_user_id);

                    $valuation->status = $buyer ? Valuation::STATUS_SOLD_OUT : Valuation::STATUS_COMPLETE_TRADED_IN;
                    $valuation->save();

                    if (!$buyer) {
                        return;
                    }

                    $vehicle = Vehicle::find($valuation->vehicle_id);
                    if (!$vehicle) {
                        \LOG::error("Auction Session[$item->auction_session_id], item[$item->auction_item_id] => Can't get vehicle[$valuation->vehicle_id] ");
                    }

                    $t = new Transaction();
                    $t->valuation_id = $item->valuation_id;
                    $t->valuation_version = $item->version;
                    $t->buyer_id = $item->bid_user_id;
                    $t->seller_id = $item->user_id;
                    $t->buyer_location = $buyer->location_id;
                    $t->seller_location = $vehicle->trade_in_location;
                    $t->status = Transaction::STATUS_WAITING_PAYMENT;
                    $t->vehicle_price = $item->bid_price;
//                    $t->shipping_price;
//                    $t->seller_fee;
//                    $t->buyer_fee;
                    $t->source = Transaction::SOURCE_LIVE_AUCTION;
                    $t->source_id = $session->id;
                    $t->save();

                    $t->rid = IdTransformer::toClientTransactionId($t->id);
                    Transaction::setMotoFee($t);
                    $t->save();

                    //update rating
                    $locationId = Valuation::where('valuation.id', $t->valuation_id)
                        ->join('vehicle', 'vehicle.id', '=', 'valuation.vehicle_id')
                        ->first()
                        ->location_id;
                    Location::updateRating($locationId);

                    $sellerData = [];
                    $buyerData  = [];
                    $event = EventCode::TRANSACTION_NEW;
                    
                    \LOG::debug("start generate document for event: [$event]");
                    $buyerData['attach'] = Document::generatorPDFAndUploadAzure($t->seller_id, $event, $t->id, 'creator');

                    User::notice($t->seller_id, $event, $t->id, $sellerData);
                    User::notice($t->buyer_id, $event, $t->id, $buyerData);

                    //save related transaction id to buyer and seller in auction session - send email report later
                    if (!isset($outcome['seller'][$t->seller_id])) {
                        $outcome['seller'][$t->seller_id] = [];
                    }
                    if (!isset($outcome['buyer'][$t->buyer_id])) {
                        $outcome['buyer'][$t->buyer_id] = [];
                    }
                    $outcome['seller'][$t->seller_id][]   = $t->id;
                    $outcome['buyer'][$t->buyer_id][]     = $t->id;

                    \LOG::info("Auto created Transaction[$t->rid] for the auction session[$session->id], item [$item->auction_item_id]");
                }

                $outcomes[] = $outcome;
            }
        }
        return $outcomes;
    }

    public function validateInEditableTime() {
        $limitTime      = AppProperty::find(AppProperty::LIMIT_TIME_EDIT_AUCTION)->value;
        $startTime      = $this->startDate;
        $now            = Carbon::now();
        if ($startTime->diffInMinute($now) < $limitTime) {
            throw new Exception(ErrorCodes::NOT_VALID_TIME_EDIT_AUCTION_SESSION);
        }
    }

    static public function maxBids($auctionSessionId) {
        $biderIds = BidLog::selectRaw("user_id, MAX(bid_price) as max_bid, MAX(update_date) as update_date")
            ->where('auction_session_id', $auctionSessionId)
            ->groupBy('user_id')
            ->get();
        return $biderIds;
    }

    static public function sendOutbid($sessionId, $bider) {
        $topBids        = AuctionSession::maxBids($sessionId);
        $maxBid         = 0;
        $dateObject     = Carbon::now();
        $minDate        = $dateObject->toDateTimeString();
        $userMaxBidId   = NULL;
        foreach ($topBids as $bid) {
            if (($bid->max_bid == $maxBid && $bid->update_date <= $minDate) || $bid->max_bid > $maxBid) {
                $userMaxBidId   = $bid->user_id;
                $maxBid         = $bid->max_bid;
                $minDate        = $bid->update_date;
            }
        }

        $outbidUserIds  = [];
        foreach ($topBids as $bid) {
            if ($bid->user_id == $userMaxBidId) { continue; }
            $outbidUserIds[] = $bid->user_id;
        }

        foreach ($outbidUserIds as $id) {
            $event = Event::OUTBID;
            $ready = NotificationStatus::readyReceiveNotification($id, $event, $sessionId);
            \LOG::debug("id: [$id] event: [$event] session: [$sessionId] ready: [$ready]");
            if ($ready) {
                User::notice($id, $event, $sessionId, []);
                $newStatus = NotificationStatus::firstOrNew([
                    'user_id'       => $id,
                    'event_code'    => $event,
                    'source_id'     => $sessionId,
                ]);
                $newStatus->is_ready       = false;
                $newStatus->save();
            }

            $userStatus = NotificationStatus::firstOrNew([
                'user_id'       => $bider->id,
                'event_code'    => $event,
                'source_id'     => $sessionId,
            ]);
            $userStatus->is_ready = true;
            $userStatus->save();
        }

    }
}