<?php

namespace App;

use Illuminate\Support\Facades\DB;

class OfferBid extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];
    protected $table = 'offer_bid';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;
    public $timestamps = false;

    public static function bestBid($valuationId)
    {
        $sql = "
		    SELECT ob.id bid_id, ob.external_offer_id, eo.valuation_id, ob.bid_date, ob.bid_price, ob.expiry_date, ob.user_id buyer_id , eo.creator_id seller_id,
		        ve.make, ve.model_family, YEAR(ve.build_date) year_build, color.`value` color, CONCAT_WS(' ', YEAR(ve.build_date), ve.make, ve.model_family, color.`value`) offer_name
            FROM offer_bid ob
                INNER JOIN external_offer eo ON eo.id = ob.external_offer_id AND eo.valuation_id = :valuationId
                INNER JOIN valuation v ON v.id = eo.valuation_id
                INNER JOIN vehicle ve ON ve.id = v.vehicle_id
                LEFT JOIN type_code color ON color.type = 'COLOR_TYPE' AND color.`code` = ve.colour
            ORDER BY bid_price DESC, expiry_date DESC, bid_date
            LIMIT 0,1";
        $params = ['valuationId' => $valuationId];
        $data = DB::select($sql, $params);
        if ($data) {
            return $data[0];
        }
        return [];
    }

    public static function getOfferBid($valuationId) {
        $sql = "
		    SELECT eo.id, eo.arrival_date, eo.create_date, eo.`status`, eo.creator_id, ob.id bid_id, ob.user_id, ob.bid_date, ob.bid_price, ob.expiry_date
            FROM external_offer eo LEFT JOIN offer_bid ob ON ob.external_offer_id = eo.id
            WHERE eo.valuation_id = :valuationId
            ORDER BY eo.id DESC
            LIMIT 0,1";
        $params = ['valuationId' => $valuationId];
        $data = DB::select($sql, $params);
        if ($data) {
            return $data[0];
        }
        return [];
    }

    public static function getOfferBidId($id) {
        $sql = "
		    SELECT eo.id, eo.arrival_date, eo.create_date, eo.`status`, eo.creator_id, ob.id bid_id, ob.user_id, ob.bid_date, ob.bid_price, ob.expiry_date
            FROM external_offer eo LEFT JOIN offer_bid ob ON ob.external_offer_id = eo.id
            WHERE eo.id = :id";
        $params = ['id' => $id];
        $data = DB::select($sql, $params);
        if ($data) {
            return $data[0];
        }
        return [];
    }
}
