<?php

namespace App;

use Illuminate\Support\Facades\DB;

class BidLog extends Backend
{
    const TYPE_AUTO = 1;
    const TYPE_MANUAL = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];


    protected $table = 'bid_log';
}
