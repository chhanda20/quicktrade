<?php

namespace App;

use Illuminate\Support\Facades\DB;

class AppProperty extends Backend
{

    const GLASS_DATA_EXPORTED_TIME              = "glass_data_exported_time";
    const GLASS_DATA_EXPORTED_FILE              = "glass_data_exported_file";

    const EXTERNAL_OFFER_TIMEOUT                = "external_offer_timeout";
    const EXTERNAL_OFFER_EXPIRY                 = "external_offer_expiry";
    const EXTERNAL_OFFER_ACCEPTANCE_TIMEOUT     = "external_offer_acceptance_timeout";

    const FIRST_NOTIFICATION_TIMEOUT            = "first_notification_timeout";
    const SECOND_NOTIFICATION_TIMEOUT           = "second_notification_timeout";
    const VALUATION_TIMEOUT                     = "valuation_timeout";

    const HEAD_VALUER_APPROVAL                  = "head_valuer_approval";
    const RECEIVE_EXTERNAL_OFFER                = "receive_external_offer";

    const AUCTION_DEFAULT_DURATION              = "auction_default_duration";
    const AUCTION_PRICE_INCREMENT               = "auction_price_increment";

    const AUTO_BID_STARTED_TIME                 = "auto_bid_started_time";
    const BUYER_MOTO_FEE                       = "buyer_moto_fee";
    const SELLER_MOTO_FEE                      = "seller_moto_fee";

    const LIMIT_TIME_EDIT_AUCTION               = "limit_time_edit_auction";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];
    public $incrementing = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'app_property';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public function getAppProperty($ids) {
        $where = [];
        foreach ($ids as $id) {
            $where[] = "id='$id'";
        }
        $whereSql = implode(' OR ', $where);
        $sql = "
            SELECT id, value
            FROM app_property
            WHERE $whereSql";
        $data = DB::select($sql);
        return $data;
    }

    public static function getConfig($id) {
        $x = AppProperty::where('id', $id)->first();
        if ($x) {
            return $x->value;
        }
        return null;
    }

    public static function setConfig($id, $value) {
        $x = AppProperty::where('id', $id)->first();
        if (!$x) {
            $x = new AppProperty();
            $x->id = $id;
        }
        $x->value = $value;
        $x->save();
    }
}
