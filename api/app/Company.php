<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Company extends Backend
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 2;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['status_code', 'company_group_id', 'contact_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'company';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public static function getChannelName($companyId) {
        return 'company'.$companyId;
    }

    public function channelName() {
        return Company::getChannelName($this->id);
    }
}
