<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Activity extends Backend
{
    const TYPE_LOGIN        = 'login';
    const TYPE_LOGOUT       = 'logout';
    const TYPE_LOGOUT_ADMIN = 'logout_admin';

    protected $fillable = [
        'type', 'source_id'
    ];

    protected $table = 'activity';

    const CREATED_AT = "create_date";
    const UPDATED_AT = "update_date";

    public $timestamps = true;
}
