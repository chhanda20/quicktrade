<?php

namespace App;

use App\ExternalOffer;
use App\Auction;
use Illuminate\Support\Facades\DB;

class WishList extends Backend
{
    protected $table = 'wish_list';

    public static function getById($id, $userLocation)
    {
        $wishList = DB::select("
            SELECT
                    w.id, w.makeInfo, w.transmission,
                    CONCAT(w.price_start,' - ',w.price_end) AS price_range,
                    CONCAT(w.year_start,' - ',w.year_end) AS year_range,
                    CONCAT(w.odometer_start,' - ',w.odometer_end) AS odometer_range,locationId,
					(SELECT GROUP_CONCAT(location.name) from location where FIND_IN_SET( location.id, locationId))AS location
                    
            FROM
                    wish_list AS w
                    WHERE
                    w.dealer_location = $userLocation AND w.id = $id
            ");

        if (!$wishList) {
            return NULL;
        }
        return $wishList;
    }

    public static function search($input, $userLocation)
    {
        $limit = $input->get('limit');
        $offset = $input->get('offset');


        $order = "";
        if ($input->has("orderBy")) {
            $order = "ORDER BY " . $input->get("orderBy");
            if ($input->has("orderDesc") && $input->get("orderDesc")) {
                $order .= ' DESC ';
            }
        }
        $limitQuery = "";
        if ($limit) {
            $limitQuery = "LIMIT $offset, $limit";
        }

        $query = $input->get("query");
        $queryRequest = "";
        if($query){
            $queryRequest .= " Where t.makeInfo LIKE '%$query%' OR t.transmission LIKE '%$query%' OR t.location LIKE '%$query%' ";
        }
        $wishList = DB::select("
            SELECT SQL_CALC_FOUND_ROWS * FROM ( Select w.id, w.makeInfo, w.transmission,
                    CONCAT(w.price_start,' - ',w.price_end) AS price_range,
                    CONCAT(w.year_start,' - ',w.year_end) AS year_range,
                    CONCAT(w.odometer_start,' - ',w.odometer_end) AS odometer_range,locationId,
					(SELECT GROUP_CONCAT(location.name)  from location where FIND_IN_SET( location.id, locationId)) AS location
                    
                    FROM
                    wish_list AS w
                    WHERE
                    w.dealer_location = $userLocation) as t
                    
            $queryRequest            
            $order
            $limitQuery
               ");


        $wishListSize = DB::select("select FOUND_ROWS() as count");

            $result = array($wishListSize[0], "wishList" => $wishList);
            return $result;

    }

    public static function getByUserId($userId) {
        $wishList = DB::select("SELECT * FROM wish_list WHERE user_id = $userId");
        return $wishList;
    }

    public static function getByUserIds($userIds) {
        $wishList = WishList::whereIn('user_id', $userIds);
        return $wishList;
    }

    public static function getByDealerLocationId($userLocation) {
        return WishList::where('dealer_location', $userLocation)->get();
    }

    public static function vehicleMatchWishlistQuery() {
        $query = WishList::join('vehicle', 'vehicle.id', 'vehicle.id');

        //odometer
        $query->where(function($query) {
            $query->whereNull('wish_list.odometer_start')->orWhereNull('wish_list.odometer_end')
                ->orWhere(function($query) {
                    $query->whereRaw('vehicle.odometer >= wish_list.odometer_start')
                        ->whereRaw('vehicle.odometer <= wish_list.odometer_end');
            });
        });

        //build date
        $query->where(function($query) {
            $query->whereNull('wish_list.year_start')
                ->orWhereNull('wish_list.year_end')
                ->orWhere(function($query) {
                    $query->whereRaw('year(vehicle.build_date) >= wish_list.year_start')
                        ->whereRaw('year(vehicle.build_date) <= wish_list.year_end');
            });    
        });
        
        //transmission
        $query->where(function($query) {
            $query->whereNull('wish_list.transmission')
                ->orWhereRaw("wish_list.transmission REGEXP concat('\"', vehicle.transmission, '\"') COLLATE utf8_unicode_ci");
        });

        //makeinfo
        $query->where(function($query) {
            $query->whereNull('wish_list.makeInfo')
                ->orWhere(function($query) {
                    $query->whereRaw("wish_list.makeInfo REGEXP 
                                    concat('{\"make\":\"', vehicle.make, '\"}')
                                    COLLATE utf8_unicode_ci")
                        ->orWhereRaw("wish_list.makeInfo REGEXP
                                    concat('{\"make\":\"', vehicle.make, '\",
                                    \"model\":\"', vehicle.model_family,'\"}') 
                                    COLLATE utf8_unicode_ci")
                        ->orWhereRaw("wish_list.makeInfo REGEXP 
                                    concat('{\"make\":\"', vehicle.make, '\",
                                    \"model\":\"', vehicle.model_family, '\",
                                    \"variant\":\"', vehicle.variant  ,'\"}') 
                                    COLLATE utf8_unicode_ci");
            });
        });
        
        return $query;
    }

    public static function locationsMatchingExternalOffer($externalOfferId) {
        $vehicleId = ExternalOffer::where('external_offer.id', $externalOfferId)
                        ->join('valuation', 'external_offer.valuation_id', 'valuation.id')
                        ->select('valuation.vehicle_id')->first()->vehicle_id;

        $query = WishList::vehicleMatchWishlistQuery()
            ->where('vehicle.id', $vehicleId)
            ->join('user', 'user.id', 'wish_list.user_id');

        return array_unique($query->pluck('user.location_id')->toArray());
    }

    public static function locationsMatchingAuction($auctionId) {
        $auctionValuationSubQuery = "(select auction_item.start_price, valuation.vehicle_id from auction_item  
                                    join valuation on auction_item.valuation_id = valuation.id
                                    where auction_item.id = '$auctionId')";

        $query = WishList::vehicleMatchWishlistQuery()
            ->join('user', 'user.id', 'wish_list.user_id')
            ->join(DB::raw("$auctionValuationSubQuery as subQuery"), 'subQuery.vehicle_id' ,'vehicle.id')
            ->where('subQuery.start_price', '>=', 'wish_list.price_start')
            ->whereRaw('subQuery.start_price <= wish_list.price_end');

        return array_unique($query->pluck('user.location_id')->toArray());
    }
}