<?php

namespace App;

use Illuminate\Support\Facades\DB;

class ModelOption extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'glass_model_option';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public function getOptionByCode($code) {
        $data = DB::select("
            SELECT GROUP_CONCAT(`option` SEPARATOR ',') option_group
            FROM glass_model_option
            WHERE glass_code = :code AND rec_type = 'Standard:'
            GROUP BY glass_code",
            ['code' => $code]);
        if ($data) {
            return $data[0];
        }
        return NULL;
    }
}
