<?php

namespace App;

use Illuminate\Support\Facades\DB;

class VehicleBlocked extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'vehicle_blocked';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;
}
