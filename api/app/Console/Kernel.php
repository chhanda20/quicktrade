<?php

namespace App\Console;

use App\AppProperty;
use App\AuctionSession;
use App\ExternalOffer;
use App\Libraries\Event as EventCode;
use App\Libraries\Helper;
use App\Libraries\Permissions;
use App\TestDrive;
use App\OfferBid;
use App\User;
use App\Valuation;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use Carbon\Carbon;

class Kernel extends ConsoleKernel
{

    
    protected $commands = [
        Commands\DoAutoBid::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {

            $notifyFirstTime = intval(AppProperty::where('id', AppProperty::FIRST_NOTIFICATION_TIMEOUT)->first()->value); // minutes
            $secondNotificationTimeout = intval(AppProperty::where('id', AppProperty::SECOND_NOTIFICATION_TIMEOUT)->first()->value); // minutes

            $notifySecondTime = $notifyFirstTime + $secondNotificationTimeout; // 5 mins after the first 3 mins
            $valuationModel = new Valuation();
            $valuations = $valuationModel->getRequestedValuation();
           // dd($valuations);
            foreach ($valuations as $valuation) {
                $minDiff = Helper::timeDiffMin(date('Y-m-d H:i:s'), $valuation->update_date);
                $title = 'New Valuation Request';
                $message = '';
                $webMessage = '';
                $msgParts = [];
                $webMsgParts = [];

                if ($valuation->year) {
                    $msgParts[] = $valuation->year;
                    $webMsgParts[] = $valuation->year;
                }
                if ($valuation->make) {
                    $msgParts[] = $valuation->make;
                    $webMsgParts[] = $valuation->make;
                }
                if ($valuation->model_family) {
                    $msgParts[] = $valuation->model_family;
                    $webMsgParts[] = $valuation->model_family;
                }
                if ($valuation->colour) {
                    $msgParts[] = $valuation->colour;
                    $webMsgParts[] = $valuation->colour;
                }
                if ($valuation->registration_number) {
                    $msgParts[] = $valuation->registration_number;
                }
                if ($msgParts) {
                    $message = implode(' ', $msgParts);
                }
                if ($webMsgParts) {
                    $webMessage = implode(' ', $webMsgParts);
                }
                $customData = [
                    'valuationId'         => $valuation->id,
                    'time'                => date('Y-m-d H:i:s'),
                    'web_message'         => $webMessage,
                    'registration_number' => $valuation->registration_number,
                ];

                \LOG::debug("valuation->id [$valuation->id] valuation->notify_sent_counter: [$valuation->notify_sent_counter] minDiff: [$minDiff] notifyFirstTime: [$notifyFirstTime]");
                // Send notification after 3 mins
                if ($valuation->notify_sent_counter == 0 && $minDiff >= $notifyFirstTime) {
                    $permissions = [Permissions::VALUATION_PERFORM];
                    $locationId = User::find($valuation->user_id)->location_id;
                    User::noticePermission($locationId, EventCode::VALUATION_REQUEST_NOTIFY_1, $valuation->id, $permissions, $customData);

                    DB::table('valuation')->where('id', $valuation->id)->update(['notify_sent_counter' => 1]);
                }

                \LOG::debug("valuation->id [$valuation->id] valuation->notify_sent_counter: [$valuation->notify_sent_counter] minDiff: [$minDiff] notifySecondTime: [$notifySecondTime]");
                // Send notification after 5 mins after the first 3 mins
                if ($valuation->notify_sent_counter == 1 && $minDiff >= $notifySecondTime) {
                    $permissions = [
                        Permissions::NOTIFICATION_HEAD_VALUER, 
                        Permissions::NOTIFICATION_SALE_MANAGER
                    ];
                    User::noticePermission($valuation->location_id, EventCode::VALUATION_REQUEST_NOTIFY_2, $valuation->id, $permissions, $customData);

                    DB::table('valuation')->where('id', $valuation->id)->update(['notify_sent_counter' => 2]);
                }
            }

            $externalOfferTimeout = AppProperty::where('id', AppProperty::EXTERNAL_OFFER_TIMEOUT)->first()->value;
            // Pending offer with no offers received
            $pendingOffers = ExternalOffer::where('status', ExternalOffer::STATUS_PENDING)->get();
            foreach ($pendingOffers as $pendingOffer) {
                $pendingMinDiff = Helper::timeDiffMin(date('Y-m-d H:i:s'), $pendingOffer->create_date);
                if ($pendingMinDiff >= $externalOfferTimeout) {
                    $offerObj = ExternalOffer::find($pendingOffer->id);
                    $offerObj->status = ExternalOffer::STATUS_PASSED;
                    $offerObj->save();

                    // $sellerData = [];
                    // $event = EventCode::EXTERNAL_OFFER_RETURNED;
                    // User::notice($offerObj->creator_id, $event, $offerObj->id, $sellerData);
                }
            }

            // External Best Offer
            $openOffers = ExternalOffer::where('status', ExternalOffer::STATUS_OFFER_MADE)->get();
            foreach ($openOffers as $openOffer) {
                \LOG::debug('openOffer called');
                $minDiff = Helper::timeDiffMin(date('Y-m-d H:i:s'), $openOffer->create_date);
                if ($minDiff >= $externalOfferTimeout) {
                    $bestOffer = OfferBid::bestBid($openOffer->valuation_id);
                    $offerStatus = null;

                    if ($bestOffer) {
                        DB::table('offer_bid')
                            ->where('external_offer_id', $bestOffer->external_offer_id)
                            ->where('id', '!=', $bestOffer->bid_id)
                            ->delete();
                        $offerStatus = ExternalOffer::STATUS_AWAITING_RESPONSE;

                        // Send Push Notification to seller
                        $title = 'External Best Offer';
                        $message = $bestOffer->offer_name . '. Best cost: ' . $bestOffer->bid_price . '. Expiry Date: ' . $bestOffer->expiry_date;
                       echo $message;
                        $web_message = $bestOffer->offer_name;
                        $customData = [
                            'bid_price' => $bestOffer->bid_price,
                            'expiry_date' => $bestOffer->expiry_date,
                            'seller_id' => $bestOffer->seller_id,
                            'buyer_id' => $bestOffer->buyer_id,
                            'valuation_id' => $bestOffer->valuation_id,
                            'external_offer_id' => $bestOffer->external_offer_id,
                            'time' => date('Y-m-d H:i:s'),
                            'web_message' => $web_message,
                        ];
                        $event = EventCode::WON_BEST_OFFER;
                        $buyerData = [];
                        User::notice($bestOffer->buyer_id, $event, $openOffer->id, $buyerData);
                    } else {
                        $offerStatus = ExternalOffer::STATUS_PASSED;
                    }
                    $offerObj = ExternalOffer::find($openOffer->id);
                    $offerObj->status = $offerStatus;
                    $offerObj->save();

                    \LOG::debug('openOffer->id['.$openOffer->id.']');

                    // $sellerData = [];
                    // $event = EventCode::EXTERNAL_OFFER_RETURNED;
                    // User::notice($openOffer->creator_id, $event, $openOffer->id, $sellerData);
                }
            }
        })->everyMinute();

        $schedule->call(function () {
            Valuation::releaseOutOfTimeValuation();
        })->everyFiveMinutes();
        //


        //send outcome email to seller and buyer
        $schedule->call(function () {
            $outcomes = AuctionSession::autoCreateTransactions();
            foreach ($outcomes as $outcome) {
                foreach (['seller', 'buyer'] as $userType) {
                    foreach ($outcome[$userType] as $userId => $transactionIds) {
                        //todo: do some thing with user type here in future :D currently don't have email template
                        $data            = [];
                        $data['body']    = $transactionIds;
                        $event           = EventCode::AUCTION_SESSION_OUTCOME;
                        //this email contain many source-id in $data, so let $sourceId = 0 for special-case
                        User::notice($userId, $event, 0, $data);
                    }
                }
            }
        })->everyMinute();

        //return vehicle reminder
        $schedule->call(function () {
            $now               = Carbon::now();
            $outdateTestDrives = TestDrive::outdateTestDrives();
            foreach ($outdateTestDrives as $testDrive) {
                if ($now->diffInMinutes(Carbon::parse($testDrive->return_date)) <= 5) {
                    $event = EventCode::RETURN_VEHICLE_REMINDER;        
                    User::notice($testDrive->creator_id, $event, $testDrive->id, []);
                } 
            }
        })->everyFiveMinutes();
    }
}
