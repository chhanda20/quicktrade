<?php

namespace App\Console\Commands;

use App\Libraries\AuctionBidHelper;
use App\User;
use Illuminate\Console\Command;
use App\AuctionSession;

class DoAutoBid extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DoAutoBid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Do auto biding';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count = AuctionBidHelper::autoBid();
        if ($count < 0) {
            $this->line("Can't start AutoBid task. Checking log for reasons.");
        } else {
            $this->line("Did auto bid for $count users.");
        }

    }
}