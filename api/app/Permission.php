<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Permission extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'permission';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public static function getListPermission($where)
    {

        return DB::select("SELECT p.id,p.alias name,p.description,
                          p.code,p.status_code,p.category,p.is_enabled,p.value,t.`value` as category_name 
                          FROM permission p
                          JOIN type_code t on t.`code` = p.category and t.type='PERMISSION_CATEGORY' $where");
    }

    public static function getPermissionGroup(){
        return DB::select("SELECT p.category,t.`value` as category_name FROM permission p
                            JOIN type_code t on t.`code` = p.category and t.type='PERMISSION_CATEGORY'
                            GROUP BY p.category");
    }
}
