<?php

namespace App;

use App\Libraries\ErrorCodes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Role extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'role';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;
    const VALUER_CODE = 1;

    public static function listAll()
    {

        return DB::select('SELECT * FROM role');

    }

    public static function getRoleByCompanyGroupId($company_group_id)
    {
        return DB::select("SELECT * FROM role WHERE company_group_id = $company_group_id");
    }

    public static function getDefaultRole()
    {
        return DB::select("SELECT * FROM role WHERE is_default = 1");
    }

    public function searchAllRoles($input, $company_group_id){
        $limit = $input->get('limit');
        $offset = $input->get('offset');

        $order = "";
        if ($input->has("orderBy")) {
            $order = "ORDER BY " . $input->get("orderBy");
            if ($input->has("orderDesc") && $input->get("orderDesc")) {
                $order .= ' DESC ';
            }
        }



        $query = "";
        if ($input->has("query")) {
            $query_input = $input->get('query');
            if ($query_input) {

                $query .= " AND (name LIKE '%$query_input%' )";
            }
        }

        $role = DB::select("
            SELECT *
            FROM role 
            WHERE company_group_id = $company_group_id
            $query       
            $order
            LIMIT $offset, $limit  ");


        $roleListSize = DB::select("
            select count(*) as count FROM role 
            WHERE company_group_id = $company_group_id
            $query                       
            ");
        if ($role) {
            $result = array($roleListSize[0], "rolesList" => $role);
            return $result;
        }
        return NULL;
    }
}
