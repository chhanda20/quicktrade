<?php

namespace App;

use Illuminate\Support\Facades\DB;

class VehicleOption extends Backend
{
    const TYPE_STANDARD = 1;
    const TYPE_OPTIONAL = 2;
    const TYPE_EXTRA = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'vehicle_option';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;
    
    public static function getDetail($vehicleId)
    {
        $sql = '
		    SELECT RTRIM(go.`name`) name, vo.price
            FROM vehicle_option vo INNER JOIN  glass_option go ON go.glass_option_id = vo.vehicle_option_code_id
            WHERE vo.vehicle_id = :vehicleId';
        $params = ['vehicleId' => $vehicleId];
        $data = DB::select($sql,$params);
        return $data;
    }
}
