<?php

namespace App;

use Illuminate\Support\Facades\DB;

class ConfigEmailNotification extends Backend
{
    const CONFIG_NONE           = "none";
    const CONFIG_EMAIL_ONLY     = "email_only";
    const CONFIG_NOTI_ONLY      = "noti_only";
    const CONFIG_BOTH           = "both";

    const CREATED_AT            = NULL;
    const UPDATED_AT            = NULL;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];


    protected $table = 'config_email_notification';
}