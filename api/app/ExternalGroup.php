<?php

namespace App;

use Illuminate\Support\Facades\DB;

class ExternalGroup extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];
    protected $table = 'external_group';
    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public static function getListDealer($id)
    {
        $listDealer = DB::select("
            SELECT
                company.id AS company_id,
                contact.`company_name` AS company_name,
                external_group.`name` as group_name,
                external_group.description as group_description
                FROM
                external_group_location
                INNER JOIN external_group ON external_group.id = external_group_location.external_group_id
                INNER JOIN location ON external_group_location.location_id = location.id
                INNER JOIN company ON location.company_id = company.id
                LEFT JOIN contact ON contact.id = company.contact_id
                WHERE external_group.id = $id
                GROUP BY company.id
                ORDER BY
                external_group_location.location_id ASC
                ,
                company_id ASC 
                
              
        ");
        return $listDealer;
    }

    public static function getListLocationByDealer($id, $companyId)
    {
        $locationList = DB::select("
            SELECT
                location.`name` as locationName,
                location.id as locationId
            FROM
              company
            INNER JOIN location ON location.company_id = company.id
            INNER JOIN external_group_location ON external_group_location.location_id = location.id
            WHERE
                company.id = $companyId AND
                location.id IN ( external_group_location.location_id) AND
                external_group_location.external_group_id = $id                
            ");
        return $locationList;
    }

    public function searchExternalGroup($input, $userId)
    {
        $limit = $input->get('limit');
        $offset = $input->get('offset');

        $order = "";
        if ($input->has("orderBy")) {
            $order = "ORDER BY " . $input->get("orderBy");
            if ($input->has("orderDesc") && $input->get("orderDesc")) {
                $order .= ' DESC ';
            }
        }



        $query = "";
        if ($input->has("query")) {
            $query_input = $input->get('query');
            if ($query_input) {

                $query .= " WHERE (name LIKE '%$query_input%' OR name LIKE '%$query_input%' )";
            }
        }

        $externalOfferGroupList = DB::select("
            SELECT * from 
                (SELECT ex.id,ex.`name`,ex.description,company_id,com.company_group_id
                            FROM external_group ex
                JOIN company com on com.id = ex.company_id                
                ) as test
                where test.company_group_id = (select company_group_id from `user` where id = $userId)
            $query          
               $order
            LIMIT $offset, $limit  ");


        $userListSize = DB::select("
            select count(*) as count FROM 
            (SELECT ex.id,ex.`name`,ex.description,company_id,com.company_group_id
                            FROM external_group ex
                JOIN company com on com.id = ex.company_id                
                ) as test
                where test.company_group_id = (select company_group_id from `user` where id = $userId)
            $query                       
            ");
        if ($externalOfferGroupList) {
            $result = array($userListSize[0], "externalOfferGroupList" => $externalOfferGroupList);
            return $result;
        }
        return NULL;
    }

    public static function findById($id)
    {
        $externalOfferGroupInfo = DB::select("
            SELECT
                external_group.`name` AS group_name,
                external_group.description,
                location.id,
                location.`name`,
                company.id AS company_id,
                company.`name` AS company_name
                FROM
                external_group_location
                INNER JOIN external_group ON external_group.id = external_group_location.external_group_id
                INNER JOIN location ON external_group_location.location_id = location.id
                INNER JOIN company ON location.company_id = company.id
                WHERE external_group.id = $id
                ORDER BY
                external_group_location.location_id ASC
                ,
                company_id ASC
            ");
        if($externalOfferGroupInfo){
            return $externalOfferGroupInfo;
        }
        return NULL;
    }


}
