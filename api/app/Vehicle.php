<?php

namespace App;

use app\Libraries\Dashboard;
use Illuminate\Support\Facades\DB;
use MicrosoftAzure\Storage\Blob\Models\ListBlobsOptions;
use Mockery\CountValidator\Exception;
use WindowsAzure\Common\ServicesBuilder;
use Carbon\Carbon;
use App\Auction;
use App\Transaction;

class Vehicle extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['vin', 'colour', 'registration_number', 'engine_number', 'odometer',
                           'registration_expiry_date', 'build_date', 'compliance_date', 'transmission',
                           'drive_train', 'chassis_number', 'trim', 'vehicle_model_id', 'has_one_owner',
                           'has_service_book', 'has_service_history', 'condition', 'trade_in_location',
                           'keys_location', 'model_family', 'make_id', 'make', 'model_code', 'registration_number',
                           'variant', 'colour', 'manufacturer_color' , 'body_type' , 'first_reg_date',
                           'registration_expiry_date', 'registered_state', 'build_date', 'compliance_date',
                           'engine_type', 'engine_size', 'engine_capacity', 'cylinder',  'fuel_type',
                           'transmission', 'drive_train', 'body_no', 'vin', 'chassis_number',
                           'trim', 'trim_color', 'glass_guide_code'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'vehicle';

    const CREATED_AT = "create_date";
    const UPDATED_AT = "update_date";

    public $timestamps = true;


    public function getVehicle($vin, $vehicleYear) {
        $data = DB::select('
            SELECT v.*
            FROM vehicle v
            WHERE v.vin = :vin AND YEAR(v.build_date) = :vehicleYear',
            [
                'vin' => $vin,
                'vehicleYear' => $vehicleYear,
            ]);
        if ($data) {
            return $data[0];
        }
        return NULL;
    }
    
    public function findWithFilter() {
    	
    }

    public function cloneImage($newId) {
        if (!$this->id) {
            throw new Exception('$this->id is empty');
        }

        $accountKey= env('azure_account_key');
        $accountName=env('azure_account_name');

        $connectionString = "DefaultEndpointsProtocol=https;AccountName=$accountName;AccountKey=$accountKey";
        $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);

        $container = "vehicle-image";


        $ops = new ListBlobsOptions();
        $ops->setPrefix($this->id);
        $blob_list = $blobRestProxy->listBlobs($container, $ops);
        $blobs = $blob_list->getBlobs();

        foreach($blobs as $blob)
        {
            $newName = str_replace($this->id, $newId, $blob->getName());
            /**
             *         $destinationContainer,
            $destinationBlob,
            $sourceContainer,
            $sourceBlob,
            $options = null
             */
            \LOG::debug( "COPY ". $blob->getName() ." TO $newName <br /> \n");
            $blobRestProxy->copyBlob($container, $newName, $container, $blob->getName());
            // 		echo $blob->getName().": ".$blob->getUrl()."<br /> \n";
        }
    }

    public static function filterQuery($type,
                                       $user,
                                       $ownStat = NULL,
                                       $locationIds = NULL,
                                       $createDate = NULL) {

        $query = Vehicle::select('vehicle.*');
//        $ids = User::usersSameCompanyQuery($user->company_group_id)->select('user.id')->get();

        switch ($type) {
            case Dashboard::CARS_WIN_BOUGHT:
                $query->join('valuation', 'valuation.vehicle_id', '=', 'vehicle.id')
                    ->join('transaction', 'transaction.valuation_id', '=', 'valuation.id')
                    ->whereIn('source', [Auction::SOURCE_LIVE, Auction::SOURCE_BUY_NOW]);
                if ($locationIds) {
                    $query->join('user as buyer', 'buyer.id', '=', 'transaction.buyer_id')
                        ->whereIn('buyer.location_id', $locationIds);
                }
                if ($ownStat) {
                    $query->where('transaction.buyer_id', $user->id);
                }

                if ($createDate && $createDate['start'] && $createDate['end']) {
                    $times = [$createDate['start'], $createDate['end']];
                    $query->whereBetween('transaction.create_date', $times);
                }
                break;
            case Dashboard::CARS_LISTED:
                $now = Carbon::now();
                $query->join('valuation', 'valuation.vehicle_id', '=', 'vehicle.id')
                    ->join('auction_item', 'auction_item.valuation_id', '=', 'valuation.id')
                    ->leftJoin('location', 'location.id', '=', 'vehicle.trade_in_location')
                    ->join('session_has_item', 'session_has_item.auction_item_id', '=', 'auction_item.id')
                    ->join('auction_session', 'auction_session.id', '=', 'session_has_item.auction_session_id')
                    ->where(function($query) use ($now) {
                        return $query->where('auction_item.status', Auction::STATUS_CODE_APPROVE)
                            ->where(DB::raw('DATE_FORMAT(auction_session.startDate,"%Y-%m-%d")'),'>=',$now);
                    })->orWhere('auction_item.status', Auction::STATUS_CODE_PENDING);
                if ($createDate && $createDate['start'] && $createDate['end']) {
                    $times = [$createDate['start'], $createDate['end']];
                    $query->whereBetween('auction_session.create_date', $times);
                }

                if ($ownStat) {
                    $query->where('session_has_item.bid_user_id', $user->id);
                }

                if ($locationIds) {
                    $query->join('user as creator', 'creator.id', '=', 'auction_item.creator_id')
                        ->whereIn('creator.location_id', $locationIds);
                }
                break;
            case Dashboard::CARS_SOLD_AT_AUCTION:
                $query->join('valuation', 'valuation.vehicle_id', '=', 'vehicle.id')
                    ->join('transaction', 'transaction.valuation_id', '=', 'valuation.id')
                    ->whereIn('source', [Auction::SOURCE_LIVE, Auction::SOURCE_BUY_NOW])
                    ->where('transaction.status', Transaction::STATUS_COMPLETE);
                if ($locationIds) {
                    $query->join('user as seller', 'seller.id', '=', 'transaction.seller_id')
                        ->whereIn('seller.location_id', $locationIds);
                }

                if ($ownStat) {
                    $query->where('transaction.seller_id', $user->id);
                }

                if ($createDate && $createDate['start'] && $createDate['end']) {
                    $times = [$createDate['start'], $createDate['end']];
                    $query->whereBetween('transaction.create_date', $times);
                }
                break;
            case Dashboard::CARS_ON_WISHLIST_APPEARING_ON_NEXT_AUCTION:
                $wishListArr = NULL;
                if ($ownStat) {
                    $wishListArr = WishList::getByUserIds([$user->id]);
                }

                if ($locationIds) {
                    $ids = User::whereIn('user.location_id', $locationIds)->select('user.id')->get();
                    $wishListArr = WishList::getByUserIds($ids);
                }

                if ($wishListArr == NULL || count($wishListArr) == 0) {
                    return NULL;
                }

                $arrays = AuctionSession::getNextSessions();
                $nextSession = $arrays ? $arrays[0] : null;
                if ($nextSession) {
                    $query->join('valuation', 'valuation.vehicle_id', '=', 'vehicle.id')
                        ->join('auction_item', 'auction_item.valuation_id', '=', 'valuation.id')
                        ->leftJoin('location', 'location.id', '=', 'vehicle.trade_in_location')
                        ->join('session_has_item', 'session_has_item.auction_item_id', '=', 'auction_item.id')
                        ->join('auction_session', 'auction_session.id', '=', 'session_has_item.auction_session_id')
                        ->join('user', function ($query) {
                            $query->on('user.id', '=', 'valuation.user_id')
                                ->on('valuation.company_group_id', '=', 'user.company_group_id');
                        })
                        ->where(function ($query) use ($nextSession, $wishListArr) {
                            $query->whereIn('session_has_item.status', [Auction::STATUS_CODE_APPROVE])
                                ->where('auction_session.id', '=', $nextSession->id);
                            foreach ($wishListArr as $k => $wishList) {
                                if ($wishList->locationId) {
                                    $query->whereIn('vehicle.trade_in_location', [$wishList->locationId]);
                                }
                                if ($wishList->odometer_start && $wishList->odometer_end) {
                                    $query->whereBetween('vehicle.odometer', [$wishList->odometer_start, $wishList->odometer_end]);
                                }

                                if ($wishList->year_start&& $wishList->year_end) {
                                    $query->whereBetween('vehicle.build_date', [$wishList->year_start, $wishList->year_end]);
                                }
                            }
                        });


                    if ($createDate && $createDate['start'] && $createDate['end']) {
                        $times = [$createDate['start'], $createDate['end']];
                        $query->whereBetween('auction_session.create_date', $times);
                    }


                } else {
                    return NULL;
                }
                break;
        }
        return $query;
    }

    public static function percentageCarsListedVsSold($user, $ownStat = NULL, $locationIds = NULL, $createDate = NULL) {

        $query = Vehicle::filterQuery(
            Dashboard::CARS_LISTED,
            $user,
            $ownStat,
            $locationIds,
            $createDate
        );
        $carInAuction = $query ? $query->count(DB::raw('distinct vehicle.id')) : 0;

        $query = Vehicle::filterQuery(
            Dashboard::CARS_SOLD_AT_AUCTION,
            $user,
            $ownStat,
            $locationIds,
            $createDate
        );
        $carSoldAtAuction = $query ? $query->count(DB::raw('distinct vehicle.id')) : 0;

        if ($carInAuction > 0) {
            return floatval(number_format($carSoldAtAuction / $carInAuction * 100, 2)) . "%";
        } else {
            return "0%";
        }
    }

    public function offerName() {
        $attributes = [$this->model_family,
            $this->make,
            $this->model_family, 
            $this->series, 
            $this->style, 
            $this->engine_type, 
            $this->engine_capacity, 
            $this->engine_size, 
            $this->variant, 
            $this->cylinder,
            $this->transmission];

        $attributesStr = implode(" ", $attributes);
        return trim(preg_replace('!\s+!', ' ', $attributesStr));
    }
}