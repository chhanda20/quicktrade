<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Option extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];
    protected $table = 'glass_option';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public function getOption($optionStr) {
        $data = DB::select("
            SELECT RTRIM(glass_option_id) glass_option_id, RTRIM(`name`) name
            FROM glass_option
            WHERE glass_option_id IN ($optionStr)");
        return $data;
    }
}
