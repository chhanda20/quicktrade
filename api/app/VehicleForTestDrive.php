<?php

namespace App;

use app\Libraries\Dashboard;
use Illuminate\Support\Facades\DB;
use MicrosoftAzure\Storage\Blob\Models\ListBlobsOptions;
use Mockery\CountValidator\Exception;
use WindowsAzure\Common\ServicesBuilder;

class VehicleForTestDrive extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['vin', 'colour', 'registration_number', 'engine_number', 'odometer',
                           'registration_expiry_date', 'build_date', 'compliance_date', 'transmission',
                           'drive_train', 'chassis_number', 'trim', 'vehicle_model_id', 'has_one_owner',
                           'has_service_book', 'has_service_history', 'condition', 'trade_in_location',
                           'keys_location', 'model_family', 'make_id', 'make', 'model_code', 'registration_number',
                           'variant', 'colour', 'manufacturer_color' , 'body_type' , 'first_reg_date',
                           'registration_expiry_date', 'registered_state', 'build_date', 'compliance_date',
                           'engine_type', 'engine_size', 'engine_capacity', 'cylinder',  'fuel_type',
                           'transmission', 'drive_train', 'body_no', 'vin', 'chassis_number',
                           'trim', 'trim_color', 'glass_guide_code'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'vehicle_for_testdrive';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;
}
