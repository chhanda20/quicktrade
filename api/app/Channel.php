<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Channel extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'channel';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public static function updateByName($oldCode, $code)
    {
        return DB::table('channel')
            ->where('name', $oldCode)
            ->update(['name' => $code]);
    }

    public function getByEventId($eventId)
    {
        $sql = '
            SELECT c.`name`
            FROM channel_event ce INNER JOIN channel c ON c.id = ce.channel_id
            WHERE ce.event_id = :eventId';
        $params = [
            'eventId' => $eventId,
        ];
        $data = DB::select($sql,$params);
        return $data;
    }

    public function getByTypeAndRole($name, $listRole)
    {
        $roleListSQLArray = implode(',', $listRole);
        $defaultChannelType = "";
        if($name)
            $defaultChannelType = "channel.type = '$name' OR";

        return DB::select("SELECT channel.id as id, channel.`name` as channelName
                            FROM
                            channel
                            LEFT JOIN role ON channel.`name` = role.`code`
                            WHERE
                            $defaultChannelType
                            (channel.type = 'role' AND
                            role.id IN (".$roleListSQLArray."))");

    }
}
