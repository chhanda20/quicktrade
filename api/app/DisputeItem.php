<?php

namespace App;

use Illuminate\Support\Facades\DB;

class DisputeItem extends Backend
{

    const TYPE_INSPECTION = 1;
    const TYPE_DISPUTE = 2;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['repair_cost', 'inspection_item_id', 'description', 'note'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];

    protected $table = 'dispute_item';

    public static function getListItemByDisputeId($id)
    {
        return DB::select("SELECT * from dispute_item WHERE dispute_id = $id");
    }

    public static function cloneImages($oldDisputeId, $newDisputeId) {
    	
    	$accountKey= env('azure_account_key');
    	$accountName=env('azure_account_name');
    	
    	$connectionString = "DefaultEndpointsProtocol=https;AccountName=$accountName;AccountKey=$accountKey";
    	$blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);
    	
    	$container = "dispute-item";
    	
    	
    	$ops = new ListBlobsOptions();
    	$ops->setPrefix($oldDisputeId."_");
    	$blob_list = $blobRestProxy->listBlobs($container, $ops);
        $blobs = $blob_list->getBlobs();
        
        $numberCharacterOldDisputeId = strlen($oldDisputeId);
    	
    	foreach($blobs as $blob)
    	{
            $newBlobName = $newDisputeId.substr($blob->getName(), $numberCharacterOldDisputeId);
    		\LOG::debug( "COPY ". $blob->getName() ." TO $newName <br /> \n");
    		$blobRestProxy->copyBlob($container, $newName, $container, $blob->getName());
    	}
    }
}
