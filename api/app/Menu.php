<?php

namespace App;


use Illuminate\Support\Facades\DB;

class Menu extends Backend
{
    protected $table = 'menu';


    public static function getListMenu()
    {
        $menuList = DB::select("
                        SELECT menu.id, menu.`code`, menu.`name`, menu.description, menu.status_code,
                               menu.category, type_code.`value` AS `group_name`,`menu`.site_menu
                        FROM  menu
                        JOIN type_code ON menu.category = type_code.`code`
                        WHERE
                            type_code.type = 'MENU_GROUP'
                        
               ");

        return $menuList;
    }

    public static function getMenuListForRole($roleId)
    {
        return DB::select("SELECT menu.`code`, menu.`name`, type_code.`value` as group_name,`menu`.site_menu
                        FROM
                        role_has_menu
                        INNER JOIN menu ON role_has_menu.menu_id = menu.id
                        JOIN type_code ON menu.category = type_code.`code`
                        where type_code.type = 'MENU_GROUP' and role_has_menu.role_id = $roleId
                        GROUP BY menu.`code`");
    }

}
