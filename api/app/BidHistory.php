<?php

namespace App;

use Illuminate\Support\Facades\DB;

class BidHistory extends Backend
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [];
    protected $table = 'bid_history';

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public $timestamps = false;

    const EXTERNAL_OFFER = "external_offer";
    const LIVE_AUCTION = "live_auction";
}
