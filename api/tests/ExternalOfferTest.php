<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use App\UserRole;
use App\Role;
use App\ExternalOffer;
use App\Transaction;
use App\Document;
use App\Valuation;

class ExternalOfferTest extends TestCase
{

    /*
    -----------
    [s] customer -> search -> pick customer -> valuation tab -> click button [new] create valuation
    [s] send external offer
    [b] place my car -> incoming offer -> external offer tab -> bid vehicle
    [s] valuation -> summary -> accept bid from [b]
    [s] valuation -> pending -> approve
    [s] valuation -> complete -> pick vehicle, click button [trade]
    [s] valuation -> complete -> pick vehicle, click button [receive-vehicle]

    //auction session
    [dts] create auction session
    [s] auction -> stocklist tab -> pick vehicle (receive/trade-in vehicle) -> click button [send to auction]
    [dts] auction -> request list -> pick vehicle (above) -> click button [approve] -> choose time match auction session (above) -> [submit]
    -- auction session start --
    [b] auction -> live auction -> quick bid/max bid
    -- auction session end --

    //external offer
    [b] place my car -> incoming -> offer won tab -> pick vehicle, click button [set-location]
    
    ===> create transaction done
    [dts] transaction -> current -> pick transaction -> click button [receive-payment]
    [s] transaction -> current -> pick transaction -> button [vehicle-ready]
    [dts] transaction -> current -> pick transaction -> click button [....] x3 time
    [b] arriving vehicle -> pick vehicle click button [inspect] => click [accept]/[log-dispute]
    -- if accept => transaction status => awaiting document									[[1]]
    [b] + [s] transaction -> current -> pick item button [detail] -> up document
    [dts] transaction -> current -> check button [...] + click button [complete]
    => transaction complete
    -- if [log-dispute]
    [b] arriving vehicle -> inspect vehicle -> dispute item -> click [submit]
    [dts] dispute center -> resolve (accept) dispute -> click button [submit]
    => transaction become awaiting document (above)
    */

    // default setting
    const POST          = 'CURLOPT_POST';
    const GET           = 'CURLOPT_HTTPGET';
    const PUT           = 'PUT';

    const URL           = "http://moto-release.mmsoft.local/__backend";
    const CUSTOMER_ID   = 291;  //dinhanhhuyit@gmail.com
    const SELLER_ID     = 135;  //dinh+anhhuyit@gmail.com
    const BUYER_ID      = 132;  //dinhanhhuy+it@gmail.com
    const BUYER_ID2     = 134;  //dinhanh+huyit@gmail.com
    const MO_ID        = 130;  //dinhanhhuy.it@gmail.com

    private function seller() {
        return User::find($this::SELLER_ID);
    }

    private function buyer() {
        return User::find($this::BUYER_ID);
    }

    private function dts() {
        return User::find($this::MO_ID);
    }

    private function timeStamp() {
        $date = new DateTime();
        return $date->getTimestamp();
    }

    private function transaction() {
        return Transaction::find(self::$transaction_id);
    }

    // curl libraries
    private function initCurl($path, $headers = null ,$body = null ,$method = "CURLOPT_POST") {
        if(!isset($headers)) {
            $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body));
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $path);
        if ($method == "CURLOPT_POST") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        } else if ($method == "CURLOPT_HTTPGET") {
            curl_setopt($ch, CURLOPT_HTTPGET, 1);
        } else if ($method == "PUT") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
            curl_setopt($ch, CURLOPT_HEADER, false);          
        }  else if ($method == "DELETE") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $server_output = curl_exec($ch);
        curl_close ($ch);
        return $server_output;
    }

    // storage global data for test follow
    static $timestamp           = 0;
    static $valuation_id        = 0;
    static $vin                 = 0;
    static $vehicle_year        = 0;
    static $externaloffer_id    = 0;
    static $dispute_id          = 0;
    static $transaction_id      = 0;
    static $auction_session_id  = 0;
    static $auction_id          = 0;

    //testing follow

    /*----------
    | Valuation |
     ----------*/

    public function testBeginValuation() {
        self::$timestamp = $this->timeStamp();
        $vin  = self::$timestamp;
        $body = [
            "customer_id" => $this::CUSTOMER_ID,
            "vin" => $vin,
            "registration_number" => self::$timestamp,
            "make" => 'BMW',
            "model_family" => 7,
            "variant" => '40i EXECUTIVE',
            "colour" => 2,
            "build_date" => "2009/01/01",
            "registration_expiry_date" => '2017/11/24',
            "has_one_owner" => 1,
            "has_service_book" => 1,
            "has_service_history" => 1,
            "trade_in_location" => 'location trade '.self::$timestamp,
            "keys_location" => 'location vehicle '.self::$timestamp,
            "compliance_date" => '2017/11/24',
            "odometer" => self::$timestamp,
            "engine_number" => self::$timestamp,
            "i_vehicle" => 2,
            "i_make" => '',
            "i_model" => null,
            "i_variant" => null,
            "i_finance_owe" => false,
            "i_payout" => 0,
            "i_monthly_payment" => 0,
            "ownership_type" => ["code" => 1, 'value' => ''],
            "special_disclosures" => ['1', '2'],
            "repairable_write_off" => 2,
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/valuations/_begin", $headers, $body);
        $result = json_decode($result);

        $this->assertTrue(isset($result->id) && $result->id);
        echo "[s] begin valuation id: [$result->id]\n";
        
        self::$valuation_id = $result->id;
        self::$vin          = $vin;
        self::$vehicle_year = '2009';   //get from build date

        return $result->id;
    }

    public function testRequestValuation() {
        $valuation_id = self::$valuation_id;
        $body = [
            "start_performing" => false,
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/valuations/$valuation_id/requestValuation", $headers, $body, $this::PUT);
        $result = json_decode($result);

        $this->assertTrue(isset($result->id) && $result->id);
        echo "[s] request valuation id: [$valuation_id]\n";
        return $result;
    }

    // public function testGetVehicleInfo() {
    //     $valuation_id = self::$valuation_id;
    //     $vin          = self::$vin;
    //     $vehicle_year = self::$vehicle_year;

    //     $body = [
    //     ];

    //     $body = json_encode($body);
    //     $headers = [
    //         "Content-Type: application/json",
    //         "Content-Length: " . strlen($body),
    //         "token: ".$this->seller()->token
    //     ];

    //     echo "/vehicles/getVehicle/$vin/$vehicle_year";
    //     $result = $this->initCurl($this::URL . "/vehicles/getVehicle/$vin/$vehicle_year", $headers, null, $this::GET);
    //     $result = json_decode($result);

    //     echo "get vehicle id: [$result->id]";
    //     $this->assertTrue(isset($result->id) && $result->id);
        
    //     return $result;
    // }

    public function testAcceptValuation() {
        $valuation_id = self::$valuation_id;
        $body = [
            "start_performing" => false,
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/valuations/$valuation_id/_accept ", $headers, $body, $this::POST);
        $result = json_decode($result);

        $this->assertTrue(isset($result->id) && $result->id);
        echo "[s] accept valuation id: [$valuation_id]\n";
        return $result;
    }

    public function testUpdateVehicle() {
        //missing many step valuate vehile, just pick 1
        $valuation_id = self::$valuation_id;
        $body = [
            "make" => "BMW",
            "model_code" => "7",
            "registration_number" => self::$timestamp,
            "variant" => "40i EXECUTIVE",
            "colour" => "2",
            "manufacturer_color" => "1",
            "first_reg_date" => "2016/01/01",
            "registration_expiry_date" => "2017/11/24",
            "registered_state" => null,
            "build_date" => "2009/01/01",
            "compliance_date" => "2017/11/24",
            "engine_type" => "MULTI POINT F/INJ",
            "engine_size" => "4.0L",
            "engine_capacity" => "4000",
            "cylinder" => "V8",
            "fuel_type" => "1",
            "transmission" => "6 SP AUTOMATIC STEPT",
            "drive_train" => "2",
            "body_no" => "",
            "vin" => self::$timestamp,
            "chassis_number" => "",
            "trim" => "2",
            "trim_color" => "3",
            "body_type" => "4D SEDAN",
            "glass_guide_code" => "BMW--74XE640GRY2009A"
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/valuations/$valuation_id/updateVehicle", $headers, $body, $this::PUT);
        $result = json_decode($result);

        $this->assertTrue(isset($result->id) && $result->id);
        echo "[s] update vehicle valuation id: [$valuation_id]\n";
        return $result;
    }

    public function testUpdateValuationVehicleImage() {
        $valuation_id   = self::$valuation_id;
        $documentData   = file_get_contents("tests/vehicle_sample.jpeg");
        $vehicle_id     = Valuation::find($valuation_id)->vehicle_id;
        $doc_name       = "$vehicle_id.jpg";
        Document::uploadToAzure('vehicle-image', $documentData, self::$valuation_id, $doc_name, false);
        echo "[s] update vehicle image id [$vehicle_id] valuation id: [$valuation_id]\n";
    }

    public function testUpdateOverallRating() {
        /*
        [2017-11-13 09:36:51] lumen.DEBUG: qs0j1x9E1mZdphykHAg7RAB2nrY5qmaW57I35vbk valuations/2232/overallRating [PUT]         {
            "overall_rating": 0
        } 
        [2017-11-13 09:36:51] lumen.DEBUG: qs0j1x9E1mZdphykHAg7RAB2nrY5qmaW57I35vbk     --> 200 {
            "id": 2232,
            "valuer_id": 4,
            "status": 14,
            "customer_id": 152,
            "user_id": 4,
            "vehicle_id": 939,
            "create_date": "2017-11-01 16:27:50",
            "update_date": "2017-11-09 20:16:04",
            "keys_location": null,
            "negotiating_price": null,
            "max_price": null,
            "has_one_owner": 1,
            "estimated_recon_cost": 0,
            "requeste
        */
        $valuation_id = self::$valuation_id;
        $body = [
            "overall_rating" => 2,
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/valuations/$valuation_id/overallRating", $headers, $body, $this::PUT);
        $result = json_decode($result);

        $this->assertTrue(isset($result->id) && $result->id);
        echo "[s] update overal rating valuation id: [$valuation_id]\n";
        return $result;
    }

    public function testUpdateSalesInfo() {
        /*
        [2017-11-13 09:36:51] lumen.DEBUG: qs0j1x9E1mZdphykHAg7RAB2nrY5qmaW57I35vbk valuations/2232/salesInfo [PUT] {
            "max_valuation_price": 0,
            "sale_status": 0,
            "valuation_price": 0
        }
        [2017-11-13 09:36:51] lumen.DEBUG: qs0j1x9E1mZdphykHAg7RAB2nrY5qmaW57I35vbk     --> 200 {
            "sale_status": 0,
            "valuation_price": 0,
            "max_valuation_price": 0
        }
        */
        $valuation_id = self::$valuation_id;
        $body = [
            "max_valuation_price" => 20000,
            "sale_status" => 0,
            "valuation_price" => 1000
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/valuations/$valuation_id/salesInfo", $headers, $body, $this::PUT);
        $result = json_decode($result);

        $this->assertTrue(isset($result->sale_status));
        echo "[s] update sales info valuation id: [$valuation_id]\n";
        return $result;
    }

    public function testRecoditioningCost() {
        /*
        [2017-11-13 09:36:51] lumen.DEBUG: qs0j1x9E1mZdphykHAg7RAB2nrY5qmaW57I35vbk valuations/2232/reconditioningCost [        PUT] {
            "estimated_recon_cost": 0
        } 
        [2017-11-13 09:36:51] lumen.DEBUG: qs0j1x9E1mZdphykHAg7RAB2nrY5qmaW57I35vbk     --> 200 {}
        */
        $valuation_id = self::$valuation_id;
        $body = [
            "estimated_recon_cost" => 10,
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/valuations/$valuation_id/reconditioningCost", $headers, $body, $this::PUT);
        $result = json_decode($result);

        echo "[s] update recoditioning cost valuation id: [$valuation_id]\n";
        return $result;
    }

    public function testUpdateOfferPrice() {
        /*
        [2017-11-13 09:36:51] lumen.DEBUG: qs0j1x9E1mZdphykHAg7RAB2nrY5qmaW57I35vbk valuations/2232/offerPrice [PUT] {
            "offer_price": 0
        }
        [2017-11-13 09:36:51] lumen.DEBUG: qs0j1x9E1mZdphykHAg7RAB2nrY5qmaW57I35vbk     --> 200 {
            "id": 2232,
            "valuer_id": 4,
            "status": 14,
            "customer_id": 152,
            "user_id": 4,
            "vehicle_id": 939,
            "create_date": "2017-11-01 16:27:50",
            "update_date": "2017-11-09 20:16:04",
            "keys_location": null,
            "negotiating_price": null,
            "max_price": null,
            "has_one_owner": 1,
            "estimated_recon_cost": 0,
            "requested_approve_price": null,
            "test_driven": 0,
            "mechanical_additional_detail": "",
            "offer_value": 0,
            "valuation_value": 0,
            "requested_price": 0,
            "trade_price": 321,
            "offer_price": 0,
            "proposed_arrival_date": "2017-11-01 16:28:52",
            "note": null,
            "has_service_history": 1,
            "has_service_book": 1,
            "notify_sent_counter": null,
            "value_of_options": null,
            "overall_rating": 0,
            "general_recondition_cost": 0,
            "total_recon_cost": 0,
            "version_creator": 4,
            "origin_id": 2232,
            "version": 2,
            "version_locker_time": null,
            "version_locker_user_id": 0,
            "company_group_id": 7,
            "stock_number": "f23",
            "from_version": 1,
            "i_finance_owe": 0,
            "i_institution": null,
            "i_payout": null,
            "i_monthly_payment": null,
            "ownership_type": {
                "code": 3,
                "value": "Ex Government"
            },
            "special_disclosures": "2",
            "i_vehicle": 3,
            "i_make": null,
            "i_model": null,
            "i_variant": null,
            "repairable_write_off": null
        }
        */
        $valuation_id = self::$valuation_id;
        $body = [
            "offer_price" => 10,
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/valuations/$valuation_id/offerPrice", $headers, $body, $this::PUT);
        $result = json_decode($result);

        $this->assertTrue(isset($result->id) && $result->id);
        echo "[s] update offerPrice valuation id: [$valuation_id]\n";
        return $result;
    }

    public function testCompleteValuation() {
        /*
        [2017-11-17 05:44:27] lumen.DEBUG: R5YgBYwrxRQQiBklMSWwZFn7WGPVLVs0B74ZTEax valuations/2890/complete [PUT] null
        [2017-11-17 05:44:27] lumen.DEBUG: R5YgBYwrxRQQiBklMSWwZFn7WGPVLVs0B74ZTEax     --> 200 {
            "id": 2890,
            "model_family": "A1",
            "colour": "White",
            "year": 2015,
            "requested_approve_price": null,
            "negotiating_price": null,
            "max_price": null,
            "user_id": 129,
            "make": "AUDI",
            "status": 4,
            "registration_number": "DSAFNFIEO",
            "company_group_id": 28,
            "valuer_id": 129,
            "make_id": null,
            "offer_value": 1000,
            "valuation_value": 3000,
            "requested_price": 0
        }
        */
        $valuation_id = self::$valuation_id;
        $body = [
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/valuations/$valuation_id/complete", $headers, $body, $this::PUT);
        $result = json_decode($result);

        $this->assertTrue(isset($result->id) && $result->id);
        echo "[s] complete valuation id: [$valuation_id]\n";
        return $result;
    }

    /*----------------
    | External offer  |
     ----------------*/
    public function testSendExternalOffer() {
        $valuation_id = self::$valuation_id;
        $body = [
            "arrivalDate" => date("Y-m-d"),
            "valuationId" => $valuation_id,
            "group" => null
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result                 = $this->initCurl($this::URL . "/externalOffer", $headers, $body, $this::POST);
        $result                 = json_decode($result);
        self::$externaloffer_id = ExternalOffer::all()[ExternalOffer::count() - 1]->id;
        $externaloffer_id       = self::$externaloffer_id;

        $this->assertTrue(isset($externaloffer_id) && $externaloffer_id);
        echo "[s] send external offer id: [$externaloffer_id]\n";
        return $result;
    }

    public function testBidExternalOffer() {
        /* [2017-11-27 10:45:24] lumen.DEBUG: R5YgBYwrxRQQiBklMSWwZFn7WGPVLVs0B74ZTEax externalOffer/bid [POST] {
            "externalOfferId": "553",
            "price": "10000",
            "expiryDate": "2017-12-19"
        }  
        [2017-11-27 10:45:24] lumen.DEBUG: R5YgBYwrxRQQiBklMSWwZFn7WGPVLVs0B74ZTEax 	--> 400 {
            "error": {
            "code": "invalid",
            "detail": "Status should be pending or offer_made"
            ----
            "success": true
        }
        */
        $externaloffer_id = self::$externaloffer_id;

        $valuation_id = self::$valuation_id;
        $body = [
            "externalOfferId" => $externaloffer_id,
            "price" => 10000,
            "expiryDate" => "2020-12-19",
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->buyer()->token
        ];

        $result = $this->initCurl($this::URL . "/externalOffer/bid", $headers, $body, $this::POST);
        $result = json_decode($result);

        $this->assertTrue(isset($result->success) && $result->success);
        echo "[b] bid external offer id: [$externaloffer_id]\n";
        return $result;
    }

    public function testAcceptBid() {
        /* 
        [2017-11-27 10:43:38] lumen.DEBUG: mBBPLWtg6BZXRcJjP6I1utF0V5XJTUhjEfayyfx7 externalOffer/accept [POST] {
            "externalOfferId": "551"
        }  
        [2017-11-27 10:43:38] lumen.DEBUG: mBBPLWtg6BZXRcJjP6I1utF0V5XJTUhjEfayyfx7 	--> 200 {
            "success": true
        }
        */
        $externaloffer_id = self::$externaloffer_id;
        $valuation_id = self::$valuation_id;
        $body = [
            "externalOfferId" => $externaloffer_id,
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/externalOffer/accept", $headers, $body, $this::POST);
        $result = json_decode($result);

        $this->assertTrue(isset($result->success) && $result->success);
        echo "[s] accept bid external offer id: [$externaloffer_id]\n";
        return $result;
    }

    public function testApproveValuation() {
        /*
        [2017-11-27 10:45:55] lumen.DEBUG: mBBPLWtg6BZXRcJjP6I1utF0V5XJTUhjEfayyfx7 valuations/2910/approve [PUT] null  
        [2017-11-27 10:45:55] lumen.DEBUG: mBBPLWtg6BZXRcJjP6I1utF0V5XJTUhjEfayyfx7 	--> 200 {
            "id": 2910,
            "model_family": "Z4",
            "colour": "Green",
            "year": 2016,
            "requested_approve_price": null,
            "negotiating_price": null,
            "max_price": null,
            "user_id": 128,
            "make": "BMW",
            "status": 4,
            "registration_number": "HFGUI",
            "company_group_id": 27,
            "valuer_id": 128,
            "make_id": null,
            "offer_value": 6000,
            "valuation_value": 10000,
            "requested_price": 0
        } 
        */

        $externaloffer_id   = self::$externaloffer_id;
        $valuation_id = self::$valuation_id;
        $body = [
            
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/valuations/$valuation_id/approve", $headers, $body, $this::PUT);
        $result = json_decode($result);

        $this->assertTrue(isset($result->id) && $result->id);
        echo "[s] head valuer approve valuation id: [$valuation_id]\n";
        return $result;
    }

    public function testTradeVehicle() {
        /*
        [2017-11-27 14:32:55] lumen.DEBUG: mBBPLWtg6BZXRcJjP6I1utF0V5XJTUhjEfayyfx7 valuations/2954/trade [PUT] {
            "note": "note",
            "trade_price": "10000.00",
            "proposed_arrival_date": "2017-11-28"
        }  
        [2017-11-27 14:32:55] lumen.DEBUG: mBBPLWtg6BZXRcJjP6I1utF0V5XJTUhjEfayyfx7 	--> 200 {
            "id": 2954,
            "valuer_id": 128,
            "status": 6,
            "customer_id": 291,
            "user_id": 128,
            "vehicle_id": 1166,
            "create_date": "2017-11-27 14:31:48",
            "update_date": "2017-11-27 14:32:54",
            "keys_location": 0,
            "negotiating_price": null,
            "max_price": null,
            "has_one_owner": 1,
            "estimated_recon_cost": 10,
            "requested_approve_price": null,
            "test_driven": 0,
            "mechanical_additional_detail": "",
            "offer_value": 1000,
            "valuation_value": 20000,
            "requested_price": 0,
            "trade_price": "10000.00",
            "offer_price": 10,
            "proposed_arrival_date": "2017-11-28",
            "note": "note",
            "has_service_history": 1,
            "has_service_book": 1,
            "notify_sent_counter": null,
            "value_of_options": null,
            "overall_rating": 2,
            "general_recondition_cost": 0,
            "total_recon_cost": null,
            "version_creator": 128,
            "origin_id": 0,
            "version": 1,
            "version_locker_time": null,
            "version_locker_user_id": 0,
            "company_group_id": 27,
            "stock_number": null,
            "from_version": null,
            "i_finance_owe": 0,
            "i_institution": null,
            "i_payout": 0,
            "i_monthly_payment": 0,
            "ownership_type": "{\"code\":1,\"value\":\"\"}",
            "special_disclosures": "1,2",
            "i_vehicle": 2,
            "i_make": "",
            "i_model": null,
            "i_variant": null,
            "repairable_write_off": 2
        } 
        */

        $externaloffer_id   = self::$externaloffer_id;
        $valuation_id = self::$valuation_id;
        $body = [
            "note" => "note",
            "trade_price" => "10000.00",
            "proposed_arrival_date" => "2018-11-28",
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/valuations/$valuation_id/trade", $headers, $body, $this::PUT);
        $result = json_decode($result);

        $this->assertTrue(isset($result->id) && $result->id == $valuation_id);
        echo "[s] trade external offer id: [$externaloffer_id]\n";
        return $result;
    }

    public function testCreateValationVersion() {
        /*
        [2017-11-27 14:40:08] lumen.DEBUG: mBBPLWtg6BZXRcJjP6I1utF0V5XJTUhjEfayyfx7 valuations/2954/version [POST] null  
        [2017-11-27 14:40:08] lumen.DEBUG: mBBPLWtg6BZXRcJjP6I1utF0V5XJTUhjEfayyfx7 	--> 200 {
            "msg": "success",
            "old_valuation_id": 2955,
            "newVersionId": 2
        }*/
        $externaloffer_id   = self::$externaloffer_id;
        $valuation_id = self::$valuation_id;
        $body = [
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/valuations/$valuation_id/version", $headers, $body, $this::POST);
        $result = json_decode($result);

        $this->assertTrue(isset($result->msg) && $result->msg === 'success');
        echo "[s] create new version valuation from id: [$valuation_id]\n";
        return $result;
    }

    public function testValuationTradeIn() {
        /*
        [2017-11-27 14:40:09] lumen.DEBUG: mBBPLWtg6BZXRcJjP6I1utF0V5XJTUhjEfayyfx7 valuations/2954/tradein [PUT] {
            "stock_number": "1",
            "location_id": "99"
        }  
        [2017-11-27 14:40:09] lumen.DEBUG: mBBPLWtg6BZXRcJjP6I1utF0V5XJTUhjEfayyfx7 	--> 200 {
            "id": 2954,
            "valuer_id": 128,
            "status": 7,
            "customer_id": 291,
            "user_id": 128,
            "vehicle_id": 1166,
            "create_date": "2017-11-27 14:40:08",
            "update_date": "2017-11-27 14:40:09",
            "keys_location": 0,
            "negotiating_price": null,
            "max_price": null,
            "has_one_owner": 1,
            "estimated_recon_cost": 10,
            "requested_approve_price": null,
            "test_driven": 0,
            "mechanical_additional_detail": "",
            "offer_value": 1000,
            "valuation_value": 20000,
            "requested_price": 0,
            "trade_price": 10000,
            "offer_price": 10,
            "proposed_arrival_date": "2017-11-27 14:40:09",
            "note": null,
            "has_service_history": 1,
            "has_service_book": 1,
            "notify_sent_counter": null,
            "value_of_options": null,
            "overall_rating": 2,
            "general_recondition_cost": 0,
            "total_recon_cost": null,
            "version_creator": 128,
            "origin_id": 0,
            "version": 2,
            "version_locker_time": "2017-11-27 14:40:07",
            "version_locker_user_id": 128,
            "company_group_id": 27,
            "stock_number": "1",
            "from_version": 1,
            "i_finance_owe": 0,
            "i_institution": null,
            "i_payout": 0,
            "i_monthly_payment": 0,
            "ownership_type": "{\"code\":1,\"value\":\"\"}",
            "special_disclosures": "1,2",
            "i_vehicle": 2,
            "i_make": "",
            "i_model": null,
            "i_variant": null,
            "repairable_write_off": 2
        }  
        */
        $externaloffer_id   = self::$externaloffer_id;
        $valuation_id = self::$valuation_id;
        $body = [
            "stock_number" => self::$timestamp,
            "location_id"  => $this->seller()->location_id
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/valuations/$valuation_id/tradein", $headers, $body, $this::PUT);
        $result = json_decode($result);

        $this->assertTrue(isset($result->id) && $result->id === $valuation_id);
        echo "[s] trade in valuation id: [$valuation_id]\n";
        return $result;
    }

    public function testReleaseVersion() {
        /*
        [2017-11-27 14:40:09] lumen.DEBUG: mBBPLWtg6BZXRcJjP6I1utF0V5XJTUhjEfayyfx7 valuations/2954/releaseVersion [POST] null  
        [2017-11-27 14:40:09] lumen.DEBUG: mBBPLWtg6BZXRcJjP6I1utF0V5XJTUhjEfayyfx7 	--> 200 {
            "code": 0,
            "msg": "new version will be kept"
        }  
        */
        $externaloffer_id   = self::$externaloffer_id;
        $valuation_id = self::$valuation_id;
        $body = [
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/valuations/$valuation_id/releaseVersion", $headers, $body, $this::POST);
        $result = json_decode($result);

        $this->assertTrue(isset($result->code) && $result->code === 0);
        echo "[s] release unchange version valuation from id: [$valuation_id]\n";
        return $result;
    }

    /*------------
    | Transaction |
     ------------*/

    public function testSetLocation() {
        /*
        [2017-11-27 15:30:11] lumen.DEBUG: R5YgBYwrxRQQiBklMSWwZFn7WGPVLVs0B74ZTEax externalOffer/setLocation [POST] {
            "externalOfferId": "574",
            "locationId": "100"
        }  
        [2017-11-27 15:30:11] lumen.DEBUG: R5YgBYwrxRQQiBklMSWwZFn7WGPVLVs0B74ZTEax 	--> 200 {
            "success": true
        }
        */
        $externaloffer_id   = self::$externaloffer_id;
        $valuation_id = self::$valuation_id;
        $body = [
            "externalOfferId" => self::$externaloffer_id,
            "locationId" => $this->buyer()->location_id
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/externalOffer/setLocation", $headers, $body, $this::POST);
        $result = json_decode($result);

        self::$transaction_id = DB::table('transaction')->orderBy('create_Date', 'desc')->first()->id;
        $transaction_id       = self::$transaction_id;

        $this->assertTrue(isset($result->success) && $result->success && $transaction_id);
        echo "[b] set location, create new transaction id: [$transaction_id]\n";

        return $result;
    }

    public function testReceivePayment() {
        /*
        [2017-11-27 15:56:29] lumen.DEBUG: fL5WlAOUATFOxVaB3F6DKLVkpeol3Nvgq3XZg98k transaction/_paymentReceived [PUT] {
            "payments": [
                {
                    "rid": "T000000296",
                    "amount": "10281"
                }
            ]
        }  
        [2017-11-27 15:56:29] lumen.DEBUG: fL5WlAOUATFOxVaB3F6DKLVkpeol3Nvgq3XZg98k 	--> 200 []
        */
        $externaloffer_id   = self::$externaloffer_id;
        $valuation_id       = self::$valuation_id;
        $transaction_id     = self::$transaction_id;
        $body = [
            "payments" => [
                [
                    "rid" => $this->transaction()->rid,
                    "amount" => 11000
                ]
            ]
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/transaction/_paymentReceived", $headers, $body, $this::PUT);
        $result = json_decode($result);

        echo "[dts] receive payment transaction id: [$transaction_id]\n";
        return $result;
    }

    public function testVehicleReady() {
        /*
        [2017-11-27 16:13:27] lumen.DEBUG: mBBPLWtg6BZXRcJjP6I1utF0V5XJTUhjEfayyfx7 transaction/296/readyForTransport [PUT] null  
        [2017-11-27 16:13:27] lumen.DEBUG: mBBPLWtg6BZXRcJjP6I1utF0V5XJTUhjEfayyfx7 	--> 200 {
            "id": 296,
            "valuation_id": 2960,
            "buyer_id": 129,
            "seller_id": 128,
            "buyer_location": 100,
            "valuation_version": 2,
            "status": 22,
            "vehicle_price": 10000,
            "shipping_price": 31,
            "seller_fee": 250,
            "buyer_fee": 250,
            "inspect_time": null,
            "create_date": "2017-11-27 15:30:06",
            "update_date": "2017-11-27 16:13:27",
            "rid": "T000000296",
            "source": "external_offer",
            "source_id": 574,
            "seller_location": 99,
            "transport_no": null,
            "payment_receive": 10281,
            "seller_pay_receive": 0,
            "payment_receive_time": "2017-11-27 15:56:28"
        }
        */
        $externaloffer_id   = self::$externaloffer_id;
        $valuation_id       = self::$valuation_id;
        $transaction_id     = self::$transaction_id;
        $body = [];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/transaction/$transaction_id/readyForTransport", $headers, $body, $this::PUT);
        $result = json_decode($result);

        $this->assertTrue(isset($result->id) && $result->id === self::$transaction_id);

        echo "[s] update status to readyForTransport (Booking transport) transaction id: [$transaction_id]\n";
        return $result;
    }

    public function testTransactionBooked() {
        /*
        [2017-11-28 09:07:34] lumen.DEBUG: JN4IVZJBmZdakLbrT3s8l3cHDl8kggdxV8ouyjbg transaction/308/transportBooked [PUT] {
            "transport_no": "T000000308"
        }  
        [2017-11-28 09:07:34] lumen.DEBUG: JN4IVZJBmZdakLbrT3s8l3cHDl8kggdxV8ouyjbg 	--> 200 {
            "id": 308,
            "valuation_id": 2985,
            "buyer_id": 129,
            "seller_id": 128,
            "buyer_location": 100,
            "valuation_version": 2,
            "status": 23,
            "vehicle_price": 10000,
            "shipping_price": 31,
            "seller_fee": 250,
            "buyer_fee": 250,
            "inspect_time": null,
            "create_date": "2017-11-28 09:01:05",
            "update_date": "2017-11-28 09:07:34",
            "rid": "T000000308",
            "source": "external_offer",
            "source_id": 586,
            "seller_location": 99,
            "transport_no": "T000000308",
            "payment_receive": 11000,
            "seller_pay_receive": 0,
            "payment_receive_time": "2017-11-28 09:01:10"
        }  
        */
        $externaloffer_id   = self::$externaloffer_id;
        $valuation_id       = self::$valuation_id;
        $transaction_id     = self::$transaction_id;
        $body = [
            "transport_no" => self::$timestamp
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/transaction/$transaction_id/transportBooked", $headers, $body, $this::PUT);
        $result = json_decode($result);

        $this->assertTrue(isset($result->id) && $result->id === self::$transaction_id);

        echo "[dts] update status to booked transaction id: [$transaction_id]\n";
        return $result;
    }

    public function testTransactionInTransit() {
        /*
        [2017-11-28 09:11:55] lumen.DEBUG: JN4IVZJBmZdakLbrT3s8l3cHDl8kggdxV8ouyjbg transaction/308/inTransit [PUT] null  
        [2017-11-28 09:11:55] lumen.DEBUG: JN4IVZJBmZdakLbrT3s8l3cHDl8kggdxV8ouyjbg 	--> 200 {
            "id": 308,
            "valuation_id": 2985,
            "buyer_id": 129,
            "seller_id": 128,
            "buyer_location": 100,
            "valuation_version": 2,
            "status": 24,
            "vehicle_price": 10000,
            "shipping_price": 31,
            "seller_fee": 250,
            "buyer_fee": 250,
            "inspect_time": null,
            "create_date": "2017-11-28 09:01:05",
            "update_date": "2017-11-28 09:11:52",
            "rid": "T000000308",
            "source": "external_offer",
            "source_id": 586,
            "seller_location": 99,
            "transport_no": "T000000308",
            "payment_receive": 11000,
            "seller_pay_receive": 0,
            "payment_receive_time": "2017-11-28 09:01:10"
        } 
        */
        $externaloffer_id   = self::$externaloffer_id;
        $valuation_id       = self::$valuation_id;
        $transaction_id     = self::$transaction_id;
        $body = [];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/transaction/$transaction_id/inTransit", $headers, $body, $this::PUT);
        $result = json_decode($result);

        $this->assertTrue(isset($result->id) && $result->id === self::$transaction_id);

        echo "[dts] update status to in-transit transaction id: [$transaction_id]\n";
        return $result;
    }

    function testTransactionShipped() {
        /*
        [2017-11-28 09:15:19] lumen.DEBUG: JN4IVZJBmZdakLbrT3s8l3cHDl8kggdxV8ouyjbg transaction/308/shipped [PUT] null  
        [2017-11-28 09:15:19] lumen.DEBUG: JN4IVZJBmZdakLbrT3s8l3cHDl8kggdxV8ouyjbg 	--> 200 {
            "id": 308,
            "valuation_id": 2985,
            "buyer_id": 129,
            "seller_id": 128,
            "buyer_location": 100,
            "valuation_version": 2,
            "status": 3,
            "vehicle_price": 10000,
            "shipping_price": 31,
            "seller_fee": 250,
            "buyer_fee": 250,
            "inspect_time": null,
            "create_date": "2017-11-28 09:01:05",
            "update_date": "2017-11-28 09:15:16",
            "rid": "T000000308",
            "source": "external_offer",
            "source_id": 586,
            "seller_location": 99,
            "transport_no": "T000000308",
            "payment_receive": 11000,
            "seller_pay_receive": 0,
            "payment_receive_time": "2017-11-28 09:01:10"
        }  
        */
        $externaloffer_id   = self::$externaloffer_id;
        $valuation_id       = self::$valuation_id;
        $transaction_id     = self::$transaction_id;
        $body = [];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/transaction/$transaction_id/shipped", $headers, $body, $this::PUT);
        $result = json_decode($result);

        $this->assertTrue(isset($result->id) && $result->id === self::$transaction_id);

        echo "[dts] update status to shipped transaction id: [$transaction_id]\n";
        return $result;
    }

    public function testAcceptTransaction() {
        /*
        [2017-11-28 09:20:19] lumen.DEBUG: R5YgBYwrxRQQiBklMSWwZFn7WGPVLVs0B74ZTEax transaction/308/accept [PUT] null  
        [2017-11-28 09:20:19] lumen.DEBUG: R5YgBYwrxRQQiBklMSWwZFn7WGPVLVs0B74ZTEax 	--> 200 {
            "company_group_id": 28,
            "rego": null,
            "vin": "1511834449",
            "colour": 2,
            "registration_number": "1511834449",
            "engine_number": "1511834449",
            "engine_type_id": null,
            "engine_size": "4.0L",
            "cylinder": "V8",
            "fuel_type": 1,
            "doors_count": null,
            "key_number": null,
            "radio_code": null,
            "boot_key": null,
            "ignition_key": null,
            "wheel_size": null,
            "odometer": 1511834449,
            "odometer_date": null,
            "registration_expiry_date": "2017-11-24",
            "registered_state": null,
            "build_date": "2009-01-01",
            "compliance_date": "2017-11-24",
            "first_reg_date": "2016-01-01",
            "delivery_date": null,
            "delivery_odo": null,
            "last_service_date": null,
            "last_service_odo": null,
            "last_service_dealer": null,
            "status_code": null,
            "warranty_type": null,
            "fleet_company_id": null,
            "fleet_no": null,
            "ssm_service_model_id": null,
            "customer_note": null,
            "followup_note": null,
            "notes": null,
            "glass_guide_code": "BMW--74XE640GRY2009A",
            "glass_guide_nvic": null,
            "transmission": "6 SP AUTOMATIC STEPT",
            "drive_train": 2,
            "chassis_number": null,
            "trim": 2,
            "vehicle_model_code": "",
            "has_one_owner": 0,
            "has_service_book": null,
            "has_service_history": null,
            "condition": null,
            "model_family": "7",
            "make_id": null,
            "model_code": "7",
            "model_description": null,
            "family": null,
            "variant": "40i EXECUTIVE",
            "series": null,
            "style": null,
            "manuf_id": null,
            "driver_id": null,
            "owner_id": null,
            "body_type_id": null,
            "manufacturer_color": 1,
            "body_no": null,
            "trim_color": 3,
            "creator_id": 129,
            "create_date": "2017\/11\/28 09:20:17",
            "updater_id": null,
            "update_date": "2017\/11\/28 09:20:17",
            "trade_in_location": 100,
            "keys_location": "locat",
            "sale_status": 0,
            "make": "BMW",
            "body_type": "4D SEDAN",
            "engine_type": "MULTI POINT F\/INJ",
            ...
        }
        */

        $externaloffer_id   = self::$externaloffer_id;
        $valuation_id       = self::$valuation_id;
        $transaction_id     = self::$transaction_id;
        $body = [];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->seller()->token
        ];

        $result = $this->initCurl($this::URL . "/transaction/$transaction_id/accept", $headers, $body, $this::PUT);
        $result = json_decode($result);

        $this->assertTrue(isset($result->vin) && $result->vin == self::$timestamp);
        $this->assertTrue(isset($result->registration_number) && $result->registration_number == self::$timestamp);

        echo "[b] accept transaction id: [$transaction_id]\n";
        return $result;
    }

    //if accept => transaction status => awaiting document	

    public function testUploadDocument() {
        /*
        [2017-11-28 09:56:34] lumen.DEBUG: R5YgBYwrxRQQiBklMSWwZFn7WGPVLVs0B74ZTEax accounting/308/documents [POST] {
            "document_name": [
                "308_vehicle_transfer_form.jpg"
            ]
        }  
        [2017-11-28 09:56:34] lumen.DEBUG: R5YgBYwrxRQQiBklMSWwZFn7WGPVLVs0B74ZTEax 	--> 200 {
            "success": true
        }  
        */

        $transaction_id = self::$transaction_id;
        $extension = 'jpg';

        $documentData = file_get_contents("tests/sample.$extension");

        $documentNames = [
            "vehicle_transfer_form", 
            "vehicle_transfer_additional", 
            "invoice_vehicle_minus_dispute", 
            "ppsr_financial_payout"
        ];
        foreach ($documentNames as $documentName) {
            
            $doc_name   = $transaction_id."_".$documentName.".".$extension;
            $blob_name  = 'transaction-document';

            echo "upload: [$doc_name], blob: [$blob_name], source_id: [$transaction_id]\n";
            Document::uploadToAzure($blob_name, $documentData, self::$transaction_id, $doc_name, false);

            $body       = ["document_name" => [$doc_name]];
            $body       = json_encode($body);
            $headers = array(
                "Content-Type: application/json",
                "Content-Length: ".strlen($body),
                "token: " . $this->seller()->token
            );
            $result = $this->initCurl($this::URL . "/accounting/$transaction_id/documents", $headers, $body, $this::POST);
            $result = json_decode($result);

            $this->assertTrue(isset($result->success) && $result->success);
        }
    }

    private function updateStatusTransaction($status) {
        $externaloffer_id   = self::$externaloffer_id;
        $valuation_id       = self::$valuation_id;
        $transaction_id     = self::$transaction_id;
        $body = [
            "status" => $status,
        ];

        $body = json_encode($body);
        $headers = [
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: ".$this->dts()->token
        ];

        $result = $this->initCurl($this::URL . "/transaction/$transaction_id/changeStatus", $headers, $body, $this::PUT);
        $result = json_decode($result);

        $this->assertTrue(isset($result->id) && $result->id == $transaction_id);
        return $result;
    }

    public function testDocumentedTransaction() {
        /*
        [2017-11-28 16:03:45] lumen.DEBUG: JN4IVZJBmZdakLbrT3s8l3cHDl8kggdxV8ouyjbg transaction/317/changeStatus [PUT] {
            "status": "27"
        }  
        [2017-11-28 16:03:45] lumen.DEBUG: JN4IVZJBmZdakLbrT3s8l3cHDl8kggdxV8ouyjbg 	--> 200 {
            "id": 317,
            "valuation_id": 3015,
            "buyer_id": 129,
            "seller_id": 128,
            "buyer_location": 100,
            "valuation_version": 2,
            "status": 27,
            "vehicle_price": 10000,
            "shipping_price": 31,
            "seller_fee": 250,
            "buyer_fee": 250,
            "inspect_time": null,
            "create_date": "2017-11-28 15:47:41",
            "update_date": "2017-11-28 16:03:45",
            "rid": "T000000317",
            "source": "external_offer",
            "source_id": 599,
            "seller_location": 99,
            "transport_no": "1511858719",
            "payment_receive": 11000,
            "seller_pay_receive": 0,
            "payment_receive_time": "2017-11-28 15:47:59"
        }
        */
        $externaloffer_id   = self::$externaloffer_id;
        $valuation_id       = self::$valuation_id;
        $transaction_id     = self::$transaction_id;

        $result = $this->updateStatusTransaction(Transaction::STATUS_DOCUMENTED);

        echo "[dts] documented transaction id: [$transaction_id]\n";
        return $result;
    }

    public function testCompleteTransaction() {
        /*
        [2017-11-28 16:06:24] lumen.DEBUG: JN4IVZJBmZdakLbrT3s8l3cHDl8kggdxV8ouyjbg transaction/317/changeStatus [PUT] {
            "status": "5"
        }  
        [2017-11-28 16:06:24] lumen.DEBUG: JN4IVZJBmZdakLbrT3s8l3cHDl8kggdxV8ouyjbg 	--> 200 {
            "id": 317,
            "valuation_id": 3015,
            "buyer_id": 129,
            "seller_id": 128,
            "buyer_location": 100,
            "valuation_version": 2,
            "status": 5,
            "vehicle_price": 10000,
            "shipping_price": 31,
            "seller_fee": 250,
            "buyer_fee": 250,
            "inspect_time": null,
            "create_date": "2017-11-28 15:47:41",
            "update_date": "2017-11-28 16:06:22",
            "rid": "T000000317",
            "source": "external_offer",
            "source_id": 599,
            "seller_location": 99,
            "transport_no": "1511858719",
            "payment_receive": 11000,
            "seller_pay_receive": 0,
            "payment_receive_time": "2017-11-28 15:47:59"
        } 
        */

        $externaloffer_id   = self::$externaloffer_id;
        $valuation_id       = self::$valuation_id;
        $transaction_id     = self::$transaction_id;

        $result = $this->updateStatusTransaction(Transaction::STATUS_COMPLETE);

        echo "[dts] complete transaction id: [$transaction_id]\n";
        return $result;
    }

    //if log-dispute

    public function testDisputeItem() {

    }

    public function testResolveDispute() {

    }
}