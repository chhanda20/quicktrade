<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use MicrosoftAzure\Storage\Common\ServicesBuilder;
use App\User;
use App\UserRole;
use App\Role;
use App\ExternalOffer;
use App\Transaction;
use App\Document;
use App\AuctionSession;
use App\SessionHasItem;
use App\Dispute;

class AzureTest extends TestCase
{
    /*-------------------------------------------
    |           TEST AZURE BLOB VALID           |
    -------------------------------------------*/

    public function testBlob() {
        $containers = [
            Document::TRANSPORT_FEE,
            Document::TRANSPORT_FEE_RETURN_VEHICLE,
            Document::FEE,
            Document::CREDIT_NOTE_COST_VARIANCE,
            Document::CREDIT_NOTE_RETURN_VEHICLE,
            Document::PAYMENT_RECEIPT,

            Document::DOCUMENT_TYPE_TRANSACTION,
            Document::DOCUMENT_TYPE_PAYMENT_RECEIPT,
            Document::DOCUMENT_TYPE_CREDIT_NOTE,
            Document::DOCUMENT_TYPE_DEBIT_NOTE,

            'inspection-item',
            'transaction-document',
            'vehicle-image',
            'user-avatar',
            'dispute-item'
        ];

        $accountKey     = env('azure_account_key');
        $accountName    = env('azure_account_name');
        echo "\nconnectiong azure: [$accountName]\n";
        $connectionString   = "DefaultEndpointsProtocol=https;AccountName=$accountName;AccountKey=$accountKey";

        $blobRestProxy      = ServicesBuilder::getInstance()->createBlobService($connectionString);

        foreach ($containers as $container) {
            try {
                echo "checking container: [$container]\n";
                $blobRestProxy->getContainerProperties($container);
                $this->assertTrue(true);
            } catch(ServiceException $e) {
                $code = $e->getCode();
                echo "error container not exist: [$container]\n";
                $this->assertTrue(false);
            }
        }
    }    
}