<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use App\UserRole;
use App\Role;

class MoTest extends TestCase
{

    const USER_IDS = [130];
    public $roles = null;

    public function testLogin()
    {
        $userId     = User::where('email', 'dinhanhhuy.it@gmail.com')->first()->id;
        $response = $this->json('POST', '/users/login', ['email' => 'dinhanhhuy.it@gmail.com', 'password' => '123123']);
        $response->assertResponseOk();
        $response->seeJson([
            'id' => $userId,
        ]);
    }

    public function testRole() {
        $user       = User::where('email', 'dinhanhhuy.it@gmail.com')->first();
        $userId     = $user->id;
        $token      = $user->token;
        $roleIds    = UserRole::where('user_id', $userId)->pluck('role_id')->toArray();
        $roleNames  = Role::whereIn('id', $roleIds)->pluck('name')->toArray();
        $header = [
            'HTTP_token' => $token
        ];

        $response   = $this->json('GET', '/users/_me/roles', [], $header);
        $response->seeJson(
            $roleNames
        );
        $response->assertResponseOk();
        $response->assertTrue(is_array($roleNames) && count($roleNames));
    }

    public function testPermission() {
        $user           = User::where('email', 'dinhanhhuy.it@gmail.com')->first();
        $userId         = $user->id;
        $token          = $user->token;
        $permissions    = DB::select("
            SELECT name from permission WHERE id in (
                SELECT id FROM permission p RIGHT JOIN user_permission up on p.id=up.permission_id WHERE up.user_id=?
            ) OR name in (
                SELECT rp.permission_id 'id' FROM role_permission rp RIGHT JOIN user_role ur on rp.role_id=ur.role_id WHERE ur.user_id=?
            )
        ", [$user->id, $user->id]);
        $permissionNames = [];
        foreach ($permissions as $permission) {
            $permissionNames[] = $permission->name;
        }

        $header = [
            'HTTP_token' => $token
        ];

        $response   = $this->json('GET', 'users/_me/permissions', [], $header);

        $response->assertResponseOk();
        $response->seeJsonEquals(
            $permissionNames
        );
        $response->assertTrue(is_array($permissionNames) && count($permissionNames));
    }
}