<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactTable extends Migration
{

    public function up()
    {
        Schema::create('contact', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 10)->nullable();
            $table->smallInteger("gender")->nullable();
            $table->string('first_name', 45);
            $table->string('last_name', 45);
            $table->mediumInteger("status_code")->nullable();
            $table->mediumInteger("privacy_flag")->nullable();
            $table->dateTime('privacy_flag_update_time')->nullable();
            $table->string('company_name', 255)->nullable();
            $table->mediumInteger("company_type")->nullable();
            $table->string('ABN', 20)->nullable();
            $table->string('ACN', 20)->nullable();
            $table->mediumInteger("tax_type")->nullable();
            $table->string('address1', 255);
            $table->string('address2', 255)->nullable();
            $table->string('suburb', 45);
            $table->string('state', 45);
            $table->string('postcode', 10)->nullable();
            $table->string('country', 45);
            $table->string('postal_address1', 255)->nullable();
            $table->string('postal_address2', 255)->nullable();
            $table->string('postal_suburb', 45)->nullable();
            $table->string('postal_state', 45)->nullable();
            $table->string('postal_postcode', 10)->nullable();
            $table->string('postal_country', 45)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('email_alternate', 255)->nullable();
            $table->string('mobile_phone', 20)->nullable();
            $table->string('home_phone', 20)->nullable();
            $table->string('work_phone', 20)->nullable();
            $table->string('fax_number', 20)->nullable();
            $table->mediumInteger('preferred_contact_method')->nullable();
            $table->string('driver_license_number', 20)->nullable();
            $table->string('dealer_license_number', 20)->nullable();
            $table->string('fleet_number', 20)->nullable();
            $table->string('bank_branch_name', 45)->nullable();
            $table->string('bank_account_name', 255)->nullable();
            $table->string('bank_bsb_number', 10)->nullable();
            $table->string('bank_account_number', 20)->nullable();
            $table->longText("note")->nullable();
            $table->unsignedBigInteger("creator_id");
            $table->unsignedBigInteger("updater_id");
            $table->bigInteger("prospect_level")->nullable();
            $table->unsignedBigInteger("company_id");
            $table->dateTime('create_date');
            $table->dateTime('update_date');
        });
        DB::statement('ALTER TABLE contact ADD FULLTEXT search(first_name, last_name, address1, address2, home_phone, work_phone, fax_number)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contact');
    }
}
