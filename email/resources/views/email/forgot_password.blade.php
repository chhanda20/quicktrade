@extends('email.layout.main')

@section('content')
    <div style="font-family: Avenir, arial, helvetica, sans-serif; font-size: 24px; font-weight: 900; line-height: 1.29;text-align: center;color: #000000;">Reset password</div>

    <div style="font-family: Avenir, arial, helvetica, sans-serif; font-size: 15px; line-height: 1.6; color: #757575;">
        <div style="margin-top: 32px;">Dear <span style="color: #132b28;">{{ $data['fullname'] }}</span>,</div>
        <div style="margin-top: 20px">You recently requested a password reset for your MotoOne account. To complete the process, click the link below.</div>
        <div style=""><a style="text-transform: uppercase; font-family: Avenir, arial, helvetica, sans-serif; font-size: 14px; font-weight: 600; text-align: center; color: #252525;  width: 144px; height: 32px; border-radius: 24px; background-color: #ffaa00; text-decoration: none; display: inline-block; line-height: 33px; margin-top: 20px;" href="{{ $data['reset_link'] }}">reset now</a></div>
        <div style="margin-top: 20px;">If you didn’t make this change or if you believe an unauthorized person has accessed your account, it is recommended that you go to MotoOne and replace your current password with a more secure one. Otherwise you can ignore this e-mail</div>
    </div>
@endsection



