<div style="max-width: 560px; width: 100%; margin: 30px auto 0 auto;">
    <div style="color: #132b28; font-family: Avenir, arial, helvetica, sans-serif; font-size: 15px; line-height: 1.6;">Moto Support</div>
</div>

<div style="width: 100%; padding:20px 0; background-color: #252525; margin-top: 88px; float: left;">
    <div style="max-width: 560px; width: 100%; margin: 0px auto;  font-size: 13px; line-height: 1.85; color: #999999; font-family: Avenir, arial, helvetica, sans-serif; ">

        <div style="max-width: 300px; width: 100%; float: left; margin-left: 10px; ">
           {{-- <div>&copy; DEALER TECHNOLOGY SOLUTIONS PTY LTD</div>
            <div style="margin-bottom: 10px;">Level 2, 58 Lorimer Street, Docklands VIC 3008</div>--}}
        </div>


        <div style="float: right; margin-right: 10px;">
            <a style="margin-right: 24px" href="#"><img src="{{ $frontend_link }}images/facebook.png" alt=""/></a>
            <a style="margin-right: 24px" href="#"><img src="{{ $frontend_link }}images/twitter.png" alt=""/></a>
            <a style="margin-right: 24px" href="#"><img src="{{ $frontend_link }}images/googleplus.png" alt=""/></a>
            <a style="" href="#"><img src="{{ $frontend_link }}images/instagram.png" alt=""/></a>
        </div>
        <div style="clear: both;"></div>
        
    </div>
    <div style="clear: both;"></div>
</div>
