@extends('email.layout.main')

@section('content')
    Dear {{ $data['fullname'] }},
    <br>
    <br>
    Welcome,
    <br>
    Generate Credit Note
    <br>
    {{ $data['attach'] }}
    <br>
    Please contact admin to login.
    <br>
    Regards
@endsection
