@extends('email.layout.main')

@section('content')
    Dear {{ $data['fullname'] }},
    <br>
    <br>
    Please find PPSR document attached with this email.
@endsection
