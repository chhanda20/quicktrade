@extends('email.layout.main')

@section('content')
    Dear {{ $data['fullname'] }},
    <br>
    <br>
    Welcome,
    <br>
    You have been add to MotoOne system as new Dealership.
    <br>
    {{ $data['attach'] }}
    <br>
    Please contact admin to login.
    <br>
    Regards
@endsection
