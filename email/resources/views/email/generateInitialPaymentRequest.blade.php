@extends('email.layout.main')

@section('content')
    Dear {{ $data['fullname'] }},
    <br>
    <br>
    Welcome,
    <br>
    Generate Initial Payment Request
    <br>
    <br>
    Please contact admin to login.
    <br>
    Regards
@endsection
