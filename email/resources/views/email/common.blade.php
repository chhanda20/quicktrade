@extends('email.layout.main')

@section('content')
    <h2>Dear {{ $data['fullname'] }}</h2>,
    <br>
    <h2>{{ $data['title'] }}</h2>
    <br>
    <h3>{{ $data['body'] }}</h3>
    <br>
    {{ $data['attach'] }}
    <br>
    Regards
@endsection