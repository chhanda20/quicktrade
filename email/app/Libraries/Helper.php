<?php

namespace App\Libraries;

use Illuminate\Hashing\BcryptHasher;

abstract class Helper
{
    /**
     * List all files in a directory
     *
     * @param $directory
     * @return array List of files
     */
    public static function getDirectoryList($directory)
    {
        $results = array();
        $handler = opendir($directory);
        while ($file = readdir($handler)) {
            if ($file != "." && $file != ".." && $file != "index.html") {
                $results[] = $file;
            }
        }
        closedir($handler);
        return $results;
    }

    /**
     * Hash a string
     *
     * @param $string
     * @return string Hashed string
     */
    public static function hash($string)
    {
        return (new BcryptHasher)->make($string);
    }

    public static function generateCharacter()
    {
        $possible = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);
        return $char;
    }


    /**
     * Generate random string with length
     *
     * @param int $stringLength
     * @return string
     */
    public static function generateGuid($stringLength = 40)
    {
        $guid = '';
        for ($i = 0; $i < $stringLength; $i++) {
            $guid .= self::generateCharacter();
        }
        return $guid;
    }
}
