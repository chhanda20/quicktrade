<?php

namespace App\Libraries;

class ResponseMessage
{
    private $data;
    private $errorCode;
    private $errorMessage;
    private $errorDetail;

    /**
     * ResponseMessage constructor.
     * @param $data
     */
    public function __construct($data = NULL)
    {
        $this->data = $data;
    }


    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    public function setErrorMessage($errorMessage, $key = '')
    {
        if ($key) {
            $this->errorMessage[$key] = $errorMessage;
        } else {
            $this->errorMessage = $errorMessage;
        }

    }

    public function setErrorDetail($errorDetail)
    {
        $this->errorDetail = $errorDetail;
    }

    public function setData($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function getMessage()
    {
        if ($this->data) {
            $result = $this->data;
        } else {
            $result['error'] = [
                'code' => $this->errorCode ? $this->errorCode : NULL,
            ];
            if ($this->errorMessage) {
                $result['error']['message'] = $this->errorMessage;
            }
            if ($this->errorDetail) {
                $result['error']['detail'] = $this->errorDetail;
            }
        }
        return $result;
    }
}
