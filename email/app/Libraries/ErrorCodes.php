<?php

namespace App\Libraries;


class ErrorCodes
{
    const INVALID = "invalid";
    const INVALID_INPUT = "invalid_input";
    const INVALID_TOKEN = "invalid_token";
    const INVALID_REQUEST = "invalid_request";

    const USER_DEACTIVATED = "user_deactivated";
    const USER_BLOCKED = "user_blocked";

    const INACTIVE = "inactive";
    const SUSPEND = "suspend";

    const INCORRECT_PASSWORD = "incorrect_password";
    const NOT_FOUND = "not_found";
    const ACCESS_DENIED = "access_denied";
    const NOT_EXISTS = "not_exists";
    const REQUIRED = "required";
    const ALPHA_NUM = "alpha_num";
    const LENGTH_MIN_INVALID = "length_min_invalid";
    const LENGTH_MAX_INVALID = "length_max_invalid";
    const NUMERIC = "numeric";

    const VALIDATE_MESSAGE_CONVERTER = [
        'required' => ErrorCodes::REQUIRED,
        'max' => ErrorCodes::LENGTH_MAX_INVALID,
        'integer' => ErrorCodes::NUMERIC
    ];
}