<?php

namespace App;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

class Helper {
    static function debug(...$logs) {
        $logger = new Logger('my_logger');
        $logger->pushHandler(new StreamHandler(__DIR__.'/my_app.log', Logger::DEBUG));
        $logger->pushHandler(new FirePHPHandler());

        $result = "";
        foreach ($logs as $log) {
            $logStr = var_export($log);
            $result = "$result $logStr";
        }
        $logger->info($result);
    }
}