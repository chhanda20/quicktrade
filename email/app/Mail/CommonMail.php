<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class CommonMail extends Mailable
{
    use Queueable, SerializesModels;

    private $data;
    private $title;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title, $data)
    {
        Log::info($data);
        
        if (is_array($data['body'])) {
            $data['body'] = 'transaction id(s): '.implode(", ", $data['body']);
        }
        $this->data  = $data;
        $this->title = $title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.common')
            ->with('data', $this->data)
            ->subject($this->title);
    }
}