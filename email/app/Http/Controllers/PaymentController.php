<?php

namespace App\Http\Controllers;

use App\Libraries\ErrorCodes;
use App\Libraries\ResponseMessage;
use App\Mail\SendEmailCreditNote;
use App\Mail\SendEmailDebitNote;
use App\Mail\SendEmailNewDealership;
use App\Mail\SendEmailNewPaymentRequest;
use App\Helper;
use App\Mail\SendEmailPaymentReceipt;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use App\Jobs\SendEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{
    public function sendPaymentRequest(Request $request) {
        $data = [
            'fullname' => $request->json()->get('fullname'),
            'attach'   => $request->json()->get('attach')
        ];
        $view = new SendEmailNewPaymentRequest($data);
        $this->sendMail($request, $view);
    }

    public function sendPaymentReceipt(Request $request) {
        $data = [
            'fullname' => $request->json()->get('fullname'),
            'attach'   => $request->json()->get('attach')
        ];
        $view = new SendEmailPaymentReceipt($data);
        $this->sendMail($request, $view);
    }

    public function sendCreditNote(Request $request) {
        $data = [
            'fullname' => $request->json()->get('fullname'),
            'attach'   => $request->json()->get('attach')
        ];
        $view = new SendEmailCreditNote($data);
        $this->sendMail($request, $view);
    }

    public function sendDebitNote(Request $request) {
        $data = [
            'fullname' => $request->json()->get('fullname'),
            'attach'   => $request->json()->get('attach')
        ];
        $view = new SendEmailDebitNote($data);
        return $this->sendMail($request, $view);
    }

    private function sendMail(Request $request, $view)
    {
        $responseMessage = new ResponseMessage();
        $v = Validator::make($request->all(), [
            'to_email' => 'required|email',
            'fullname' => 'required',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'email'    => ErrorCodes::INVALID
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            $statusCode = 400;
            $responseMessage->setErrorCode(ErrorCodes::INVALID_INPUT);
            $responseMessage->setErrorDetail($errorMessages);
        } else {
            $toEmail = $request->json()->get('to_email');
            $fullname = $request->json()->get('fullname');

            $statusCode = 200;
            dispatch(new SendEmail($toEmail, $view));
            $responseMessage->setData('success', TRUE);
        }
        $result = $responseMessage->getMessage();
        return response()->json($result, $statusCode);
    }
}
