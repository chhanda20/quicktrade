<?php

namespace App\Http\Controllers;

use App\Libraries\ErrorCodes;
use App\Libraries\ResponseMessage;
use Illuminate\Http\Request;
use App\Mail\Ppsr;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Jobs\SendEmail;

class PpsrController extends Controller
{
    public function sendEmail(Request $request)
    {
        $responseMessage = new ResponseMessage();
        $v = Validator::make($request->all(), [
            'to_email' => 'required|email',
            'fullname'     => 'required',
        ], [
            'required' => ErrorCodes::REQUIRED,
            'email'    => ErrorCodes::INVALID
        ]);
        if ($v->fails()) {
            $errorMessages = $v->errors()->getMessages();
            $statusCode = 400;
            $responseMessage->setErrorCode(ErrorCodes::INVALID_INPUT);
            $responseMessage->setErrorDetail($errorMessages);
        } else {
            $toEmail = $request->json()->get('to_email');
            $fullname = $request->json()->get('fullname');

            $body = [
                'fullname' => $fullname,
            ];
            $statusCode = 200;
            $email = new Ppsr($body);
            dispatch(new SendEmail($toEmail, $email));
            $responseMessage->setData('success', TRUE);
        }
        $result = $responseMessage->getMessage();
        return response()->json($result, $statusCode);
    }
}
