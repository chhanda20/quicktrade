<?php

namespace App\Http\Controllers;

use App\Libraries\ErrorCodes;
use App\Libraries\ResponseMessage;
use App\Mail\SendEmailPaymentReceipt;
use App\Mail\CommonMail;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Jobs\SendEmail;

class TestController extends Controller
{
    public function sendMail(Request $request) {
        $title              = $request->json()->get('title');
        $toEmail            = $request->json()->get('to_email');
        $fullname           = $request->json()->get('fullname');
        $body               = $request->json()->get('body');
        $attach             = $request->json()->get('attach');
        $attach             = $attach ? 'attach document: '.$attach : '';

        $data               = [];
        $data['fullname']   = $request->json()->get('fullname');
        $data['attach']     = $attach;
        $data['body']       = $body;
        $data['title']      = $title;

        $statusCode = 200;
//        Mail::to($toEmail)->send(new CommonMail($title, $data));
        $email = new CommonMail($title, $data);
        dispatch(new SendEmail($toEmail, $email));

        $responseMessage = new ResponseMessage();
        $responseMessage->setData('success', TRUE);
        $result = $responseMessage->getMessage();
    }

    public function test(Request $request)
    {
        var_dump($request);
        $payment = new PaymentController();
        $payment->sendCreditNote($request);
        $payment->sendDebitNote($request);
        $payment->sendPaymentRequest($request);

        $responseMessage = new ResponseMessage();
        $result = $responseMessage->getMessage();
        $responseMessage->setData('success', TRUE);
        $payment->send;

        return response()->json($result, 200);
    }

    public function test1() {
        $responseMessage = new ResponseMessage();

        $toEmail = "chhanda.rane@siliconstack.com.au";
        $fullname = "chhanda Rane";
        $body = [
            'fullname' => $fullname
        ];
        $statusCode = 200;
        Mail::to($toEmail)->send(new SendEmailPaymentReceipt($body));
        $responseMessage->setData('success', TRUE);

        $result = $responseMessage->getMessage();
        return response()->json($result, $statusCode);
    }
}