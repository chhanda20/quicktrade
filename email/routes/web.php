<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('test', 'TestController@test1');

Route::post('users/sendForgotPasswordEmail', 'ForgotPasswordController@sendForgotPasswordEmail');
Route::post('ppsr/sendEmail', 'PpsrController@sendEmail');
Route::post('company/welcome', 'CompanyController@sendWelcomeEmail');
Route::post('wishlist/notify', 'PaymentController@sendPaymentReceipt');

Route::post('transaction/paymentRequest', 'PaymentController@sendPaymentRequest');
Route::post('transaction/paymentReceipt', 'PaymentController@sendPaymentReceipt');
Route::post('transaction/creditNote', 'PaymentController@sendCreditNote');
Route::post('transaction/debitNote', 'PaymentController@sendDebitNote');

Route::post('send', 'TestController@sendMail');