<?php

namespace App\Library;

use Illuminate\Http\Request;
use Illuminate\Routing\RouteCollection;
use Illuminate\Routing\UrlGenerator;

class CustomUrlGenerator extends UrlGenerator
{
    public function __construct(RouteCollection $routes, Request $request)
    {
        parent::__construct($routes, $request);
    }

    protected function getRootUrl($scheme, $root = null)
    {
        $rootUrl = parent::getRootUrl($scheme, $root);
        $urlArr = parse_url($rootUrl);
        $scheme = $urlArr['scheme'];
        $host = $urlArr['host'];
        $urlNew = rtrim($scheme . '://' . $host, '/');
        return $urlNew;
    }
}