<?php

namespace App\Library;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

use WindowsAzure\Common\ServicesBuilder;
use MicrosoftAzure\Storage\Blob\Models\CreateContainerOptions;
use MicrosoftAzure\Storage\Blob\Models\PublicAccessType;
use MicrosoftAzure\Storage\Blob\Models\ListBlobsOptions;
use MicrosoftAzure\Storage\Common\ServiceException;

class AzureCloud
{
	function __construct() {
		$GLOBALS["connectionString"] = "DefaultEndpointsProtocol=https;AccountName=".Config::get('app.azure_storage_account_name').";AccountKey=".Config::get('app.azure_account_key');
		$GLOBALS["blobRestProxy"] = ServicesBuilder::getInstance()->createBlobService($GLOBALS["connectionString"]);
	}

	public function createContainer($container_name) {
		// Call blob REST proxy.
		//$GLOBALS["blobRestProxy"];

		// OPTIONAL: Set public access policy and metadata.
		// Create container options object.
		$createContainerOptions = new CreateContainerOptions();

		// Set public access policy. Possible values are
		// PublicAccessType::CONTAINER_AND_BLOBS and PublicAccessType::BLOBS_ONLY.
		// CONTAINER_AND_BLOBS:
		// Specifies full public read access for container and blob data.
		// proxys can enumerate blobs within the container via anonymous
		// request, but cannot enumerate containers within the storage account.
		//
		// BLOBS_ONLY:
		// Specifies public read access for blobs. Blob data within this
		// container can be read via anonymous request, but container data is not
		// available. proxys cannot enumerate blobs within the container via
		// anonymous request.
		// If this value is not specified in the request, container data is
		// private to the account owner.
		$createContainerOptions->setPublicAccess(PublicAccessType::CONTAINER_AND_BLOBS);

		// Set container metadata.
		$createContainerOptions->addMetaData("key1", "value1");
		$createContainerOptions->addMetaData("key2", "value2");

		try {
		    // Create container.
		    $GLOBALS["blobRestProxy"]->createContainer($container_name, $createContainerOptions);
		    return true;
		}
		catch(ServiceException $e) {
		    // Handle exception based on error codes and messages.
		    // Error codes and messages are here:
		    // http://msdn.microsoft.com/library/azure/dd179439.aspx
		    $code = $e->getCode();
		    $error_message = $e->getMessage();
		    Log::error($code.": ".$error_message);
		    if (409 == $code) {
		    	return false;
		    }
		}

	}

	public function uploadBlobImage($image_content, $image_name, $container = "inspection-item", $cdn_endpoint_url = null)
	{
		//UPLOAD BLOB
		try {
            //Upload blob
            $GLOBALS["blobRestProxy"]->createBlockBlob($container, $image_name, $image_content);
        }
        catch(ServiceException $e) {
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();
            Log::error($code.": ".$error_message);
            return false;
        }
        //END UPLOAD BLOB

        //GET image_link
        try {
            // List blobs.
            $blob_list = $GLOBALS["blobRestProxy"]->listBlobs($container);
            $blobs = $blob_list->getBlobs();

            foreach($blobs as $blob)
            {
                if ($blob->getName() == $image_name)
                {
                	if($cdn_endpoint_url) {
                		return $cdn_endpoint_url ."/".$container."/".$blob->getName();
                	}

                    return $blob->getUrl();
                    //echo '<img src="'.$blob->getUrl().'" />';
                }
            }
        }
        catch(ServiceException $e) {
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();
            Log::error($code.": ".$error_message);
            return false;
        }
        //END GET image_link
	}

	public function getBlobLink()
	{
		//GET image_link
        try {
            // List blobs.
            $blob_list = $GLOBALS["blobRestProxy"]->listBlobs("vehicle-image");
            $blobs = $blob_list->getBlobs();

            foreach($blobs as $blob)
            {
                echo $blob->getUrl();
            }
        }
        catch(ServiceException $e) {
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();
            Log::error($code.": ".$error_message);
            return false;
        }
        //END GET image_link
	}

    //return: array of BlobsName, return false when fail
    public function getBlobsNameByVehicleId($vehicle_id)
    {
        try {
            // List blobs.
            $blobListOptions = new ListBlobsOptions();
            $blobListOptions->setPrefix($vehicle_id);
            $blob_list = $GLOBALS["blobRestProxy"]->listBlobs("vehicle-image", $blobListOptions);
            //$blob_list = $GLOBALS["blobRestProxy"]->listBlobs("vehicle-image");
            $blobs = $blob_list->getBlobs();

            $vehicle_blobs = array();
            foreach($blobs as $blob)
            {
                //$vehicle_blobs[$blob->getName()] = $blob->getName();
                $vehicle_blobs[] = $blob->getName();
            }
            return $vehicle_blobs;
        } catch (ServiceException $e) {
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();
            Log::error($code.": ".$error_message);
            return false;
        }
    }

    //return: image path name, return false when fail
    public function uploadVehicleImage($file_input_field_name, $image_vehicle_name)
    {
        $image = $_FILES[$file_input_field_name]["tmp_name"];
        Log::debug("uploadVehicleImage ".exif_imagetype($image));
        if(exif_imagetype($image)){
            $imageFileType = "jpg";
            $image_name = $image_vehicle_name.".".$imageFileType;

            if ($image != null) {
                $fileContent = file_get_contents($image);
                $image_content = $fileContent;
                $image_name = basename($image_name);
                $image = $this->uploadBlobImage($image_content, $image_name, "vehicle-image");
                if ($image) {
                    return $image;
                } else {
                    return false;
                }
            }
        }else{
            return "error";
        }
        return false;
    }

    //return: image path name, return false when fail
    public function uploadEditedVehicleImage($file_data_url, $image_vehicle_name)
    {
        $imageFileType = "jpg";
        $image_name = $image_vehicle_name.".".$imageFileType;
        
        if ($file_data_url != null) {
            $image_content = $file_data_url;
            $image_content = explode(',', $image_content);
            $image_content = $image_content[1];
            
            // Decode to blob
            $image_content = base64_decode($image_content);
            $image_name = basename($image_name);

            $image = $this->uploadBlobImage($image_content, $image_name, "vehicle-image");
            if ($image) {
                return $image;
            } else {
                return false;
            }
        }
    }

    public function uploadDriverLicense($file_data_url, $image_driver_license_name)
    {
        //Todo
        $imageFileType = "jpg";
        $image_name = $image_driver_license_name.".".$imageFileType;
        
        if ($file_data_url != null) {
            $image_content = $file_data_url;
            $image_content = explode(',', $image_content);
            $image_content = $image_content[1];
            
            // Decode to blob
            $image_content = base64_decode($image_content);
            $image_name = basename($image_name);

            $image = $this->uploadBlobImage($image_content, $image_name, "vehicle-image");
            if ($image) {
                return $image;
            } else {
                return false;
            }
        }
    }

    public function uploadSignature($file_data_url, $image_signature_name)
    {
        $imageFileType = "jpg";
        $image_name = $image_signature_name.".".$imageFileType;

        if ($file_data_url != null) {
            $image_content = $file_data_url;
            $image_content = explode(',', $image_content);
            $image_content = $image_content[1];
            
            // Decode to blob
            $image_content = base64_decode($image_content);
            $image_name = basename($image_name);

            $image = $this->uploadBlobImage($image_content, $image_name, "user-avatar");
            if ($image) {
                return $image;
            } else {
                return false;
            }
        }
    }

    public function uploadTransactionDocument($file_input_field_name, $transaction_document_name)
    {
        $document = $_FILES[$file_input_field_name]["tmp_name"];
        if(isset($document)){
            $documentFileType = "";
            $document_name = $transaction_document_name;

            if ($document != null) {
                $fileContent = file_get_contents($document);
                $document_content = $fileContent;
                $document_name = basename($document_name);
                $document = $this->uploadBlobImage($document_content, $document_name, "transaction-document");
                if ($document) {
                    return $document;
                } else {
                    return false;
                }
            }
        }else{
            return "error";
        }
        return false;
    }

    //return: true when delete success and vice versa
    public function deleteVehicleImage($vehicle_name, $container_name = "vehicle-image")
    {
        try    {
            // Delete blob.
            $GLOBALS["blobRestProxy"]->deleteBlob($container_name, $vehicle_name);
            return true;
        }
        catch(ServiceException $e){
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();
            Log::error($code.": ".$error_message);
            return false;
        }
    }
}