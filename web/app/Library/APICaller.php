<?php

namespace App\Library;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class APICaller
{
	private function initCurl($path, $headers = null ,$body = null ,$method = "CURLOPT_POST")
    {
        //echo $method."\n".$path."<br>" ;
    //    print_r($body);
        if(!isset($headers)) {
            $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body));
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $path);

        if ($method == "CURLOPT_POST") {

            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

        } else if ($method == "CURLOPT_HTTPGET") {
            curl_setopt($ch, CURLOPT_HTTPGET, 1);

        } else if ($method == "PUT") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
            curl_setopt($ch, CURLOPT_HEADER, false);          
        }  else if ($method == "DELETE") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $server_output = curl_exec($ch);

        curl_close ($ch);
       // dd($server_output) ;
        return $server_output;
    }

    public function login($email, $password)
    {
        // Assign params to array and encode it to json format.
        $body = array("email" => $email, "password" => $password);
        $body = json_encode($body);

        $headers = array("Content-Type: application/json", 
            "Content-Length: " . strlen($body));

        $result = $this->initCurl(Config::get('app.api_url') . "/users/login", $headers, $body);
        $result = json_decode($result);
        return $result;
    }

    public function logout($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return json_decode($this->initCurl(Config::get('app.api_url') . "/users/logout", $headers, null));
    }

    public function getContacts($token, $limit, $offset, $query = null)
    {
        $body = array("limit" => $limit, "offset" => $offset, "query" => $query);
        $body = json_encode($body);
        $headers = array("Content-Type: application/json", 
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/contacts/_find", $headers, $body);
    }

    public function getLengthContacts($token, $query = null)
    {
        $body = json_encode(array("query" => $query));
        $headers = array("Content-Type: application/json", 
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/contacts/_findCount", $headers, $body);
    }

    public function getContactById($token, $id)
    {
        $body = null;
        $headers = array("Content-Type: application/json", 
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/contacts/" . $id, $headers, $body, "CURLOPT_HTTPGET");
    }

    public function addContact($token, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json", 
            "Content-Length: " . strlen($body), "token: " . $token);

        // $title = null;
        // $gender = null;
        // $first_name = null;
        // $last_name = null;
        // $privacy_flag = null;
        // $private_last_update_time = null;
        // $company_name = null;
        // $company_type = null;
        // $ABN = null;
        // $ACN = null;
        // $tax_type = null;
        // $address1 = null;
        // $address2 = null;
        // $suburb = null;
        // $state = null;
        // $postcode = null;
        // $country = null;
        // $postal_address1 = null;
        // $postal_address2 = null;
        // $postal_state = null;
        // $postal_postcode = null;
        // $postal_country = null;
        // $email = null;
        // $email_alternate = null;
        // $mobile_phone = null;
        // $home_phone = null;
        // $work_phone = null;
        // $fax_number = null;
        // $preferred_contact_method = null;
        // $driver_license_number = null;
        // $dealer_license_number = null;
        // $fleet_number = null;
        // $bank_branch_name = null;
        // $bank_account_name = null;
        // $bank_bsb_number = null;
        // $bank_account_number = null;
        // $note = null;
        // $prospect_level = null;
        
        return json_decode($this->initCurl(Config::get('app.api_url') . "/contacts", $headers, $body));
    }

    public function updateContact($token, $params, $id)
    {
        $body = json_encode($params);

        $headers = array("Content-Type: application/json", 
            "Content-Length: " . strlen($body), "token: " . $token);

        return json_decode($this->initCurl(Config::get('app.api_url') . "/contacts/" . $id, $headers, $body, "PUT"));
    }

    public function createBeginValuation($token, $params)
    {

        $body = json_encode($params);

        $headers = array("Content-Type: application/json", 
            "Content-Length: " . strlen($body), "token: " . $token);
        $url = Config::get('app.api_url') . "/valuations/_begin";

        return json_decode($this->initCurl(Config::get('app.api_url') . "/valuations/_begin", $headers, $body));
    }

    public function requestValuation($token, $valuationID, $params = null)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token,
            "Content-Length: " . strlen($body)
        );

        return json_decode($this->initCurl(Config::get('app.api_url') . "/valuations/".$valuationID."/requestValuation", $headers, $body, "PUT"));
    }

    public function getTypeCode($token, $params)
    {
        //params: type
        $body = json_encode($params);
        $headers = array("Content-Type: application/json", 
            "Content-Length: " . strlen($body), "token: " . $token);

        return json_decode($this->initCurl(Config::get('app.api_url') . "/type_codes/_find", $headers, $body));
    }

    public function _getValuations($token, $params)
    {
        /* status is 1 default for 'waiting' <=> 'requests' in frontend */
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/_find", $headers, $body);
    }

    public function _getLengthValuations($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/_findCount", $headers, $body);
    }

    public function getValuations($token, $limit, $offset, $status = 1, $query = null, $orderBy = null, $orderDesc = null)
    {
        /* status is 1 default for 'waiting' <=> 'requests' in frontend */
        $body = array("limit" => $limit, "offset" => $offset, "status" => $status, "query" => $query, "orderBy" => $orderBy, "orderDesc" => $orderDesc);
        $body = json_encode($body);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

       // dd($body);
        return $this->initCurl(Config::get('app.api_url') . "/valuations/_find", $headers, $body);
    }

    public function getLengthValuations($token, $status = 1, $query = null)
    {
        $body = array("status" => $status, "query" => $query);
        $body = json_encode($body);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/valuations/_findCount", $headers, $body);
    }

    public function searchVinNumber($token, $vin, $year)
    {
        $headers = array("Content-Type: application/json", "token: " . $token);
        $url = Config::get('app.api_url') . "/vehicles/getVehicle/".$vin."/".$year ;
        return $this->initCurl($url, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getMakes($token)
    {
        $headers = array("Content-Type: application/json", "token: " . $token);
        return json_decode($this->initCurl(Config::get('app.api_url') . "/makes/_find", $headers, null));      
    }

    public function cancelValuation($token, $valuationId)
    {
        $body = array("id" => $valuationId);
        $body = json_encode($body);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);
                $url = Config::get('app.api_url') . "/valuations/_cancel" ;
              /*  print_r($body);
                echo $url ;
                exit;*/
        return $this->initCurl($url , $headers, $body);
    }

    public function addInspectionItem($token, $params)
    {
        $body = $params;
        $body = json_encode($body);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/valuations/addInspectionItem", $headers, $body);
    }

    public function getVehicleDetail($token, $valuationId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/getVehicleDetail/".$valuationId, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getVehicleOptionalOptions($token, $vehicleId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/vehicles/".$vehicleId."/options", $headers, null, "CURLOPT_HTTPGET");
    }

    public function updateVehicleOptionalOptions($token, $vehicleId, $options)
    {
        $body = array("options"=>$options);
        $body = json_encode($body);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/vehicles/".$vehicleId ."/options", $headers, $body, "PUT");
    }

    public function getVehicleExtrasOptions($token, $vehicleId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/vehicles/".$vehicleId."/extraOptions", $headers, null, "CURLOPT_HTTPGET");
    }

    public function createVehicleExtrasOptions($token, $vehicleId, $description = '')
    {
        $body = array("description" => $description);
        $body = json_encode($body);
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/vehicles/".$vehicleId."/extraOptions", $headers, $body);
    }

    public function deleteVehicleExtrasOptions($token, $vehicleId, $optionId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/vehicles/".$vehicleId."/extraOptions/".$optionId, $headers, null, "DELETE");
    }

    public function getValuationDetail($token, $valuationId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/getValuationDetail/".$valuationId, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getValuationDetailIncludeVersion($token, $valuationId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuationId, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getInspectionItem($token, $valuationId, $sectionGroup)
    {
        //$body = $params;
        //$body = json_encode($body);
        $headers = array("Content-Type: application/json", "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/valuations/getInspectionItem/" . $valuationId . "/" . $sectionGroup, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getInspectionItemsList($token, $sectionType)
    {
        $headers = array("Content-Type: application/json", "token: " . $token);
        return $this->initCurl(Config::get('app.api_url') . "/inspectionItems/".$sectionType, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getInspectionItemDetail($token, $valuationId, $inspectionItemId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/inspectionItems/".$valuationId ."/".$inspectionItemId, $headers, null, "CURLOPT_HTTPGET");
    }

    public function updateInspectionItem($token, $valuationId, $inspectionItemId, $params)
    {

        $body = $params;
        $body = json_encode($body);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuationId ."/inspectionItem/".$inspectionItemId, $headers, $body, "PUT");
    }

    public function deleteInspectionItem($token, $valuationId, $inspectionItemId)
    {
        $headers = array("Content-Type: application/json", "token: " . $token);
        return $this->initCurl(Config::get('app.api_url') . "/valuations/" . $valuationId . "/inspectionItem/". $inspectionItemId, $headers, null, "DELETE");
    }

    public function getMechanicalDetails($token, $valuationId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuationId."/mechanicalDetails", $headers, null, "CURLOPT_HTTPGET");
    }

    public function updateMechanicalDetails($token, $valuationId, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuationId ."/mechanicalDetails", $headers, $body, "PUT");
    }

    public function getWholesaleValues($token, $valuationId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuationId."/wholesaleValues", $headers, null, "CURLOPT_HTTPGET");
    }

    public function getSalesInfo($token, $valuationId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuationId."/salesInfo", $headers, null, "CURLOPT_HTTPGET");
    }

    public function updateSalesInfo($token, $valuationId, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuationId ."/salesInfo", $headers, $body, "PUT");
    }

    public function getReconditioningCost($token, $valuationId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuationId."/reconditioningCost", $headers, null, "CURLOPT_HTTPGET");
    }

    public function updateReconditioningCost($token, $valuationId, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuationId ."/reconditioningCost", $headers, $body, "PUT");
    }

    public function trade($token, $id, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/valuations/". $id ."/trade", $headers, $body, "PUT");

    }

    public function reEvaluate($token, $id, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/valuations/". $id ."/reprice", $headers, $body, "PUT");
    }

    public function tradeIn($token, $valuation_id, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);
        
        return $this->initCurl(Config::get('app.api_url') . "/valuations/" . $valuation_id . "/tradein", $headers, $body, "PUT");
    }

    public function updateWholesaleValues($token, $valuationId, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuationId ."/wholesaleValues", $headers, $body, "PUT");
    }

    public function updateValuations($token, $valuationId, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);
                return $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuationId, $headers, $body, "PUT");
    }

    public function updateOverallRating($token, $valuationId, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuationId."/overallRating", $headers, $body, "PUT");
    }

    public function approveRevaluation($token, $valuation_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/" . $valuation_id . "/approveReprice", $headers, null, "PUT");
    }

    public function rejectRevaluation($token, $valuation_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/" . $valuation_id . "/rejectReprice", $headers, null, "PUT");
    }

    public function approveValuation($token, $valuation_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/" . $valuation_id . "/approve", $headers, null, "PUT");
    }

    public function resetPassword($email)
    {
        $body = array("email" => $email);
        $body = json_encode($body);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body));
       // echo Config::get('app.api_url') . "/users/resetPassword" ;
        // Call API to request forgot password
        $result = $this->initCurl(Config::get('app.api_url') . "/users/resetPassword", $headers, $body);
        $result = json_decode($result);

        return $result;
    }

    public function updatePassword($password_token, $password)
    {
        $body = array("password_token" => $password_token, "password" => $password);
        $body = json_encode($body);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body));

        // Call API to request forgot password
        $result = $this->initCurl(Config::get('app.api_url') . "/users/updatePassword", $headers, $body);
        $result = json_decode($result);

        return $result;
    }

    public function getChannelByContactId($token, $contact_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/users/getChannelByContactId/" . $contact_id, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getCountry($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return json_decode($this->initCurl(Config::get('app.api_url') . "/country", $headers, null, "CURLOPT_HTTPGET"));
    }

    public function getCountryByCode($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        $country = json_decode($this->initCurl(Config::get("app.api_url")."/country/getByCode", $headers, $body));
        return $country;
    }

    public function deleteValuation($token, $valuation_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/" . $valuation_id, $headers, null, "DELETE");
    }

    public function reconditioningCost($token, $valuation_id, $general_cost)
    {
        $body = array("generalCost" => $general_cost);
        $body = json_encode($body);
        
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/valuations/updateGeneralReconditionCost/" . $valuation_id, $headers, $body, "PUT");
    }

    public function getGeneralReconditionCost($token, $valuation_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return json_decode($this->initCurl(Config::get('app.api_url') . "/valuations/getGeneralReconditionCost/".$valuation_id, $headers, null, "CURLOPT_HTTPGET"));
    }

    public function getRoles($token){
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return json_decode($this->initCurl(Config::get('app.api_url') . "/users/_me/roles", $headers, null, "CURLOPT_HTTPGET"));
    }

    public function getPermissions($token){
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return json_decode($this->initCurl(Config::get('app.api_url') . "/users/_me/permissions", $headers, null, "CURLOPT_HTTPGET"));
    }

    public function getValuationById($token, $valuationId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuationId, $headers, null, "CURLOPT_HTTPGET");
    }

    /*public function getModelList($token, $make)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/glassModel/getFamily/".$make, $headers, null, "CURLOPT_HTTPGET");
    }*/

    public function checkPPSR($token, $vin, $year)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/ppsr", $headers, null);
    }

    public function updateVehicle($token, $valuationId, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuationId."/updateVehicle", $headers, $body, "PUT");
    }

    /*public function getVariantList($token, $make, $family)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/glassModel/getVariant/".$make."/".$family, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getReleaseYear($token, $make, $family, $variant = null)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        $api_url = "/glassModel/getReleaseYear/".$make."/".$family;
        if($variant){
            $api_url .= "/".$variant;
        }

        return $this->initCurl(Config::get('app.api_url') . $api_url, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getReleaseMonth($token, $make, $family, $releaseYear, $variant = null)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        $api_url = "/glassModel/getReleaseMonth/".$make."/".$family."/".$releaseYear;
        if($variant){
            $api_url .= "/".$variant;
        }

        return $this->initCurl(Config::get('app.api_url') . $api_url, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getModelDetail($token, $valuationId, $make, $family, $releaseYear, $releaseMonth, $variant = null)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        $api_url = "/glassModel/getModelDetail/".$valuationId."/".$make."/".$family."/".$releaseYear."/".$releaseMonth;
        if($variant){
            $api_url .= "/".$variant;
        }

        return $this->initCurl(Config::get('app.api_url') . $api_url, $headers, null, "CURLOPT_HTTPGET");
    }*/

    public function completeValuation($token, $valuation_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        $result = $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuation_id."/complete", $headers, null, "PUT");
        return $result;
    }

    public function acceptValuation($token, $valuation_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );
        $result = $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuation_id."/_accept", $headers, null);
        return $result;
    }

    public function getValuationsByCustomerId($token, $customerId, $offset, $limit)
    {
        $body = json_encode(array(
            "customer_id" => $customerId,
            "offset" => $offset,
            "limit" => $limit
        ));
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/_findByCustomerId", $headers, $body);
    }

    public function getLengthValuationsByCustomer($token, $customerId)
    {
        $body = json_encode(array("customer_id" => $customerId));
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/valuations/_countByCustomerId", $headers, $body);
    }

    public function getGlassesData($token, $selectedField, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/glasses/code/".$selectedField, $headers, $body);
    }

    public function getGlassesModelDetail($token, $glassCode)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/glasses/modelDetail/".$glassCode, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getGlassesOptionsList($token, $glassCode)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/glasses/optionList/".$glassCode, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getUserInfo($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/contacts/_me", $headers, null, "CURLOPT_HTTPGET");
    }

    /** DEALERSHIP API **/
    public function getCompanies($token, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/company/_find", $headers, $body);
    }

    public function loginForMOAdmin($email, $password)
    {
        // Assign params to array and encode it to json format.
        $body = array("email" => $email, "password" => $password);
        $body = json_encode($body);

        $headers = array("Content-Type: application/json", 
            "Content-Length: " . strlen($body));

        $result = $this->initCurl(Config::get('app.api_url') . "/admin/login", $headers, $body);
        $result = json_decode($result);
        
        return $result;
    }

    public function listCompanyGroups($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/companyGroups", $headers, null, "CURLOPT_HTTPGET");
    }

    public function createCompany($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/company", $headers, $body);
    }

    public function createAdmin($token, $params, $companyId)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/company/".$companyId."/setAdmin", $headers, $body);
    }

    public function getCompanyById($token, $companyId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/company/".$companyId, $headers, null, "CURLOPT_HTTPGET");
    }

    public function updateCompany($token, $params, $companyId)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        $result = $this->initCurl(Config::get('app.api_url') . "/company/".$companyId, $headers, $body, "PUT");
        return $result;
    }

    public function getLengthOfCompany($token, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/company/_findCount", $headers, $body);
    }

    public function fillterCompany($token, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/company/_findCount", $headers, $body);
    }

    public function suspendCompany($token, $status_code, $company_id)
    {
        $body = array("status_code" => $status_code);
        $body = json_encode($body);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/company/".$company_id, $headers, $body, "PUT");
    }

    public function getLocations($token, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/location/_find", $headers, $body);
    }

    public function getLocationByCurrentLoggedUser($token)
    {
        $headers = array("Content-Type: application/json",
                         "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/location", $headers, null, "CURLOPT_HTTPGET");
    }

    public function getLocationById($token, $location_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/location/".$location_id, $headers, null, "CURLOPT_HTTPGET");
    }

    public function createLocation($token, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/location", $headers, $body);
    }

    public function updateLocation($token, $location_id, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/location/".$location_id, $headers, $body, "PUT");
    }
    public function getLengthLocation($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/location/_findCount", $headers, $body);
    }

    public function getAllLocations($token, $params = [])
    {
        $body = NULL;
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );
        if ($params) {
            $body = json_encode($params);
            $headers[] = "Content-Length: " . strlen($body);
        }

        return $this->initCurl(Config::get('app.api_url') . "/location/all", $headers, $body);
    }

    /** END DEALERSHIP API **/

    /** DEALER GROUP API **/
    public function getCompanyGroups($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/companyGroup/_find", $headers, $body);
    }

    public function getLengthOfCompanyGroups($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/companyGroup/_findCount", $headers, $body);
    }

    public function createCompanyGroup($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body), "token: ".$token);

        return $this->initCurl(Config::get('app.api_url') . "/companyGroup", $headers, $body);
    }

    public function getCompanyGroupById($token, $company_group_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/companyGroup/" . $company_group_id, $headers, null, "CURLOPT_HTTPGET");

    }

    public function updateCompanyGroup($token, $params, $company_group_id)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/companyGroup/". $company_group_id, $headers, $body, "PUT");
    }
    /** END DEALER GROUP API **/


    /** SETTING API **/
    public function getSettingExternalOffer($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/appProperty/externalOffer", $headers, null, "CURLOPT_HTTPGET");
    }

    public function saveSettingExternalOffer($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/appProperty/externalOffer", $headers, $body, "PUT");
    }

    public function getValuationSettings($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/appProperty/valuationTimeout", $headers, null, "CURLOPT_HTTPGET");
    }

    public function saveValuationSettings($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/appProperty/valuationTimeout", $headers, $body, "PUT");
    }

    public function getRatingSettings($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/appProperty/speedySetting", $headers, null, "CURLOPT_HTTPGET");
    }

    public function saveRatingSettings($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/appProperty/updateSpeedySetting", $headers, $body, "PUT");
    }
    /** END SETTING API **/

    /** DEALERSHIP SETTING API **/
    public function getDealershipSettings($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/appProperty/optionSetting", $headers, null, "CURLOPT_HTTPGET");
    }

    public function saveDealershipSettings($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/appProperty/optionSetting", $headers, $body, "PUT");
    }

    public function getRegionsList($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/regions", $headers, null, "CURLOPT_HTTPGET");
    }

    public function getTransactionFees($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/appProperty/transactionFees", $headers, null, "CURLOPT_HTTPGET");
    }

    //params: buyer_fee, seller_fee, transport_prices
    public function setTransactionFees($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/appProperty/transactionFees", $headers, $body, "PUT");
    }
    /** END DEALERSHIP SETTING API **/

    public function searchContacts($token, $query, $limit, $offset, $contact_not_link_user)
    {
        $params = array("query" => $query, "limit" => $limit, "offset" => $offset, "contactNotLinkUser" => $contact_not_link_user);
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/contacts/_find", $headers, $body);
    }

    public function searchCompanyContacts($token, $query, $limit, $offset)
    {
        $params = array(
            "query"              => $query,
            "limit"              => $limit,
            "offset"             => $offset,
            "isCompanyContact" => TRUE,
            "contactNotLinkUser" => TRUE
        );
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        $apiUrl = Config::get("app.api_url") . "/contacts/admin/_find";
        return $this->initCurl($apiUrl, $headers, $body);
    }

    public function getListOfRole($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/role", $headers, null, "CURLOPT_HTTPGET");
    }

    public function getListOfRoleByCurrentLoggedUser($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token,
            "Content-Length: ".strlen($body)
        );
        return $this->initCurl(Config::get("app.api_url") . "/role/search", $headers);
    }

    public function getRolesByCompanyGroupId($token, $company_group_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/companyGroup/".$company_group_id."/roles", $headers, null, "CURLOPT_HTTPGET");
    }

    /*
     * Getting roles of company group by Id
     */
    public function getRolesCompanyGroup($token, $company_group_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/companyGroup/$company_group_id/roles", $headers, null, "CURLOPT_HTTPGET");
    }
    
	/** USER API **/
    public function getUsers($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/users/search", $headers, $body);
    }

    /*
     * Search and get List All User not forbidden by company group
     */

    public function usersAdminSearch($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/users/admin/search", $headers, $body);
    }

    public function getUserById($token, $user_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/users/".$user_id, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getUserPermission($token, $userId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/permission/user/$userId", $headers, null, "CURLOPT_HTTPGET");
    }

    public function getLengthOfUsersByRoleId($token, $params, $roleId)
    {
        $params = array_merge($params, array("roleId"=>$roleId));
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/users/search", $headers, $body);
    }

    public function getUsersByRoleId($token, $params, $roleId)
    {
        $params = array_merge($params, array("roleId"=>$roleId));
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/users/admin/search", $headers, $body);
    }

    public function createUser($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/users", $headers, $body);
    }

    public function updateUser($token, $params, $user_id)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/users/".$user_id, $headers, $body, "PUT");
    }

    public function updateStatus($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/users/status", $headers, $body);
    }
    /** end USER API **/

    /*************** START NEW ROLE API ***************/

    public function getPermission($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/permission", $headers, null, "CURLOPT_HTTPGET");
    }

    public function createNewRole($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body), "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/role", $headers, $body);
    }

    public function getRoleById($token, $role_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/role/" . $role_id, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getRoleByIdOfPermission($token, $role_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/role/".$role_id."/permissions", $headers, null, "CURLOPT_HTTPGET");
    }

    public function updateNewRole($token, $params, $role_id)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/role/" . $role_id, $headers, $body, "PUT");
    }

    public function deleteRole($token, $role_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/role/".$role_id, $headers, null, "DELETE");
    }

    public function findRolesList($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/role/search", $headers, $body);
    }

    /*************** END NEW ROLE API ***************/

    /*************** START EXTERNAL OFFER GROUPS API ***************/
    public function getExternalGroups($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/externalOffer/search", $headers, $body);
    }

    public function getDealerLocationsList($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/dealer/search", $headers, $body);
    }

    public function createExternalOfferGroup($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body), "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/externalGroup", $headers, $body);
    }

    public function updateExternalOfferGroup($token, $params, $external_id)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/externalGroup/" . $external_id, $headers, $body, "PUT");
    }

    public function deleteExternalOfferGroupById($token, $external_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/externalGroup/" . $external_id, $headers, null, "DELETE");
    }

    public function getExternalOfferGroupById($token, $external_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/externalGroup/" . $external_id, $headers, null, "CURLOPT_HTTPGET");
    }
    /*************** END EXTERNAL OFFER GROUPS API ***************/

     /*************** EXTERNAL OFFERING **************/
    public function getTransMissionType($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/glasses/transmissionType", $headers, null, "CURLOPT_HTTPGET");
    }

    public function setLocationForExternalOffer($token, $params)
    {
        //params: externalOfferId, locationId
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/externalOffer/setLocation", $headers, $body);
    } 

    public function bidPriceForExternalOffer($token, $params)
    {
        //params: externalOfferId, price, expiryDate
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/externalOffer/bid", $headers, $body);
    }

    public function sendExternalOffer($token, $params)
    {
        //params: arrivalDate, valuationId, group
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/externalOffer", $headers, $body);
    }

    public function getExternalOffer($token, $valuation_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/externalOffer/" . $valuation_id, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getExternalGroupList($token, $params)
    {

    }

    public function acceptBestOffer($token, $params)
    {
        //params: externalOfferId
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/externalOffer/accept", $headers, $body);
    }

    public function rejectBestOffer($token, $params)
    {
        //params: externalOfferId
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/externalOffer/reject", $headers, $body);
    }

    public function blockOffer($token, $params)
    {
        //params: vehicleId
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/externalOffer/blockVehicle", $headers, $body);
    }

    public function getIncomingExternalOffer($token, $params)
    {
        //params: status, limit, offset, orderDesc
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/externalOffer/inComing", $headers, $body);
    }
    public function getExternalOfferWon($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/externalOffer/offerWon", $headers, $body);
    }
    public function getOutgoingExternalOffer($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/externalOffer/outgoing", $headers, $body);
    }

    public function getPurchaseHistory($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/externalOffer/purchaseHistory", $headers, $body);
    }

    public function getSoldHistory($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/externalOffer/soldHistory", $headers, $body);
    }

    public function getGroupOfExternalOffer($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/externalOffer/group", $headers, null, "CURLOPT_HTTPGET");
    }
    /*************** END EXTERNAL OFFERING **************/


    /*************** START PROFILE SETTING **************/

    public function getProfileSetting($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/users/_me/config", $headers, null, "CURLOPT_HTTPGET");
    }

    public function updateProfileConfig($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ". $token
        );
        return $this->initCurl(Config::get("app.api_url")."/users/_me/config", $headers, $body, "PUT");
    }

    public function updateProfileQuickCode($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ". $token
        );
        return $this->initCurl(Config::get("app.api_url")."/users/quickCode", $headers, $body, "PUT");
    }

    public function updateProfilePassword($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ". $token
        );
        return $this->initCurl(Config::get("app.api_url")."/users/_me/updatePassword", $headers, $body, "PUT");
    }

    public function getUserEvent($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url"). "/users/event", $headers, NULL, "CURLOPT_HTTPGET");
    }

    public function updateUserEvent($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url"). "/users/updateEvent", $headers, $body, "PUT");
    }
    /*************** END PROFILE SETTING **************/


    /*************** START PERMISSION **************/
    public function updatePermission($token, $params, $id)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/permission/".$id, $headers, $body, "PUT");
    }

    /*************** END PERMISSION **************/

    /*************** START AUCTION **************/
    public function getVehicleStockList($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/vehicleStock/_find", $headers, $body);
    }

    public function getAuctionList($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/auctions/search", $headers, $body);
    }

    public function getSetting($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/appProperty/getSetting", $headers, $body);
    }

    public function setWatching($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/auctions/watchList", $headers, $body);
    }

    public function unsetWatching($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/auctions/watchList", $headers, $body, 'DELETE');
    }

    public function setRecentlyViewed($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/auctions/recentView", $headers, $body);
    }

    public function buyNow($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/auctions/trading/buyNow", $headers, $body);
    }

    public function saveMaxBidPrice($token, $params, $sessionId, $itemId)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/auctions/session/$sessionId/items/$itemId/userSetting", $headers, $body, 'PUT');
    }

    public function auctionById($token, $auctionId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        $result = $this->initCurl(Config::get('app.api_url') . "/auctions/$auctionId", $headers, NULl, 'CURLOPT_HTTPGET');
        return $result;
    }

    /*************** END AUCTION **************/

    /*************** START VALUATION VERSION **************/
    public function getValuationVersionById($token, $valuationId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/valuations/".$valuationId."/version", $headers, null, "CURLOPT_HTTPGET");
    }

    public function createValuationVersionId($token, $params, $valuation_id)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
      /*  echo "<pre>";
       echo  $url = Config::get("app.api_url")."/valuations/".$valuation_id."/version";
        echo $body ;
        dd($headers);*/
          //  dd($this->initCurl(Config::get("app.api_url")."/valuations/".$valuation_id."/version", $headers, $body));
        return $this->initCurl(Config::get("app.api_url")."/valuations/".$valuation_id."/version", $headers, $body);
    }

    public function releaseValuationVersionId($token, $valuationId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
         Config::get("app.api_url")."/valuations/".$valuationId."/releaseVersion" ;
      //  exit;
        //dd($this->initCurl(Config::get("app.api_url")."/valuations/".$valuationId."/releaseVersion", $headers, null));
        return $this->initCurl(Config::get("app.api_url")."/valuations/".$valuationId."/releaseVersion", $headers, null);
    }

    public function cancelValuationVersionId($token, $params, $valuationId)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/valuations/".$valuationId."/cancelModifyingVersion", $headers, $body);
    }

    /*************** END VALUATION VERSION **************/


    /*************** START AUCTION SESSION **************/
    public function getAllAuctionSession($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/session/list", $headers, $body);
    }

    public function getAuctionSessionById($token, $sessionId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/session/".$sessionId, $headers, null, "CURLOPT_HTTPGET");
    }

    public function createAuctionSessionTrading($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/session", $headers, $body);
    }


    public function updateAuctionSessionTrading($token, $params, $sessionId)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/session/".$sessionId, $headers, $body, "PUT");
    }

    /*************** START REQUEST LIST *****************/
    public function getRequestList($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/requestList", $headers, $body);
    }

    public function approveOrRejectRequestList($token, $params)
    {
        //params: token, request_id, action = "approve" or "reject", comment, session_id
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/request/approval", $headers, $body);
    }
    /*************** END REQUEST LIST ******************/

    /*************** START LIVE AUCTION ****************/
    public function getActiveAuctionSessions($token)
    {
        //params:
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/active", $headers, null, "CURLOPT_HTTPGET");
    }

    public function findVehiclesInAuctionSession($token, $session_id, $params)
    {
        //params:
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/session/".$session_id."/_findVehicle", $headers, $body);
    }

    public function bidPrice($token, $session_id, $item_id, $params)
    {
        //params: bid_price
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/session/".$session_id."/item/".$item_id."/bid", $headers, $body);
    }

    public function getMaxBidPrice($token, $session_id, $item_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/session/".$session_id."/items/".$item_id."/userSetting", $headers, null, "CURLOPT_HTTPGET");
    }

    public function getSystemProperty()
    {
        $headers = array(
            "Content-Type: application/json"
        );
        return $this->initCurl(Config::get("app.api_url")."/appProperty", $headers, null, "CURLOPT_HTTPGET");
    }

    public function getNextAuctionSession($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/nextSession", $headers, null, "CURLOPT_HTTPGET");
    }
    /*************** END LIVE AUCTION ******************/

    /******************* TRADING SETTING *******************/
    public function getAuctionSettings($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/settings", $headers, null, "CURLOPT_HTTPGET");
    }

    public function updateAuctionSettings($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/settings", $headers, $body, "PUT");
    }

    /*************** start SHOWROOM VEHICLE DETAILS *****************/
    public function getVehicleDetailsById($token, $vehicleId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/vehicles/".$vehicleId, $headers, null, "CURLOPT_HTTPGET");
    }
    /*************** end SHOWROOM VEHICLE DETAILS *****************/

    public function sendToAuction($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/sendToAuction", $headers, $body);
    }

    public function resendAuction($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/resend", $headers, $body);
    }


	/*************** start INTERACTION *****************/
    public function getInteractionById($token, $contact_id, $id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/contacts/".$contact_id."/interaction/".$id, $headers, null, "CURLOPT_HTTPGET");   
    }

    public function getAnInteractionById($token, $params, $contact_id)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/contacts/".$contact_id."/interaction/_find", $headers, $body);
    }


    public function createInteractionById($token, $params, $contact_id)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/contacts/".$contact_id."/interaction", $headers, $body);
    }

    public function updateInteractionById($token, $params, $contact_id, $id)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/contacts/".$contact_id."/interaction/".$id, $headers, $body, "PUT");
    }

    public function deleteInteractionById($token, $contact_id, $id)
    {   
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/contacts/".$contact_id."/interaction/".$id, $headers, null, "DELETE");
    }

    public function countInteraction($token, $contact_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/contacts/".$contact_id."/interaction/_findCount", $headers, null);
    }

	public function getAuctionInfoById($token, $auctionId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/".$auctionId, $headers, null, "CURLOPT_HTTPGET");
    }

    public function cancelAuctionRequest($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/auctions/request/cancel", $headers, $body);
    }

    public function addUserInAdmin($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/admin/createUser", $headers, $body);
    }

    public function updateUserInAdmin($token, $userId, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/admin/users/$userId", $headers, $body, "PUT");
    }


    /*************** start WISHLIST *****************/

    public function createWishList($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/wishLists", $headers, $body);
    }

    public function updateWishList($token, $params, $id)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/wishLists/".$id, $headers, $body, "PUT");
    }

    public function searchWishList($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/wishLists/search", $headers, $body);
    }

    public function getWishListById($token, $id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/wishLists/".$id, $headers, null, "CURLOPT_HTTPGET");
    }


    /************** start - DISPUTE ***************/
    public function getDisputeItems($token, $transactionId, $disputeId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/transaction/".$transactionId."/dispute/".$disputeId."/items", $headers, null, "CURLOPT_HTTPGET");
    }

    //params: comment(string), inspectionItems(object[]), disputeItems(object[]) 
    public function createDispute($token, $transactionId, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/transaction/".$transactionId."/dispute", $headers, $body);
    }

    public function getDisputeById($token, $disputeId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/dispute/".$disputeId, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getDisputeNotes($token, $disputeId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/dispute/".$disputeId."/note", $headers, null, "CURLOPT_HTTPGET");
    }

    public function createDisputeNote($token, $disputeId, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/dispute/".$disputeId."/note", $headers, $body);
    }

    public function deleteDisputeNote($token, $disputeId, $noteId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/dispute/".$disputeId."/note/".$noteId, $headers, null, "DELETE");
    }

    public function resolveDispute($token, $disputeId, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/dispute/".$disputeId."/resolve", $headers, $body, "PUT");
    }

    public function underReviewDispute($token, $disputeId, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/dispute/".$disputeId."/underReview", $headers, $body, "PUT");
    }

    public function getTransactionById($token, $transactionId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/transaction/".$transactionId, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getDealershipDisputeList($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/dispute/_findCompanyDispute", $headers, $body);
    }
    
	public function findDispute($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/dispute/_find", $headers, $body);
    }

    public function acceptDispute($token, $id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );
        return json_decode($this->initCurl(Config::get('app.api_url') . "/dispute/$id/accept", $headers, NULL, "PUT"));
    }
    /************** end - DISPUTE ***************/

    /************** start - ARRIVING VEHICLE ***************/
    public function getTransactions($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/transaction/_find", $headers, $body);
    }

    public function getTransactionCount($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/transaction/_findCount", $headers, $body);
    }

    public function getMOTransactions($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/transaction/admin/_find", $headers, $body);
    }

    public function getMOTransactionCount($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/transaction/admin/_findCount", $headers, $body);
    }

    public function getInspectionItemsByValuationId($token, $valuationId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/getInspectionItem/" . $valuationId , $headers, null, "CURLOPT_HTTPGET");
    }

    public function getInspectionItemsByValuationVersion($token, $valuationId, $valuationVersion)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return $this->initCurl(Config::get('app.api_url') . "/valuations/" . $valuationId . "/version/" . $valuationVersion . "/inspectionItems", $headers, null, "CURLOPT_HTTPGET");
    }

    public function valuationByVersion($token, $valuationId, $valuationVersion)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/valuations/" . $valuationId . "/version/" . $valuationVersion, $headers, null, "CURLOPT_HTTPGET");
    }
    /************** end - ARRIVING VEHICLE ***************/

	public function getLocationAlls($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/location/all", $headers, $body);
    }

    public function acceptTransaction($token, $transactionId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/transaction/".$transactionId."/accept", $headers, null, "PUT");
    }


    /************** start - TEST DRIVE ***************/
    public function searchTestDriveWithCurrentUser($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/testDrives/search", $headers, $body);
    }

    public function getTestDriveListByCustomerId($token, $params) {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/testDrives/search", $headers, $body);
    }

    public function getSearchVehicle($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/testDrives/vehicle/_find", $headers, $body);
    }

    public function findCountVehicle($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/testDrives/vehicle/_findCount", $headers, $body);
    }

    public function createTestDrivesSession($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/testDrives/create", $headers, $body);
    }

    public function completeTestDriveSession($token, $params, $testDriveId)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/testDrives/complete/".$testDriveId, $headers, $body, "PUT");
    }

    public function searchCountTestDrive($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/testDrives/searchCount", $headers, $body);
    }

    public function getTestDriveById($token, $testDriveId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/testDrives/".$testDriveId, $headers, null, "CURLOPT_HTTPGET");
    }

    public function getDataAccess($token, $userId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/dataAccess/$userId", $headers, null, "CURLOPT_HTTPGET");
    }

    public function permissionByGroup($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/permissionByGroup", $headers, NULL, "CURLOPT_HTTPGET");
    }


    /******* start - TRANSACTION *******/
    public function transactionPaymentReceived($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );

        return json_decode($this->initCurl(Config::get('app.api_url') . "/transaction/_paymentReceived", $headers, $body, "PUT"));
    }

    public function transactionReadyForTransport($token, $transactionId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return json_decode($this->initCurl(Config::get('app.api_url') . "/transaction/".$transactionId."/readyForTransport", $headers, null, "PUT"));
    }

    public function transactionTransportBooked($token, $transactionId, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return json_decode($this->initCurl(Config::get('app.api_url') . "/transaction/".$transactionId."/transportBooked", $headers, $body, "PUT"));
    }

    public function transactionInTransit($token, $transactionId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return json_decode($this->initCurl(Config::get('app.api_url') . "/transaction/".$transactionId."/inTransit", $headers, null, "PUT"));
    }

    public function transactionShipped($token, $transactionId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return json_decode($this->initCurl(Config::get('app.api_url') . "/transaction/".$transactionId."/shipped", $headers, null, "PUT"));
    }

    public function transactionReturning($token, $transactionId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return json_decode($this->initCurl(Config::get('app.api_url') . "/transaction/".$transactionId."/setStatusReturning", $headers, null, "PUT"));
    }

    public function transactionDocumenting($token, $transactionId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return json_decode($this->initCurl(Config::get('app.api_url') . "/transaction/".$transactionId."/setStatusDocumenting", $headers, null, "PUT"));
    }

    public function transactionInspect($token, $transactionId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );

        return json_decode($this->initCurl(Config::get('app.api_url') . "/transaction/".$transactionId."/inspect", $headers, null, "PUT"));
    }

    public function completeTransaction($token, $transactionId, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: " . $token
        );

        return json_decode($this->initCurl(Config::get('app.api_url') . "/transaction/".$transactionId."/changeStatus", $headers, $body, "PUT"));
    }

    public function saveTransactionDocuments($token, $transactionId, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/accounting/".$transactionId."/documents", $headers, $body);
    }

    public function getTransactionDocuments($token, $transactionId)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/accounting/".$transactionId."/documents", $headers, NULL, "CURLOPT_HTTPGET");
    }
    /******* end - TRANSACTION *******/


    /******* start - DASHBOARD *******/
    public function getCustomizeDashBoard($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/dashboard", $headers, NULL, "CURLOPT_HTTPGET");
    }

    public function getDashBoard($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get('app.api_url') . "/dashboard", $headers, $body);
    }


    /******* start - MENU ITEMS ROLE *******/
    public function getMenuItems($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/menu", $headers, NULL, "CURLOPT_HTTPGET");
    }

    public function getMenuByRoleId($token, $role_id)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/role/".$role_id."/menu", $headers, NULL, "CURLOPT_HTTPGET");
    }

    public function setMenuAccessForRole($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/menu/setRole", $headers, $body);
    }

    public function manualVehicleCreate($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url"). "/manualVehicle/create", $headers, $body);
    }

    public function getTermAndCondition($token)
    {
        $headers = array(
            "Content-Type: Application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url"). "/company/getTermAndCondition", $headers, NULL, "CURLOPT_HTTPGET");
    }

    public function updateTermAndCondition($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: Application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url"). "/company/updateTermAndCondition", $headers, $body);
    }

    public function userAcceptTermCondition($token)
    {
        $headers = array(
            "Content-Type: Application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url"). "/users/acceptTermAndCondition", $headers, NULL);
    }

   /* public function getTestDrives($token)
    {
        $headers = array(
            "Content-Type: application/json",
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url") . "/testDrives", $headers, NULL, "CURLOPT_HTTPGET");
    }*/

    /******************* APP PROPERTY *******************/
    public function getAppProperty($token,$params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: Application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/appProperty/get", $headers, $body);
    }

    public function setAppProperty($token, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        return $this->initCurl(Config::get("app.api_url")."/appProperty/update", $headers, $body, "PUT");
    }

	public function auctionsSearchItem($token, $auctionId)
    {
        $headers = array(
            "Content-Type: Application/json",
            "token: ".$token
        );
        $url = Config::get("app.api_url"). "/auctions/searchItem/$auctionId";
        return $this->initCurl($url, $headers, NULL, "CURLOPT_HTTPGET");
    }

    public function updateOfferPrice($token, $valuationId, $params)
    {
        $body = json_encode($params);
        $headers = array("Content-Type: application/json",
            "Content-Length: " . strlen($body), "token: " . $token);

        return $this->initCurl(Config::get('app.api_url') . "/valuations/".$valuationId ."/offerPrice", $headers, $body, "PUT");
	}
	
    public function auctionSessionHistory($token, $sessionId, $params)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: Application/json",
            "Content-Length: ".strlen($body),
            "token: ".$token
        );
        $url = Config::get("app.api_url"). "/auctions/session/$sessionId/history";
        return $this->initCurl($url, $headers, $body);
    }

    public function getDocumentOfTransaction($token, $params, $id)
    {
        $body = json_encode($params);
        $headers = array(
            "Content-Type: Application/json",
            "Content-Length: " . strlen($body),
            "token: " . $token
        );
        return $this->initCurl(Config::get("app.api_url")."/transaction/".$id."/documents", $headers, $body);
    }

 	public function notifications($token)
    {
        $headers = array(
            "Content-Type: Application/json",
            "token: ".$token
        );
        $url = Config::get("app.api_url"). "/notifications";
        return $this->initCurl($url, $headers);
    }
}

