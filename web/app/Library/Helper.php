<?php

namespace App\Library;

use Illuminate\Support\Facades\Log;

abstract class Helper
{
    public static function arrayCheck($array = array())
    {
        /***************** USING FOR ARRAY CHECKING *****************
         *
         * return true if this is array and no empty elements found
         * return false if failed
         *
         *************************************************************/
        if (is_array($array) && !in_array("", $array)) {
            return true;
        }
        return false;
    }

    public static function getTypeName($objects, $code)
    {
        /******** USING FOR GET TYPE NAME ********
         *
         * receive 2 params:
         * 1. object after query get type code
         * 2. type code
         *
         *****************************************/
        $value = $code;
        if(!empty($code)){
            foreach($objects as $obj){
                if ($code === $obj->code) {
                    $value = $obj->value;
                }
            }
        }
        return $value;
    }

    public static function explodeX( $delimiters, $string )
    {
        /**** USING WHEN YOU WANT TO ADD MORE DELIMITERS, UPGRADE OF EXPLODE ****/
        return explode( chr( 1 ), str_replace( $delimiters, chr( 1 ), $string ) );
    }

    public static function resultCheck($object)
    {
        /**** USING FOR CHECKING RESULT AFTER QUERY ****
         *
         * return true if no error found (check === true)
         * return error code if have error
         * return null when nothing catch
         *
         ***********************************************/
        if(!isset($object->error)){
            return true;
        }else{
            if(isset($object->error->message)){
                return $object->error->message;
            }
            return $object->error->code;
        }
        /*if(!empty($object)){
            if(!isset($object->error)){
                return true;
            }else{
                if(isset($object->error->message)){
                    return $object->error->message;
                }
                return $object->error->code;
            }
        }*/
        return null;
    }

    public static function getRepairCost($disputeItem)
    {
        /**** USING FOR DISPUTE ONLY ****/
        $totalRepairCost = 0;
        if(isset($disputeItem->EXTERIOR_SECTION)){
            foreach ($disputeItem->EXTERIOR_SECTION as $exterior_item){
                $totalRepairCost += $exterior_item->repair_cost;
            }
        }
        if(isset($disputeItem->INTERIOR_SECTION)){
            foreach ($disputeItem->INTERIOR_SECTION as $interior_item){
                $totalRepairCost += $interior_item->repair_cost;
            }
        }
        if(isset($disputeItem->MECHANICAL_SECTION)){
            foreach ($disputeItem->MECHANICAL_SECTION as $mechanical_item){
                $totalRepairCost += $mechanical_item->repair_cost;
            }
        }
        return $totalRepairCost;
    }

    public static function getVarianceCost($disputeItem)
    {
        /**** USING FOR DISPUTE ONLY ****/
        $totalRepairCost = 0;
        if(isset($disputeItem->EXTERIOR_SECTION)){
            foreach ($disputeItem->EXTERIOR_SECTION as $exterior_item){
                if($exterior_item->cost_variance != null){
                    $totalRepairCost += $exterior_item->cost_variance;
                }
            }
        }
        if(isset($disputeItem->INTERIOR_SECTION)){
            foreach ($disputeItem->INTERIOR_SECTION as $interior_item){
                if($interior_item->cost_variance != null){
                    $totalRepairCost += $interior_item->cost_variance;
                }
            }
        }
        if(isset($disputeItem->MECHANICAL_SECTION)){
            foreach ($disputeItem->MECHANICAL_SECTION as $mechanical_item){
                if($mechanical_item->cost_variance != null){
                    $totalRepairCost += $mechanical_item->cost_variance;
                }
            }
        }
        return $totalRepairCost;
    }

    public static function updateMOMenuAccessSession($menus)
    {
        $menu_links = array(
            1 => "mo-admin/dealer-groups-list",
            2 => "mo-admin/dealership-list",
            3 => "mo-admin/location-list",
            4 => "",
            5 => "mo-admin/user/list",
            6 => "mo-admin/role-list",
            7 => "mo-admin/edit-permission",
            8 => "mo-admin/settings/valuation-settings",
            9 => "mo-admin/settings/external-offering-settings",
            10 => "mo-admin/settings/auction-settings",
            11 => "mo-admin/trading/auction-session-list",
            12 => "mo-admin/trading/request-list",
            13 => "",
            14 => "mo-admin/trading/live-auction",
            39 => "mo-admin/transaction/view-transactions/current",
            40 => "mo-admin/transaction/view-transactions/complete",
            41 => "mo-admin/transaction/setting-fees",
            45 => "mo-admin/settings/rating-settings",
            46 => "mo-admin/valuations/requests",
            47 => "mo-admin/valuations/in-progress",
            48 => "mo-admin/valuations/pending",
            49 => "mo-admin/valuations/complete",
            50 => "mo-admin/settings/general",
            51 => "mo-admin/trading/auction-history"
        );
        /*Log::debug("login - menu_links: ".json_encode($menu_links));*/

        $collection = collect($menus)->map(function ($item, $key) use ($menu_links) {
            /*Log::debug("Helper - updateMOMenuAccessSession - item: ".json_encode($item));*/
            if($item->site_menu == "mo_admin"){
                foreach($item->group as $group_menu){
                    $group_name = str_replace(" ", "_", $group_menu->group_name);
                    if(strtolower($group_name) == "dispute_centre"){
                        $group_menu->group_link = "mo-admin/dispute/show";
                        unset($group_menu->menu_items);
                    }
                    if(isset($group_menu->menu_items)){
                        foreach($group_menu->menu_items as $menu_item){
                            if(isset($menu_links[$menu_item->code])){
                                $menu_item->link = $menu_links[$menu_item->code];
                            }else{
                                $menu_item->link = "";
                            }
                        }
                    }
                }
            }
            return $item;
        });
        /*Log::debug("Helper - updateMOMenuAccessSession - collection: ".$collection);*/
        return $collection;
    }

    public static function updateDealershipMenuAccessSession($menus)
    {
        $menu_links = array(
            17 => "dealership/location",
            18 => "dealership/roles",
            19 => "",
            20 => "dealership/users-list",
            21 => "dealership/settings",
            44 => "dealership/external-offer-groups",
            23 => "valuations/requests",
            24 => "valuations/in-progress",
            25 => "valuations/pending",
            26 => "valuations/complete",
            27 => "external-offer/incoming-offers",
            28 => "external-offer/outgoing-offers",
            29 => "test-drives/inprogress",
            30 => "test-drives/complete",
            31 => "trading/stock-list",
            32 => "trading/auction",
            33 => "trading/live-auction",
            34 => "trading/wishlist",
            35 => "dispute/current",
            36 => "dispute/resolved",
            37 => "transaction/view-transactions/current",
            38 => "transaction/view-transactions/complete",
        );
        /*Log::debug("login - menu_links: ".json_encode($menu_links));*/

        $collection = collect($menus)->map(function ($item, $key) use ($menu_links) {
            /*Log::debug("Helper - updateDealershipMenuAccessSession - item: ".json_encode($item));*/
            if($item->site_menu == "dealership"){
                foreach($item->group as $group_menu){
                    $group_name = strtolower(str_replace(" ", "_", $group_menu->group_name));

                    switch($group_name){
                        case "dashboard":
                            $group_menu->group_link = "dashboard";
                            unset($group_menu->menu_items);
                            $group_menu->menu_order = 0;
                            break;
                        case "configure":
                            $group_menu->menu_order = 1;
                            break;
                        case "customers":
                            $group_menu->group_link = "customers";
                            unset($group_menu->menu_items);
                            $group_menu->menu_order = 2;
                            break;
                        case "valuation":
                            $group_menu->menu_order = 3;
                            break;
                        case "place_my_car":
                            $group_menu->menu_order = 4;
                            break;
                        case "test_drives":
                            $group_menu->menu_order = 5;
                            break;
                        case "auction":
                            $group_menu->menu_order = 6;
                            break;
                        case "arriving_vehicle":
                            $group_menu->group_link = "arriving-vehicle/arriving-list";
                            unset($group_menu->menu_items);
                            $group_menu->menu_order = 7;
                            break;
                        case "dispute_centre":
                            $group_menu->menu_order = 8;
                            break;
                        case "transaction":
                            $group_menu->menu_order = 9;
                            break;
                    }

                    if(isset($group_menu->menu_items)){
                        foreach($group_menu->menu_items as $menu_item){
                            if(isset($menu_links[$menu_item->code])){
                                $menu_item->link = $menu_links[$menu_item->code];
                            }else{
                                $menu_item->link = "";
                            }
                        }
                    }
                }
                usort($item->group, function ($a, $b)
                {
                    return ($a->menu_order < $b->menu_order) ? -1 : 1;
                });
            }
            return $item;
        });
        /*$collection = usort(json_decode($collection), function($a, $b)
        {
            return strcmp($a->name, $b->name);
        });*/
        /*Log::debug("Helper - updateDealershipMenuAccessSession - collection: ".$collection);*/
        return $collection;
    }

    public static function isObjectEmpty($obj) {
        $propertiesArr = get_object_vars($obj);
        return count($propertiesArr) === 0;
    }
}