<?php

namespace App\Http\Middleware;

use Closure;

class RolesChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //global $roles;
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $roles = $api->getRoles($token);
        if(isset($roles->error)){
            $roles = null;
        }

        $request->merge(compact('roles'));
        return $next($request);
    }
}
