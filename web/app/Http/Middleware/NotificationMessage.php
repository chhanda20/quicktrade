<?php

namespace App\Http\Middleware;

use App\Library\Helper;
use Closure;
use Illuminate\Support\Facades\Log;

class NotificationMessage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //global $channels;
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $notifications = [];
        $result = json_decode($api->notifications($token));
        if ($result && !isset($result->error)) {
            $slicedResult = array_slice($result, 0, 20);
            $notifications = collect($slicedResult)->map(function($noti) {
                $data = new \stdClass();
                $data->eventCode = $noti->custom_data->eventCode;
                $data->title = $noti->custom_data->title;
                $data->valuationId = isset($noti->custom_data->valuationId) ? $noti->custom_data->valuationId : 0;
//                $data->time = isset($noti->custom_data->time) ? $noti->custom_data->time : '';
                $data->message = isset($noti->custom_data->message) ? $noti->custom_data->message : '';
//                $data->registration_number = isset($noti->custom_data->registration_number) ? $noti->custom_data->registration_number : '';
                return $data;
            });
        }
        $request->session()->put('notifications', $notifications);
        return $next($request);
    }
}
