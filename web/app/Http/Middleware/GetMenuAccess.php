<?php

namespace App\Http\Middleware;

use App\Library\Helper;
use Closure;
use Illuminate\Support\Facades\Log;

class GetMenuAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //global $channels;
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $role_id = $request->session()->get("role_id");

        /** GET MENUS and PUT in SESSION  **/
        $menus = json_decode($api->getMenuByRoleId($token, $role_id));
        Log::debug("middleware - GetMenuAccess - handle - menus ".json_encode($menus));
        if(!isset($menus->error)){
            $collection = Helper::updateMOMenuAccessSession($menus->menuList);
            //$request->session()->forget('mo_menus_sidebar');
            $request->session()->put('mo_menus_sidebar', $collection);

            $collection = Helper::updateDealershipMenuAccessSession($menus->menuList);
            $request->session()->put('dealership_menus_sidebar', $collection);
        }

        return $next($request);
    }
}
