<?php

namespace App\Http\Middleware;

use Closure;

class TokenChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->session()->get('token');
        if (isset($token) && $token != null) {
            return $next($request);
        } else {
            if ($request->is('mo-admin/*')) {
                return redirect("mo-admin/login");
            }else{
                return redirect("login");
            }
        }
    }
}