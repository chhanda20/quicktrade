<?php

namespace App\Http\Middleware;

use Closure;

class PermissionsChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //global $permissions;
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $permissions = $api->getPermissions($token);
        if(isset($permissions->error)){
            $permissions = null;
        }

        $request->merge(compact('permissions'));
        return $next($request);
    }
}
