<?php

namespace App\Http\Middleware;

use Closure;

class ChannelsHandle
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //global $channels;
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $contact_id = $request->session()->get("contact_id");

        $channels = json_decode($api->getChannelByContactId($token, $contact_id));
        /*$channels = $api->getChannels($token);*/
        if(isset($channels->error)){
            $channels = null;
        }

        $request->merge(compact('channels'));
        //$request->attributes->add($channels);

        return $next($request);
    }
}
