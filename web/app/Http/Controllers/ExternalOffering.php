<?php

namespace App\Http\Controllers;

use App\Library\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

//use Illuminate\Support\Facades\Request;

class ExternalOffering extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }

    public function sendRequestExternalOffer(Request $request)
    {
        $token = $request->session()->get("token");
        $arrival_date = $request->input("external-offer-arrival-date");
        $valuation_id = $request->input("valuation-id");

        $send_to = $request->input("external-offer-send-to");

        if ($send_to == "external-offer-send-to-public") {
            $group = null;
        } else if ($send_to == "external-offer-send-to-group") {
            $group = $request->input("external-offer-group");
        }

        $params = array("arrivalDate" => $arrival_date,
                        "valuationId" => $valuation_id,
                        "group" => $group);

        
        $api = new \App\Library\APICaller();

        return $api->sendExternalOffer($token, $params);
    }

    public function getExternalOffer(Request $request)
    {
        $token = $request->session()->get("token");
        $valuation_id = $request->input("valuation_id");

        $api = new \App\Library\APICaller();
        $result = json_decode($api->getExternalOffer($token, $valuation_id));
        $time_remaining = json_decode($api->getSettingExternalOffer($token));

        if (isset($time_remaining->external_offer_timeout)) {
            $result->time_remaining = $time_remaining->external_offer_timeout;
        } else {
            $result->time_remaining = 30;
        }
        return json_encode($result);
    }

    public function bidPriceForExternalOffer(Request $request)
    {
        $token = $request->session()->get("token");
        $external_offer_id = $request->input("external_offer_id");
        $price = $request->input("price");
        $expiry_date = $request->input("expiry_date");

        $api = new \App\Library\APICaller();
        $params = array("externalOfferId" => $external_offer_id,
                        "price" => $price,
                        "expiryDate" => $expiry_date);

        return $api->bidPriceForExternalOffer($token, $params);
    }

    public function acceptBestOffer(Request $request)
    {
        $token = $request->session()->get("token");
        $external_offer_id = $request->input("external_offer_id");

        $api = new \App\Library\APICaller();
        $params = array("externalOfferId" => $external_offer_id);
        $result = $api->acceptBestOffer($token, $params);
        return $result;
    }

    public function rejectBestOffer(Request $request)
    {
        $token = $request->session()->get("token");
        $external_offer_id = $request->input("external_offer_id");

        $api = new \App\Library\APICaller();
        $params = array("externalOfferId" => $external_offer_id);
        $result = $api->rejectBestOffer($token, $params);
        return $result;
    }

    public function blockOffer(Request $request)
    {
        $token = $request->session()->get("token");
        $vehicle_id = $request->input("vehicle_id");

        $api = new \App\Library\APICaller();
        $params = array("vehicleId" => $vehicle_id);
        $result = $api->blockOffer($token, $params);
        return $result;
    }

    public function getOutgoingOffers(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $channels = $request->channels;
        $data = array(
            "channels" => $channels,
            "header" => "Place My Car",
            "breadcrumb" => "Outgoing Offers"
        );

        $limit = $request->input("limit");
        if(!isset($limit)){
            $limit = 20;
        }
        $offset = $request->input("offset");
        if(!isset($offset)){
            $offset = 0;
        }

        $orderDesc = true;
        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "orderDesc" => $orderDesc
        );
        // get user location
        $location_result = json_decode($api->getLocationByCurrentLoggedUser($token));
        $data["user_locations"] = null;
        if(!isset($location_result->error)){
            $data["user_locations"] = $location_result;
        }
        //get transmission type
        $data["external_offer_filter_transmission_type"] = json_decode($api->getTransMissionType($token));
        //get makes
        $data["external_offer_filter_make_list"] = json_decode($api->getGlassesData($token, "make", array(null)));

        $offer_status_types = $api->getTypeCode($token, array("type" => "OFFER_STATUS_SELLER"));
        $offer_status_array = array();
        foreach($offer_status_types as $offer_status_type){
            $offer_status_array[$offer_status_type->code] = $offer_status_type->value;
        }
        $params_submitted = array("status" => [1, 2]);
        /*Log::debug("submitted offers - token: ".$token);
        Log::debug("submitted offers - params: ".json_encode(array_merge($params, $params_submitted)));*/
        $submittedOfferList = json_decode($api->getOutgoingExternalOffer($token, array_merge($params, $params_submitted)));
        if(!isset($submittedOfferList->error)){
            $data["submitted_offers_list"] = json_decode($this->getStatusValue($submittedOfferList, $offer_status_types));
        }

        $params_received = array("status" => [3,4,5,6,7,8,10]);
        $receivedOfferList = json_decode($api->getOutgoingExternalOffer($token, array_merge($params, $params_received)));

        if(!isset($receivedOfferList->error)){
            $data["received_offers_list"] = json_decode($this->getStatusValue($receivedOfferList, $offer_status_types));
        }

        /** GET OFFER STATUS FILTER **/
        $offer_received_statuses_seller = array();
        foreach ($offer_status_types as $status) {
            if(in_array($status->code, [3,4,5,6,7,8,10])){
                $offer_received_statuses_seller[] = (object)array(
                    "code"=>$status->code,
                    "value"=>$status->value
                );
            }
        }
        $data["offer_received_statuses_seller"] = $offer_received_statuses_seller;
        Log::debug("getOutgoingOffers - offer_received_statuses_seller: ".json_encode($offer_received_statuses_seller));


        $params_sold = array("status" => [9]);
        $soldOfferList = json_decode($api->getSoldHistory($token, array_merge($params, $params_sold)));
        if(!isset($soldOfferList->error)){
            $data["sold_offers_list"] = json_decode($this->getStatusValue($soldOfferList, $offer_status_types));
        }
        $data["offer_status_array"] = $offer_status_array;
        //var_dump(gettype($result));die();
        return view('main.external-offer/outgoing-offers', $data);
	}

    public function setLocationForExternalOffer(Request $request)
    {
        $token = $request->session()->get("token");

        $api = new \App\Library\APICaller();

        $external_offer_id = $request->input("external_offer_id");
        $location_id = $request->input("location_id");

        $params = array("externalOfferId" => $external_offer_id,
                        "locationId" => $location_id);

        $result = $api->setLocationForExternalOffer($token, $params);

        return $result;
    }

    public function getIncomingExternalOffer(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $channels = $request->channels;

        $result = array(
            "channels" => $channels,
            "header" => "Place My Car",
            "breadcrumb" => "Incoming Offers"
        );
        
        $limit = 20;
        $offset = 0;
        $orderDesc = true;

        $offer_statuses_buyer = $api->getTypeCode($token, array("type" => "OFFER_STATUS_BUYER"));

        /** EXTERNAL OFFER (INCOMING) **/
        $status = [1,2];
        $params = array("status" => $status, "limit" => $limit, "offset" => $offset, "orderDesc" => $orderDesc);
        $incoming_external_offer_list = json_decode($api->getIncomingExternalOffer($token, $params));

        $result["incoming_external_offer_list"] = null;
        if(!isset($incoming_external_offer_list->error)){
            $result["incoming_external_offer_list"] = json_decode($this->getStatusValue($incoming_external_offer_list, $offer_statuses_buyer));
        }

        /** EXTERNAL OFFER (OFFER WON) **/
        $status = [3,4,6,7,8,5];
        $params = array("status" => $status, "limit" => $limit, "offset" => $offset, "orderDesc" => $orderDesc);
        $external_offer_won_list = json_decode($api->getExternalOfferWon($token, $params));

        $location_result = json_decode($api->getLocationByCurrentLoggedUser($token));;
        $result["user_locations"] = null;
        if(!isset($location_result->error)){
            $result["user_locations"] = $location_result;
        }
        $result["external_offer_filter_transmission_type"] = json_decode($api->getTransMissionType($token));

        $result["external_offer_won_list"] = null;
        if(!isset($external_offer_won_list->error)){
            $result["external_offer_won_list"] = json_decode($this->getStatusValue($external_offer_won_list, $offer_statuses_buyer));
        }

        /** GET OFFER STATUS FILTER **/
        $offer_won_statuses_buyer = array();
        foreach ($offer_statuses_buyer as $status) {
            if(in_array($status->code, [3,4,5,6,7,8])){
                $offer_won_statuses_buyer[] = (object)array(
                    "code"=>$status->code,
                    "value"=>$status->value
                );
            }
        }
        $result["offer_won_statuses_buyer"] = $offer_won_statuses_buyer;

        /** EXTERNAL OFFER (PURCHASE HISTORY) **/
        $status = array(9);
        $params = array("status" => $status, "limit" => $limit, "offset" => $offset, "orderDesc" => $orderDesc);
        Log::debug("getIncomingExternalOffer - purchase history params: ".json_encode($params));
        $external_offer_purchase_history_list = json_decode($api->getPurchaseHistory($token, $params));

        $result["external_offer_purchase_history_list"] = null;
        if(!isset($external_offer_purchase_history_list->error)){
            $result["external_offer_purchase_history_list"] = json_decode($this->getStatusValue($external_offer_purchase_history_list, $offer_statuses_buyer));
        }


        //get makes
        $params = array(null);
        $result["external_offer_filter_make_list"] = json_decode($api->getGlassesData($token, "make", $params));

        //get setting external offer - expiry date
        $externalOfferSettings = json_decode($api->getSettingExternalOffer($token));
        $result["external_offer_expiry"] = null;
        if(Helper::resultCheck($externalOfferSettings)===true){
            $result["external_offer_expiry"] = $externalOfferSettings->external_offer_expiry;
        }
        $data = $result;
        return view("main.external-offer/incoming-offers", $data);
    }

    private function getStatusValue($obj, $status_array)
    {
        Log::debug("getStatusValue - obj: ".json_encode($obj));
        $collection = collect($obj)->map(function ($item, $key) use ($status_array) {
            foreach ($status_array as $status){
                if($item->offer_status == $status->code){
                    $item->offer_status_value = $status->value;
                    return $item;
                }
            }
        });
        return $collection;
    }

    public function getOutgoingOffersByStatus(Request $request, $status)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $data = array();

        $limit = $request->input("limit");
        $offset = $request->input("offset");

        switch($status){
            case "submitted":
                $status_value = [1,2];
                break;
            case "received":
                $status_value = [3,4,5,6,7,8,10];
                break;
            case "sold":
                $status_value = [9];
                break;
            default:
                $status_value = [1];
                break;
        }

        $orderDesc = true;
        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "status" => $status_value,
            "orderDesc" => $orderDesc
        );

        $offer_status_types = $api->getTypeCode($token, array("type" => "OFFER_STATUS_SELLER"));
        $offer_status_array = array();
        foreach($offer_status_types as $offer_status_type){
            $offer_status_array[$offer_status_type->code] = $offer_status_type->value;
        }
        $data["offer_status_array"] = $offer_status_array;

        if($status == "sold") {
            $offerList = json_decode($api->getSoldHistory($token, $params));
        } else {
            $offerList = json_decode($api->getOutgoingExternalOffer($token, $params));
        }
        
        if(!isset($offerList->error)){
            $data["offers_list"] = $offerList;
            if(empty($offerList)){
                return null;
            }
        }

        return view('main.external-offer/outgoing-offers-sample-list', $data);
    }

    public function getIncomingOfferByStatus(Request $request, $status)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $data = array();

        $limit = $request->input("limit");
        $offset = $request->input("offset");
        //$offset = 0;

        switch($status){
            case "incoming":
                $status_value = [1,2];
                break;
            case "offer-won":
                $status_value = [3,4,5,6,7,8];
                break;
            case "purchase-history":
                $status_value = [9];
                break;
            default:
                $status_value = [1,2];
                break;
        }

        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "status" => $status_value,
            "orderDesc" => true
        );

        $offer_status_types = $api->getTypeCode($token, array("type" => "OFFER_STATUS_BUYER"));
        $offer_status_array = array();
        foreach($offer_status_types as $offer_status_type){
            $offer_status_array[$offer_status_type->code] = $offer_status_type->value;
        }
        $data["offer_status_array"] = $offer_status_array;

        if($status == "offer-won") {
            $offerList = json_decode($api->getExternalOfferWon($token, $params));
        } else if ($status == "purchase-history"){
            $offerList = json_decode($api->getPurchaseHistory($token, $params));
        }else {
            $offerList = json_decode($api->getIncomingExternalOffer($token, $params));
        }
        
        if(!isset($offerList->error)){
            $data["offers_list"] = $offerList;
            if(empty($offerList)){
                return null;
            }
        }else{
            return null;
        }

        return view('main.external-offer/incoming-offers-sample-list', $data);
    }

    public function getIncomingOfferByFilters(Request $request, $status)
    {
        $filter_data = $request->input("filter_data");
        if (isset($filter_data)) {
            $filter_data = json_decode($filter_data);
        }

        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $data = array();

        $limit = $request->input("limit");
        $offset = $request->input("offset");
        //$offset = 0;

        switch($status){
            case "incoming-offer":
                $status_value = [1,2];
                break;
            case "offer-won":
                
                //$status_value = [3,4,6,7,8]; 
                if (isset($filter_data->status) && !empty($filter_data->status)) {
                    $status_value = $filter_data->status;
                } else {
                    $status_value = [3,4,6,7,8,5];
                }

                break;
            case "purchase-history":
                $status_value = [9];
                break;
            default:
                $status_value = [1,2];
                break;
        }

        if (isset($filter_data) && !empty($filter_data)) {

            $make_data = [];

            foreach ($filter_data->makeInfo->make as $key => $value) {
                //set make name
                
                foreach ($value->model as $key2 => $value2) {
                    //set model name
                    if(json_encode($value2->variant) == "{}") {
                        $make_data_record = array("make" => $value->name);
                        $make_data_record["model"] = $value2->name;
                        array_push($make_data, $make_data_record);
                    }
                    else {
                        foreach ($value2->variant as $key3 => $value3) {
                            //set variant name
                            $make_data_record = array("make" => $value->name);
                            $make_data_record["model"] = $value2->name;
                            $make_data_record["variant"] = $value3->name;
                            array_push($make_data, $make_data_record);
                        }
                    }
                    
                }
                
            }
                    
                    
            $odo_range = array("from" => $filter_data->odoRange->from,
                               "to" => $filter_data->odoRange->to);

            $year_range = array("from" => $filter_data->yearRange->from,
                               "to" => $filter_data->yearRange->to);                                  

            $vehicle_location = null;
            if(isset($filter_data->vehicleLocation)) {
                $vehicle_location = [];
                foreach ($filter_data->vehicleLocation as $key => $value) {
                    array_push($vehicle_location, $value);
                }
            }

            $transmision_type = null;    
            if(isset($filter_data->transmissionType)) {
                $transmision_type = [];  
                foreach ($filter_data->transmissionType as $key => $value) {
                    array_push($transmision_type, $value);
                }
            }

            $params = array(
                "limit" => $limit,
                "offset" => $offset,
                "status" => $status_value,
                "orderDesc" => true,
                "odoRange" => $odo_range,
                "yearRange" => $year_range
            );     

            //date range
            if($filter_data->dateRange == "last_14_days") {
                $params["lastDay"] = 14;
            } else if ($filter_data->dateRange == "last_7_days") {
                $params["lastDay"] = 7;
            }else if ($filter_data->dateRange == "last_month"){  
                $params["lastDay"] = 30;
            } else {
                $params["dateRange"] = $filter_data->dateRange;
            }

            if($vehicle_location != null) {
                $params["vehicleLocation"] = $vehicle_location;
            }

            if($transmision_type != null) {
                $params["transmisionType"] = $transmision_type;
            }

            if(isset($make_data) && !empty($make_data)) {
                $params["makeInfo"] = $make_data;
            }
        }

        //get status name of external offer
        $offer_status_types = $api->getTypeCode($token, array("type" => "OFFER_STATUS_BUYER"));
        $offer_status_array = array();
        foreach($offer_status_types as $offer_status_type){
            $offer_status_array[$offer_status_type->code] = $offer_status_type->value;
        }
        $data["offer_status_array"] = $offer_status_array;
        
        //reset params for filter when user click reset button
        $is_reset = $request->input("reset_filter");
        if (isset($is_reset) && $is_reset == "true") {
            $params = array(
                "limit" => $limit,
                "offset" => $offset,
                "status" => $status_value,
                "orderDesc" => true
            ); 
        }

        Log::debug("getIncomingOfferByFilters - status: ".$status);
        Log::debug("getIncomingOfferByFilters - params: ".json_encode($params));

        if($status == "offer-won") {
            $offerList = json_decode($api->getExternalOfferWon($token, $params));
            Log::debug("getIncomingOfferByFilters - offerList: ".json_encode($offerList));
        } else if ($status == "purchase-history"){
            $offerList = json_decode($api->getPurchaseHistory($token, $params));
        }else {
            $offerList = json_decode($api->getIncomingExternalOffer($token, $params));
        }

        $no_data = "no-data";
        if(!isset($offerList->error)){
            $data["offers_list"] = $offerList;
            if(empty($offerList)){
                return $no_data;
            }
        }else{
            return $no_data;
        }

        return view('main.external-offer/incoming-offers-sample-list', $data);
    }

    public function getOutgoingOfferByFilters(Request $request, $status)
    {
        $filter_data = $request->input("filter_data");

        Log::debug("getOutgoingOfferByFilters - filter_data: ".$filter_data);

        if (isset($filter_data)) {
            $filter_data = json_decode($filter_data);
        }
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $data = array();

        $limit = $request->input("limit");
        $offset = $request->input("offset");
        //$offset = 0;

        switch($status){
            case "submitted-offer":
                $status_value = [1,2];
                break;
            case "offer-received":
                
                //$status_value = [3,4,5,6,7,8,10];
                //$status_value = $filter_data->status;
                if (isset($filter_data->status) && !empty($filter_data->status)) {
                    $status_value = $filter_data->status;
                } else {
                    $status_value = [3,4,5,6,7,8,10];
                }

                break;
            case "sold":
                $status_value = [9];
                break;
            default:
                $status_value = [1,2];
                break;
        }

        if (isset($filter_data) && !empty($filter_data)) {

            $make_data = [];

            foreach ($filter_data->makeInfo->make as $key => $value) {
                //set make name
                
                foreach ($value->model as $key2 => $value2) {
                    //set model name
                    if(json_encode($value2->variant) == "{}") {
                        $make_data_record = array("make" => $value->name);
                        $make_data_record["model"] = $value2->name;
                        array_push($make_data, $make_data_record);
                    }
                    else {
                        foreach ($value2->variant as $key3 => $value3) {
                            //set variant name
                            $make_data_record = array("make" => $value->name);
                            $make_data_record["model"] = $value2->name;
                            $make_data_record["variant"] = $value3->name;
                            array_push($make_data, $make_data_record);
                        }
                    }
                    
                }
                
            }
            
            
            $odo_range = array("from" => $filter_data->odoRange->from,
                               "to" => $filter_data->odoRange->to);

            $year_range = array("from" => $filter_data->yearRange->from,
                               "to" => $filter_data->yearRange->to);

            $params = array(
                "limit" => $limit,
                "offset" => $offset,
                "status" => $status_value,
                "orderDesc" => true,
                "odoRange" => $odo_range,
                "yearRange" => $year_range
            );

            $vehicle_location = null;
            if(isset($filter_data->vehicleLocation)) {
                $vehicle_location = [];
                foreach ($filter_data->vehicleLocation as $key => $value) {
                    array_push($vehicle_location, $value);
                }
            }

            $transmision_type = null;    
            if(isset($filter_data->transmissionType)) {
                $transmision_type = [];  
                foreach ($filter_data->transmissionType as $key => $value) {
                    array_push($transmision_type, $value);
                }
            }   

            //date range
            if($filter_data->dateRange == "last_14_days") {
                $params["lastDay"] = 14;
            } else if ($filter_data->dateRange == "last_7_days") {
                $params["lastDay"] = 7;
            }else if ($filter_data->dateRange == "last_month"){  
                $params["lastDay"] = 30;
            } else {
                $params["dateRange"] = $filter_data->dateRange;
            }

            if($vehicle_location != null) {
                $params["vehicleLocation"] = $vehicle_location;
            }

            if($transmision_type != null) {
                $params["transmisionType"] = $transmision_type;
            }

            if(isset($make_data) && !empty($make_data)) {
                $params["makeInfo"] = $make_data;
            }

        } 

        $offer_status_types = $api->getTypeCode($token, array("type" => "OFFER_STATUS_SELLER"));
        $offer_status_array = array();
        foreach($offer_status_types as $offer_status_type){
            $offer_status_array[$offer_status_type->code] = $offer_status_type->value;
        }
        $data["offer_status_array"] = $offer_status_array;

        //reset params for filter when user click reset button
        $is_reset = $request->input("reset_filter");
        if (isset($is_reset) && $is_reset == "true") {
            $params = array(
                "limit" => $limit,
                "offset" => $offset,
                "status" => $status_value,
                "orderDesc" => true
            );

        }
        Log::debug("getOutgoingOfferByFilters - status: ".json_encode($status));
        Log::debug("getOutgoingOfferByFilters - params: ".json_encode($params));

        if($status == "sold") {
            $offerList = json_decode($api->getSoldHistory($token, $params));
        } else {

            $offerList = json_decode($api->getOutgoingExternalOffer($token, $params));
        }

        $no_data = "no-data";
        if(!isset($offerList->error)){
            $data["offers_list"] = $offerList;
            if(empty($offerList)){
                return $no_data;
            }
        }else{
            
            
            return $no_data;            
        }

        //exit(var_dump($data));

        return view('main.external-offer/outgoing-offers-sample-list', $data);
    }

}