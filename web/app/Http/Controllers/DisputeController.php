<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\Helper;
use Illuminate\Support\Facades\Log;

class DisputeController extends Controller
{
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }

    public function viewDisputeList(Request $request, $disputeType, $type = "all")
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $channels = $request->channels;

        $data = array(
            "header" => "Dispute Centre",
            "breadcrumb" => ucfirst($type),
            "channels" => $channels
        );

        $limit = 20;
        $offset = 0;

        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "type" => $type
        );

        switch ($disputeType){
            case "current":
                $status = [1,2,3];
                break;
            case "resolved":
                $status = [4];
                break;
        }
        $params["status"] = $status;

        $disputeStatuses = $api->getTypeCode($token, array("type" => "DISPUTE_STATUS"));

        $disputeList = json_decode($api->getDealershipDisputeList($token, $params));
        if(Helper::resultCheck($disputeList) === true){
            $collection = collect($disputeList)->map(function ($item, $key) use ($disputeStatuses) {
                foreach ($disputeStatuses as $disputeStatus){
                    if($item->status == $disputeStatus->code){
                        $item->status_value = $disputeStatus->value;
                        return $item;
                    }
                }
            });
            $data["disputeList"] = json_decode($collection);
        }
        $data["js"][] = "dispute/viewDisputeList";
        
        return view("main.dispute.dispute-list", $data);
    }

    public function getDisputeDataList(Request $request, $disputeType)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $limit = $request->input("limit");
        $offset = $request->input("offset");
        $type = $request->input("type");

        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "type" => $type
        );

        switch ($disputeType){
            case "current":
                $status = [1,2,3];
                break;
            case "resolved":
                $status = [4];
                break;
        }

        if(isset($status)){
            $params["status"] = $status;
        }

        $data = array();
        $data["type"] = $type;
        $disputeStatuses = $api->getTypeCode($token, array("type" => "DISPUTE_STATUS"));

        Log::debug("getDisputeDataList ".json_encode($params));

        $disputeList = json_decode($api->getDealershipDisputeList($token, $params));
        if(Helper::resultCheck($disputeList) === true){
            $collection = collect($disputeList)->map(function ($item, $key) use ($disputeStatuses) {
                foreach ($disputeStatuses as $disputeStatus){
                    if($item->status == $disputeStatus->code){
                        $item->status_value = $disputeStatus->value;
                        return $item;
                    }
                }
            });
            $data["disputeList"] = json_decode($collection);
        }
        return view("main.dispute.dispute-data-grid", $data);
    }

    public function viewDisputeInfo(Request $request, $disputeType, $transactionId, $disputeId)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $channels = $request->channels;

        $data = array(
            "header" => "Dispute Centre",
            "breadcrumb" => "Dispute Info",
            "channels" => $channels
        );

        /** LIMITATION DISPLAY INFO BY TYPE (CURRENT OR RESOLVED) **/
        $data["disputeType"] = $disputeType;

        /** DISPUTE DECISION **/
        $disputeDecisions = $api->getTypeCode($token, array("type" => "DISPUTE_DECISION"));

        /** DISPUTE INFO **/
        $disputeInfo = json_decode($api->getDisputeById($token, $disputeId));
        $general_recondition_cost = 0;
        $general_recondition_cost_variance = 0;
        if(Helper::resultCheck($disputeInfo) === true){
            $data["disputeInfo"] = $disputeInfo;

            /** GET GENERAL RECONDITIONING COST **/
            if($disputeInfo->general_recondition_cost == 1){
                $result = $api->getGeneralReconditionCost($token, $disputeInfo->valuation_id);
                if(Helper::resultCheck($result)===true){
                    $general_recondition_cost = $result->generalReconditionCost;
                }
                $general_recondition_cost_variance = $disputeInfo->general_recondition_cost_variance;
            }

            $data["viewOrEdit"] = "view";
            $data["disputeSelectedDecisionId"] = $disputeInfo->decision;
            $data["disputeSelectedDecisionName"] = Helper::getTypeName($disputeDecisions, $disputeInfo->decision);

            /** TRANSACTION INFO **/
            $transactionInfo = json_decode($api->getTransactionById($token, $disputeInfo->transaction_id));
            if(Helper::resultCheck($transactionInfo) === true){
                $data["transactionInfo"] = $transactionInfo;
            }
        }
        $data["general_recondition_cost"] = $general_recondition_cost;
        $data["general_recondition_cost_variance"] = $general_recondition_cost_variance;

        /** DISPUTE ITEMS **/
        $disputeItems = json_decode($api->getDisputeItems($token, $transactionId, $disputeId));
        $data["originalRepairCost"] = 0;
        $data["additionalRepairCost"] = 0;
        $data["totalRepairCost"] = 0;

        $data["originalVarianceCost"] = 0;
        $data["additionalVarianceCost"] = 0;
        $data["totalVarianceCost"] = 0;
        $data["additionalVarianceCostDisplay"] = 0;
        if(Helper::resultCheck($disputeItems) === true){
            $data["disputeItems"] = $disputeItems;
            $data["originalRepairCost"] = Helper::getRepairCost($disputeItems->original) + $general_recondition_cost;
            $data["additionalRepairCost"] = Helper::getRepairCost($disputeItems->additional);
            $data["totalRepairCost"] = $data["originalRepairCost"] + $data["additionalRepairCost"];

            $data["originalVarianceCost"] = Helper::getVarianceCost($disputeItems->original) + $general_recondition_cost_variance;
            $data["additionalVarianceCost"] = Helper::getVarianceCost($disputeItems->additional);
            $data["additionalVarianceCostDisplay"] = $data["additionalRepairCost"] - $data["additionalVarianceCost"];
            $data["totalVarianceCost"] = $data["originalVarianceCost"] + $data["additionalVarianceCost"];
        }

        $data["js"][] = "mo-admin/dispute/viewDisputeInfo";

        return view("main.dispute.dispute-info", $data);
    }

}
