<?php

namespace App\Http\Controllers;

use App\Library\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

//use Illuminate\Support\Facades\Request;

class APIController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $email = $request->input("email");
        $password = $request->input("password");
        $api = new \App\Library\APICaller();
        $result = $api->login($email, $password);
        if(isset($result->token) && $result->token != false) {
            $request->session()->put('token', $result->token);
            if(isset($result->id)) {
                $request->session()->put('id', $result->id);
            }
            if(isset($result->contact_id)) {
                $request->session()->put('contact_id', $result->contact_id);
            }
            Log::debug("login - company_id".$result->company_id);
            if(isset($result->company_id)) {
                $request->session()->put('company_id', $result->company_id);
            }
            if(isset($result->company_group_id)) {
                $request->session()->put('company_group_id', $result->company_group_id);
            }
            /** GET ROLES and PUT in SESSION **/
            $roles = $result->roles;
            $request->session()->put('roles', $roles);

            /** GET PERMISSIONS and PUT in SESSION  **/
            $permissions = $result->permissions;
            $request->session()->put('permissions', $permissions);

            /** GET USER INFORMATION and PUT in SESSION  **/
            $user_info = json_decode($api->getUserInfo($result->token));
            if(!isset($user_info->error)){
                $request->session()->put('user_info', $user_info);
            }

            /** GET ROLE ID and PUT in SESSION  **/
            $user_info_by_id = json_decode($api->getUserById($result->token, $result->id));
            if(!isset($user_info_by_id->error)){
                $request->session()->put('role_id', $user_info_by_id->userInfo[0]->roleId);
            }

            /** GET MENUS and PUT in SESSION  **/
            /*$menus = $result->menu;
            $collection = Helper::updateDealershipMenuAccessSession($menus);
            $request->session()->put('dealership_menus_sidebar', $collection);*/

            /*return redirect("/home");*/

            /** GET TERM AND CONDITION **/
            $need_accept_term_and_condition = $result->need_accept_term_and_condition;
            if($need_accept_term_and_condition == 1){
                $term_and_condition = json_decode($api->getTermAndCondition($result->token));
                $result->term_html = view('login_term', ['term_and_condition' => $term_and_condition->term_and_condition])->render();

            }
           // dd($result);
            return json_encode($result);
        } else {
            $error = array(
                "error" => true,
                "error_desc" => Helper::resultCheck($result)
            );
            return json_encode($error);
            /*if (isset($result)) {
                $data = array("error" => $result->error->code);
            } else {
                $data = array("error" => "Could not connect to the server.");
            }
            return view("login", $data);*/
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|\Laravel\Lumen\Http\Redirector
     */
    public function forgotPassword(Request $request)
    {
        
        $email = $request->input("email");

        //$this->callResetPasswordApi($email);
        $api = new \App\Library\APICaller();

        $result = $api->resetPassword($email);

       /* if (isset($result->success)) {
            echo "Please check your email!.";
        } else {
            $data = array("error" => $result->error->code);
            return view("forgot-password", $data);
        }*/

       if(isset($result->success)){
           Session::flash("message_success", "Please check your email !");
           return redirect("/forgot-password");
       }else{
           $data = array("error" => $result->error->code);
           return view("forgot-password", $data);
       }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Laravel\Lumen\Http\Redirector
     */
    public function setNewPassword(Request $request)
    {
        $new_password = $request->input("new_password");
        $confirm_password = $request->input("confirm_new_password");
        $password_token = $request->input("password_token");
        $api = new \App\Library\APICaller();
        $result = $api->updatePassword($password_token, $new_password);
        if ($result) {
            return redirect("login");
        } else {
            return redirect("reset-password/".$password_token);
        }
    }

    public function index(Request $request)
    {
        echo "<pre>";
        echo var_dump($request::all());
        echo "</pre>";
        exit;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Laravel\Lumen\Http\Redirector
     */
    public function logout(Request $request) {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $result = $api->logout($token);
        if(Helper::resultCheck($result) === true){
            $request->session()->forget("token");
            if ($request->is('mo-admin/*')) {
                return redirect("mo-admin/login");
            }else{
                return redirect("login");
            }
        }
    }

    /**
     * @param Request $request
     * @return int
     */
    public function acceptTermAndCondition(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $result = json_decode($api->userAcceptTermCondition($token));
        if(!isset($result->error)){
            return 1;
        }
        return $result->error;
    }
}