<?php
/**
 * Created by PhpStorm.
 * User: Son
 * Date: 8/1/2017
 * Time: 11:04 AM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('handling-channel');
        $this->middleware('get-menu-access');
    }

    public function viewDashBoard(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $channels = $request->channels;
        $dashboard_list = json_decode($api->getCustomizeDashBoard($token));
        $listDashboard = $dashboard_list->listDashboard;
        $list_location = json_decode($api->getLocationByCurrentLoggedUser($token));
        $medias = $api->getTypeCode($token, array("type"=>"MEDIA"));
        $data = array(
            "headers" => "Dashboard",
            "listDashboard" => $listDashboard,
            "list_location" => $list_location,
            "marketing_option" => $medias,
            "channels" => $channels
        );

        $test_drive_list = $this->getTestDriveInprogress($request);
        $data["test_drive_list"] = $test_drive_list;

        $valuation_request_list = $this->getValuationRequest($request);
        $data["valuation_request_list"] = $valuation_request_list;

        $valuation_pending = $this->getValuationPendingApproval($request);
        $data["valuation_pending"] = $valuation_pending;

        $place_my_car_incoming = $this->getPlaceMyCarIncoming($request);
        $data["place_my_car_incoming"] = $place_my_car_incoming;

        $get_external_offer_setting = $this->getExternalOfferExpiry($request);
        $data["external_offer_expiry"] = $get_external_offer_setting;

        $place_my_car_outgoing = $this->getPlaceMyCarOutgoing($request);
        $data["place_my_car_outgoing"] = $place_my_car_outgoing;

        $transaction_list = $this->getTransactionCurrent($request);
        $data["transaction_list"] = $transaction_list;

        $location_list = $this->getLocationForIncoming($request);
        $data["user_location"] = $location_list;

        $dashboard_list = [];
        $data["dashboard_list"] = '';
        $data["permissions"] = $request->permissions;
        if(isset($listDashboard)){
            foreach($listDashboard as $dashboard){
                $dashboard_list[] =[
                    "category_type" => $dashboard->dashboard_category,
                    "list_items" => $dashboard->dashboard_items
                ];
                $data["dashboard_list"] = $dashboard_list;
            }
        }
        return view("main.dashboard.view-dashboard", $data);
    }

    public function getDashBoard(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');
        $range_from = $request->input("mo-date-range-from");
        $range_to = $request->input("mo-date-range-to");
        $own_stat = $request->input("show_only_my_stats");
        $location_list = $request->input("location_items");
        if(!empty($location_list)){
            $location_list = array_map("intval", $location_list);
        }
        $dashboard_list = $request->input("customize_items_list");
        $params = array(
            "dashboard_list" => $dashboard_list,
            "location" => $location_list,
            "range_from" => $range_from,
            "range_to" => $range_to,
            "own_stat" => $own_stat
        );
        Log::debug("getDashBoard - params ".json_encode($params));
        $result = $api->getDashBoard($token, $params);
        //Log::debug("getDashBoard - result ".json_encode($result));
        if(!isset($result->error)){
            return $result;
        }else{
            return null;
        }
    }

    public function getTestDriveInprogress(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $limit = 5;
        $offset = 0;
        $status = [1];
        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "status" => $status
        );

        $results = json_decode($api->searchTestDriveWithCurrentUser($token, $params));
        if(!isset($results->error)){
            return $results;
        }
        return null;

    }

    public function getValuationRequest(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');
        $status = 1;
        $offset = 0;
        $query = null;
        $limit = 5;
        $orderBy = "valuation_create_date";
        $orderDesc = true;
        $valuations_requests = json_decode($api->getValuations($token, $limit, $offset, $status, $query, $orderBy, $orderDesc));
        if(!isset($valuations_requests->error)){
            return $valuations_requests;
        }
        return null;
    }

    public function getValuationPendingApproval(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');
        $status = [4,5];
        $offset = 0;
        $query = null;
        $limit = 5;
        $orderBy = "valuation_create_date";
        $orderDesc = true;
        $valuations_pending_approval = json_decode($api->getValuations($token, $limit, $offset, $status, $query, $orderBy, $orderDesc));
        if(!isset($valuations_pending_approval->error)){
            return $valuations_pending_approval;
        }
        return null;
    }

    public function getPlaceMyCarIncoming(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $limit = 5;
        $offset = 0;
        $status = [1,2];
        $offer_statuses_buyer = $api->getTypeCode($token, array("type" => "OFFER_STATUS_BUYER"));
        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "status" => $status,
        );
        $incoming_external_offer_list = json_decode($api->getIncomingExternalOffer($token, $params));

        /** External Offer Won **/
        $status = [4];
        $params = array("status" => $status, "limit" => $limit, "offset" => $offset);
        $external_offer_won_list = json_decode($api->getExternalOfferWon($token, $params));

        if(is_array($incoming_external_offer_list) && is_array($external_offer_won_list)){
            $external_offer_list = array_merge($incoming_external_offer_list, $external_offer_won_list);
            if(!isset($external_offer_list->error)){
                return json_decode($this->getStatusValue($external_offer_list, $offer_statuses_buyer));
            }
        }

    }

    public function getExternalOfferExpiry(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $externalOfferSettings = json_decode($api->getSettingExternalOffer($token));
//        $result["external_offer_expiry"] = null;
        if(!isset($externalOfferSettings->error)){
//            $result["external_offer_expiry"] = $externalOfferSettings->external_offer_expiry;
            $external_offer_expiry = $externalOfferSettings->external_offer_expiry;
            return $external_offer_expiry;
        }
    }

    public function getLocationForIncoming(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $location_result = json_decode($api->getLocationByCurrentLoggedUser($token));
        if(!isset($location_result->error)){
            return $location_result;
        }
        return null;

    }

    public function getPlaceMyCarOutgoing(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $limit = 5;
        $offset = 0;
        $status = [3];
        $offer_status_types = $api->getTypeCode($token, array("type" => "OFFER_STATUS_SELLER"));
        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "status" => $status
        );
        $result_outgoing = json_decode($api->getOutgoingExternalOffer($token, $params));
        if(!isset($result_outgoing->error)){
            return json_decode($this->getStatusValue($result_outgoing, $offer_status_types));
        }
        return null;
    }

    public function getTransactionCurrent(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');
        $limit = 5;
        $offset = 0;
        $status = [20];
        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "status" => $status
        );
        $result_transaction = json_decode($api->getTransactions($token, $params));
        if(!isset($result_transaction->error)){
            return $result_transaction;
        }
        return null;


    }
    private function getStatusValue($obj, $status_array)
    {
        Log::debug("getStatusValue - obj: ".json_encode($obj));
        $collection = collect($obj)->map(function ($item, $key) use ($status_array) {
            foreach ($status_array as $status){
                if($item->offer_status == $status->code){
                    $item->offer_status_value = $status->value;
                    return $item;
                }
            }
        });
        return $collection;
    }
}