<?php
/**
 * Created by PhpStorm.
 * User: Son
 * Date: 5/6/2017
 * Time: 3:36 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library;

class InteractionController
{
    public function __construct()
    {
//        $this->middleware('check-token');
//        $this->middleware('handling-channel');
//        $this->middleware('check-role');
//        $this->middleware('check-permission');
    }


    public function createInteraction(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $contactId = $request->input("contact_id");
        $date_time = $request->input("date_time");
        $type = $request->input("type");
        $note = $request->input("note");

        $params = array(
            "contact_id" => $contactId,
            "input_date" => $date_time,
            "type" => $type,
            "note" => $note
        );

        $result = json_decode($api->createInteractionById($token, $params, $contactId));
        if(!isset($result->error)){
            return redirect("/customer-details/view/".$contactId."#interaction") ;
        }
        return redirect("/home");
    }

    /** Call from mo-master.js to get data **/
    public function getInteraction(Request $request, $contact_id = 0)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $limit = 20;
        $offset = $request->input("offset");

        $interaction_types = $api->getTypeCode($token, array("type" => "INTERACTION_TYPE"));

        $params = array(
            "contact_id" => $contact_id,
            "limit" => $limit,
            "offset" => $offset
        );
        $result = json_decode($api->getAnInteractionById($token, $params, $contact_id));

        if(!isset($result->error)){
            $result_list = array('data' => array());
            $type_value = "";
            foreach($result as $key => $value){
                foreach ($interaction_types as $interaction_type) {
                    if($value->type == $interaction_type->code){
                        $type_value = $interaction_type->value;
                    }
                }
                
                $arr = array(
                    0=>$value->id,
                    1=>$value->input_date,
                    2=>$value->type,
                    3=>$type_value,
                    4=>$value->note
                );
                array_push($result_list['data'], $arr);
            }

//            $result_count = $api->countInteraction($token, $contact_id);
            if(!$result_list['data']){
                return null;
            }
            return json_encode($result_list);
        }
        else{
            return null;
        }
    }

   public function updateInteraction(Request $request, $interaction_id)
   {

       $api = new \App\Library\APICaller();
       $token = $request->session()->get("token");

       $contactId = $request->input("contact_id");
       $input_date = $request->input("date_time");
       $type = $request->input("type");
       $note = $request->input("note");

       $params = array(
           "contact_id" => $contactId,
           "input_date" => $input_date,
           "type" => $type,
           "note" => $note
       );
       $result = json_decode($api->updateInteractionById($token, $params, $contactId, $interaction_id));
       if(!isset($result->error)){
           return 1;
       }
       return 0;

   }


    public function deleteInteraction(Request $request, $interaction_id)
    {   
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $contactId = $request->input("contact_id");

        $result = json_decode($api->deleteInteractionById($token, $contactId, $interaction_id));
        if(!isset($result->error)){
            return 1;
        }
        return 0;
    }
}