<?php

namespace App\Http\Controllers;

use App\Library\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class LocationController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('handling-channel');
        $this->middleware('get-menu-access');
    }

    public function viewLocations(Request $request)
    {
        //return data format
        $channels = $request->channels;
        $data = array(
            "header"      => "Configure",
            "breadcrumb" => "Location",
            "channels" => $channels
        );
        
        return view('main.locations', $data);
    }

    public function getLocations(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $company_id = $request->session()->get("company_id");
        $query = $request->input("mo_txt_search");
        $limit = 20;
        $offset = $request->input("offset");
        
        $params = array(
            "query" => $query,
            "limit" => $limit,
            "offset" => $offset,
            "company_id" => $company_id
        );
        $locations = json_decode($api->getLocations($token, $params));

        //console_log($locations, "locations");
        if (!isset($locations->error)) {

            $locations_json = array('data' => array ());

            foreach ($locations as $key => $value) {

                $arr = array(
                    0 => $value->id,
                    1 => $value->name,
                    2 => $value->location_code,
                    3 => $value->status_code == 1 ? "Inactive" : "Active"
                );
                array_push($locations_json["data"], $arr);
            }
            $params = array("query" => $query, "company_id" => $company_id);
            $length = $api->getLengthLocation($token, $params);
            $locations_json["count"] = json_decode($length)->count;
            echo json_encode($locations_json);

        } else {
            echo null;
        }

    }

    public function initLocationForm(Request $request, $type, $locationId = 0)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $params = array("type" => "STATUS_CODE");
        $location_status_list = $api->getTypeCode($token, $params);

        /** Get Country **/
        $country_list = $api->getCountry($token);

        $channels = $request->channels;
        $data = array(
            "channels" => $channels,
            "header"      => "Location",
            "location_status_list" => $location_status_list,
            "country_list" => $country_list
        );

        $data["mode"] = "new";

        if($type == "new"){
            $data["breadcrumb"] = "New";

        }else if($type == "edit"){
            $data["breadcrumb"] = "Edit";
            $data["mode"] = "edit";
            $result = json_decode($api->getLocationById($token, $locationId));
            if(Helper::resultCheck($result) === true){
                $result = $result[0];
                $data["location_details"] = $result;

                $data["selected_state"] = isset($result->state) ? $result->state : null;

                foreach($country_list as $key=>$value){
                    foreach($value->stateList as $key2=>$value2){
                        if($value2->id == $result->state){
                            $data["state_list"] = $value->stateList;
                            break;
                        }
                    }
                }
            }
        }

        $data["type"] = $type;

        $data["js"][] = "location/createLocation";
        $data["css"][] = "star-rating";

        return view('main.create-location', $data);
    }

    public function saveLocation(Request $request, $location_id = 0)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $new_or_edit = $request->input("new_or_edit");

        $location_name = $request->input("dealer_location_name");
        $location_code = $request->input("dealer_location_code");
        $status_code = $request->input("dealer_location_status");
        $company_id = $request->session()->get("company_id");
        /** Get input field in location **/
        $location_lmct = $request->input("dealer_location_lmct");
        $location_address = $request->input("dealer_location_address");
        $location_suburb = $request->input("dealer_location_suburb");
        $location_country = $request->input("dealer_location_country");
        if($location_country == null){
            $location_country = $request->input("dealer_location_country_input");
        }
        $location_state = $request->input("dealer_location_state");
        $location_post_code = $request->input("dealer_location_post_code");

        $params = array(
            "name" => $location_name,
            "location_code" => $location_code,
            "status_code" => $status_code,
            "company_id" => $company_id,
            "lmct" => $location_lmct,
            "address" => $location_address,
            "suburb" => $location_suburb,
            "country" => $location_country,
            "state" => $location_state,
            "postcode" => $location_post_code
        );

        Log::debug("saveLocation - params: ".json_encode($params));
        Log::debug("saveLocation - new_or_edit: ".$new_or_edit);

        if($new_or_edit == "edit")
        {
            $result = json_decode($api->updateLocation($token, $location_id, $params));
        }else if ($new_or_edit == "new")
        {
            $result = json_decode($api->createLocation($token, $params));
        }
        Log::debug("saveLocation - result: ".json_encode($result));

        if(Helper::resultCheck($result) === true){
            return 1;
        }
        return json_encode(Helper::resultCheck($result));
    }

    public function getLocationsByCurrentUserID(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        /*$company_id = $request->session()->get('company_id');
        $length = json_decode($api->getLengthLocation($token, array("company_id" => $company_id)));
        $offset = 0;

        $params = array(
            "limit" => $length->count,
            "offset" => $offset,
            "company_id" => $company_id
        );
        $locationList = json_decode($api->getLocations($token, $params));*/
        $locationList = json_decode($api->getLocationByCurrentLoggedUser($token));

        if(!empty($locationList) && !isset($locationList->error)){
            $locationArr = array();
            foreach($locationList as $location){
                $locationArr[$location->id] = $location->name;
            }
            return $locationArr;
        }else {
            return null;
        }
    }

    public function getByCompany(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $companyId =  $request->input("dealership");
        $params = [
            'company_id' => $companyId,
            'limit' => 50000, // get all
            'offset' => 0
        ];
        $result = json_decode($api->getAllLocations($token, $params));
        if ($result){
            return $result->locationList;
        }
        return NULL;
    }

    public function getAllLocations(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $locationList = json_decode($api->getAllLocations($token));

        if(!isset($locationList->error) && !empty($locationList && isset($locationList->locationList))){
            $locationArr = array();
            foreach($locationList->locationList as $location){
                $locationArr[$location->id] = $location->name;
            }
            return $locationArr;
        }else {
            return null;
        }
    }
}