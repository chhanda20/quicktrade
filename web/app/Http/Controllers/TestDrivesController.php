<?php
/**
 * Created by PhpStorm.
 * User: Son
 * Date: 6/16/2017
 * Time: 1:32 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class TestDrivesController extends Controller
{
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }
    public function getTestDrives(Request $request, $customer_id = 0)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        
        $limit = 20;
        $offset = $request->input("offset");
        $status = [1,2];
        $colours = $api->getTypeCode($token, array("type"=>"COLOR_TYPE"));
        $medias = $api->getTypeCode($token, array("type"=>"MEDIA"));
        $params = array(
            "customer_id" => $customer_id,
            "limit" => $limit,
            "status"=> $status,
            "offset" => $offset
        );
        $result = json_decode($api->getTestDriveListByCustomerId($token, $params));
        $result_list_size = json_decode($api->searchCountTestDrive($token, $params));
        if(!isset($result->error)) {
            $result_list = array("data" => array());
            $colour_name = "";
            $media_name = "";
            foreach($result as $key=>$value){
                /** Calculate duration test drive **/
                $date_a = new \DateTime($value->create_date);
                $date_b = new \DateTime($value->update_date);
                $interval = date_diff($date_a,$date_b);
                $complete_date = $interval->format('%dd %H:%I:%S');

                foreach($colours as $colour){
                    if($value->color == $colour->code){
                        $colour_name = $colour->value;
                    }
                }
                foreach($medias as $media){
                    if($value->marketing_code == $media->code){
                        $media_name = $media->value;
                    }
                }
                $arr = array(
                    "0"=>$value->id,
                    "1"=>$value->make,
                    "2"=>$value->model,
                    "3"=>date('Y', strtotime($value->build_date)),
//                    "3"=>$value->year,
//                    "4"=>$value->color,
                    "4"=>$colour_name,
                    "5"=>$value->create_date,
                    /*"6"=>$value->duration,
                    "7"=>$value->return_date,*/
                    "6"=>$value->status == 2 ? $complete_date : $value->duration,
                    "7"=>$value->status == 2 ? $value->update_date : $value->return_date,
                    "8"=>$value->status,
                    "9"=>$value->comment,
                    "10"=>$value->vehicle_rating,
                    "11"=>$value->service_rating,
                    "12"=>$media_name,
                    "13"=>$value->marketing_option
                );
                array_push($result_list["data"], $arr);
            }
            if(!empty($result_list_size)) {
                $result_list["count"] = $result_list_size->list_size->count;
                $result_list["test_drive_count"] = $result_list_size->list_size->count;
            }
            if(!$result_list["data"]){
                return null;
            }else{
                return json_encode($result_list);
            }
        }else{
            return null;
        }

    }

    public function viewSearchVehicle(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $colour_list = $api->getTypeCode($token, array("type" => "COLOR_TYPE"));
        $make_list = $api->getMakes($token);
        $request->session()->put('test-drives-url', url()->previous());
        $channel = $request->channel;
        $data = array(
            "header" => "Choose Vehicle",
            "make_list" => $make_list,
            "colour_list" => $colour_list,
            "channel" => $channel
        );
        $data["js"][] = "test-drive/search-vehicle";
        return view("main.test-drives.search-vehicle", $data);
    }

    public function getSearchVehicle(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $limit = 20;
        $offset = 0;

        $params = array(
            "limit" => $limit,
            "offset" => $offset
        );

        $result = json_decode($api->getSearchVehicle($token, $params));
        

        if(!isset($result->error)){
            $result_list = array("data" => array());
            foreach($result as $key=>$value){
                $arr = array(
                    "0"=>$value->id,
                    "1"=>$value->make,
                    "2"=>$value->model_family,
                    "3"=>$value->registration_number,
                    "4"=>$value->vin
                );
                array_push($result_list["data"], $arr);
            }
            if(!$result_list["data"]){
                return null;
            } else {
                $result_list["count"] = json_decode($api->findCountVehicle($token, $params))->count;
                return json_encode($result_list);
            }
        }else{
            return null;
        }
    }

    public function initCreateTestDrives(Request $request, $customer_id)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $data = array(
            "headers" => "Enter Vehicle Details",
            "customer_id"=>$customer_id
        );

        $data["js"][] = "test-drive/createTestDrive";
        return view("main.test-drives.create-test-drives", $data);
    }

    //format: license_front_[customerId].jpg, license_back_[customerId].jpg
    public function uploadDriverLicensePhoto(Request $request)
    {
        return true ; //23-8-19 By SS0033 .just for making test drive to run
        $image = $_FILES["test_drive_driver_licences_photo"]["tmp_name"];
        $image_file_type = ".jpg";
        $customer_id = $request->input("customer_id");
        $license_section = $request->input("license_section");
        $image_name = $license_section . $customer_id . $image_file_type;

        if ($image != null) {
            $fileContent = file_get_contents($image);
            $image_content = $fileContent;
            $image_name = basename($image_name);
            $azure = new \App\Library\AzureCloud();
            $image = $azure->uploadBlobImage($image_content, $image_name, "user-avatar");
            if ($image) {
                return $image;
            } else {
                return false;
            }
        }
    }

    public function selectVehicle(Request $request, $vehicle_id, $vehicle_name)
    {
        /*if ($request->session()->get('test-drives-url') != null) {
            return redirect($request->session()->get('test-drives-url'))->with('vehicle_id', $vehicle_id)->with('vehicle_name', $vehicle_name);
        } else {
            return redirect()->back();
        }*/
        if($request->session()->has("test-drives-url")){
            /*session(["vehicle_id" => $vehicle_id]);
            session(["vehicle_name" => $vehicle_name]);*/
            Session::put("vehicle_id", $vehicle_id);
            Session::put("vehicle_name", $vehicle_name);
            return redirect($request->session()->get("test-drives-url"));
        }else{
            return redirect()->back();
        }
    }

    public function createTestDrives(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $returnedDate = $request->input("returnedDate");
        $vehicleId = $request->input("vehicleId");
        $customerId = $request->input("customerId");
        $manual_vehicle = $request->input("manual_vehicle");

        $params = array(
            "vehicleId" => $vehicleId,
            "returnedDate" => $returnedDate,
            "customerId"=>$customerId,
            "manual_vehicle" => $manual_vehicle
        );
        return $result = $api->createTestDrivesSession($token, $params);
        
    }

    public function viewInprogressList(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $marketing_option = $api->getTypeCode($token, array("type"=>"MEDIA"));
        $channel = $request->channel;
        $data = array(
            "header" => "Test Drives",
            "breadcrumb" => "In Progress",
            "channel" => $channel,
            "marketing_option" => $marketing_option
        );
        return view("main.test-drives.inprogress-list", $data);
    }

    public function viewCompleteList(Request $request)
    {
        $channel = $request->channel;
        $data = array(
            "header" => "Test Drives",
            "breadcrumb" => "Complete",
            "channel" => $channel
        );

        return view("main.test-drives.complete-list", $data);
    }

    public function getInProgressList(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $search_txt = $request->input("mo_txt_search");
        $status = [1];
        $colours = $api->getTypeCode($token, array("type" => "COLOR_TYPE"));
        $limit = 20;
        $offset = $request->input("offset");

        $params = array(
            "limit" => $limit,
            "offset" => (int)$offset,
            "query" => $search_txt,
            "status" => $status,
        );

        $result = json_decode($api->searchTestDriveWithCurrentUser($token, $params));
        $result_list_size = json_decode($api->searchCountTestDrive($token, $params));
        if(!isset($result->error)){
            $result_list = array("data" => array());
            $colour_name = "";
            foreach($result as $key=>$value){
                foreach($colours as $colour){
                    if($value->color == $colour->code){
                        $colour_name = $colour->value;
                    }
                }
                $arr = array(
                    0=>$value->id,
                    1=>$value->make,
                    2=>$value->model,
                    3=>date('Y', strtotime($value->build_date)),
                    /*4=>$value->color,*/
                    4=>$colour_name,
                    5=>$value->create_date,
                    6=>$value->duration,
                    7=>$value->return_date,
                    8=>$value->status,
                    9=>$value->registration_number,
                    10=>$value->stock_number
                );
                array_push($result_list["data"], $arr);
            }
            if(!empty($result_list_size)) {
                $result_list["count"] = $result_list_size->list_size->count;
            }
            if(!$result_list["data"]){
                return null;
            }
            return json_encode($result_list);
        }else{
            return null;
        }
    }

    public function completeTestDrive(Request $request, $testdrive_id)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $test_drive_note = $request->input("test_drive_note");
        $vehicle_rating = $request->input("vehicle_rating");

        $service_rating = $request->input("service_rating");
        $marketing_code = $request->input("marketing_code");
        $marketing_option = $request->input("marketing_option");

        $params = array(
            "comment"=>$test_drive_note,
            "vehicle_rating"=>$vehicle_rating,
            "service_rating"=>$service_rating,
            "marketing_code"=>$marketing_code,
            "marketing_option"=>$marketing_option
        );

        $result = json_decode($api->completeTestDriveSession($token, $params, $testdrive_id));
        if(!isset($result->error)){
            return 1;
        }
        return json_encode($result);
    }

    public function getCompleteList(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $limit = 20;
        $offset = $request->input("offset");
        $search_txt = $request->input("mo_txt_search");

        $range_from = $request->input("date_range_from");
        $range_to = $request->input("date_range_to");

        $colours = $api->getTypeCode($token, array("type"=>"COLOR_TYPE"));
        $medias = $api->getTypeCode($token, array("type"=>"MEDIA"));

        $status = [2];

        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "query" => $search_txt,
            "status" => $status,
            "range_from" => $range_from,
            "range_to" => $range_to
        );

        $result = json_decode($api->searchTestDriveWithCurrentUser($token, $params));
        $result_list_size = json_decode($api->searchCountTestDrive($token, $params));

        if(!isset($result->error)) {
            $result_complete_list = array("data" => array());
            $colour_name = "";
            foreach($result as $key=>$value) {
                /** Calculate duration test drive **/
                $date_a = new \DateTime($value->create_date);
                $date_b = new \DateTime($value->update_date);
                $interval = date_diff($date_a,$date_b);
                $complete_date = $interval->format('%dd %H:%I:%S');

                foreach($colours as $colour) {
                    if($value->color == $colour->code) {
                        $colour_name = $colour->value;
                    }
                }
                foreach($medias as $media){
                    if($value->marketing_code == $media->code){
                        $media_name = $media->value;
                    }
                }
                $arr = array(
                    0=>$value->id,
                    1=>$value->make,
                    2=>$value->model,
                    3=>date('Y', strtotime($value->build_date)),
                    /*4=>$value->color,*/
                    4=>$colour_name,
                    5=>$value->create_date,
                    /*6=>$value->duration,*/
                    /*7=>$value->return_date,*/
                    6=>$complete_date,
                    7=>$value->update_date,
                    8=>$value->status,
                    9=>$value->comment,
                    10=>$value->registration_number,
                    11=>$value->stock_number,
                    12=>$value->vehicle_rating,
                    13=>$value->service_rating,
                    14=>$media_name,
                    15=>$value->marketing_option,

                );
                array_push($result_complete_list["data"], $arr);
            }

            if(!empty($result_list_size) && $result_list_size->list_size != null) {
                $result_complete_list["count"] = $result_list_size->list_size->count;
            }

            if(!$result_complete_list["data"]) {
                return null;
            }

            return json_encode($result_complete_list);

        }else{
            return null;
        }
    }

    public function getTestDriveById(Request $request, $testDrive_id = 0)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $result = json_decode($api->getTestDriveById($token, $testDrive_id));
        if(!isset($result->error)){
            return 1;
        }
        return 0;
    }

    public function initTermAndCondition(Request $request, $customer_id)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $text_condition = json_decode($api->getDealershipSettings($token));

        $returnedDate = $request->input("estimated_return_time");
        $vehicleId = $request->input("mo_selected_vehicle");

        $data = array(
            "header" => "Term & Condition",
            "text_condition" => $text_condition,
            //"test_drive_id" => $test_drive_id,
            "customer_id" => $customer_id,
            "vehicle_id" => $vehicleId,
            "returned_date" => $returnedDate
        );
        return view("main.test-drives.term-and-condition", $data);
    }

    public function uploadSignatureToAzure(Request $request)
    {
        return true ; //23-8-19 By SS0033 .just for making test drive to run
        $azure = new \App\Library\AzureCloud();
        $dataURL = $request->input("data_url");
        $testDriveId = $request->input("test_drive_id");
        $signatureImageName = "signature_".$testDriveId;
        $res = $azure->uploadSignature($dataURL, $signatureImageName);
        if(!empty($res)) {
            return $res;
        }
        return false;
    }

    public function addManualVehicle(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $vin = $request->input("vin_number");
        $build_date = $request->input("build_date");
        $make = $request->input("make");
        $model_family = $request->input("model");
        $variant = $request->input("variant");
        $colour = $request->input("colour");
        $registration_number = $request->input("registration");
        /*$trade_no = $request->input("trade_no");*/
        $stock_number = $request->input("stock_number");

        $params = array(
            "vin" => $vin,
//            "build_date" => $build_date,
            "build_date" => date("Y-m-d", strtotime("01-".$build_date)),
            "registration_number" => $registration_number,
            "make" => $make,
            "model_family" => $model_family,
            "variant" => $variant,
            "colour" => $colour,
            /*"trade_no" => $trade_no,*/
            "stock_number" => $stock_number
        );
        $result = $api->manualVehicleCreate($token, $params);
        if(!isset($result->error)){
            return $result;
        }else{
            return $result->error;
        }
    }

}

