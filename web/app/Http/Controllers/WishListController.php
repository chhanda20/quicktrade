<?php
/**
 * Created by PhpStorm.
 * User: Son
 * Date: 5/16/2017
 * Time: 3:39 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WishListController extends Controller
{
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }

    public function viewWishList(Request $request)
    {
        $channels = $request->channels;
        $data = array(
            "headers"  => "Auction",
            "breadcrumb"  => "Wish List",
            "channels" => $channels
        );

        $data["breadcrumb_auction"] = $data["breadcrumb"];
        return view('main.trading.wishlist.wishlist', $data);
    }

    public function getWishList(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $limit = 20;
        $query = $request->input("mo_txt_search");
        $offset = $request->input("offset");

        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "query" => $query
        );

        $wishlists = json_decode($api->searchWishList($token, $params));

        if(!isset($wishlists->error)) {
            $wishlist_arr = array('data'=> array(), 'count' => $wishlists->countList->count);

            $result_wishList = $wishlists->wishList;

            foreach($result_wishList as $key=>$value) {

                if (empty($value->makeInfo)) {
                    $arr = array(
                        0 => $value->id,
                        1 => "ALL",
                        2 => $value->odometer_range,
                        3 => $value->year_range,
                        4 => isset($value->location) ? $value->location : "ALL",
                        5 => isset($value->transmission) ? $value->transmission : "ALL",
                        6 => $value->price_range,
                    );
                    array_push($wishlist_arr["data"], $arr);
                }
                else {

                    foreach ($value->makeInfo as $make_info_key => $make_info_value) {
                        
                        $make_model_variant = isset($make_info_value->make) ? $make_info_value->make : null;
                        $make_model_variant .= isset($make_info_value->model) ? " - " . $make_info_value->model : null;
                        $make_model_variant .= isset($make_info_value->variant) ? " - " . $make_info_value->variant : null;
                        $arr = array(
                            0 => $value->id,
                            1 => $make_model_variant,
                            2 => $value->odometer_range,
                            3 => $value->year_range,
                            4 => isset($value->location) ? $value->location : "ALL",
                            5 => isset($value->transmission) ? $value->transmission : "ALL",
                            6 => $value->price_range,
                        );
                        array_push($wishlist_arr["data"], $arr);
                        
                    }
                }
                
            }

            return $wishlist_arr;

        }

        else{
            
            echo null;
        }
    }

    public function initWishListForm(Request $request, $type, $wishlist_id = 0, $error_result = null)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $make_list = $api->getMakes($token);
        $location_list = json_decode($api->getAllLocations($token));
        $transmission_type_list = json_decode($api->getTransMissionType($token));

        $data = array(
            "headers" => "Wishlist",
            "make_list" => $make_list,
            "location_list" => isset($location_list->locationList) ? $location_list->locationList : null,
            "transmission_type_list" => $transmission_type_list
        );

        if($type == "new"){
            $data["breadcrumb"] = "New";
            $data["error_result"] = $error_result;
            $data["type"] = $type;
        }else if($type == "edit") {
            $data["type"] = $type;
            if ($wishlist_id != 0) {
                $data["breadcrumb"] = "Edit";
                $result = json_decode($api->getWishListById($token, $wishlist_id));
                if(!isset($result->error)) {
                    $data["wish_list_detail"] = $result;
                }

            } else {
                //if wishlist_id = 0 redirect to new wishlist mode
                $data["breadcrumb"] = "New";
                $data["error_result"] = $error_result;
            }

        }

        $data["breadcrumb_auction"] = $data["breadcrumb"];
        return view("main.trading.wishlist.wishlist-form", $data);
    }

    public function updateWishList(Request $request, $wish_list_id)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        //make info
        $make_info = $request->input("make_info");

        //odo range
        $wishlist_odometer = explode("-",$request->input("wishlist_odo_amount"));
        $wishlist_odometer[0] = str_replace(',', '',$wishlist_odometer[0]);
        $wishlist_odometer[1] = str_replace(',', '',$wishlist_odometer[1]);

        //year
        $wishlist_year = explode("-",$request->input("wishlist_year_amount"));

        //location
        $wishlist_location = $request->input("wishlist_location");

        //transmission
        $wishlist_transmission = $request->input("wishlist_transmission_type");

        //price
        $wishlist_price = explode("-",$request->input("wishlist_price_amount"));
        $wishlist_price[0] = str_replace(',', '',$wishlist_price[0]);
        $wishlist_price[1] = str_replace(',', '',$wishlist_price[1]);
        $wishlist_price[0] = str_replace('$', '',$wishlist_price[0]);
        $wishlist_price[1] = str_replace('$', '',$wishlist_price[1]);

        $params = array(
            "makeInfo" => json_decode($make_info),
            "odometer" => $wishlist_odometer,
            "year" => $wishlist_year,
            "price" => $wishlist_price,
            "transmission" => $wishlist_transmission,
            "location" => $wishlist_location
        );
        
        $result = json_decode($api->updateWishList($token, $params, $wish_list_id));
        
        if(!isset($result->error)){
            return redirect("/trading/wishlist/")->with('info', 'A wishlist is just updated!');
        } else {
            return redirect("/trading/wishlist/edit/".$wish_list_id)->with('info', 'Oops something went wrong!');
        }
        
    }
    
    public function createWishList(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        //make info
        //$wishlist_make = $request->input("wishlist_make");
        //$wishlist_model = $request->input("wishlist_model");
        //$wishlist_variant = $request->input("wishlist_variant");
        $make_info = $request->input("make_info");

        //odo range
        $wishlist_odometer = explode("-",$request->input("wishlist_odo_amount"));
        $wishlist_odometer[0] = str_replace(',', '',$wishlist_odometer[0]);
        $wishlist_odometer[1] = str_replace(',', '',$wishlist_odometer[1]);

        //year
        $wishlist_year = explode("-",$request->input("wishlist_year_amount"));

        //location
        $wishlist_location = $request->input("wishlist_location");

        //transmission
        $wishlist_transmission = $request->input("wishlist_transmission_type");
        
        //price
        $wishlist_price = explode("-",$request->input("wishlist_price_amount"));
        $wishlist_price[0] = str_replace(',', '',$wishlist_price[0]);
        $wishlist_price[1] = str_replace(',', '',$wishlist_price[1]);
        $wishlist_price[0] = str_replace('$', '',$wishlist_price[0]);
        $wishlist_price[1] = str_replace('$', '',$wishlist_price[1]);

        $params = array(
            "makeInfo" => json_decode($make_info),
            "odometer" => $wishlist_odometer,
            "year" => $wishlist_year,
            "price" => $wishlist_price,
            "transmission" => $wishlist_transmission,
            "location" => $wishlist_location
        );

        $result = json_decode($api->createWishList($token, $params));
        
        if(!isset($result->error)){
            return redirect("/trading/wishlist")->with('info', 'A wishlist is just created!');
        }

        return $this->initWishListForm($request, "new", 0, json_encode($result));
    }
}