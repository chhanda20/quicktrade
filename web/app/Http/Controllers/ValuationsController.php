<?php

namespace App\Http\Controllers;
use App\Library\Helper;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class ValuationsController extends Controller
{
    public function __construct() {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }

    public function viewValuations(Request $request, $type)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $header = "Valuations";

        switch ($type) {
            case "requests":
                $status = 1;
                $breadcrumb = "Requests";
                break;

            case "in-progress":
                $status = 2;
                $breadcrumb = "In Progress";
                break;

            case "complete":
                $status = 3;
                $breadcrumb = "Complete";
                break;

            case "pending":
                $status = 4;
                $breadcrumb = "Pending Approval";
                break;

            default:
                $status = 1;
        }

        $length = json_decode($api->getLengthValuations($token, $status));
        if (isset($length->count)) {
            $page_number = ceil($length->count/20);
        } else {
            $page_number = 0;
        }
        
        /** GET CHANNELS from middleware **/
        $channels = $request->channels;
        $data = array(
            "header" => $header,
            "breadcrumb" => $breadcrumb,
            "page_number" => $page_number,
            "channels" => $channels
        );

        switch ($type) {
            case "requests":
                return view("main.valuations-requests", $data);
                break;
                
            case "in-progress":
                return view("main.valuations-in-progress", $data);
                break;

            case "complete":
                return view("main.valuations-complete", $data);
                break;

            case "pending":
                return view("main.valuations-pending", $data);
                break;       
        }
    }

    public function trade(Request $request)
    {
        $token = $request->session()->get("token");
        $valuation_id = $request->input("valuation_id");
        $note = $request->input("note");
        $trade_price = $request->input("trade_in_price");
        $proposed_arrival_date = $request->input("arrival_date");

        $api = new \App\Library\APICaller();
        $params = array("note" => $note, "trade_price" => $trade_price, "proposed_arrival_date" => $proposed_arrival_date);

        return $api->trade($token, $valuation_id, $params);
    }

    public function getSalesInfo(Request $request)
    {
        $token = $request->session()->get("token");
        $valuation_id = $request->input("valuation_id");

        $api = new \App\Library\APICaller();
        return $api->getSalesInfo($token, $valuation_id);
    }

    public function initAddValuationForm(Request $request, $customer_id)
    {
        $token = $request->session()->get("token");

        $api = new \App\Library\APICaller();


        $interested_vehicles = $api->getTypeCode($token, array("type" => "INTERESTED_VEHICLE_TYPE"));
        $colors = $api->getTypeCode($token, array("type" => "COLOR_TYPE"));
        $ownership_types = $api->getTypeCode($token, array("type" => "OWNERSHIP_TYPE"));
        $special_disclosures = $api->getTypeCode($token, array("type" => "SPECIAL_DISCLOSURES"));
        $repairable_list = $api->getTypeCode($token, array("type" => "REPAIRABLE_WRITE_OFF"));

        $makes = $api->getMakes($token);

        /** GET ROLES from middleware **/
        $roles = $request->roles;

        /** GET CHANNELS from middleware **/
        $channels = $request->channels;

        /** GET PERMISSIONS from middleware **/
        $permissions = $request->permissions;

        $data = array(
            "header" => "Valuations",
            "breadcrumb" => "New Valuation",
            "customer_id" => $customer_id,
            "interested_vehicles" => $interested_vehicles,
            "colors" => $colors,
            "makes" => $makes,
            "channels" => $channels,
            "roles" => $roles,
            "permissions" => $permissions,
            "ownership_types" => $ownership_types,
            "special_disclosures" => $special_disclosures,
            "repairable_list" => $repairable_list
        );

        $data["js"][] = "valuation/initAddValuationForm";

        return view("main.create-valuation", $data);
    }

    public function searchVin(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $vin_number = $request->input("vin_number");
        $year = $request->input("year");

        return $api->searchVinNumber($token, $vin_number, $year);
    }

    public function createValuation(Request $request)
    {

        $token = $request->session()->get("token");
        $customer_id = $request->input("customer_id");
        $vin = $request->input("vin");
        $vin = strtoupper($vin);
        $registration_number = strtoupper($request->input("registration_number"));
        $make = $request->input("make");
        $model = $request->input("model");
        $variant = $request->input("variant");
        $colour = $request->input("colour");
        $registration_expiry_date = $request->input("registration_expiry_date");

        /** CHECK BOX **/
        $one_owner_input = $request->input("one_owner");
        $one_owner = isset($one_owner_input) && $one_owner_input ? "1" : "0";

        $service_book_input = $request->input("service_book");
        $service_book = isset($service_book_input) && $service_book_input ? "1" : "0";

        $service_history_input = $request->input("service_history");
        $service_history = isset($service_history_input) && $service_history_input ? "1" : "0";
        /** end CHECK BOX **/

        $trade_in_location = $request->input("location_of_trade_in_vehicle");
        $keys_location = $request->input("location_of_vehicle_key");

        $build_date = $request->input("build_date");
        $compliance_date = $request->input("compliance_date");
        $odometer = $request->input("odometer");
        $engine_number = $request->input("engine_number");
        $engine_number = strtoupper($engine_number);
        /** INTERESTED VEHICLE **/
        $i_vehicle = $request->input("interested_in_vehicle");
        $i_make = $request->input("interested_make");
        $i_model = $request->input("interested_model");
        $i_variant = $request->input("interested_variant");

        /** FINANCE OWING **/
        $i_finance_owe = $request->input("finance_owing");
        $i_institution = $request->input("institution");
        $i_payout = $request->input("payout");
        $i_monthly_payment = $request->input("monthly_payment");

        /** OWNERSHIP TYPE **/
        $ownership_type_code = $request->input("ownership_type");
        $ownership_type_input = $request->input("ownership_type_other");
        $ownership_type = (object)array("code"=>(int)$ownership_type_code, "value"=>$ownership_type_input);

        /** SPECIAL DISCLOSURES **/
        $special_disclosures = $request->input("special_disclosure");

        /** REPAIRABLE WRITE OFF **/
        $repairable_write_off = $request->input("repairable_write_off");
        //exit(var_dump($ownership_type));

        $api = new \App\Library\APICaller();

        $params = array(
            "customer_id" => (int)$customer_id,
            "vin" => isset($vin)&&$vin!=''?$vin:null,
            "registration_number" => $registration_number,
            "make" => $make,
            "model_family" => $model,
            "variant" => $variant,
            "colour" => (int)$colour,
            "build_date" => $build_date!=''?date("Y/m/d", strtotime("01-".$build_date)):"",
            "registration_expiry_date" => $registration_expiry_date!=''?date("Y/m/d", strtotime($registration_expiry_date)):"",
            "has_one_owner" => (int)$one_owner,
            "has_service_book" => (int)$service_book,
            "has_service_history" => (int)$service_history,
            "customer_id" => $customer_id,
            "trade_in_location" => $trade_in_location,
            "keys_location" => $keys_location,
            "compliance_date" => $compliance_date!=''?date("Y/m/d", strtotime($compliance_date)):"",
            "odometer" => (int)$odometer,
            "engine_number" => $engine_number,
            "i_vehicle" => (int)$i_vehicle,
            "i_make" => $i_make,
            "i_model" => $i_model,
            "i_variant" => $i_variant,
            "i_finance_owe" => ($i_finance_owe === 'true'),
            "i_institution" => $i_institution,
            "i_payout" => (int)$i_payout,
            "i_monthly_payment" => (int)$i_monthly_payment,
            "ownership_type" => $ownership_type,
            "special_disclosures" => $special_disclosures,
            "repairable_write_off" => (int)$repairable_write_off
        );

        $submit_button = $request->input("submit_button");


        /** CREATE VALUATION **/
        $result = $api->createBeginValuation($token, $params);
       // dd($result);
        /** GET PERMISSIONS from middleware **/
        $permissions = $request->permissions;

        if(isset($result->id)){
            /** BEGIN VALUATION **/
            if(strtolower($submit_button) == "begin valuation"){
                /* Only user have valuation_perform could perform valuation */
                if(is_array($permissions) && in_array("valuation_perform", $permissions)){
                    $params = array("start_performing" => true); //change valuation status to 2 (in_process)
                    $result = $api->requestValuation($token, $result->id, $params);
                }

                return redirect("review-valuation/".$result->id);
            }
            /** REQUEST VALUATION **/
            else if(strtolower($submit_button) == "request valuation"){
                $result = $api->requestValuation($token, $result->id);
                /*if(!property_exists($result, 'error') && !is_null($result)){*/
                if(!isset($result->error)){
                    return redirect("/valuations/requests");
                }
            }
        }
    }

    /** AJAX call from mo-master.js to get DATA **/

    public function getDataOfValuations(Request $request, $type)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $limit = 20;
        $offset = $request->input("offset");
        $query = $request->input("mo_txt_search");

        /*Log::debug($query);*/

        switch ($type) {
            
            case "requests":

                $status = 1;
                $offset = $request->input("offset");
                $query = $request->input("mo_txt_search");
                $limit = 20;
                $orderBy = "valuation_create_date";
                $orderDesc = true;
                $valuations_requests = json_decode($api->getValuations($token, $limit, $offset, $status, $query, $orderBy, $orderDesc));

                if (!isset($valuations_requests->error)) {

                    $valuations_requests_json = array('data' => array ());
                    $colours = $api->getTypeCode($token, array("type" => "COLOR_TYPE"));

                    /** GET PERMISSIONS from middleware **/
                    $permissions = $request->permissions;

                    foreach ($valuations_requests as $key => $value) {

                        $colour = null;
                        foreach ($colours as $key2 => $value2) {

                            if ($colours[$key2]->code == $value->colour) {
                                $colour = $colours[$key2]->value;
                            }
                        }

                        $arr = array(
                            0 => $value->id,
                            1 => $value->math_wishlist,
                            2 => $value->make,
                            3 => $value->model_family,
                            4 => isset ($value->build_date) ? substr($value->build_date, 0, 4) : $value->build_date,
                            5 => $value->vin,
                            6 => $colour,
                            7 => $value->registration_number,
                            8 => date('M Y', strtotime($value->registration_expiry_date)),
                            9 => $value->salesperson_first_name.' '.$value->salesperson_last_name,
                            10 => is_array($permissions) && in_array("valuation_perform", $permissions)?"valuation_perform":"",
                        );
                        array_push($valuations_requests_json["data"], $arr);
                    }
                    $length = $api->getLengthValuations($token, $status, $query);
                    $valuations_requests_json["count"] = json_decode($length)->count;
                    echo json_encode($valuations_requests_json);

                } else {
                    echo null;
                }

                /*Log::debug(json_encode($valuations_requests_json));*/

            break;

            case "in-progress":
                //Log::debug($request->input("_token"));
                //Log::debug($request->input("mo_txt_search"). "mo_txt_search");
                //Log::debug($request->input("offset") . "offset");

                $status = 2;
                $offset = $request->input("offset");
                $query = $request->input("mo_txt_search");
                $limit = 20;
                $orderBy = "valuation_create_date";
                $orderDesc = true;
                $valuations_in_progress = json_decode($api->getValuations($token, $limit, $offset, $status, $query, $orderBy, $orderDesc));
                if (!isset($valuations_in_progress->error)) {
                    $valuations_in_progress_json = array('data' => array ());
                    $condition_type = $api->getTypeCode($token, array("type" => "CONDITION"));
                    //Log::debug($condition_type);
                    foreach ($valuations_in_progress as $key => $value) {
                        $condition = null;
                        foreach ($condition_type as $key2 => $value2) {
                            if ($condition_type[$key2]->code == $value->condition) {
                                $condition = $condition_type[$key2]->value;
                            }
                        }

//                        $arr = array(
//                            0 => $value->id,
//                            1 => $value->make,
//                            2 => $value->model_family,
//                            3 => isset ($value->build_date) ? substr($value->build_date, 0, 4) : $value->build_date,
//                            //4 => $value->condition,
//                            4 => $condition,
//                            5 => $value->valuer_first_name.' '.$value->valuer_last_name,
//                            6 => $value->salesperson_first_name.' '.$value->salesperson_last_name,
//                            7 => $value->location_name
//                            );

                        $arr = array(
                            0 => $value->id,
                            1 => $value->math_wishlist,
                            2 => $value->make,
                            3 => $value->model_family,
                            4 => isset ($value->build_date) ? substr($value->build_date, 0, 4) : $value->build_date,
                            //4 => $value->condition,
//                            4 => $condition,
                            5 => $value->registration_number,
                            6 => $value->valuer_first_name.' '.$value->valuer_last_name,
                            7 => $value->salesperson_first_name.' '.$value->salesperson_last_name,
                            8 => $value->location_name
                        );
                        array_push($valuations_in_progress_json["data"], $arr);
                    }

                    $length = $api->getLengthValuations($token, $status, $query);
                    $valuations_in_progress_json["count"] = json_decode($length)->count;
                    echo json_encode($valuations_in_progress_json);
                } else {
                    echo null;
                }

                break;

            case "complete":
                $status = [3,6,7,12,13,14];
                $offset = $request->input("offset");
                $query = $request->input("mo_txt_search");
                $limit = 20;
                $orderBy = "valuation_create_date";
                $orderDesc = true;
                $valuations_complete = json_decode($api->getValuations($token, $limit, $offset, $status, $query, $orderBy, $orderDesc));
                
                /** GET VALUATION STATUS FROM CODE ID **/
                $valuation_statuses = $api->getTypeCode($token, array("type" => "VALUATION_STATUS"));
                
                // foreach ($valuation_statuses as $key => $value) {
                //     if($valuation_statuses[$key]->code == $status){
                //         $valuation_status = $valuation_statuses[$key]->value;
                //         /*Log::debug($valuation_status);*/
                //     }
                // }

                if (!isset($valuations_complete->error)) {

                    $valuations_complete_json = array('data' => array ());
                    
                    //condition
                    //$condition_type = $api->getTypeCode($token, array("type" => "CONDITION"));

                    foreach ($valuations_complete as $key => $value) {
                        /** get CONDITION value **/

                        /* - condition
                        $condition = null;
                        foreach ($condition_type as $key2 => $value2) {
                            if ($condition_type[$key2]->code == $value->condition) {
                                $condition = $condition_type[$key2]->value;
                            }
                        }
                        */

                        foreach($status as $key3 => $value3) {

                            if ($value3 == $value->status) {
                                foreach($valuation_statuses as $key4 => $value4) {
                                    if ($value3 == $valuation_statuses[$key4]->code) {
                                        $valuation_status = $valuation_statuses[$key4]->value;
                                        break;
                                    }
                                }
                                break;
                            }
                        }

                        $arr = array(
                            0 => $value->id,
                            1 => $value->math_wishlist,
                            2 => $value->make,
                            3 => $value->model_family,
                            4 => isset ($value->build_date) ? substr($value->build_date, 0, 4) : $value->build_date,
                            //4 => $condition,
                            5 => $value->registration_number,
                            6 => $value->valuer_first_name.' '.$value->valuer_last_name,
                            7 => $value->salesperson_first_name.' '.$value->salesperson_last_name,
                            8 => $value->location_name,
                            9 => $valuation_status,
                            10 => $value->status,
                            11 => $value->valuation_value
                        );
                        array_push($valuations_complete_json["data"], $arr);
                    }

                    $length = $api->getLengthValuations($token, $status, $query);
                    $valuations_complete_json["count"] = json_decode($length)->count;
                    echo json_encode($valuations_complete_json);
                } else {
                    echo null;
                }

                /*Log::debug(json_encode($valuations_complete_json));*/
                break;

            case "pending":
                $status = [4,5];
                $offset = $request->input("offset");
                $query = $request->input("mo_txt_search");
                $limit = 20;
                $orderBy = "valuation_create_date";
                $orderDesc = true;
                $valuations_pending = json_decode($api->getValuations($token, $limit, $offset, $status, $query, $orderBy, $orderDesc));

                if (!isset($valuations_pending->error)) {
                    $valuations_pending_json = array('data' => array ());
                    $condition_type = $api->getTypeCode($token, array("type" => "CONDITION"));
                    $colours = $api->getTypeCode($token, array("type" => "COLOR_TYPE"));

                    foreach ($valuations_pending as $key => $value) {
                        /** get COLOUR value **/
                        $colour = null;
                        foreach ($colours as $key2 => $value2) {

                            if ($colours[$key2]->code == $value->colour) {
                                $colour = $colours[$key2]->value;
                            }
                        }

                        /** get CONDITION value **/
                        $condition = null;
                        foreach ($condition_type as $key2 => $value2) {
                            if ($condition_type[$key2]->code == $value->condition) {
                                $condition = $condition_type[$key2]->value;
                            }
                        }

                       $arr = array(
                           0 => $value->math_wishlist,
                           1 => $value->make,
                           2 => $value->model_family,
                           3 => isset ($value->build_date) ? substr($value->build_date, 0, 4) : $value->build_date,
                           //4 => $value->colour,
                           4 => $value->registration_number,
                           5 => $colour,
                           6 => $condition,
                           7 => $value->valuer_first_name.' '.$value->valuer_last_name,
                           8 => $value->salesperson_first_name.' '.$value->salesperson_last_name,
                           9 => $value->offer_value,
                           10 => $value->valuation_value,
                           11 => ($value->estimated_recon_cost == 0) ? $value->total_recon_cost : $value->estimated_recon_cost,
                           12 => $value->requested_price,
                           13 => $value->status,
                           14 => $value->id,
                       );

                        array_push($valuations_pending_json["data"], $arr);
                    }

                    $length = $api->getLengthValuations($token, $status, $query);
                    $valuations_pending_json["count"] = json_decode($length)->count;
                    echo json_encode($valuations_pending_json);
                } else {
                    echo null;
                }

                /*Log::debug(json_encode($valuations_pending_json));*/
                break;

            default:
                $status = 1;
        }
    }

    public function cancelValuation(Request $request, $id)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $result = json_decode($api->cancelValuation($token, (int)$id));

        if(Helper::resultCheck($result) === true){
            return 1;
        }
        return Helper::resultCheck($result);
    }

    public function reEvaluate(Request $request)
    {
        $token = $request->session()->get("token");

        $valuation_id = $request->input("valuation_id");
        $requested_price = $request->input("re_evaluate_price");
        $api = new \App\Library\APICaller();
        $params = array("requested_price" => $requested_price);

        $result = $api->reEvaluate($token, $valuation_id, $params);
        return $result;
    }

    public function tradeIn(Request $request, $valuation_id)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        /** SAVE SUMARY INFO **/
        /** get FORM INPUT **/
        $registration_no = $request->input("registration_no");
        $service_history = $request->input("service_history");
        $service_books = $request->input("service_books");
        $one_owner = $request->input("one_owner");
        $odometer_reading = $request->input("odometer_reading");
        $vehicleParams = array(
            "registration_number" => $registration_no,
            "has_service_history" => $service_history,
            "has_service_book" => $service_books,
            "has_one_owner" => $one_owner,
            "odometer" => $odometer_reading
        );

        $overall_rating = $request->input("overall_rating");
        $overallRatingParams = array(
            "overall_rating" => $overall_rating
        );

        $wholesale_value_options = $request->input("wholesale_value_options");
        $retailObj = (object) array('retail'=>$wholesale_value_options);
        $wholesaleParams = array(
            "value_of_options" => $retailObj
        );

        $vehicle = $request->input("vehicle");
        $valuation_price = $request->input("valuation_price");
        $maximum_valuation_price = $request->input("maximum_valuation_price");

        $salesInfoParams = array(
            "sale_status" => $vehicle,
            "valuation_price" => $valuation_price,
            "max_valuation_price" => $maximum_valuation_price
        );

        $overallEstimatedCost = $request->input("overall_estimated_cost");
        $estimatedParams = array(
            "estimated_recon_cost" => floatval($overallEstimatedCost)
        );

        /** create new version and set status to trade in **/
       /* $create_new_valuation_params = array(
            'valuation_id'=>$valuation_id,
            'fromVersion'=> null
        );*/

        $result_create_new_valuation_version = json_decode($api->createValuationVersionId($token, null, $valuation_id));

         /* TODO New code be SS0033 4thSept 2019
          * while trade In (receive vehicle) if create valuation vrsion returns error as "Valuation is modifying by someone"
          * display the error message to user and dont save anything.
          *
         */

          /* echo "result valuation version";
           dd($result_create_new_valuation_version);*/
        /** update VEHICLE DETAILS **/
        $api->updateValuations($token, $valuation_id, $vehicleParams);

        /** update OVERALL RATING **/
        $api->updateOverallRating($token, $valuation_id, $overallRatingParams);

        /** update SALES INFO **/
        $api->updateSalesInfo($token, $valuation_id, $salesInfoParams);

        /** update RECONDITIONING COST **/
        $api->updateReconditioningCost($token, $valuation_id, $estimatedParams);

        /** update WHOLESALE VALUE OPTIONS **/
        $api->updateWholesaleValues($token, $valuation_id, $wholesaleParams);
        /** SAVE SUMARY INFO **/

        $stock_number = $request->input("stock-number");
        if(isset($stock_number)) {
            $params["stock_number"] = $stock_number;
        }

        $location_id = $request->input("location_id");
        if(isset($location_id)) {
            $params["location_id"] = $location_id;
        }

        if(isset($result_create_new_valuation_version->msg) && $result_create_new_valuation_version->msg == "success") {
            //change status to trade in 
            $result = $api->tradeIn($token, $valuation_id, $params);
            //release version locker
            $result = json_decode($api->releaseValuationVersionId($token, $valuation_id));

        } else {
            //friendly show error message here
            return redirect("/review-valuation/".$valuation_id."/true#summary")->with('info', 'Create valuation version error.');
        }
        /** end create new version  **/

        if (!isset($result->error->code)) {
            return redirect("/review-valuation/".$valuation_id."#summary");
        }
        return redirect("/review-valuation/".$valuation_id."#summary");
    }

    public function approveRevaluation(Request $request)
    {
        $token = $request->session()->get("token");
        $valuation_id = $request->input("valuation_id");
        $api = new \App\Library\APICaller();
        $result = $api->approveRevaluation($token, $valuation_id);

        return $result;
    }

    public function rejectRevaluation(Request $request)
    {
        $token = $request->session()->get("token");
        $valuation_id = $request->input("valuation_id");
        $api = new \App\Library\APICaller();
        $result = $api->rejectRevaluation($token, $valuation_id);
        
        return $result;
    }

    public function approveValuate(Request $request)
    {
        $token = $request->session()->get("token");
        $valuation_id = $request->input("valuation_id");
        $api = new \App\Library\APICaller();
        $result = $api->approveValuation($token, $valuation_id);

        return $result;
    }

    public function deleteValuation(Request $request, $valuation_id)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $result = json_decode($api->deleteValuation($token, (int)$valuation_id));

        if(!isset($result->error)){
            return 1;
        }else{
            return 0;
        }
    }

    /*public function getModelList(Request $request, $make){
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $result = json_decode($api->getModelList($token, $make));
        //var_dump($result);die();

        if(!is_object($result)){
            return null;
        }else if(property_exists($result, "family")){
            return $result->family;
        }
    }

    public function getVariantList(Request $request, $make, $family){
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $result = json_decode($api->getVariantList($token, $make, $family));
        //var_dump($result);die();

        if(!is_object($result)){
            return null;
        }else if(property_exists($result, "variant")){
            return $result->variant;
        }
    }

    public function getReleaseYear(Request $request, $make, $family, $variant = null){
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $result = json_decode($api->getReleaseYear($token, $make, $family, $variant));
        //var_dump($result);die();

        if(!is_object($result)){
            return null;
        }else if(property_exists($result, "releaseYear")){
            return $result->releaseYear;
        }
    }

    public function getReleaseMonth(Request $request, $make, $family, $releaseYear, $variant = null){
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $result = json_decode($api->getReleaseMonth($token, $make, $family, $releaseYear, $variant));
        //var_dump($result);die();

        if(!is_object($result)){
            return null;
        }else if(property_exists($result, "releaseMonth")){
            return $result->releaseMonth;
        }
    }

    public function getModelDetail(Request $request, $valuationId, $make, $family, $releaseYear, $releaseMonth, $variant = null){
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $result = json_decode($api->getModelDetail($token, $valuationId, $make, $family, $releaseYear, (int)$releaseMonth, $variant));
        //var_dump($result);die();

        if(!is_object($result)){
            return null;
        }else if(property_exists($result, "releaseMonth")){
            return $result->releaseMonth;
        }
    }*/

    public function getValuationsByCustomerId(Request $request, $customerId) {

        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        //Log::debug("logged");
        $offset = $request->input("offset");
        $limit = 20;
        $valuations = json_decode($api->getValuationsByCustomerId($token, $customerId, $offset, $limit));

        if (!isset($valuations->error)) {
            $valuations_json = array('data' => array ());

            $statuses = $api->getTypeCode($token, array("type" => "VALUATION_STATUS"));
            $valuations_json["count"] = 0;
            foreach ($valuations as $key => $value) {
                $status_value = "";
                foreach ($statuses as $status) {

                    //compare status code
                    if ($status->code == $value->status) {
                        /*switch ($status->code){
                            case 1:
                                $status_label = "Requests";
                                break;
                            case 2:
                                $status_label = "In Progress";
                                break;
                            case 4:
                                $status_label = "Pending Approval";
                                break;
                            case 5:
                                $status_label = "Re-evaluate";
                                break;
                            default:
                                $status_label = $status->value;
                        }*/

                        $status_value = $status->value;
                        $status_code = $status->code;
                    }
                }
                if ($status_value != "" && $status_value != null) {
                    if ($status_code == 6 || $status_code == 7 || $status_code == 3
                        || $status_code == 2 || $status_code == 5 || $status_code == 4 || $status_code == 1
                        || $status_code == 12 || $status_code == 13 || $status_code == 14 )
                    {
                        $day_of_week = date('w', strtotime($value->create_date));
                        $day_of_week_names = ["Sun","Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
                        $create_date = $day_of_week_names[$day_of_week].' '.date('d M Y h:i a', strtotime($value->create_date));
                        $arr = array(
                            0 => $value->id,
                            1 => $create_date,
                            2 => $value->make." ".$value->model_family,
                            3 => "$".$value->offer_value,
                            4 => $status_value,
                            5 => $status_code
                        );
                        array_push($valuations_json["data"], $arr);

                        $valuations_json["count"] = $valuations_json["count"] + 1;
                    }
                }
            }

            //$length = $api->getLengthValuationsByCustomer($token, $customerId);
            //$valuations_json["count"] = json_decode($length)->count;
            $valuations_json["valuation_count"] = $valuations_json["count"];
            echo json_encode($valuations_json);

        } else {
            echo null;
        }
    }

    public function getGlassesData(Request $request, $selectedField){
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $params = array();

        /** GET FORM INPUT **/
        $year = $request->input("year");
        $make = $request->input("make");
        $family = $request->input("model");
        $variant = $request->input("variant");
        $month = (int)$request->input("month");
        /*$build_date = $request->input("build_date");
        $month = isset($build_date) && $build_date!=''?(int)substr($build_date,0,2):null;*/
        $transmission = $request->input("transmission");
        $body_type = $request->input("body_type");

        if(isset($year) && !empty($year)){
            $params["release_year"] = $year;
        }
        if(isset($make) && !empty($make)){
            $params["make"] = $make;
        }
        if(isset($family) && !empty($family)){
            $params["family"] = $family;
        }
        if(isset($variant) && !empty($variant)){
            $params["variant"] = $variant;
        }
        if(isset($month) && !empty($month)){
            $params["release_month"] = $month;
        }
        if(isset($body_type) && !empty($body_type)){
            $params["body_type"] = $body_type;
        }
        if(isset($transmission) && !empty($transmission)){
            $params["transmission"] = $transmission;
        }

        $result = json_decode($api->getGlassesData($token, $selectedField, $params));

        if(!empty($result)){
            return $result;
        }else {
            return null;
        }
    }

    public function getGlassesModelDetail(Request $request, $glass_code){
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $result = $api->getGlassesModelDetail($token, $glass_code);

        if(!empty($result)){
            return $result;
        }else {
            return null;
        }
    }

    public function getGlassesOptionsList(Request $request, $glass_code){
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $glassOptions = json_decode($api->getGlassesOptionsList($token, $glass_code));
        $data = array();

        if(!empty($glassOptions)){
            $data["option_list"] = $glassOptions;
            return view("main.valuation.glass-options-list", $data);
        }
        return null;
    }

    public function getVehicleOptionalOptions(Request $request, $vehicle_id){
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $optionalOptions = json_decode($api->getVehicleOptionalOptions($token, $vehicle_id));
        $data = array();

        if(Helper::resultCheck($optionalOptions) === true){
            $data['disableIt'] = '';
            $data["optional_options_list"] = $optionalOptions;
            return view("main.valuation.optional-options", $data);
        }
        return Helper::resultCheck($optionalOptions);
    }

    /******* START VALUATION VERSION ******/

    public function getValuationVersionId(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $valuation_id = $request->input('valuation_id');
        $valuation_status = $request->input('valuation_status');
        $result = json_decode($api->getValuationVersionById($token, $valuation_id));

        $current_user_id = $request->session()->get('id');

        if(!isset($result->error)){
            $valuation_version_list = array();

            if(empty($result)){
                return null;
            }
            else{
                $access = "create";
                foreach($result as $version){
                    $version_user_id = $version->version_locker_user_id;
                    if($version_user_id == $current_user_id){
                        return 1;
                    }else{
                        if($version_user_id != 0 || $valuation_status == "Traded in" || $valuation_status == "Sold" ||
                                $valuation_status == "Listed For Auction" || $valuation_status == "Submit To Auction")
                        {
                            $access = "view";
                            $arr = array(
                                "valuation_id"=>$version->valuation_id,
                                "version"=>$version->version,
                                /*"version_creator"=>$version->version_creator,*/
                                "version_creator"=>$version->first_name." ".$version->last_name,
                                "create_date"=>date("d M Y", strtotime($version->create_date)),
                                "access" => $access
                            );
                            array_push($valuation_version_list, $arr);
                        }
                        elseif($version_user_id == 0){
                            $arr = array(
                                "valuation_id"=>$version->valuation_id,
                                "version"=>$version->version,
                                /*"version_creator"=>$version->version_creator,*/
                                "version_creator"=>$version->first_name." ".$version->last_name,
                                "create_date"=>date("d M Y", strtotime($version->create_date)),
                                "access" => $access
                            );
                            array_push($valuation_version_list, $arr);
                        }
                    }
                }
            }
            return json_encode($valuation_version_list);

        }

    }

    public function createValuationVersion(Request $request)
    {
        $token = $request->session()->get('token');
        $api = new \App\Library\APICaller();

        $valuation_id = $request->input('valuation_id');
        $valuation_status = $request->input('valuation_status');
        $from_version = $request->input('from_version');
        $access = $request->input("access");
        $params = array(
            'valuation_id'=>$valuation_id,
            'fromVersion'=>$from_version
        );
        if ($access == "create" || $valuation_status != "Traded in" || $valuation_status != "Sold" || $valuation_status != "Listed For Auction" || $valuation_status != "Submit To Auction") {
            $result = json_decode($api->createValuationVersionId($token,$params, $valuation_id));
        } else {
            $result = 1;
        }
        // $result = json_decode($api->createValuationVersionId($token,$params, $valuation_id));
        
        if(!isset($result->error)){
            return 1;
        }
        elseif(isset($result->error->detail) && $result->error->detail->code == -4){
            return $result->error->message;
        }
        return $result->error->message;
    }

    public function cancelValuationVersion(Request $request, $valuationId)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $result = json_decode($api->cancelValuationVersionId($token, null, (Int)$valuationId));
        if(Helper::resultCheck($result) == true){
            return 1;
        }
        return Helper::resultCheck($result);
    }

    /*public function releaseValuationVersion(Request $request, $valuationId)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $result = json_decode($api->releaseValuationVersionId($token, (int)$valuationId));
        if(!isset($result->error)){
            return 1;
        }else{
            return 0;
        }
    }*/
}
