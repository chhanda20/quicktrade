<?php
/**
 * Created by PhpStorm.
 * User: Son
 * Date: 4/10/2017
 * Time: 4:07 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileSettingController extends Controller
{
    public function __construct() {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }

    public function initProfileSetting(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $profile_settings = json_decode($api->getProfileSetting($token));
        $user_event_list = json_decode($api->getUserEvent($token));

        $data = array(
            "headers" => "Ricardo Luz Profile",
            "profile_settings" => $profile_settings,
            "user_event_list" => $user_event_list
        );

        $user_event = [];
        $data["user_event"] = '';
        if(isset($user_event_list)){
            foreach($user_event_list as $users){
                $user_event[] = [
                    "category_type"=>$users->category,
                    "event_list"=>$users->events
                ];
                $data["user_event"] = $user_event;
            }
        }
        return view('main.profile-setting', $data);
    }

    public function updateProfileSetting(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        /** GET INPUT FOR PASSWORD AND QUICK CODE **/
        $profile_password = $request->input("profile_password");

        $result_all = array();

        if (isset($profile_password)) {
            $params_password = array(
                "password" => $profile_password,
            );
            $result_password = json_decode($api->updateProfilePassword($token, $params_password));
            $result_all['result_password'] = $result_password;
        }

        $profile_quick_code = $request->input("profile_quick_code");
        if(isset($profile_quick_code)){
            $params_quick_code = array(
                "quick_code" => $profile_quick_code
            );
            $result_quick_code = json_decode($api->updateProfileQuickCode($token, $params_quick_code));
            $result_all['result_quick_code'] = $result_quick_code;
        }

        /** GET INPUT FOR PROFILE SETTING **/
        $email_notification_input = $request->input("email_notification");
        $email_notification = isset($email_notification_input) && $email_notification_input ? "1" : "0";

        $push_notification_input = $request->input("push_notification");
        $push_notification = isset($push_notification_input) && $push_notification_input ? "1" : "0";

        $params = array(
            "receive_email" => $email_notification,
            "receive_noti" => $push_notification,
        );
        $result_notification = $api->updateProfileConfig($token, $params);
        $result_all['result_notification'] = $result_notification;


        /** GET INPUT EVENT FOR PROFILE SETTING **/
        $notification_obj = $request->input("notification_obj");
        /*var_dump(\GuzzleHttp\json_decode($notification_obj)); die();*/

        $result_event = $api->updateUserEvent($token, json_decode($notification_obj));
        $result_all["event"] = $result_event;

        return json_encode($result_all);
    }

}