<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

use WindowsAzure\Common\ServicesBuilder;
//use WindowsAzure\Common\ServiceException;

use MicrosoftAzure\Storage\Blob\Models\CreateContainerOptions;
use MicrosoftAzure\Storage\Blob\Models\PublicAccessType;
use MicrosoftAzure\Storage\Common\ServiceException;

class VehiclePhotoController extends Controller
{
	public function __construct() {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }

    public function doDemoAzure() {
    	$azure = new \App\Library\AzureCloud();
    	echo var_dump($azure->getBlobsNameByVehicleId(16));
    	//$azure->uploadVehicleImage("sample", "sample");
        echo "<form method='POST' action='/azure/upload' enctype='multipart/form-data'>"
                .csrf_field()
                ."<input type='file' name='vehicle_image' />"
                ."<input type='submit' />"
            ."</form>";
    }

    public function uploadPhoto(Request $request){
        $azure = new \App\Library\AzureCloud();
        $file_input_field_name = 'vehicle_image';
        $image_vehicle_name = '16_2';

        return $azure->uploadVehicleImage($file_input_field_name, $image_vehicle_name);
    }


}