<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\Helper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ArrivingVehicleController extends Controller
{
    public function __construct() {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }

    public function viewArrivingList(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $channels = $request->channels;

        $data = array(
            "header" => "Arriving Vehicle",
            "breadcrumb" => "",
            "channels" => $channels
        );

        $limit = 20;
        $offset = 0;

        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "status" => [1,2,3,4,8],
            "transaction_type" => "buy"
        );

        $transactionStatuses = $api->getTypeCode($token, array("type" => "TRANSACTION_STATUS"));

        $transactionList = json_decode($api->getTransactions($token, $params));
        if(Helper::resultCheck($transactionList) === true){
            $collection = collect($transactionList)->map(function ($item, $key) use ($transactionStatuses) {
                foreach ($transactionStatuses as $transactionStatus){
                    if($item->status == $transactionStatus->code){
                        $item->status_value = $transactionStatus->value;
                        return $item;
                    }
                }
            });
            $data["transactionList"] = json_decode($collection);
        }
        
        $data["js"][] = "arriving-vehicle/viewArrivingList";

        return view("main.arriving-vehicle.arriving-list", $data);
    }

    public function getDataArrivingList(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $limit = $request->input("limit");
        $offset = $request->input("offset");

        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "status" => [1,2,3,4],
            "transaction_type" => "buy"
        );

        $transactionStatuses = $api->getTypeCode($token, array("type" => "TRANSACTION_STATUS"));

        $transactionList = json_decode($api->getTransactions($token, $params));
        //exit(var_dump(\GuzzleHttp\json_encode($params)));
        $data = array();
        if(Helper::resultCheck($transactionList) === true){
            $collection = collect($transactionList)->map(function ($item, $key) use ($transactionStatuses) {
                foreach ($transactionStatuses as $transactionStatus){
                    if($item->status == $transactionStatus->code){
                        $item->status_value = $transactionStatus->value;
                        return $item;
                    }
                }
            });
            $data["transactionList"] = json_decode($collection);
        }

        return view("main.arriving-vehicle.arriving-data-gird", $data);
    }

    public function getInspectionItems(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $valuation_id = $request->input("valuation_id");
        $valuation_version = $request->input("valuation_version");

        $data = array();
        $generalReconditionCost = $api->getGeneralReconditionCost($token, $valuation_id);
        $inspectionItems = json_decode($api->getInspectionItemsByValuationVersion($token, $valuation_id, $valuation_version));
        $generalRecost = 0;
        if(Helper::resultCheck($generalReconditionCost) === true){
            $generalRecost = $generalReconditionCost->generalReconditionCost;
            $data["generalReconditionCost"] = number_format($generalRecost, 2);
        }
        if(Helper::resultCheck($inspectionItems) === true){
            $collection = collect($inspectionItems)->map(function ($item, $key) use ($generalRecost) {
                if(!empty($item)){
                    $total = 0;
                    foreach ($item as $ins_item)
                    {
                        if(count($ins_item->items) > 0){
                            foreach ($ins_item->items as $it)
                            {
                                $total += $it->repair_cost;
                            }
                        }
                    }
                    if ($key == 'exterior_section') {
                        $total += $generalRecost;
                    }
                    $item["total_cost"] = $total;
                    return $item;
                }
                return $item;
            });
            $data["inspectionItems"] = json_decode($collection);
        }
        return view("main.arriving-vehicle.arriving-vehicle-inspection-items", $data);
    }

    public function inspect(Request $request, $transactionId)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $inspect = $api->transactionInspect($token, $transactionId);
        $inspect_error = NULL;
        if (!isset($inspect->id)) {
            $inspect_error = $inspect;
        }
        $transaction = json_decode($api->getTransactionById($token, $transactionId));
        $valuationId = $transaction->valuation_id;
        $version = $transaction->valuation_version;
        $valuationVersion = json_decode($api->valuationByVersion($token, $valuationId, $version));
        $valuationDetail = json_decode($api->getValuationDetail($token, $valuationVersion->id));
        $data['valuationDetail'] = $valuationDetail;
        $data['version'] = $version;
        $data['valuationId'] = $valuationVersion->id;
        $data['vehicleId'] = $valuationVersion->vehicle_id;
        $data['transactionId'] = $transactionId;
        $data['js'][] = 'arriving-vehicle/inspect';

        return view("main.arriving-vehicle.arriving-vehicle-inspect", $data)->with("error", $inspect_error);
    }

    public function viewAdditionalDetails(Request $request, $transaction_id)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        //get user id 
        $user_id = $request->session()->get('user_info')->id;
        $data["user_id"] = $user_id;
        //get transaction info
        $transaction_result = json_decode($api->getTransactionById($token, $transaction_id));

        $valuationId = $transaction_result->valuation_id;
        $valuationVersion = $transaction_result->valuation_version;
        //get inspection items
        $data[] = '';
        //$generalReconditionCost = $api->getGeneralReconditionCost($token, $valuationId);
        $generalReconditionCost = $api->valuationByVersion($token, $valuationId, $valuationVersion);
        $generalReconditionCost = json_decode($generalReconditionCost);
        $generalRecost = 0;
        if(Helper::resultCheck($generalReconditionCost) === true){
            $generalRecost = $generalReconditionCost->general_recondition_cost;
            $data["generalReconditionCost"] = number_format($generalRecost, 2);
        }
        $inspectionItems = json_decode($api->getInspectionItemsByValuationVersion($token, $valuationId, $valuationVersion));
        if(Helper::resultCheck($inspectionItems) === true){
            $collection = collect($inspectionItems)->map(function ($item, $key) use ($generalRecost) {
                if(!empty($item)){
                    $total = 0;
                    foreach ($item as $ins_item) {
                        if(count($ins_item->items) > 0){
                            foreach ($ins_item->items as $it) {
                                $total += $it->repair_cost;
                            }
                        }
                    }
                    if ($key == 'exterior_section') {
                        $total += $generalRecost;
                    }
                    $item["total_cost"] = $total;
                    return $item;
                }
                return $item;
            });
            $collectionItems = json_decode($collection);
            $data["inspectionItems"] = $collectionItems;
        }
        $damageTotal = 0;
        if (isset($collectionItems->exterior_section->total_cost)) {
            $damageTotal += $collectionItems->exterior_section->total_cost;
        }
        if (isset($collectionItems->interior_section->total_cost)) {
            $damageTotal += $collectionItems->interior_section->total_cost;
        }
        if (isset($collectionItems->mechanical_section->total_cost)) {
            $damageTotal += $collectionItems->mechanical_section->total_cost;
        }
        $data["damageTotal"] = $damageTotal;
        $data['js'][] = 'arriving-vehicle/newDisputeItem';
        $data['transaction_id'] = $transaction_id;
        $data['valuation_id'] = $valuationId;
        $data['valuation_version'] = $valuationVersion;
        //end get inspection items
        return view("main.arriving-vehicle.arriving-vehicle-additional-detail", $data);
    }

    public function createLodgeDispute(Request $request)
    {
        $token = $request->session()->get("token");
        $transaction_id = $request->input("transaction_id");
        $inspection_items_id = $request->input("inspection_items_id");
        $user_id = $request->input("user_id");
        if(!isset($inspection_items_id)) {
            $inspection_items_id = [];
        }
        $dispute_items = $request->input("dispute_inspect_items");
        $dispute_items_for_api = [];
        if(!isset($dispute_items)) {
            $dispute_items = [];
        } else {
            //convert to adapt with api
            foreach ($dispute_items as $key => $value) {
                $dispute_item = new \stdClass;
                if (isset($value['item_id'])) {
                    $dispute_item->inspection_item_id = $value['item_id'];
                } 
                if (isset($value['item_cost'])) {
                    $dispute_item->repair_cost = $value['item_cost'];
                } 
                if (isset($value['item_note'])) {
                    $dispute_item->note = $value['item_note'];
                }
                array_push($dispute_items_for_api, $dispute_item);
            }
        }

        $first_name = $request->input("dispute_contact_first_name");
        $last_name = $request->input("dispute_contact_last_name");
        $email = $request->input("dispute_contact_email");
        $phone = $request->input("dispute_contact_phone");
        $comment = $request->input("dispute_comment");

        $general_reconditioning = $request->input("general_reconditioning");

        $params = ["comment" => $comment,
                "inspectionItems" => $inspection_items_id,
                "disputeItems" => $dispute_items_for_api,
                "contact_person_first_name" => $first_name,
                "contact_person_last_name" => $last_name,
                "contact_person_email" => $email,
                "contact_person_phone" => $phone,
                "general_recondition_cost" => $general_reconditioning == "true" ? true : false];
             
        $api = new \App\Library\APICaller();
        $result_create_dispute = json_decode($api->createDispute($token, $transaction_id, $params));
        if (isset($result_create_dispute->id)) {
            $dispute_id = $result_create_dispute->id;
            foreach ($dispute_items as $key => $value) {
                $inspect_id = $value["item_id"];
                $this->uploadTemporaryPhotoToAzure($dispute_id, $inspect_id, $user_id);
            }
            return json_encode($result_create_dispute);
        } else {
            return json_encode($result_create_dispute);
        }

    }

    public function clearDisputeInspectionPhotoTemporary($user_id) 
    {
        //return json_encode(File::cleanDirectory(Storage::disk('uploads_dispute_inspection')->getDriver()->getAdapter()->getPathPrefix()));
        $file = glob('images_tmp/dispute_inspect_item_photo_tmp/*_'.$user_id.'.*');
        
        $deleted_file = [];
        foreach ($file as $key => $value) {
            array_push($deleted_file, basename($value));
        }
        return json_encode(Storage::disk('uploads_dispute_inspection')->delete($deleted_file));
    }   

    public function uploadTemporaryPhotoToAzure($dispute_id, $inspection_item_id, $user_id)
    {
        $i = 1;
        while($i<4){
            $image_name = $inspection_item_id."_".$i."_".$user_id.".jpg";
            LOG::debug("ADD INSPECTION ITEM - IMAGE NAME: ".$image_name);
            if(Storage::disk('uploads_dispute_inspection')->exists($image_name)){
                $image_content = Storage::disk('uploads_dispute_inspection')->get($image_name);
                $new_image_name = $dispute_id."_".$inspection_item_id."_".$i.".jpg";
                $new_image_name = basename($new_image_name);
                LOG::debug("ADD INSPECTION ITEM - new image name: ".$new_image_name);
                $azure = new \App\Library\AzureCloud();
                $image = $azure->uploadBlobImage($image_content, $new_image_name, "dispute-item");
                if($image){
                    LOG::debug("ADD INSPECTION ITEM - added successfully");
                    Storage::disk('uploads_dispute_inspection')->delete($image_name);
                }
            }
        $i++;
        }
    }

    public function uploadDisputeItemPhotoToAzure(Request $request)
    {
        $image = $_FILES["arriving-vehicle-dispute-inspect-photo"]["tmp_name"];
        $imageFileType = "jpg";
        $dispute_id = null;
        $inspection_item_id = $request->input("arriving_vehicle_item");
        $sequence = null;
        $image_name = $dispute_id . "_" . $inspection_item_id . "_" . $sequence.".".$imageFileType;
        if ($image != null) {
            $fileContent = file_get_contents($image);
            $image_content = $fileContent;
            $image_name = basename($image_name);
            $azure = new \App\Library\AzureCloud();
            $image = $azure->uploadBlobImage($image_content, $image_name, "dispute-item");

            if ($image) {
                return $image;
            }

        }
    }

    public function uploadDisputeItemPhotoTemporary(Request $request)
    {
        //create image name
        //$image_file_type = pathinfo($_FILES["arriving_vehicle_dispute_inspect_item_photo"]["name"],PATHINFO_EXTENSION);
        $image_file_type = "jpg";
        $inspection_item_id = $request->input("dispute_item_id");
        $sequence = $request->input("sequence");
        $user_id = $request->input("user_id");
        $image_name = $inspection_item_id . "_" . $sequence."_".$user_id.".".$image_file_type;
        
        //upload image to tmp folder
        /*
        $upload_dir = $_SERVER['DOCUMENT_ROOT']."/images_tmp/dispute_inspect_item_photo_tmp/";
        $file_tmp = $upload_dir . $image_name;

        if(move_uploaded_file($_FILES["arriving_vehicle_dispute_inspect_item_photo"]["tmp_name"], $file_tmp)) {
            return "/images_tmp/dispute_inspect_item_photo_tmp/".$image_name;
        }
        */

        //upload image to tmp folder
        $file = $_FILES["arriving_vehicle_dispute_inspect_item_photo"]["tmp_name"];
        $fileContent = file_get_contents($file);
        Storage::disk('uploads_dispute_inspection')->put($image_name, $fileContent);
        if(Storage::disk('uploads_dispute_inspection')->exists($image_name)) {
            //return Storage::disk('uploads_inspection')->url($image_name);
            return '/images_tmp/dispute_inspect_item_photo_tmp/'.$image_name."?".rand();
        }

    }

    public function getInspectItem(Request $request, $valuationId, $valuationVersion)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $data[] = '';

        $generalReconditionCost = $api->getGeneralReconditionCost($token, $valuationId);
        $generalRecost = 0;
        if(Helper::resultCheck($generalReconditionCost) === true){
            $generalRecost = $generalReconditionCost->generalReconditionCost;
            $data["generalReconditionCost"] = number_format($generalRecost, 2);
        }
        $inspectionItems = json_decode($api->getInspectionItemsByValuationVersion($token, $valuationId, $valuationVersion));
        if(Helper::resultCheck($inspectionItems) === true){
            $collection = collect($inspectionItems)->map(function ($item, $key) use ($generalRecost) {
                if(!empty($item)){
                    $total = 0;
                    foreach ($item as $ins_item) {
                        if(count($ins_item->items) > 0){
                            foreach ($ins_item->items as $it) {
                                $total += $it->repair_cost;
                            }
                        }
                    }
                    if ($key == 'exterior_section') {
                        $total += $generalRecost;
                    }
                    $item["total_cost"] = $total;
                    return $item;
                }
                return $item;
            });
            $collectionItems = json_decode($collection);
            $data["inspectionItems"] = $collectionItems;
        }
        $damageTotal = 0;
        if (isset($collectionItems->exterior_section->total_cost)) {
            $damageTotal += $collectionItems->exterior_section->total_cost;
        }
        if (isset($collectionItems->interior_section->total_cost)) {
            $damageTotal += $collectionItems->interior_section->total_cost;
        }
        if (isset($collectionItems->mechanical_section->total_cost)) {
            $damageTotal += $collectionItems->mechanical_section->total_cost;
        }
        $data["damageTotal"] = $damageTotal;
        return view("main.arriving-vehicle.inspect-item", $data);
    }

    public function getOptionExtra(Request $request, $vehicleId)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $data = [];
        $optionalOptions = json_decode($api->getVehicleOptionalOptions($token, $vehicleId));
        if(!isset($optionalOptions->error) && !empty($optionalOptions)){
            $optionalCollection = collect(get_object_vars($optionalOptions));
            $data["optionalCollection"] = $optionalCollection;
        }

        $extraOptions = json_decode($api->getVehicleExtrasOptions($token, $vehicleId));
        if(!isset($extraOptions->error) && !empty($extraOptions)) {
            $data["extraOptions"] = $extraOptions;
        }

        return view("main.arriving-vehicle.option-extra", $data);
    }

    public function accept(Request $request, $transactionId)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $res = json_decode($api->acceptTransaction($token, $transactionId));

        Log::debug("accept - res: ".json_encode($res));

        if (isset($res->error)) {
            return 0;
        }
        return 1;
    }

}
