<?php

namespace App\Http\Controllers;

use App\Library\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class RoleController extends Controller
{
    public function __construct() {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }


    public function initRoleForm(Request $request, $type, $roleId=0)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $permissions = json_decode($api->getPermission($token));
        $channels = $request->channels;

        $menu_list = json_decode($api->getMenuItems($token));
        if(isset($menu_list->error)){
            Session::flash("menu_error_message", $menu_list->error->code);
            return redirect("/dealership/roles");
        }
        $menu_items_list = $menu_list->menuList;

        $data = array(
            "header" => "Configure",
            "breadcrumb" => "Roles",
            "channels" => $channels,
            "permissions" => $permissions,
            "menu_list" => $menu_items_list
        );

        $menu_group = [];
        $data["menu_group"]="";
        if(isset($menu_items_list)){
            foreach($menu_items_list as $menu){
                if($menu->site_menu == "dealership"){
                    foreach($menu->group as $group_menu){
                        $menu_group[] = [
                            "group_name" => $group_menu->group_name,
                            "menu_items" => $group_menu->menu_items
                        ];
                        $data["menu_group"] = $menu_group;
                    }
                }
            }
        }



        if($type == 'new'){
            $data["action"] = "New";
        }else if ($type == 'edit'){
            $data["action"] =  "Edit";
            $result = json_decode($api->getRoleById($token, $roleId));
            if(!isset($result->error)){
                $data["role_detail"] = $result;
            }
            $permission_result = json_decode($api->getRoleByIdOfPermission($token, $roleId));
            if(!isset($permission_result->error)){
                $permissionArrId = [];
                foreach($permission_result as $result){
                    array_push($permissionArrId, $result->id);
                }
                $data["permission_detail"] = $permissionArrId;
            }

            $menu_role_result = json_decode($api->getMenuByRoleId($token, $roleId));
            if(!isset($menu_role_result->error)){
                $menu_role_list = $menu_role_result->menuList;
                $menu_group = [];
                foreach($menu_role_list as $key=>$value){
                    foreach($value->group as $group_menu){
                        foreach($group_menu->menu_items as $menu_item){
                            array_push($menu_group, $menu_item->code);
                        }
                    }
                }
                Log::debug("menu_group ".json_encode($menu_group));
                $data["menu_role_detail"] = $menu_group;
            }
        }
        $data["js"][] = "role/createNewRole";
        return view("main.dealership.create-new-role", $data);
    }

    public function createNewRole(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $role_name = $request->input("role_name");
        $permission_list = $request->input("permissions_list");
        $permissionIntegerIDs = array_map('intval', $permission_list);
        $menu_id = $request->input("menu_items_list");
        $params = array(
            "name" => $role_name,
            "permissions" => $permissionIntegerIDs,
        );

        $params_menu = array(
            "menu_id" => $menu_id
        );
        $result = json_decode($api->createNewRole($token, $params));

        if(!isset($result->error)){
            $params_menu["role_id"] = $result->id;
            $result_menu = json_decode($api->setMenuAccessForRole($token, $params_menu));
            if(!isset($result_menu->error)){
                return redirect ("/dealership/roles");
            }
        }
        Session::flash("error_message", $result->error->code);
        return $this->initRoleForm($request, "new");
    }

    public function updateNewRole(Request $request, $roleId)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $role_name = $request->input("role_name");
        $permission_list = $request->input("permissions_list");
        $permissionIntegerIDs = array_map('intval', $permission_list);

        $menu_id = $request->input("menu_items_list");
        $role_id = $request->input("mo_role_id");

        $params = array(
            "name" => $role_name,
            "permissions" => $permissionIntegerIDs
        );

        $params_menu = array(
            "role_id" => $role_id,
            "menu_id" => $menu_id
        );

        $result = json_decode($api->updateNewRole($token, $params, $roleId));
        if(!isset($result->error)){
            Log::debug("updateNewRole - params ".json_encode($params_menu));
            $result_menu = json_decode($api->setMenuAccessForRole($token, $params_menu));
            if(!isset($result_menu->error)){
                /** GET MENUS and PUT in SESSION  **/
                /*$menus = json_decode($api->getMenuByRoleId($token, $roleId));
                if(!isset($menus->error)){
                    $collection = Helper::updateDealershipMenuAccessSession($menus->menuList);
                    Log::debug("updateNewRole - collection: ".$collection);
                    $request->session()->forget('dealership_menus_sidebar');
                    $request->session()->put('dealership_menus_sidebar', $collection);
                }*/

                return redirect ("/dealership/roles");
            }
        }
        Session::flash("error_message", $result->error->code);
        return $this->initRoleForm($request, "edit", $roleId);
    }

    public function deleteRole(Request $request, $role_id)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $result = json_decode($api->deleteRole($token, $role_id));
        if(!isset($result->error))
        {
            return 1;
        }else{
            return 0;
        }
    }

    public function viewRoles(Request $request)
    {
        $channels = $request->channels;
        $data = array(
            "header" => "Configure",
            "breadcrumb" => "Roles",
            "channels" => $channels
        );

        return view('main.dealership.roles', $data);
    }

    public function getRolesList(Request $request)
    {
        $token = $request->session()->get("token");
        $search_txt = $request->input("mo_txt_search");
        $offset = $request->input("offset");

        $api = new \App\Library\APICaller();

        $params = array(
            "limit" => 20,
            "offset" => $offset,
            "query" => $search_txt
        );

        $rolesList = json_decode($api->findRolesList($token, $params));
    
        if (!isset($rolesList->error)) {
            $roles_list_response_json = array('data' => array ());
            if(!empty($rolesList->roleList)){
                foreach ($rolesList->roleList as $key => $value) {
                    $arr = array(
                        0 => $value->id,
                        1 => $value->name
                    );
                    array_push($roles_list_response_json["data"], $arr);
                }
            }

            if(!empty($rolesList->list_size)){
                $roles_list_response_json["count"] = $rolesList->list_size->count;
            }

            if (!$roles_list_response_json["data"]) {
                return null;
            }
            return json_encode($roles_list_response_json);

        } else {
            return null;
        }
    }

//    public function setMenuAccessRole(Request $request)
//    {
//        $api = new \App\Library\APICaller();
//        $token = $request->session()->get("token");
//        $menu_id = $request->input("menu_items_list");
//        $menu_id = array_map("intval", $menu_id);
//        $role_id = $request->input("mo_role_id");
//        $params = array(
//            "role_id" => $role_id,
//            "menu_id" => $menu_id
//        );
//        $result = json_decode($api->setMenuAccessForRole($token, $params));
//        if(!isset($result->error)){
//            return 1;
//        }return 0;
//    }

}
