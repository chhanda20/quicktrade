<?php

namespace App\Http\Controllers;

use App\Library\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TransactionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('handling-channel');
        $this->middleware('get-menu-access');
    }

    /**
     * Return List of Transaction (without data)
     *
     * @route /transaction/view-transactions
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    
    public function viewTransactions(Request $request, $type = "current")
    {
        $channels = $request->channels;
        $data = array(
            "header" => "Transaction",
            "breadcrumb" => $type,
            "type" => $type,
            "channels" => $channels
        );

        $data['tabs'] = [
            [
                'id' => 'all',
                'name' => 'All'
            ],
            [
                'id' => 'bought',
                'name' => 'Bought'
            ],
            [
                'id' => 'sold',
                'name' => 'Sold'
            ]
        ];

        $data['js'][] = 'mo-admin/transaction/viewTransactions-filter';

        $data['js'][] = 'transaction/viewTransactions';

        return view('main.transaction.transaction-main', $data);
    }

    /**
     * Return List Data of Transaction
     *
     * @route /transaction/get-transactions
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function getTransactions(Request $request, $type, $transactionType = "")
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        /*$limit = $request->input('limit');*/
        $offset = $request->input('offset');
        $active = (bool)$request->input('active');
        $status_datatable = $request->input('status');
        $year = $request->input("year_range");
        $makeInfo = $request->input("makeInfo");

        switch($transactionType){
            case "bought":
                $transactionType = "buy";
                break;
            case "sold":
                $transactionType = "sell";
                break;
            default:
                $transactionType = "";
                break;
        }

        switch($type){
            case "complete":
                $status = [5];
                break;
            default:
                $status = [];
                break;
        }

        if(Helper::arrayCheck($status_datatable)){
            $status = $status_datatable;
        }

        Log::debug("getTransactions - active: ".$active);
        Log::debug("getTransactions - type: ".$type);

        if($active === true && $type != "complete"){//required action
            Log::debug("getTransactions - inside");
            if($transactionType == "sell"){
                $status = [20];
            }else if($transactionType == "buy"){
                $status = [8,25];
            }else {
                $status = [20,8,25];
            }

            Log::debug("getTransactions - status: ".json_encode($status));

            if(Helper::arrayCheck($status_datatable)){
                $status_required_action = [20,8,25];
                $status = array_intersect($status_datatable, $status_required_action);
                if(count($status) == 0){
                    $status = [0];
                }
            }

        }

        $params = array(
            "limit" => 20,
            "offset" => $offset,
            "transaction_type" => $transactionType,
            "status" => array_values($status)
        );

        if(Helper::arrayCheck($year)){
            $params["year"] = $year;
        }

        if(Helper::arrayCheck($makeInfo)){
            $params["makeInfo"] = $makeInfo;
        }

        Log::debug("getTransactions - params: ".json_encode($params));

        $transactionList = json_decode($api->getTransactions($token, $params));
        if(Helper::resultCheck($transactionList) === true){
            $transactionStatuses = $api->getTypeCode($token, array("type" => "TRANSACTION_STATUS"));
            if(Helper::resultCheck($transactionList) === true){
                $collection = collect($transactionList)->map(function ($item, $key) use ($transactionStatuses, $active, $transactionType) {
                    $item->status_value = "";
                    foreach ($transactionStatuses as $transactionStatus){
                        /** ALTER STATUS VALUE **/
                        switch($item->status) {
                            case $transactionStatus->code:
                                $item->status_value = $transactionStatus->value;
                                break;
                            case 20:
                                $item->status_value = "Preparing Vehicle";
                                break;
                            /*case 25:
                                $item->status_value = "Arrived";
                                break;*/
                            case 26:
                                $item->status_value = "Awaiting Document";
                                break;
                            case 27:
                                $item->status_value = "Documents Received";
                                break;
                            case 28:
                                $item->status_value = "Waiting to return";
                                break;
                        }
                    }
                    if($active === true && $transactionType == ""){
                        /** FILTER REQUIRED ACTION **/
                        switch (strtolower($item->transaction_type)){
                            case "in":
                                if(in_array($item->status, array(8,25))){
                                    return $item;
                                }
                                break;
                            case "out":
                                if($item->status == 20){
                                    return $item;
                                }
                                break;

                        }
                    }else{
                        return $item;
                    }
                });
                $transactionList = json_decode($collection);
            }

            $transactions_list_response_json = array('data' => array ());
            foreach ($transactionList as $transaction) {
                if(!empty($transaction)){
                    $arr = array(
                        0 => $transaction->id,
                        1 => $transaction->transaction_type,
                        2 => $transaction->rid,
                        3 => $transaction->vehicle_name,
                        4 => $transaction->registration_number,
                        5 => $transaction->transaction_type == "In" ?  $transaction->location_name_buyer : $transaction->location_name_seller,
                        6 => $transaction->status_value,
                        7 => $transaction->status
                    );
                    array_push($transactions_list_response_json["data"], $arr);
                }
            }

            $result = json_decode($api->getTransactionCount($token, array("transaction_type" => $transactionType, "status" => $status)));
            if(Helper::resultCheck($result)){
                $transactions_list_response_json["count"] = $result->count;
            }

            if (!$transactions_list_response_json["data"]) {
                return null;
            }
            return json_encode($transactions_list_response_json);
        }else{
            return null;
        }
    }

    public function readyForTransport(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $transactionId = $request->input('transaction_id');

        Log::debug("readyForTransport - transactionId: ".json_encode($transactionId));

        $result = $api->transactionReadyForTransport($token, $transactionId);
        if(Helper::resultCheck($result) === true){
            return 1;
        }
        return Helper::resultCheck($result);
    }

    public function inspect(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $transactionId = $request->input('transaction_id');

        Log::debug("inspect - transactionId: ".json_encode($transactionId));

        $result = $api->transactionInspect($token, $transactionId);
        if(Helper::resultCheck($result) === true){
            return 1;
        }
        return Helper::resultCheck($result);
    }

    public function transactionDetails(Request $request, $type, $transactionId)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $channels = $request->channels;
        $data = array(
            "header" => "Transaction",
            "breadcrumb" => $type,
            "type" => $type,
            "channels" => $channels
        );

        $data["transactionDetails"] = "";
        $transactionDetails = json_decode($api->getTransactionById($token, $transactionId));
        $transactionStatuses = $api->getTypeCode($token, array("type" => "TRANSACTION_STATUS"));
        if(Helper::resultCheck($transactionDetails) === true){
            $transactionActionButton = "";
            $seller_buyer = strtolower($transactionDetails->transaction_type);
            $data["seller_buyer"] = $seller_buyer;
            switch ($transactionDetails->status){
                case 20://preparing vehicle
                    if($seller_buyer == 'out'){
                        $transactionActionButton = "vehicle ready";
                    }
                    break;
                case 3://arrived
                    if($seller_buyer == 'in'){
                        $transactionActionButton = "inspect";
                    }
                    break;
                case 8://in inspection
                    if($seller_buyer == 'in'){
                        $transactionActionButton = "continue inspection";
                    }
                    break;
            }

            Log::debug("transactionDetails: ".$transactionDetails->status);

            $status_value = Helper::getTypeName($transactionStatuses, $transactionDetails->status);

            switch($transactionDetails->status){
                case 20:
                    $status_value= "Preparing Vehicle";
                    break;
                case 26:
                    $status_value = "Awaiting Document";
                    break;
                case 27:
                    $status_value = "Documents Received";
                    break;
                case 28:
                    $status_value = "Waiting to return";
                    break;
            }

            $transactionDetails->status_value = $status_value;

            //$transactionDetails->status_value = Helper::getTypeName($transactionStatuses, $transactionDetails->status);
            $transactionDetails->action_button = $transactionActionButton;
            $total_price = $transactionDetails->buy_vehicle_price + $transactionDetails->buy_mo_fee;
            if($seller_buyer == 'out'){
                $total_price = $transactionDetails->sell_vehicle_price - $transactionDetails->sell_mo_fee;
            }
            if($seller_buyer == "in"){
                $total_price += $transactionDetails->buy_transport;
            }
            /*$total_price += $transactionDetails->buy_transport;*/

            if($total_price != null){
                $transactionDetails->total_price = number_format($total_price);
            }

            /** FORMAT SELLER PRICE **/
            $transactionDetails->sell_vehicle_price_format = 0;
            $transactionDetails->sell_mo_fee_format = 0;
            $transactionDetails->sell_dispute_adjust_format = 0;
            if($transactionDetails->sell_vehicle_price != null){
                $transactionDetails->sell_vehicle_price_format = number_format($transactionDetails->sell_vehicle_price);
            }
            if($transactionDetails->sell_mo_fee != null){
                $transactionDetails->sell_mo_fee_format = number_format($transactionDetails->sell_mo_fee);
            }
            if($transactionDetails->sell_dispute_adjust != null){
                $transactionDetails->sell_dispute_adjust_format = number_format($transactionDetails->sell_dispute_adjust);
            }

            /** FORMAT BUYER PRICE **/
            $transactionDetails->buy_vehicle_price_format = 0;
            $transactionDetails->buy_transport_format = 0;
            $transactionDetails->buy_mo_fee_format = 0;
            $transactionDetails->buy_payment_made_format = 0;
            $transactionDetails->buy_dispute_adjust_format = 0;
            if($transactionDetails->buy_vehicle_price != null){
                $transactionDetails->buy_vehicle_price_format = number_format($transactionDetails->buy_vehicle_price);
            }
            if($transactionDetails->buy_transport != null){
                $transactionDetails->buy_transport_format = number_format($transactionDetails->buy_transport);
            }
            if($transactionDetails->buy_mo_fee != null){
                $transactionDetails->buy_mo_fee_format = number_format($transactionDetails->buy_mo_fee);
            }
            if($transactionDetails->buy_payment_made != null){
                $transactionDetails->buy_payment_made_format = number_format($transactionDetails->buy_payment_made);
            }
            if($transactionDetails->buy_dispute_adjust != null){
                $transactionDetails->buy_dispute_adjust_format = number_format($transactionDetails->buy_dispute_adjust);
            }

            /** FORMAT COMMON PRICE - SELLER_BUYER **/
            $transactionDetails->vehicle_price_format = $transactionDetails->buy_vehicle_price_format;
            if($seller_buyer == 'out'){
                $transactionDetails->vehicle_price_format = $transactionDetails->sell_vehicle_price_format;
            }
            $transactionDetails->mo_fee_format = $transactionDetails->buy_mo_fee_format;
            if($seller_buyer == 'out'){
                $transactionDetails->mo_fee_format = $transactionDetails->sell_mo_fee_format;
            }
            $transactionDetails->dispute_adjust_format = $transactionDetails->buy_dispute_adjust_format;
            if($seller_buyer == 'out'){
                $transactionDetails->mo_fee_format = $transactionDetails->sell_mo_fee_format;
            }
            $transactionDetails->total_after_resolve_dispute_format = 0;
            $total_after_resolve_dispute = 0;
            if($total_price != null && $total_price > 0){
                $total_after_resolve_dispute = number_format($total_price - $transactionDetails->buy_dispute_adjust);
                if($seller_buyer == 'out'){
                    $total_after_resolve_dispute = number_format($total_price - $transactionDetails->sell_dispute_adjust);
                }
            }
            $transactionDetails->total_after_resolve_dispute_format = $total_after_resolve_dispute;

            /** GET TRANSACTION DOCUMENTS **/
            $documents = json_decode($api->getTransactionDocuments($token, $transactionId));
            if(Helper::resultCheck($documents) === true){
                $data["documents"] = $documents->uploadedDocuments;
            }

            /** GET TRANSACTION DOCUMENTS TYPE **/
            $params = array(
                "types" => ["transport-fee", "fee", "credit-note-cost-variance", "credit-note-return-vehicle", "payment-receipt"]
            );
            $document_types = json_decode($api->getDocumentOfTransaction($token, $params, $transactionId));
            if(Helper::resultCheck($document_types) === true){
                foreach($document_types as $key=>$value){
                    if($value->document_type == "transport-fee"){
                        $data["invoice_buyer_document"] = $value->document_uploaded;
                    }
                    if($value->document_type == "fee" || $value->document_type == "transport-fee-2"){
                        $data["invoice_seller_document"] = $value->document_uploaded;
                    }
                    if($value->document_type == "credit-note-cost-variance" || $value->document_type == "credit-note-return-vehicle"){
                        $data["credit_note_document"] = $value->document_uploaded;
                    }
                }
                $data["document_types"] = $document_types;
            }

            $data['js'][] = 'transaction/jquery.custom-file-input';

            $data['js'][] = 'transaction/transactionDetails';

            $data["transactionDetails"] = $transactionDetails;
        }

        return view('main.transaction.transaction-details', $data);
    }

    public function uploadDocument(Request $request, $transactionId, $documentCode)
    {
        $extension = $request->input("file_extension");
        $api = new \App\Library\APICaller();
        $azure = new \App\Library\AzureCloud();
        $token = $request->session()->get("token");

        switch ($documentCode){
            case "transfer":
                $documentName = "vehicle_transfer_form";
                $file_input_field_name = "vehicle_transfer_file";
                break;
            case "transfer_additional":
                $documentName = "vehicle_transfer_additional";
                $file_input_field_name = "vehicle_transfer_file_additional";
                break;
            case "dispute":
                $documentName = "invoice_vehicle_minus_dispute";
                $file_input_field_name = "invoice_dispute_cost_file";
                break;
            case "ppsr_financial":
                $documentName = "ppsr_financial_payout";
                $file_input_field_name = "ppsr_finance_payout_file";
                break;
            default:
                $documentName = "";
                $file_input_field_name = "";
                break;
        }

        if(!empty($file_input_field_name) && !empty($documentName)){
            $doc_name = $transactionId."_".$documentName.".".$extension;
            Log::debug("uploadDocument - file_input_field_name: ".$file_input_field_name);
            Log::debug("uploadDocument - transaction_document_name: ".$doc_name);
            $res = $azure->uploadTransactionDocument($file_input_field_name, $doc_name);
            if(!empty($res)){
                $result = json_decode($api->saveTransactionDocuments($token, $transactionId, array("document_name"=>[$doc_name])));
                Log::debug("uploadDocument - saveTransactionDocuments: ".Helper::resultCheck($result));
                return $res;
            }
        }
        return null;
    }
}
