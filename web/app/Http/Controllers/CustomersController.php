<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library;

class CustomersController extends Controller
{
	public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }

    public function viewCustomers(Request $request) 
    {
        $channels = $request->channels;
        $data = array(
            "header"      => "Customers",
            "channels" => $channels
        );
    	return view('main.customers', $data);
    }

    public function getDataOfCustomers(Request $request)
    {
    	$api = new \App\Library\APICaller();
    	$token = $request->session()->get("token");
    	$limit = 20; 
    	$offset = $request->input("offset");

        //$offset = 0;

	  	//$txt =	'{
			//   "data": [
			//     [
			//       "Tiger Nixon",
			//       "System Architect",
			//       "Edinburgh",
			//       "5421",
			//       "2011/04/25",
			//       "$320,800"
			//     ],
			//     [
			//       "Garrett Winters",
			//       "Accountant",
			//       "Tokyo",
			//       "8422",
			//       "2011/07/25",
			//       "$170,750"
			//     ]
			//   ]
			// }';

        $query = $request->input("mo_txt_search");
        //$query = null;


    	$customers_data = json_decode($api->getContacts($token, $limit, $offset, $query));

        if(!isset($customers_data->error)) {

        	$customers_jsons_data = '{"data": [';
        	$i = 1;
        	$arr_size = sizeof($customers_data);

        	foreach ($customers_data as $key => $value) {

        		if($i < $arr_size) {
    	    		$customers_jsons_data .= '["' . $value->id . '", "' . $value->first_name . '", "' . $value->last_name . '", "' . $value->company_name . '", "' . $value->email . '", "' . $value->mobile_phone . '", "' . $value->location_name . '", "' . $value->salesperson_first_name ." ". $value->salesperson_last_name . '"],';

        		} else {
        			$customers_jsons_data .= '["' . $value->id . '", "' . $value->first_name . '", "' . $value->last_name . '", "' . $value->company_name . '", "' . $value->email . '", "' . $value->mobile_phone . '", "' . $value->location_name . '", "' . $value->salesperson_first_name ." ". $value->salesperson_last_name . '"]';
        		}
        		$i++;
        		
        	}

        	$customers_jsons_data .= ']}';
            
            $customers_jsons_data = json_decode($customers_jsons_data);

            $length = $api->getLengthContacts($token, $query);


            $customers_jsons_data->count = json_decode($length)->count;


        	echo json_encode($customers_jsons_data);
        } else {
            $customers_jsons_data = '{"data": [';
            $customers_jsons_data .= '["' . null . '", "' . null . '", "' . null . '", "' . null . '", "' . null . '", "' . null . '", "' . null . '", "' . null . '"],';
            $customers_jsons_data .= ']}';

            echo $customers_jsons_data;
        }
    }

    public function initAddCustomerForm(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        //$params = array("type" => "TITLE_TYPE");

        $titles = $api->getTypeCode($token, array("type" => "TITLE_TYPE"));

        $genders = $api->getTypeCode($token, array("type" => "GENDER_TYPE"));

        $contact_method = $api->getTypeCode($token, array("type" => "PREFERED_CONTACT_METHOD"));

        $vehicle_type = $api->getTypeCode($token, array("type" => "INTERESTED_VEHICLE_TYPE"));

        $country_list = $api->getCountry($token);

        $privacy_flag = $api->getTypeCode($token, array("type" => "PRIVACY_FLAG"));

        /** GET CHANNELS from middleware **/
        $channels = $request->channels;

        $data = array(
            "header" => "Customers",
            "breadcrumb" => "Add Customer",
            "titles" => $titles,
            "genders" => $genders,
            "contact_method" => $contact_method,
            "vehicle_type" => $vehicle_type,
            "channels" => $channels,
            "country_list" => $country_list,
            "privacy_flag" => $privacy_flag
            );

        return view('main.create-customer', $data);

    }

    /* ADD CUSTOMER */
    public function addCustomer(Request $request) 
    {
    	$title = $request->input("title");
        if (!isset($title)) {
            $title = 0;
        }
        $gender = $request->input("gender");
        if (!isset($gender)) {
            $gender = 0;
        }

        $first_name = $request->input("first_name");
        $last_name = $request->input("last_name");
        $privacy_flag = $request->input("privacy");
        $private_last_update_time = null;
        $company_name = $request->input("company");
        $company_type = null;
        $ABN = null;
        $ACN = null;
        $tax_type = null;
        $address1 = $request->input("address1");
        $address2 = $request->input("address2");
        $suburb = $request->input("suburb");
        $state = $request->input("state");
        $postcode = $request->input("postcode");
        $country = $request->input("country");
        if($country == null){
            $country = $request->input("input_country_id");
        }
        $postal_address1 = null;
        $postal_address2 = null;
        $postal_state = null;
        $postal_postcode = null;
        $postal_country = null;
        $email = $request->input("email");
        $email_alternate = null;
        $mobile_phone = $request->input("mobile");
        $home_phone = null;
        $work_phone = null;
        $fax_number = null;
        $preferred_contact_method = $request->input("contact_method");
        
        if (!isset($preferred_contact_method)) {
            $preferred_contact_method = 0;
        }

        $driver_license_number = null;
        $dealer_license_number = null;
        $fleet_number = null;
        $bank_branch_name = null;
        $bank_account_name = null;
        $bank_bsb_number = null;
        $bank_account_number = null;
        $note = $request->input("note");
        $prospect_level = null;
        $interested_vehicle = $request->input("new_used_vehicle");

        if(!isset($interested_vehicle)) {
            $interested_vehicle = 0;
        }

        $params = array (
            
        	"title" => $title,
        	"gender" => $gender,
        	"first_name" => $first_name,
        	"last_name" => $last_name,
        	"company_name" => $company_name,
            //"company_type" => $company_type,
        	"address1" => $address1,
        	"address2" => $address2,
        	"suburb" => $suburb,
            "country" => $country,
        	"state" => $state,
        	"postcode" => $postcode,
        	"email" => $email,
        	"mobile_phone" => $mobile_phone,
            //"fax_number" => $fax_number,
            //"driver_license_number" => $driver_license_number,
            //"dealer_license_number" => $dealer_license_number,
        	"note" => $note,
            "preferred_contact_method" => $preferred_contact_method,
            "privacy_flag" => $privacy_flag,
            "interested_vehicle" =>$interested_vehicle
            
        );

        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $result = $api->addContact($token, $params);
        /** GET CHANNELS from middleware **/
        $channels = $request->channels;

        if (isset($result->create_date)) {
            return redirect("/customer-details/view/".$result->id);
        } else if (isset($result->error->code)) {
            $data = array(
                "channels" => $channels
            );

            return view("main.create-customer", $data)->with("info", $result->error->code);
        }
    }
    /* END ADD CUSTOMER */

    /*public function searchCustomer(Request $request) {
    	$api = new \App\Library\APICaller();
    	$token = $request->session()->get("token");
    	$query = null;

        $params = $request->input("search");
        //exit(var_dump($params));

    }*/

    public function updateCustomer(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $id = $request->input("id");


        $title = $request->input("title");
        if (!isset($title)) {
            $title = 0;
        }
        $gender = $request->input("gender");
        if (!isset($gender)) {
            $gender = 0;
        }

        $first_name = $request->input("first_name");
        $last_name = $request->input("last_name");

        $privacy_flag = $request->input("privacy");
        $private_last_update_time = null;
        $company_name = $request->input("company");


        $company_type = $request->input("company_type");
        if (!isset($company_type)) {
            $company_type = 0;
        }
        $ABN = $request->input("ABN");
        $ACN = $request->input("ACN");
        $tax_type = $request->input("tax_type");

        $address1 = $request->input("address1");
        $address2 = $request->input("address2");

        $suburb = $request->input("suburb");
        $state = $request->input("state");

        $postcode = $request->input("postcode");
        $country = $request->input("country");
        if($country == null){
            $country = $request->input("input_country_id");
        }
        $postal_suburb = $request->input("postal_suburb");
        $postal_address1 = $request->input("postal_address1");
        $postal_address2 = $request->input("postal_address2");
        $postal_state = $request->input("postal_state");
        $postal_postcode = $request->input("postal_postcode");
        $postal_country = $request->input("postal_country");
        $email = $request->input("email");
        $email_alternate = $request->input("email_alternate");

        $mobile_phone = $request->input("mobile_phone");

        $home_phone = $request->input("home_phone");
        $work_phone = $request->input("work_phone");
        $fax_number = $request->input("fax");
        //$preferred_contact_method = null;
        $driver_license_number = $request->input("driver_number");
        $dealer_license_number = $request->input("dealer_number");
        $fleet_number = $request->input("fleet_number");
        $bank_branch_name = $request->input("bank_branch_name");
        $bank_account_name = $request->input("bank_account_name");
        $bank_bsb_number = $request->input("bank_bsb_number");
        $bank_account_number = $request->input("bank_account_number");
        $note = $request->input("note");
        $prospect_level = null;
        $contact_method = $request->input("contact_method");
        $interested_vehicle = $request->input("new_used_vehicle");

        if(!isset($interested_vehicle)) {
            $interested_vehicle = 0;
        }

        if (!isset($contact_method)) {
            $contact_method = 0;
        }


        $params = array (

            "title" => $title,
            "gender" => $gender,
            "first_name" => $first_name,
            "last_name" => $last_name,
            "privacy_flag" => $privacy_flag,
            "privacy_flag_update_time" => null,
            "company_name" => $company_name,
            "company_type" => $company_type,
            "ABN" => $ABN,
            "ACN" => $ACN,
            "tax_type" => 0,

            "address1" => $address1,
            "address2" => $address2,

            "suburb" => $suburb,

            "state" => $state,

            "postcode" => $postcode,
            "country" => $country,
            "postal_suburb" => $postal_suburb,
            "postal_address1" => $postal_address1,
            "postal_address2" => $postal_address2,
            "postal_state" => $postal_state,

            "postal_postcode" => $postal_postcode,

            "postal_country" => $postal_country,

            "email" => $email,
            "email_alternate" => $email_alternate,


            "mobile_phone" => $mobile_phone,

            "home_phone" => $home_phone,
            "work_phone" => $work_phone,
            "fax_number" => $fax_number,
            "preferred_contact_method" => $contact_method,
            "driver_license_number" => $driver_license_number,
            "dealer_license_number" => $dealer_license_number,
            "fleet_number" => $fleet_number,
            "bank_branch_name" => $bank_branch_name,
            "bank_account_name" => $bank_account_name,
            "bank_bsb_number" => $bank_bsb_number,
            "bank_account_number" => $bank_account_number,
            "note" => $note,
            "prospect_level" => 0,
            "interested_vehicle" => $interested_vehicle
            );


        $result = $api->updateContact($token, $params, $id);

        if (isset($result->update_date)) {
//            return redirect("/edit-customer/edit/" . $id);
            return redirect("/customer-details/view/". $id);
        } else {
            var_dump($result);
        }
    }

    /** EDIT CUSTOMER **/
    public function getCustomerById(Request $request, $type, $id)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $result =  json_decode($api->getContactById($token, $id));

        $titles = $api->getTypeCode($token, array("type" => "TITLE_TYPE"));

        $genders = $api->getTypeCode($token, array("type" => "GENDER_TYPE"));

        $contact_method = $api->getTypeCode($token, array("type" => "PREFERED_CONTACT_METHOD"));

        $vehicle_type = $api->getTypeCode($token, array("type" => "INTERESTED_VEHICLE_TYPE"));

        $company_type_list = $api->getTypeCode($token, array("type" => "COMPANY_TYPE"));

        $privacy_flag_list = $api->getTypeCode($token, array("type" => "PRIVACY_FLAG"));

        $country_list = $api->getCountry($token);

        $interaction_type = $api->getTypeCode($token, array("type" => "INTERACTION_TYPE"));

        $testdrive_media_type = $api->getTypeCode($token, array("type" => "MEDIA"));

        $contact_id = $id;
        
        $state_list = null;
        foreach ($country_list as $key => $value) {
            
            foreach($value->stateList as $key2 => $value2) {
                if ($value2->id == $result->state) {
                    $state_list = $value2;
                    break;
                }
            }

        }

        /** GET CHANNELS from middleware **/
        $channels = $request->channels;

        /** GET ROLES from middleware **/
        $roles = $request->roles;

        /** GET PERMISSIONS from middleware **/
        $permissions = $request->permissions;

        $data = array(
            "header" => "Customers",
            "name" => "Tony Spalla",
            "id" => $result->id,
            "title_code" => $result->title,
            "titles" => $titles,
            "gender_code" => $result->gender,
            "genders" => $genders,
            "first_name" => $result->first_name,
            "last_name" => $result->last_name,
            "status_code" => $result->status_code,
            "privacy_flag" => $result->privacy_flag,
            "privacy_flag_list" => $privacy_flag_list,
            "privacy_flag_update_time" => $result->privacy_flag_update_time,
            "company_name" => $result->company_name,
            "company_type_code" => $result->company_type,
            "company_type_list" => $company_type_list,
            "ABN" => $result->ABN,
            "ACN" => $result->ACN,
            "tax_type" => $result->tax_type,
            "address1" => $result->address1,
            "address2" => $result->address2,
            "suburb" => $result->suburb,
            "state" => $result->state,
            "state_list" => $state_list,

            "postcode" => $result->postcode,
            "country_code" => $result->country,
            "country_name" => $result->country_name,
            "country_list" => $country_list,
            "postal_address1" => $result->postal_address1,
            "postal_address2" => $result->postal_address2,
            "postal_suburb" => $result->postal_suburb,
            "postal_state" => $result->postal_state,
            "postal_postcode" => $result->postal_postcode,
            "postal_country" => $result->postal_country,
            "email" => $result->email,
            "email_alternate" => $result->email_alternate,
            "mobile_phone" => $result->mobile_phone,
            "home_phone" => $result->home_phone,
            "work_phone" => $result->work_phone,
            "fax_number" => $result->fax_number,
            "preferred_contact_method" => $result->preferred_contact_method,
            "interested_vehicle" => $result->interested_vehicle,
            "contact_method" => $contact_method,
            "driver_license_number" => $result->driver_license_number,
            "dealer_license_number" => $result->dealer_license_number,
            "fleet_number" => $result->fleet_number,
            "bank_branch_name" => $result->bank_branch_name,
            "bank_account_name" => $result->bank_account_name,
            "bank_bsb_number" => $result->bank_bsb_number,
            "bank_account_number" => $result->bank_account_number,
            "note" => $result->note,
            "salesperson_id" => $result->salesperson_id,
            "updater_id" => $result->updater_id,
            "prospect_level" => $result->prospect_level,
            "create_date" => $result->create_date,
            "update_date" => $result->update_date,
            "vehicle_type" => $vehicle_type,
            "channels" => $channels,
            "roles" => $roles,
            "permissions" => $permissions,
            "type" => $interaction_type,
            "media_type" => $testdrive_media_type

        );

        /** GET TOTAL OF VALUATIONS **/
        //$length = $api->getLengthValuationsByCustomer($token, $result->id);
        //$data["valuations_count"] = json_decode($length)->count;

        $interaction_length = $api->countInteraction($token, $contact_id);
        $data['interaction_count'] = json_decode($interaction_length)->count;

        $data["js"][] = "customer/viewCustomerDetail";

        /* Add default header */
        $data['header'] = 'Customers';
        if ($type == 'view') {
            return view('main.customer-details', $data);
        } else if ($type == 'edit') {
            return view('main.edit-customer', $data);
        }
    }

}