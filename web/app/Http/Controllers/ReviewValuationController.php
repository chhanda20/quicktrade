<?php

namespace App\Http\Controllers;

use App\Library\Helper;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use WindowsAzure\Common\ServicesBuilder;
//use WindowsAzure\Common\ServiceException;

use MicrosoftAzure\Storage\Blob\Models\CreateContainerOptions;
use MicrosoftAzure\Storage\Blob\Models\PublicAccessType;
use MicrosoftAzure\Storage\Common\ServiceException;

class ReviewValuationController extends Controller
{
    public function __construct() {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }

    public function checkPPSR(Request $request)
    {
        $token = $request->session()->get("token");

        $vin = $request->input("vin");
        $year = $request->input("year");

        $api = new \App\Library\APICaller();
        
        return $api->checkPPSR($token, $vin, $year);
    }

    public function reviewValuation(Request $request, $valuationId, $enable_receive_vehicle = null)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $result = json_decode($api->getVehicleDetail($token, $valuationId));
        $colors = $api->getTypeCode($token, array("type" => "COLOR_TYPE"));
        $fuel_types = $api->getTypeCode($token, array("type" => "FUEL_TYPE"));
        $trim_types = $api->getTypeCode($token, array("type" => "TRIM_TYPE"));
        $body_types = $api->getTypeCode($token, array("type" => "BODY_TYPE"));
        $transmissions = $api->getTypeCode($token, array("type" => "TRANSMISSION"));
        $drivetrains = $api->getTypeCode($token, array("type" => "DRIVETRAIN"));
        $registered_states = $api->getTypeCode($token, array("type" => "AU_STATE"));

        $ownership_types = $api->getTypeCode($token, array("type" => "OWNERSHIP_TYPE"));
        $special_disclosures_type = $api->getTypeCode($token, array("type" => "SPECIAL_DISCLOSURES"));
        $repairable_write_off_type = $api->getTypeCode($token, array("type" => "REPAIRABLE_WRITE_OFF"));

        if (!isset($result->error)) {
            /** GET MAKE VALUE **/
            /*$make_id = $result->make;
            $make_value = $make_id;
            $makes = $api->getMakes($token);
            foreach ($makes as $make){
                if($make_id === $make->id){
                    $make_value = $make->name;
                }
            }*/

            /** get COLOR value **/
            $colour = $result->colour;
            $manufacturer_color = $result->manufacturer_color;
            $trim_color = $result->trim_color;
            $colour_value = null;
            $manufacturer_color_value = null;
            $trim_color_value = null;
            foreach($colors as $key => $value){
                if ($colour == $colors[$key]->code) {
                    $colour_value = $colors[$key]->value;
                }
                if ($manufacturer_color == $colors[$key]->code) {
                    $manufacturer_color_value = $colors[$key]->value;
                }
                if ($trim_color == $colors[$key]->code) {
                    $trim_color_value = $colors[$key]->value;
                }
            }

            /**** GET OTHER LIST VALUE ****/
            /** OWNERSHIP TYPE**/
            $ownership_type = $result->ownership_type->code;
            $ownership_type_value = "";
            foreach($ownership_types as $key=>$value){
                if($ownership_type == $ownership_types[$key]->code){
                    $ownership_type_value = $ownership_types[$key]->value;
                }
                if($ownership_type == 6){
                    $ownership_type_value = $result->ownership_type->value;
                }

            }

            /** SPECIAL DISCLOSURES **/
            $special_disclosures = explode(",", $result->special_disclosures);
            $repairable_write_off = $result->repairable_write_off;
            $special_disclosures_value = [];
            foreach($special_disclosures as $special_disclosure_code){
                if($special_disclosure_code == 2){
                    foreach($special_disclosures_type as $key=>$value){
                        if($special_disclosures_type[$key]->code == 2){
                            array_push($special_disclosures_value, $special_disclosures_type[$key]->value);
                        }
                    }
                }else if($special_disclosure_code == 1){
                    foreach($repairable_write_off_type as $key1=>$value1){
                        if($repairable_write_off == $repairable_write_off_type[$key1]->code){
                            array_push($special_disclosures_value, $repairable_write_off_type[$key1]->value);
                        }
                    }
                }
            }

            /** OFFER PRICE **/
            $offer_price = $result->offer_price;

            /** TRIM TYPE **/
            $trim = $result->trim;
            $trim_value = $trim;
            foreach($trim_types as $key => $value){
                if ($trim === $trim_types[$key]->code) {
                    $trim_value = $trim_types[$key]->value;
                }
            }
            /** FUEL TYPE **/
            $fuel_type = $result->fuel_type;
            $fuel_type_value = $fuel_type;
            foreach($fuel_types as $key => $value){
                if ($fuel_type === $fuel_types[$key]->code) {
                    $fuel_type_value = $fuel_types[$key]->value;
                }
            }
            /** BODY TYPE **/
            $body_type = $result->body_type;
            $body_type_value = $body_type;
            foreach($body_types as $key => $value){
                if ($body_type === $body_types[$key]->code) {
                    $body_type_value = $body_types[$key]->value;
                }
            }
            /** TRANSMISSION **/
            $transmission = $result->transmission;
            $transmission_value = $transmission;
            foreach($transmissions as $key => $value){
                if ($transmission === $transmissions[$key]->code) {
                    $transmission_value = $transmissions[$key]->value;
                }
            }
            /** DRIVETRAIN **/
            $drive_train = $result->drive_train;
            $drive_train_value = $drive_train;
            foreach($drivetrains as $key => $value){
                if ($drive_train === $drivetrains[$key]->code) {
                    $drive_train_value = $drivetrains[$key]->value;
                }
            }
            /** REGISTERED STATE **/
            $registered_state = $result->registered_state;
            $registered_state_value = $registered_state;
            foreach($registered_states as $key => $value){
                if ($registered_state === $registered_states[$key]->code) {
                    $registered_state_value = $registered_states[$key]->value;
                }
            }

            /** format DATE **/
            $build_date = $result->build_date;
            $compliance_date = $result->compliance_date;
            $year_first_registered = $result->year_first_registered;
            $registration_expiry_date = $result->registration_expiry_date;

            /** COMPANY ID **/
            $company_id = $result->company_id;


            /** MODEL SPECS data **/
            $data = array(
                "valuation_id" => $result->valuation_id,
                "vehicle_id" => $result->vehicle_id,
                "status" => $result->status,
                "make" => $result->make,
                /*"make_code" => $make_id,*/
                "model" => $result->model_family,
                "registration" => $result->registration_number,
                "variant" => $result->variant,
                "colour" => $colour_value,
                "colour_code" => $colour,
                "manufacturer_color" => $manufacturer_color_value,
                "manufacturer_color_code" => $manufacturer_color,
                "body_type" => $body_type_value,
                "body_type_code" => $body_type,
                "year_first_registered" => isset ($year_first_registered) ? substr($year_first_registered, 0, 4) : $year_first_registered,
                "registration_expiry_date" => $registration_expiry_date,
                "registered_state" => $registered_state_value,
                "registered_state_code" => $registered_state,
                "build_date" => $build_date,
                "compliance_date" => $compliance_date,
                "engine" => $result->engine,
                "engine_size" => $result->engine_size,
                "engine_capacity" => $result->engine_capacity,
                "cylinder" => $result->cylinder,
                "fuel_type" => $fuel_type_value,
                "fuel_type_code" => $fuel_type,
                "transmission" => $transmission_value,
                "transmission_code" => $transmission,
                "drive_train" => $drive_train_value,
                "drive_train_code" => $drive_train,
                "body_no" => $result->body_no,
                "vin" => $result->vin,
                "chassis_number" => $result->chassis_number,
                "trim_code" => $trim,
                "trim" => $trim_value,
                "trim_color" => $trim_color_value,
                "trim_color_code" => $trim_color,
                "option_list" => $result->option_list,
                "ownership_type" => $ownership_type_value,
                "special_disclosures" => $special_disclosures_value,
                "offer_price" => $offer_price,
                /** SUMMARY - VEHICLE DATA **/
                "year" => isset ($build_date) ? substr($build_date, 0, 4) : $build_date,
                "badge" => $result->badge,
                "series" => $result->series,
                "has_service_history" => $result->has_service_history,
                "has_service_book" => $result->has_service_book,
                "has_one_owner" => $result->has_one_owner,
                "odometer" => $result->odometer,
                "overall_rating" => $result->overall_rating,
                "customer_id" => null,
                "company_id" => $company_id
            );

            /** RETURN DROP-DOWN LIST DATA **/
            $data["colors"] = $colors;
            $data["fuel_types"] = $fuel_types;
            $data["trim_types"] = $trim_types;
            $data["transmissions"] = $transmissions;
            $data["body_types"] = $body_types;
            $data["drivetrains"] = $drivetrains;
            $data["registered_states"] = $registered_states;

            /** OPTIONAL and EXTRAS options **/
            $optional_options_list = json_decode($api->getVehicleOptionalOptions($token, $result->vehicle_id));
            $data["optional_options_list"] = null;
            if (!isset($optional_options_list->error)) {
                $data["optional_options_list"] = $optional_options_list;
            }

            $extras_options_list = json_decode($api->getVehicleExtrasOptions($token, $result->vehicle_id));
            $data["extras_options_list"] = null;
            if (!isset($extras_options_list->error)) {
                $data["extras_options_list"] = $extras_options_list;
            }

            /** INSPECTION ITEM LIST **/
            $inspection_item_list = json_decode($api->getInspectionItem($token, $valuationId, "EXTERIOR_SECTION"));
            $general_cost = $api->getGeneralReconditionCost($token, $valuationId);
            $data["general_cost"] = $general_cost;

            /** COST SECTION **/
            $sections = array();
            $all_section_cost = array();
            $total_cost = 0;

            if (!isset($inspection_item_list->error)) {
            	foreach ($inspection_item_list as $key => $value) {
                	$section_cost = 0;
                    $sections = $sections + array($value->section_type => $value->section_name);
                	foreach ($value->items as $key2 => $value2) {
                    	 $section_cost = $value2->repair_cost + $section_cost;
                	}
                    $total_cost += $section_cost;
                	$all_section_cost = array_merge($all_section_cost, array("section".$value->section_type => $section_cost));
                }
            }
            $data["sections"] = $sections;

            $data["all_section_cost"] = $all_section_cost;
            $data["total_cost"] = $total_cost;

            if(!isset($inspection_item_list->error->code)) {

            } else {
                $inspection_item_list = null;
            }

            $data["inspection_item_list"]= $inspection_item_list;
            $data["inspection_item_no"]= 1;

            /*** INTERIOR ***/
            $interior_item_list = json_decode($api->getInspectionItem($token, $valuationId, "INTERIOR_SECTION"));

            $interior_total_cost = 0;
            if (!isset($interior_item_list->error)) {
                foreach ($interior_item_list as $key => $value) {
                    foreach ($value->items as $key2 => $value2) {
                        $interior_total_cost += $value2->repair_cost;
                    }
                }
            } else {
                $interior_item_list = null;
            }

            $data["interior_item_list"] = $interior_item_list;

            $data["interior_total_cost"] = $interior_total_cost;
            $data["interior_item_no"] = 1;

            /*** END INTERIOR ***/

            /*** MECHANICAL ***/
            $mechanical_item_list = json_decode($api->getInspectionItem($token, $valuationId, "MECHANICAL_SECTION"));
            $mechanical_total_cost = 0;
            if (!isset($mechanical_item_list->error)) {

                $data["mechanical_item_list"] = $mechanical_item_list;


                foreach ($mechanical_item_list as $key => $value) {
                    foreach ($value->items as $key2 => $value2) {
                        $mechanical_total_cost += $value2->repair_cost;
                    }
                }

            }
            $data["mechanical_total_cost"] = $mechanical_total_cost;
            $data["mechanical_item_no"] = 1;
            /*** END MACHANICAL***/

            $data['hideBorderBottom'] = 'hideIt';
            $data['disableIt'] = 'disableIt';
            /** 7 is for TRADED IN, 9 is for VERSION VIEW **/

            if($result->status == 7 || $result->status == 9 || $result->status == 12 || $result->status == 13 || $result->status == 14) {
                $data['hideEditField'] = 'hidden';
                $data["disableEditField"] = "disabled-div";
                $data["disabledUploadImage"] = " disabled";
                if($result->status == 12 || $result->status == 13 || $result->status == 14){
                    $data["disableEditButton"] = "disabled-div";
                }
            }else{
                $data['hideViewField'] = 'hidden';
                $data['hideBorderBottom'] = '';
                $data['disableIt'] = '';
            }
        }

        $valuationDetail = json_decode($api->getValuationDetail($token, $valuationId));
        if (!isset($valuationDetail->error)) {
            $data["valuation_name"] = $valuationDetail->valuation_name;
        }

        /************* get VERSION *************/
        $valuation = json_decode($api->getValuationDetailIncludeVersion($token, $valuationId));
        if (!isset($valuation->error)) {
            $data["version_locker_user_id"] = $valuation->version_locker_user_id;
        }

        /************* start SUMMARY *************/
        /** OVERALL RATING **/
        $overallRatingValues = $api->getTypeCode($token, array("type"=>"OVERALL_RATING"));
        $data["overall_rating_values"] = $overallRatingValues;

        foreach ($overallRatingValues as $overallRatingValue) {
            if($overallRatingValue->code == $result->overall_rating) {
                $data["overall_rating_selected_value"] = $overallRatingValue->value;
            }
        }

        /** WHOLESALE VALUES **/
        $wholesaleValues = json_decode($api->getWholesaleValues($token, $valuationId));
        if (!isset($wholesaleValues->error)) {
            $data["wholesale_glass"] = $wholesaleValues->glass;
            $data["wholesale_km_adjustment"] = $wholesaleValues->km_adjustment;
            $data["wholesale_value_options"] = $wholesaleValues->value_of_options;
        }

        /** SALES INFO **/
        $salesInfo = json_decode($api->getSalesInfo($token, $valuationId));
        if (!isset($salesInfo->error)) {
            if($salesInfo->valuation_price == 0){
                $salesInfo->valuation_price = null;
            }
            if($salesInfo->max_valuation_price == 0){
                $salesInfo->max_valuation_price = null;
            }
            $data["salesInfo"] = $salesInfo;
            /** get SALE STATUS **/
            $saleStatus = $salesInfo->sale_status;
            $salesInfoStatuses = $api->getTypeCode($token, array("type"=>"SALE_STATUS", "code"=>$saleStatus));
            $data["salesInfoStatuses"] = $salesInfoStatuses;

            foreach ($salesInfoStatuses as $salesInfoStatus) {
                if($saleStatus == $salesInfoStatus->code){
                    $data["sales_status_selected_value"] = $salesInfoStatus->value;
                }
            }
        }

        /** SEND EXTERNAL OFFER REQUEST **/
        //get external offer group
        $result_external_offer_group = json_decode($api->getGroupOfExternalOffer($token));
        $data["external_offer_group"] = $result_external_offer_group;
        //end get external offer group

        $result_external_offer = json_decode($api->getExternalOffer($token, $valuationId));
        
        if (isset($result_external_offer->id)) {

            //time remaining calc
            $data["external_offer_status"] = $result_external_offer;
            $api_system_datetime = $result_external_offer->system_time;
            $api_system_datetime = new \DateTime($api_system_datetime);
            $external_offer_create_datetime = $result_external_offer->create_date;
            $external_offer_create_datetime = new \DateTime($external_offer_create_datetime);
            

            $seconds = $api_system_datetime->diff($external_offer_create_datetime)->format('%a') * 24 * 3600;
            $seconds += $api_system_datetime->diff($external_offer_create_datetime)->format('%h') * 3600;
            $seconds += $api_system_datetime->diff($external_offer_create_datetime)->format('%i') * 60;
            $seconds += $api_system_datetime->diff($external_offer_create_datetime)->format('%s');
            
            $setting_configuration = json_decode($api->getSettingExternalOffer($token));

            //let configuration time defaul is 30 mins
            if (isset($setting_configuration->external_offer_timeout)) {
                $time_remaining = $setting_configuration->external_offer_timeout;
            } else {
                $time_remaining = 30;
            }

            $seconds = ($time_remaining * 60) - $seconds;

            if($seconds > 0) {
                //$minutes = floor($seconds % 3600 / 60);
                //$seconds = $seconds % 60;
                
                //$data["remaining_time"] = $minutes . ":" . $seconds;
                $data["remaining_time"] = gmdate("H:i:s", $seconds);
            } else {
                $data["remaining_time"] = "time_is_up";
                $result_external_offer = json_decode($api->getExternalOffer($token, $valuationId));
                $data["external_offer_status"] = $result_external_offer;
                $data["best_offer"] = $result_external_offer->best_offer;
            }
            
            //end time remaining calc

        } else {
            $data["external_offer_status"] = "begin_send_request";
        }

        /** RECONDITIONING COST **/
        $reconditioningCost = json_decode($api->getReconditioningCost($token, $valuationId));
        if (!isset($reconditioningCost->error)) {
            $data["reconditioningCost"] = $reconditioningCost;
        }

        /************* end SUMMARY *************/

        /** get MECHANICAL **/
        $mechanicalDetails = json_decode($api->getMechanicalDetails($token, $valuationId));
        if (!isset($mechanicalDetails->error)) {
            $data["mechanicalDetails"] = $mechanicalDetails;
        }

        $price_array = ["Valuation Price", "Max. Valuation Price", "Estimated Total Reconditioning Cost"];

        $step_array = ["details", "exterior", "interior/electronics", "mechanical", "summary"];
        $step_data_array = ["details", "exterior", "interior", "mechanical", "summary"];

        $details_step_array = ["model specs & standard features", "options & extras"];
        $details_data_step_array = ["model_specs", "options_extras"];

        /** GET CHANNELS from middleware **/
        $channels = $request->channels;
        $data["channels"] = $channels;

        /** GET ROLES from middleware **/
        $roles = $request->roles;
        $data["roles"] = $roles;

        /** GET PERMISSIONS from middleware **/
        $permissions = $request->permissions;
        $data["permissions"] = $permissions;

        /** USING FOR SALESPERSON TO BEGIN A VALUATION **/
        if(!in_array("valuation_perform", $permissions) && $data['status'] == 10){
            array_pop($step_array);
        }

        $data["step_array"] = $step_array;
        $data["step_data_array"] = $step_data_array;
        $data["price_array"] = $price_array;
        $data["details_step_array"] = $details_step_array;
        $data["details_data_step_array"] = $details_data_step_array;

        /** SET BACK TO LIST **/
        $status = $data['status'];
        $back_to_list = "";
        $back_to_list_link = "";
        switch ($status) {
            case 1:
                $back_to_list = "Request List";
                $back_to_list_link = "valuations/requests";
                break;
            case 2:
                $back_to_list = "In Progress List";
                $back_to_list_link = "valuations/in-progress";
                break;
            case in_array($status, [3,6,7]):
                $back_to_list = "Complete List";
                $back_to_list_link = "valuations/complete";
                break;
            case in_array($status, [4,5]):
                $back_to_list = "Pending Approval List";
                $back_to_list_link = "valuations/pending";
                break;
        }
        $data["back_to_list"] = $back_to_list;
        $data["back_to_list_link"] = $back_to_list_link;

        /** SET HEADER **/
        if($data['status'] == 10){
            $data["header"] = "Begin Valuations";
        }else{

            if ($data['status'] == 3 || $data['status'] == 4 || $data['status'] == 5 
                || $data['status'] == 6 || $data['status'] == 7) 
            {
                $data["header"] = "Review Valuations";
            }
            else
            {
                $data["header"] = "Complete Valuations";
            }
            
        }

        /** GET CUSTOMER ID **/
        $valuation = json_decode($api->getValuationById($token, $valuationId));
        if(!property_exists($valuation, 'error')){
            $data["customer_id"] = $valuation->customer_id;
        }

        /** GET VALUATION CURRENT VERSION **/
        if(!isset($valuation->error)){
            $data["valuation_current_version"] = $valuation->version;
        }

        if (isset($enable_receive_vehicle)) {
            $data["enable_receive_vehicle"] = $enable_receive_vehicle;
            Log::info('Get receive param from URL: '.$enable_receive_vehicle);
        }

        /** GET LOCATION **/
        /*$locations = json_decode($api->getLocationByCurrentLoggedUser($token));
        if (!isset($locations->error)) {
            $data["locations"] = $locations;
        }*/


        /** GET LOCATION **/
        $limit = 100;
        $offset = 0;
        $params = array(
            'limit'=>$limit,
            'offset'=>$offset,
            'company_id'=> $company_id
        );
        $locations = json_decode($api->getLocations($token, $params));
        if(!isset($locations->error)){
            $data["locations"] = $locations;
        }


        $data["js"][] = "review-valuation/reviewValuation-inspectionItems";

        $data["js"][] = "review-valuation/reviewValuation-master";

        $data["js"][] = "review-valuation/reviewValuation-summary";

        $data["js"][] = "review-valuation/reviewValuation-externalOffer";

        return view("layouts.review-valuation", $data);
    }

    public function deleteItem(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $valuation_id = $request->input("valuation_id");
        $inspection_item_id = $request->input("inspection_item_id");

        LOG::debug("DELETE INSPECTION ITEMS - params: ".\GuzzleHttp\json_encode(array($valuation_id, $inspection_item_id)));

        return $api->deleteInspectionItem($token, $valuation_id, $inspection_item_id);
    }

    public function getDataFromAzure(Request $request)
    {
        $connectionString = "DefaultEndpointsProtocol=https;AccountName=mo;AccountKey=5GscYicqgiJTdvrhiCldmINsQojGYwMkwbe+i/YeBmQMgeXfMtHCVx6SLwymY9YS8cJ7WxdAuM1B4y7fg4T6bw==";

        $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);

        // Create blob REST proxy.
        //$blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);

        try    {
            // Get blob.
            $blob = $blobRestProxy->getBlob("mycontainer", "myblob");
            //fpassthru($blob->getContentStream());

           $image = imagecreatefromstring($blob->getContentStream());

            ob_start(); //You could also just output the $image via header() and bypass this buffer capture.
            imagepng($image);
            $data = ob_get_contents();
            ob_end_clean();
            echo '<img src="data:image/jpg;base64,' .  base64_encode($data)  . '" />';

        }
        catch(ServiceException $e){
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();
            echo $code.": ".$error_message."<br />";
        }
    }

    public function uploadImagesToAzure($image_arr)
    {
        //session_start();
        //$images = $_SESSION["images"];
        $images = $image_arr;
        foreach ($images as $key => $value) {
            if ($value != null) {
                $fileContent = file_get_contents($value);
                $image_content = $fileContent;
                //$image_name = $_FILES["fileToUpload"]["name"];
                $image_name = basename($value);
                $azure = new \App\Library\AzureCloud();
                //$azure->createContainer("mo-exterior");
                $image = $azure->uploadBlobImage($image_content, $image_name, "mo-exterior");

                if ($image) {
                    //echo "ok";
                }
            }
        }
    }

    private function checkImage($file)
    {
        $imageFileType = pathinfo($file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        $check = getimagesize($file);
        if($check !== false) {
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                return false;
            } else {
                // Check file size
                if ($_FILES["fileToUpload"]["size"] > 500000) {
                    return false;
                }
            }

        } else {
            return false;
        }
        return true;
    }

    //image of inspection items
    public function uploadImageToAzure(Request $request)
    {

        $image = $_FILES["fileToUpload"]["tmp_name"];
        $imageFileType = "jpg";
        $inspection_item_id = $request->input("exterior-item-id");
        if($inspection_item_id == ""){
            $inspection_item_id = $request->input("inspection_item_id");
        }
        $valuation_id = $request->input("valuation_id");
        $valuation_version = $request->input("valuation_version");
        $sequence = $request->input("sequence");
        if($sequence == ""){
            $sequence = $request->input("sequence_add_photo");
        }
        $image_name = $valuation_id . "_" . $valuation_version . "_" . $inspection_item_id . "_" . $sequence.".".$imageFileType;
        if ($image != null) {
            $fileContent = file_get_contents($image);
            $image_content = $fileContent;
            $image_name = basename($image_name);
            $azure = new \App\Library\AzureCloud();
            $image = $azure->uploadBlobImage($image_content, $image_name, "inspection-item");

            if ($image) {
                return $image;
            }

        }
    }

    //vehicle image
    public function uploadImageVehicleToAzure(Request $request)
    {
        $image = $_FILES["mo-vehicle-photo-upload"]["tmp_name"];
        $imageFileType = "jpg";
        $vehicle_id = $request->input("vehicle_id");

        $image_name = $vehicle_id.".".$imageFileType;

        if ($image != null) {
            $fileContent = file_get_contents($image);
            $image_content = $fileContent;
            $image_name = basename($image_name);
            $azure = new \App\Library\AzureCloud();
            $image = $azure->uploadBlobImage($image_content, $image_name, "vehicle-image");

            if ($image) {
                return $image;
            }

        }
    }

    public function getImageVehicleLink(Request $request)
    {
        $azure = new \App\Library\AzureCloud();
        echo $azure->getBlobLink();
        echo "Hello World";
    }

    public function uploadImageTmp(Request $request)
    {
        if ($this->checkImage($_FILES["fileToUpload"]["tmp_name"]))
        {
            $imageFileType = pathinfo($_FILES["fileToUpload"]["name"],PATHINFO_EXTENSION);
            $inspection_item_id = $request->input("exterior-items");
            $valuation_id = $request->input("valuation_id");
            $sequence = $request->input("sequence");
            //upload image to tmp folder
            $uploaddir = $_SERVER['DOCUMENT_ROOT']."/images_tmp/";

            //$file_tmp = $uploaddir . basename($_FILES["fileToUpload"]["name"]);

            $image_name = $valuation_id . "_" . $inspection_item_id . "_" . $sequence.".".$imageFileType;
            $file_tmp = $uploaddir . $image_name;
            $document_root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
            $images_name = array("1" => null, "2" => null, "3" => null);
            $images = array("1" => null, "2" => null, "3" => null);
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $file_tmp)) {
                $image = $document_root . "images_tmp/". $image_name;
                echo $image;
                //store images_tmp path to session
                session_start();
                if (isset($_SESSION["images_name"])) {
                    $images_name = $_SESSION["images_name"];
                }

                if (isset($_SESSION['images'])) {
                    $images = $_SESSION['images'];
                }

                $images_name[$request->input("sequence")] = $image_name;
                $images[$request->input("sequence")] = $image;

                $_SESSION["images_name"] = $images_name;
                $_SESSION["images"] = $images;
                //$request->session()->put("images", $image_name);
                //$GLOBALS['images'] = $images;
            }
        }
    }

    public function updateInspectionItem(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $valuation_id = $request->input("valuation_id");
        $inspection_item_id = $request->input("inspection_item_id");
        $repair_cost = $request->input("repair_cost");
        $note = $request->input("note");

        $params = array(
            "repairCost" => $repair_cost
        );

        if(!empty($note)){
            $params["note"] = $note;
        }

        //exit(var_dump(json_encode($params)));

        /*session_start();
        $images = $_SESSION["images"];
        $images_name = $_SESSION["images_name"];
        $this->uploadImageToAzure($images);*/

        return $api->updateInspectionItem($token, $valuation_id, $inspection_item_id, $params);
    }

    public function reconditioningCost(Request $request)
    {
        $valuation_id = $request->input("valuation_id");
        $general_cost = $request->input("general_cost");
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        return $api->reconditioningCost($token, $valuation_id, $general_cost);
    }

    public function addInspectionItem(Request $request)
    {
        $valuation_id = $request->input("valuation_id");

        $inspection_item_id = $request->input("inspection_item_id");

        $repair_cost = $request->input("repair_cost");

        $note = $request->input("note");

        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();


        $params = array("inspectionItemId" => $inspection_item_id, "valuationId" => $valuation_id, "repairCost" => $repair_cost, "note" => $note);

        return $api->addInspectionItem($token, $params);

    }

    public function getInspectionItems(Request $request, $section_type)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $inspectionItems = $api->getInspectionItemsList($token, $section_type);
        if(Helper::resultCheck($inspectionItems) === true){
            return $inspectionItems;
        }
        return Helper::resultCheck($inspectionItems);
    }

    public function getInspectionItemDetail(Request $request, $valuation_id, $inspection_item_id)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        return $api->getInspectionItemDetail($token, $valuation_id, $inspection_item_id);
    }

    public function listAzure(Request $request)
    {

        $a = new \App\Library\AzureCloud();
        $connectionString = "DefaultEndpointsProtocol=https;AccountName=mo;AccountKey=5GscYicqgiJTdvrhiCldmINsQojGYwMkwbe+i/YeBmQMgeXfMtHCVx6SLwymY9YS8cJ7WxdAuM1B4y7fg4T6bw==";
        $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);

        try {
            // List blobs.
            $blob_list = $blobRestProxy->listBlobs("mycontainer");
            $blobs = $blob_list->getBlobs();

            foreach($blobs as $blob)
            {
                echo $blob->getName().": ".$blob->getUrl()."<br />";
                //echo '<img src="'.$blob->getUrl().'" />';
            }
        }
        catch(ServiceException $e) {
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();
            echo $code.": ".$error_message."<br />";
        }
    }


    public function connectAzure(Request $request)
    {
        $connectionString = "DefaultEndpointsProtocol=https;AccountName=mo;AccountKey=5GscYicqgiJTdvrhiCldmINsQojGYwMkwbe+i/YeBmQMgeXfMtHCVx6SLwymY9YS8cJ7WxdAuM1B4y7fg4T6bw==";
        $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);
        $content = fopen("C:\Users\PC-18\Desktop\demo\bg-img@3x.png.png", "r");
        $blob_name = "myblob.png";
        try {
            //Upload blob
            $blobRestProxy->createBlockBlob("mycontainer", $blob_name, $content);
        }

        catch(ServiceException $e) {
            // Handle exception based on error codes and messages.
            // Error codes and messages are here:
            // http://msdn.microsoft.com/library/azure/dd179439.aspx
            $code = $e->getCode();
            $error_message = $e->getMessage();
            echo $code.": ".$error_message."<br />";
        }
    }

    public function saveOptionsAndExtras(Request $request,$valuationId)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        /** GET FORM INPUT **/
        $vehicle_id = $request->input("vehicle_id");
        $optional_options = $request->input("optional_options");
        $extras_options = $request->input("extras_options");
        $deleted_extras_options = $request->input("deleted_extras_options");

        /** update Optional Options **/
        $api->updateVehicleOptionalOptions($token, $vehicle_id, $optional_options);

        /** create Extras Options **/
        if(isset($extras_options) && is_array($extras_options)){
            foreach($extras_options as $extras_option){
                $api->createVehicleExtrasOptions($token, $vehicle_id, $extras_option);
            }
        }

        /** delete Extras Options**/
        if(isset($deleted_extras_options) && is_array($deleted_extras_options)){
            foreach($deleted_extras_options as $deleted_extras_option){
                $api->deleteVehicleExtrasOptions($token, $vehicle_id, $deleted_extras_option);
            }
        }

        return redirect("review-valuation/".$valuationId."#exterior");
    }

    public function saveMechanicalDetails(Request $request,$valuationId)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        /** get FORM INPUT **/
        $test_driven_input = $request->input("test_driven");
        $test_driven = isset($test_driven_input) && $test_driven_input ? "1" : "0";
        $mechanical_report = $request->input("mechanical_report");
        $params = array(
            "test_driven" => $test_driven,
            "mechanical_additional_detail" => $mechanical_report
        );

        /** update MECHANICAL DETAILS **/
        $api->updateMechanicalDetails($token, $valuationId, $params);

        /** REQUEST VALUATION **/
        if(strtolower($request->input('submit')) == "request valuation"){
            $result = $api->requestValuation($token, $valuationId);
            if(!property_exists($result, 'error')){
                return redirect("/valuations/requests");
            }
        }

        return redirect("review-valuation/".$valuationId."#mechanical");
    }

    public function saveSummaryInfo(Request $request,$valuationId)
    {

        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        /** get FORM INPUT **/
        $mo_admin = $request->input("mo_admin");
        $registration_no = $request->input("registration_no");
        $service_history = $request->input("service_history");
        $service_books = $request->input("service_books");
        $one_owner = $request->input("one_owner");
        $odometer_reading = $request->input("odometer_reading");
        $vehicleParams = array(
            "registration_number" => $registration_no,
            "has_service_history" => $service_history,
            "has_service_book" => $service_books,
            "has_one_owner" => $one_owner,
            "odometer" => $odometer_reading
        );

        $overall_rating = $request->input("overall_rating");
        $overallRatingParams = array(
            "overall_rating" => $overall_rating
        );

        $wholesale_value_options = $request->input("wholesale_value_options");
        $retailObj = (object) array('retail'=>$wholesale_value_options);
        $wholesaleParams = array(
            "value_of_options" => $retailObj
        );

        $vehicle = $request->input("vehicle");
        $valuation_price = $request->input("valuation_price");
        $maximum_valuation_price = $request->input("maximum_valuation_price");
        $salesInfoParams = array(
            "sale_status" => $vehicle,
            "valuation_price" => $valuation_price,
            "max_valuation_price" => $maximum_valuation_price
        );

//        $overallEstimatedCost = $request->input("overall_estimated_cost");
        $overallEstimatedCost = $request->input("estimated_recon_cost");
        $estimatedParams = array(
            "estimated_recon_cost" => floatval($overallEstimatedCost)
        );

        $offer_price = $request->input("offer_price");
        $offerPriceParams = array(
            "offer_price" => $offer_price
        );

        /** update VEHICLE DETAILS **/
        $api->updateValuations($token, $valuationId, $vehicleParams);

        /** update OVERALL RATING **/
        $api->updateOverallRating($token, $valuationId, $overallRatingParams);

        /** update SALES INFO **/
        $api->updateSalesInfo($token, $valuationId, $salesInfoParams);

        /** update RECONDITIONING COST **/
        $api->updateReconditioningCost($token, $valuationId, $estimatedParams);

        /** update OFFER PRICE **/
        $api->updateOfferPrice($token, $valuationId, $offerPriceParams);

        /** update WHOLESALE VALUE OPTIONS **/
        $api->updateWholesaleValues($token, $valuationId, $wholesaleParams);
        $valuation_status = $request->input("valuation_status");
        //dd($valuation_status);
        if (isset ($valuation_status) && $valuation_status == 10 || $valuation_status == 1 || $valuation_status == 2) {
            $result = json_decode($api->completeValuation($token, $valuationId));

            if (isset($result->id)) {
                return redirect($mo_admin."/valuations/pending");
            }
            else {
                //exit(var_dump($result));
            }   
        }else if($valuation_status == 4 || $valuation_status == 5){
            return redirect($mo_admin."/valuations/pending");
        }


        /** CHECK AND RELEASE NEW VERSION **/
/*        dd($request->session());
dd($request->all());*/
        $version_locker_user_id = $request->input("version_locker_user_id");

        if($version_locker_user_id != 0){
            $valuation_user_id = $request->session()->get("id");
            if($version_locker_user_id == $valuation_user_id){
                $result = json_decode($api->releaseValuationVersionId($token, $valuationId));
                if(!isset($result->error)){
                    return redirect($mo_admin."/valuations/complete");
                }else{
                    return \Redirect::back()->withErrors(['msg', $result->error->detail]);
                    //return $result->error->detail;
                }

            }
        }
        return redirect($mo_admin."/review-valuation/".$valuationId."#summary");
    }

    public function completeValuationForSendRequest(Request $request, $valuationId)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        /** get FORM INPUT **/
        $mo_admin = $request->input("mo_admin");
        $registration_no = $request->input("registration_no");
        $service_history = $request->input("service_history");
        $service_books = $request->input("service_books");
        $one_owner = $request->input("one_owner");
        $odometer_reading = $request->input("odometer_reading");
        $vehicleParams = array(
            "registration_number" => $registration_no,
            "has_service_history" => $service_history,
            "has_service_book" => $service_books,
            "has_one_owner" => $one_owner,
            "odometer" => $odometer_reading
        );

        $overall_rating = $request->input("overall_rating");
        $overallRatingParams = array(
            "overall_rating" => $overall_rating
        );

        $wholesale_value_options = $request->input("wholesale_value_options");
        $retailObj = (object) array('retail'=>$wholesale_value_options);
        $wholesaleParams = array(
            "value_of_options" => $retailObj
        );

        $vehicle = $request->input("vehicle");
        $valuation_price = $request->input("valuation_price");
        $maximum_valuation_price = $request->input("maximum_valuation_price");
        $salesInfoParams = array(
            "sale_status" => $vehicle,
            "valuation_price" => $valuation_price,
            "max_valuation_price" => $maximum_valuation_price
        );

        $overallEstimatedCost = $request->input("overall_estimated_cost");
        $estimatedParams = array(
            "estimated_recon_cost" => floatval($overallEstimatedCost)
        );

        $offer_price = $request->input("offer_price");
        $offerPriceParams = array(
            "offer_price" => $offer_price
        );

        /** update VEHICLE DETAILS **/
        $api->updateValuations($token, $valuationId, $vehicleParams);

        /** update OVERALL RATING **/
        $api->updateOverallRating($token, $valuationId, $overallRatingParams);

        /** update SALES INFO **/
        $api->updateSalesInfo($token, $valuationId, $salesInfoParams);

        /** update RECONDITIONING COST **/
        $api->updateReconditioningCost($token, $valuationId, $estimatedParams);

        /** update OFFER PRICE **/
        $api->updateOfferPrice($token, $valuationId, $offerPriceParams);

        /** update WHOLESALE VALUE OPTIONS **/
        $api->updateWholesaleValues($token, $valuationId, $wholesaleParams);

        $result = json_decode($api->completeValuation($token, $valuationId));
        if(isset($result->id)){
            return 1;
        }
        return $result->error->detail;
    }

    public function acceptValuation(Request $request, $valuationld) {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        return $api->acceptValuation($token, $valuationld);
    }

    public function saveModelSpec(Request $request, $valuationId){
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        /** GET FORM INPUT **/
        $make = $request->input("make");
        $model_code = $request->input("model");
        $registration_number = $request->input("registration");
        $variant = $request->input("variant");
        $colour = $request->input("general_colour");
        $manufacturer_color = $request->input("manufacturer_colour");
        $first_reg_date = $request->input("year_first_registered");
        $registration_expiry_date = $request->input("registration_expiry_date");
        $registered_state = $request->input("registered_state");
        $build_date = $request->input("build_date");
        $compliance_date = $request->input("compliance_date");
        $engine_type = $request->input("engine");
        $engine_size = $request->input("engine_size");
        $engine_capacity = $request->input("engine_capacity");
        $cylinder = $request->input("cylinder");
        $fuel_type = $request->input("fuel_type");
        $transmission = $request->input("transmission");
        $drive_train = $request->input("drive_train");
        $body_no = $request->input("body_no");
        $vin = $request->input("vin");
        $chassis_number = $request->input("chassis_number");
        $trim = $request->input("trim_type");
        $trim_color = $request->input("trim_color");
        $body_type = $request->input("body_type");
        $glass_guide_code = $request->input("glass_code");

        $params = array(
            "make" => $make,
            "model_code" => $model_code,
            "registration_number" => $registration_number,
            "variant" => $variant,
            "colour" => $colour,
            "manufacturer_color" => $manufacturer_color,
            "first_reg_date" => $first_reg_date!=''?date("Y/m/d", strtotime($first_reg_date."-01-01")):"",
            "registration_expiry_date" => $registration_expiry_date!=''?date("Y/m/d", strtotime($registration_expiry_date)):"",
            "registered_state" => $registered_state,
            "build_date" => $build_date!=''?date("Y/m/d", strtotime($build_date)):"",
            "compliance_date" => $compliance_date!=''?date("Y/m/d", strtotime($compliance_date)):"",
            "engine_type" => $engine_type,
            "engine_size" => $engine_size,
            "engine_capacity" => $engine_capacity,
            "cylinder" => $cylinder,
            "fuel_type" => $fuel_type,
            "transmission" => $transmission,
            "drive_train" => $drive_train,
            "body_no" => $body_no,
            "vin" => $vin,
            "chassis_number" => $chassis_number,
            "trim" => $trim,
            "trim_color" => $trim_color,
            "body_type" => $body_type,
            "glass_guide_code" => $glass_guide_code
        );

        /** UPDATE VEHICLE DETAIL **/
        $result = $api->updateVehicle($token, $valuationId, $params);

        if(!property_exists($result, 'error')){
            return 1;
        }else{
            return 0;
        }
    }

    public function requestValuation(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        /** REQUEST VALUATION **/
        $valuationId = $request->input("valuation_id");
        $result = $api->requestValuation($token, $valuationId);
        if(!property_exists($result, 'error')){
            return "/valuations/requests";
            //return redirect("/valuations/requests");
        }
    }

    public function getInspectionItemsBySectionGroup(Request $request, $sectionGroup)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $valuation_id = $request->input("valuation_id");

        $inspectionItems = $api->getInspectionItem($token, $valuation_id, $sectionGroup);
        if(Helper::resultCheck($inspectionItems) === true){
            return $inspectionItems;
        }
        return Helper::resultCheck($inspectionItems);

    }

    public function uploadInspectionPhotoTemporary(Request $request)
    {
        //create image name
        //$image_file_type = pathinfo($_FILES["fileToUpload"]["name"],PATHINFO_EXTENSION);
        $image_file_type = "jpg";
        $valuation_id = $request->input("valuation_id");
        $valuation_version = $request->input("valuation_version");
        $sequence = $request->input("sequence_add_photo");
        $image_name = $valuation_id. "_" . $valuation_version . '_' . $sequence.".".$image_file_type;

        LOG::debug("UPLOAD TEMPORARY IMAGE - image name: ".$image_name);

        //upload image to tmp folder
        $file = $_FILES["fileToUpload"]["tmp_name"];
        $fileContent = file_get_contents($file);
        Storage::disk('uploads_inspection')->put($image_name, $fileContent);
        if(Storage::disk('uploads_inspection')->exists($image_name)){
            //return Storage::disk('uploads_inspection')->url($image_name);
            return '/images_tmp/inspect_item_photo_tmp/'.$image_name."?".rand();
        }
    }

    public function addInspectionItemWithPhoto(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $valuation_id = $request->input("valuation_id");
        $valuation_version = $request->input("valuation_version");
        $inspection_item_id = $request->input("inspection_item_id");
        $repair_cost = $request->input("repair_cost");
        $note = $request->input("note");

        $params = array(
            "inspectionItemId" => $inspection_item_id,
            "valuationId" => $valuation_id,
            "repairCost" => $repair_cost,
            "note" => $note
        );

        $result = json_decode($api->addInspectionItem($token, $params));

        if(Helper::resultCheck($result) === true){
            $i = 1;
            while($i<4){
                $image_name = $valuation_id."_".$valuation_version."_".$i.".jpg";
                LOG::debug("ADD INSPECTION ITEM - IMAGE NAME: ".$image_name);
                if(Storage::disk('uploads_inspection')->exists($image_name)){
                    $image_content = Storage::disk('uploads_inspection')->get($image_name);
                    $new_image_name = $valuation_id."_".$valuation_version."_".$inspection_item_id."_".$i.".jpg";
                    $new_image_name = basename($new_image_name);
                    LOG::debug("ADD INSPECTION ITEM - new image name: ".$new_image_name);
                    $azure = new \App\Library\AzureCloud();
                    $image = $azure->uploadBlobImage($image_content, $new_image_name, "inspection-item");
                    if($image){
                        LOG::debug("ADD INSPECTION ITEM - added successfully");
                        Storage::disk('uploads_inspection')->delete($image_name);
                    }

                }
                $i++;
            }
            return 1;
        }
        return Helper::resultCheck($result);
    }

}
