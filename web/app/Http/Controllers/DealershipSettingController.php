<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DealershipSettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('handling-channel');
        $this->middleware('get-menu-access');
    }

    public function initSettingDetailsForm(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $dealerSettings = json_decode($api->getDealershipSettings($token));

        $data = array(
            "header" => "Configure",
            "breadcrumb" => "Settings",
            "dealerSettings" => $dealerSettings
        );

        $data['js'][] = 'dealership/initSettingDetailsForm';

        return view("main.dealership.settings", $data);
    }

    public function saveSettings(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        /** GET INPUT FOR DEALERSHIP SETTINGS **/
        /** CHECK BOX **/
        $head_valuer_approval_input = $request->input("head_valuer_approval");
        $head_valuer_approval = isset($head_valuer_approval_input) && $head_valuer_approval_input ? "1" : "0";

        $receive_external_offers_input = $request->input("receive_external_offers");
        $receive_external_offers = isset($receive_external_offers_input) && $receive_external_offers_input ? "1" : "0";

        $test_drive_terms = $request->input("test_drive_terms");

        $params = array(
            "head_valuer_approval" => $head_valuer_approval,
            "receive_external_offer" => $receive_external_offers,
            "test_drive_terms" => str_replace("'","\\'", $test_drive_terms)
        );

        $result = json_decode($api->saveDealershipSettings($token, $params));

        if(!isset($result->error)){
            return 1;
        }else{
            if(isset($result->error->code) && $result->error->code == "access_denied"){
                return $result->error->code;
            }
            return 0;
        }
    }
}
