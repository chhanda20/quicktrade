<?php
/**
 * Created by PhpStorm.
 * User: Son
 * Date: 4/27/2017
 * Time: 2:51 PM
 */

namespace App\Http\Controllers\MOAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\Helper;
use Illuminate\Support\Facades\Redirect;

class AuctionSessionController extends Controller
{
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('handling-channel');
        $this->middleware('get-menu-access');
    }

    public function initAuctionSession(Request $request, $type, $session_id=0)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $data = array(
            "headers" => "Auction",
        );

        $data["type"] = $type;
        $trading_setting_list = json_decode($api->getAuctionSettings($token));
        if(!isset($trading_setting_list->error)){
            $data["auction_default_duration"] = $trading_setting_list->auction_default_duration;
            $data["auction_price_increment"] = $trading_setting_list->auction_price_increment;
        }
        if($type == "new"){
            $data["breadcrumb"] = "New";
        }else if($type == "edit"){
            $data["breadcrumb"] = "Edit";
            $result = json_decode($api->getAuctionSessionById($token, $session_id));
            if(!isset($result->error)){
                $data['auction_session_detail'] = $result;
            }
        }
        $data["breadcrumb_auction"] = $data["breadcrumb"];
        return view("mo-admin.main.trading.create-auction-session", $data);
    }

    
    public function createAuctionSession(Request $request)
    {

        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');
        /** Get value from auction session form **/

            /** Checkbox recurring or ad-hoc **/
        $recurring_input = $request->input("recurring");
        $recurring = isset($recurring_input) && $recurring_input ? "1" : "0";

        $ad_hoc_input = $request->input("ad_hoc");
        $ad_hoc = isset($ad_hoc_input) && $ad_hoc_input ? "1" : "0";

        $session_type = "";
        if($recurring != 0){
            $session_type = "recurring";
        }else{
            if($ad_hoc != 0){
                $session_type = "adhoc";
            }
        }
            /** Checkbox recurring date **/
        $recurring_date = $request->input("day");

            /** Auction Session Fields of RECURRING and AD-HOC**/
        $name = $request->input("name");
        $start_date = $request->input("start_date");
        $end_date = $request->input("end_date");
        $start_time = $request->input("start_time");
        $duration_default = $request->input("duration_default");
        $duration_custom = $request->input("duration_custom");
        $bid_increment_default = $request->input("bid_increment_default");
        $bid_increment_custom = $request->input("bid_increment_custom");

        $duration = "";
        if($duration_default != ""){
            $duration = $duration_default;
        }else if($duration_custom != ""){
            $duration = $duration_custom;
        }

        $bid_price_increment = "";
        if($bid_increment_default != ""){
            $bid_price_increment = $bid_increment_default;
        }else if($bid_increment_custom != ""){
            $bid_price_increment = $bid_increment_custom;
        }

        $params = array(
            "name" => $name,
            "sessionType" => $session_type,
            "startDate" => $start_date,
            "endDate" => $end_date,
            "time" => $start_time,
            "recurringDate" => $recurring_date,
            "duration" => $duration,
            "bid_price_increment" => $bid_price_increment
        );

        $result = json_decode($api->createAuctionSessionTrading($token, $params));
        if(!isset($result->error)){
//            return redirect("/mo-admin/trading/auction-session-list");
            return 1;
        }
//        return redirect("/mo-admin/create-auction-session/new");
        return $result->error->code;

    }

    public function updateAuctionSession(Request $request, $sessionId)
    {
    
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $start_date = $request->input("start_date");
        $start_time = $request->input("start_time");
        $name = $request->input("name");
        $duration_default = $request->input('duration_default');
        $duration_custom = $request->input("duration_custom");
        $bid_increment_default = $request->input("bid_increment_default");
        $bid_increment_custom = $request->input("bid_increment_custom");

        $duration = "";
        if($duration_default != ""){
            $duration = $duration_default;
        }else if($duration_custom != ""){
            $duration = $duration_custom;
        }

        $bid_price_increment = "";
        if($bid_increment_default != ""){
            $bid_price_increment = $bid_increment_default;
        }else if($bid_increment_custom != ""){
            $bid_price_increment = $bid_increment_custom;
        }

        $params = array(
            'sessionId' => $sessionId,
            'startDate' => $start_date,
            'time' => $start_time,
            'duration' => $duration,
            "bid_price_increment" => $bid_price_increment,
            "name" => $name
        );
        $result = json_decode($api->updateAuctionSessionTrading($token, $params, $sessionId));
        if(!isset($result->error)){
            return 1;
        }
        return $result->error->code;
    }


    public function viewAuctionSession(Request $request)
    {
        $channel = $request->channel;
        $data = array(
            "header" => "Auction",
            "breadcrumb" => "Auctions Session",
            "channel" => $channel
        );
        $data["breadcrumb_auction"] = $data["breadcrumb"];
        return view("mo-admin.main.trading.auction-session", $data);
    }

    public function viewAuctionHistory(Request $request)
    {
        $channel = $request->channel;
        $data = array(
            "header" => "Auction",
            "breadcrumb" => "Auction History",
            "channel" => $channel
        );
        $data["breadcrumb_auction"] = $data["breadcrumb"];
        return view("mo-admin.main.trading.auction-history", $data);
    }

    public function viewHistoryDetail(Request $request, $auctionSessionId)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $channel = $request->channel;
        $data = array(
            "header" => "Auction",
            "breadcrumb" => "Auction History Detail",
            "channel" => $channel
        );
        $params = [
//            "limit" => $limit,
//            "offset" => $offset
        ];
        $historyList = [];
        $sessionData = json_decode($api->auctionSessionHistory($token, $auctionSessionId, $params));
        if (isset($sessionData->value) && is_array($sessionData->value) && $sessionData->value) {
            $historyList = collect($sessionData->value)->map(function ($auction) {
                $year = '';
                if ($auction->vehicle_build_date) {
                    $dateArr = explode('-', $auction->vehicle_build_date);
                    $year = $dateArr[0];
                }
                $auction->vehicle_name = implode(' ', [$auction->vehicle_make, $auction->vehicle_model_family, $year, $auction->vehicle_variant]);
                $auction->buyer = implode(' ', [$auction->buyer_first_name, $auction->buyer_last_name]);
                return $auction;
            });
        }
        $data["breadcrumb_auction"] = $data["breadcrumb"];
        $data['historyList'] = $historyList;
        $data['js'][] = 'mo-admin/auction/viewHistoryDetail';
        return view("mo-admin.main.trading.auction-history-detail", $data);
    }

    public function getAllAuctionSession(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $limit = 20;
        $offset = $request->input('offset');
        
        $params = array(
            "limit" => $limit,
            "offset" => $offset
        );
        $result = json_decode($api->getAllAuctionSession($token, $params));
        if(!isset($result->error)){

            $result_list = array('data' => array());
            foreach($result->sessionList as $key=>$value){
                $arr = array(
                    0=>$value->id,
                    1=>$value->date,
                    2=>$value->time,
                    3=>$value->duration,
                    4=>$value->custom_bid_price_increment,
                    5=>$value->vehicle
                );
                array_push($result_list['data'], $arr);
            }
            $result_list['count'] = $result->list_size->count;

            if(!$result_list["data"]){
                return null;
            }
            return json_encode($result_list);
        }else{
            return null;
        }

    }

    public function getAllAuctionSessionHistory(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $limit = 20;
        $offset = $request->input('offset');

        $params = array(
            "history" => true,
            "limit" => $limit,
            "offset" => $offset
        );
        $result = json_decode($api->getAllAuctionSession($token, $params));
        if(!isset($result->error)){

            $result_list = array('data' => array());
            foreach($result->sessionList as $key=>$value){
                $arr = array(
                    0=>$value->id,
                    1=>$value->name,
                    /*2=>$value->date,*/
                    2=>date('d-m-Y', strtotime($value->date)),
                    3=>$value->time,
                    4=>$value->duration,
                    5=>$value->vehicle
                );
                array_push($result_list['data'], $arr);
            }
            $result_list['count'] = $result->list_size->count;

            if(!$result_list["data"]){
                return null;
            }
            return json_encode($result_list);
        }else{
            return null;
        }

    }

    public function showRequestList(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $data = array(
            "header" => "Auction",
            "breadcrumb" => "Request List"
        );

        $limit = 6;
        $offset = 0;

        $params = array(
            "limit" => $limit,
            "offset" => $offset
        );

        $query = $request->input("mo-request-list-search-input");

        if(isset($query) && !empty($query)) {
            $params["query"] = $query;
            $data["query"] = $query;
        }

        $result = json_decode($api->getRequestList($token, $params));

        if(isset($result->requestList) && !empty($result->requestList)) {
            $data["request_list"] = $result->requestList;
        } else {
            $data["request_list"] = null;
        }

        //get reason reject list
        $result = json_decode($api->getAuctionSettings($token, $params));
        if(isset($result->rejectReason) && !empty($result->rejectReason)) {
            $data["reject_reason_list"] = $result->rejectReason;
        }

        return view("mo-admin.main.trading.request-list", $data);
    }

    public function showRequestListByOffset(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $offset = $request->input("offset");
        if (!isset($offset)) {
            $offset = 0; 
        }

        $limit = $request->input('limit');
        if (!isset($limit)) {
            $limit = 21;
        }

        $params = array(
            "limit" => $limit,
            "offset" => $offset
        );

        $odometer = $request->input("odo_range");
        $year = $request->input("year_range");
        $trade_price = $request->input("price_range");
        $seller_rating = $request->input("rating_range");
        $location = $request->input("location");
        $transmissionType = $request->input("transmissionType");
        $makeInfo = $request->input("makeInfo");
        $orderBy = $request->input("orderBy");
        $orderDesc = $request->input("orderDesc");
        $query = $request->input("query");

        if(Helper::arrayCheck($odometer)){
            $params["odometer"] = $odometer;
        }

        if(Helper::arrayCheck($year)){
            $params["year"] = $year;
        }

        if(Helper::arrayCheck($trade_price)){
            $params["trade_price"] = $trade_price;
        }

        if(Helper::arrayCheck($seller_rating)){
            $params["seller_rating"] = range($seller_rating[0], $seller_rating[1]);
        }

        if(Helper::arrayCheck($location)){
            $params["location"] = $location;
        }

        if(Helper::arrayCheck($transmissionType)){
            $params["transmissionType"] = $transmissionType;
        }

        if(Helper::arrayCheck($makeInfo)){
            $params["makeInfo"] = $makeInfo;
        }

        if(!empty($orderBy)){
            $params["orderBy"] = $orderBy;
        }

        if(!empty($orderDesc)){
            $params["orderDesc"] = $orderDesc === 'true'? true: false;
        }

        if(!empty($query)){
            $params["query"] = $query;
        }


        /*$query = $request->input("query");

        if(isset($query) && !empty($query)) {
            $params["query"] = $query;
        }*/

        $result = json_decode($api->getRequestList($token, $params));

        
        if(isset($result->requestList) && !empty($result->requestList)) {
            $data["request_list"] = $result->requestList;
        } else {
            $data["request_list"] = null;
        }
        
        return view("mo-admin.main.trading.request-sample-list", $data);
    }

    public function approveRequestList(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $request_id = $request->input("request_id");
        $action = "approve";

        $session_id = $request->input("auction_session_id");

        $params = array(
            "request_id" => $request_id,
            "action" => $action,
            "session_id" => $session_id
        );

        return $api->approveOrRejectRequestList($token, $params);
    }

    public function rejectRequestList(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $request_id = $request->input("request_id");
        $action = "reject";
        $comment = $request->input("comment");
        $reason = $request->input("reason");

        $session_id = null;

        $params = array(
            "request_id" => $request_id,
            "action" => $action,
            "comment" => $comment,
            "reject_code" => $reason
        );

        return $api->approveOrRejectRequestList($token, $params);
    }

    public function viewLiveAuctionList(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $channels = $request->channels;
        
        //$channels = "auctionbid[10]";
        $data = array(
            "header" => "Auction",
            "breadcrumb" => "Live Auction",
            "channels" => $channels
        );

        //step1: get active auction sessions
        $active_session = json_decode($api->getActiveAuctionSessions($token));
        //step2: retrieve id from active auction sessions to get vehicles
        $data["live_auction_vehicle_list"] = array();
        if(isset($active_session) && !empty($active_session)) {
            //$query = $request->input("mo-live-auction-search");
            $params = array("offset" => 0,
                            "limit" => 6);
            foreach ($active_session as $key => $value) {

                //subscribe live auction channel
                $auction_channel = new \stdClass();
                $auction_channel->name = "auction_bid_".$value->id;
                $auction_channel->live_auction = "auction_channel";
                if(!isset($channels) && empty($channels)) {
                    $channels = array();
                }
                array_push($channels, $auction_channel);

                $live_auction_vehicle_list = json_decode($api->findVehiclesInAuctionSession($token, $value->id, $params));
                
                if(isset($live_auction_vehicle_list) && !empty($live_auction_vehicle_list)) {
                    $data["live_auction_vehicle_list"] += $live_auction_vehicle_list;
                }
                //get auction session id temp
                $data["auction_session_id"] = $value->id;
            }
            //re-assign channels
            $data["channels"] = $channels;

        }

        //get auction setting
        $auction_setting = json_decode($api->getAuctionSettings($token));
        if(isset($auction_setting) && !empty($auction_setting)) {
            $data["auction_price_increment"] = $auction_setting->auction_price_increment;
        }

        //live auction timer
        $api_system_datetime = json_decode($api->getSystemProperty());

        if (isset($api_system_datetime) && !empty($api_system_datetime)) {
            $api_system_datetime = new \DateTime($api_system_datetime->date);
            if(isset($active_session) && !empty($active_session)) {
                $active_auction_session_created_datetime = new \DateTime($active_session[0]->startDate);
                $seconds = $api_system_datetime->diff($active_auction_session_created_datetime)->format('%a') * 24 * 3600;
                $seconds += $api_system_datetime->diff($active_auction_session_created_datetime)->format('%h') * 3600;
                $seconds += $api_system_datetime->diff($active_auction_session_created_datetime)->format('%i') * 60;
                $seconds += $api_system_datetime->diff($active_auction_session_created_datetime)->format('%s');
                
                $seconds = ($active_session[0]->duration * 60) - $seconds;
                $auction_session_time_remaining = $seconds;
                $data["live_auction_timer"] = gmdate("H:i:s", $auction_session_time_remaining);
                $data["live_auction_seconds_timer"] = $auction_session_time_remaining;
            }
        }

        //get next auction session information
        $next_auction_session = json_decode($api->getNextAuctionSession($token));
        if(isset($next_auction_session) && !empty($next_auction_session)) {
            $next_auction_session_start_date = new \DateTime($next_auction_session[0]->startDate);
            
            $seconds = $next_auction_session_start_date->diff($api_system_datetime)->format('%a') * 24 * 3600;
            $seconds += $next_auction_session_start_date->diff($api_system_datetime)->format('%h') * 3600;
            $seconds += $next_auction_session_start_date->diff($api_system_datetime)->format('%i') * 60;
            $seconds += $next_auction_session_start_date->diff($api_system_datetime)->format('%s');

            // extract hours
            $next_auction_hours = floor($seconds / (60 * 60));
            if(strlen($next_auction_hours) == 1) {
                $next_auction_hours = sprintf("%02d", $next_auction_hours);
            }

            // extract minutes
            $divisor_for_minutes = $seconds % (60 * 60);
            $next_auction_minutes = floor($divisor_for_minutes / 60);
            if(strlen($next_auction_minutes) == 1) {
                $next_auction_minutes = sprintf("%02d", $next_auction_minutes);
            }

            // extract the remaining seconds
            $divisor_for_seconds = $divisor_for_minutes % 60;
            $next_auction_seconds = ceil($divisor_for_seconds);
            if(strlen($next_auction_seconds) == 1) {
                $next_auction_seconds = sprintf("%02d", $next_auction_seconds);
            }

            $data["up_next_auction_session_seconds"] = $seconds;
            $data["up_next_auction_session_h_m_s"] = $next_auction_hours.":".$next_auction_minutes.":".$next_auction_seconds;
        }

        $data["css"][] = "star-rating";
        return view("mo-admin.main.trading.live-auction", $data);
    }

    public function getLiveAuctionTimer (Request $request) {

        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $api_system_datetime = json_decode($api->getSystemProperty());
        $active_session = json_decode($api->getActiveAuctionSessions($token));

        if (isset($api_system_datetime) && !empty($api_system_datetime)) {
            $api_system_datetime = new \DateTime($api_system_datetime->date);
            if(isset($active_session) && !empty($active_session)) {
                $active_auction_session_created_datetime = new \DateTime($active_session[0]->startDate);
                $seconds = $api_system_datetime->diff($active_auction_session_created_datetime)->format('%a') * 24 * 3600;
                $seconds += $api_system_datetime->diff($active_auction_session_created_datetime)->format('%h') * 3600;
                $seconds += $api_system_datetime->diff($active_auction_session_created_datetime)->format('%i') * 60;
                $seconds += $api_system_datetime->diff($active_auction_session_created_datetime)->format('%s');
                
                $seconds = ($active_session[0]->duration * 60) - $seconds;
                $auction_session_time_remaining = $seconds;

                //$data["live_auction_timer"] = gmdate("H:i:s", $auction_session_time_remaining);

                return $auction_session_time_remaining;
            }
        }

    }
}