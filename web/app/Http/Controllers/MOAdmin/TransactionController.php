<?php

namespace App\Http\Controllers\MOAdmin;

use App\Library\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('handling-channel');
        $this->middleware('get-menu-access');
    }

    /**
     * Return List of Transaction (without data)
     *
     * @route /transaction/view-transactions
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function viewTransactions(Request $request, $type = "current")
    {
        $channels = $request->channels;
        $data = array(
            "header" => "Transaction",
            "breadcrumb" => $type,
            "type" => $type,
            "channels" => $channels
        );

        $data['tabs'] = [
            [
                'id' => 'all',
                'name' => 'All'
            ],
            [
                'id' => 'awaiting-payment',
                'name' => 'Awaiting Payment'
            ],
            [
                'id' => 'organise-transport',
                'name' => 'Organise Transport'
            ],
            [
                'id' => 'payouts',
                'name' => 'Payouts'
            ]
        ];

        $data['js'][] = 'mo-admin/transaction/viewTransactions-filter';

        $data['js'][] = 'mo-admin/transaction/viewTransactions';

        return view('mo-admin.main.transaction.transaction-main', $data);
    }

    /**
     * Return List Data of Transaction
     *
     * @route /transaction/get-transactions
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function getTransactions(Request $request, $type, $statusType = "")
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        /*$limit = $request->input('limit');*/
        $offset = $request->input('offset');
        $active = (bool)$request->input('active');
        $status_datatable = $request->input('status');
        $year = $request->input("year_range");
        $makeInfo = $request->input("makeInfo");

        Log::debug("mo - getTransactions - type: ".$statusType);

        $showOutstandingAmount = true;
        switch($statusType){
            case "awaiting-payment":
                $status = [1];
                break;
            case "organise-transport":
                $status = [20,22,23,24,3];
                $showOutstandingAmount = false;
                break;
            case "payouts":
                $status = [5,26,27];
                $showOutstandingAmount = false;
                break;
            default:
                $status = [];
                break;
        }

        if(Helper::arrayCheck($status_datatable)){
            $status = $status_datatable;
        }

        Log::debug("mo - getTransactions - active: ".$active);
        Log::debug("mo - getTransactions - status_datatable: ".json_encode($status_datatable));

        if($active === true){//required action
            Log::debug("mo - getTransactions - inside");

            switch($statusType){
                case "awaiting-payment":
                    // remove 'vehicle picked up', 'transport booked'
                    $initialStatus = [1,24,28];
                    break;
                case "organise-transport":
                    // remove 'receive payment'
                    $initialStatus = [22,23,24,28];
                    break;
                case "payouts":
                    $initialStatus = [0];
                    break;
                default:
                    $initialStatus = [1,22,23,24,28];
            }

            if(Helper::arrayCheck($status_datatable)){
                $status_required_action = $initialStatus;
                $status = array_intersect($status_datatable, $status_required_action);
                if(count($status) == 0){
                    $status = [0];
                }
            }else{
                $status = $initialStatus;
            }
        }

        Log::debug("mo - getTransactions - match status: ".json_encode(array_values($status)));

        if($type == "complete"){
            $status = [5];
        }

        $params = array(
            "limit" => 20,
            "offset" => $offset,
            "status" => array_values($status)
        );

        if(Helper::arrayCheck($year)){
            $params["year"] = $year;
        }

        if(Helper::arrayCheck($makeInfo)){
            $params["makeInfo"] = $makeInfo;
        }

        Log::debug("mo - getTransactions - params: ".json_encode($params));

        $transactionList = json_decode($api->getMOTransactions($token, $params));
        if(Helper::resultCheck($transactionList) === true){
            $transactionStatuses = $api->getTypeCode($token, array("type" => "TRANSACTION_STATUS"));
            if(Helper::resultCheck($transactionList) === true){
                $collection = collect($transactionList)->map(function ($item, $key) use ($transactionStatuses) {
                    $item->status_value = "";
                    foreach ($transactionStatuses as $transactionStatus){
                        switch($item->status){
                            case $transactionStatus->code:
                                $item->status_value = $transactionStatus->value;
                                break;
                            case 20:
                                $item->status_value = "Preparing Vehicle";
                                break;
                            /*case 25:
                                $item->status_value = "Arrived";
                                break;*/
                            case 26:
                                $item->status_value = "Awaiting Document";
                                break;
                            case 27:
                                $item->status_value = "Documents Received";
                                break;
                            case 28:
                                $item->status_value = "Waiting to return";
                                break;
                        }
                    }
                    $item->outstanding_amount = number_format($item->vehicle_price + $item->shipping_price + $item->buyer_fee);
                    return $item;
                });
                $transactionList = json_decode($collection);
            }

            $transactions_list_response_json = array('data' => array ());
            foreach ($transactionList as $transaction) {
                if(!empty($transaction)){
                    $arr = array(
                        0 => $transaction->id,
                        1 => $transaction->rid,
                        2 => $transaction->selling_dealer,
                        3 => $transaction->buying_dealer,
                        4 => $transaction->vehicle_name
                    );
                    $arr_after = array(
                        5 => $transaction->status_value,
                        6 => $transaction->status
                    );
                    if($showOutstandingAmount){
                        $arr_after = array(
                            5 => $transaction->outstanding_amount,
                            6 => $transaction->status_value,
                            7 => $transaction->status
                        );
                    }
                    $arr = array_merge($arr, $arr_after);
                    array_push($transactions_list_response_json["data"], $arr);
                }
            }

            $result = json_decode($api->getMOTransactionCount($token, $params));
            if(Helper::resultCheck($result)){
                $transactions_list_response_json["count"] = $result->count;
            }

            if (!$transactions_list_response_json["data"]) {
                return null;
            }
            return json_encode($transactions_list_response_json);
        }else{
            return null;
        }
    }

    public function receivePayment(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $transactionRid = $request->input('transaction_rid');
        $paymentAmount = $request->input('payment_amount');

        $params = array(
            "payments"=>[(object)(
                array(
                    "rid"=>$transactionRid,
                    "amount"=>$paymentAmount
                )
            )]
        );

        Log::debug("mo - receivePayment - params: ".json_encode($params));

        $result = $api->transactionPaymentReceived($token, $params);
        if(Helper::resultCheck($result) === true){
            return 1;
        }
        return Helper::resultCheck($result);
    }

    public function bookTransport(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $transactionId = $request->input('transaction_id');
        $transport_no = $request->input('transport_no');

        Log::debug("mo - bookTransport - transactionId: ".json_encode($transactionId));

        $result = $api->transactionTransportBooked($token, $transactionId, array("transport_no"=>$transport_no));
        if(Helper::resultCheck($result) === true){
            return 1;
        }
        return Helper::resultCheck($result);
    }

    public function pickUpVehicle(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $transactionId = $request->input('transaction_id');

        Log::debug("mo - pickUpVehicle - transactionId: ".json_encode($transactionId));

        $result = $api->transactionInTransit($token, $transactionId);
        if(Helper::resultCheck($result) === true){
            return 1;
        }
        return Helper::resultCheck($result);
    }

    public function shippedVehicle(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $transactionId = $request->input('transaction_id');

        Log::debug("mo - shippedVehicle - transactionId: ".json_encode($transactionId));

        $result = $api->transactionShipped($token, $transactionId);
        if(Helper::resultCheck($result) === true){
            return 1;
        }
        return Helper::resultCheck($result);
    }

    public function returnVehicle($token, $transactionId)
    {
        $api = new \App\Library\APICaller();

        Log::debug("mo - returnVehicle - transactionId: ".json_encode($transactionId));

        $result = $api->transactionReturning($token, $transactionId);
        if(Helper::resultCheck($result) === true){
            return 1;
        }
        return Helper::resultCheck($result);
    }

    public function awaitingDocument(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $transactionId = $request->input('transaction_id');

        Log::debug("mo - awaitingDocument - transactionId: ".json_encode($transactionId));

        $result = $api->transactionDocumenting($token, $transactionId);
        if(Helper::resultCheck($result) === true){
            return 1;
        }
        return Helper::resultCheck($result);
    }

    public function completeTransaction(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $transactionId = $request->input('transaction_id');
        $expectedStatus = $request->input('expected_status');//5 for complete, 27 for documented

        Log::debug("mo - completeTransaction - transactionId: ".json_encode($transactionId));
        Log::debug("mo - completeTransaction - expectedStatus: ".json_encode($expectedStatus));

        $result = $api->completeTransaction($token, $transactionId, array("status"=>$expectedStatus));
        if(Helper::resultCheck($result) === true){
            return 1;
        }
        return Helper::resultCheck($result);
    }

    public function transactionDetails(Request $request, $type, $transactionId)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $channels = $request->channels;
        $data = array(
            "header" => "Transaction",
            "breadcrumb" => $type,
            "type" => $type,
            "channels" => $channels
        );

        $data["transactionDetails"] = "";
        $transactionDetails = json_decode($api->getTransactionById($token, $transactionId));
        $transactionStatuses = $api->getTypeCode($token, array("type" => "TRANSACTION_STATUS"));
        if(Helper::resultCheck($transactionDetails) === true){
            $transactionActionButton = "";
            switch ($transactionDetails->status){
                case 1://waiting payment
                    $transactionActionButton = "receive payment";
                    break;
                case 22://booking transport
                    $transactionActionButton = "transport booked";
                    break;
                case 23://booked transport
                    $transactionActionButton = "vehicle picked up";
                    break;
                case 24://in transit
                    $transactionActionButton = "vehicle arrived";
                    break;
                case 28://waiting to return
                    $transactionActionButton = "vehicle returned";
                    break;
                case 26://awaiting document
                    $transactionActionButton = "complete";
                    break;
                case 27://document approved
                    $transactionActionButton = "complete";
                    break;
            }

            Log::debug("transactionDetails: ".$transactionDetails->status);
            $status_value = Helper::getTypeName($transactionStatuses, $transactionDetails->status);

            switch($transactionDetails->status){
                case 20:
                    $status_value= "Preparing Vehicle";
                    break;
                /*case 25:
                    $status_value = "Arrived";
                    break;*/
                case 26:
                    $status_value = "Awaiting Document";
                    break;
                case 27:
                    $status_value = "Documents Received";
                    break;
                case 28:
                    $status_value = "Waiting to return";
                    break;
            }

            $transactionDetails->status_value = $status_value;


            $transactionDetails->action_button = $transactionActionButton;

            /** FORMAT SELLER PRICE **/
            $transactionDetails->sell_vehicle_price_format = 0;
            $transactionDetails->sell_mo_fee_format = 0;
            $transactionDetails->sell_dispute_adjust_format = 0;
            if($transactionDetails->sell_vehicle_price != null){
                $transactionDetails->sell_vehicle_price_format = number_format($transactionDetails->sell_vehicle_price);
            }
            if($transactionDetails->sell_mo_fee != null){
                $transactionDetails->sell_mo_fee_format = number_format($transactionDetails->sell_mo_fee);
            }
            if($transactionDetails->sell_dispute_adjust != null){
                $transactionDetails->sell_dispute_adjust_format = number_format($transactionDetails->sell_dispute_adjust);
            }

            $transactionDetails->sell_pay_amount_format = number_format($transactionDetails->sell_vehicle_price - $transactionDetails->sell_mo_fee - $transactionDetails->sell_dispute_adjust);

            /** FORMAT BUYER PRICE **/
            $transactionDetails->buy_vehicle_price_format = 0;
            $transactionDetails->buy_transport_format = 0;
            $transactionDetails->buy_mo_fee_format = 0;
            $transactionDetails->buy_payment_made_format = 0;
            $transactionDetails->buy_dispute_adjust_format = 0;
            if($transactionDetails->buy_vehicle_price != null){
                $transactionDetails->buy_vehicle_price_format = number_format($transactionDetails->buy_vehicle_price);
            }
            if($transactionDetails->buy_transport != null){
                $transactionDetails->buy_transport_format = number_format($transactionDetails->buy_transport);
            }
            if($transactionDetails->buy_mo_fee != null){
                $transactionDetails->buy_mo_fee_format = number_format($transactionDetails->buy_mo_fee);
            }
            if($transactionDetails->buy_payment_made != null){
                $transactionDetails->buy_payment_made_format = number_format($transactionDetails->buy_payment_made);
            }
            if($transactionDetails->buy_dispute_adjust != null){
                $transactionDetails->buy_dispute_adjust_format = number_format($transactionDetails->buy_dispute_adjust);
            }

            $transactionDetails->buy_pay_amount_format = number_format($transactionDetails->buy_vehicle_price + $transactionDetails->buy_transport + $transactionDetails->buy_mo_fee - $transactionDetails->buy_payment_made + $transactionDetails->buy_dispute_adjust);

            /** GET TRANSACTION DOCUMENTS **/
            $documents = json_decode($api->getTransactionDocuments($token, $transactionId));
            if(Helper::resultCheck($documents) === true){
                if($documents->uploadedDocuments != null){
                    foreach ($documents->uploadedDocuments as $document){
                        if(strpos($document, $transactionDetails->id."_vehicle_transfer_form") !== false)
                        {
                            $data["vehicle_transfer_doc"] = $document;
                        }
                        if(strpos($document, $transactionDetails->id."_vehicle_transfer_additional") !== false)
                        {
                            $data["vehicle_transfer_additional_doc"] = $document;
                        }
                        if(strpos($document, $transactionDetails->id."_invoice_vehicle_minus_dispute") !== false)
                        {
                            $data["invoice_vehicle_doc"] = $document;
                        }
                        if(strpos($document, $transactionDetails->id."_ppsr_financial_payout") !== false)
                        {
                            $data["ppsr_financial_payout_doc"] = $document;
                        }
                    }
                }

                $data["documents"] = $documents->uploadedDocuments;
            }

            /** GET TRANSACTION DOCUMENT TYPES **/
            $params = array(
                "types" => ["transport-fee", "fee", "credit-note-cost-variance", "credit-note-return-vehicle", "payment-receipt"]
            );
            $document_types = json_decode($api->getDocumentOfTransaction($token, $params, $transactionId));
            if(Helper::resultCheck($document_types) === true){
                foreach($document_types as $key=>$value){
                    if($value->document_type == "transport-fee"){
                        $data["invoice_buyer_document"] = $value->document_uploaded;
                    }
                    if($value->document_type == "fee" || $value->document_type == "transport-fee-2"){
                        $data["invoice_seller_document"] = $value->document_uploaded;
                    }
                    if($value->document_type == "credit-note-cost-variance" || $value->document_type == "credit-note-return-vehicle"){
                        $data["credit_note_document"] = $value->document_uploaded;
                    }
                }
                $data["document_types"] = $document_types;
            }

            $data['js'][] = 'mo-admin/transaction/transactionDetails';

            $data["transactionDetails"] = $transactionDetails;
        }

        return view('mo-admin.main.transaction.transaction-details', $data);
    }

    public function getStatuses(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $transactionStatuses = $api->getTypeCode($token, array("type" => "TRANSACTION_STATUS"));

        if(Helper::resultCheck($transactionStatuses) === true){
            foreach ($transactionStatuses as $key=>$value){
                if(in_array($value->code, [9,10])){
                    unset($transactionStatuses[$key]);
                }
            }
            return $transactionStatuses;
        }
        return null;
    }

    public function getTransactionFee(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $channels = $request->channels;
        $data = array(
            "header" => "Settings",
            "breadcrumb" => "Fees",
            "channels" => $channels
        );

        $data["regions"] = json_decode($api->getRegionsList($token));
        $data["transaction_fees"] = json_decode($api->getTransactionFees($token));

        $data['js'][] = 'mo-admin/transaction/editTransactionFees';

        return view("mo-admin.main.transaction.setting-fees", $data);
    }

    public function setTransactionFee(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $buyer_fee = $request->input("buyer_fee");
        $seller_fee = $request->input("seller_fee");
        $transport_prices = $request->input("transport_prices");

        $params = array("buyer_fee" => $buyer_fee,
                        "seller_fee" => $seller_fee,
                        "transport_prices" => $transport_prices);

        return $api->setTransactionFees($token, $params);
    }
}
