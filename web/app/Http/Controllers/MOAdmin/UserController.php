<?php

namespace App\Http\Controllers\MOAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('handling-channel');
        $this->middleware('get-menu-access');
    }

    public function createForm(Request $request, $user_id = 0)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $user_info = $request->session()->get("user_info");
        $company_group_id = $user_info->company_group_id;

        $channels = $request->channels;
        // Get Austrlia only
        $params['code'] = ['aus'];
        $country_list = $api->getCountryByCode($token, $params);
        $genders = $api->getTypeCode($token, array("type" => "GENDER_TYPE"));
        $titles = $api->getTypeCode($token, array("type" => "TITLE_TYPE"));
        $privacies_flag = $api->getTypeCode($token, array("type" => "PRIVACY_FLAG"));
        $listCompanyGroups = json_decode($api->listCompanyGroups($token));
        $permissions = json_decode($api->permissionByGroup($token));

        $data = array(
            "header" => "User",
            "country_list" => $country_list,
            "genders" => $genders,
            "titles" => $titles,
            "privacies_flag" => $privacies_flag,
            "channels" => $channels,
            "listCompanyGroups" => $listCompanyGroups,
            "permissions" => $permissions
        );
        if ($user_id != 0)
        {
            $data["breadcrumb"] = "Edit";
            $data["mode"] = "edit";
            $data["user_data"] = json_decode($api->getUserById($token, $user_id));
            $userPermissions = json_decode($api->getUserPermission($token, $user_id));
            $selectedPermission = [];
            if (!isset($userPermissions->error)) {
                foreach ($userPermissions as $permission) {
                    $selectedPermission[] = $permission->code;
                }
            }
            $data['selectedPermission'] = $selectedPermission;

            $selectedLocation = [];
            $dataAccess = json_decode($api->getDataAccess($token, $user_id));
            if (isset($dataAccess->dataAccessInfo)) {
                $from = $dataAccess->dataAccessInfo->range_from;
                $to = $dataAccess->dataAccessInfo->range_to;
                $include = $dataAccess->dataAccessInfo->include;
                $includeLocations = array_map('trim', explode(',', $include));
                $exclude = $dataAccess->dataAccessInfo->exclude;
                $excludeLocations = array_map('trim', explode(',', $exclude));

                for ($i = $from; $i <= $to; $i++) {
                    if (!in_array($i, $excludeLocations)) {
                        $location[] = $i;
                    }
                }
                $selectedLocation = array_merge($includeLocations, $location);
                asort($selectedLocation);
                $data['accessInfo'] = $dataAccess->dataAccessInfo;
            }
            $data['selectedLocation'] = $selectedLocation;
        }
        else
        {
            $data["breadcrumb"] = "New";
            $data["mode"] = "new";
        }
        return view('mo-admin.main.user.user-new', $data);
    }

    public function saveUser(Request $request, $user_id = 0)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $dealer_group = $request->input("dealer_group");
        $location = $request->input("location");

        $first_name = $request->input("user-firstname");
        $last_name = $request->input("user-lastname");
        $password = $request->input("user-password");
        $email = $request->input("user-email");
        $code = $request->input("user-code");
        $existing_contact = $request->input("user-contact");
        if ($existing_contact == "new-contact") {
            $existing_contact = false;
            $exist_contact_id = null;
        } else {
            $existing_contact = true;
            $exist_contact_id = $request->input("contact-id");
        }
        $title = $request->input("user-title");
        $gender = $request->input("user-gender");
        if (!isset($gender)) {
            $gender = 0;
        }
        $address_1 = $request->input("user-address-1");
        $address_2 = $request->input("user-address-2");
        $suburb = $request->input("user-suburb");
        $postcode = $request->input("user-postcode");
        $country = $request->input("user-country");
        if($country == null){
            $country = $request->input("user_country_input");
        }
        $state = $request->input("user-state");
        $mobile_number = $request->input("user-mobile-number");
        $privacy = $request->input("user-privacy");
        $note = $request->input("user-note");
        $company_name = $request->input("user-company-name");
        $role_id = array($request->input("user-role"));
        $permissionList = $request->input("permissionList");
        $range_from = $request->input("range_from");
        $range_to = $request->input("range_to");
        $include = $request->input("include");
        $exclude = $request->input("exclude");

        if (!isset($exist_contact_id)) {
            $exist_contact_id = null;
        }

        $params = array(
            "company_group_id" => $dealer_group,
            "location_id" => $location,
            "first_name" => $first_name,
            "last_name" => $last_name,
            "password" => $password,
            "email" => $email,
            "code" => $code,
            "existingContact" => $existing_contact,
            "title" => $title,
            "gender" => $gender,
            "address1" => $address_1,
            "address2" => $address_2,
            "suburb" => $suburb,
            "postcode" => $postcode,
            "country" => $country,
            "state" => $state,
            "mobileNumber" => $mobile_number,
            "privacy" => $privacy,
            "note" => $note,
            "roleId" => $role_id,
            "existContactId" => $exist_contact_id,
            "companyName" => $company_name,
            "permissions" => $permissionList,
            "range_from" => $range_from,
            "range_to" => $range_to,
            "include" => $include,
            "exclude" => $exclude
        );

        if ($user_id != 0) {
            $params["existContactId"] = null;
            $params["existingContact"] = null;
            $result = json_decode($api->updateUserInAdmin($token, $user_id, $params));
            if (!property_exists($result, 'error')) {
                return 'success';
            }
            return json_encode($result->error);
        } else {
            $result = json_decode($api->addUserInAdmin($token, $params));
            /*if (!property_exists($result, 'error')) {
                return 'success';
            }*/
            if(!isset($result->error)){
                return "success";
            }
            return json_encode($result->error);
        }


    }

    public function viewUsersList(Request $request)
    {
        $channels = $request->channels;
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $params = array(
            "limit" => 20,
            "offset" => 0
        );

        $countAll = 0;
        $listSizeAll = json_decode($api->usersAdminSearch($token, $params))->list_size;
        if($listSizeAll != null){
            $countAll = $listSizeAll->count;
        }

        $roleId = [7];//super admin
        $countSuperAdmin = 0;
        $listSizeSuperAdmin = json_decode($api->getUsersByRoleId($token, $params, $roleId))->list_size;
        if($listSizeSuperAdmin != null){
            $countSuperAdmin = $listSizeSuperAdmin->count;
        }

        $roleId = [6];//dealer admin
        $countDealerAdmin = 0;
        $listSizeDealerAdmin = json_decode($api->getUsersByRoleId($token, $params, $roleId))->list_size;
        if($listSizeDealerAdmin != null){
            $countDealerAdmin = $listSizeDealerAdmin->count;
        }
        $countRemainingUsers = (int)$countAll - ((int)$countDealerAdmin + (int)$countSuperAdmin);

        $data = array(
            "header" => "Dealerships",
            "breadcrumb" => "Users",
            "channels" => $channels,
            "countAll" => $countAll,
            "countSuperAdmin" => $countSuperAdmin,
            "countDealerAdmin" => $countDealerAdmin,
            "countRemainingUsers" => $countRemainingUsers
        );

        return view('mo-admin.main.user.users-manage', $data);
    }

    public function getUsers(Request $request, $roleId = '')
    {
        $token = $request->session()->get("token");
        $search_txt = $request->input("mo_txt_search");
        $offset = $request->input("offset");
        $active = $request->input("active");

        $api = new \App\Library\APICaller();

        $params = array(
            "limit" => 20,
            "offset" => $offset,
            "query" => $search_txt
        );

        if ($active == "true") {
            $params = array_merge($params, array("status"=>1));
        }

        if($roleId != '' && is_numeric($roleId)){
            $params = array_merge($params, array("roleId"=>[$roleId]));
        }
        elseif($roleId == 'remaining'){
            $role_list = json_decode($api->getListOfRole($token));
            $role_array_ids = [];
            foreach($role_list as $role){
                array_push($role_array_ids, $role->id);
            }
            $role_array_remaining_ids = array_diff($role_array_ids, ["6", "7"]);//hard-code to exclude super admins and admins
            $params = array_merge($params, array("roleId"=>$role_array_remaining_ids));
        }
        $users = json_decode($api->usersAdminSearch($token, $params));

        if (!isset($users->error) && $users->userList != null) {
            $users_response_json = array('data' => array ());

            foreach ($users->userList as $key => $value) {

                $arr = array(
                    0 => $value->company_group_name,
                    1 => $value->company_name,
                    2 => $value->first_name,
                    3 => $value->last_name,
                    4 => $value->role,
                    5 => $value->last_login,
                    6 => $value->last_logout,
                    7 => $value->name,
                    8 => $value->active,
                    9 => $value->id
                );
                array_push($users_response_json["data"], $arr);
            }

            $count = 0;
            if($users->list_size != null){
                $count = $users->list_size->count;
            }
            $users_response_json["count"] = $count;

            if (!$users_response_json["data"]) {
                return null;
            }
            return json_encode($users_response_json);

        } else {
            return null;
        }
    }
}