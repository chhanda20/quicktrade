<?php
/**
 * Created by PhpStorm.
 * User: Son
 * Date: 6/1/2017
 * Time: 10:46 AM
 */

namespace App\Http\Controllers\MOAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\Helper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class LocationController extends Controller
{
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('handling-channel');
        $this->middleware('get-menu-access');
    }

    public function initLocationForm(Request $request, $type, $locationId = 0)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');
        $location_dealergroup_list = json_decode($api->listCompanyGroups($token));
        $params = array("type" => "STATUS_CODE");
        $location_status_list = $api->getTypeCode($token, $params);
        $location_country_list = $api->getCountry($token);

        $data = array(
            "headers" => "Location",
            "dealergroup_list" => $location_dealergroup_list,
            "location_status_list" => $location_status_list,
            "country_list" => $location_country_list,
        );

        $data["mode"] = "new";

        if($type == "new"){
            $data["breadcrumb"] = "New";
        }else if($type == "edit"){
            $data["breadcrumb"] = "Edit";
            $data["mode"] = "edit";
            $result = json_decode($api->getLocationById($token, $locationId));

            if(!isset($result->error)){

                foreach($result as $key=>$value){
                    $data["location_details"] = $value;

                    $data["selected_state"] = $value->state;
                    foreach($location_country_list as $key1=>$value1){
                        foreach($value1->stateList as $key2=>$value2){
                            if($value2->id == $value->state){
                                $data["state_list"] = $value1->stateList;
                                break;
                            }
                        }
                    }

                }

            }
        }

        $data["type"] = $type;

        $data["js"][] = "mo-admin/location/createLocation";
        $data["css"][] = "star-rating";

        return view("mo-admin.main.create-location", $data);
    }

    public function saveLocation(Request $request, $locationId = "")
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $new_or_edit = $request->input("new_or_edit");

        $location_lmct = $request->input("location_lmct");
        $location_name = $request->input("location_name");
        $location_code = $request->input("location_code");
        $location_address = $request->input("location_address");
        $location_suburb = $request->input("location_suburb");
        $location_country = $request->input("location_country");
        if($location_country == null){
            $location_country = $request->input("location_country_input");
        }
        $location_state = $request->input("location_state");
        $location_status = $request->input("location_status");
        $location_dealership = $request->input("location_dealership");
        $location_post_code = $request->input("location_post_code");

        $params = array(
            "lmct" => $location_lmct,
            "name" => $location_name,
            "status_code" => $location_status,
            "location_code" => $location_code,
            "address" => $location_address,
            "suburb" => $location_suburb,
            "country" => $location_country,
            "state" => $location_state,
            "company_id" => $location_dealership,
            "postcode" => $location_post_code
        );

        Log::debug("mo - saveLocation - params: ".json_encode($params));

        if($new_or_edit == "new"){
            $result = json_decode($api->createLocation($token, $params));
        }else if($new_or_edit == "edit"){
            $result = json_decode($api->updateLocation($token, $locationId, $params));
        }

        if(Helper::resultCheck($result) === true){
            return 1;
        }
        return json_encode(Helper::resultCheck($result));
    }

    public function viewLocation(Request $request)
    {
        $channels = $request->channels;
        $data = array(
            "header" => "Location",
            "channels" => $channels
        );
        return view("mo-admin.main.location", $data);
    }

    public function getLocations(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $query = $request->input("mo_txt_search");
        $limit = 20;
        $offset = $request->input("offset");
        $status = [1,2];

        $params = array(
            "query" => $query,
            "limit"=> $limit,
            "offset" => $offset,
            "status" => $status

        );

//        $result = json_decode($api->getAllLocations($token, $params));
        $result = json_decode($api->getLocationAlls($token, $params));
        $result_location_list = $result->locationList;

        if(!isset($result_location_list->error)){
            $location_json = array("data" => array());
            foreach($result_location_list as $key=>$value){
                $arr = array(
                    0=>$value->id,
                    1=>$value->company_group_name,
                    2=>$value->company_name,
                    3=>$value->name,
                    4=>$value->location_code,
                    5=>$value->status_code == 1 ? "Inactive" : "Active"
                );
                array_push($location_json["data"], $arr);
            }
            $location_json["count"] = $result->list_size->count;

            if(!$location_json){
                return null;
            }
            return json_encode($location_json);
        }
    }

    public function dataAccessLocation(Request $request)
    {
        $data = [];
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');
        $company_group_id = $request->input('company_group_id');
        $params = array(
            "company_group_id" => $company_group_id,
            "status" => [2] // active locations

        );
        $res = json_decode($api->getAllLocations($token, $params));
        if (isset($res->locationList)) {
            $data['locations'] = $res->locationList;
        }
        return view("mo-admin.main.user.location", $data);
    }

    public function dealerAccessLocation(Request $request)
    {
        $data = [];
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');
        $res = json_decode($api->getLocationByCurrentLoggedUser($token));
        if (isset($res) && is_array($res)) {
            $data['locations'] = $res;
        }
        return view("mo-admin.main.user.location", $data);
    }
}