<?php

namespace App\Http\Controllers\MOAdmin;

use App\Library\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ValuationController extends Controller
{
    public function __construct() {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }

    public function viewValuations(Request $request, $type)
    {
        /** GET CHANNELS from middleware **/
        $channels = $request->channels;
        $data = array(
            "header" => "Valuations",
            "channels" => $channels
        );

        switch ($type) {
            case "requests":
                $data['js'][] = 'mo-admin/valuation/viewValuations-requests';
                $data["breadcrumb"] = "Requests";
                $view = "mo-admin.main.valuation.valuations-requests";
                break;

            case "in-progress":
                $data['js'][] = 'mo-admin/valuation/viewValuations-in-progress';
                $data["breadcrumb"] = "In Progress";
                $view = "mo-admin.main.valuation.valuations-in-progress";
                break;

            case "complete":
                $data['js'][] = 'mo-admin/valuation/viewValuations-complete';
                $data["breadcrumb"] = "Complete";
                $view = "mo-admin.main.valuation.valuations-complete";
                break;

            case "pending":
                $data['js'][] = 'mo-admin/valuation/viewValuations-pending';
                $data["breadcrumb"] = "Pending Approval";
                $view = "mo-admin.main.valuation.valuations-pending";
                break;

            default:
                $data['js'][] = 'mo-admin/valuation/viewValuations-requests';
                $data["breadcrumb"] = "Requests";
                $view = "mo-admin.main.valuation.valuations-requests";
                break;
        }

        return view($view, $data);
    }

    public function getDataOfValuations(Request $request, $type)
    {

        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $limit = $request->input("limit");
        if(empty($limit)){
            $limit = 20;
        }
        $offset = $request->input("offset");
        $query = $request->input("mo_txt_search");
        $orderBy = "valuation_create_date";

        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "query" => $query,
            "orderBy" => $orderBy,
            "orderDesc" => true,
            "view_as_mo" => true
        );

        switch ($type) {

            case "requests":

                $params = array_merge($params, array("status"=>1));
                $valuations_requests = json_decode($api->_getValuations($token, $params));

                if (Helper::resultCheck($valuations_requests) === true) {

                    $valuations_requests_json = array('data' => array ());
                    $colours = $api->getTypeCode($token, array("type" => "COLOR_TYPE"));

                    /** GET PERMISSIONS from middleware **/
                    $permissions = $request->permissions;

                    foreach ($valuations_requests as $valuation) {

                        $colour = null;
                        foreach ($colours as $color) {

                            if ($color->code == $valuation->colour) {
                                $colour = $color->value;
                            }
                        }

                        $arr = array(
                            0 => $valuation->id,
                            1 => $valuation->make,
                            2 => $valuation->model_family,
                            3 => isset ($valuation->build_date) ? substr($valuation->build_date, 0, 4) : $valuation->build_date,
                            4 => $valuation->vin,
                            5 => $colour,
                            6 => $valuation->registration_number,
                            7 => date('M Y', strtotime($valuation->registration_expiry_date)),
                            8 => $valuation->salesperson_first_name.' '.$valuation->salesperson_last_name,
                            9 => is_array($permissions) && in_array("valuation_perform", $permissions)?"valuation_perform":""
                        );
                        array_push($valuations_requests_json["data"], $arr);
                    }
                    $length = $api->_getLengthValuations($token, array("status"=>1, "query"=>$query));
                    $valuations_requests_json["count"] = json_decode($length)->count;
                    dd($valuations_requests_json);
                    echo json_encode($valuations_requests_json);

                } else {
                    echo null;
                }
                break;

            case "in-progress":

                $params = array_merge($params, array("status"=>2));
                $valuations_in_progress = json_decode($api->_getValuations($token, $params));

                if (!isset($valuations_in_progress->error)) {
                    $valuations_in_progress_json = array('data' => array ());
                    foreach ($valuations_in_progress as $valuation) {
                        $arr = array(
                            0 => $valuation->id,
                            1 => $valuation->make,
                            2 => $valuation->model_family,
                            3 => isset ($valuation->build_date) ? substr($valuation->build_date, 0, 4) : $valuation->build_date,
                            4 => $valuation->registration_number,
                            5 => $valuation->valuer_first_name.' '.$valuation->valuer_last_name,
                            6 => $valuation->salesperson_first_name.' '.$valuation->salesperson_last_name,
                            7 => $valuation->location_name
                        );
                        array_push($valuations_in_progress_json["data"], $arr);
                    }

                    $length = $api->_getLengthValuations($token, array("status"=>2, "query"=>$query));
                    $valuations_in_progress_json["count"] = json_decode($length)->count;
                    echo json_encode($valuations_in_progress_json);
                } else {
                    echo null;
                }

                break;

            case "complete":
                $statuses = [3,6,7,12,13,14];

                $params = array_merge($params, array("status"=>[3,6,7,12,13,14]));
                $valuations_complete = json_decode($api->_getValuations($token, $params));

                /** GET VALUATION STATUS FROM CODE ID **/
                $valuation_statuses = $api->getTypeCode($token, array("type" => "VALUATION_STATUS"));

                if (!isset($valuations_complete->error)) {

                    $valuations_complete_json = array('data' => array ());

                    foreach ($valuations_complete as $valuation) {
                        /** get CONDITION value **/

                        foreach($statuses as $status) {

                            if ($status == $valuation->status) {
                                foreach($valuation_statuses as $valuation_status) {
                                    if ($status == $valuation_status->code) {
                                        $v_status = $valuation_status->value;
                                        break;
                                    }
                                }
                                break;
                            }
                        }

                        $arr = array(
                            0 => $valuation->id,
                            1 => $valuation->make,
                            2 => $valuation->model_family,
                            3 => isset ($valuation->build_date) ? substr($valuation->build_date, 0, 4) : $valuation->build_date,
                            4 => $valuation->registration_number,
                            5 => $valuation->valuer_first_name.' '.$valuation->valuer_last_name,
                            6 => $valuation->salesperson_first_name.' '.$valuation->salesperson_last_name,
                            7 => $valuation->location_name,
                            8 => $v_status,
                            9 => $valuation->status,
                            10 => $valuation->valuation_value
                        );
                        array_push($valuations_complete_json["data"], $arr);
                    }

                    $length = $api->_getLengthValuations($token, array("status"=>$statuses, "query"=>$query));
                    $valuations_complete_json["count"] = json_decode($length)->count;
                    echo json_encode($valuations_complete_json);
                } else {
                    echo null;
                }

                /*Log::debug(json_encode($valuations_complete_json));*/
                break;

            case "pending":
                $status = [4,5];

                $params = array_merge($params, array("status"=>[4,5]));
                $valuations_pending = json_decode($api->_getValuations($token, $params));

                if (!isset($valuations_pending->error)) {
                    $valuations_pending_json = array('data' => array ());
                    $condition_type = $api->getTypeCode($token, array("type" => "CONDITION"));
                    $colours = $api->getTypeCode($token, array("type" => "COLOR_TYPE"));

                    foreach ($valuations_pending as $valuation) {
                        /** get COLOUR value **/
                        $colour = null;
                        foreach ($colours as $color) {

                            if ($color->code == $valuation->colour) {
                                $colour = $color->value;
                            }
                        }

                        /** get CONDITION value **/
                        $condition = null;
                        foreach ($condition_type as $type) {
                            if ($type->code == $valuation->condition) {
                                $condition = $type->value;
                            }
                        }

                        $arr = array(
                            0 => $valuation->make,
                            1 => $valuation->model_family,
                            2 => isset ($valuation->build_date) ? substr($valuation->build_date, 0, 4) : $valuation->build_date,
                            3 => $valuation->registration_number,
                            4 => $colour,
                            5 => $condition,
                            6 => $valuation->valuer_first_name.' '.$valuation->valuer_last_name,
                            7 => $valuation->salesperson_first_name.' '.$valuation->salesperson_last_name,
                            8 => $valuation->offer_value,
                            9 => $valuation->valuation_value,
                            10 => ($valuation->estimated_recon_cost == 0) ? $valuation->total_recon_cost : $valuation->estimated_recon_cost,
                            11 => $valuation->requested_price,
                            12 => $valuation->status,
                            13 => $valuation->id,
                        );

                        array_push($valuations_pending_json["data"], $arr);
                    }

                    $length = $api->getLengthValuations($token, $status, $query);
                    $valuations_pending_json["count"] = json_decode($length)->count;
                    echo json_encode($valuations_pending_json);
                } else {
                    echo null;
                }

                /*Log::debug(json_encode($valuations_pending_json));*/
                break;
        }
    }
}
