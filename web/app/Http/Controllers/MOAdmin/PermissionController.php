<?php
/**
 * Created by PhpStorm.
 * User: Son
 * Date: 4/14/2017
 * Time: 10:09 AM
 */

namespace App\Http\Controllers\MOAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('handling-channel');
        $this->middleware('get-menu-access');
    }

    public function initEditPermission(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $permission_list = json_decode($api->getPermission($token));
        $category_group = $api->getTypeCode($token, array("type" => "PERMISSION_CATEGORY"));

        $params = array(
            "permission_list" => $permission_list,
            "category_group" => $category_group,
        );

        $category_flag = null;
        $params['categories_type'] = array();
        $params['categories_group'] = array();
        if (isset($permission_list)) {
            foreach ($permission_list as $permission) {
                $category = $permission->category;
                if ($category != $category_flag) {
                    array_push($params['categories_group'], $category);
                    $category_flag = $category;
                }
            }
            $params['categories_type'] = array_unique($params['categories_group']);
        }
        return view("mo-admin.main.dealership.edit-permission", $params);
    }

    public function updatePermission(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $permission_name = $request->input("permission_name");
        $permission_description = $request->input("permission_description");
        $permission_code = $request->input("permission_code");

        $params = array(
            "name" => $permission_name,
            "description" => $permission_description
        );
        $result =  $api->updatePermission($token, $params, $permission_code);

        if(!isset($result->error))
        {
            return 1;
        }
        return 0;
    }
}