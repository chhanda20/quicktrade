<?php

namespace App\Http\Controllers\MOAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\Helper;
use Illuminate\Support\Facades\Log;

class DisputeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('handling-channel');
        $this->middleware('get-menu-access');
    }

    public function viewDisputeInfo(Request $request, $transactionId, $disputeId)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $channels = $request->channels;

        $data = array(
            "header" => "Dispute Centre",
            "breadcrumb" => "Dispute Info",
            "channels" => $channels
        );

        /** DISPUTE DECISION **/
        $disputeDecisions = $api->getTypeCode($token, array("type" => "DISPUTE_DECISION"));
        $data["disputeDecisions"] = $disputeDecisions;

        /** DISPUTE INFO **/
        $disputeInfo = json_decode($api->getDisputeById($token, $disputeId));
        $viewOrEdit = "edit";
        $data["viewOrEdit"] = $viewOrEdit;
        $general_recondition_cost = 0;
        $general_recondition_cost_variance = 0;
        if(Helper::resultCheck($disputeInfo) === true){
            $data["disputeInfo"] = $disputeInfo;

            /** GET GENERAL RECONDITIONING COST **/
            if($disputeInfo->general_recondition_cost == 1){
                $result = $api->getGeneralReconditionCost($token, $disputeInfo->valuation_id);
                Log::debug("viewDisputeInfo - result: ".json_encode($result));
                if(Helper::resultCheck($result)===true){
                    $general_recondition_cost = $result->generalReconditionCost;
                }
                $general_recondition_cost_variance = $disputeInfo->general_recondition_cost_variance;
            }

            if($disputeInfo->status == 4){//status resolved
                $viewOrEdit = "view";
            }
            $data["viewOrEdit"] = $viewOrEdit;
            /** GET SELLER - BUYER INFO **/
            $sellerInfo = json_decode($api->getContactById($token, $disputeInfo->seller_contact_id));
            if(Helper::resultCheck($sellerInfo) === true){
                $data["sellerInfo"] = $sellerInfo;
            }
            /** SHOW THE CONTACT PERSON - NOT BUYER **/

            /*$buyerInfo = json_decode($api->getContactById($token, $disputeInfo->buyer_id));
            if(Helper::resultCheck($buyerInfo) === true){
                $data["buyerInfo"] = $buyerInfo;
            }*/

            $data["disputeSelectedDecisionId"] = $disputeInfo->decision;
            $data["disputeSelectedDecisionName"] = Helper::getTypeName($disputeDecisions, $disputeInfo->decision);

            /** AT FAULT **/
            $fault_user_ids = explode(",", $disputeInfo->fault_user_ids);
            switch ($fault_user_ids){
                case Helper::arrayCheck($fault_user_ids) && count($fault_user_ids) == 2 && in_array($disputeInfo->buyer_id, $fault_user_ids) && in_array($disputeInfo->seller_id, $fault_user_ids):
                    $who_fault = 3;
                    break;
                case Helper::arrayCheck($fault_user_ids) && count($fault_user_ids) == 1 && in_array($disputeInfo->buyer_id, $fault_user_ids):
                    $who_fault = 1;
                    break;
                case Helper::arrayCheck($fault_user_ids) && count($fault_user_ids) == 1 && in_array($disputeInfo->seller_id, $fault_user_ids):
                    $who_fault = 2;
                    break;
                default:
                    $who_fault = 0;
                    break;
            }
            $data["who_fault"] = $who_fault;

        }
        $data["general_recondition_cost"] = $general_recondition_cost;
        $data["general_recondition_cost_variance"] = $general_recondition_cost_variance;

        /** DISPUTE ITEMS **/
        $disputeItems = json_decode($api->getDisputeItems($token, $transactionId, $disputeId));
        $data["originalRepairCost"] = 0;
        $data["additionalRepairCost"] = 0;
        $data["totalRepairCost"] = 0;

        $data["originalVarianceCost"] = 0;
        $data["additionalVarianceCost"] = 0;
        $data["additionalVarianceCostDisplay"] = 0;
        $data["totalVarianceCost"] = 0;
        if(Helper::resultCheck($disputeItems) === true){
            $data["disputeItems"] = $disputeItems;
            $data["originalRepairCost"] = Helper::getRepairCost($disputeItems->original) + $general_recondition_cost;
            $data["additionalRepairCost"] = Helper::getRepairCost($disputeItems->additional);
            $data["totalRepairCost"] = $data["originalRepairCost"] + $data["additionalRepairCost"];

            $data["originalVarianceCost"] = Helper::getVarianceCost($disputeItems->original) + $general_recondition_cost_variance;
            $data["additionalVarianceCost"] = Helper::getVarianceCost($disputeItems->additional);
            $data["additionalVarianceCostDisplay"] = $data["additionalRepairCost"] - $data["additionalVarianceCost"];
            $data["totalVarianceCost"] = $data["originalVarianceCost"] + $data["additionalVarianceCost"];
        }

        /** DISPUTE NOTE **/
        $disputeNotes = json_decode($api->getDisputeNotes($token, $disputeId));
        if(Helper::resultCheck($disputeNotes) === true){
            $data["disputeNotes"] = $disputeNotes;
        }

        $data["js"][] = "mo-admin/dispute/viewDisputeInfo";

        return view("mo-admin.main.dispute.dispute-info", $data);
    }

    public function createNote(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $disputeId = $request->input("dispute_id");
        $note = $request->input("note");

        $params = array(
            "note" => $note
        );

        $result = json_decode($api->createDisputeNote($token, $disputeId, $params));
        if(Helper::resultCheck($result) === true){
            return json_encode($result);
        }
        return Helper::resultCheck($result);
    }

    public function deleteNote(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $disputeId = $request->input("dispute_id");
        $noteId = $request->input("note_id");

        $result = json_decode($api->deleteDisputeNote($token, $disputeId, $noteId));
        if(Helper::resultCheck($result) === true){
            return 1;
        }
        return Helper::resultCheck($result);
    }

    public function resolve(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $disputeId = $request->input("dispute_id");
        $items = $request->input("items");
        $decision = $request->input("decision");
        $general_recondition_cost = $request->input("general_recondition_cost");
        $transaction_id = $request->input("transaction_id");
        $user_fault = $request->input("user_fault");

        switch ($user_fault)
        {
            case Helper::arrayCheck($user_fault) && count($user_fault) == 2 && in_array(1, $user_fault) && in_array(2, $user_fault):
                $user_fault = 3;
                break;
            default:
                $user_fault = (int)$user_fault[0];
                break;
        }

        if($items == null){
            $items = [];
        }

        if($general_recondition_cost == null){
            $general_recondition_cost = "";
        }

        $params = array(
            "items" => $items,
            "decision" => $decision,
            "general_recondition_cost" => $general_recondition_cost,
            "user_type" => $user_fault
        );

        Log::debug("resolve - params: ".json_encode($params));

        //exit(var_dump(\GuzzleHttp\json_encode($params)));

        $result = json_decode($api->resolveDispute($token, $disputeId, $params));
        if(Helper::resultCheck($result) === true){
            /** CHANGE TRANSACTION STATUS **/
            if($decision == 3){//return vehicle
                $transaction = new \App\Http\Controllers\MOAdmin\TransactionController();
                if($transaction_id != ""){
                    $transaction->returnVehicle($token, $transaction_id);
                }
            }
            return 1;
        }
        return Helper::resultCheck($result);
    }

    public function underReview(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $disputeId = $request->input("dispute_id");
        $user_fault = $request->input("user_fault");

        switch ($user_fault)
        {
            case Helper::arrayCheck($user_fault) && count($user_fault) == 2 && in_array(1, $user_fault) && in_array(2, $user_fault):
                $user_fault = 3;
                break;
            default:
                $user_fault = (int)$user_fault[0];
                break;
        }

        $params = array(
            "user_type" => $user_fault
        );

        $result = json_decode($api->underReviewDispute($token, $disputeId, $params));
        if(Helper::resultCheck($result) === true){
            return 1;
        }
        return Helper::resultCheck($result);
    }

    public function showList(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $channels = $request->channels;
        $data["channels"] = $channels;
        $data["header"] = 'Dispute Centre';
        $data['tabs'] = [
            [
                'id' => 'new',
                'name' => 'New',
                'status' => 1
            ],
            [
                'id' => 'in-progress',
                'name' => 'In Progress',
                'status' => 2
            ],
            [
                'id' => 'under-review',
                'name' => 'Under Review',
                'status' => 3
            ],
            [
                'id' => 'resolved',
                'name' => 'Resolved',
                'status' => 4
            ]
        ];
        $data['js'][] = 'mo-admin/dispute/master';
        $data['js'][] = 'mo-admin/dispute/showList';
        return view('mo-admin.main.dispute.main', $data);
    }


    public function getList(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $limit = $request->input('limit');
        $offset = $request->input('offset');
        $status = $request->input('status');
        $query = $request->input("query");

        $statusArr = [];
        array_push($statusArr, $status);

        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "status" => $statusArr
        );

        if(!empty($query)){
            $params["query"] = $query;
        }
        $disputeList = [];
        $res = json_decode($api->findDispute($token, $params));
        if (!isset($res->error)) {
            $disputeList = $res;
        }
        $data["disputeList"] = $disputeList;
        return view('mo-admin.main.dispute.list', $data);

    }

    public function accept(Request $request)
    {
        $token = $request->session()->get("token");
        $id = $request->input('id');
        $api = new \App\Library\APICaller();
        $res = $api->acceptDispute($token, $id);
        if (isset($res->status)) {
            return 1;
        }
        return 0;
    }

}
