<?php

namespace App\Http\Controllers\MOAdmin;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Library\Helper;

class AuthenticationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $email = $request->input("email");
        $password = $request->input("password");

        $api = new \App\Library\APICaller();
        $result = $api->loginForMOAdmin($email, $password);

        if (isset($result->token) && $result->token != false) {
            $request->session()->put('token', $result->token);
            if(isset($result->id)) {
                $request->session()->put('id', $result->id);
            }
            if(isset($result->contact_id)) {
                $request->session()->put('contact_id', $result->contact_id);
            }

            /** GET ROLES and PUT in SESSION **/
            $roles = $api->getRoles($result->token);
            if(!isset($roles->error)){
                $request->session()->put('roles', $roles);
            }

            /** GET PERMISSIONS and PUT in SESSION  **/
            $permissions = $api->getPermissions($result->token);
            if(!isset($permissions->error)){
                $request->session()->put('permissions', $permissions);
            }

            /** GET USER INFORMATION and PUT in SESSION  **/
            $user_info = json_decode($api->getUserInfo($result->token));
            if(!isset($user_info->error)){
                $request->session()->put('user_info', $user_info);
            }

            /** GET ROLE ID and PUT in SESSION  **/
            $user_info_by_id = json_decode($api->getUserById($result->token, $result->id));
            if(!isset($user_info_by_id->error)){
                $request->session()->put('role_id', $user_info_by_id->userInfo[0]->roleId);
            }

            /** GET MENUS and PUT in SESSION  **/
            $menus = $result->menu;
            $collection = Helper::updateMOMenuAccessSession($menus);
            $request->session()->put('mo_menus_sidebar', $collection);
            $collection = Helper::updateDealershipMenuAccessSession($menus);
            $request->session()->put('dealership_menus_sidebar', $collection);

            return json_encode($result);
            /*return redirect("/mo-admin/dealership-list");*/
        } else {
            $error = array(
                "error" => true,
                "error_desc" => Helper::resultCheck($result)
            );
            return json_encode($error);

            /*$data = array("error" => $result->error->code);
            return view("mo-admin/login", $data);*/
        }
    }
    
    public function forgotPassword(Request $request)
    {
        
    }

    public function setNewPassword(Request $request)
    {

    }

    public function index(Request $request)
    {

    }
}