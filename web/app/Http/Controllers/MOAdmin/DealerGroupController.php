<?php

namespace App\Http\Controllers\MOAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DealerGroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('handling-channel');
        $this->middleware('get-menu-access');
    }

    public function initCompanyGroupForm(Request $request, $type, $company_group_id=0)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $status_code = $api->getTypeCode($token, array("type"=>"STATUS_CODE"));
        $channels = $request->channels;
        $params = array(
            "headers" => "Dealer Group",
            "channels" => $channels,
            "status_code" => $status_code,
        );
        if($type == 'new') {
            $params["breadcrumb"]= "New";
        }else if ($type == 'edit'){
            $params["breadcrumb"]= "Edit";
            $result = json_decode($api->getCompanyGroupById($token, $company_group_id));
            if(!isset($result->error)){
                $params["company_group_detail"] = $result;
            }
        }
        return view("mo-admin.main.create-company-group", $params);

    }

    public function createCompanyGroup(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $name = $request->input("dealer_group_name");
        $description = $request->input("dealer_group_description");
        $status_code = $request->input("dealer_group_status");

        $params = array(
            "name" => $name,
            "description" => $description,
            "status_code" => $status_code
        );

        $result = json_decode($api->createCompanyGroup($token, $params));
        if(!isset($result->error)){
            return redirect("mo-admin/dealer-groups-list");
        }
        return $this->initCompanyGroupForm($request, "new");
    }

    public function updateCompanyGroup(Request $request, $companyGroupId)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $name = $request->input("dealer_group_name");
        $description = $request->input("dealer_group_description");
        $status_code = $request->input("dealer_group_status");

        $params = array(
            "name" => $name,
            "description" => $description,
            "status_code" => $status_code,
            "id"=>$companyGroupId,
        );
        $result = $api->updateCompanyGroup($token, $params, $companyGroupId);

        if(!isset($result->error))
        {
            return redirect("/mo-admin/dealer-groups-list");
        }

        return $this->initCompanyGroupForm($request, "edit", $companyGroupId);
    }

    public function viewCompanyGroups(Request $request)
    {
        $channels = $request->channels;
        $data = array(
            "header" => "Dealer Group",
            "channels" => $channels
        );

        return view('mo-admin.main.company-groups', $data);
    }

    public function getCompanyGroups(Request $request)
    {
        //Log::debug('INSIDE');
        $token = $request->session()->get("token");
        $search_txt = $request->input("mo_txt_search");
        $offset = $request->input("offset");
        $active = $request->input("active");

        $api = new \App\Library\APICaller();

        if ($active == "true") {
            $params = array(
                "status_code" => array("2"),
                "limit" => 20,
                "offset" => $offset,
                "query" => $search_txt
            );
        } else {
            $params = array(
                "status_code" => array("1", "2"),
                "limit" => 20,
                "offset" => $offset,
                "query" => $search_txt
            );
        }

        $companyGroups = json_decode($api->getCompanyGroups($token, $params));
        if ($active == "true") {
            $params = array(
                "status_code" => array("2"),
                "query" => $search_txt
            );
        } else {
            $params = array(
                "status_code" => array("1", "2"),
                "query" => $search_txt
            );
        }
        $length = $api->getLengthOfCompanyGroups($token, $params);

        if (!isset($companyGroups->error)) {
            $company_groups_response_json = array('data' => array ());
            $group_types=$api->getTypeCode($token, array("type" => "STATUS_CODE"));

            foreach ($companyGroups as $key => $value) {

                $group_type_value = null;
                foreach ($group_types as $group_type) {
                    if ($group_type->code == $value->status_code) {
                        $group_type_value = $group_type->value;
                    }
                }

                $arr = array(
                    0 => $value->id,
                    1 => $value->name,
                    2 => $value->description,
                    3 => $group_type_value
                );
                array_push($company_groups_response_json["data"], $arr);
            }

            $company_groups_response_json["count"] = json_decode($length)->count;

            if (!$company_groups_response_json["data"]) {
                return null;
            }
            return json_encode($company_groups_response_json);

        } else {
            return null;
        }
    }

}
