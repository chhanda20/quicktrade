<?php

namespace App\Http\Controllers\MOAdmin;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class DealershipController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('handling-channel');
        $this->middleware('get-menu-access');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewCompanies(Request $request)
    {
        $channels = $request->channels;
        $data = array(
            "header"      => "Dealership",
            "channels" => $channels
        );

        return view('mo-admin.main.companies', $data);
    }

    public function getCompanies(Request $request)
    {
        //return data format
        $token = $request->session()->get("token");
        $active = $request->input("active");
        $search_txt = $request->input("mo_txt_search");
        $offset = $request->input("offset");


        $api = new \App\Library\APICaller();

        if ($active == "true") {
            $params = array(
                "status_code" => array("2"),
                "limit" => 20,
                "offset" => $offset,
                "query" => $search_txt
            );
        } else {
            $params = array(
                "status_code" => array("1", "2"),
                "limit" => 20,
                "offset" => $offset,
                "query" => $search_txt
            );
        }
        
        $companies = json_decode($api->getCompanies($token, $params));

        if ($active == "true") {
            $params = array(
                "status_code" => array("2"),
                "query" => $search_txt
            );
        } else {
            $params = array(
                "status_code" => array("1", "2"),
                "query" => $search_txt
            );
        }
        $length = $api->getLengthOfCompany($token, $params);

        if (!isset($companies->error)) {

            $companies_response_json = array('data' => array ());

            foreach ($companies as $key => $value) {

                $arr = array(
                    0 => $value->id,
                    1 => $value->company_name,
                    2 => $value->address1,
                    3 => $value->first_name,
                    4 => $value->last_name,
                    5 => $value->email,
                    6 => $value->status_code
                );
                array_push($companies_response_json["data"], $arr);
            }

            $companies_response_json["count"] = json_decode($length)->count;

            if (!$companies_response_json["data"]) {
                return null;
            }
            return json_encode($companies_response_json);

        } else {
            return null;
        }
    }

    public function initCompanyForm(Request $request, $type, $companyId = 0)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $listCompanyGroups = json_decode($api->listCompanyGroups($token));

        $channels = $request->channels;
        $country_list = $api->getCountry($token);
        $privacies_flag = $api->getTypeCode($token, array("type" => "PRIVACY_FLAG"));

        $data = array(
            "channels" => $channels,
            "header"      => "Dealership",
            "listCompanyGroups" => $listCompanyGroups,
            "country_list" => $country_list,
            "privacies_flag" => $privacies_flag
        );

        if($type == "new"){
            $data["breadcrumb"] = "New";
        }else if($type == "edit"){
            $data["breadcrumb"] = "Edit";
            $result = json_decode($api->getCompanyById($token, $companyId));
            if(!isset($result->error)) {
                $data["company_details"] = $result;
            }
        }

        return view('mo-admin.main.create-company', $data);
    }

    public function createCompany(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        /** GET INPUT FOR DEALERSHIP **/
        $company_group_id = $request->input("dealer_group");
        $name = $request->input("dealer_name");
        $address = $request->input("dealer_address");
        $suburb = $request->input("dealer_suburb");
        $state = $request->input("dealer_state");
        $country = $request->input("dealer_country");
        if($country == null){
            $country = $request->input("input_country_id");
        }
        $status_code = 2; //1 is inactive, 2 is active

        /** GET INPUT FOR AGENT **/
        $first_name = $request->input("first_name");
        $last_name = $request->input("last_name");
        $email = $request->input("email");
        $mobile_phone = $request->input("mobile");
        $privacy_flag = $request->input("privacy");

        $params = array(
            "company_name" => $name,
            "address1" => $address,
            "status_code" => $status_code,
            "company_group_id" => $company_group_id,
            "first_name" => $first_name,
            "last_name" => $last_name,
            "email" => $email,
            "suburb" => $suburb,
            "state" => $state,
            "country" => $country,
            "privacy_flag" => $privacy_flag,
            "mobile_phone" => $mobile_phone
        );
        $result = json_decode($api->createCompany($token, $params));

        if(isset($result->error)){
            $errors = [];
            if (isset($result->error->detail)) {
                $properties = get_object_vars($result->error->detail);
                foreach ($properties as $prop) {
                    foreach ($prop as $errorMsg) {
                        $errors[] = $errorMsg;
                    }
                }
            }
            return json_encode($errors);
        }
        return 1;
    }

    public function updateCompany(Request $request, $companyId)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        /** GET INPUT FOR DEALERSHIP **/
        $company_group_id = $request->input("dealer_group");
        $name = $request->input("dealer_name");
        $address = $request->input("dealer_address");
        $suburb = $request->input("dealer_suburb");
        $state = $request->input("dealer_state");
        $country = $request->input("dealer_country");
        if($country == null){
            $country = $request->input("input_country_id");
        }
        $status_code = $request->input("dealer_status"); //1 is inactive, 2 is active

        $params = array(
            "company_name" => $name,
            "address1" => $address,
            "status_code" => $status_code,
            "company_group_id" => $company_group_id,
            "id" => $companyId,
            "suburb" => $suburb,
            "state" => $state,
            "country" => $country
        );

        $result = json_decode($api->updateCompany($token, $params, $companyId));
        if(isset($result->error)){
            $errors = [];
            if (isset($result->error->detail)) {
                $properties = get_object_vars($result->error->detail);
                foreach ($properties as $prop) {
                    foreach ($prop as $errorMsg) {
                        $errors[] = $errorMsg;
                    }
                }
            }
            return json_encode($errors);
        }
        return 1;
    }

    public function suspendCompany(Request $request, $companyId)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $status_code = $request->input("status");

        return $api->suspendCompany($token, $status_code, $companyId);
    }
}