<?php

namespace App\Http\Controllers\MOAdmin;

use App\Library\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('handling-channel');
        $this->middleware('get-menu-access');
    }

    public function initSettingDetailsForm(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $externalOfferSettings = json_decode($api->getSettingExternalOffer($token));
        $valuationSettings = json_decode($api->getValuationSettings($token));

        $data = array(
            "headers" => "Settings",
            "breadcrumb" => "External Offer",
            "externalOfferSettings" => $externalOfferSettings,
            "valuationSettings" => $valuationSettings
        );
        return view("mo-admin.main.settings", $data);
    }

    public function initValuationSettingDetailsForm(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $valuationSettings = json_decode($api->getValuationSettings($token));

        $data = array(
            "headers" => "Settings",
            "breadcrumb" => "Valuation",
            "valuationSettings" => $valuationSettings
        );

        $data['js'][] = 'mo-admin/settings/saveSettings';

        return view("mo-admin.main.settings.valuation-settings", $data);
    }

    public function initExternalOfferingSettingDetailsForm(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $externalOfferSettings = json_decode($api->getSettingExternalOffer($token));

        $data = array(
            "headers" => "Settings",
            "breadcrumb" => "External Offering",
            "externalOfferSettings" => $externalOfferSettings
        );

        $data['js'][] = 'mo-admin/settings/saveSettings';

        return view("mo-admin.main.settings.external-offering-settings", $data);
    }

    public function initAuctionSettingDetailsForm(Request $request)
    {

        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $params = array(
            "ids"=>array("auction_default_duration", "auction_price_increment", "limit_time_edit_auction")
        );

        $auctionSettings = json_decode($api->getAppProperty($token, $params));
        $data = array(
            "headers" => "Settings",
            "breadcrumb" => "Auction",
            "auctionSettings" => $auctionSettings
        );

        $data['js'][] = 'mo-admin/settings/saveSettings';

        return view("mo-admin.main.settings.auction-settings", $data);
    }

    public function initRatingSettingDetailsForm(Request $request)
    {

        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $ratingSettings = json_decode($api->getRatingSettings($token));
        $data = array(
            "headers" => "Settings",
            "breadcrumb" => "Rating System",
            "ratingSettings" => $ratingSettings
        );

        $data['js'][] = 'mo-admin/settings/saveSettings';

        return view("mo-admin.main.settings.rating-settings", $data);
    }

    public function saveSettings(Request $request, $settingType)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        switch ($settingType){
            case "valuation":
                /** GET INPUT FOR VALUATION SETTINGS **/
                $first_notification_timeout = $request->input("first_notification_timeout");
                $second_notification_timeout = $request->input("second_notification_timeout");
                $valuation_timeout = $request->input("valuation_timeout");

                $valuation_params = array(
                    "first_notification_timeout" => (int)$first_notification_timeout,
                    "second_notification_timeout" => (int)$second_notification_timeout,
                    "valuation_timeout" => (int)$valuation_timeout
                );
                $result = json_decode($api->saveValuationSettings($token, $valuation_params));
                break;
            case "external_offering":
                /** GET INPUT FOR EXTERNAL OFFER SETTINGS **/
                $external_offer_timeout = $request->input("external_offer_timeout");
                $external_offer_expiry_period = $request->input("external_offer_expiry_period");
                $external_offer_acceptance_timeout = $request->input("external_offer_acceptance_timeout");

                $external_offer_params = array(
                    "external_offer_timeout" => (int)$external_offer_timeout,
                    "external_offer_expiry" => (int)$external_offer_expiry_period,
                    "external_offer_acceptance_timeout" => (int)$external_offer_acceptance_timeout
                );

                $result = json_decode($api->saveSettingExternalOffer($token, $external_offer_params));
                break;

            case "auction":
                $auction_default_duration = $request->input("auction_default_duration");
                $auction_price_increment = $request->input("auction_price_increment");
                $limit_time_edit_auction = $request->input("limit_time_edit_auction");

                $params = array(
                    "auction_default_duration" => $auction_default_duration,
                    "auction_price_increment" => $auction_price_increment,
                    "limit_time_edit_auction" => $limit_time_edit_auction
                );
                $result = json_decode($api->setAppProperty($token, $params));
                break;

            case "rating":
                $rating_expired_time = $request->input("rating_expired_time");
                $speedy_seller_avg_time = $request->input("speedy_seller_avg_time");
                $speedy_buyer_avg_time = $request->input("speedy_buyer_avg_time");

                $params = array(
                    "rating_expired_time" => $rating_expired_time,
                    "speedy_seller_avg_time" => $speedy_seller_avg_time,
                    "speedy_buyer_avg_time" => $speedy_buyer_avg_time
                );
                $result = json_decode($api->saveRatingSettings($token, $params));
                break;
        }

        if(Helper::resultCheck($result) === true){
            return 1;
        }
        return Helper::resultCheck($result);
    }

    public function initGeneralSetting(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");
        $term_and_condition = json_decode($api->getTermAndCondition($token));

        $data = array(
            "headers" => "Settings",
            "breadcrumb" => "General",
            "term_and_condition" => $term_and_condition
        );
        $data["js"][] = 'mo-admin/settings/saveSettings';
        return view("mo-admin.main.settings.general-settings", $data);
    }

    public function updateTermAndCondition(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $term_and_condition = $request->input("setting_term_condition");
        $params = array(
            "term_and_condition" => $term_and_condition
        );
        $result = json_decode($api->updateTermAndCondition($token, $params));
        if(!isset($result->error)){
            return 1;
        }
        return 0;
    }
}
