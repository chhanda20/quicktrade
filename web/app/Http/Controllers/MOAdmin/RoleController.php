<?php
/**
 * Created by PhpStorm.
 * User: Son
 * Date: 6/6/2017
 * Time: 2:01 PM
 */

namespace App\Http\Controllers\MOAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library\Helper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }

    public function viewRoleList(Request $request)
    {
        $channel = $request->channel;
        $data = array(
            "header" => "Role",
            "channel" => $channel
        );
        return view("mo-admin.main.role.role", $data);
    }

    public function getRoleList(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token  = $request->session()->get('token');

        $query = $request->input("mo_txt_search");
        $limit = 20;
        $offset = $request->input("offset");

        $data = array(
            "query" => $query,
            "limit" => $limit,
            "offset" => $offset,
        );
        $result = json_decode($api->findRolesList($token, $data));
        $result_role_list = $result->roleList;

        if(!isset($result_role_list->error)){
            $role_json = array("data" => array());
            foreach($result_role_list as $role){
                $arr = array(
                    0=>$role->id,
                    1=>$role->name,
                    2=>$role->is_default == 1 ? "X" : ''
                );
                array_push($role_json["data"], $arr);
            }
            if(!empty($result->list_size)){
                $role_json["count"] = $result->list_size->count;
            }
            if(!$role_json["data"]){
                return null;
            }
            return json_encode($role_json);
        }
        else{
            return null;
        }
    }

    public function initCreateNewRole(Request $request, $type, $role_id = 0)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $permission_list = json_decode($api->getPermission($token));
        $category_group = $api->getTypeCode($token, array("type" => "PERMISSION_CATEGORY"));

        $menu_list = json_decode($api->getMenuItems($token));
        $menu_items_list = $menu_list->menuList;

        $data = array(
            "headers" => "Roles",
            "permission_list" => $permission_list,
            "category_group" => $category_group,
            "menu_items_list" => $menu_items_list
        );

        $category_flag = null;
        $data["category_type"] = array();
        $data["categories_group"] = array();
        if(isset($permission_list)){
            foreach($permission_list as $permission){
                $category = $permission->category;
                if($category != $category_flag){
                    array_push($data["categories_group"], $category);
                    $category_flag = $category;
                }
            }
            $data["category_type"] = array_unique($data["categories_group"]);
        }
        /*$menu_group = [];
        $data["menu_group"]="";
        if(isset($menu_items_list)){
            foreach($menu_items_list as $menu){
                if($menu->site_menu == "mo_admin"){
                    foreach($menu->group as $group_menu){
                        $menu_group[] = [
                            "group_name" => $group_menu->group_name,
                            "menu_items" => $group_menu->menu_items
                        ];
                        $data["menu_group"] = $menu_group;
                    }
                }
            }
        }*/

        $menu_group = [];
        $site_menu_flag = null;
        $data["site_menu"]= array();
        $data["site_menu_type"] = array();
        if(isset($menu_items_list)){
            foreach($menu_items_list as $menu){
                $site_menu = $menu->site_menu;
                if($site_menu != $site_menu_flag){
                    array_push($data["site_menu"], $site_menu);
                    $site_menu_flag = $site_menu;
                    foreach($menu->group as $group_menu){
                        $menu_group[] = [
                            "group_name" => $group_menu->group_name,
                            "menu_items" => $group_menu->menu_items
                        ];
                        $data["menu_group"] = $menu_group;
                    }
                }
            }
        }

        if($type == "new"){
            $data["breadcrumb"] = "New";
        }else if($type == "edit"){
            $data["breadcrumb"] = "Edit";
            $result = json_decode($api->getRoleById($token, $role_id));
            if(!isset($result->error)){
                $data["role_detail"] = $result;
            }
            $permission_result = json_decode($api->getRoleByIdOfPermission($token, $role_id));
            if(!isset($permission_result->error)){
                $permissionArrId = [];
                foreach($permission_result as $result){
                    array_push($permissionArrId, $result->id);
                }
                $data["permission_detail"] = $permissionArrId;
            }

            $menu_role_result = json_decode($api->getMenuByRoleId($token, $role_id));
            if(!isset($menu_role_result->error)){
                $menu_role_list = $menu_role_result->menuList;
                $menu_group = [];
                foreach($menu_role_list as $key=>$value){
                    foreach($value->group as $group_menu){
                        foreach($group_menu->menu_items as $menu_item){
                            array_push($menu_group, $menu_item->code);
                        }
                    }
                }
//                Log::debug("menu_group ".json_encode($menu_group));
                $data["menu_role_detail"] = $menu_group;
            }
        }
        $data["js"][] = "mo-admin/role/createRole";
        return view("mo-admin.main.role.create-role", $data);

    }

    public function createNewRole(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $role_name = $request->input("role_name");
        $role_default = $request->input("mo_role_default");
        $permissions = $request->input("role_permission_list");
        $permissionIntegerIDs = array_map('intval', $permissions);

        $menu_id = $request->input("menu_items_list");

        $data = array(
            "name" => $role_name,
            "is_default" => $role_default,
            "permissions" => $permissionIntegerIDs
        );
        $data_menu = array(
            "menu_id" => $menu_id
        );
        $result = json_decode($api->createNewRole($token, $data));
        if(!isset($result->error)){
            $data_menu["role_id"] = $result->id;
            $result_menu = json_decode($api->setMenuAccessForRole($token, $data_menu));
            if(!isset($result_menu->error)){
                return redirect("/mo-admin/role-list");
            }
        }
        Session::flash("error_message", "Role Name Duplicate");
        return $this->initCreateNewRole($request, 'new');
//        return $result->error->code;
    }

    public function updateNewRole(Request $request, $role_id)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $role_name = $request->input("role_name");
        $role_default = $request->input("mo_role_default");
        $permissions= $request->input("role_permission_list");
        $permissionIntegerIDs = array_map('intval', $permissions);
        $menu_id = $request->input('menu_items_list');
        $roleId = $request->input("mo_role_id");

        $param = array(
            "name" => $role_name,
            "is_default" => (bool)$role_default,
            "permissions" => $permissionIntegerIDs
        );
        $param_menu = array(
            "menu_id" => $menu_id,
            "role_id" => $roleId,
            "update_mo_menu" => true
        );

        Log::debug("updateNewRole - param: ".json_encode($param));

        $result = json_decode($api->updateNewRole($token, $param, $role_id));
        Log::debug("updateNewRole - result: ".json_encode($result));
        if(!isset($result->error)){
            $result_menu = json_decode($api->setMenuAccessForRole($token, $param_menu));
            Log::debug("updateNewRole - setMenuAccessForRole ".json_encode($result_menu));
            if(!isset($result_menu->error)){
                /** GET MENUS and PUT in SESSION  **/
                /*$menus = json_decode($api->getMenuByRoleId($token, $roleId));
                if(!isset($menus->error)){
                    $collection = Helper::updateMOMenuAccessSession($menus->menuList);
                    Log::debug("updateNewRole - collection: ".$collection);
                    $request->session()->forget('mo_menus_sidebar');
                    $request->session()->put('mo_menus_sidebar', $collection);
                }*/

                return redirect("/mo-admin/role-list");
            }
        }
        Session::flash("error_message", "Role Code Duplicate");
        return $this->initCreateNewRole($request, "edit", $role_id);
//        return $result->error->code;
    }

    public function deleteRole(Request $request, $role_id)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get("token");

        $result = json_decode($api->deleteRole($token, $role_id));
        if(!isset($result->error))
        {
            return 1;
        }else{
            return 0;
        }

    }

    public function getByCompanyGroup(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $companyGroupId =  $request->input("dealer_group");
        $result = $api->getRolesCompanyGroup($token, $companyGroupId);
        if ($result){
            return $result;
        }
        return NULL;
    }
}