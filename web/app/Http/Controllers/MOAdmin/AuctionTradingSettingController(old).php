<?php
/**
 * Created by PhpStorm.
 * User: Son
 * Date: 5/4/2017
 * Time: 5:25 PM
 */

namespace App\Http\Controllers\MOAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class old extends Controller
{
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('handling-channel');
        $this->middleware('get-menu-access');
    }

    public function initAuctionSettings(Request $request)
    {

        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $result = json_decode($api->getAuctionSettings($token));
        $params = array(
            "headers" => "Settings",
            "breadcrumb" => "Auction",
            "result" => $result
        );
        // if(!isset($result->error)){
        //     $params["auction_list"] = $result;
        // }
        return view("mo-admin.main.trading.settings", $params);
    }

    public function updateAuctionSetting(Request $request)
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');

        $auction_default_duration = $request->input("auction_default_duration");
        $auction_price_increment = $request->input("auction_price_increment");

        $params = array(
            "auction_default_duration" => $auction_default_duration,
            "auction_price_increment" => $auction_price_increment
        );
        $result = json_decode($api->updateAuctionSettings($token, $params));
        if(!isset($result->error)){
            return 1;
        }
        return $result->error->message;
    }

}