<?php

namespace App\Http\Controllers;

use App\Library\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UsersController extends Controller
{
    public function __construct() {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }

    public function viewUsersList(Request $request)
    {
        $channels = $request->channels;
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $params = array(
            "limit" => 20,
            "offset" => 0
        );

        $countAll = 0;
        $listSizeAll = json_decode($api->getUsers($token, $params))->list_size;
        if($listSizeAll != null){
            $countAll = $listSizeAll->count;
        }
        $roleId = [7];//super admin
        $countSuperAdmin = 0;
        $listSizeSuperAdmin = json_decode($api->getLengthOfUsersByRoleId($token, $params, $roleId))->list_size;
        if($listSizeSuperAdmin != null){
            $countSuperAdmin = $listSizeSuperAdmin->count;
        }

        $roleId = [6];//dealer admin
        $countDealerAdmin = 0;
        $listSizeDealerAdmin = json_decode($api->getLengthOfUsersByRoleId($token, $params, $roleId))->list_size;
        if($listSizeDealerAdmin != null){
            $countDealerAdmin = $listSizeDealerAdmin->count;
        }
        $countRemainingUsers = (int)$countAll - ((int)$countDealerAdmin + (int)$countSuperAdmin);

        $data = array(
            "header" => "Configure",
            "breadcrumb" => "Users",
            "channels" => $channels,
            "countAll" => $countAll,
            "countSuperAdmin" => $countSuperAdmin,
            "countDealerAdmin" => $countDealerAdmin,
            "countRemainingUsers" => $countRemainingUsers
        );

        return view('main.dealership.users-manage', $data);
    }

    public function getUsers(Request $request, $roleId = '')
    {
        $token = $request->session()->get("token");
        $search_txt = $request->input("mo_txt_search");
        $offset = $request->input("offset");
        $active = $request->input("active");

        $api = new \App\Library\APICaller();

        $params = array(
            "limit" => 20,
            "offset" => $offset,
            "query" => $search_txt
        );

        if ($active == "true") {
            $params = array_merge($params, array("status"=>1));
        }

        if($roleId != '' && is_numeric($roleId)){
            $params = array_merge($params, array("roleId"=>[$roleId]));
        }
        elseif($roleId == 'remaining'){
            $role_list = json_decode($api->getListOfRole($token));
            $role_array_ids = [];
            foreach($role_list as $role){
                array_push($role_array_ids, $role->id);
            }
            $role_array_remaining_ids = array_diff($role_array_ids, ["6", "7"]);//hard-code to exclude super admins and admins
            $params = array_merge($params, array("roleId"=>$role_array_remaining_ids));
        }
        $users = json_decode($api->getUsers($token, $params));

        if (!isset($users->error) && $users->userList != null) {
            $users_response_json = array('data' => array ());

            foreach ($users->userList as $key => $value) {

                $arr = array(
                    0 => $value->id,
                    1 => $value->first_name,
                    2 => $value->last_name,
                    3 => $value->role,
                    4 => $value->email,
                    5 => $value->mobile_phone,
                    6 => $value->active
                );
                array_push($users_response_json["data"], $arr);
            }

            $count = 0;
            if($users->list_size != null){
                $count = $users->list_size->count;
            }
            $users_response_json["count"] = $count;

            if (!$users_response_json["data"]) {
                return null;
            }
            return json_encode($users_response_json);

        } else {
            return null;
        }
    }

    public function initUserForm(Request $request, $user_id = 0)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        //$role_list = json_decode($api->getListOfRole($token));
        $company_group_id = $request->session()->get("company_group_id");
        $company_id = $request->session()->get("company_id");
        $role_list = json_decode($api->getRolesByCompanyGroupId($token, $company_group_id));

        $country_list = $api->getCountry($token);
        $genders = $api->getTypeCode($token, array("type" => "GENDER_TYPE"));
        $titles = $api->getTypeCode($token, array("type" => "TITLE_TYPE"));
        $privacies_flag = $api->getTypeCode($token, array("type" => "PRIVACY_FLAG"));
        $permissionGroup = json_decode($api->permissionByGroup($token));
        $permissions = [];

        // Don't show 'MO Permission'
        foreach ($permissionGroup as $group) {
            if ($group->category_id !== 1) {
                $permissions[] = $group;
            }
        }

        $data = array(
            "header" => "Configure",
            "breadcrumb" => "Users",
            "roles" => $role_list,
            "company_id" => $company_id,
            "country_list" => $country_list,
            "genders" => $genders,
            "titles" => $titles,
            "privacies_flag" => $privacies_flag,
            "permissions" => $permissions
        );
        if ($user_id != 0)
        {
            $data["action"] = "Edit";
            $data["mode"] = "edit";
            $data["user_data"] = json_decode($api->getUserById($token, $user_id));
            $userPermissions = json_decode($api->getUserPermission($token, $user_id));
            $selectedPermission = [];
            if (!isset($userPermissions->error)) {
                foreach ($userPermissions as $permission) {
                    $selectedPermission[] = $permission->code;
                }
            }
            $data['selectedPermission'] = $selectedPermission;

            $selectedLocation = [];
            $dataAccess = json_decode($api->getDataAccess($token, $user_id));
            if (isset($dataAccess->dataAccessInfo)) {
                $from = $dataAccess->dataAccessInfo->range_from;
                $to = $dataAccess->dataAccessInfo->range_to;
                $include = $dataAccess->dataAccessInfo->include;
                $includeLocations = array_map('trim', explode(',', $include));
                $exclude = $dataAccess->dataAccessInfo->exclude;
                $excludeLocations = array_map('trim', explode(',', $exclude));

                for ($i = $from; $i <= $to; $i++) {
                    if (!in_array($i, $excludeLocations)) {
                        $location[] = $i;
                    }
                }
                $selectedLocation = array_merge($includeLocations, $location);
                asort($selectedLocation);
                $data['accessInfo'] = $dataAccess->dataAccessInfo;
            }
            $data['selectedLocation'] = $selectedLocation;
        }
        else
        {
            $data["action"] = "New";
            $data["mode"] = "new";  
        }
        $data["js"][] = "users/initUserForm";
        return view('main.dealership.user-new', $data);
    }

    public function searchUsers(Request $request)
    {
        //$query = trim($request->input("term"));
        $query = $request["term"];
        if (!isset($query)) {
            $query = null;
        }
        
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $limit = 20;
        $offset = 0;
        $contact_not_link_user = true;
        $result = json_decode($api->searchContacts($token, $query, $limit, $offset, $contact_not_link_user));
        $auto_complete = array();
        if (isset($result)) {
            foreach ($result as $key => $value) {
                 $data = array("id" => $value->id, "label" => $value->first_name ." ". $value->last_name, "value" => $value->first_name ." ". $value->last_name, "value2" => $value);
                 array_push($auto_complete, $data);
            }
        }

        return json_encode($auto_complete);
    }

    public function searchCompanyUsers(Request $request)
    {
        $query = trim($request->input("query"));
        if (!isset($query)) {
            $query = null;
        }
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $limit = 100;
        $offset = 0;
        $result = json_decode($api->searchCompanyContacts($token, $query, $limit, $offset));
        $auto_complete = array();
        if (isset($result)) {
            foreach ($result as $key => $value) {
                $data = array(
                    "id"     => $value->id,
                    "label"  => $value->first_name . " " . $value->last_name,
                    "value"  => $value->first_name . " " . $value->last_name,
                    "value2" => $value
                );
                array_push($auto_complete, $data);
            }
        }

        return json_encode($auto_complete);
    }

    public function createUser(Request $request, $user_id = 0)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $first_name = $request->input("user-firstname");
        $last_name = $request->input("user-lastname");
        $password = $request->input("user-password");
        $email = $request->input("user-email");
        $code = $request->input("user-code");
        $existing_contact = $request->input("user-contact");
        if ($existing_contact == "new-contact") {
            $existing_contact = false;
            $exist_contact_id = null;
        } else {
            $existing_contact = true;
            $exist_contact_id = $request->input("contact-id");
        }
        $title = $request->input("user-title");
        $gender = $request->input("user-gender");
        if (!isset($gender))
        {
            $gender = 0;
        }
        $address_1 = $request->input("user-address-1");
        $address_2 = $request->input("user-address-2");
        $suburb = $request->input("user-suburb");
        $postcode = $request->input("user-postcode");
        $country = $request->input("user-country");
        if($country == null){
            $country = $request->input("input_country_new_user");
        }
        $state = $request->input("user-state");
        $mobile_number = $request->input("user-mobile-number");
        $privacy = $request->input("user-privacy");
        $note = $request->input("user-note");
        $company_name = $request->input("user-company-name");
        $role_id = array($request->input("user-role"));
        $permissionList = $request->input("permissionList");
        $range_from = $request->input("range_from");
        $range_to = $request->input("range_to");
        $include = $request->input("include");
        $exclude = $request->input("exclude");
        
        if (!isset($exist_contact_id)) {
            $exist_contact_id = null;
        }

        $params = array(
            "first_name"      => $first_name,
            "last_name"       => $last_name,
            "password"        => $password,
            "email"           => $email,
            "code"            => $code,
            "existingContact" => $existing_contact,
            "title"           => $title,
            "gender"          => $gender,
            "address1"        => $address_1,
            "address2"        => $address_2,
            "suburb"          => $suburb,
            "postcode"        => $postcode,
            "country"         => $country,
            "state"           => $state,
            "mobileNumber"    => $mobile_number,
            "privacy"         => $privacy,
            "note"            => $note,
            "roleId"          => $role_id,
            "existContactId"  => $exist_contact_id,
            "companyName"     => $company_name,
            "permissions" => $permissionList,
            "range_from" => $range_from,
            "range_to" => $range_to,
            "include" => $include,
            "exclude" => $exclude
        );


        if ($user_id != 0)
        {
            $params["existContactId"] = null;
            $params["existingContact"] = null;
            //return var_dump(json_encode($params));
            $result = json_decode($api->updateUser($token, $params, $user_id));
            if(!isset($result->error)) {
                return 'success';
            }
            return json_encode($result->error);
        }
        else
        {
            //return var_dump(json_encode($params));
            $result = json_decode($api->createUser($token, $params));
            /*if(!property_exists($result, 'error')) {
                return 'success';
            }*/
            if(!isset($result->error)){
                return "success";
            }
            return json_encode($result->error);
        }

    }

    public function updateStatus(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $userId = $request->input("userId");
        $status_code = $request->input("status");
        $params = array(
            "userId" => $userId,
            "status" => $status_code
        );

        $result = json_decode($api->updateStatus($token, $params));
        if(!property_exists($result, 'error')){
            return 1;
        }
        return 0;

    }
}
