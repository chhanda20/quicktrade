<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\Helper;

class AuctionController extends Controller
{
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }

    public function getAuctionList(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $channels = $request->channels;
        $data = array(
            "header" => "Auction",
            "breadcrumb" => "Auction Listings",
            "channels" => $channels
        );

        $data['tabs'] = [
            [
                'id' => 'all',
                'name' => 'All',
            ],
            [
                'id' => 'wish-list',
                'name' => 'Wish List',
            ],
            [
                'id' => 'watching',
                'name' => 'Watching',
            ],
            [
                'id' => 'recently-viewed',
                'name' => 'Recently Viewed',
            ],
            [
                'id' => 'selling',
                'name' => 'Selling',
            ]
        ];
        $data["css"][] = "star-rating";
        return view('main.trading.auction.auction-main', $data);
    }

    public function getAllItem(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $limit = $request->input('limit');
        $offset = $request->input('offset');
        $params = array(
            "limit" => $limit,
            "offset" => $offset
        );

        $odometer = $request->input("odo_range");
        $year = $request->input("year_range");
        $trade_price = $request->input("price_range");
        $seller_rating = $request->input("rating_range");
        $location = $request->input("location");
        $transmissionType = $request->input("transmissionType");
        $makeInfo = $request->input("makeInfo");
        $orderBy = $request->input("orderBy");
        $orderDesc = $request->input("orderDesc");
        $query = $request->input("query");

        if(Helper::arrayCheck($odometer)){
            $params["odometer"] = $odometer;
        }

        if(Helper::arrayCheck($year)){
            $params["year"] = $year;
        }

        if(Helper::arrayCheck($trade_price)){
            $params["trade_price"] = $trade_price;
        }

        if(Helper::arrayCheck($seller_rating)){
            $params["seller_rating"] = range($seller_rating[0], $seller_rating[1]);
        }

        if(Helper::arrayCheck($location)){
            $params["location"] = $location;
        }

        if(Helper::arrayCheck($transmissionType)){
            $params["transmissionType"] = $transmissionType;
        }

        if(Helper::arrayCheck($makeInfo)){
            $params["makeInfo"] = $makeInfo;
        }

        if(!empty($orderBy)){
            $params["orderBy"] = $orderBy;
        }

        if(!empty($orderDesc)){
            $params["orderDesc"] = $orderDesc === 'true'? true: false;
        }

        if(!empty($query)){
            $params["query"] = $query;
        }

        $vehicleAuctionList = [];
        $res = json_decode($api->getAuctionList($token, $params));
        if (isset($res->auctionList)) {
            $vehicleAuctionList = $res->auctionList;
        }
        $data["vehicle_auction_list"] = $vehicleAuctionList;
        return view('main.trading.auction.auction-list', $data);
    }

    public function getWishListItem(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $limit = $request->input('limit');
        $offset = $request->input('offset');

        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "viewType" => 'wish_list'
        );

        $odometer = $request->input("odo_range");
        $year = $request->input("year_range");
        $trade_price = $request->input("price_range");
        $seller_rating = $request->input("rating_range");
        $location = $request->input("location");
        $transmissionType = $request->input("transmissionType");
        $makeInfo = $request->input("makeInfo");
        $orderBy = $request->input("orderBy");
        $orderDesc = $request->input("orderDesc");
        $query = $request->input("query");

        if(Helper::arrayCheck($odometer)){
            $params["odometer"] = $odometer;
        }

        if(Helper::arrayCheck($year)){
            $params["year"] = $year;
        }

        if(Helper::arrayCheck($trade_price)){
            $params["trade_price"] = $trade_price;
        }

        if(Helper::arrayCheck($seller_rating)){
            $params["seller_rating"] = range($seller_rating[0], $seller_rating[1]);
        }

        if(Helper::arrayCheck($location)){
            $params["location"] = $location;
        }

        if(Helper::arrayCheck($transmissionType)){
            $params["transmissionType"] = $transmissionType;
        }

        if(Helper::arrayCheck($makeInfo)){
            $params["makeInfo"] = $makeInfo;
        }

        if(!empty($orderBy)){
            $params["orderBy"] = $orderBy;
        }

        if(!empty($orderDesc)){
            $params["orderDesc"] = $orderDesc === 'true'? true: false;
        }

        if(!empty($query)){
            $params["query"] = $query;
        }

        $vehicleAuctionList = [];
        $res = json_decode($api->getAuctionList($token, $params));
        if (isset($res->auctionList)) {
            $vehicleAuctionList = $res->auctionList;
        }
        $data["vehicle_auction_list"] = $vehicleAuctionList;
        return view('main.trading.auction.auction-list', $data);
    }

    public function getWatchingItem(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $limit = $request->input('limit');

        $offset = $request->input('offset');

        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "viewType" => 'watching'
        );
        $odometer = $request->input("odo_range");
        $year = $request->input("year_range");
        $trade_price = $request->input("price_range");
        $seller_rating = $request->input("rating_range");
        $location = $request->input("location");
        $transmissionType = $request->input("transmissionType");
        $makeInfo = $request->input("makeInfo");
        $orderBy = $request->input("orderBy");
        $orderDesc = $request->input("orderDesc");
        $query = $request->input("query");

        if(Helper::arrayCheck($odometer)){
            $params["odometer"] = $odometer;
        }

        if(Helper::arrayCheck($year)){
            $params["year"] = $year;
        }

        if(Helper::arrayCheck($trade_price)){
            $params["trade_price"] = $trade_price;
        }

        if(Helper::arrayCheck($seller_rating)){
            $params["seller_rating"] = range($seller_rating[0], $seller_rating[1]);
        }

        if(Helper::arrayCheck($location)){
            $params["location"] = $location;
        }

        if(Helper::arrayCheck($transmissionType)){
            $params["transmissionType"] = $transmissionType;
        }

        if(Helper::arrayCheck($makeInfo)){
            $params["makeInfo"] = $makeInfo;
        }

        if(!empty($orderBy)){
            $params["orderBy"] = $orderBy;
        }

        if(!empty($orderDesc)){
            $params["orderDesc"] = $orderDesc === 'true'? true: false;
        }

        if(!empty($query)){
            $params["query"] = $query;
        }

        $vehicleAuctionList = [];
        $res = json_decode($api->getAuctionList($token, $params));
        if (isset($res->auctionList)) {
            $vehicleAuctionList = $res->auctionList;
        }
        $data["vehicle_auction_list"] = $vehicleAuctionList;
        return view('main.trading.auction.auction-list', $data);
    }

    public function getRecentlyViewedItem(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $limit = $request->input('limit');
        $offset = $request->input('offset');

        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "viewType" => 'recent_view'
        );

        $odometer = $request->input("odo_range");
        $year = $request->input("year_range");
        $trade_price = $request->input("price_range");
        $seller_rating = $request->input("rating_range");
        $location = $request->input("location");
        $transmissionType = $request->input("transmissionType");
        $makeInfo = $request->input("makeInfo");
        $orderBy = $request->input("orderBy");
        $orderDesc = $request->input("orderDesc");
        $query = $request->input("query");

        if(Helper::arrayCheck($odometer)){
            $params["odometer"] = $odometer;
        }

        if(Helper::arrayCheck($year)){
            $params["year"] = $year;
        }

        if(Helper::arrayCheck($trade_price)){
            $params["trade_price"] = $trade_price;
        }

        if(Helper::arrayCheck($seller_rating)){
            $params["seller_rating"] = range($seller_rating[0], $seller_rating[1]);
        }

        if(Helper::arrayCheck($location)){
            $params["location"] = $location;
        }

        if(Helper::arrayCheck($transmissionType)){
            $params["transmissionType"] = $transmissionType;
        }

        if(Helper::arrayCheck($makeInfo)){
            $params["makeInfo"] = $makeInfo;
        }

        if(!empty($orderBy)){
            $params["orderBy"] = $orderBy;
        }

        if(!empty($orderDesc)){
            $params["orderDesc"] = $orderDesc === 'true'? true: false;
        }

        if(!empty($query)){
            $params["query"] = $query;
        }

        $vehicleAuctionList = [];
        $res = json_decode($api->getAuctionList($token, $params));
        if (isset($res->auctionList)) {
            $vehicleAuctionList = $res->auctionList;
        }
        $data["vehicle_auction_list"] = $vehicleAuctionList;
        return view('main.trading.auction.auction-list', $data);
    }

    public function getSellingItem(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $limit = $request->input('limit');
        $offset = $request->input('offset');

        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "viewType" => 'selling'
        );

        $odometer = $request->input("odo_range");
        $year = $request->input("year_range");
        $trade_price = $request->input("price_range");
        $seller_rating = $request->input("rating_range");
        $location = $request->input("location");
        $transmissionType = $request->input("transmissionType");
        $makeInfo = $request->input("makeInfo");
        $orderBy = $request->input("orderBy");
        $orderDesc = $request->input("orderDesc");
        $query = $request->input("query");

        if(Helper::arrayCheck($odometer)){
            $params["odometer"] = $odometer;
        }

        if(Helper::arrayCheck($year)){
            $params["year"] = $year;
        }

        if(Helper::arrayCheck($trade_price)){
            $params["trade_price"] = $trade_price;
        }

        if(Helper::arrayCheck($seller_rating)){
            $params["seller_rating"] = range($seller_rating[0], $seller_rating[1]);
        }

        if(Helper::arrayCheck($location)){
            $params["location"] = $location;
        }

        if(Helper::arrayCheck($transmissionType)){
            $params["transmissionType"] = $transmissionType;
        }

        if(Helper::arrayCheck($makeInfo)){
            $params["makeInfo"] = $makeInfo;
        }

        if(!empty($orderBy)){
            $params["orderBy"] = $orderBy;
        }

        if(!empty($orderDesc)){
            $params["orderDesc"] = $orderDesc === 'true'? true: false;
        }

        if(!empty($query)){
            $params["query"] = $query;
        }

        $vehicleAuctionList = [];
        $res = json_decode($api->getAuctionList($token, $params));
        if (isset($res->auctionList)) {
            $vehicleAuctionList = collect($res->auctionList)->map(function ($auction) use($api, $token) {

                $city = "Australia/NSW";
                $timezone = new \DateTimeZone($city);
                $date_time = new \DateTime('now', $timezone);

                $currentTime = strtotime($date_time->format('Y-m-d H:i:s')); // AEDT
                $sessionTime = strtotime("{$auction->session_date} {$auction->session_time}");
                $minDiff = round(($sessionTime - $currentTime) / 60,2);
                $settingParam = array(
                    "setting" => ['limit_time_edit_auction']
                );
                $settingArr = json_decode($api->getSetting($token, $settingParam));
                $minAllowed = 30;
                if ($settingArr && is_array($settingArr)) {
                    $resObj = $settingArr[0];
                    if ($resObj->id === 'limit_time_edit_auction') {
                        $minAllowed = $resObj->value;
                        $auction->limit_time_edit_auction = $resObj->value;
                    }
                }
                $auction->editAllow = $minDiff > $minAllowed ? 1 : 0;
                return $auction;
            });
        }
        $data["vehicle_auction_list"] = $vehicleAuctionList;
        return view('main.trading.auction.selling-auction-list', $data);
    }

    public function setWatching(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $params['auction_id'] = $request->input('auction_id');
        $result = json_decode($api->setWatching($token, $params));
        if (isset($result->success) && $result->success) {
            return 1;
        }
        return 0;
    }

    public function unsetWatching(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $params['auction_id'] = $request->input('auction_id');
        $result = json_decode($api->unsetWatching($token, $params));
        if (isset($result->success) && $result->success) {
            return 1;
        }
        return 0;
    }

    public function saveMaxValue(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $sessionId = $request->input('session_id');
        $itemId = $request->input('auction_id');
        $params['max_price'] = $request->input('max_price');

        $response = json_decode($api->saveMaxBidPrice($token, $params, $sessionId, $itemId));
        if (isset($response->auction_session_id) && $response->auction_session_id) {
            return 1;
        }
        return 0;
    }

    public function viewVehicleDetails(Request $request, $vehicleId, $valuationId, $auctionId, $isSelling = 0)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $params['auction_id'] = $auctionId;
        $api->setRecentlyViewed($token, $params);

        $data['header'] = 'Auction';
        $data['breadcrumb'] = 'Auction Listing';
        $data['auction_id'] = $auctionId;
        $data['isSelling'] = $isSelling;

        $auction = json_decode($api->auctionById($token, $auctionId));
        $auction->buy_now_price_text = 'AU $' . number_format($auction->buy_now_price, 0, '.', ',');

        $data['auction'] = $auction;

        $colors = $api->getTypeCode($token, array("type" => "COLOR_TYPE"));
        $fuel_types = $api->getTypeCode($token, array("type" => "FUEL_TYPE"));
        $trim_types = $api->getTypeCode($token, array("type" => "TRIM_TYPE"));
        $transmissions = $api->getTypeCode($token, array("type" => "TRANSMISSION"));
        $drive_trains = $api->getTypeCode($token, array("type" => "DRIVETRAIN"));
        $registered_states = $api->getTypeCode($token, array("type" => "AU_STATE"));

        $vehicleDetails = json_decode($api->getVehicleDetailsById($token, $vehicleId));
        if(!isset($vehicleDetails->error) && !empty($vehicleDetails)){
            $general_color = Helper::getTypeName($colors, $vehicleDetails->colour);
            $manufacturer_color = Helper::getTypeName($colors, $vehicleDetails->manufacturer_color);
            $trim_color = Helper::getTypeName($colors, $vehicleDetails->trim_color);
            $fuel_type = Helper::getTypeName($fuel_types, $vehicleDetails->fuel_type);
            $trim_type = Helper::getTypeName($trim_types, $vehicleDetails->trim);
            $transmission = Helper::getTypeName($transmissions, $vehicleDetails->transmission);
            $drive_train = Helper::getTypeName($drive_trains, $vehicleDetails->drive_train);
            $registered_state = Helper::getTypeName($registered_states, $vehicleDetails->registered_state);
            $data["general_color"] = $general_color;
            $data["manufacturer_color"] = $manufacturer_color;
            $data["trim_color"] = $trim_color;
            $data["fuel_type"] = $fuel_type;
            $data["trim_type"] = $trim_type;
            $data["transmission"] = $transmission;
            $data["drive_train"] = $drive_train;
            $data["registered_state"] = $registered_state;

            $data["vehicleDetails"] = $vehicleDetails;
            $data["standardFeatures"] = $vehicleDetails->option_list;

            $valuationDetails = json_decode($api->getValuationDetailIncludeVersion($token, $valuationId));
            if(!isset($valuationDetails->error) && !empty($valuationDetails)){
                $data["valuationDetails"] = $valuationDetails;
            }

            $locationDetails = json_decode($api->getLocationById($token, $vehicleDetails->location_id));
            if(!isset($locationDetails->error) && !empty($locationDetails)){
                $data["locationName"] = $locationDetails[0]->name;//only have 1 record
            }

            /** VEHICLE PHOTOS **/
            $data["vehiclePhotos"] = array();
            $azure = new \App\Library\AzureCloud();
            $vehicle_photos = $azure->getBlobsNameByVehicleId($vehicleDetails->id);
            //exit(var_dump($vehicle_photos));
            if(Helper::arrayCheck($vehicle_photos)){
                $data["vehiclePhotos"] = $vehicle_photos;
            }
        }

        $optionalOptions = json_decode($api->getVehicleOptionalOptions($token, $vehicleId));
        if(!isset($optionalOptions->error) && !empty($optionalOptions)){
            $data["optionalOptions"] = $optionalOptions;
        }

        $extraOptions = json_decode($api->getVehicleExtrasOptions($token, $vehicleId));
        if(!isset($extraOptions->error) && !empty($extraOptions)){
            $data["extraOptions"] = $extraOptions;
        }
        $data['js'][] = 'auction/viewViewVehicleDetails';

        return view("main.trading.auction.auction-vehicle-detail", $data);
    }

    public function buyNow(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $params['auction_id'] = $request->input('auction_id');
        $response = json_decode($api->buyNow($token, $params));
        if (isset($response->success) && $response->success) {
            return 1;
        }
        return 0;
    }

    public function quickBid(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $session_id = $request->input("session_id");
        $item_id = $request->input("item_id");
        $bid_price = $request->input("bid_price");
        $params = array("bid_price" => $bid_price);
        return $api->bidPrice($token, $session_id, $item_id, $params);
    }

    public function getMaxBidPrice(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $session_id = $request->input("session_id");
        $item_id = $request->input("item_id");
        return $api->getMaxBidPrice($token, $session_id, $item_id);
    }

    public function viewLiveAuctionList(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $channels = $request->channels;
        
        //$channels = "auctionbid[10]";
        $data = array(
            "header" => "Auction",
            "breadcrumb" => "Live Auction",
            "channels" => $channels
        );

        //step1: get active auction sessions
        $active_session = json_decode($api->getActiveAuctionSessions($token));
        //step2: retrieve id from active auction sessions to get vehicles
        $data["live_auction_vehicle_list"] = array();
        if(isset($active_session) && !empty($active_session)) {
            //$query = $request->input("mo-live-auction-search");
            $params = array("offset" => 0,
                            "limit" => 6);
            foreach ($active_session as $key => $value) {

                //subscribe live auction channel
                $auction_channel = new \stdClass();
                $auction_channel->name = "auction_bid_".$value->id;
                $auction_channel->live_auction = "auction_channel";
                if(!isset($channels) && empty($channels)) {
                    $channels = array();
                }
                array_push($channels, $auction_channel);

                $live_auction_vehicle_list = json_decode($api->findVehiclesInAuctionSession($token, $value->id, $params));
                
                if(isset($live_auction_vehicle_list) && !empty($live_auction_vehicle_list)) {
                    $data["live_auction_vehicle_list"] += $live_auction_vehicle_list;
                }
                //get auction session id temp
                $data["auction_session_id"] = $value->id;
            }
            //re-assign channels

            $data["channels"] = $channels;
        }

        //get auction setting
        $auction_setting = json_decode($api->getAuctionSettings($token));
        if(isset($auction_setting) && !empty($auction_setting)) {
            $data["auction_price_increment"] = $auction_setting->auction_price_increment;
        }

        //live auction timer
        $api_system_datetime = json_decode($api->getSystemProperty());

        if (isset($api_system_datetime) && !empty($api_system_datetime)) {
            $api_system_datetime = new \DateTime($api_system_datetime->date);
            if(isset($active_session) && !empty($active_session)) {
                $active_auction_session_created_datetime = new \DateTime($active_session[0]->startDate);
                $seconds = $api_system_datetime->diff($active_auction_session_created_datetime)->format('%a') * 24 * 3600;
                $seconds += $api_system_datetime->diff($active_auction_session_created_datetime)->format('%h') * 3600;
                $seconds += $api_system_datetime->diff($active_auction_session_created_datetime)->format('%i') * 60;
                $seconds += $api_system_datetime->diff($active_auction_session_created_datetime)->format('%s');
                
                $seconds = ($active_session[0]->duration * 60) - $seconds;
                $auction_session_time_remaining = $seconds;
                $data["live_auction_timer"] = gmdate("H:i:s", $auction_session_time_remaining);
                $data["live_auction_seconds_timer"] = $auction_session_time_remaining;
            }
        }

        //get next auction session information
        $next_auction_session = json_decode($api->getNextAuctionSession($token));
        if(isset($next_auction_session) && !empty($next_auction_session)) {
            $next_auction_session_start_date = new \DateTime($next_auction_session[0]->startDate);
            
            $seconds = $next_auction_session_start_date->diff($api_system_datetime)->format('%a') * 24 * 3600;
            $seconds += $next_auction_session_start_date->diff($api_system_datetime)->format('%h') * 3600;
            $seconds += $next_auction_session_start_date->diff($api_system_datetime)->format('%i') * 60;
            $seconds += $next_auction_session_start_date->diff($api_system_datetime)->format('%s');

            // extract hours
            $next_auction_hours = floor($seconds / (60 * 60));

            // extract minutes
            $divisor_for_minutes = $seconds % (60 * 60);
            $next_auction_minutes = floor($divisor_for_minutes / 60);

            // extract the remaining seconds
            $divisor_for_seconds = $divisor_for_minutes % 60;
            $next_auction_seconds = ceil($divisor_for_seconds);

            $data["up_next_auction_session_seconds"] = $seconds;
            $data["up_next_auction_session_h_m_s"] = $next_auction_hours.":".$next_auction_minutes.":".$next_auction_seconds;
        }

        $data["css"][] = "star-rating";
        return view("main.trading.live-auction-list", $data);
    }

    public function viewLiveAuctionListByOffset(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        //step1: get active auction sessions
        $active_session = json_decode($api->getActiveAuctionSessions($token));

        //step2: retrieve id from active auction sessions to get vehicles
        $offset = $request->input("offset");
        $data["live_auction_vehicle_list"] = array();
        if(isset($active_session) && !empty($active_session)) {
            //$query = $request->input("mo-live-auction-search");
            $params = array(
                "offset" => $offset,
                "limit" => 6
            );

            $odometer = $request->input("odo_range");
            $year = $request->input("year_range");
            $trade_price = $request->input("price_range");
            $seller_rating = $request->input("rating_range");
            $location = $request->input("location");
            $transmissionType = $request->input("transmissionType");
            $makeInfo = $request->input("makeInfo");
            $orderBy = $request->input("orderBy");
            $orderDesc = $request->input("orderDesc");
            $query = $request->input("query");

            if(Helper::arrayCheck($odometer)){
                $params["odometer"] = $odometer;
            }

            if(Helper::arrayCheck($year)){
                $params["year"] = $year;
            }

            if(Helper::arrayCheck($trade_price)){
                $params["trade_price"] = $trade_price;
            }

            if(Helper::arrayCheck($seller_rating)){
                $params["seller_rating"] = range($seller_rating[0], $seller_rating[1]);
            }

            if(Helper::arrayCheck($location)){
                $params["location"] = $location;
            }

            if(Helper::arrayCheck($transmissionType)){
                $params["transmissionType"] = $transmissionType;
            }

            if(Helper::arrayCheck($makeInfo)){
                $params["makeInfo"] = $makeInfo;
            }

            if(!empty($orderBy)){
                $params["orderBy"] = $orderBy;
            }

            if(!empty($orderDesc)){
                $params["orderDesc"] = $orderDesc === 'true'? true: false;
            }

            if(!empty($query)){
                $params["query"] = $query;
            }

            $live_auction_vehicle_list = json_decode($api->findVehiclesInAuctionSession($token, $active_session[0]->id, $params));
            $data["live_auction_vehicle_list"] = $live_auction_vehicle_list;

            /*foreach ($active_session as $key => $value) {
                $live_auction_vehicle_list = json_decode($api->findVehiclesInAuctionSession($token, $value->id, $params));
                $data["live_auction_vehicle_list"] += $live_auction_vehicle_list;
            }*/
        }
        $data["call_sample_list"] = true;

        //get auction setting
        $auction_setting = json_decode($api->getAuctionSettings($token));

        if(isset($auction_setting) && !empty($auction_setting)) {
            $data["auction_price_increment"] = $auction_setting->auction_price_increment;
        }

        return view("main.trading.live-auction-sample-list", $data);
    }
}