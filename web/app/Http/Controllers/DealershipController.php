<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library;

class DealershipController extends Controller
{
	public function __construct() {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }

    public function getByGroup(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $companyGroupId =  $request->input("dealer_group");
        $params = [
            'status_code' => [2], // active companies
            'company_group_id' => $companyGroupId,
            'limit' => 50000, // get all companies
            'offset' => 0
        ];
        $result = $api->getCompanies($token, $params);

        if ($result){
            return $result;
        }
        return NULL;
    }
}