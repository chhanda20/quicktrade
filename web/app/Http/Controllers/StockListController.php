<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use App\Library\Helper;
use Illuminate\Support\Facades\Log;

class StockListController extends Controller
{
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('handling-channel');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('get-menu-access');
    }

    public function viewStockList(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $channels = $request->channels;
        $data = array(
            "header" => "Auction",
            "breadcrumb" => "Stock List",
            "channels" => $channels
        );

        $limit = 21;
        $offset = 0;

        $params = array(
            "limit" => $limit,
	        "offset" => $offset
        );

        $vehicleStockList = json_decode($api->getVehicleStockList($token, $params));
        if(!isset($vehicleStockList->error)){
            $data["vehicle_stock_list"] = $vehicleStockList;
        }

        $vehicleSubmittedList = array();
        $params["viewType"] = "submitted";
        $res = json_decode($api->getAuctionList($token, $params));
        if (!isset($res->error) && isset($res->auctionList)) {
            $vehicleSubmittedList = $res->auctionList;
        }
        $data["vehicle_submitted_list"] = $vehicleSubmittedList;

        $auctionSessionList = json_decode($api->getAllAuctionSession($token, null));
        if(!isset($auctionSessionList->error) && !empty($auctionSessionList->sessionList)){
            $sessionListDate = array();
            foreach($auctionSessionList->sessionList as $sessionList){
                array_push($sessionListDate, $sessionList->date);
                //$sessionListDate[] = $sessionList->date;
            }
            $data["session_list_date"] = implode(",",(array_unique($sessionListDate)));
        }

        return view('main.trading.stock-list', $data);
    }

    public function getVehicleList(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $data = array();

        $limit = $request->input("limit");
        $offset = $request->input("offset");

        $params = array(
            "limit" => $limit,
            "offset" => $offset
        );

        $odometer = $request->input("odo_range");
        $year = $request->input("year_range");
        $trade_price = $request->input("price_range");
        $location = $request->input("location");
        $transmissionType = $request->input("transmissionType");
        $makeInfo = $request->input("makeInfo");
        $orderBy = $request->input("orderBy");
        $orderDesc = $request->input("orderDesc");
        $query = $request->input("query");

        if($this->arrayCheck($odometer)){
            $params["odometer"] = $odometer;
        }

        if($this->arrayCheck($year)){
            $params["year"] = $year;
        }

        if($this->arrayCheck($trade_price)){
            $params["trade_price"] = $trade_price;
        }

        if($this->arrayCheck($location)){
            $params["location"] = $location;
        }

        if($this->arrayCheck($transmissionType)){
            $params["transmissionType"] = $transmissionType;
        }

        if($this->arrayCheck($makeInfo)){
            $params["makeInfo"] = $makeInfo;
        }

        if(!empty($orderBy)){
            $params["orderBy"] = $orderBy;
        }

        if(!empty($orderDesc)){
            $params["orderDesc"] = $orderDesc === 'true'? true: false;
        }

        if(!empty($query)){
            $params["query"] = $query;
        }

        $vehicleStockList = json_decode($api->getVehicleStockList($token, $params));
        if(!isset($vehicleStockList->error)){
            $data["vehicle_stock_list"] = $vehicleStockList;
            if(empty($vehicleStockList)){
                return null;
            }
        }

        return view('main.trading.stock-list-grid', $data);
    }

    public function getVehicleSubmittedList(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $limit = $request->input('limit');
        $offset = $request->input('offset');

        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "viewType" => 'submitted'
        );

        $odometer = $request->input("odo_range");
        $year = $request->input("year_range");
        $trade_price = $request->input("price_range");
        $location = $request->input("location");
        $transmissionType = $request->input("transmissionType");
        $makeInfo = $request->input("makeInfo");
        $orderBy = $request->input("orderBy");
        $orderDesc = $request->input("orderDesc");
        $query = $request->input("query");

        if($this->arrayCheck($odometer)){
            $params["odometer"] = $odometer;
        }

        if($this->arrayCheck($year)){
            $params["year"] = $year;
        }

        if($this->arrayCheck($trade_price)){
            $params["trade_price"] = $trade_price;
        }

        if($this->arrayCheck($location)){
            $params["location"] = $location;
        }

        if($this->arrayCheck($transmissionType)){
            $params["transmissionType"] = $transmissionType;
        }

        if($this->arrayCheck($makeInfo)){
            $params["makeInfo"] = $makeInfo;
        }

        if(!empty($orderBy)){
            $params["orderBy"] = $orderBy;
        }

        if(!empty($orderDesc)){
            $params["orderDesc"] = $orderDesc === 'true'? true: false;
        }

        if(!empty($query)){
            $params["query"] = $query;
        }

        $vehicleSubmittedList = null;
        $res = json_decode($api->getAuctionList($token, $params));
        if (!isset($res->error) && isset($res->auctionList) && !empty($res->auctionList)) {
            $vehicleSubmittedList = $res->auctionList;
            $data["vehicle_submitted_list"] = $vehicleSubmittedList;
        }

        return view('main.trading.submitted-list-grid', $data);
    }

    private function arrayCheck($array = array())
    {
        if(is_array($array) && !in_array("", $array)){
            return true;
        }
        return false;
    }

    public function viewVehicleDetails(Request $request, $vehicleId, $valuationId)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $channels = $request->channels;

        $data = array(
            "header" => "Auction",
//            "breadcrumb" => "Stock List",
            "breadcrumb" => strpos($request->path(), 'mo-admin') === 0 ? 'Request List' : 'Stock List',
            "channels" => $channels
        );

        $colors = $api->getTypeCode($token, array("type" => "COLOR_TYPE"));
        $fuel_types = $api->getTypeCode($token, array("type" => "FUEL_TYPE"));
        $trim_types = $api->getTypeCode($token, array("type" => "TRIM_TYPE"));
        $transmissions = $api->getTypeCode($token, array("type" => "TRANSMISSION"));
        $drive_trains = $api->getTypeCode($token, array("type" => "DRIVETRAIN"));
        $registered_states = $api->getTypeCode($token, array("type" => "AU_STATE"));

        $vehicleDetails = json_decode($api->getVehicleDetailsById($token, $vehicleId));
        if (isset($vehicleDetails->error)) {
            $vehicleDetails = null;
        }
        if(!isset($vehicleDetails->error) && !empty($vehicleDetails)){
            $general_color = $this->getTypeName($colors, $vehicleDetails->colour);
            $manufacturer_color = $this->getTypeName($colors, $vehicleDetails->manufacturer_color);
            $trim_color = $this->getTypeName($colors, $vehicleDetails->trim_color);
            $fuel_type = $this->getTypeName($fuel_types, $vehicleDetails->fuel_type);
            $trim_type = $this->getTypeName($trim_types, $vehicleDetails->trim);
            $transmission = $this->getTypeName($transmissions, $vehicleDetails->transmission);
            $drive_train = $this->getTypeName($drive_trains, $vehicleDetails->drive_train);
            $registered_state = $this->getTypeName($registered_states, $vehicleDetails->registered_state);
            $data["general_color"] = $general_color;
            $data["manufacturer_color"] = $manufacturer_color;
            $data["trim_color"] = $trim_color;
            $data["fuel_type"] = $fuel_type;
            $data["trim_type"] = $trim_type;
            $data["transmission"] = $transmission;
            $data["drive_train"] = $drive_train;
            $data["registered_state"] = $registered_state;

            $data["vehicleDetails"] = $vehicleDetails;
            $data["standardFeatures"] = $vehicleDetails->option_list;

            $valuationDetails = json_decode($api->getValuationDetailIncludeVersion($token, $valuationId));
            if(!isset($valuationDetails->error) && !empty($valuationDetails)){
                $data["valuationDetails"] = $valuationDetails;
            }

            $locationDetails = json_decode($api->getLocationById($token, $vehicleDetails->location_id));
            if(!isset($locationDetails->error) && !empty($locationDetails)){
                $data["locationName"] = $locationDetails[0]->name;
            }

            /** VEHICLE PHOTOS **/
            $data["vehiclePhotos"] = array();
            $data["vehiclePhotosCount"] = 1;
            $azure = new \App\Library\AzureCloud();
            $vehicle_photos = $azure->getBlobsNameByVehicleId($vehicleDetails->id);
            //exit(var_dump($vehicle_photos));
            if(Helper::arrayCheck($vehicle_photos)){
                /** Remove First Element out of Array  **/
                reset($vehicle_photos);
                if((string)array_search($vehicleDetails->id.".jpg", $vehicle_photos) == "0"){
                    $key = key($vehicle_photos);
                    unset($vehicle_photos[$key]);
                    $vehicle_photos = array_values($vehicle_photos);
                }
                /** Remove First Element out of Array  **/

                $data["vehiclePhotos"] = $vehicle_photos;
                $data["vehiclePhotosCount"] = count($vehicle_photos);
            }
            //exit(var_dump($vehicle_photos));
            /*$photos_arr = array();
            foreach($vehicle_photos as $vehicle_photo){
                if(count(Helper::explodeX(array("_",".jpg"), $vehicle_photo)) == 3){
                    array_push($photos_arr, Helper::explodeX(array("_",".jpg"), $vehicle_photo)[1]);
                }
            }
            arsort($photos_arr);
            $tmpArray = array_values($photos_arr);
            $a = array_shift($tmpArray);
            exit(var_dump($a));*/
        }

        $optionalOptions = json_decode($api->getVehicleOptionalOptions($token, $vehicleId));
        if(!isset($optionalOptions->error) && !empty($optionalOptions)){
            $data["optionalOptions"] = $optionalOptions;
        }

        $extraOptions = json_decode($api->getVehicleExtrasOptions($token, $vehicleId));
        if(!isset($extraOptions->error) && !empty($extraOptions)){
            $data["extraOptions"] = $extraOptions;
        }

        $data["breadcrumb_auction"] = $data["breadcrumb"];
        return view("main.trading.stock-vehicle-detail", $data);
    }

    private function getTypeName($objects, $code)
    {
        $value = $code;
        if(!empty($code)){
            foreach($objects as $obj){
                if ($code === $obj->code) {
                    $value = $obj->value;
                }
            }
        }
        return $value;
    }

    public function sendToAuction(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $start_price = $request->input("starting_price");
        $buy_now_price = $request->input("buy_now_price");
        $auction_session_id = $request->input("auction_time_selected_id");

        /** CHECK BOX **/
        $all_info_input = $request->input("all_info");
        $all_info = isset($all_info_input) && $all_info_input ? true : false;

        $ppsr_input = $request->input("ppsr");
        $ppsr = isset($ppsr_input) && $ppsr_input ? true : false;

        $term_on_sale_input = $request->input("term_on_sale");
        $term_on_sale = isset($term_on_sale_input) && $term_on_sale_input ? true : false;

        $extended_input = $request->input("extended");
        $extended = isset($extended_input) && $extended_input ? true : false;

        $ex_rental_input = $request->input("ex_rental");
        $ex_rental = isset($ex_rental_input) && $ex_rental_input ? true : false;

        $ex_hire_input = $request->input("ex_hire");
        $ex_hire = isset($ex_hire_input) && $ex_hire_input ? true : false;
        /** end CHECK BOX **/

        $send_or_resend = $request->input("send_or_resend");//flag to decide to call which API

        $params = array(
            "start_price" => $start_price,
            "buy_now_price" => $buy_now_price,
            "ppsr_cleared" => $ppsr,
            "all_info_correct" => $all_info,
            "terms_agreed" => $term_on_sale,
            "extended" => $extended,
            "ex_rental" => $ex_rental,
            "ex_hire" => $ex_hire,
            "auctionSessionId" => $auction_session_id,
        );

        $valuation_id = $request->input("valuation_id");

        switch(strtolower($send_or_resend)){
            case "send":
                $result = null;
                $params["valuation_id"] = $valuation_id;
                $result = json_decode($api->sendToAuction($token, $params));
                break;
            case "resend":
                $auctionId = $request->input("auction_id");
                $params["auctionId"] = $auctionId;
                $result = json_decode($api->resendAuction($token, $params));
                break;
        }


        if(Helper::resultCheck($result) === true){
            LOG::debug("sendToAuction - params: ".json_encode($params));

            /** RELEASE NEW VALUATION VERSION **/
            $releaseResult = json_decode($api->releaseValuationVersionId($token, $valuation_id));
            if(Helper::resultCheck($releaseResult) === true){
                LOG::debug("sendToAuction - result: ".json_encode($releaseResult));
            }else{
                return Helper::resultCheck($releaseResult);
            }
            return $result->success ? 'true' : 'false';
        }
        return Helper::resultCheck($result);
    }

    public function getAuctionTimes(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $sessionDate = $request->input("session_date");
        $sessionDate = date("Y-m-d", strtotime($sessionDate));

        $auctionSessionList = json_decode($api->getAllAuctionSession($token, array("sessionDate" => $sessionDate)));
        if(!isset($auctionSessionList->error) && !empty($auctionSessionList->sessionList)){
            $sessionListTime = array();
            foreach($auctionSessionList->sessionList as $sessionList){
                /*LOG::debug("getAuctionTimes - session time: ".strtotime($sessionList->time.":00"));
                $dateTime = new DateTime($sessionList->time);
                LOG::debug(date_default_timezone_get());
                if ($dateTime->diff(new DateTime)->format('%R') == '+') {
                    $sessionListTime[$sessionList->id] = $sessionList->time;
                }*/
                $sessionListTime[$sessionList->id] = $sessionList->time;
            }
            return json_encode($sessionListTime);
        }
        return null;
    }

    public function getAuctionInfoById(Request $request, $auctionId)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $auctionInfo = json_decode($api->getAuctionInfoById($token, $auctionId));
        if(!isset($auctionInfo->error) && !empty($auctionInfo)){
            return json_encode($auctionInfo);
        }
        return null;
    }

    public function cancelAuctionRequest(Request $request, $auctionId)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $result = json_decode($api->cancelAuctionRequest($token, array("auction_id"=>$auctionId)));
        if(!isset($result->error) && $result->success){
            return $result->success ? 'true' : 'false';
        }
        return $result->error->code;
    }

    public function uploadPhotoToAzure($vehicleId, $featured = "")
    {
        $azure = new \App\Library\AzureCloud();

        if(strtolower($featured) != "featured"){
            $vehicle_photos =  $azure->getBlobsNameByVehicleId($vehicleId);
            $latest_photo = 1;
            if(Helper::arrayCheck($vehicle_photos)){
                $photos_arr = array();
                foreach($vehicle_photos as $vehicle_photo){
                    if(count(Helper::explodeX(array("_",".jpg"), $vehicle_photo)) == 3){
                        array_push($photos_arr, Helper::explodeX(array("_",".jpg"), $vehicle_photo)[1]);
                    }
                }
                if(Helper::arrayCheck($photos_arr)){
                    arsort($photos_arr);
                    $tmpArray = array_values($photos_arr);
                    $latest_photo = (int)array_shift($tmpArray) + 1;
                }
                /*exit(var_dump($latest_photo));*/
            }

            if(count($vehicle_photos) < 10){
                $res = $azure->uploadVehicleImage("mo_vehicle_photo_upload", $vehicleId."_".$latest_photo);
                if(!empty($res)){
                    return $res;
                }
                return null;
            }
        }else{
            $res = $azure->uploadVehicleImage("mo_vehicle_featured_photo_upload", $vehicleId);
            if(!empty($res)){
                return $res;
            }
            return null;
        }


        return null;
    }

    public function uploadEditedPhotoToAzure(Request $request)
    {
        $azure = new \App\Library\AzureCloud();

        $vehicleImageName = $request->input("vehicle_image_name");
        $dataURI = $request->input("data_uri");

        $res = $azure->uploadEditedVehicleImage($dataURI, $vehicleImageName);
        if(!empty($res)){
            return $res;
        }
        return null;
    }

    public function deletePhotoFromAzure($vehicleName)
    {
        $azure = new \App\Library\AzureCloud();

        //return 1;
        $res = $azure->deleteVehicleImage($vehicleName);
        if($res){
            return 1;
        }
        return 0;
    }

    public function reviewBeforeSubmitForAuction(Request $request, $vehicleId, $auctionId = NULL)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $channels = $request->channels;

        $breadcrumb = $auctionId ? "Resend To Auction" : "Send To Auction";
        $data = array(
            "header" => "Auction",
            "breadcrumb" => $breadcrumb,
            "channels" => $channels
        );

        $limit = 21;
        $offset = 0;

        $params = array(
            "limit" => $limit,
            "offset" => $offset,
            "vehicle_id" => $vehicleId //specify one Vehicle only
        );

        $data["send_or_resend"] = "send";


        if(isset($auctionId)) {
            $data["auction_id"] = $auctionId;
            $data["send_or_resend"] = "resend";

            $auctionInfo = json_decode($api->auctionsSearchItem($token, $auctionId));

            $dateTimeArr = explode(' ', $auctionInfo->auction_session_start_date);
            $auctionInfo->auctionDate =  date('d-m-Y', strtotime($auctionInfo->auction_session_start_date));
            $auctionInfo->auctionTime = date('H:i:s', strtotime($auctionInfo->auction_session_start_date));
            $data["auctionInfo"] = $auctionInfo;
        }



        $vehicleStockInfo = json_decode($api->getVehicleStockList($token, $params));
        $data["vehicleStockInfo"] = null;
        if(Helper::resultCheck($vehicleStockInfo) === true && Helper::arrayCheck($vehicleStockInfo) && count($vehicleStockInfo) != 0){
            /** GET SPECIAL DISCLOSURES VALUE **/
            $special_disclosures_name= "";
            if(isset($vehicleStockInfo[0]->special_disclosures)){
                $special_disclosures_arr = explode(",",$vehicleStockInfo[0]->special_disclosures);
                $specialDisclosures = $api->getTypeCode($token, array("type"=>"SPECIAL_DISCLOSURES"));
                foreach ($special_disclosures_arr as $special_disclosures)
                {
                    $special_disclosures_name .= Helper::getTypeName($specialDisclosures, (int)$special_disclosures);
                    if((int)$special_disclosures != end($special_disclosures_arr)){
                        $special_disclosures_name .= ", ";
                    }
                }
            }

            /** GET OWNERSHIP TYPE VALUE **/
            $ownershipTypes = $api->getTypeCode($token, array("type"=>"OWNERSHIP_TYPE"));
            $ownership_type_name = "";
            if(isset($vehicleStockInfo[0]->ownership_type->code)){
                $ownership_type_name = Helper::getTypeName($ownershipTypes, (int)$vehicleStockInfo[0]->ownership_type->code);
            }

            /** MAP COLLECTION **/
            $collection = collect($vehicleStockInfo)->map(function ($item, $key) use ($special_disclosures_name, $ownership_type_name) {
                $item->trade_price_format = $item->trade_price?number_format($item->trade_price, 2):number_format(0.00, 2);
                $item->ownership_type_value = $ownership_type_name;
                if(!empty($item->ownership_type->value)){
                    $item->ownership_type_value .= ": ".$item->ownership_type->value;
                }
                $item->special_disclosures_name = $special_disclosures_name;
                return $item;
            });
            $data["vehicleStockInfo"] = $collection[0];

            /** GET VALUATION LATEST VERSION **/
            $valuations = json_decode($api->getValuationById($token, $collection[0]->valuation_id));
            $latest_version_id = '';
            if(Helper::resultCheck($valuations) === true){
                $latest_version_id = $valuations->version;
            }
            $data["latest_version_id"] = $latest_version_id;

            /** 1ST - CREATE A NEW VERSION **/
            $result = json_decode($api->createValuationVersionId($token, null, $collection[0]->valuation_id));
            if(Helper::resultCheck($result) === true) {
                $version_id = $result->newVersionId;
            } else {
                if($request->session()->get('id') == $result->error->detail->user_id){
                    $version_id = $latest_version_id;
                }else{
                    /* TODO: return error */
                    $data["error"] = "Cannot get inspection items right now. The valuation is modifying by someone else.";
                }
            }

            /** GET INSPECTION ITEMS **/
            if(isset($version_id)) {
                $data_inspection_items = $this->getInspectItem($request, $collection[0]->valuation_id, $version_id);
                $data = array_merge($data, $data_inspection_items);
                $data["latest_version_id"] = $version_id;
            }

            //exit(var_dump(json_encode($data_inspection_items)));

            /** GET AUCTION SESSIONS **/
            $auctionSessionList = json_decode($api->getAllAuctionSession($token, null));
            if(!isset($auctionSessionList->error) && !empty($auctionSessionList->sessionList)){
                $sessionListDate = array();
                foreach($auctionSessionList->sessionList as $sessionList){
                    array_push($sessionListDate, $sessionList->date);
                }
                $data["session_list_date"] = implode(",",(array_unique($sessionListDate)));
            }

            /** VEHICLE PHOTOS **/
            $data["vehiclePhotos"] = array();
            $azure = new \App\Library\AzureCloud();
            $vehicle_photos = $azure->getBlobsNameByVehicleId($vehicleStockInfo[0]->id);
            if(Helper::arrayCheck($vehicle_photos)){
                $data["vehiclePhotos"] = $vehicle_photos;
            }
        }

        $data['js'][] = 'trading/stocklist/reviewBeforeSubmitForAuction';

        $data["breadcrumb_auction"] =  $data["breadcrumb"];
        return view('main.trading.stocklist.send-to-auction', $data);
    }

    private function getInspectItem(Request $request, $valuationId, $valuationVersion)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();
        $data = array();
        $generalReconditionCost = $api->getGeneralReconditionCost($token, $valuationId);
        $generalRecost = 0;
        if(Helper::resultCheck($generalReconditionCost) === true){
            $generalRecost = $generalReconditionCost->generalReconditionCost;
            $data["generalReconditionCost"] = number_format($generalRecost, 2);
        }
        $inspectionItems = json_decode($api->getInspectionItemsByValuationVersion($token, $valuationId, $valuationVersion));
        if(Helper::resultCheck($inspectionItems) === true){
            $collection = collect($inspectionItems)->map(function ($item, $key) use ($generalRecost) {
                if(!empty($item)){
                    $total = 0;
                    $total_format = "";
                    foreach ($item as $ins_item) {
                        if(count($ins_item->items) > 0){
                            foreach ($ins_item->items as $it) {
                                $total += $it->repair_cost;
                            }
                        }
                    }
                    if ($key == 'exterior_section') {
                        $total += $generalRecost;
                    }
                    if ($total > 0) {
                        $total_format = number_format($total, 2);
                    }
                    $item["total_cost"] = $total;
                    $item["total_cost_format"] = $total_format;
                    return $item;
                }
                return $item;
            });
            $collectionItems = json_decode($collection);
            $data["inspectionItems"] = $collectionItems;
        }
        $damageTotal = 0;
        if (isset($collectionItems->exterior_section->total_cost)) {
            $damageTotal += $collectionItems->exterior_section->total_cost;
        }
        if (isset($collectionItems->interior_section->total_cost)) {
            $damageTotal += $collectionItems->interior_section->total_cost;
        }
        if (isset($collectionItems->mechanical_section->total_cost)) {
            $damageTotal += $collectionItems->mechanical_section->total_cost;
        }
        $data["damageTotal"] = $damageTotal;
        $data["damageTotal_format"] = number_format($damageTotal, 2);
        return $data;
    }

    public function cancelModifyingVersion(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $valuation_id = $request->input("valuation_id");

        $result = json_decode($api->cancelValuationVersionId($token, null, $valuation_id));
        if(Helper::resultCheck($result) === true){
            return 1;
        }
        return Helper::resultCheck($result);
    }

}
