<?php

namespace App\Http\Controllers;

use App\Library\Helper;
use Illuminate\Http\Request;

class ExternalOfferGroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('check-token');
        $this->middleware('check-role');
        $this->middleware('check-permission');
        $this->middleware('handling-channel');
        $this->middleware('get-menu-access');
    }

    public function initExternalForm(Request $request, $type, $externalId = '')
    {
        $api = new \App\Library\APICaller();
        $token = $request->session()->get('token');
        $channels = $request->channels;

        $data = array(
            "header" => "Configure",
            "breadcrumb" => "External Offer Group",
            "channels" => $channels
        );

        if($type == 'new'){
            $data["action"] = "New";
        }else if ($type == 'edit'){
            $data["action"] =  "Edit";
            $groupInfo = json_decode($api->getExternalOfferGroupById($token, $externalId));
            if(!isset($groupInfo->error)){
                $data["groupInfo"] = $groupInfo;
                $location_ids = array();
                if(!empty($groupInfo->listLocation) && !empty($groupInfo->listLocation)){
                    foreach($groupInfo->listLocation as $listLocation){
                        if(!empty($listLocation->locationList)){
                            foreach($listLocation->locationList as $location){
                                array_push($location_ids, $location->locationId);
                            }
                        }
                    }
                }
                $data["location_ids"] = $location_ids;
            }
        }
        $params = array("query" => null);
        $data["dealersList"] = null;
        $dealersList = json_decode($api->getDealerLocationsList($token, $params));
        if(Helper::resultCheck($dealersList) === true){
            $data["dealersList"] = $dealersList;
        }

        return view("main.dealership.create-external-offer-group", $data);
    }

    public function viewExternalOfferGroups(Request $request)
    {
        $channels = $request->channels;
        $data = array(
            "header" => "Configure",
            "breadcrumb" => "External Offer Group",
            "channels" => $channels
        );

        return view('main.dealership.external-groups', $data);
    }

    public function getExternalOfferGroupsList(Request $request)
    {
        $token = $request->session()->get("token");
        $search_txt = $request->input("mo_txt_search");
        $offset = $request->input("offset");

        $api = new \App\Library\APICaller();

        $params = array(
            "limit" => 20,
            "offset" => $offset,
            "query" => $search_txt
        );

        $groupsList = json_decode($api->getExternalGroups($token, $params));

        if (!isset($groupsList->error)) {
            $groups_list_response_json = array('data' => array ());
            foreach ($groupsList->externalOfferGroupList as $key => $value) {
                $arr = array(
                    0 => $value->id,
                    1 => $value->name,
                    2 => $value->description
                );
                array_push($groups_list_response_json["data"], $arr);
            }

            $groups_list_response_json["count"] = $groupsList->list_size->count;

            if (!$groups_list_response_json["data"]) {
                return null;
            }
            return json_encode($groups_list_response_json);

        } else {
            return null;
        }
    }

    public function searchExternalOfferGroupsList(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $query = trim($request->get("term"));
        if (!isset($query)) {
            $query = null;
        }

        $params = array("query" => $query);

        $dealersList = json_decode($api->getDealerLocationsList($token, $params));
        $auto_complete = array();
        if (isset($dealersList)) {
            foreach ($dealersList as $dealer) {
                $data = array(
                    "id" => $dealer->dealerId,
                    "label" => $dealer->dealerName,
                    "value" => $dealer->dealerName,
                    "location" => $dealer->locationList
                );
                array_push($auto_complete, $data);
            }
        }
        return json_encode($auto_complete);
    }

    public function createExternalOfferGroup(Request $request)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        /** GET INPUT FOR EXTERNAL OFFER GROUP **/
        $group_name = $request->input("group_name");
        $group_desc = $request->input("group_desc");
        $company_id = $request->session()->get('company_id');
        $location_select = $request->input("select_location");


        $params = array(
            "name" => $group_name,
            "companyId" => $company_id,
            "description" => $group_desc,
            "location" => $location_select
        );

        $result = json_decode($api->createExternalOfferGroup($token, $params));

        if(!isset($result->error)){
            return 1;
        }
        return 0;
    }

    public function updateExternalOfferGroup(Request $request, $externalId)
    {
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        /** GET INPUT FOR EXTERNAL OFFER GROUP **/
        $group_name = $request->input("group_name");
        $group_desc = $request->input("group_desc");
        $company_id = $request->session()->get('company_id');
        $location_select = $request->input("select_location");


        $params = array(
            "name" => $group_name,
            "companyId" => $company_id,
            "description" => $group_desc,
            "location" => $location_select
        );

        $result = json_decode($api->updateExternalOfferGroup($token, $params, $externalId));

        if(!isset($result->error)){
            return 1;
        }
        return 0;
    }

    public function removeExternalOfferGroup(Request $request, $externalId){
        $token = $request->session()->get("token");
        $api = new \App\Library\APICaller();

        $result = json_decode($api->deleteExternalOfferGroupById($token, $externalId));

        if(!isset($result->error)){
            return 1;
        }
        return 0;
    }
}
