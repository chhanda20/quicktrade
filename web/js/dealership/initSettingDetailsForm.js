/** Popup **/

$("#head-valuer-approval").on("change",function(){
    if($('input#head-valuer-approval').is(':checked') === false){
        openSettingWarningPopup();
    }
});

function openSettingWarningPopup(el) { // get the class name in arguments here
    $.magnificPopup.open({
        items: {
            src: '#mo-settings-action-confirm-popup',
        },
        type: 'inline'
    });
}
var magnificPopup = $.magnificPopup.instance;

$('.mo-cancel-btn').click(function (){
    magnificPopup.close();
});