$(document).ready(function(){
    var pathname = window.location.pathname;
    var dispute_type = "current";
    var token = $("input[name='_token']").val();/** COMMON TOKEN **/

    if(pathname.indexOf("/current") != -1){
        dispute_type = "current";
    }else if(pathname.indexOf("/resolved") != -1){
        dispute_type = "resolved";
    }

    $(".mo-block").first().toggleClass("mo-block-active");
    $("#mo-dispute-info").removeClass("hidden").fadeIn("slow");

    $(".mo-block").click(function(){
        $(".mo-block").removeClass("mo-block-active");
        $(this).toggleClass("mo-block-active");

        var mo_txt = $(this).find('.mo-block-txt').text();

        if(mo_txt == "all"){
            loadDisputeData("all");
            infinityScroll("all");
            $("#dispute-breadcrumb").text("All");
        }
        else if(mo_txt == "sold"){
            loadDisputeData("seller");
            infinityScroll("seller");
            $("#dispute-breadcrumb").text("Sold");
        }else if(mo_txt == "bought"){
            loadDisputeData("buyer");
            infinityScroll("buyer");
            $("#dispute-breadcrumb").text("Bought");
        }
    });

    function loadDisputeData(type){
        $(document).off(".scroll");
        $(document).on("ajaxStart.loadDisputeList", function () {
            $(".mo-loading-image").removeClass("hidden");
        });
        $.ajax({
            type: 'POST',
            data: {
                _token: token,
                limit: 20,
                offset: 0,
                type:type
            },
            url: "/dispute/get-dispute-data/"+dispute_type,
            success: function (html) {
                console.log(html);
                $("#mo-dispute-info").empty();
                $("#mo-dispute-info").append(html);
            },
            error: function (data) {
                console.log('error: ' + data);
            },
            complete: function () {
                $(".mo-loading-image").addClass("hidden");
            }
        });
    }

    //infinityScroll("all");

    function infinityScroll(type) {
        var win = $(window);
        win.off('scroll.windowscroll');

        if (typeof(Storage) !== "undefined") {
            localStorage.setItem("dispute_grid_content_length", 0);
        }

        // Each time the user scrolls
        win.on('scroll.windowscroll', function() {
            // End of the document reached?
            var content = $("#mo-dispute-info");
            if ($(document).height() - win.height() == win.scrollTop()) {
                var content_length = content.children(":not(.mo-loading-wrapper)").length;
                /*console.log("content_length "+content_length);
                console.log(localStorage.getItem("dispute_grid_content_length"));*/
                if(content_length > localStorage.getItem("dispute_grid_content_length" )){
                    localStorage.setItem("dispute_grid_content_length", content_length);
                    var token = $("input[name='_token']").val();
                    $(document).off(".loadDisputeList");
                    $(document).on("ajaxStart.scroll", function () {
                        $('.mo-loading-wrapper').show();
                        $('.mo-loading-icon').show();
                    });

                    var data = {
                        _token: token,
                        limit: 20,
                        offset: content_length,
                        type:type
                    };

                    $.ajax({
                        type: "POST",
                        url: "/dispute/get-dispute-data/"+dispute_type,
                        data: data,
                        success: function(html) {
                            if(html == ""){
                                win.off('scroll.windowscroll');
                            } else {
                                content.append(html);
                            }
                        },
                        complete: function(){
                            $(".mo-loading-icon").parent().not(":last").remove();
                        }
                    });

                    $(document).on("ajaxStop.scroll", function () {
                        $('.mo-loading-icon').hide();
                        $('.mo-loading-wrapper').hide();
                    });
                }
            }
        });
    }

    $(document).on("click",".mo-dispute-view",function() {
        window.location.href = "/dispute/info/"+dispute_type+"/"+$(this).data("transaction-id")+"/"+$(this).data("dispute-id");
    });
});