/** SHOW SORT MENU **/
var hash = window.location.hash;

$(".mo-sort-stock-list").show();
$(".mo-sort-live-auction-list").hide();

/** SET LOCAL STORAGE TO APPLY FILTER **/
var data = {
    content_id: "#mo-vehicle-grid-wrapper",
    url: "/trading/get-vehicle-list"
};
localStorage.setItem("elem_to_filter", JSON.stringify(data));

//console.log(JSON.parse(localStorage.getItem("elem_to_filter")));

/** TOGGLE FIRST BLOCK **/
$('.mo-block').first().toggleClass('mo-block-active');

/** TOGGLE BY HASH IN URL **/
function toggleTab(tab_id, tab_block, data){
    /** CHANGE BREADCRUMB **/
    $("span.yellow-text").text("Stock List");
    if(tab_id.children("span").text().toLowerCase().indexOf("submitted") != -1){
        $("span.yellow-text").text("Submitted List");
    }

    $('.mo-block').removeClass('mo-block-active');
    tab_id.toggleClass('mo-block-active');
    $('.mo-block-inside-list').hide();
    tab_block.hide();
    tab_block.removeClass('hidden').fadeIn('slow');
    localStorage.setItem("elem_to_filter", JSON.stringify(data));
}

if(hash){
    if (hash.indexOf("#vehicle-available") != -1) {
        /** SET LOCAL STORAGE TO APPLY FILTER **/
        var data = {
            content_id: "#mo-vehicle-grid-wrapper",
            url: "/trading/get-vehicle-list"
        };
        toggleTab($('#mo-vehicle-available-tab'), $('#mo-vehicle-grid-wrapper'), data);
        $(".mo-sort-stock-list").show();
        $(".mo-sort-auction-list").hide();
    } else if (hash.indexOf("#vehicle-submitted") != -1){
        /** SET LOCAL STORAGE TO APPLY FILTER **/
        var data = {
            content_id: "#mo-vehicle-submitted-grid-wrapper",
            url: "/trading/get-submitted-list"
        };
        toggleTab($('#mo-vehicle-submitted-tab'), $('#mo-vehicle-submitted-grid-wrapper'), data);
        $(".mo-sort-auction-list").show();
        $(".mo-sort-stock-list").hide();
    }
}

/** BLOCKS TOGGLE **/
$('.mo-block').click(function() {
    $('.mo-block').removeClass('mo-block-active');
    $(this).toggleClass('mo-block-active');

    var mo_txt = $(this).find('.mo-block-txt').text();

    $('.mo-block-inside-list').hide();

    $("#mo-btn-reset-filter").click();
    $("#mo-txt-search").val("");
    /** SET LOCAL STORAGE TO APPLY FILTER **/
    localStorage.removeItem("vehicle_grid_content_length");
    var data = {
        content_id: "#mo-vehicle-grid-wrapper",
        url: "/trading/get-vehicle-list"
    };

    if(mo_txt.toLowerCase() == 'available') {
        $('#mo-vehicle-grid-wrapper').hide();
        $('#mo-vehicle-grid-wrapper').removeClass('hidden').fadeIn('slow');
        $(".mo-sort-stock-list").show();
        $(".mo-sort-auction-list").hide();
    } else if(mo_txt.toLowerCase() == 'submitted for auction') {
        data = {
            content_id: "#mo-vehicle-submitted-grid-wrapper",
            url: "/trading/get-submitted-list"
        };
        $('#mo-vehicle-submitted-grid-wrapper').hide();
        $('#mo-vehicle-submitted-grid-wrapper').removeClass('hidden').fadeIn('slow');
        $(".mo-sort-auction-list").show();
        $(".mo-sort-stock-list").hide();
    }
    localStorage.setItem("elem_to_filter", JSON.stringify(data));
    infinityScroll("");

    /** CHANGE BREADCRUMB **/
    $("span.yellow-text").text("Stock List");
    if($(this).children("span").text().toLowerCase().indexOf("submitted") != -1){
        $("span.yellow-text").text("Submitted List");
    }
});

/** AUCTION DATE PICKER **/
var auction_date_array = $("#mo-vehicle-auction-date-array").val().split(',');
//console.log(auction_date_array);
$('#mo-vehicle-auction-date').datepicker({
    todayHighlight: true,
    orientation: 'bottom',
    beforeShowDay:function(Date){
        //var curr_date = Date.toJSON().substring(0,10);
        var months = Date.getMonth() + 1;
        if(months.toString().length == 1){
            months = "0"+months;
        }

        var dates = Date.getDate();
        if(dates.toString().length == 1){
            dates = "0"+dates;
        }

        var curr_date = Date.getFullYear() + '-' + months + '-' + dates;
        /**  show available dates in auction session only **/
        if (auction_date_array.indexOf(curr_date) == -1) return false;
    }
});

$(".glyphicon-calendar").click(function(){
    $("#mo-vehicle-auction-date").focus();/** focus to show calendar **/
});

$('#mo-vehicle-auction-date').on('changeDate', function(ev){
    $(this).datepicker('hide');
    $(".mo-auction-time").parent().next().removeClass("disabled-div");
});

$('#mo-vehicle-auction-date').change(function(){
    $(document).off(".sendToAuction");
    $.ajax({
        type: "POST",
        url: "/trading/get-auction-times",
        data: {
            _token: $("input[name='_token']").val(),
            session_date: $(this).val()
        },
        success: function(data) {
            if(data){
                $("#mo-vehicle-auction-time-select option[value]").remove();
                time_data = JSON.parse(data);
                console.log(time_data);
                $.each(time_data, function (index, value) {
                    //console.log(index + ':' + value["section_name"]);
                    $("#mo-vehicle-auction-time-select").append($("<option>", {
                        value: index,
                        text: value
                    }));
                });
            }
        },
        complete: function(){
        }
    });
});

/** AUCTION TIME PICKER **/
$('#mo-vehicle-auction-time').timepicker();

/** code to not allow any changes to be made to input field **/
$('#mo-vehicle-auction-time, #mo-vehicle-auction-date').keydown(function() {
    return false;
});

/** SEND TO AUCTION - POPUP **/
sendToAuctionPopup();

/** POPULATE DATA - POPUP **/
$(document).on("click","#mo-send-popup",function() {
    $('.alert').addClass('hidden');
    $("#mo-send-to-auction-form").find("input[type=text], input[type=number], select").val("");
    $("#mo-send-to-auction-form").find("input[type=checkbox]").attr('checked', false);

    var ancestor = $(this).parents(".mo-content-wrapper");
    var vehicle_name = ancestor.children(".mo-vehicle-name").text().trim();
    var vehicle_days_stock = ancestor.children().children(".mo-vehicle-days-stock").text().trim();
    var vehicle_stock_no = ancestor.children().children(".mo-vehicle-stock-no").text().trim();
    var vehicle_reg_no = ancestor.children().children(".mo-vehicle-reg-no").text().trim();
    var vehicle_total_cost = ancestor.children().children(".mo-vehicle-total-cost").text().trim();
    var vehicle_location = ancestor.children().children(".mo-vehicle-location").text().trim();
    var valuation_id = ancestor.children("#mo-valuation-id").val();
    var vehicle_id = ancestor.children("#mo-vehicle-id").val();

    $("#mo-vehicle-name").text(vehicle_name);
    $("#mo-vehicle-days-stock").text(vehicle_days_stock);
    $("#mo-vehicle-stock-no").text(vehicle_stock_no);
    $("#mo-vehicle-reg-no").text(vehicle_reg_no);
    $("#mo-vehicle-total-cost").text(vehicle_total_cost);
    $("#mo-vehicle-location").text(vehicle_location);
    $(".mo-valuation-id").val(valuation_id);
    $(".mo-send-resend").val("send");

    /** POPULATE VEHICLE IMAGE IN POPUP **/
    String.prototype.filename=function(extension){
        var s= this.replace(/\\/g, '/');
        s= s.substring(s.lastIndexOf('/')+ 1);
        return extension? s.replace(/[?#].+$/, ''): s.split('.')[0];
    }
    var vehicle_filename = $('#mo-vehicle-img').attr('src').filename();
    $('#mo-vehicle-img').attr('src',function(i,e){
        $(this).attr("onerror", "this.onerror=null; this.src='../images/main/no_photo_available@3x.png'");
        return $(this).attr('src').replace(vehicle_filename+".jpg",vehicle_id+".jpg?"+Math.floor(Math.random() * 100000));
    });
});

$(document).on("click","#mo-resend-popup",function() {
    $('.alert').addClass('hidden');
    $("#mo-send-to-auction-form").find("input[type=text], input[type=number], select").val("");
    $("#mo-send-to-auction-form").find("input[type=checkbox]").attr('checked', false);

    var ancestor = $(this).parents(".mo-content-wrapper");
    var vehicle_name = ancestor.children(".mo-vehicle-name").text().trim();
    var valuation_id = ancestor.children("#mo-valuation-id").val();
    var vehicle_id = ancestor.children("#mo-vehicle-id").val();
    var auction_id = ancestor.children("#mo-auction-id").val();

    $("#mo-vehicle-name").text(vehicle_name);
    $(".mo-valuation-id").val(valuation_id);
    $(".mo-auction-id").val(auction_id);
    $(".mo-send-resend").val("resend");

    /** GET AUCTION INFO **/
    $(document).off(".sendToAuction");
    $(document).off(".cancelAuction");
    $.ajax({
        type: "POST",
        url: "/trading/get-auction-info/"+auction_id,
        data: {_token:$("input[name='_token']").val()},
        success: function(data) {
            if(data){
                var obj = JSON.parse(data);
                $("#mo-vehicle-days-stock").text(obj.day_in_stock);
                $("#mo-vehicle-stock-no").text(obj.stock_number);
                $("#mo-vehicle-reg-no").text(obj.registration_number);
                $("#mo-vehicle-total-cost").text("$"+parseFloat(obj.trade_price).toFixed(2));
                $("#mo-vehicle-location").text(obj.location_text);
            }
        }
    });

    /** POPULATE VEHICLE IMAGE IN POPUP **/
    String.prototype.filename=function(extension){
        var s= this.replace(/\\/g, '/');
        s= s.substring(s.lastIndexOf('/')+ 1);
        return extension? s.replace(/[?#].+$/, ''): s.split('.')[0];
    }
    var vehicle_filename = $('#mo-vehicle-img').attr('src').filename();
    $('#mo-vehicle-img').attr('src',function(i,e){
        $(this).attr("onerror", "this.onerror=null; this.src='../images/main/no_photo_available@3x.png'");
        return $(this).attr('src').replace(vehicle_filename+".jpg",vehicle_id+".jpg?"+Math.floor(Math.random() * 100000));
    });
});

$(document).on("click","#mo-cancel-popup",function() {
    $('.alert').addClass('hidden');
    /** SET AUCTION ID FOR CANCEL REQUEST **/
    var ancestor = $(this).parents(".mo-content-wrapper");
    var auction_id = ancestor.children("#mo-auction-id").val();

    $("#mo-confirm-cancel").attr("data-auction-id", auction_id);
});

$(document).on("click","#mo-reject-info-popup",function() {
    /** POPULATE REJECT REASON **/
    $("#mo-reject-reason").text($(this).data("reject-reason"));
    $("#mo-reject-content").text($(this).data("comment"));
});

/** VALIDATE FORM - POPUP **/
$('#mo-send-to-auction-form').validate({
    rules:{
        starting_price: "required",
        auction_date: "required",
        buy_now_price: "required",
        auction_time_selected_id: "required",
        all_info: "required",
        ppsr: "required",
        term_on_sale: "required"
    },
    errorElement: 'div',
    errorClass: 'mo-error-wrapper',
    errorPlacement: function(error, element) {
        if (element.attr("name") == "auction_date" ) {
            error.appendTo("#errorToShow_auction_date");
        } else if (element.attr("name") == "auction_time" ) {
            error.appendTo("#errorToShow_auction_time");
        } else if (element.attr("name") == "all_info" ) {
            error.appendTo("#errorToShow_all_info");
        } else if (element.attr("name") == "ppsr" ) {
            error.appendTo("#errorToShow_ppsr");
        }  else if (element.attr("name") == "term_on_sale" ) {
            error.appendTo("#errorToShow_term_on_sale");
        } else {
            error.insertAfter(element);// default placement
        }
    },
    messages:{
        starting_price: "<div class='mo-error-wrapper'><img src='../images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
        auction_date: "<div class='mo-error-wrapper'><img src='../images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
        buy_now_price: "<div class='mo-error-wrapper'><img src='../images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
        auction_time_selected_id: "<div class='mo-error-wrapper'><img src='../images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
        all_info: "<div class='mo-error-wrapper'><img src='../images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
        ppsr: "<div class='mo-error-wrapper'><img src='../images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
        term_on_sale: "<div class='mo-error-wrapper'><img src='../images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>"
    },
    submitHandler: function(form){
        //form.submit();
    }
});

$(document).on("click","#mo-confirm-cancel",function() {
    var content_submitted = $("#mo-vehicle-submitted-grid-wrapper");
    var url_submitted = "/trading/get-submitted-list";
    var content = $("#mo-vehicle-grid-wrapper");
    var url = "/trading/get-vehicle-list";

    var magnificPopup = $.magnificPopup.instance;
    $(document).off(".sendToAuction");
    $(document).on("ajaxStart.cancelAuction", function () {
        $(".mo-loading-image").removeClass("hidden");
    });
    $(document).on("ajaxStop.cancelAuction", function () {
        $(".mo-loading-image").addClass("hidden");
    });
    $.ajax({
        type: "POST",
        url: "/trading/cancel-request-auction/"+$(this).data("auction-id"),
        data: {_token: $("input[name='_token']").val()},
        success: function(data) {
            //console.log(data);
            if(data == "true"){
                magnificPopup.close();
                getVehicleList(content_submitted, url_submitted);
                getVehicleList(content, url);
            }else{
                $(".alert-danger").find(".mo-error-message").text(data);
                $(".alert-danger").removeClass("hidden");
            }
        }
    });
});

$(document).on("click","#mo-confirm-send-auction",function() {
    var form = $('#mo-send-to-auction-form');
    var content = $("#mo-vehicle-grid-wrapper");
    var url = "/trading/get-vehicle-list";
    var content_submitted = $("#mo-vehicle-submitted-grid-wrapper");
    var url_submitted = "/trading/get-submitted-list";

    var magnificPopup = $.magnificPopup.instance;
    if(form.valid()){
        $(document).off(".cancelAuction");
        $(document).on("ajaxStart.sendToAuction", function () {
            $(".mo-loading-image").removeClass("hidden");
        });
        $(document).on("ajaxStop.sendToAuction", function () {
            $(".mo-loading-image").addClass("hidden");
        });
        $.ajax({
            type: "POST",
            url: "/trading/send-to-auction",
            data: form.serialize(),
            success: function(data) {
                //console.log(data);
                if(data == "true"){
                    magnificPopup.close();
                    getVehicleList(content, url);
                    getVehicleList(content_submitted, url_submitted);
                }else{
                    $(".alert-danger").find(".mo-error-message").text(data);
                    $(".alert-danger").removeClass("hidden");
                    $("#mo-send-to-auction-popup").scrollTop(0);
                }
            }
        });
    }
});

/** INFINITE SCROLL **/
infinityScroll("");