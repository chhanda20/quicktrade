/** TOGGLE FIRST BLOCK **/
var hash = window.location.hash;

$('.mo-block-border-bottom').first().toggleClass('mo-block-border-bottom-active');

/** TOGGLE BY HASH IN URL **/
function toggleTab(tab_id, tab_block){
    $('.mo-block-border-bottom').removeClass('mo-block-border-bottom-active');
    tab_id.toggleClass('mo-block-border-bottom-active');
    $('.mo-block-inside-list').hide();
    tab_block.hide();
    tab_block.removeClass('hidden').fadeIn('slow');
}

if(hash){
    if (hash.indexOf("#details-model") != -1) {
        toggleTab($('#details-model-tab'), $('#mo-model-specs-block'));
    } else if (hash.indexOf("#details-options") != -1){
        toggleTab($('#details-options-tab'), $('#mo-options-extras-block'));
    }
}

/** BLOCKS TOGGLE **/
$('.mo-block-border-bottom').click(function() {
    $('.mo-block-border-bottom').removeClass('mo-block-border-bottom-active');
    $(this).toggleClass('mo-block-border-bottom-active');

    var mo_txt = $(this).find('.mo-block-txt').text();

    $('.mo-block-inside-list').hide();

    if(mo_txt.toLowerCase() == 'model specs & standard features') {
        $('#mo-model-specs-block').hide();
        $('#mo-model-specs-block').removeClass('hidden').fadeIn('slow');
    } else if(mo_txt.toLowerCase() == 'options & extras') {
        $('#mo-options-extras-block').hide();
        $('#mo-options-extras-block').removeClass('hidden').fadeIn('slow');
    }
});

/** TOGGLE DELETE ICON **/
$(document).on("mouseenter","#bx-pager a",function() {
    $(this).find(".mo-icon-wrapper").show();
    if($(this).next(".mo-featured-photo").length == 1){
        $(this).children("img:first-child").attr("style","opacity:0.2");
        $(this).next(".mo-featured-photo").show();
    }
});

$(document).on("mouseleave","#bx-pager a",function() {
    $(this).find(".mo-icon-wrapper").hide();
    if($(this).next(".mo-featured-photo").length == 1){
        $(this).children("img:first-child").attr("style","");
        $(this).next(".mo-featured-photo").hide();
    }
});

$(".mo-featured-photo").hover(
    function(){
        $(this).prev("a").find(".mo-icon-wrapper").show();
        $(this).prev("a").children("img:first-child").attr("style","opacity:0.2");
        $(this).show();
    },
    function(){
        $(this).prev("a").find(".mo-icon-wrapper").hide();
        $(this).prev("a").children("img:first-child").attr("style","");
        $(this).hide();
    }
);

/** DELETE VEHICLE IMAGE **/
$(document).on("click",".mo-icon-wrapper",function() {
    var this_elem = $(this);
    var vehicle_photo_src = this_elem.parent().find("img:first-child").attr("src");
    var vehicle_photo_name = vehicle_photo_src.substring(vehicle_photo_src.lastIndexOf('/')+1);
    var token = $('input[name="_token"]').val();

    //$(document).off(".updateStatus");
    $(document).on("ajaxStart.vehicleLoading", function () {
        this_elem.parent().find("img:first-child").attr("style","opacity:0.4");
        this_elem.prev('.mo-icon-loading').removeClass("hidden");
    });

    $.ajax({
        type:'POST',
        url: "/trading/delete-vehicle-photo/"+vehicle_photo_name,
        data:{
            _token: token
        },
        success:function(data) {
            if(data){
                console.log(data);
                if(data == 1){

                    if(this_elem.parent().next(".mo-featured-photo").length == 1){
                        $("#bx-pager a:first-child").find("img:first").attr("src", "/images/main/img-add-photo@3x.png");
                        $("#bx-pager a:first-child").addClass("mo-add-featured-photo-wrapper vertical-center");
                        $("#bx-pager a:first-child").attr("style", "margin-right: 10px;width: 175px !important;");
                        $("#bx-pager a:first-child").find("img:first").addClass("img-responsive mo-add-photo align-center");
                    }else{
                        /** check photo counts **/
                        $("#mo-vehicle-photos-count").val(parseInt($("#mo-vehicle-photos-count").val())- 1);
                        var photos_count = $("#mo-vehicle-photos-count").val();
                        console.log(parseInt(photos_count)+1);
                        if(parseInt(photos_count)+1 < 10){
                            if($("#mo-vehicle-add-photo").hasClass("hidden")){
                                $("#mo-vehicle-add-photo").removeClass("hidden");
                            }
                        }

                        this_elem.parent().remove();
                        return false;
                    }
                }

            }
        },
        error: function(data){
            console.log("error");
            console.log(data);
        }
    });

    $(document).on("ajaxStop.vehicleLoading", function () {
        this_elem.parent().find("img:first-child").attr("style","");
        this_elem.prev('.mo-icon-loading').addClass("hidden");
        if(this_elem.parent().next(".mo-featured-photo").length == 1){
            this_elem.remove();
        }
    });
    location.reload();
});

/** VEHICLE THUMBS IMAGE SLIDER **/
var light_slider = $("#bx-pager").lightSlider({
    pager: false,
    /*autoWidth: true,*/
    item: 5,
    onSliderLoad: function (el) {
        $(".mo-featured-photo").removeClass("lslide");
        $(".mo-featured-photo").attr("style","");
    }
});

$(document).ready(function() {
    $("#bx-pager a:first-child").find("img:first-child").one("load", function() {

        var this_elem = $(this);
        setTimeout( function(){
            // Do something after 1 second
            var first_child_width = $("#bx-pager a:first-child").width();
            $(".mo-featured-photo").width(first_child_width);

            if(this_elem.attr("src").indexOf("no_photo_available") != -1){
                this_elem.attr("src", "/images/main/img-add-photo@3x.png");
                this_elem.parent().addClass("mo-add-featured-photo-wrapper vertical-center");
                this_elem.parent().attr("style", "margin-right: 10px;width: 175px !important;");
                this_elem.addClass("img-responsive mo-add-photo align-center");
                this_elem.parent().find(".mo-icon-wrapper").addClass("hidden");

                var first_child_width = $("#bx-pager a:first-child").width();
                $(".mo-featured-photo").width(first_child_width);
            }
        }  , 1000 );

    }).each(function() {
        if(this.complete) $(this).load();
    });
});

/** VEHICLE IMAGE SLIDER **/
var vehicleSlider = $('.bxslider').bxSlider({
    /*adaptiveHeight: true,*/
    mode: 'fade',
    pagerCustom: '#bx-pager',
    onSliderLoad: function(){
        light_slider.refresh();
    }/*,
     onSlideAfter: function(){
     light_slider.refresh();
     }*/
});

/** check photo counts **/
var photos_count = $("#mo-vehicle-photos-count").val();//old count
console.log(parseInt(photos_count)+1);
if(parseInt(photos_count)+1 == 10){
    $("#mo-vehicle-photo-form").parent().addClass("hidden");
}

/** ADD VEHICLE PHOTO **/
$("#mo-vehicle-image-upload").change(function (){
    if ($("#mo-vehicle-image-upload")) {
        $("#mo-vehicle-image-upload").submit();
    }
});

$("#mo-vehicle-photo-form").on("submit",(function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    var this_elem = $(this);
    this_elem.find("img.mo-add-photo").attr("src", "/images/perform_valuation/img-loading.gif");

    $.ajax({
        type:'POST',
        url: "/trading/upload-vehicle-photo/"+$("#vehicle-id").val(),
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success:function(data) {
            if(data != null && data != "error"){
                console.log(data);
                var slider_append_elem = $("ul.bxslider").children("li:first-child").clone();
                slider_append_elem.css({ "z-index": 0, "display":"none" });
                slider_append_elem.children("img").attr("src", data+"?"+Math.random());
                $("ul.bxslider").append(slider_append_elem);

                var pager_append_elem = $("div#bx-pager").children("a:first-child").clone();
                pager_append_elem.removeClass("active");
                pager_append_elem.attr("data-slide-index", vehicleSlider.getSlideCount());
                pager_append_elem.children("img").attr("src", data+"?"+Math.random());
                $("div#bx-pager").append(pager_append_elem);
                this_elem.find("img.mo-add-photo").attr("src", "/images/main/img-add-photo@3x.png");

                /** check photo counts **/
                $("#mo-vehicle-photos-count").val(parseInt($("#mo-vehicle-photos-count").val())+ 1);
                var photos_count = $("#mo-vehicle-photos-count").val();
                console.log(parseInt(photos_count)+1);
                if(parseInt(photos_count)+1 == 10){
                    this_elem.parent().addClass("hidden");
                }

                /** clear input file **/
                $("#mo-vehicle-image-upload").val("");

                vehicleSlider.reloadSlider();
                light_slider.refresh();
            }else if(data == "error") {
                console.log("error");
                $(".alert-danger").find(".mo-error-message").text("your uploading file is not a image. Please try again!");
                $(".alert-danger").removeClass("hidden");
            }else{
                console.log("null");
                $(".alert-danger").find(".mo-error-message").text("there is some error happens when uploading image. Please try again!");
                $(".alert-danger").removeClass("hidden");
            }
        },
        error: function(data){
            console.log("error");
            console.log(data);
        },
        complete: function (data) {
            this_elem.find("img.mo-add-photo").attr("src", "/images/main/img-add-photo@3x.png");
        }
    });
}));

/** ADD VEHICLE FEATURED PHOTO **/
$("#mo-vehicle-featured-photo-upload").change(function (){
    if ($("#mo-vehicle-featured-photo-upload")) {
        $("#mo-vehicle-featured-photo-upload").submit();
    }
});

$("#mo-vehicle-featured-photo-form").on("submit",(function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    var featured_img_src = $("#bx-pager a:first-child").find("img:first-child").attr("src");
    $("#bx-pager a:first-child").find("img:first-child").attr("src", "/images/perform_valuation/img-loading.gif");

    $.ajax({
        type:'POST',
        url: "/trading/upload-vehicle-photo/"+$("#vehicle-id").val()+"/featured",
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success:function(data) {
            if(data != null && data != "error"){
                console.log(data);
                $("#bx-pager a:first-child").find("img:first-child").attr("src", data+"?"+Math.random());
                $("ul.bxslider").children("li:first-child").children("img").attr("src", data+"?"+Math.random());
                //set width of featured icon
                var first_child_width = $("#bx-pager a:first-child").width();
                $(".mo-featured-photo").width(first_child_width);

                $("#bx-pager a:first-child").find(".mo-icon-wrapper").removeClass("hidden");

                vehicleSlider.reloadSlider();
                light_slider.refresh();
            }else if(data == "error") {
                console.log("error");
                $(".alert-danger").find(".mo-error-message").text("your uploading file is not a image. Please try again!");
                $(".alert-danger").removeClass("hidden");
                $("#bx-pager a:first-child").find("img:first").attr("src", featured_img_src);
            }/*else{
                console.log("null");
                $(".alert-danger").find(".mo-error-message").text("there is some error happens when uploading image. Please try again!");
                $(".alert-danger").removeClass("hidden");
                $("#bx-pager a:first-child").find("img:first").attr("src", "/images/main/img-add-photo@3x.png");
                $("#bx-pager a:first-child").addClass("mo-add-featured-photo-wrapper vertical-center");
                $("#bx-pager a:first-child").attr("style", "margin-right: 10px;width: 175px !important;");
                $("#bx-pager a:first-child").find("img:first").addClass("img-responsive mo-add-photo align-center");
            }*/
        },
        error: function(data){
            console.log("error");
            console.log(data);
        }
    });
}));

$('.bxslider').imageSmudge({
    height:400
});