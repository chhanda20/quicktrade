/** SHOW FILTER & SORT MENU HEADER **/
$(".mo-dropdown-menu-wrapper").removeClass("hidden");
$(".mo-sort-stock-list").hide();
$(".mo-sort-auction-list").hide();

/** STOP MENU DISAPPEAR **/
$(document).on('click', '#mo-trading-filter', function (e) {
    e.stopPropagation();
});

/** HANDLE FILTER **/
var token = $("input[name='_token']").val();
$("#mo-make-filter").hover(function(){
    /** GET MAKE LIST **/
    getMakeList(token);
    $(this).unbind('mouseenter mouseleave');//force hover run one time
});

$("#mo-location-filter").hover(function(){
    /** GET LOCATION LIST **/
    getLocationList(token);
    $(this).unbind('mouseenter mouseleave');//force hover run one time
});

$("#mo-transmission-filter").hover(function(){
    /** GET TRANSMISSION LIST **/
    getTransmissionList(token);
    $(this).unbind('mouseenter mouseleave');//force hover run one time
});

/** MONEY FORMAT COMMA NUMBER **/
function digits(num){
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/** RANGE SLIDER **/
function rangeDisplay(slider_elem, display_elem, min, max, step, unit, type, from, to, filter_display){
    slider_elem.slider({
        range: true,
        min: min,
        max: max,
        values: [ min, max ],
        step: step,
        slide: function( event, ui ) {
            var value_0 = ui.values[ 0 ];
            var value_1 = ui.values[ 1 ];
            if(type == "number"){
                value_0 = digits(value_0);
                value_1 = digits(value_1);
            }
            display_elem.val( unit + value_0 + " - "+ unit + value_1 );
        },
        change: function( event, ui ) {
            /** SET VALUES FOR HIDDEN FIELDS **/
            var value_0 = ui.values[ 0 ];
            var value_1 = ui.values[ 1 ];
            from.val(value_0);
            to.val(value_1);
            if(type == "number"){
                value_0 = digits(value_0);
                value_1 = digits(value_1);
            }
            filter_display.text( unit + value_0 + " - "+ unit + value_1 );
        }
    });
    var value_0 = slider_elem.slider( "values", 0 );
    var value_1 = slider_elem.slider( "values", 1 );
    if(type == "number"){
        value_0 = digits(value_0);
        value_1 = digits(value_1);
    }
    display_elem.val( unit + value_0 +
    " - "+ unit + value_1 );
}

rangeDisplay($( "#price-slider-range" ), $( "#price-amount" ), 0, 100000, 1000, "$", "number", $( "#price-range-from" ), $( "#price-range-to" ), $( "#price-filter" ));
rangeDisplay($( "#rating-slider-range" ), $( "#rating-amount" ), 0, 5, 1, "", "number", $( "#rating-range-from" ), $( "#rating-range-to" ), $( "#rating-filter" ));
rangeDisplay($( "#odo-slider-range" ), $( "#odo-amount" ), 0, 1000000, 5000, "", "number", $( "#odo-range-from" ), $( "#odo-range-to" ), $( "#odo-filter" ));
var year = new Date().getFullYear();
rangeDisplay($( "#year-slider-range" ), $( "#year-amount" ), 1975, year, 1, "", "", $( "#year-range-from" ), $( "#year-range-to" ), $( "#year-filter" ));

/*** start PRE-DEFINED FUNCTION ***/
function resetSlider() {
    var $slider = $( "#price-slider-range" );
    $slider.slider("values", 0, 0);
    $slider.slider("values", 1, 100000);
}

function getMakeList(token){
    $.ajax({
        type: 'POST',
        data: {_token: token},
        url: "/get-glasses-data/make",
        success: function (data) {
            //console.log('success: ' + data);
            //$("#mo-2nd-level").empty();
            $.each(data, function(key, value) {
                //console.log(value);
                var append_elem = $("#mo-2nd-level-clone").children(".menu-item").clone();
                var first_child_clone = append_elem.find(">:first-child");
                var last_child_clone = append_elem.find(">:last-child");

                last_child_clone.children(".menu-item").remove();
                first_child_clone.find("input").attr("id", "mo-make-"+key);
                first_child_clone.find("input").addClass("mo-make-checkbox");
                first_child_clone.find("input").attr("value", value);
                first_child_clone.find("label").attr("for", "mo-make-"+key);
                first_child_clone.find("span.mo-make-txt").text(value);
                $("#mo-make-filter").find("#mo-2nd-level").append(append_elem);
            });
        },
        error: function (data) {
            console.log('error: ' + data);
        },
        complete: function (data) {
            $("#mo-make-filter").find("#mo-2nd-level").children(".menu-item").hover(function() {
                /** GET MODEL LIST - BY MAKE **/
                //console.log("hover");
                var first_child = $(this).find(">:first-child");
                getModelList($(this), token, first_child.find("input").val());
                $(this).unbind('mouseenter mouseleave');//force hover run one time
            });

            /** CHECK BOX - FILTER **/
            var menu_item_elem = $("#mo-make-filter").find("#mo-2nd-level").children(".menu-item");
            var checkbox_1st = menu_item_elem.find(">:first-child").find("input");
            checkbox_1st.change(function(){
                var parent_menu_item = $(this).parents(".menu-item");
                var last_child_elem = parent_menu_item.find(">:last-child").children(".menu-item");
                if($(this).is(':checked')){
                    last_child_elem.each(function(i, obj) {
                        $(obj).find("input").prop('checked',true);
                    });
                    checkLengthCheckBox(menu_item_elem);
                }else{
                    last_child_elem.each(function(i, obj) {
                        $(obj).find("input").prop('checked',false);
                    });
                    checkLengthCheckBox(menu_item_elem);
                }
            });

            /*$(".mo-model-checkbox").change(function(){
             console.log('changed');
             var parent_menu_item = $(this).parents("#mo-2nd-level").children(".menu-item");
             checkLengthCheckBox(parent_menu_item);
             });*/

        }
    });
}

function checkLengthCheckBox(menu_item_elem){
    if(menu_item_elem.find("input:checkbox:checked").length > 0){
        $("#mo-variant-filter").parent().removeClass("disabled-div");
        var make_arr = [];var model_arr = [];var model_obj = {};
        /*menu_item_elem.find(".mo-make-checkbox:checked").each(function(i, obj) {
         //make_arr[i] = $(obj).val();
         make_arr[$(obj).data("id")] = $(obj).val();
         });*/

        var vehicle_name = "";
        menu_item_elem.find(".mo-model-checkbox:checked").each(function(i, obj) {
            //model_arr[i] = $(obj).val();
            if(vehicle_name != $(obj).data("vehicle-name")){
                vehicle_name = $(obj).data("vehicle-name");
                if(vehicle_name != -1){
                    model_obj[vehicle_name] = [];//initialize array inside object
                }
            }
            if(vehicle_name != -1){
                model_obj[vehicle_name].push($(obj).val());//add item into array in object
            }
        });
        //console.log(model_obj);
        $("#mo-variant-filter").find("#mo-2nd-level").empty();
        for (var key in model_obj) {
            if (model_obj.hasOwnProperty(key)) {
                //console.log(key + " -> " + model_obj[key]);
                for (var i = 0; i<model_obj[key].length; i++){
                    getVariantList(token, key, model_obj[key][i]);
                }
            }
        }
    }else{
        $("#mo-variant-filter").parent().addClass("disabled-div");
    }
}

function getModelList(this_elem, token, make){
    $.ajax({
        type: 'POST',
        data: {_token: token, make: make},
        url: "/get-glasses-data/family",
        success: function (data) {
            //console.log('success: ' + data);
            //this_elem.children("#mo-3rd-level").empty();
            $.each(data, function(key, value) {
                //console.log(value);
                var elem_1st = $("#mo-2nd-level-clone").children(".menu-item");
                var last_child_clone = elem_1st.find(">:last-child").children(".menu-item").clone();

                this_elem.children("#mo-3rd-level").removeClass("hidden");
                last_child_clone.find("input").attr("id", "mo-model-"+make.replace(/\s/g,'')+"-"+key);
                last_child_clone.find("input").addClass("mo-model-checkbox");
                last_child_clone.find("input").attr("value", value);
                last_child_clone.find("input").attr("data-vehicle-name", make);
                last_child_clone.find("label").attr("for", "mo-model-"+make.replace(/\s/g,'')+"-"+key);
                last_child_clone.find("span.mo-model-txt").text(value);
                this_elem.children("#mo-3rd-level").append(last_child_clone);
            });
        },
        complete: function (data){
            $(".mo-model-checkbox").change(function(){
                var parent_menu_item = $(this).parents("#mo-2nd-level").children(".menu-item");
                checkLengthCheckBox(parent_menu_item);
            });
        },
        error: function (data) {
            console.log('error: ' + data);
        }
    });
}

function getVariantList(token, make_arr, model_arr){
    /*console.log(make_arr);
     console.log(model_arr);*/
    $.ajax({
        type: 'POST',
        data: {_token: token, make: make_arr, model: model_arr},
        url: "/get-glasses-data/variant",
        success: function (data) {
            //console.log('success: ' + data);
            var newArray = data.filter(function(v){return v!==''});
            //$("#mo-variant-filter").find("#mo-2nd-level").empty();
            var total = newArray.length;
            $.each(newArray, function(key, value) {
                //console.log(value);
                if(value != ''){
                    var append_elem = $("#mo-2nd-level-clone").children(".menu-item").clone();
                    var first_child_clone = append_elem.find(">:first-child");
                    var last_child_clone = append_elem.find(">:last-child");

                    if (key === 0) {
                        // this is the first one
                        var make_model_elem = $("#mo-make-model-name").clone().removeClass("hidden");
                        make_model_elem.find("span.mo-make-model-txt").text(make_arr+"/"+model_arr);
                        append_elem.prepend(make_model_elem);
                    }

                    last_child_clone.remove();
                    first_child_clone.find("input").attr("id", "mo-variant-"+model_arr+"-"+value.replace(/\s/g,''));
                    first_child_clone.find("input").attr("value", value);
                    first_child_clone.find("input").attr("data-vehicle-name", make_arr);
                    first_child_clone.find("input").attr("data-model-name", model_arr);
                    first_child_clone.find("input").addClass("mo-variant-checkbox");
                    first_child_clone.find("label").attr("for", "mo-variant-"+model_arr+"-"+value.replace(/\s/g,''));
                    first_child_clone.find("span.mo-make-txt").attr("class", "mo-variant-txt padding-left-5");
                    first_child_clone.find("span.mo-variant-txt").text(value);
                    first_child_clone.find("span.glyphicon-menu-right").remove();
                    $("#mo-variant-filter").find("#mo-2nd-level").append(append_elem);
                }
            });
        },
        error: function (data) {
            console.log('error: ' + data);
        }
    });
}

function getLocationList(token){
    $.ajax({
        type: 'POST',
        data: {_token: token},
        //url: "/get-locations-by-logged-user",
        url: "/get-all-locations",
        success: function (data) {
            //console.log('success: ' + data);
            $.each(data, function(key, value) {
                //console.log(value);
                var append_elem = $("#mo-2nd-level-clone").children(".menu-item").clone();
                var first_child_clone = append_elem.find(">:first-child");
                var last_child_clone = append_elem.find(">:last-child");

                last_child_clone.remove();
                first_child_clone.find("input").attr("id", "mo-location-"+key);
                first_child_clone.find("input").attr("data-location-id", key);
                first_child_clone.find("input").addClass("mo-location-checkbox");
                first_child_clone.find("input").attr("value", value);
                first_child_clone.find("label").attr("for", "mo-location-"+key);
                first_child_clone.find("span.mo-make-txt").attr("class", "mo-location-txt padding-left-5");
                first_child_clone.find("span.mo-location-txt").text(value);
                first_child_clone.find("span.glyphicon-menu-right").remove();
                $("#mo-location-filter").find("#mo-2nd-level").append(append_elem);
            });
        },
        error: function (data) {
            console.log('error: ' + data);
        }
    });
}

function getTransmissionList(token){
    $.ajax({
        type: 'POST',
        data: {_token: token},
        url: "/get-glasses-data/transmission",
        success: function (data) {
            //console.log('success: ' + data);
            $.each(data, function(key, value) {
                //console.log(value);
                var append_elem = $("#mo-2nd-level-clone").children(".menu-item").clone();
                var first_child_clone = append_elem.find(">:first-child");
                var last_child_clone = append_elem.find(">:last-child");

                last_child_clone.remove();
                first_child_clone.find("span.col-xs-10").attr("class", "col-xs-12");
                first_child_clone.find("input").attr("id", "mo-tranmission-"+value.replace(/\s/g,''));
                first_child_clone.find("input").addClass("mo-transmission-checkbox");
                first_child_clone.find("input").attr("value", value);
                first_child_clone.find("label").attr("for", "mo-tranmission-"+value.replace(/\s/g,''));
                first_child_clone.find("span.mo-make-txt").attr("class", "mo-transmission-txt padding-left-5");
                first_child_clone.find("span.mo-transmission-txt").text(value);
                first_child_clone.find("span.glyphicon-menu-right").remove();
                $("#mo-transmission-filter").find("#mo-2nd-level").append(append_elem);

            });
        },
        error: function (data) {
            console.log('error: ' + data);
        }
    });
}

function getVehicleList(content, url){
    var token = $("input[name='_token']").val();
    var data = {
        _token: token,
        limit: 21,
        offset: 0
    };
    if(localStorage.getItem("apply_filter")){
        var data_obj = JSON.parse(localStorage.getItem("apply_filter"));

        if(data_obj !== null && typeof data_obj === 'object'){
            data = $.extend({}, data, data_obj);
        }
    }
    //console.log(data);
    $.ajax({
        type: "POST",
        /*url: "/trading/get-vehicle-list",*/
        url: url,
        data: data,
        success: function(html) {
            content.empty();
            content.append(html);
        },
        complete: function(){
            sendToAuctionPopup();
            approveRequestPopup();
            rejectRequestPopup();
            liveAuctionPopup();
        }
    });
}
function infinityScroll(data_obj) {
    var data_storage = JSON.parse(localStorage.getItem("elem_to_filter"));
    var content = $(data_storage.content_id);
    var url = data_storage.url;

    var win = $(window);
    win.off('scroll.windowscroll');

    if (typeof(Storage) !== "undefined") {
        localStorage.setItem("vehicle_grid_content_length", 0);
    }

    // Each time the user scrolls
    win.on('scroll.windowscroll', function() {
        // End of the document reached?
        if ($(document).height() - win.height() == win.scrollTop()) {
            var content_length = content.children(":not(.mo-loading-wrapper)").length;
            /*console.log("content_length "+content_length);
             console.log(localStorage.getItem("vehicle_grid_content_length"));*/
            if(content_length > localStorage.getItem("vehicle_grid_content_length" )){
                localStorage.setItem("vehicle_grid_content_length", content_length);
                var token = $("input[name='_token']").val();
                $(document).off(".updateStatus");
                $(document).on("ajaxStart.scroll", function () {
                    $('.mo-loading-wrapper').show();
                    $('.mo-loading-icon').show();
                });

                var data = {
                    _token: token,
                    limit: 21,
                    offset: content_length
                };
                if(data_obj != ''){
                    data = $.extend({}, data, data_obj);
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(html) {
                        if(html == ""){
                            win.off('scroll.windowscroll');
                        } else {
                            content.append(html);
                        }
                    },
                    complete: function(){
                        sendToAuctionPopup();
                        approveRequestPopup();
                        rejectRequestPopup();
                        liveAuctionPopup();
                        preBidPopup();
                        buyNowPopup();
                        $(".mo-loading-icon").parent().not(":last").remove();

                        //Hide loading icon
                        $('.mo-loading-icon').hide();
                        $('.mo-loading-wrapper').hide();
                    }
                });

                $(document).on("ajaxStop.scroll", function () {
                    $('.mo-loading-icon').hide();
                    $('.mo-loading-wrapper').hide();
                });
            }
        }
    });
}

function showFilterText(this_elem){
    if(this_elem.text() != ''){
        this_elem.removeClass("hidden");
    }
}

function displayFilterHandle(){
    var location_filter_txt = $(".mo-location-checkbox:checked:first").val();
    var transmission_filter_txt = $(".mo-transmission-checkbox:checked:first").val();
    var variant_filter_txt = $(".mo-variant-checkbox:checked:first").val();
    var make_filter_txt = $(".mo-make-checkbox:checked:first").val();
    var model_filter_txt = $(".mo-model-checkbox:checked:first").val()!=null?"/"+$(".mo-model-checkbox:checked:first").val():"";
    var make_model_filter_txt = make_filter_txt+model_filter_txt;
    if($(".mo-location-checkbox:checked").length > 1){
        location_filter_txt+=",...";
    }else if($(".mo-location-checkbox:checked").length == 0){
        location_filter_txt="";
    }

    if($(".mo-transmission-checkbox:checked").length > 1){
        transmission_filter_txt+=",...";
    }else if($(".mo-transmission-checkbox:checked").length == 0){
        transmission_filter_txt="";
    }

    if($(".mo-variant-checkbox:checked").length > 1){
        variant_filter_txt+=",...";
    }else if($(".mo-variant-checkbox:checked").length == 0){
        variant_filter_txt="";
    }

    if($(".mo-model-checkbox:checked").length > 1){
        make_model_filter_txt+=",...";
    }else if($(".mo-make-checkbox:checked").length == 0){
        make_model_filter_txt="";
    }
    showFilterText($("#price-filter"));
    showFilterText($("#rating-filter"));
    showFilterText($("#odo-filter"));
    showFilterText($("#year-filter"));
    $("#location-filter").text(location_filter_txt);
    showFilterText($("#location-filter"));
    $("#transmission-filter").text(transmission_filter_txt);
    showFilterText($("#transmission-filter"));
    $("#variant-filter").text(variant_filter_txt);
    showFilterText($("#variant-filter"));
    $("#make-model-filter").text(make_model_filter_txt);
    showFilterText($("#make-model-filter"));

    var filter_length = $(".mo-filter").not( ".hidden" ).length;
    if(filter_length > 0){
        $("#mo-filter-header").addClass("slide-txt-display");
        $("#mo-filter-header").text("filter ("+filter_length+")");
    }
}

function resetFilterHandle(){
    /** RESET FILTER **/
    $("#price-filter").addClass("hidden");
    $("#rating-filter").addClass("hidden");
    $("#odo-filter").addClass("hidden");
    $("#year-filter").addClass("hidden");
    $("#location-filter").addClass("hidden");
    $("#transmission-filter").addClass("hidden");
    $("#variant-filter").addClass("hidden");
    $("#make-model-filter").addClass("hidden");

    /** UNCHECK CHECKBOX **/
    uncheck($(".mo-make-checkbox:checked"));
    uncheck($(".mo-model-checkbox:checked"));
    uncheck($(".mo-variant-checkbox:checked"));
    $("#mo-variant-filter").parent().addClass("disabled-div");
    uncheck($(".mo-transmission-checkbox:checked"));
    uncheck($(".mo-location-checkbox:checked"));

    /** RESET SORT **/
    $(".glyphicon-ok").hide();
    $(".dropdown-header").children("span:first").text("SORT BY:");
    $(".glyphicon-sort-by-alphabet-alt").hide();
    $(".glyphicon-sort-by-alphabet").show();

    /** RESET STYLE FOR FILTER AND SORT **/
    $("#mo-filter-header").removeClass("slide-txt-display");
    $("#mo-filter-header").text("filter");
    $("#mo-sort-header").removeClass("slide-txt-display");

    //resetSlider();
}

function callAjaxGetList(content, param_obj, url){
    if(localStorage.getItem("apply_filter")){
        var data_obj = JSON.parse(localStorage.getItem("apply_filter"));
        if(data_obj !== null && typeof data_obj === 'object'){
            var data = $.extend({}, data_obj, param_obj);
            localStorage.setItem("apply_filter", JSON.stringify(data));
        }
    }else{
        localStorage.setItem("apply_filter", JSON.stringify(param_obj));
    }
    //var url = "/trading/get-vehicle-list";
    getVehicleList(content, url);
    var scroll_data_obj = JSON.parse(localStorage.getItem("apply_filter"));
    //console.debug(scroll_data_obj); /** DEBUG **/
    infinityScroll(scroll_data_obj);
}

function makeInfo(){
    var make_info = [];
    var make_lenth = $(".mo-make-checkbox:checked").length;
    var model_lenth = $(".mo-model-checkbox:checked").length;
    var variant_lenth = $(".mo-variant-checkbox:checked").length;
    //console.log("variant length: "+variant_lenth);
    if(variant_lenth > 0){
        $(".mo-variant-checkbox:checked").each(function(i, obj) {
            var make_info_obj = {
                make: $(obj).data("vehicle-name"),
                model: $(obj).data("model-name"),
                variant: $(obj).val()
            };
            make_info[i] = make_info_obj;
        });
    }
    else if(model_lenth > 0 && variant_lenth == 0){
        $(".mo-model-checkbox:checked").each(function(i, obj) {
            var make_info_obj = {
                make: $(obj).data("vehicle-name"),
                model: $(obj).val(),
                variant: ""
            };
            make_info[i] = make_info_obj;
        });
    }
    else if(make_lenth > 0 && model_lenth == 0 && variant_lenth == 0){
        $(".mo-make-checkbox:checked").each(function(i, obj) {
            var make_info_obj = {
                make: $(obj).val(),
                model: "",
                variant: ""
            };
            make_info[i] = make_info_obj;
        });
    }

    return make_info;
}

function uncheck(checkbox_elem){
    checkbox_elem.each(function(i, obj) {
        $(obj).prop('checked', false);
    });
}

/** start - STOCK LIST POPUP **/
function sendToAuctionPopup(){
    $(document).on("click",".mo-cancel-btn",function() {
        var magnificPopup = $.magnificPopup.instance;
        magnificPopup.close();
    });

    $(".mo-stock-popup").magnificPopup({
        type:'inline',
        showCloseBtn: true,
        closeOnBgClick: false
    });
}
/** end - STOCK LIST POPUP **/

/** start - REQUEST LIST POPUP **/
function approveRequestPopup(){
    $(document).on("click",".mo-cancel-btn",function() {
        var magnificPopup = $.magnificPopup.instance;
        magnificPopup.close();
    });

    $(".mo-approve-request-popup").magnificPopup({
        type:'inline',
        showCloseBtn: true,
        closeOnBgClick: false
    });
}

function rejectRequestPopup(){
    $(document).on("click",".mo-cancel-btn",function() {
        var magnificPopup = $.magnificPopup.instance;
        magnificPopup.close();
    });

    $(".mo-reject-request-popup").magnificPopup({
        type:'inline',
        showCloseBtn: true,
        closeOnBgClick: false
    });
}
/** end - REQUEST LIST POPUP **/

/** start - LIVE AUCTION POPUP **/
function liveAuctionPopup(){
    $(document).on("click",".live-auction-cancel-btn",function() {
        var magnificPopup = $.magnificPopup.instance;
        magnificPopup.close();
    });

    var magnificPopupOnCloseFunction = null;
    $(".mo-live-auction-popup").magnificPopup({
        type:'inline',
        showCloseBtn: false,
        closeOnBgClick: false,
        callbacks: {
            close: function(){
                magnificPopupOnCloseFunction && magnificPopupOnCloseFunction();
            }
        }
    });
}
/** end - LIVE AUCTION POPUP **/


/*** end PRE-DEFINED FUNCTION ***/

/** TOGGLE SORT A-Z **/
$(".glyphicon-sort-by-alphabet").click(function(){
    $(this).toggle();
    $(".glyphicon-sort-by-alphabet-alt").toggle();
    var sort_obj = {orderDesc:true};

    var data = JSON.parse(localStorage.getItem("elem_to_filter"));
    var content = $(data.content_id);
    var url = data.url;
    callAjaxGetList(content, sort_obj, url);
});

$(".glyphicon-sort-by-alphabet-alt").click(function(){
    $(this).toggle();
    $(".glyphicon-sort-by-alphabet").toggle();
    var sort_obj = {orderDesc:false};

    var data = JSON.parse(localStorage.getItem("elem_to_filter"));
    var content = $(data.content_id);
    var url = data.url;
    callAjaxGetList(content, sort_obj, url);
});

/** HANDLE SORT CLICK **/
$(".mo-sort-by").click(function(){
    var span_txt = $(this).children("span:first").text();
    $(".dropdown-header").children("span:first").text("SORT BY: "+span_txt);
    $(".glyphicon-ok").hide();
    $(this).children(".glyphicon-ok").toggle();

    var order_by = $(this).children("span:first").data("order-by");
    var sort_obj = {orderBy:order_by};

    var data = JSON.parse(localStorage.getItem("elem_to_filter"));
    var content = $(data.content_id);
    var url = data.url;
    callAjaxGetList(content, sort_obj, url);

    if($(this).find(".glyphicon-ok").css("display") !== "none"){
        $("#mo-sort-header").addClass("slide-txt-display");
    }
});

/*** start - APPLY FILTER ***/
$("#mo-btn-apply-filter").click(function(){
    var data = JSON.parse(localStorage.getItem("elem_to_filter"));
    var content = $(data.content_id);
    var url = data.url;

    var data_obj = {};
    data_obj.odo_range = [$("#odo-range-from").val(), $("#odo-range-to").val()];
    data_obj.year_range = [$("#year-range-from").val(), $("#year-range-to").val()];
    data_obj.price_range = [$("#price-range-from").val(), $("#price-range-to").val()];
    data_obj.rating_range = [$("#rating-range-from").val(), $("#rating-range-to").val()];
    var location_arr=[];var transmission_arr=[];
    $(".mo-location-checkbox:checked").each(function(i, obj) {
        location_arr[i] = $(obj).data("location-id");
    });
    $(".mo-transmission-checkbox:checked").each(function(i, obj) {
        transmission_arr[i] = $(obj).val();
    });
    data_obj.location = location_arr;
    data_obj.transmissionType = transmission_arr;
    data_obj.makeInfo = makeInfo();
    callAjaxGetList(content, data_obj, url);
    displayFilterHandle();
});

$("#mo-btn-reset-filter").click(function(){
    localStorage.removeItem("apply_filter");

    var data = JSON.parse(localStorage.getItem("elem_to_filter"));
    var content = $(data.content_id);
    var url = data.url;
    getVehicleList(content, url);
    infinityScroll("");
    resetFilterHandle();
});

$(window).unload(function() {
    localStorage.removeItem("apply_filter");
    localStorage.removeItem("elem_to_filter");
});
/*** end - APPLY FILTER ***/

/*** start - QUERY SEARCH ***/
$(".mo-txt-search").click(function(){
    var query_obj = {query:$("#mo-txt-search").val()};
    var data = JSON.parse(localStorage.getItem("elem_to_filter"));
    var content = $(data.content_id);
    var url = data.url;
    callAjaxGetList(content, query_obj, url);
});
$("#mo-txt-search").bind("enterKey", function(e){
    var query_obj = {query:$("#mo-txt-search").val()};
    var data = JSON.parse(localStorage.getItem("elem_to_filter"));
    var content = $(data.content_id);
    var url = data.url;
    callAjaxGetList(content, query_obj, url);
});
$('#mo-txt-search').keyup(function(e){
    if(e.keyCode == 13)
    {
        $(this).trigger("enterKey");
    }
});
/*** end - APPLY FILTER ***/