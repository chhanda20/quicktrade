$(document).ready(function(){

    $("#wishlist-make").multiselect({
        maxHeight: 300,
        buttonWidth: '100%'
    });

    $("#wishlist-model").multiselect({
        maxHeight: 300,
        buttonWidth: '100%'
    });

    $("#wishlist-variant").multiselect({
        maxHeight: 300,
        buttonWidth: '100%'
    });

    $("#wishlist-location").multiselect({
        maxHeight: 300,
        buttonWidth: '100%',
    });

    $("#wishlist-transmission-type").multiselect({
        maxHeight: 300,
        buttonWidth: '100%',
    });

    rangeDisplay($( "#wishlist-odo-slider-range" ), $( "#wishlist-odo-amount" ), 0, 1000000, 5000, "", "number", $( "#wishlist-odo-range-from" ), $( "#wishlist-odo-range-to" ) , $( "#wishlist-odo-filter" ));
    var year = new Date().getFullYear();
    rangeDisplay($( "#wishlist-year-slider-range" ), $( "#wishlist-year-amount" ), 1975, year, 1, "", "", $( "#wishlist-year-range-from" ), $( "#wishlist-year-range-to" ), $( "#wishlist-year-filter" ));
    rangeDisplay($( "#wishlist-price-slider-range" ), $( "#wishlist-price-amount" ), 0, 100000, 1000, "$", "number", $( "#wishlist-price-range-from" ), $( "#wishlist-price-range-to" ), $( "#wishlist-price-filter" ));

    /* validate wishlist form */
    /*
    $("#mo-wishlist-form").validate({
        rules: {
            "wishlist_make": "required"
        },
        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        errorPlacement: function(error, element){
            error.insertAfter(element.next());
        },
        messages: {

            "wishlist_make": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>"
           
        }          
    });
    */

});

var selected_make = null;
var model_arr_for_select_box = [];
var selected_model = null;
$(document).ready(function(){

    sessionStorage.setItem("wish_list_make_model_variant_data_checked", 0);

    /** MAKE SECTION **/
    $("#wishlist-make").next().children().find("input[type='checkbox']").click(function(e){
        /* event for make is checked */
        if ($(this).is(":checked")) {

            selected_make = $(this).val();
            //use ajax to get model by make
            $(document).ajaxStart(function() {
                $(".mo-loading-image").removeClass("hidden");
            });
            $.ajax({
                type: 'POST',
                data: {_token: token, make: selected_make},
                url: "/get-glasses-data/family",
                success:function(model_data) {

                    var model_data_arr = model_data.toString().split(',');
                    model_data_arr.clean("");

                    model_arr_for_select_box.push({
                        label: selected_make,
                        children: [
                        ]
                    });

                    $.each(model_data_arr, function(key_model_data, value_model_data) {

                        model_arr_for_select_box[model_arr_for_select_box.length-1].children.push(
                            { label: value_model_data, value: selected_make+"_"+value_model_data }
                        );

                    });

                    //push data to model
                    $("#wishlist-model").multiselect('dataprovider', model_arr_for_select_box);

                    //update or add more make to session local storage
                    var wish_list_make_model_variant_data_checked = null;
                    if (sessionStorage.getItem("wish_list_make_model_variant_data_checked") == 0) {
                        wish_list_make_model_variant_data_checked = {"data_checked": []};
                        
                    } else {
                        wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
                        wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);
                    }

                    wish_list_make_model_variant_data_checked.data_checked.push({
                        "make_name": selected_make,
                        "model_items": []
                    });

                    console.log(wish_list_make_model_variant_data_checked, "after check make");

                    wish_list_make_model_variant_data_checked = JSON.stringify(wish_list_make_model_variant_data_checked);
                    sessionStorage.setItem("wish_list_make_model_variant_data_checked", wish_list_make_model_variant_data_checked);
                    
                    //recheck model
                    $.reUpdateMultiselect("#wishlist-model");

                    //bind event click to model

                    /** MODEL SECTION **/
                    //$("#wishlist-model").next().children().find("input[type='checkbox']").unbind();
                    $("#wishlist-model").next().children().find("input[type='checkbox']").click(function(){
                        
                        /* model is checked (call event change) */
                        if ($(this).is(":checked")) {
                            selected_model = $(this).val().split("_");
                        } 
                        /* model is unchecked */
                        else if (!$(this).is(":checked")) {
                            /* when user uncheck model, it will work here */

                            //get make
                            make_of_model_selected = $(this).val().split("_")[0];

                            var unselect_model = $(this).val();

                            //remove model that uncheck in datachecked session local storage
                            var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
                            wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);
                            console.log("uncheck-mode", wish_list_make_model_variant_data_checked);
                            //begin remove
                            $.each(wish_list_make_model_variant_data_checked.data_checked, function(index, value){
                                console.log(value, "make item");
                                if (value.make_name == make_of_model_selected) {

                                    $.each(value.model_items, function(model_items_index, model_items_value){
                                        if(typeof(model_items_value) != "undefined"){
                                            console.log(model_items_value, "model_items_value");
                                            if (model_items_value.model_name.toString().replace(" ", "") == unselect_model.split("_")[1].toString().replace(" ", "")) {

                                                wish_list_make_model_variant_data_checked.data_checked[index].model_items.splice(model_items_index, 1);
                                                selected_model = false;
                                            }
                                        }

                                    });
                                }
                            });
                            //save remove
                            wish_list_make_model_variant_data_checked = JSON.stringify(wish_list_make_model_variant_data_checked);
                            sessionStorage.setItem("wish_list_make_model_variant_data_checked", wish_list_make_model_variant_data_checked);
                            //console.log($.parseJSON(wish_list_make_model_variant_data_checked), "uncheck");
                            selected_model = false;
                            //remove variant here
                            if(typeof (variant_arr_for_select_box) != "undefined" && variant_arr_for_select_box.length >= 1) {
                                var unselect_model = $(this).val();
                                $.each(variant_arr_for_select_box, function(variant_arr_for_select_box_index, variant_arr_for_select_box_value){

                                    if(variant_arr_for_select_box_value.label == unselect_model.replace("_", " ")) {

                                        delete variant_arr_for_select_box[variant_arr_for_select_box_index];
                                    }
            
                                });
                                //console.log(variant_arr_for_select_box, "affter delete");
                                variant_arr_for_select_box.clean(undefined);
                                //console.log(variant_arr_for_select_box, "affter clean");
                                //push data to variant multi select
                                $("#wishlist-variant").multiselect('dataprovider', variant_arr_for_select_box);
                                //reload checked variant
                                $.reUpdateMultiselect("#wishlist-model");
                            }
                        }
                    });
                    /** end MODEL SECTION **/
                },

                error: function(error){
                    console.log('/get-glasses-data/family error: '+error);
                },
                complete: function(){
                    $(".mo-loading-image").addClass("hidden");
                }
                
            });
        
        } 
        /* event for make is uncheck */
        else if ($(this).is(":checked") == false) {

            selected_model = false;

            var uncheck_make = $(this).val();
            //remove datachecked session storage here
            
            var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
            wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);
            
            //begin remove make session storage
            $.each(wish_list_make_model_variant_data_checked.data_checked, function(index, value){
                if (typeof (value) != "undefined") {
                    if (value.make_name.toString() == uncheck_make) {
                        wish_list_make_model_variant_data_checked.data_checked.splice(index, 1);
                    }
                }
            });

            //save after remove make 
            wish_list_make_model_variant_data_checked = JSON.stringify(wish_list_make_model_variant_data_checked);
            sessionStorage.setItem("wish_list_make_model_variant_data_checked", wish_list_make_model_variant_data_checked);
            
            //remove model here
            if(typeof (model_arr_for_select_box) != "undefined" && model_arr_for_select_box.length >= 1) {
                unselect_make = $(this).val();
                
                $.each(model_arr_for_select_box, function(model_arr_for_select_box_index, model_arr_for_select_box_value){
                    
                    if(model_arr_for_select_box_value.label == unselect_make) {

                        console.log(model_arr_for_select_box_value, "delete variant");
                        //remove variant here
                        if(typeof (variant_arr_for_select_box) != "undefined" && variant_arr_for_select_box.length >= 1) {
                                     
                                $.each(variant_arr_for_select_box, function(variant_arr_for_select_box_index, variant_arr_for_select_box_value){
                                    $.each(model_arr_for_select_box_value.children, function(model_index, model_value){  
                                        
                                        if(variant_arr_for_select_box_value.label == model_value.value.replace("_", " ")) {

                                            delete variant_arr_for_select_box[variant_arr_for_select_box_index];
                                        }

                                    })
                                });
                            
                            //console.log(variant_arr_for_select_box, "affter delete");
                            
                            variant_arr_for_select_box.clean(undefined);

                            //console.log(variant_arr_for_select_box, "affter clean");
                            //push data to model
                            $("#wishlist-variant").multiselect('dataprovider', variant_arr_for_select_box);

                        }

                        delete model_arr_for_select_box[model_arr_for_select_box_index];
                        
                    }
                    
                });
                
                model_arr_for_select_box.clean(undefined);
                //push data to model
                $("#wishlist-model").multiselect('dataprovider', model_arr_for_select_box);

                //using local storage to set selected item here
                //"#wishlist-model"
                $.reUpdateMultiselect("#wishlist-model");

            }

            //rebind event for model
            $("#wishlist-model").next().children().find("input[type='checkbox']").click(function(){
                if ($(this).is(":checked")) {
                    selected_model = $(this).val().split("_");

                } else if (!$(this).is(":checked")) {
                    /* when user uncheck model, it will work here */

                    //get make
                    make_of_model_selected = $(this).val().split("_")[0];

                    var unselect_model = $(this).val();

                    //remove model that uncheck in datachecked session local storage
                    var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
                    wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);
                    console.log("uncheck-mode", wish_list_make_model_variant_data_checked);
                    //begin remove
                    $.each(wish_list_make_model_variant_data_checked.data_checked, function(index, value){
                        console.log(value, "make item");
                        if (value.make_name == make_of_model_selected) {

                            $.each(value.model_items, function(model_items_index, model_items_value){
                                if(typeof(model_items_value) != "undefined"){
                                    console.log(model_items_value, "model_items_value");
                                    if (model_items_value.model_name.toString().replace(" ", "") == unselect_model.toString().replace(" ", "")) {

                                        wish_list_make_model_variant_data_checked.data_checked[index].model_items.splice(model_items_index, 1);
                                        selected_model = false;
                                    }
                                }

                            });
                        }
                    });
                    //save remove
                    wish_list_make_model_variant_data_checked = JSON.stringify(wish_list_make_model_variant_data_checked);
                    sessionStorage.setItem("wish_list_make_model_variant_data_checked", wish_list_make_model_variant_data_checked);

                    selected_model = false;
                    //remove variant here
                    if(typeof (variant_arr_for_select_box) != "undefined" && variant_arr_for_select_box.length >= 1) {
                        var unselect_model = $(this).val();
                        $.each(variant_arr_for_select_box, function(variant_arr_for_select_box_index, variant_arr_for_select_box_value){
                            
                            if(variant_arr_for_select_box_value.label == unselect_model.replace("_", " ")) {
                                delete variant_arr_for_select_box[variant_arr_for_select_box_index];
                            }
    
                        });
                        variant_arr_for_select_box.clean(undefined);
                        //push data to model
                        $("#wishlist-variant").multiselect('dataprovider', variant_arr_for_select_box);
                    }
                }
            });

            //rebind event for variant
            //bind event click for variant list
            $("#wishlist-variant").next().children().find("input[type='checkbox']").click(function(){
                /* variant is checked */
                if($(this).is(":checked")) {

                    var variant_value = $(this).val();

                    var make = variant_value.split("_")[0];
                    var model = variant_value.split("_")[1];
                    var variant = variant_value.split("_")[2];

                    //save variant to datachecked local session storage
                    var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
                    wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);
                    
                    //begin remove make session storage
                    //make
                    $.each(wish_list_make_model_variant_data_checked.data_checked, function(index, value){
                        if (typeof (value) != "undefined") {
                            if (value.make_name.toString() == make) {
                                //model
                                $.each(value.model_items, function(model_items_index, model_items_value) {

                                    if(typeof(model_items_value) != "undefined") {
                                        if (model_items_value.model_name == model) 
                                        {
                                            //if no any variant
                                            if (model_items_value.variant_items.length < 1) {
                                                wish_list_make_model_variant_data_checked.data_checked[index].model_items[model_items_index].variant_items.push({
                                                    "variant_name": variant
                                                });
                                            } else {
                                                //variant
                                                $.each(model_items_value.variant_items, function(variant_items_index, variant_items_value) {

                                                    if (typeof (variant_items_value) != "undefined") 
                                                    {

                                                        if (variant_items_value.variant_name == variant) {
                                                            return false;
                                                        } else {
                                                            //assign variant index to var add_more_variant
                                                           
                                                            //if (parseInt(variant_items_index)+1 == parseInt(variant_items_value.length)) {
                                                                
                                                                wish_list_make_model_variant_data_checked.data_checked[index].model_items[model_items_index].variant_items.push({
                                                                    "variant_name": variant
                                                                });
                                                                return false;
                                                            //}
                                                        }
                                                    }

                                                });
                                            }
                                        }
                                    }
                                    if (model_items_value.variant_items.length == -1) {
                                        wish_list_make_model_variant_data_checked.data_checked[index].model_items[model_items_index].variant_items.push({
                                            "variant_name": variant
                                        });
                                    } else {

                                    }
                                });

                            }
                        }
                    });
                    
                    //save remove
                    wish_list_make_model_variant_data_checked = JSON.stringify(wish_list_make_model_variant_data_checked);
                    sessionStorage.setItem("wish_list_make_model_variant_data_checked", wish_list_make_model_variant_data_checked);

                    var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
                    wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);

                    console.log(wish_list_make_model_variant_data_checked, "variant add");

                } 
                /* variant is uncheck */
                else if (!$(this).is(":checked")) {
                    
                    var variant_value = $(this).val();
                    var make = variant_value.split("_")[0];
                    var model = variant_value.split("_")[1];
                    var variant = variant_value.split("_")[2];
             
                    //remove variant item in datachecked session local storage
                    //save variant to datachecked local session storage
                    var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
                    wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);
                    
                    //begin remove make session storage
                    $.each(wish_list_make_model_variant_data_checked.data_checked, function(index, value){
                        if (typeof (value) != "undefined") {
                            if (value.make_name.toString() == make) {
                                
                                $.each(value.model_items, function(model_items_index, model_items_value) {
                                    if(typeof(model_items_value) != "undefined") {

                                        if (model_items_value.model_name == model) 
                                        {
                                        
                                            $.each(model_items_value.variant_items, function(variant_items_index, variant_items_value) {  
                                                if(typeof(variant_items_value) != "undefined") {        
                                                    console.log(variant_items_value, "variant delete");
                                                    console.log(variant);  
                                                    if (variant_items_value.variant_name == variant) {
                                                        wish_list_make_model_variant_data_checked.data_checked[index].model_items[model_items_index].variant_items.splice(variant_items_index, 1);
                                                    }
                                                }
                                            });

                                        }

                                    }
                                });
                            }
                        }
                    });

                    //save remove
                    wish_list_make_model_variant_data_checked = JSON.stringify(wish_list_make_model_variant_data_checked);
                    sessionStorage.setItem("wish_list_make_model_variant_data_checked", wish_list_make_model_variant_data_checked);

                    var wish_list_make_model_variant_data_checked2 = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
                    wish_list_make_model_variant_data_checked2 = $.parseJSON(wish_list_make_model_variant_data_checked2);
                    console.log(wish_list_make_model_variant_data_checked2, "variant delete");
                }
            });
            //end bind event click for variant list
        }
    });
    /** end MAKE SECTION **/
});

$.reUpdateMultiselect = function(selector){
    
    //get var from session local storage
    var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
    wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);
    
    console.log(wish_list_make_model_variant_data_checked, "reupdate multiselect");

    if (wish_list_make_model_variant_data_checked != null) {
        var data_checked_for_multiselect = [];
        var data_checked_for_variant_multiselect = [];

        /* loop make */
        $.each(wish_list_make_model_variant_data_checked.data_checked, function(index, value){

            /* loop model */
            $.each(value.model_items, function(model_items_index, model_items_value){
                //for model checked
                data_checked_for_multiselect.push(value.make_name+"_"+model_items_value.model_name);
                
                //for variant checked
                if (model_items_value.variant_items.length >= 1) {
                    
                    /* loop variant */
                    $.each(model_items_value.variant_items, function(variant_items_index, variant_items_value) {
                        
                        data_checked_for_variant_multiselect.push(value.make_name+"_"+model_items_value.model_name+"_"+variant_items_value.variant_name);
                    });

                }
            });

        });
        console.log(data_checked_for_variant_multiselect, "data_checked_for_variant_multiselect");
        console.log(data_checked_for_multiselect, "data_checked_for_multiselect");
        $(selector).multiselect('select', data_checked_for_multiselect);
        $("#wishlist-variant").multiselect('select', data_checked_for_variant_multiselect);
    }

}

var make_of_model_selected = null;
var variant_arr_for_select_box = [];

/* model is checked */
$("#wishlist-model").change(function ()
{
    if(selected_model != false) {
        make_of_model_selected = selected_model[0];
    }

    if (typeof (make_of_model_selected) != "undefined" && selected_model) {
        
        selected_model = selected_model[0]+"_"+selected_model[1];

        /* when user select model it will work here */
        
        // using session storage to store checked model items
        var wish_list_make_model_variant_data_checked = '{"data_checked": [{"make_name": "ABARTH","model_items": [{"model_name": "595","variant_items": []}]}]}';

        if (sessionStorage.getItem("wish_list_make_model_variant_data_checked") != 0) {

            //local session already exist
            wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
            
            wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);
            //var add_more_make = true;
            $.each(wish_list_make_model_variant_data_checked.data_checked, function(index, value) {
                //update model with make already exist
                
                if (value.make_name == make_of_model_selected) {
                    if (value.model_items.length < 1) {
                        value.model_items.push({
                            "model_name": selected_model.split("_")[1],
                            "variant_items": []
                        });
                    } 
                    else {
                        
                        $.each(value.model_items, function(model_items_index, model_items_value){
                            //is model already exist ?
                            if(model_items_value.model_name == selected_model.split("_")[1]) {
                                return false;
                            } else {
                                //add more model into make already exist
                                value.model_items.push({
                                    "model_name": selected_model.split("_")[1],
                                    "variant_items": []
                                });
                                return false;
                            }
                        });
                    }
                    
                }

            });

            //add new make into wish_list_make_model_variant_data_checked
            /*
            if(add_more_make) {
                wish_list_make_model_variant_data_checked.data_checked.push({
                        "make_name": make_of_model_selected,
                        "model_items": [{
                            "model_name": selected_model,
                            "variant_items": []
                        }]
                    });
                console.log("add new make into wish_list_make_model_variant_data_checked");
            }
            */

            wish_list_make_model_variant_data_checked = JSON.stringify(wish_list_make_model_variant_data_checked);
            sessionStorage.setItem("wish_list_make_model_variant_data_checked", wish_list_make_model_variant_data_checked);

        } else {
            //init session
            wish_list_make_model_variant_data_checked = '{"data_checked": [{"make_name": "ABARTH","model_items": [{"model_name": "595","variant_items": []}]}]}';
            //check_model_string_tmp = {"model" : selected_model[1]};
            wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);
            
            wish_list_make_model_variant_data_checked.data_checked[0].make_name = make_of_model_selected;

            wish_list_make_model_variant_data_checked.data_checked[0].model_items[0].model_name = selected_model;

            wish_list_make_model_variant_data_checked = JSON.stringify(wish_list_make_model_variant_data_checked);
            sessionStorage.setItem("wish_list_make_model_variant_data_checked", wish_list_make_model_variant_data_checked);
        }

        console.log($.parseJSON(sessionStorage.getItem("wish_list_make_model_variant_data_checked")), "local storage");
        // use ajax to get variant by make and model
        $.ajax({
            type: 'POST',
            data: {_token: token, make: make_of_model_selected, model: selected_model.split("_")[1]},
            url: "/get-glasses-data/variant",
            success:function(data_variant) {
                var data_variant_arr = data_variant.toString().split(',');
                data_variant_arr.clean("");

                console.log("data_variant", data_variant_arr);

                variant_arr_for_select_box.push({
                    label: make_of_model_selected +" "+ selected_model.split("_")[1],
                    children: [
                    ]
                });

                $.each(data_variant_arr, function(key_data_variant_arr, value_data_variant_arr) {

                    variant_arr_for_select_box[variant_arr_for_select_box.length-1].children.push(
                        { label: value_data_variant_arr, value: make_of_model_selected+"_"+selected_model.split("_")[1]+"_"+value_data_variant_arr }
                    );

                });

                //push data to variant
                $("#wishlist-variant").multiselect('dataprovider', variant_arr_for_select_box);

                //reload checked variant
                $.reUpdateMultiselect("#wishlist-model");

                //bind event click for variant list
                $("#wishlist-variant").next().children().find("input[type='checkbox']").click(function(){
                    /* variant is checked */
                    if($(this).is(":checked")) {
                        
                        var variant_value = $(this).val();

                        var make = variant_value.split("_")[0];
                        var model = variant_value.split("_")[1];
                        var variant = variant_value.split("_")[2];

                        //save variant to datachecked local session storage
                        var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
                        wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);
                        
                        //begin remove make session storage
                        //make
                        $.each(wish_list_make_model_variant_data_checked.data_checked, function(index, value){
                            if (typeof (value) != "undefined") {
                                if (value.make_name.toString() == make) {
                                    //model
                                    $.each(value.model_items, function(model_items_index, model_items_value) {

                                        if(typeof(model_items_value) != "undefined") {
                                            if (model_items_value.model_name == model) 
                                            {
                                                //if no any variant
                                                if (model_items_value.variant_items.length < 1) {
                                                    wish_list_make_model_variant_data_checked.data_checked[index].model_items[model_items_index].variant_items.push({
                                                        "variant_name": variant
                                                    });
                                                } else {
                                                    //variant
                                                    $.each(model_items_value.variant_items, function(variant_items_index, variant_items_value) {

                                                        if (typeof (variant_items_value) != "undefined") 
                                                        {

                                                            if (variant_items_value.variant_name == variant) {
                                                                return false;
                                                            } else {
                                                                //assign variant index to var add_more_variant
                                                                

                                                                //if (parseInt(variant_items_index)+1 == parseInt(variant_items_value.length)) {
                                                                    
                                                                    wish_list_make_model_variant_data_checked.data_checked[index].model_items[model_items_index].variant_items.push({
                                                                        "variant_name": variant
                                                                    });
                                                                    return false;
                                                                //}
                                                            }
                                                        }

                                                    });
                                                }
                                            }
                                        }
                                        if (model_items_value.variant_items.length == -1) {
                                            wish_list_make_model_variant_data_checked.data_checked[index].model_items[model_items_index].variant_items.push({
                                                "variant_name": variant
                                            });
                                        } else {

                                        }
                                    });

                                }
                            }
                        });
                        
                        //save remove
                        wish_list_make_model_variant_data_checked = JSON.stringify(wish_list_make_model_variant_data_checked);
                        sessionStorage.setItem("wish_list_make_model_variant_data_checked", wish_list_make_model_variant_data_checked);

                        var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
                        wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);

                        console.log(wish_list_make_model_variant_data_checked, "variant add");

                    } 
                    /* variant is uncheck */
                    else if (!$(this).is(":checked")) {

                        var variant_value = $(this).val();
                        var make = variant_value.split("_")[0];
                        var model = variant_value.split("_")[1];
                        var variant = variant_value.split("_")[2];
                 
                        //remove variant item in datachecked session local storage
                        //save variant to datachecked local session storage
                        var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
                        wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);
                        
                        //begin remove make session storage
                        $.each(wish_list_make_model_variant_data_checked.data_checked, function(index, value){
                            if (typeof (value) != "undefined") {
                                if (value.make_name.toString() == make) {
                                    
                                    $.each(value.model_items, function(model_items_index, model_items_value) {
                                        if(typeof(model_items_value) != "undefined") {

                                            if (model_items_value.model_name == model) 
                                            {
                                            
                                                $.each(model_items_value.variant_items, function(variant_items_index, variant_items_value) {  
                                                    if(typeof(variant_items_value) != "undefined") {        
                                                        console.log(variant_items_value, "variant delete");
                                                        console.log(variant);  
                                                        if (variant_items_value.variant_name == variant) {
                                                            wish_list_make_model_variant_data_checked.data_checked[index].model_items[model_items_index].variant_items.splice(variant_items_index, 1);
                                                        }
                                                    }
                                                });

                                            }

                                        }
                                    });
                                }
                            }
                        });

                        //save remove
                        wish_list_make_model_variant_data_checked = JSON.stringify(wish_list_make_model_variant_data_checked);
                        sessionStorage.setItem("wish_list_make_model_variant_data_checked", wish_list_make_model_variant_data_checked);

                        var wish_list_make_model_variant_data_checked2 = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
                        wish_list_make_model_variant_data_checked2 = $.parseJSON(wish_list_make_model_variant_data_checked2);
                        console.log(wish_list_make_model_variant_data_checked2, "variant delete");
                    }
                });

            },
            error: function(data){
                console.log('/get-glasses-data/variant error: '+data);
            },
            complete: function(){
                $(".mo-loading-image").addClass("hidden");
            }

        });

    }
});

Array.prototype.clean = function(deleteValue) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == deleteValue) {         
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};

/*** SUBMIT FORM ***/
$("#wishlist-create-btn").click(function(){
    
    /*
    if(!$("#mo-wishlist-form").valid()) {
        return false;
    }
    */

    var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
    console.log($.parseJSON(wish_list_make_model_variant_data_checked), "session submit");

    var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
    var make_info = $.parseJSON(wish_list_make_model_variant_data_checked);

    var make_info_api_format = [];

    if(make_info != 0 && make_info.data_checked.length >= 1) {

        $.each(make_info.data_checked, function(index, value) {
            
            console.log(value, "make info submit");

            if (value.model_items.length < 1) {
                make_info_api_format.push({
                    "make": value.make_name
                });
            } else {
                $.each(value.model_items, function(model_items_index, model_items_value){
                    console.log(model_items_value, "value");

                    if(model_items_value.variant_items.length < 1) {
                        make_info_api_format.push({
                            "make": value.make_name,
                            "model": model_items_value.model_name
                        });
                    } 
                    else 
                    {
                        $.each(model_items_value.variant_items, function(variant_items_index, variant_items_value){
                            make_info_api_format.push({
                                "make": value.make_name,
                                "model": model_items_value.model_name,
                                "variant": variant_items_value.variant_name
                            });
                        });

                    }
                });
            }
        });

    }
   
    console.log(make_info_api_format, "make_info");
    var make_info_field = "<input type='hidden' name='make_info' value='"+JSON.stringify(make_info_api_format)+"'/>";

    $("#mo-wishlist-form").append(make_info_field);
    $("#mo-wishlist-form").submit();
    
});