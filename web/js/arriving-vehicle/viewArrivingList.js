$(document).ready(function(){
    /** COMMON TOKEN **/
    var token = $("input[name='_token']").val();

    /** PRE-DEFINED FUNCTION **/
    function getInspectionItem(this_elem_parent, valuation_id, valuation_version){
        $(document).off(".scroll");
        $(document).on("ajaxStart.getInspectionItem", function () {
            $('.mo-loading-three-dot-wrapper').hide();
            $('.mo-loading-three-dot-icon').hide();
            this_elem_parent.find('.mo-loading-three-dot-wrapper').show();
            this_elem_parent.find('.mo-loading-three-dot-icon').show();
        });
        $.ajax({
            type: 'POST',
            data: {
                _token: token,
                valuation_id: valuation_id,
                valuation_version: valuation_version
            },
            url: "/arriving-vehicle/get-inspection-items",
            success: function (html) {
                this_elem_parent.find("#mo-arriving-vehicle-details").show().fadeIn('slow');
                this_elem_parent.find("#mo-arriving-vehicle-details").empty();
                this_elem_parent.find("#mo-arriving-vehicle-details").append(html);
            },
            error: function (data) {
                console.log('error: ' + data);
            },
            complete: function () {
                $('.mo-loading-three-dot-wrapper').hide();
                $('.mo-loading-three-dot-icon').hide();
            }
        });
    }

    function infinityScroll() {
        var win = $(window);
        win.off('scroll.windowscroll');

        if (typeof(Storage) !== "undefined") {
            sessionStorage.setItem("arriving_vehicle_grid_content_length", 0);
        }

        // Each time the user scrolls
        win.on('scroll.windowscroll', function() {
            // End of the document reached?
            var content = $("#mo-arriving-list");
            if ($(document).height() - win.height() == win.scrollTop()) {
                var content_length = content.children(".mo-arriving-vehicle-info:not(.mo-loading-wrapper)").length;
                /*console.log("content_length "+content_length);
                 console.log(sessionStorage.getItem("dispute_grid_content_length"));*/
                if(content_length > sessionStorage.getItem("arriving_vehicle_grid_content_length" )){
                    sessionStorage.setItem("arriving_vehicle_grid_content_length", content_length);
                    var token = $("input[name='_token']").val();
                    $(document).off(".getInspectionItem");
                    $(document).on("ajaxStart.scroll", function () {
                        $('.mo-loading-wrapper').show();
                        $('.mo-loading-icon').show();
                    });

                    var data = {
                        _token: token,
                        limit: 20,
                        offset: content_length
                    };

                    $.ajax({
                        type: "POST",
                        url: "/arriving-vehicle/get-data-arriving-list",
                        data: data,
                        success: function(html) {
                            if(html == ""){
                                win.off('scroll.windowscroll');
                            } else {
                                content.append(html);
                            }
                        },
                        complete: function(){
                            $(".mo-loading-icon").parent().not(":last").remove();
                        }
                    });

                    $(document).on("ajaxStop.scroll", function () {
                        $('.mo-loading-icon').hide();
                        $('.mo-loading-wrapper').hide();
                    });
                }
            }
        });
    }

    /** TOGGLE MORE INFO - CLOSE INFO **/
    $(document).on("click",".mo-more-info",function() {
        var this_elem_parent = $(this).parents(".mo-arriving-vehicle-info");
        var valuation_id =  $(this).data("valuation-id");
        var valuation_version =  $(this).data("valuation-version");

        $(this).hide();
        this_elem_parent.find(".mo-close-info").show();
        this_elem_parent.find("#mo-arriving-vehicle-details").show().fadeIn('slow');
        getInspectionItem(this_elem_parent, valuation_id, valuation_version);
    });
    
    $(document).on("click",".mo-close-info",function() {
        $(this).hide();
        $(this).parents(".mo-arriving-vehicle-info").find(".mo-more-info").show();
        $(this).parents(".mo-arriving-vehicle-info").find("#mo-arriving-vehicle-details").hide().fadeOut('slow');
    });

    infinityScroll();
});