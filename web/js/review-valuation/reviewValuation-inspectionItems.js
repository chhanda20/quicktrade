/*$(document).ready(function(){*/
    /*** start ADD NEW ITEM ***/
    $(".mo-add-new-item").click(function() {

        popupValidator.resetForm();

        $("#exterior-section").attr("disabled", false);
        $("#exterior-section option:disabled").prop("selected", true);

        $("#exterior-items").attr("disabled", false);
        $("#exterior-items option:disabled").prop("selected", true);
        $("#exterior-items option[value]").remove();

        var image_upload = "/images/perform_valuation/image_upload.png";
        $("#mo-image-box-1").children().children().attr("src", image_upload);
        $("#mo-image-box-2").children().children().attr("src", image_upload);
        $("#mo-image-box-3").children().children().attr("src", image_upload);

        $("#estimate-cost").val(null);
        $("#note").val(null);


        changeToAddStatus();


    });
    /*** end ADD NEW ITEM ***/
    /* COST EDIT INLINE */
    $(".mo-cost-estimate-row .name").click(function(){
        return false;
    });

    $(".mo-cost-estimate-row label").click(function() {
        return false;
    });

    var current_price = 0;
    $(".mo-cost-number").click(function (event) {
        var current_cost = $.trim($(this).text());
        current_price = current_cost.replace(/[^0-9\.]+/g,"");
        $(".mo-cost-number-input input").val(current_cost.replace(/[^0-9\.]+/g,""));
        $(this).next(".mo-cost-number-input").removeClass("hide");
        $(this).addClass("hide");
        $(".mo-cost-number-input input").focus();
        return false;
    });

    $(".mo-cost-number-input").click(function() {
        return false;
    });

    function isFloatNumber(value) {
        var regex = /^[+]?\d+(\.\d+)?$/;
        return regex.test(value);
    };

    $(".mo-cost-number-input input").blur(function() {

        if(!isFloatNumber($(this).val())) {
            $(this).parent().addClass("hide");
            $(this).parent().prev().removeClass("hide");
            $(this).parent().prev().html(accounting.formatMoney(current_price, "", 2, ",", "."));
            return false;
        }

        if ($(this).data("recost") == "recost") {

            var token = $("input[name='_token']").val();
            var item_id = $(this).parent().parent().parent().parent().attr("id");
            var general_cost = $(this).val();
            $.ajax({
                type:'POST',
                url: base_url+"/re-conditioning-cost",
                headers: {'X-CSRF-TOKEN': token},
                data: {"valuation_id" : $("#valuation_id").val(), "inspection_item_id" : item_id, "general_cost" : general_cost
                },

                success:function(data) {
                    console.log(data);
                }

            });

        } else {

            var token = $("input[name='_token']").val();
            var item_id = $(this).parent().parent().parent().parent().attr("id");
            console.log("item_id: " + item_id);
            var repair_cost = $(this).val();

            $.ajax({
                type:'POST',
                url: base_url+"/update-inspection-item",
                headers: {'X-CSRF-TOKEN': token},
                data: {
                    "valuation_id" : $("#valuation_id").val(),
                    "inspection_item_id" : item_id,
                    "repair_cost" : repair_cost
                },
                success:function(data) {
                    console.log(data);
                }

            });

        }

        $(this).parent().addClass("hide");
        $(this).parent().prev().removeClass("hide");

        var updated_price = $(this).val();
        //update price
        $(this).parent().prev().html(accounting.formatMoney(updated_price, "", 2, ",", "."));

        if (window.location.hash == "#exterior") {
            //calculate price
            var section_id = $(this).parent().parent().parent().parent().parent().attr("class");
            var total_price = $("#mo-exterior-total-estimate-cost").text();
            total_price = total_price.replace(/[^0-9\.]+/g,"");
            var section_price = $("#section_cost_"+section_id+ " span").text();
            section_price = section_price.replace(/[^0-9\.]+/g,"");

            //update section price
            var updated_section_price = (parseFloat(section_price) - parseFloat(current_price)) + parseFloat(updated_price.replace(/[^0-9\.]+/g,""));
            $("#section_cost_"+section_id+ " span").text(accounting.formatMoney(updated_section_price, "", 2, ",", "."));

            //update total price
            var updated_total_price = (parseFloat(total_price) - parseFloat(current_price)) + parseFloat(updated_price.replace(/[^0-9\.]+/g,""));
            $("#mo-exterior-total-estimate-cost").text(accounting.formatMoney(updated_total_price, "", 2, ",", "."));
            $("#mo-exterior-cost").text("$"+accounting.formatMoney(updated_total_price, "", 2, ",", "."));//setting price for summary tab


            //update items list
            if (items_data == null) {

                if ($("#mo-item-data").val() != null) {
                    items_data = JSON.parse($("#mo-item-data").val());
                }
            }
        } else if (window.location.hash == "#interior") {
            //calculate price

            var total_price = $("#mo-interior-total-estimate-cost").text();
            total_price = total_price.replace(/[^0-9\.]+/g,"");


            //update total price
            var updated_total_price = (parseFloat(total_price) - parseFloat(current_price)) + parseFloat(updated_price.replace(/[^0-9\.]+/g,""));
            $("#mo-interior-total-estimate-cost").text(accounting.formatMoney(updated_total_price, "", 2, ",", "."));


            //update items list
            if (items_data == null) {

                if ($("#mo-interior-item-data").val() != null) {
                    items_data = JSON.parse($("#mo-interior-item-data").val());
                }
            }
        } else if (window.location.hash == "#mechanical") {
            //calculate price

            var total_price = $("#mo-mechanical-total-estimate-cost").text();
            total_price = total_price.replace(/[^0-9\.]+/g,"");


            //update total price
            var updated_total_price = (parseFloat(total_price) - parseFloat(current_price)) + parseFloat(updated_price.replace(/[^0-9\.]+/g,""));
            $("#mo-mechanical-total-estimate-cost").text(accounting.formatMoney(updated_total_price, "", 2, ",", "."));


            //update items list
            if (items_data == null) {

                if ($("#mo-mechanical-item-data").val() != null) {
                    items_data = JSON.parse($("#mo-mechanical-item-data").val());
                }
            }
        }

        $.each(items_data, function(index, value) {
            $.each(value.items, function(index2, value2) {
                if (item_id == value2.inspection_item_id) {

                    items_data[index].items[index2].repair_cost = updated_price;
                    return false;
                }
            });
        });
        return false;
    });


    /* END COST EDIT INLINE*/
    /* Popup */
    //addFloatZero($("#estimate-cost"));

    var magnificPopup = $.magnificPopup.instance;
    $('.open-popup-link').magnificPopup({
        type:'inline',
        showCloseBtn: true,
        closeOnBgClick: false,
        callbacks: {
            beforeClose: function(){
                popupValidator.resetForm();
                changeButtonLabel();
            }
        }
    });

    /*** CHANGE BUTTON LABEL ***/
    function changeButtonLabel() {
        $("#mo-edit-inspection-item-popup .mo-popup-heading span").html("Edit");

        $("#exterior-edit-button").val("Update");
        $("#exterior-edit-button").data("status", "update");

        $("#exterior-section").attr("disabled", true);
        $("#exterior-items").attr("disabled", true);

    }
    /*** END CHANGE BUTTON LABEL***/

    /*** CLOSE EXTERIOR POPUP***/
    $("#mo-edit-inspection-item-popup .mfp-close").click(function(){
        popupValidator.resetForm();
        changeButtonLabel();
    });
    /*** END CLOSE EXTERIOR POPUP***/

    $('.mo-cancel-btn').click(function (){
        popupValidator.resetForm();
        magnificPopup.close();
        changeButtonLabel();
    });

    var current_element = null;
    $("#fileToUpload").on("change", function() {
        if($("#fileToUpload").val())
        {
            $("#mo-exterior-popup-form").submit();
        }
        $("#fileToUpload").val("");
    });

    $("#exterior-edit-button").click(function() {

        if(!$('#mo-exterior-popup-form').valid()) {
            return false;
        }

        if ($("#exterior-edit-button").data("status") == "update")
        {
            // if($("#estimate-cost").val() == "") {
            //     return false;
            // }

            var token = $("input[name='_token']").val();
            var estimated_cost = 0;
            if($("#estimate-cost").val() != null && $("#estimate-cost").val() != ''){
                estimated_cost = $("#estimate-cost").val();
            }

            $.ajax({
                type:'POST',
                url: base_url+"/update-inspection-item",
                headers: {'X-CSRF-TOKEN': token},
                data: {
                    "valuation_id" : $("#valuation_id").val(),
                    "inspection_item_id" : $("#exterior-items").val(),
                    "repair_cost" : estimated_cost,
                    "note" : $("#note").val()
                },

                success:function(data) {
                    var section_id = $("#"+$("#exterior-items").val()).parent().attr("class");
                    if (window.location.hash == "#mechanical") {
                        var estimated_cost = 0;
                        if($("#estimate-cost").val() != null && $("#estimate-cost").val() != ''){
                            estimated_cost = $("#estimate-cost").val();
                        }

                        var new_price = estimated_cost;
                        new_price = new_price.toString().replace(/[^0-9\.]+/g,"");

                        var old_price = $("#item_cost_"+section_id+"_"+inspection_item_id+" span").text();
                        old_price = old_price.replace(/[^0-9\.]+/g,"");

                        var total_price = $("#mo-mechanical-total-estimate-cost").text();
                        total_price = total_price.replace(/[^0-9\.]+/g,"");
                        total_price = (parseFloat(total_price) - parseFloat(old_price)) + parseFloat(new_price);

                        total_price = accounting.formatMoney(total_price, "", 2, ",", ".");
                        $("#mo-mechanical-total-estimate-cost").text(total_price);
                        $("#mo-mechanical-cost").text(total_price);//set price for mechanical in SUMMARY tab

                        var update_price =  accounting.formatMoney(new_price, "", 2, ",", ".");

                        var new_price_2 = new_price;
                        new_price_2 = accounting.formatMoney(new_price_2, "", 2, "", ".");
                        new_price = accounting.formatMoney(new_price, "", 2, ",", ".");

                        $("#item_cost_"+section_id+"_"+$("#exterior-items").val()+" .mo-cost-number").text(new_price);
                        $("#item_cost_"+section_id+"_"+$("#exterior-items").val()+" .mo-cost-number-input input").val(new_price);

                        //$("#item_cost_"+section_id+"_"+inspection_item_id+" span").text(update_price);
                        //update note
                        $("#"+$("#exterior-items").val()).children().find(".note").text($("#note").val());



                    } else if(window.location.hash == "#interior"){
                        var estimated_cost = 0;
                        if($("#estimate-cost").val() != null && $("#estimate-cost").val() != ''){
                            estimated_cost = $("#estimate-cost").val();
                        }

                        var new_price = estimated_cost;
                        new_price = new_price.toString().replace(/[^0-9\.]+/g,"");

                        var old_price = $("#item_cost_"+section_id+"_"+inspection_item_id+" span").text();
                        old_price = old_price.replace(/[^0-9\.]+/g,"");

                        var total_price = $("#mo-interior-total-estimate-cost").text();
                        total_price = total_price.replace(/[^0-9\.]+/g,"");
                        total_price = (parseFloat(total_price) - parseFloat(old_price)) + parseFloat(new_price);

                        total_price = accounting.formatMoney(total_price, "", 2, ",", ".");

                        $("#mo-interior-total-estimate-cost").text(total_price);
                        $("#mo-interior-cost").text(total_price);//set price for interior in SUMMARY tab
                        var update_price =  accounting.formatMoney(new_price, "", 2, ",", ".");

                        var new_price_2 = new_price;
                        new_price_2 = accounting.formatMoney(new_price_2, "", 2, "", ".");
                        new_price = accounting.formatMoney(new_price, "", 2, ",", ".");

                        $("#item_cost_"+section_id+"_"+$("#exterior-items").val()+" .mo-cost-number").text(new_price);
                        $("#item_cost_"+section_id+"_"+$("#exterior-items").val()+" .mo-cost-number-input input").val(new_price);

                        //$("#item_cost_"+section_id+"_"+inspection_item_id+" span").text(update_price);

                        //update note
                        $("#"+$("#exterior-items").val()).children().find(".note").text($("#note").val());


                    } else if (window.location.hash == "#exterior") {
                        var estimated_cost = 0;
                        if($("#estimate-cost").val() != null && $("#estimate-cost").val() != ''){
                            estimated_cost = $("#estimate-cost").val();
                        }

                        var new_price = estimated_cost;
                        new_price = new_price.toString().replace(/[^0-9\.]+/g,"");

                        var old_price = $("#item_cost_"+section_id+"_"+$("#exterior-items").val()+" span").text();
                        old_price = old_price.replace(/[^0-9\.]+/g,"");



                        var section_price = $("#section_cost_"+section_id+ " span").text();
                        section_price = section_price.replace(/[^0-9\.]+/g,"");

                        var total_price = $("#mo-exterior-total-estimate-cost").text();
                        total_price = total_price.replace(/[^0-9\.]+/g,"");
                        total_price = (parseFloat(total_price) - parseFloat(old_price)) + parseFloat(new_price);

                        total_price = accounting.formatMoney(total_price, "", 2, ",", ".");

                        $("#mo-exterior-total-estimate-cost").text(total_price);
                        $("#mo-exterior-cost").text("$"+total_price);//set price for exterior in SUMMARY tab
                        var update_price = (parseFloat(section_price) - parseFloat(old_price)) + parseFloat(new_price);
                        update_price = accounting.formatMoney(update_price, "", 2, ",", "."); // 4,999.99
                        $("#section_cost_"+section_id+ " span").text(update_price);
                        var new_price_2 = new_price;
                        new_price_2 = accounting.formatMoney(new_price_2, "", 2, "", ".");
                        new_price = accounting.formatMoney(new_price, "", 2, ",", ".");

                        $("#item_cost_"+section_id+"_"+$("#exterior-items").val()+" .mo-cost-number").text(new_price);
                        $("#item_cost_"+section_id+"_"+$("#exterior-items").val()+" .mo-cost-number-input input").val(new_price);

                        //update note
                        $("#"+$("#exterior-items").val()).children().find(".note").text($("#note").val());
                    }

                    $.each(items_data, function(index, value) {
                        $.each(value.items, function(index2, value2) {
                            if ($("#exterior-items").val() == value2.inspection_item_id) {
                                items_data[index].items[index2].repair_cost = new_price_2;
                                items_data[index].items[index2].note = $("#note").val();
                                return false;
                            }
                        });
                    });

                    magnificPopup.close();
                    changeButtonLabel();
                },
                error: function(data){
                    console.log("error");
                    console.log(data);
                }

            });

        } else {

            var token = $("input[name='_token']").val();
            var estimated_cost = 0;
            if($("#estimate-cost").val() != null && $("#estimate-cost").val() != ''){
                estimated_cost = $("#estimate-cost").val();
            }

            $.ajax({
                type:'POST',
                url: base_url+"/add-inspection-item",
                headers: {'X-CSRF-TOKEN': token},
                data: {"valuation_id" : $("#valuation_id").val(), "inspection_item_id" : $("#exterior-items").val(), "repair_cost" : estimated_cost,
                    "note" : $("#note").val()},

                success:function(data) {
                    console.log(data);
                    data = JSON.parse(data);
                    if (data.success) {
                        magnificPopup.close();
                        location.reload();
                    }
                }
            });

        }

    });

    $("#mo-vehicle-image-upload").on("change", (function(e){
        if ($("#mo-vehicle-image-upload")) {
            $("#mo-vehicle-image-upload").submit();
        }
        $("#mo-vehicle-image-upload").val(null);
    }));

    $("#mo-vehicle-image-upload-form").on("submit", (function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        var vehicle_image = $(this);
        vehicle_image.children().children().children().attr("src", "/images/perform_valuation/img-loading.gif");

        $.ajax({
            type:'POST',
            url: base_url+"/upload-image-vehicle",
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data) {
                vehicle_image.children().children().children().attr("src", data+"?"+Math.random());
            },
            error: function(data){
                console.log("error");
            }

        });
    }));


    $("#mo-exterior-popup-form").on("submit",(function(e) {
        e.preventDefault();
        var formData = new FormData(this);

        current_element.children().attr("src", "/images/perform_valuation/img-loading.gif");

        $.ajax({
            type:'POST',
            url: base_url+"/upload-image",
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data) {
                //console.log(data);
                var index = data.substring(data.lastIndexOf(".")-1, data.lastIndexOf("."));
                $("#mo-image-box-"+index).children().children().attr("src", data+"?"+Math.random());
            },
            error: function(data){
                console.log("error");
                //console.log(data);
            }
        });

    }));

    /* START ERROR MESSAGE FOR ADD INSPECTION ITEM */
    var popupValidator = $("#mo-exterior-popup-form").validate({
        rules:{
            "exterior-section": "required",
            "exterior-items": "required",
            "estimate_cost": {
                required: true,
                number: true
            }
            //"fileToUpload" : "required"
        },
        messages: {
            "exterior-section": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
            "exterior-items": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",

            "estimate_cost": {
                required: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                number: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter a valid number.</span>"+"</div>"
            }
            //"fileToUpload" : {
            //   required: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please add at least 1 photo.</span>"+"</div>"
            //}
        }

    });
    // $("#estimate-cost").keyup(function(){
    //     var $this = $(this);
    //     $this.val($this.val().replace(/[^\d]/g, ''));

    // });

    /* END ERROR MESSAGE */
    $("label[for='fileToUpload']").on("click", function () {
        current_element = $(this);
        var sequence_number = null;
        if (current_element.parent().attr("id") == "mo-image-box-1") {
            sequence_number = 1;
        } else if (current_element.parent().attr("id") == "mo-image-box-2") {
            sequence_number = 2;
        } else if (current_element.parent().attr("id") == "mo-image-box-3") {
            sequence_number = 3;
        }
        $("input[name='sequence']").val(sequence_number);
    });

    var inspection_item_id = null;
    var cost = null;

    var items_data = null;

    function clearPopUpContent(){
        var image_upload = "/images/perform_valuation/image_upload.png";
        $("#mo-image-box-1").children().children().attr("src", image_upload);
        $("#mo-image-box-2").children().children().attr("src", image_upload);
        $("#mo-image-box-3").children().children().attr("src", image_upload);

        $("#exterior-items option:disabled").prop("selected", true);
        $("#estimate-cost").val(null);
        $("#note").val(null);
        $("#fileToUpload").val(null);
    }

    $(".mo-section-number").on("click", function() {

        if ($(this).hasClass("highlight") == false){
            changeToAddStatus();
        }
        /*** CLEAR POPUP CONTENT ***/
        clearPopUpContent();
        /*** END CLEAR POPUP CONTENT***/

        var valuation_id = $("#valuation_id").val();

        var section_id = $(this).val();
        $("#exterior-section").val(section_id);
        $("#exterior-items").attr("disabled", null);

        console.log(exterior_data);
        if (items_data == null) {

            if ($("#mo-item-data").val() != null) {
                items_data = JSON.parse($("#mo-item-data").val());
            }
        }

        $.each(exterior_data, function (index, value) {
            if (value.section_id == section_id) {

                $("#exterior-items option[value]").remove();

                $.each(value.items, function(index2, value2) {

                    $("#exterior-items").append($("<option>", {
                        value: value2.id,
                        text: value2.item
                    }));

                });

                //get images of exterior item
                if ($("#exterior-items").val() != null)
                {
                    $("#estimate-cost").val(value.items[0].repair_cost);
                    $("#note").val(value.items[0].note);
                    var image_upload = "/images/perform_valuation/image_upload.png";
                    var azure_account_name = $("input[name='azure-accout-name']").val();
                    var valuation_current_version = $("input[name='valuation_version']").val();
                    var image_1 = "https://"+azure_account_name+".blob.core.windows.net/inspection-item/"+valuation_id+"_"+valuation_current_version+"_"+$("#exterior-items").val()+"_1.jpg?"+Math.random();
                    var image_2 = "https://"+azure_account_name+".blob.core.windows.net/inspection-item/"+valuation_id+"_"+valuation_current_version+"_"+$("#exterior-items").val()+"_2.jpg?"+Math.random();
                    var image_3 = "https://"+azure_account_name+".blob.core.windows.net/inspection-item/"+valuation_id+"_"+valuation_current_version+"_"+$("#exterior-items").val()+"_3.jpg?"+Math.random();

                    $("#mo-image-box-1").children().children().error(function() {
                        $("#mo-image-box-1").children().children().attr("src", image_upload);
                    }).attr("src", image_1);

                    try {
                        $("#mo-image-box-2").children().children().error(function() {
                            $("#mo-image-box-2").children().children().attr("src", image_upload);
                        }).attr("src", image_2);
                    } catch (e){
                        return null;
                    }



                    $("#mo-image-box-3").children().children().error(function() {
                        $("#mo-image-box-3").children().children().attr("src", image_upload);
                    }).attr("src", image_3);
                }

                return false;
            }
        });

    });

    $(".mo-cost-estimate-row").on("click", function (e) {

        // if (e.offsetY <= 7 || e.offsetY >= 16) {
        //     return false;
        // }

        $("#exterior-items").attr("disabled", true);
        var valuation_id = $("#valuation_id").val();
        inspection_item_id = $(this).attr("id");

        $("input[name='exterior-item-id']").val(inspection_item_id);
        //set images to images box
        var image_upload = "/images/perform_valuation/image_upload.png";
        var azure_account_name = $("input[name='azure-accout-name']").val();
        var valuation_current_version = $("input[name='valuation_version']").val();
        var image_1 = "https://"+azure_account_name+".blob.core.windows.net/inspection-item/"+valuation_id+"_"+valuation_current_version+"_"+inspection_item_id+"_1.jpg?"+Math.random();
        var image_2 = "https://"+azure_account_name+".blob.core.windows.net/inspection-item/"+valuation_id+"_"+valuation_current_version+"_"+inspection_item_id+"_2.jpg?"+Math.random();
        var image_3 = "https://"+azure_account_name+".blob.core.windows.net/inspection-item/"+valuation_id+"_"+valuation_current_version+"_"+inspection_item_id+"_3.jpg?"+Math.random();

        $("#mo-image-box-1").children().children().error(function() {
            $("#mo-image-box-1").children().children().attr("src", image_upload);
        }).attr("src", image_1);

        $("#mo-image-box-2").children().children().error(function() {
            $("#mo-image-box-2").children().children().attr("src", image_upload);
        }).attr("src", image_2);

        $("#mo-image-box-3").children().children().error(function() {
            $("#mo-image-box-3").children().children().attr("src", image_upload);
        }).attr("src", image_3);

        var item_detail = null;

        $.ajax({
            type: "GET",
            url: base_url+"/_get-inspection-item-detail/"+valuation_id+"/"+inspection_item_id,
            data: null,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {

                if(data != null) {
                    item_detail = JSON.parse(data);
                }

                //console.log(item_detail);
                cost = item_detail["repair_cost"];
                var note = item_detail["note"];


                $("#estimate-cost").val(cost);
                $("#note").val(note);

                $.each(item_detail["itemImage"], function(index, value) {
                    //console.log(value);
                });

                $("#exterior-items option[value]").remove();
                $.each(exterior_data, function(index, value) {
                    //console.log(value);
                    $.each(value["items"], function(index2, value2) {

                        if (value2["id"] == inspection_item_id) {
                            //console.log(value["items"]);

                            $("#exterior-section").val(value2["section_type"]);

                            $.each(value["items"], function(index3, value3) {

                                $("#exterior-items").append($("<option>", {
                                    value: value3["id"],
                                    text: value3["item"]
                                }));

                            });

                            $("#exterior-items").val(inspection_item_id);

                            return false;
                        }
                    });
                });

            },
            error: function (data) {

            }
        });
    });

    $("#mo-detele-item-record").click(function() {

        var valuation_id = $("#valuation_id").val();

        // var item_price = $("#"+item_id).children().children().children("span").text();
        // var total_price = $("#mo-exterior-total-estimate-cost").text();
        // total_price = total_price.replace(/[^0-9\.]+/g,"");
        // total_price = parseFloat(total_price) - parseFloat(item_price);
        // total_price = accounting.formatMoney(total_price, "", 2, ",", ".");
        // $("#mo-exterior-total-estimate-cost").text(total_price);

        $.ajax({
            type:'POST',
            url: base_url+"/delete-inspection-item",
            headers: {'X-CSRF-TOKEN': $("input[name=_token]").val()},
            data: {"valuation_id" : valuation_id, "inspection_item_id" : item_id},

            success:function(data) {
                //item_row.remove();
                window.location.reload(false);
            },
            error: function(data){

            }

        });
    });

    var item_id = null;
    $(".mo-delete-item").click(function(){
        item_id = $(this).parent().parent().parent().parent().attr("id");
    });

    var exterior_data = null;
    function init_items(group_section) {
        $(function() {
            $.ajax({
                type: "GET",
                url: base_url+"/get-inspection-items-list/"+group_section,
                data: null,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {

                    $("#exterior-section option[value]").remove();
                    exterior_data = JSON.parse(data);
                    //console.log(exterior_data);
                    $.each(exterior_data, function (index, value) {
                        //console.log(index + ':' + value["section_name"]);
                        $("#exterior-section").append($("<option>", {
                            value: value["section_id"],
                            text: value["section_name"]
                        }));
                    });
                },
                error: function (data) {

                }
            });

        });
    }

    //init_items("EXTERIOR_SECTION");

    $("#exterior-section").on("change", function(){
        var section_id = $("#exterior-section").val();
        $.each(exterior_data, function (index, value) {

            if (value["section_id"] == section_id) {
                $("#exterior-items option[value]").remove();
                $.each(value["items"], function (index2, value2) {
                    $("#exterior-items").append($("<option>", {
                        value: value2["id"],
                        text: value2["item"]
                    }));
                });
                return false;
            }

        });

    });

    $("#exterior-items").on("change", function() {

        if (window.location.hash == "#exterior")
        {
            if (items_data == null) {
                if ($("#mo-item-data").val() != null) {
                    items_data = JSON.parse($("#mo-item-data").val());
                } else {
                    items_data = null;
                }
            }
        } else if (window.location.hash == "#interior") {
            if (items_data == null) {
                if ($("#mo-interior-item-data").val() != null) {
                    items_data = JSON.parse($("#mo-interior-item-data").val());
                } else {
                    items_data = null;
                }
            }

        } else if (window.location.hash == "#mechanical") {
            if (items_data == null) {
                if ($("#mo-mechanical-item-data").val() != null) {
                    items_data = JSON.parse($("#mo-mechanical-item-data").val());
                } else {
                    items_data = null;
                }
            }

        }

        var item_id = $("#exterior-items").val();
        $("input[name='exterior-item-id']").val(item_id);
        //set images to images box
        var image_upload = "/images/perform_valuation/image_upload.png";
        var azure_account_name = $("input[name='azure-accout-name']").val();
        var valuation_current_version = $("input[name='valuation_version']").val();
        var image_1 = "https://"+azure_account_name+".blob.core.windows.net/inspection-item/"+$("#valuation_id").val()+"_"+valuation_current_version+"_"+item_id+"_1.jpg?"+Math.random();
        var image_2 = "https://"+azure_account_name+".blob.core.windows.net/inspection-item/"+$("#valuation_id").val()+"_"+valuation_current_version+"_"+item_id+"_2.jpg?"+Math.random();
        var image_3 = "https://"+azure_account_name+".blob.core.windows.net/inspection-item/"+$("#valuation_id").val()+"_"+valuation_current_version+"_"+item_id+"_3.jpg?"+Math.random();


        $("#mo-image-box-1").children().children().error(function() {
            $("#mo-image-box-1").children().children().attr("src", image_upload);
        }).attr("src", image_1);

        $("#mo-image-box-2").children().children().error(function() {
            $("#mo-image-box-2").children().children().attr("src", image_upload);
        }).attr("src", image_2);

        $("#mo-image-box-3").children().children().error(function() {
            $("#mo-image-box-3").children().children().attr("src", image_upload);
        }).attr("src", image_3);

        var add_item_flag = true;
        $.each(items_data, function(index, value) {

            $.each(value.items, function(index2, value2) {
                if (value2.inspection_item_id == item_id) {
                    $("#estimate-cost").val(value2.repair_cost);
                    $("#note").val(value2.note);

                    add_item_flag = false;

                    changeToEditStatus();

                    return false;
                }

            })

        });

        if (add_item_flag) {
            $("#estimate-cost").val(null);
            $("#note").val(null);
            changeToAddStatus();
        }
    });

    function changeToEditStatus(){
        $("#mo-edit-inspection-item-popup .mo-popup-heading span").html("Edit");
        $("#exterior-edit-button").val("Update");
        $("#exterior-edit-button").data("status", "update");
    }

    function changeToAddStatus(){
        $("#mo-edit-inspection-item-popup .mo-popup-heading span").html("Add");
        $("#exterior-edit-button").val("Add");
        $("#exterior-edit-button").data("status", "add");
    }

    /*** MECHANICAL ***/
    var tab = null;
    $("#mechanical_tab").click(function() {
        tab = "mechanical_tab";
        init_items("MECHANICAL_SECTION");
        items_data = null;
    });
    /*** END MACHANICAL ***/

    /*** INTERIOR ***/
    $("#interior_tab").click(function() {
        tab = "interior/electronics_tab";
        init_items("INTERIOR_SECTION");
        items_data = null;
    });
    /*** END INTERIOR ***/

    /*** EXTERIOR ***/
    $("#exterior_tab").click(function() {
        tab = "interior/electronics_tab";
        init_items("EXTERIOR_SECTION");
        items_data = null;
    });
    /*** END EXTERIOR***/

    /** BUTTERFLY DIAGRAM **/
    var butterflyDiagramWidth = $("#mo-vehicle-diagram").attr('width');
    $(".mo-butterfly-diagram").attr('max-width',butterflyDiagramWidth);
    //console.log('width: '+butterflyDiagramWidth);

    $("div.grid-item div:nth-last-child(1n)").hover(function(){
        $(this).css({
            "background": "rgba(255, 170, 0, .4)"
        });
    }, function(){
        $(this).css({
            "background": "rgba(255, 255, 255, .1)"
        });
    });

    $("div.grid-item div:nth-last-child(1n)").parent().hover(function(){
        $(this).css({
            "background": "rgba(255, 255, 255, .1)"
        });
    });
/*
});*/
