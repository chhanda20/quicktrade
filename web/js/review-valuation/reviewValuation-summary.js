/*$(document).ready(function(){*/
    /** Add float type for number field **/
    function addFloatZero(number_field_id){
        number_field_id.blur(function() {
            this.value = parseFloat(this.value.replace(/,/g, ""))
                .toFixed(2)
                .toString()
                .replace(/\B(?=(\d{10})+(?!\d))/g, ",");
        });
    }

    /** ALL DETAIL EVENT **/
    $("#all-detail").click(function(){

        if($("#all-detail").is(":checked")){
            $("input[name='submit_button']").attr("disabled", false);
            $("input[name='submit_button']").css({"background-color" : "#ffaa00"});
        } else {
            $("input[name='submit_button']").attr("disabled", true);
            $("input[name='submit_button']").css({"background-color" : "#dadada"});
        }
    });
    /** END ALL DETAIL EVENT **/

    /** VALIDATE SUMMARY FORM **/
    $("#create-valuations-summary-form").validate({

        rules:{
            "overall_rating": "required",
            "vehicle": "required",
            "valuation_price": {
                required: true,
                number: true
            },
            "maximum_valuation_price": {
                required: true,
                number: true
            },
            all_detail: "required"
        },

        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        // errorPlacement: function(error, element) {
        //     if(element.parent('.input-group').length) {
        //         error.insertAfter(element.parent());
        //     } else {
        //         error.insertAfter(element);
        //     }
        // },
        errorPlacement: function(error, element) {
            if (element.is(":checkbox")) {
                error.appendTo("#errorToShow");
            } else {
                error.insertAfter(element);   // default placement
            }
        },
        messages: {
            "overall_rating": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
            "vehicle": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",

            "valuation_price": {
                required: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                number: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter a valid number.</span>"+"</div>"
            },
            "maximum_valuation_price": {
                required: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                number: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter a valid number.</span>"+"</div>"
            },
            all_detail: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>"
        },

        submitHandler:function(form){
            var valuation_price = $("#valuation-price").val();
            var maximum_valuation_price = $("#maximum-valuation-price").val();
            if(parseFloat(valuation_price) > parseFloat(maximum_valuation_price)) {
                $("#valuation-price-error").remove();
                var error_message = '<label id="valuation-price-error" class="error" for="valuation-price" style="display: inline-block;"><div class="mo-error-wrapper"><img src="/images/svg/main/ic-warning.svg"><span>Valuation Price must be less than Maximum Valuation Price</span></div></label>';
                $("#valuation-price").parent().append(error_message);
                return false;
            } else {
                $(".mo-loading-image").removeClass("hidden");
                form.submit();
            }
        }
    });
    /** END VALIDATE SUMMARY FORM **/


    /** validate SUMMARY **/
    /*$('#create-valuations-summary-form').validate({
        rules: {
            vehicle: "required",
            valuation_price: "required",
            maximum_valuation_price: "required"

        },
        messages: {
            vehicle:"<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter vehicle</span>"+"</div>",
            valuation_price: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter valuation price</span>"+"</div>",
            maximum_valuation_price: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter maximum valuation price</span>"+"</div>"
        },

        submitHandler:function(form){
            var valuation_price = $("#valuation-price").val();
            var maximum_valuation_price = $("#maximum-valuation-price").val();
            if(parseFloat(valuation_price) > parseFloat(maximum_valuation_price)) {
                $("#valuation-price-error").remove();
                var error_message = '<label id="valuation-price-error" class="error" for="valuation-price" style="display: inline-block;"><div class="mo-error-wrapper"><img src="/images/svg/main/ic-warning.svg"><span>Valuation Price must be less than Maximum Valuation Price</span></div></label>';
                $("#valuation-price").parent().append(error_message);
                return false;
            } else {
                form.submit();
            }

        }
    });*/

    $("#ppsr-check").val(null);

    $("#mo-receive-vehicle").hide();
    $("#ppsr-check").prop("checked", false);
    $("#ppsr-check").click(function() {

        if ($("#ppsr-check:checked").length == 1) {
            $("#mo-receive-vehicle").show();
        } else {
            $("#mo-receive-vehicle").hide();
        }

    })

    $("#mo-trade-in-vehicle-form").validate({
        rules: {
            "stock-number": "required",
            "receive-vehicle-location": "required"
        },
        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        },
        messages: {
            "stock-number": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
            "receive-vehicle-location": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>"
        }
    });

    $("#mo-receive-vehicle").click(function(){
        if ($("#ppsr-check:checked").length == 1) {
            var stock_number = $("#stock-number").val();
            $("input[name='stock-number']").val(stock_number);
                var location_code = $("#receive-vehicle-location").val();
            var location_field = "<input type='hidden' name='location_id' value='"+location_code+"' />";
            if($("#mo-trade-in-vehicle-form").valid()) {
                $("#receive-vehicle-form").append(location_field);
                $("#receive-vehicle-form").submit();
            }

            return false;
        }
        return false;
    });

    /** validate ALL DETAILS checkbox **/
    /*$("#create-valuations-summary-form").submit(function(){
     if(!($("#all-detail").is(":checked"))){
     $("#messages").removeClass('hidden').fadeIn('slow');
     return false;
     }
     else{
     $("#messages").hide();
     }
     });*/

    /** toggle EDIT OVERALL COST **/
    /*$(".mo-icon-edit").click(function(e) {
        e.preventDefault();
        $(".mo-overall-cost-input").toggleClass('hidden').fadeIn('slow');
        $(".mo-overall-cost-span").toggleClass('hidden').fadeIn('slow');
        $(".mo-overall-cost-span").text($(".mo-overall-cost-input").val());
        $(".mo-icon-edit").toggleClass('hidden').fadeIn('slow');
    });

    $(".mo-overall-cost-input").blur(function(e) {
        e.preventDefault();
        $(".mo-overall-cost-input").toggleClass('hidden').fadeIn('slow');
        $(".mo-overall-cost-span").toggleClass('hidden').fadeIn('slow');
        $(".mo-overall-cost-span").text($(".mo-overall-cost-input").val());
        $(".mo-icon-edit").toggleClass('hidden').fadeIn('slow');
    });*/

    $(".mo-icon-edit").click(function(e){
        e.preventDefault();
        $(".mo-overall-cost-input").toggleClass("hidden").fadeIn("slow");
        $(".mo-overall-cost-span").toggleClass("hidden").fadeIn("slow");
        $(".estimated-recon-cost").val($(".mo-overall-cost-input").val());
        $(".mo-overall-cost-span").text($(".estimated-recon-cost").val());
        $(".mo-icon-edit").toggleClass("hidden").fadeIn("slow");
    });

    $(".mo-overall-cost-input").blur(function(e){
        e.preventDefault();
        $(".mo-overall-cost-input").toggleClass('hidden').fadeIn("slow");
        $(".mo-overall-cost-span").toggleClass("hidden").fadeIn("slow");
        $(".estimated-recon-cost").val($(".mo-overall-cost-input").val());
        $(".mo-overall-cost-span").text($(".estimated-recon-cost").val());
        $(".mo-icon-edit").toggleClass("hidden").fadeIn("slow");
    });

    /*** REVIEW VALUATION - SUMMARY ***/
    addFloatZero($("#valuation-price"));
    addFloatZero($("#maximum-valuation-price"));

    /*** POPUP EXTERNAL OFFERING ***/
    var magnificPopup = $.magnificPopup.instance;
    $('#mo-external-offer-open-popup').magnificPopup({
        type:'inline',
        showCloseBtn: false,
        closeOnBgClick: false
    });
    /*** END POPUP EXTERNAL OFFERING ***/

    /*** POPUP CHECK PPSR ***/
    var magnificPopup = $.magnificPopup.instance;
    $('#mo-ppsr-check-open-popup').magnificPopup({
        type:'inline',
        showCloseBtn: false,
        closeOnBgClick: false
    });

    $("#ppsr-yes").click(function() {

        var vin = $(this).data("vin");
        var year = $(this).data("year");
        var token = $("input[name='_token']").val();

        $(this).addClass("hide");
        $(".mo-btn-loading").parent().removeClass("hide");
        $.ajax({
            type: "POST",
            url: "/check-ppsr",

            data: {
                "_token" : token,
                "vin" : vin,
                "year" : year
            },
            success: function(data){

                data = jQuery.parseJSON(data);
                console.log(data);

                if(data.success) {
                    $(".mo-btn-loading").parent().addClass("hide");
                    $("#ppsr-yes").removeClass("hide");

                    magnificPopup.close();

                }
            }
        });
    });
    /*** END POPUP CHECK PPSR ***/
/*
});*/
