/*$(document).ready(function(){*/
    /** SUBMIT FORM WHEN CLICK SEND OFFER REQUEST **/
    $("#mo-external-offer-send-btn").click(function(e){
        if($("#overall-rating").val()){
            $.magnificPopup.open({
                items: {
                    src: "#mo-external-offering-popup"
                },
                type: "inline",
                showCloseBtn: true,
                closeOnBgClick: true
            });
            $("#errorShowMessage").empty();
            var valuation_status = $("input[name='valuation_status']").val();
            console.log(valuation_status);
            var valuation_id = $("input[name='valuation_id']").val();
            console.log(valuation_id);
            $("#external-offering-send").click(function(){
                if(valuation_status == 2){
                    if($("#mo-external-offering-form").valid()){
                        $(document).ajaxStart(function(){
                            $(".mo-loading-image").removeClass("hidden");
                        });
                        $.ajax({
                            type: "POST",
                            _token: token,
                            url: "/request-external-offer",
                            data: $("#mo-external-offering-form").serialize(),
                            success: function(data){
                                data = $.parseJSON(data);
                                console.log(data);
                                if(data.success){
                                    $("input#mo-external-offer-send-btn").prop("disabled", true);
                                    $("input#mo-external-offer-send-btn").val("awaiting offer...");
                                    $(".mo-external-offer-remaining-clock").removeClass("hidden");
                                    $(".time-remaining").removeClass("hidden");
                                    $.calcRemainingTime();
                                }
                            },
                            error: function(){
                                console.log("error");
                            },
                            complete: function(){
                                console.log("continue");
                                $.ajax({
                                    type: "POST",
                                    _token: token,
                                    url: "/completeValuation/" + valuation_id,
                                    data: $("#create-valuations-summary-form").serialize(),
                                    success: function(res){
                                        if(res == 1){
                                            window.location.replace("/valuations/pending");
                                        }
                                    },
                                    error: function(){
                                        console.log("error");
                                    }
                                });
                            }
                        });
                    }
                    return false;
                }
                else{
                    $.requestExternalOffer();
                }
            });

        }
        else{
            $("#errorShowMessage").html("<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>");
        }
    });

    $.requestExternalOffer = function(){
        $(document).ajaxStart(function() {
            $(".mo-loading-image").removeClass("hidden");
        });
        $.ajax({
            type:'POST',
            url: '/request-external-offer',
            _token: token,
            data: $("#mo-external-offering-form").serialize(),
            success:function(data) {
                data = $.parseJSON(data);
                if (data.success) {
                    $("input#mo-external-offer-send-btn").prop("disabled", true);
                    $("input#mo-external-offer-send-btn").val("awaiting offer...");
                    $(".mo-external-offer-remaining-clock").removeClass("hidden");
                    $(".time-remaining").removeClass("hidden");
                    $.calcRemainingTime();
                }
            },
            error: function(data) {
                console.log("error");
            },
            complete: function() {
                $('.mo-loading-image').addClass("hidden");
                magnificPopup.close();
                location.reload();
            },
            global: true
        });
    }


    /** ACCEPT EXTERNAL OFFER **/
    $("#mo-external-offer-accept-btn").click(function(){
        var external_offer_id = $("input[name='external_offer_id']").val();
        $.ajax({
            type:'POST',
            url: '/external-offer/accept',
            data: {"_token": token, "external_offer_id": external_offer_id},
            success:function(data) {
                if (data != null)
                {
                    data = $.parseJSON(data);
                    console.log(data);
                    if (data.success) {
                        window.location.reload();
                    }
                }


            },
            error: function(data) {
                console.log("/external-offer/accept error");
            },
            complete: function() {

            },
            global: true
        });
    });

    $("#mo-external-offer-reject-btn").click(function(){
        var external_offer_id = $("input[name='external_offer_id']").val();
        $.ajax({
            type:'POST',
            url: '/external-offer/reject',
            data: {"_token": token, "external_offer_id": external_offer_id},
            success:function(data) {
                if (data != null)
                {
                    data = $.parseJSON(data);
                    console.log(data);
                    if (data.success) {
                        window.location.reload();
                    }
                }


            },
            error: function(data) {
                console.log("/external-offer/reject error");
            },
            complete: function() {

            },
            global: true
        });
    });
    /** END ACCEPT AND REJECT EXTERNAL OFFER **/

    /** EXTERNAL OFFER GROUP **/
    $("#external-offer-send-to-group").click(function(){

        if($("#external-offer-send-to-group").is(":checked")) {
            $("#mo-external-group").removeClass("hidden");
            $("input[name='external-offer-group[]']").prop("disabled", false);
        }

    });

    $("#external-offer-send-to-public").click(function(){
        $("#mo-external-group").addClass("hidden");
        $("input[name='external-offer-group[]']").prop("disabled", true);
        $("input[name='external-offer-group[]']").prop("checked", false);

    });
    /** EXTERNAL OFFER GROUP **/

    /** VALIDATE SEND EXTERNAL OFFER FORM **/
    $("#mo-external-offering-form").validate({

        rules:{
            "external-offer-arrival-date": "required",
            "external-offer-group[]": "required"
        },

        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if(element.parent('.mo-external-group-wrapper').length){
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },

        messages: {
            "external-offer-arrival-date": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
            "external-offer-group[]" : "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>These fields above must be at least one checked.</span>"+"</div>"
        }
    });
    /** END VALIDATE EXTERNAL OFFER FORM **/

    /** ACCEPT EXTERNAL OFFER **/
    $("#mo-external-offer-accept-btn").click(function(){
        var external_offer_id = $("input[name='external_offer_id']").val();
        $.ajax({
            type:'POST',
            url: '/external-offer/accept',
            data: {"_token": token, "external_offer_id": external_offer_id},
            success:function(data) {
                if (data != null)
                {
                    data = $.parseJSON(data);
                    console.log(data);
                    if (data.success) {
                        window.location.reload();
                    }
                }


            },
            error: function(data) {
                console.log("/external-offer/accept error");
            },
            complete: function() {

            },
            global: true
        });
    });
    /** VALIDATE SEND EXTERNAL OFFER FORM **/

    /** SEND EXTERNAL OFFER **/
    var refreshIntervalId;
    var refreshIntervalId2;
    var seconds;
    $.calcRemainingTime = function() {
        var token = $("input[name='_token']").val();

        var valuation_id = $("input[name='valuation_id']").val();
        $.ajax({
            type:'POST',
            url: base_url+'/external-offer/get-info',
            data: {"_token": token, "valuation_id": valuation_id},
            success:function(data) {
                if (data != null)
                {
                    data = $.parseJSON(data);
                    console.log(data);
                    $("input[name='external_offer_id']").val(data.id);
                    var api_system_datetime = new Date(data.system_time);

                    var external_offer_created_datetime = new Date(data.create_date);

                    var diff = api_system_datetime - external_offer_created_datetime; //miniseconds
                    //default is 30 mins
                    var time_remaining = 0;
                    if(data.time_remaining != null) {
                        time_remaining = data.time_remaining;
                    } else {
                        time_remaining = 30;
                    }
                    seconds = time_remaining * 60 - parseInt(diff/1000);
                    var remaining_minutes = Math.floor(seconds % 3600 / 60);
                    var remaining_seconds = seconds % 60;

                    if (seconds > 0) {
                        refreshIntervalId = setInterval(function(){
                            seconds -= 1;
                            console.log("second: "+seconds);
                            if (seconds > 0) {
                                //var remaining_minutes = Math.floor(seconds % 3600 / 60);
                                //var remaining_seconds = seconds % 60;
                                //convert seconds to H:m:s
                                var date = new Date(null);
                                date.setSeconds(seconds); // specify value for SECONDS here
                                var result = date.toISOString().substr(11, 8);
                                //$(".remaining-time-label").text(remaining_minutes +":"+ remaining_seconds);
                                $(".remaining-time-label").text(result);

                            } else if (seconds == 0) {
                                //$("#mo-external-offer-send-btn").addClass("hidden");
                                //call ajax here
                                $("span.time-remaining").text("getting best price");

                            } else if (seconds == -10) {
                                refreshIntervalId2 = setInterval(function(){
                                    console.log("getting best price");
                                    $.getExternalOfferInfo();
                                }, 5000);
                            }
                        }, 1000);
                    } else if (seconds == 0) {
                        $.getExternalOfferInfo();
                    }

                }


            },
            error: function(data) {
                console.log("/external-offer/get-info error");
            },
            complete: function() {

            },
            global: false
        });
    }

    $.calcRemainingTime();

    $.getExternalOfferInfo = function() {
        var token = $("input[name='_token']").val();

        var valuation_id = $("input[name='valuation_id']").val();
        $.ajax({
            type:'POST',
            url: base_url+'/external-offer/get-info',
            data: {"_token": token, "valuation_id": valuation_id},
            success:function(data) {
                console.log("inside");
                if(data != null) {
                    data = $.parseJSON(data);
                    console.log(data);
                    if(parseInt(data.status) == 2 || parseInt(data.status) == 3) {

                        if(data.best_offer != null) {
                            clearInterval(refreshIntervalId);
                            clearInterval(refreshIntervalId2);
                            $("#mo-external-offer-accept-btn").removeClass("hidden");
                            $("#mo-external-offer-reject-btn").removeClass("hidden");
                            $("#mo-external-offer-send-btn").addClass("hidden");
                            $(".external-offer-currency-symbol").removeClass("hidden");
                            $("span.bid_price").text(data.best_offer.bid_price);
                            $("span.mo-external-offer-remaining-clock").addClass("hidden");
                            $("span.time-remaining").addClass("hidden");
                            $("span.external-offer-expiring").removeClass("hidden");
                            $("span.expiring-day").text(data.best_offer.expiry_date);
                        }
                    }
                    else
                    {
                        clearInterval(refreshIntervalId);
                        clearInterval(refreshIntervalId2);
                        window.location.reload();
                        //show null message here
                        //$("#mo-external-offer-send-btn").val("SEND OFFER REQUEST");
                        //$("#mo-external-offer-send-btn").prop("disabled", false);
                        ////$("span.mo-external-offer-null-message").removeClass("hidden");
                        //$("span.time-remaining").addClass("hidden");
                        //$("span.mo-external-offer-remaining-clock").addClass("hidden");

                    }
                }
            },
            error: function(data) {
                console.log("/external-offer/get-info error");
            },
            complete: function() {

            },
            global: false
        });
    }

    /*$("#external-offering-send").click(function(){
        console.log($("#mo-external-offering-form").serialize());
        if($("#mo-external-offering-form").valid()) {

            $(document).ajaxStart(function() {
                $(".mo-loading-image").removeClass("hidden");
            });
            $.ajax({
                type:'POST',
                url: '/request-external-offer',
                data: $("#mo-external-offering-form").serialize(),
                success:function(data) {

                    data = $.parseJSON(data);
                    if (data.success) {
                        $("input#mo-external-offer-send-btn").prop("disabled", true);
                        $("input#mo-external-offer-send-btn").val("awaiting offer...");
                        $(".mo-external-offer-remaining-clock").removeClass("hidden");
                        $(".time-remaining").removeClass("hidden");
                        $.calcRemainingTime();
                    }
                },
                error: function(data) {
                    console.log("error");
                },
                complete: function() {
                    // $('.mo-loading-image').addClass("hidden");
                    // magnificPopup.close();
                    window.location.reload();
                },
                global: true
            });
        }
    });*/
    /** END SEND EXTERNAL OFFER **/
/*
});*/
