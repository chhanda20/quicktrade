(function($){
    var path_name = window.location.pathname;
	
	/** INITIALIZE EMPTY CUSTOMER LIST **/
    $.constructDataTable = function(id, destroy, txt_mgs) {
        $(id).DataTable({
            "destroy" : destroy,
            "columnDefs": [
                {
                    "aTargets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ],
            "language": {
                "emptyTable": txt_mgs
            },
            "paging": false,
            "info": false,
            "filter": false,
            "responsive": true,
            //initialization params as usual
            fnInitComplete : function(settings, json) {
                if ($(this).find('tbody tr').length <= 1) {

                    $(this).find('thead th').hide();
                    $('.mo-pagination-wrapper').hide();
                    $(".mo-filter").hide();

                    /*** RESIZE WRAPPER ***/
                    // var $header = $('.mo-main-header');
                    // var $wrapper = $('#wrapper');
                    // var $window = $(window).on('resize', function() {
                    //     console.log('resize');
                    //     var height = $(this).height() - $header.height();
                    //     $wrapper.height(height);
                    // }).trigger('resize'); //on page load

                    /** vertical align DIV **/
                    $("#mo-companies-list_wrapper").toggleClass('percent-h-100');
                    $("#mo-list-wrapper").addClass("padding-bottom-32");
                }
            }
        });
    }

    $.initDataTable = function(id, destroy, columns, request_url, query, offset, init_complete, error_msg, scrollX, active, ajax_data) {

	    $(id).find('thead th').show();
        $('.mo-pagination-wrapper').show();
        $(".mo-filter").show();

        if (path_name.indexOf("transaction/view-transactions") != -1) {
            $(document).off(".buttonAction");
        }

        var ajax_data_datatable = function (d) {
            d._token = $("input[name='_token']").val();
            d.mo_txt_search = query;
            d.offset = offset;
            if(active){
                d.active = active
            }
        };

        if(ajax_data){
            ajax_data_datatable = ajax_data;
        }

	    $(id).DataTable({
	        "destroy" : destroy,
	        "language": {
	            "emptyTable": "<div>There are no valuation to display at this time.<br> Create a valuation by clicking on NEW button for specific Customer Detail or search for a valuation in the search box.</div>"
	        },
	        "paging": false,
	        "columnDefs": [
	            {
	                "aTargets": [ 0 ],
	                "visible": true,
	                "searchable": false
	            }
	        ],
	        "aoColumns": columns,
	        "info": false,
	        "filter": false,
	        "pageLength": 20,
	        "lengthChange": false,
	        "responsive": true,

            "dom": "Zlfrtip", //resizable column

	        "ajax" : {
                "url" : request_url,
                "type" : "POST",
                "data" : ajax_data_datatable,
                "error" : function () {
                    $.constructDataTable(id, true, error_msg);
                }
            },
	        //initialization params as usual
	        fnInitComplete : function(settings, json) {

                if (path_name.indexOf("dealership-list") != -1) {
                    var status = null;
                    var company_id = null;
                    $(".mo-dealership-suspend").click(function() {

                        status = $(this).data("status");
                        company_id = $(this).data("company-id");

                        if (status == 1) {
                            $(".suspend-popup-content").text("Are you sure you want to unlock this account ?");
                            $(".mo-popup-heading").text("Unlock");
                        } else {
                            $(".suspend-popup-content").text("Are you sure you want to suspend this account ?");
                            $(".mo-popup-heading").text("Suspend");
                        }

                    });
                    $("#mo-company-change-status").click(function() {
                        var token = $("input[name=_token]").val();
                        if (status == 1) {
                            status = 2
                            $(".suspend-popup-content").val("Are you sure you want to unlock this account ?");
                            $(".mo-popup-heading").val("Unlock");
                        } else {
                            $(".suspend-popup-content").val("Are you sure you want to suspend this account ?");
                            $(".mo-popup-heading").val("Suspend");
                            status = 1
                        }
                        $.ajax({
                            type: "POST",
                            url: "/mo-admin/suspend-company/"+company_id,
                            headers: {"X-CSRF-TOKEN": token},
                            data: {"status" : status},

                            success: function(result) {

                                result = $.parseJSON(result);
                                console.log(result);
                                if(result.hasOwnProperty("id")){
                                    magnificPopup.close();
                                    location.reload();
                                }
                            },

                            error: function() {
                                console.log("Error");
                            }
                        });
                    });

                    $(".mo-dealership-edit").click(function() {
                    });

                    $(".mo-cancel-btn").click(function() {
                        magnificPopup.close();
                    });

                    var magnificPopup = $.magnificPopup.instance;
                    $('.mo-suspend-popup').magnificPopup({
                        type:'inline',
                        showCloseBtn: true,
                        closeOnBgClick: true,
                        callbacks: {
                            beforeClose: function(){

                            }
                        }
                    });
                }

                if(path_name.indexOf("/user/list") != -1){
                    var status = null;
                    var user_id = null;
                    $(".mo-user-suspend").click(function() {
                        status = $(this).data("status");
                        user_id = $(this).data("user-id");

                        if (status == 0) {
                            $(".suspend-popup-content").text("Are you sure you want to restore this account?");
                            $(".mo-popup-heading").text("Restore");
                        } else {
                            $(".suspend-popup-content").text("Are you sure you want to suspend this account?");
                            $(".mo-popup-heading").text("Suspend");
                        }

                    });
                    $("#mo-user-change-status").click(function() {
                        $(document).ajaxStart(function() {
                            $(".mo-loading-image").removeClass("hidden");
                        });
                        var token = $("input[name=_token]").val();
                        if (status == 0) {
                            status = 1;
                            $(".suspend-popup-content").val("Are you sure you want to restore this account?");
                            $(".mo-popup-heading").val("Restore");
                        } else {
                            $(".suspend-popup-content").val("Are you sure you want to suspend this account?");
                            $(".mo-popup-heading").val("Suspend");
                            status = 0;
                        }
                        $.ajax({
                            type: "POST",
                            url: "/dealership/update-status",
                            headers: {"X-CSRF-TOKEN": token},
                            data: {"userId" : user_id,"status" : status},

                            success: function(result) {
                                console.log(result);
                                if(result == 1){
                                    magnificPopup.close();
                                    location.reload();
                                }
                            },
                            error: function() {
                                console.log("Error");
                            },
                            complete: function(){
                                $('.mo-loading-image').addClass("hidden");
                            }
                        });
                    });

                    $(".mo-cancel-btn").click(function() {
                        magnificPopup.close();
                    });

                    var magnificPopup = $.magnificPopup.instance;
                    console.log('magnificPopup:' + magnificPopup);
                    $('.mo-suspend-popup').magnificPopup({
                        type:'inline',
                        showCloseBtn: true,
                        closeOnBgClick: false,
                        callbacks: {
                            beforeClose: function(){

                            }
                        }
                    });
                }
                
                /*********** start ROLE LIST *********/
                if(path_name.indexOf("/mo-admin/role-list") != -1){
                    var role_id = null;
                    $(".mo-delete-btn").click(function(){
                        role_id = $(this).data("role-id");
                    });

                    $("#mo-confirm-delete").click(function() {
                        $(document).ajaxStart(function() {
                            $(".mo-loading-image").removeClass("hidden");
                        });
                        var token = $("input[name=_token]").val();
                        $.ajax({
                            type: "POST",
                            url: "/mo-admin/delete-role/"+role_id,
                            headers: {"X-CSRF-TOKEN": token},
                            success: function(result) {
                                console.log(result);
                                if(result == 1){
                                    magnificPopup.close();
                                    location.reload();
                                }
                            },
                            error: function() {
                                console.log("Error");
                            },
                            complete: function(){
                                $('.mo-loading-image').addClass("hidden");
                            }
                        });
                    });

                    $(".mo-cancel-btn").click(function() {
                        magnificPopup.close();
                    });
                    var magnificPopup = $.magnificPopup.instance;
                    $('.mo-delete-popup').magnificPopup({
                        type:'inline',
                        showCloseBtn: true,
                        closeOnBgClick: false,
                        callbacks: {
                            beforeClose: function(){

                            }
                        }
                    });
                }
                /**************** end ROLE LIST*************/

	            if (init_complete) {
	            	console.log(json);
                    var list_length = json.count;
                    if(list_length <= 20){
                        $(".mo-pagination-wrapper").hide();
                    }
	                page_length = data_length = Math.ceil(json.count/20);
	                $(".mo-page-number-wrap").remove();
                    var activeClass = '';
	                for(var i = data_length; i >= 1; i--) {
                        if(i == 1) {
                            activeClass = 'active';
                        }else {
                            activeClass = '';
                        }
	                    $("<li class='mo-page-number-wrap'><a class='mo-page-number "+activeClass+"' href='#'>"+i+"</a></li>").insertAfter(".mo-page-number-list");
	                }

	                $(".mo-page-number").click(function() {

	                    $(".mo-page-number").removeClass("active");
	                    var clicked = $(this);
	                    clicked.addClass("active");
	                    current_page = clicked.html();

	                    var offset = (clicked.html() - 1)*20;
	                    console.log("offset "+offset);
                        var destroy = true;
                        var init = false;
                        var query = $("#mo-txt-search").val();
                        var active = true;
                        if ($("#mo-filter-dealership").is(":checked")) {
                            active = true;
                        } else {
                            active = false;
                        }

	                    //$.initDataTable(id,true, columns, request_url, query, offset, false, error_msg);
                        if (path_name.indexOf("dealership-list") != -1) {
                            $.getDealership(destroy, active, init, query, offset);
                        }
                        else if (path_name.indexOf("dealer-groups-list") != -1){
                            $.getDealerGroups(destroy, active, init, query, offset);
                        }
                        else if (path_name.indexOf("location-list") != -1){
                            $.getLocation(destroy, active, init, query, offset);
                        }
                        else if (path_name.indexOf("role-list") != -1){
                            $.getRoleList(destroy, active, init, query, offset);
                        }
                        else if (path_name.indexOf("auction-session-list") != -1){
                            $.getAuctionSession(destroy, active, init, query, offset);
                        }
                        else if (path_name.indexOf("auction-history") != -1){
                            $.getAuctionSessionHistory(destroy, active, init, query, offset);
                        }
                        else if (path_name.indexOf("request-list") != -1) {
                            
                            $.getAuctionSession(destroy, active, init, query, offset);
                            
                        }
                        else if (path_name.indexOf("user/list") != -1) {
                            $.getUsers(destroy, active, init, query, offset);
                        }
                        else if (path_name.indexOf("transaction/view-transactions") != -1) {
                            if(sessionStorage.getItem("elem_to_paging")) {
                                var data_obj = JSON.parse(sessionStorage.getItem("elem_to_paging"));
                            }
                            if(offset && data_obj.ajax_data){
                                data_obj.ajax_data.offset = offset;
                            }
                            $.getTransactionsList(destroy, init, query, offset, data_obj.request_url, data_obj.table_id, active, data_obj.tab, data_obj.ajax_data);
                        }
                        else if (path_name.indexOf("valuations") != -1) {
                            $.getValuations(destroy, active, init, query, offset);
                        }

	                });
	            }

                if (path_name.indexOf("transaction/view-transactions") != -1) {
                    /** BOOKING TRANSPORT POPUP **/
                    $(document).on("click",".mo-cancel-btn",function() {
                        var magnificPopup = $.magnificPopup.instance;
                        magnificPopup.close();
                    });

                    $(".mo-booking-popup").magnificPopup({
                        type:'inline',
                        showCloseBtn: true,
                        closeOnBgClick: false
                    });

                    $(".mo-payment-popup").magnificPopup({
                        type:'inline',
                        showCloseBtn: true,
                        closeOnBgClick: false
                    });
                }

                if (path_name.indexOf("trading/request-list") != -1) {
                    //auto check auction session 
                    var request_id = $(".pagination").data("request-id");
                    
                    var session_id = $(".pagination").data("session-id");
                    
                    $("input[name='request-list-select-auction-session']").each(function(index, value) {
                        if ($(value).val() == session_id) {
                            $(value).prop("checked", true);
                            return;
                        }
                    });
                    //end auto check auction session
                }

                if(path_name.indexOf("valuations") != -1) {
	                handleValuationFunction();//in ---js/mo-admin/valuation/viewValuation.js---
                }
	        }
	    });

	}

	$.moDataTable = function(id_table, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg, active, ajax_data) {
        if (path_name.indexOf("dealership-list") != -1
            || path_name.indexOf("dealer-groups-list") != -1
            || path_name.indexOf("user/list") != -1
            || path_name.indexOf("valuations") != -1) {
            $.initDataTable(id_table, destroy, columns, request_url, query, offset, init, error_msg, scrollX, active);
        }else if(path_name.indexOf("location-list") != -1
            || path_name.indexOf("role-list") != -1
            || path_name.indexOf("auction-session-list") != -1
            || path_name.indexOf("auction-history") != -1
        ) {
            $.initDataTable(id_table, destroy, columns, request_url, query, offset, init, error_msg, scrollX, active);
            $(id_table).DataTable().order([ 1, 'asc' ]);
        }else {
            $(document).ready(function(){
                console.log('id_table:' + id_table);
                $.initDataTable(id_table, destroy, columns, request_url, query, offset, init, error_msg, scrollX, active, ajax_data);
                $(id_table).DataTable().order([ 1, 'asc' ]);
            });
        }
	}

	$(document).ready(function() {
        var current_page = 1;

		$("#mo-page-prev").click(function() {

            if (current_page > 1) {
                $(".mo-page-number").removeClass("active");
                current_page = parseInt(current_page) - 1;
                var offset = (current_page - 1)*20;
                var destroy = true;
				var active = true;
				if ($("#mo-filter-dealership").is(":checked")) {
					active = true;
				} else {
					active = false;
				}
				var init = false;
				var query = $("#mo-txt-search").val();

                if (path_name.indexOf("dealership-list") != -1) {
                    $.getDealership(destroy, active, init, query, offset);
                }
                else if (path_name.indexOf("dealer-groups-list") != -1){
                    $.getDealerGroups(destroy, active, init, query, offset);
                }
                else if (path_name.indexOf("location-list") != -1){
                    $.getLocation(destroy, active, init, query, offset);
                }
                else if (path_name.indexOf("role-list") != -1){
                    $.getRoleList(destroy, active, init, query, offset);
                }
                else if (path_name.indexOf("auction-session-list") != -1){
                    $.getAuctionSession(destroy, active, init, query, offset);
                }
                else if (path_name.indexOf("auction-history") != -1){
                    $.getAuctionSessionHistory(destroy, active, init, query, offset);
                }
                else if (path_name.indexOf("request-list") != -1){
                    $.getAuctionSession(destroy, active, init, query, offset);
                }
                else if (path_name.indexOf("user/list") != -1){
                    $.getUsers(destroy, active, init, query, offset);
                }
                else if (path_name.indexOf("transaction/view-transactions") != -1) {
                    if(sessionStorage.getItem("elem_to_paging")) {
                        var data_obj = JSON.parse(sessionStorage.getItem("elem_to_paging"));
                        if(offset && data_obj.ajax_data){
                            data_obj.ajax_data.offset = offset;
                        }
                    }
                    $.getTransactionsList(destroy, init, query, offset, data_obj.request_url, data_obj.table_id, active, data_obj.tab, data_obj.ajax_data);
                }
                else if (pathname.indexOf("valuations") != -1) {
                    $.getValuations(destroy, active, init, query, offset);
                }

                //$.initDataTable(id,true, columns, request_url, query, offset, false, error_msg, scrollX);
                //$(".mo-page-number")[0].addClass("active");
                $($(".mo-page-number")[current_page - 1]).addClass("active");
            }

        });

        $("#mo-page-next").click(function() {
            if (current_page < page_length) {

                $(".mo-page-number").removeClass("active");
                current_page = parseInt(current_page) + 1;
                var offset = (current_page - 1)*20;
                var destroy = true;
				var active = true;
				if ($("#mo-filter-dealership").is(":checked")) {
					active = true;
				} else {
					active = false;
				}
				var init = false;
				var query = $("#mo-txt-search").val();

                if (path_name.indexOf("dealership-list") != -1) {
                    $.getDealership(destroy, active, init, query, offset);
                }
                else if (path_name.indexOf("dealer-groups-list") != -1){
                    $.getDealerGroups(destroy, active, init, query, offset);
                }
                else if (path_name.indexOf("location-list") != -1){
                    $.getLocation(destroy, active, init, query, offset);
                }
                else if (path_name.indexOf("role-list") != -1){
                    $.getRoleList(destroy, active, init, query, offset);
                }
                else if (path_name.indexOf("auction-session-list") != -1){
                    $.getAuctionSession(destroy, active, init, query, offset);
                }
                else if (path_name.indexOf("auction-history") != -1){
                    $.getAuctionSessionHistory(destroy, active, init, query, offset);
                }
                else if (path_name.indexOf("request-list") != -1){
                    $.getAuctionSession(destroy, active, init, query, offset);
                }
                else if (path_name.indexOf("user/list") != -1){
                    $.getUsers(destroy, active, init, query, offset);
                }
                else if (path_name.indexOf("transaction/view-transactions") != -1) {
                    if(sessionStorage.getItem("elem_to_paging")) {
                        var data_obj = JSON.parse(sessionStorage.getItem("elem_to_paging"));
                        if(offset && data_obj.ajax_data){
                            data_obj.ajax_data.offset = offset;
                        }
                    }
                    $.getTransactionsList(destroy, init, query, offset, data_obj.request_url, data_obj.table_id, active, data_obj.tab, data_obj.ajax_data);
                }
                else if (pathname.indexOf("valuations") != -1) {
                    $.getValuations(destroy, active, init, query, offset);
                }

                //$.initDataTable(id,true, columns, request_url, query, offset, false, error_msg, scrollX);
                $($(".mo-page-number")[current_page - 1]).addClass("active");
            }

        });

		//filter dealership list
		$("#mo-filter-dealership").click(function(){
			current_page = 1;
			var destroy = true;
			var init = true;
			var query = $("#mo-txt-search").val();
			var offset = 0;
			if ($("#mo-filter-dealership").is(":checked")) {
				var active = true;
			} else {
				var active = false;
			}

            if (path_name.indexOf("dealership-list") != -1) {
                $.getDealership(destroy, active, init, query, offset);
            }
            else if (path_name.indexOf("dealer-groups-list") != -1){
                $.getDealerGroups(destroy, active, init, query, offset);
            }
            else if (path_name.indexOf("user/list") != -1){
                $.getUsers(destroy, active, init, query, offset);
            }

		});
		//end filter dealership list

        $('.tran-tab').click(function() {
            sessionStorage.removeItem("required-action");
        });

        //filter dealership list
        $("#required-action").click(function(){
            current_page = 1;
            var destroy = true;
            var init = true;
            var query = null;
            var offset = 0;
            var active = null;
            if ($("#required-action").is(":checked")) {
                active = true;
                sessionStorage.setItem("required-action", active);
            } else {
                sessionStorage.removeItem("required-action");
            }


            if (path_name.indexOf("transaction/view-transactions") != -1) {
                if(sessionStorage.getItem("elem_to_paging")) {
                    var data_obj = JSON.parse(sessionStorage.getItem("elem_to_paging"));
                    if(active && data_obj.ajax_data){
                        data_obj.ajax_data.active = true;
                    }else{
                        //data_obj.ajax_data.active = null;
                    }
                }
                console.log(data_obj.ajax_data);
                $.getTransactionsList(destroy, init, query, offset, data_obj.request_url, data_obj.table_id, active, data_obj.tab, data_obj.ajax_data);
            }

        });

		//search dealership
		$(".mo-txt-search").click(function(){
			current_page = 1;
			var destroy = true;
			var active = true;
			if ($("#mo-filter-dealership").is(":checked")) {
				active = true;
			} else {
				active = false;
			}
			var init = true;
			var query = $("#mo-txt-search").val();
			var offset = 0;

            if (pathname.indexOf("dealership-list") != -1) {
                $.getDealership(destroy, active, init, query, offset);
            }
            else if (pathname.indexOf("dealer-groups-list") != -1) {
                $.getDealerGroups(destroy, active, init, query, offset);
            }
            else if (pathname.indexOf("location-list") != -1) {
                $.getLocation(destroy, init, active, query, offset);
            }
            else if (pathname.indexOf("auction-session-list") != -1) {
                $.getAuctionSession(destroy, init, active, query, offset);
            }
            else if (pathname.indexOf("auction-history") != -1) {
                $.getAuctionSessionHistory(destroy, init, active, query, offset);
            }
            else if (pathname.indexOf("user/list") != -1) {
                $.getUsers(destroy, init, active, query, offset);
            }
            else if (pathname.indexOf("valuations") != -1) {
                $.getValuations(destroy, active, init, query, offset);
            }

		});

		$("#mo-txt-search").bind("enterKey", function(e){
			current_page = 1;
			var destroy = true;
			var active = true;
			if ($("#mo-filter-dealership").is(":checked")) {
				active = true;
			} else {
				active = false;
			}
			var init = true;
			var query = $("#mo-txt-search").val();
			var offset = 0;

            if (path_name.indexOf("dealership-list") != -1) {
                $.getDealership(destroy, active, init, query, offset);
            }
            else if (path_name.indexOf("dealer-groups-list") != -1){
                $.getDealerGroups(destroy, active, init, query, offset);
            }
            else if (pathname.indexOf("location-list") != -1) {
                $.getLocation(destroy, active, init, query, offset);
            }
            else if (path_name.indexOf("user/list") != -1){
                $.getUsers(destroy, active, init, query, offset);
            }
            else if (pathname.indexOf("valuations") != -1) {
                $.getValuations(destroy, active, init, query, offset);
            }

        });

		$('#mo-txt-search').keyup(function(e){
			if(e.keyCode == 13)
			{
			  $(this).trigger("enterKey");
			}
		});
        
		//end search dealership

		var pathname = window.location.pathname;
		
		/** COMPANIES **/
		$.getDealership = function(destroy, active, init, query, offset) {
	 		var id = "#mo-companies-list";
	    	var destroy = destroy;
			var columns = 	[
                {className: "never"},
                {className: "all"},
                {className: "all"},
                {className: "desktop","bSortable": false },
                {className: "all","bSortable": false },
                {className: "desktop","bSortable": false},
                {className: "desktop", "width": "15%", "bSortable": false,

                    "mRender": function (data, type, row, meta) {
                        if (row[6] == 2) {
                            return '<a href="#mo-suspend-popup" class="mo-suspend-popup"><span class="fa fa-unlock-alt float-right cursor-pointer mo-dealership-function mo-dealership-suspend" data-company-id="'+row[0]+'" data-status="'+row[6]+'"></span></a>' +
                                '<a href="/mo-admin/create-dealership/edit/'+row[0]+'"><span class="glyphicon glyphicon-pencil float-right cursor-pointer mo-dealership-function mo-dealership-edit" data-company-id="'+row[0]+'"></span></a>';
                        } else {
                            return '<a href="#mo-suspend-popup" class="mo-suspend-popup"><span class="fa fa-lock float-right cursor-pointer mo-dealership-function mo-dealership-suspend" data-company-id="'+row[0]+'" data-status="'+row[6]+'"></span></a>' +
                                '<a href="/mo-admin/create-dealership/edit/'+row[0]+'"><span class="glyphicon glyphicon-pencil float-right cursor-pointer mo-dealership-function mo-dealership-edit" data-company-id="'+row[0]+'"></span></a>';
                        }

                    }
                }
            ];
			var request_url = "/mo-admin/get-companies";
			var query = query;
			var offset = offset;
			var init = init;
			var error_msg = "<div>There are no dealership to display at this time.<br>Create a dealership by clicking the new icon on the bottom right or "+
							"search for a dealership in the search box.</div>";
            var scrollX = false;
			$.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, active);
		}

		if (pathname.indexOf("dealership-list") != -1) {
	 		var id = "#mo-companies-list";
	 		var error_msg = "<div>There are no dealership to display at this time.<br>Create a dealership by clicking the new icon on the bottom right or "+
							"search for a dealership in the search box.</div>";
	 		$.constructDataTable(id, false, error_msg);
	 	}

        /** COMPANY GROUPS **/
        else if(pathname.indexOf("dealer-groups-list" ) != -1){

            $.getDealerGroups = function(destroy, active, init, query, offset) {
                var id = "#mo-company-groups-list";
                var destroy = destroy;
                var columns = 	[
                    {className: "never"},
                    {className: "all"},
                    {className: "all", "width": "50%", "bSortable": false },
                    {className: "desktop","bSortable": false },
                    {className: "all", "width": "15%", "bSortable": false,
                        "mRender": function (data, type, row, meta) {
                            return '<a href="/mo-admin/create-dealer-group/edit/' + row[0] + '"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                        }
                    }
                ];
                var request_url = "/mo-admin/get-dealer-groups";
                var query = query;
                var offset = offset;
                var init = init;
                var error_msg = "<div>There are no dealer group to display at this time.<br>Create a dealer group by clicking the new icon on the bottom right or "+
                    "search for a dealer group in the search box.</div>";
                var scrollX = false;
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, active);
            }

            var id = "#mo-company-groups-list";
            var error_msg = "<div>There are no dealer group to display at this time.<br>Create a dealer group by clicking the new icon on the bottom right or "+
                "search for a dealer group in the search box.</div>";
            $.constructDataTable(id, false, error_msg);
        }


        /** LOCATION **/
        else if(pathname.indexOf("location-list") != -1){
            $.getLocation = function(destroy, active, init, query, offset) {
                var id = "#mo-location-list";
                var destroy = destroy;
                var columns = [
                    {className: "never" }, //ID - hidden
                    {className: "all", "bSortable": true },  // dealergroup
                    {className: "all", "bSortable": true }, // dealership
                    {className: "all", "bSortable": true }, // name
                    {className: "all", "bSortable": false }, // code
                    {className: "all", "bSortable": false }, // status
                    {className: "all", "bSortable": false,
                        "mRender": function (data, type, row, meta) {
                            return '<a href="/mo-admin/init-location-form/edit/' + row[0] + '"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                        }
                    }
                ];
                var request_url = "/mo-admin/get-location-list";
                var offset = offset;
                var init = init;
                var error_msg = "<div>There are no auction session to display at this time.<br> Create new auction session by clicking on NEW button.</div>";
                var empty_msg = "<div>There are no auction session to display at this time.<br> Create new auction session by clicking on NEW button.</div>";
                var query = query;
                var scrollX = true;
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg, active);
            }

            var destroy = true;
            var active = true;
            var init = true;
            var query= null;
            var offset=0;
            $.getLocation(destroy, active, init, query, offset);      
        }

        /** ROLE **/
        else if(pathname.indexOf("role-list") != -1){
            $.getRoleList = function(destroy, active, init, query, offset) {
                var id = "#mo-role-list";
                var destroy = destroy;
                var columns = [
                    {className: "never" }, //ID - hidden
                    {className: "all", "bSortable": true },  // dealergroup
                    {className: "all", "bSortable": true }, // dealership
                    {
                        className: "all", "bSortable": false,
                        "mRender": function (data, type, row, meta) {
                            return '<a href="#mo-delete-confirm-popup" class="mo-delete-popup"><input type="button" class="mo-delete-btn text-capitalize float-right" value="Delete" data-role-id="'+row[0]+'"></a>' +
                                '<a href="/mo-admin/create-role/edit/'+row[0]+'"><input type="button" class="mo-permissions-btn text-capitalize float-right" value="Permissions"></a>';

                        }
                    }
                ];
                var request_url = "/mo-admin/get-role-list";
                var offset = offset;
                var init = init;
                var error_msg = "<div>There are no auction session to display at this time.<br> Create new auction session by clicking on NEW button.</div>";
                var empty_msg = "<div>There are no auction session to display at this time.<br> Create new auction session by clicking on NEW button.</div>";
                var query = query;
                var scrollX = true;
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg, active);
            }

            var destroy = true;
            var active = true;
            var init = true;
            var query= null;
            var offset=0;
            $.getRoleList(destroy, active, init, query, offset);
        }

        /** AUCTION SESSION LIST **/
        else if(pathname.indexOf("/trading/auction-session-list") != -1) {
            $.getAuctionSession = function(destroy, active, init, query, offset) {
                var id = "#mo-auction-session-list";
                var destroy = destroy;
                var columns = [
                    {className: "never" }, //ID - hidden
                    {className: "all", "bSortable": true, "width": "20%" }, // start date
                    {className: "all", "bSortable": false, "width": "15%" }, // start time
                    {className: "all", "bSortable": false, "width": "25%" }, // duration
                    {className: "all", "bSortable": false, "width": "25%" }, // bid increment
                    {className: "all", "bSortable": false }, // vehicle
                    {className: "all", "width": "10%", "bSortable": false,
                        "mRender": function (data, type, row, meta) {
                            return '<a href="/mo-admin/trading/create-auction-session/edit/' + row[0] + '"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                        }
                    }
                    

                ];
                var request_url = "/mo-admin/trading/get-auction-session-list";
                var offset = offset;
                var init = init;
                var error_msg = "<div>There are no auction session to display at this time.<br> Create new auction session by clicking on NEW button.</div>";
                var empty_msg = "<div>There are no auction session to display at this time.<br> Create new auction session by clicking on NEW button.</div>";
                var query = query;
                var scrollX = true;
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg, active);
            }
            var destroy = true;
            var active = true;
            var init = true;
            var query= null;
            var offset=0;
            $.getAuctionSession(destroy, active, init, query, offset);
            // var id = "#mo-auction-session-list";
            // var error_msg = "<div>There are no auction session to display at this time.<br> Create new auction session by clicking on NEW button.</div>";
            // $.constructDataTable(id, false, error_msg);

        }

        else if(pathname.indexOf("/trading/auction-history") != -1) {
            $.getAuctionSessionHistory = function(destroy, active, init, query, offset) {
                var id = "#mo-auction-session-history-list";
                var destroy = destroy;
                var columns = [
                    {className: "never" }, //ID - hidden
                    {className: "all", "bSortable": true, "width": "25%" }, // Auction Name
                    {className: "all", "bSortable": true, "width": "20%" }, // start date
                    {className: "all", "bSortable": false, "width": "15%" }, // start time
                    {className: "all", "bSortable": false, "width": "25%" }, // duration
                    {className: "all", "bSortable": false }, // vehicle
                    {className: "all", "width": "10%", "bSortable": false,
                        "mRender": function (data, type, row, meta) {
                            //console.log('data:');
                            //console.log(data);
                            //
                            //console.log('type:');
                            //console.log(type);

                            //console.log('meta:');
                            //console.log(meta);
                            var vehicleCount = parseInt(row[5]);
                            if (vehicleCount > 0) {
                                return '<a href="/mo-admin/trading/auction-history-detail/' + row[0] + '"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                            }
                            return '';

                        }
                    }


                ];
                var request_url = "/mo-admin/trading/get-auction-session-history-list";
                var offset = offset;
                var init = init;
                var error_msg = "<div>There are no auction session to display at this time.<br> Create new auction session by clicking on NEW button.</div>";
                var empty_msg = "<div>There are no auction session to display at this time.<br> Create new auction session by clicking on NEW button.</div>";
                var query = query;
                var scrollX = true;
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg, active);
            }
            var destroy = true;
            var active = true;
            var init = true;
            var query= null;
            var offset=0;
            $.getAuctionSessionHistory(destroy, active, init, query, offset);
            // var id = "#mo-auction-session-list";
            // var error_msg = "<div>There are no auction session to display at this time.<br> Create new auction session by clicking on NEW button.</div>";
            // $.constructDataTable(id, false, error_msg);

        }

        /** REQUEST LIST SELECT AUCTION SESSION **/
        else if(pathname.indexOf("request-list") != -1) {

            $(document).ready(function(){
                $.getAuctionSession = function(destroy, active, init, query, offset) {
                    var id = "#mo-table-approve-request-list";
                    var destroy = destroy;
                    var columns = [
                        {className: "never" }, //ID - hidden
                        {className: "all", "bSortable": true, "width": "20%" }, // start date
                        {className: "all", "bSortable": false, "width": "15%" }, // start time
                        {className: "all", "bSortable": false, "width": "20%" }, // start duration
                        {className: "all", "bSortable": false, "width": "20%" }, // start bid increment
                        {className: "all", "bSortable": false, "width": "15%" }, // start vehicle
                        {className: "all", "width": "10%", "bSortable": false,
                            "mRender": function (data, type, row, meta) {
                                return '<input type="radio" class="mo-round-check" id="request-list-select-auction-session-'+row[0]+'" name="request-list-select-auction-session" value="'+row[0]+'" />'
                                       +'<label for="request-list-select-auction-session-'+row[0]+'" class="text-capitalize"><span class=""></span></label>';
                            }
                        }
                    ];
                    var request_url = base_url+"/mo-admin/trading/get-auction-session-list";
                    var offset = offset;
                    var init = init;
                    var error_msg = "<div>There are no auction session to display at this time.<br> Create new auction session by clicking on NEW button.</div>";
                    var empty_msg = "<div>There are no auction session to display at this time.<br> Create new auction session by clicking on NEW button.</div>";
                    var query = query;
                    var scrollX = true;
                    $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg, active);
                }

                var destroy = true;
                var active = true;
                var init = true;
                var query= null;
                var offset=0;
                $.getAuctionSession(destroy, active, init, query, offset);

            });
        }

        else if (path_name.indexOf("/mo-admin/user/list") != -1 || path_name.indexOf("web/mo-admin/user/list") != -1) {
debugger;
            $.getUsers = function(destroy, active, init, query, offset) {
                debugger;
                var id = "#mo-table-user-list";
                var destroy = destroy;
                var columns = [
                    {className: "all"}, //dealergroup
                    {className: "all"}, //dealership
                    {className: "all"},//firstName
                    {className: "all"},//lastName
                    {className: "all", "bSortable": false},//role
                    {className: "none", "bSortable": false},//login
                    {className: "none", "bSortable": false},//logout
                    {className: "all", "bSortable": false},//location
                    {className: "never", "bSortable": false},//active
                    {className: "never"},//ID - hidden
                    {
                        className: "all", "bSortable": false,
                        "mRender": function (data, type, row, meta) {
                            if(row[8] == 1){
                                return '<a href="#mo-suspend-popup" class="mo-suspend-popup"><span class="fa fa-unlock-alt cursor-pointer mo-dealership-function mo-user-suspend" data-user-id="'+row[9]+'" data-status="'+row[8]+'"></span></a>' +
                                    '<a href='+base_url+'"/mo-admin/user/edit/' + row[9] + '"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                            }else{
                                return '<a href="#mo-suspend-popup" class="mo-suspend-popup"><span class="fa fa-lock cursor-pointer mo-dealership-function mo-user-suspend" data-user-id="'+row[9]+'" data-status="'+row[8]+'"></span></a>' +
                                    '<a href='+base_url+'"/mo-admin/user/edit/' + row[9] + '"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                            }

                        }
                    }
                ];
                var request_url = base_url+"/mo-admin/user/get-users";
                if($('#mo-role-id').val()){
                    request_url = base_url+"/mo-admin/user/get-users/"+$('#mo-role-id').val();
                }
                var query = query;
                var offset = offset;
                var init = init;
                var error_msg = "<div>There are no users to display at this time.</div>";
                var scrollX = true;
                var empty_msg = "<div>There are no users to display at this time.<br> Create new user by clicking on NEW button or search for a user in the search box.</div>";
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg, active);
            }
            var destroy = false;
            var init = true;
            var query = null;
            var offset = 0;
            var active = false;
            $.getUsers(destroy, active, init, query, offset);


            //filter dealership list
            $("#mo-filter-user").click(function(){
                current_page = 1;
                var destroy = true;
                var init = true;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                if ($("#mo-filter-user").is(":checked")) {
                    var active = true;
                } else {
                    var active = false;
                }
                $.getUsers(destroy, active, init, query, offset);
            });
            //end filter dealership list
        }
	});

}(jQuery));