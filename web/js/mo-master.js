/*
 * Created by user on 11/18/2016.
 */

/** Toggle Show/Hide Menu Sidebar **/

(function ($) {
    $(document).ready(function(){

        var url_location = window.location;
        var pathname = window.location.pathname;
        var hash = window.location.hash;

        /*** SHOW latest NOTIFICATION when RELOAD page ***/
        if(typeof(Storage) !== "undefined" && localStorage.getItem("notification_content")){
            // Retrieve the object from storage
            var retrievedObject = localStorage.getItem('notification_content');
            var parseRetrievedObject = JSON.parse(retrievedObject);

            /** APPEND NOTIFICATION **/
            var mo_content_event_code = base_url+'/valuations/requests';
            var mo_content_a_tag = '';
            var mo_content_start ='<div class="mo-notification-item"><div class="mo-notification-title vertical-center">';
            var mo_content_end = '</p></div></a><hr class="mo-notification-hr">';

            /** REVERSE and SHOW LATEST notification **/
            //parseRetrievedObject.reverse();
            //console.log(parseRetrievedObject.length);

            for(var i=0; i<parseRetrievedObject.length; i++) {
                var mo_content_time = '';
                var mo_content_reg = '';
                var mo_content_res_price = '';
                var mo_content_val_price = '';
                var mo_content_max_price = '';

                switch (parseRetrievedObject[i].eventCode) {
                    case "valuation_request":
                        mo_content_event_code = base_url+'/valuations/requests';
                        break;
                    case "valuation_request_notify_1":
                        mo_content_event_code = base_url+'/valuations/requests';
                        break;
                    case "valuation_request_notify_2":
                        mo_content_event_code = base_url+'/valuations/requests';
                        break;
                    case "valuation_accepted":
                        mo_content_event_code = 'javascript:void(0)';
                        break;
                    case "valuer_complete_valuation":
                        mo_content_event_code = base_url+'/valuations/pending';
                        break;
                    case "adjust_price_only":
                        mo_content_event_code = base_url+'/valuations/pending';
                        mo_content_res_price = '<br/>Requested Price: ';
                        mo_content_val_price = '<br/>Valuation Price: ';
                        break;
                    case "approve_price_only":
                        mo_content_event_code = base_url+'/valuations/pending';
                        mo_content_val_price = '<br/>Valuation Price: ';
                        break;
                    case "approve_price_max":
                        mo_content_event_code = base_url+'/valuations/pending';
                        mo_content_val_price = '<br/>Valuation Price: ';
                        mo_content_max_price = '<br/>Max Valuation Price: ';
                        break;
                    case "adjust_price_max":
                        mo_content_event_code = base_url+'/valuations/pending';
                        mo_content_res_price = '<br/>Requested Price: ';
                        mo_content_val_price = '<br/>Valuation Price: ';
                        mo_content_max_price = '<br/>Max Valuation Price: ';
                        break;
                    default:
                        mo_content_event_code = 'javascript:void(0)';
                }
                mo_content_a_tag = '<a href="'+mo_content_event_code+'" class="mo-content">';

                var web_message = parseRetrievedObject[i].message;
                if(parseRetrievedObject[i].web_message){
                    web_message = parseRetrievedObject[i].web_message;
                }

                if(parseRetrievedObject[i].time){
                    mo_content_time = '<br/>'+moment(parseRetrievedObject[i].time).format('Do MMM YYYY h:mm a').toString();
                }

                if(parseRetrievedObject[i].registration_number){
                    mo_content_reg = '<br/>Reg: '+parseRetrievedObject[i].registration_number;
                }

                if(parseRetrievedObject[i].requested_price){
                    mo_content_res_price += parseRetrievedObject[i].requested_price;
                }

                if(parseRetrievedObject[i].offer_value){
                    mo_content_val_price += parseRetrievedObject[i].offer_value;
                }

                if(parseRetrievedObject[i].valuation_value){
                    mo_content_max_price += parseRetrievedObject[i].valuation_value;
                }

                $("#mo-notification-wrapper").append(
                    mo_content_a_tag +
                    mo_content_start +
                    '<h5 class="mo-item-title col-xs-11 no-padding">'+parseRetrievedObject[i].title+'</h5>' +'</div>'+
                    '<p class="mo-item-info">'+web_message +
                    mo_content_time +
                    mo_content_reg +
                    mo_content_res_price +
                    mo_content_val_price +
                    mo_content_max_price +
                    mo_content_end
                );
            }
        }

        /*** BACK-TO-TOP ***/
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');

        /*** HANDLE SIDEBAR MENU ***/
        $(".sidebar-submenu").hide();
        $('.sidebar-nav').find('a').removeClass('active');
        $('.sidebar-nav').find('a').each(function() {
            if(pathname.indexOf("/home") != -1 && $(this).attr('href').indexOf("/customers") != -1) {
                $(this).addClass('active');
            } else {
                if($(this).attr('href') == url_location) {
                    $(this).addClass('active');
                } else if(pathname.indexOf("customer") != -1 && $(this).attr('href').indexOf("/customers") != -1) {
                    $(this).addClass('active');
                }
            }
        });
        $(".sidebar-submenu").siblings().closest('a').append('<span onclick="return false;" class="mo-icon-swipe glyphicon glyphicon-menu-down float-right"></span>');

        if (pathname.indexOf("/valuations") != -1
            || pathname.indexOf("/users") != -1
            || pathname.indexOf("/mo-admin") != -1
            || pathname.indexOf("/dealership") != -1
            || pathname.indexOf("/external-offer") != -1
            || pathname.indexOf("/trading") != -1
            || pathname.indexOf("/dispute") != -1
            || pathname.indexOf("/transaction") != -1
            || pathname.indexOf("/test-drives")!= -1)
        {
            var active_child = $(".sidebar-submenu").find('.active');
            if(active_child) {
                //active_child.parent().parent().slideDown('slow');
                active_child.parent().parent().show();
                active_child.parent().parent().siblings().closest('a').children().removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
            }
            /** DEALERSHIP SIDEBAR **/
            if(pathname.indexOf("/mo-admin") != -1) {
                /*$("#mo-dealership-sidebar-ul").children().children('a').addClass('mo-highlight');*/
                if(pathname.indexOf("/dealership-list") != -1) {
                    /*$("#mo-dealership-sidebar-ul").children().children('a').removeAttr('href');
                    $("#mo-dealership-sidebar-ul").children().children('a').attr('href', 'javascript:void(0)');*/
                }
            }
        }

        /*** end ***/

        if (pathname.indexOf("/customers") != -1  || pathname.indexOf("/home") != -1) {
            var data_length = null;
            var current_page = 1;
            var page_length = 1;

            /** INITIALIZE EMPTY CUSTOMER LIST **/
            $.constructDataTable = function(destroy, txt_mgs) {
                $('#mo-customers-list').DataTable({
                    "destroy" : destroy,
                    "columnDefs": [
                        {
                            "aTargets": [ 0 ],
                            "visible": false,
                            "searchable": false
                        }
                    ],
                    "language": {
                        "emptyTable": txt_mgs
                    },
                    "paging": false,
                    "info": false,
                    "filter": false,
                    "responsive": true,
                    //initialization params as usual
                    fnInitComplete : function(settings, json) {
                        if ($(this).find('tbody tr').length <= 1) {
                            $(this).find('thead th').hide();
                            //$(this).parent().hide();
                            $('.mo-pagination-wrapper').hide();

                            /*** RESIZE WRAPPER ***/
                            var $header = $('.mo-main-header');
                            var $wrapper = $('#wrapper');
                            var $window = $(window).on('resize', function(){
                                var height = $(this).height() - $header.height();
                                $wrapper.height(height);
                            }).trigger('resize'); //on page load

                            /** vertical align DIV **/
                            $("#mo-customers-list_wrapper").toggleClass('percent-h-100');
                            $("#mo-customers-list").parent().parent().addClass('percent-h-100 vertical-center');
                        }
                    }
                });
            }
            $.constructDataTable(false, "<div>There are no customers to display at this time.<br>Create a customer by clicking the new button on the bottom right or search for a client in the search box.</div>");

            $.initDataTable = function(destroy, query, offset, init_complete) {
                $('#mo-customers-list').DataTable({
                    "destroy" : destroy,
                    "language": {
                        "emptyTable": "<div>There are no customers to display at this time.<br>Create a customer by clicking the new button on the bottom right or search for a client in the search box.</div>"
                    },
                    "paging": false,
                    "columnDefs": [
                        {
                            "aTargets": [ 0 ],
                            "visible": false,
                            "searchable": false
                        }
                    ],

                    "aoColumns": [
                        {className: "never"},
                        {className: "all"},
                        {className: "all"},
                        {className: "desktop tablet-l tablet-p","bSortable": false },
                        {className: "desktop tablet-l tablet-p","bSortable": false },
                        {className: "desktop","bSortable": false},
                        {className: "desktop"},
                        {className: "desktop"},
                        {className: "all","bSortable": false ,
                            "mRender": function (data,type,row,meta) {
                                return '<a href='+base_url+'/customer-details/view/'+row[0]+'><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                            }
                        }
                    ],
                    "info": false,
                    "filter": false,
                    "pageLength": 20,
                    "lengthChange": false,
                    "responsive": true,
                    "ajax" : {
                        "url" : base_url+"/_customers",
                        "type" : "POST",
                        "data" : function (d) {
                            d._token = $("input[name='_token']").val();
                            d.mo_txt_search = query;
                            d.offset = offset;
                        },
                        "error" : function () {
                            $.constructDataTable(true, "<div>There are no customers to display at this time<div>");

                        }

                    },
                    //initialization params as usual
                    fnInitComplete : function(settings, json) {
                        if ($(this).find('tbody tr td').length<=1) {
                            $(this).find('thead th').hide();
                            //$(this).parent().hide();
                            /*$('.dataTables_paginate').hide();*/
                            $('.mo-pagination-wrapper').hide();

                            /*** RESIZE WRAPPER ***/
                            var $header = $('.mo-main-header');
                            var $wrapper = $('#wrapper');
                            var $window = $(window).on('resize', function(){
                                var height = $(this).height() - $header.height();
                                $wrapper.height(height);
                            }).trigger('resize'); //on page load

                            /** vertical align DIV **/
                            $("#mo-customers-list_wrapper").toggleClass('percent-h-100');
                            $("#mo-customers-list").parent().parent().addClass('percent-h-100 vertical-center');
                        }else {
                            $('.mo-pagination-wrapper').show();

                            /** remove WRAPPER height **/
                            $("#wrapper").removeAttr('style');
                        }


                        if (init_complete) {

                            page_length = data_length = Math.ceil(json.count/20);

                            var activeClass = '';
                            for(var i = data_length; i >= 1; i--) {
                                if(i == 1) {
                                    activeClass = 'active';
                                }else{
                                    activeClass = '';
                                }
                                $("<li class='mo-page-number-wrap'><a class='mo-page-number "+activeClass+"' href='#'>"+i+"</a></li>").insertAfter(".mo-page-number-list");
                            }


                            $(".mo-page-number").click(function() {
                                $(".mo-page-number").removeClass("active");

                                var clicked = $(this);
                                clicked.addClass("active");
                                current_page = clicked.html();

                                var offset = (clicked.html() - 1)*20;
                                $.initDataTable(true, $("#mo-txt-search").val(), offset, false);

                            });
                        }
                    }
                });
            }

            $("#mo-btn-search").click(function() {
                $(".mo-page-number-wrap").remove();
                current_page = 1;
                $.initDataTable(true, $("#mo-txt-search").val(), 0, true);
            });

            $("#mo-page-prev").click(function() {
                if (current_page > 1) {
                    $(".mo-page-number").removeClass("active");
                    current_page = parseInt(current_page) - 1;
                    var offset = (current_page - 1)*20;
                    $.initDataTable(true, $("#mo-txt-search").val(), offset, false);
                    //$(".mo-page-number")[0].addClass("active");
                    $($(".mo-page-number")[current_page - 1]).addClass("active");
                }
            });

            $("#mo-page-next").click(function() {
                if (current_page < page_length) {

                    $(".mo-page-number").removeClass("active");
                    current_page = parseInt(current_page) + 1;
                    var offset = (current_page - 1)*20;

                    $.initDataTable(true, $("#mo-txt-search").val(), offset, false);
                    $($(".mo-page-number")[current_page - 1]).addClass("active");
                }
            });

            $("#mo-txt-search").bind("enterKey", function(e){
                $(".mo-page-number-wrap").remove();
                $.initDataTable(true, $("#mo-txt-search").val(), 0, true);
            });

            $("#mo-txt-search").keyup(function(e){
                if(e.keyCode == 13)
                {
                    $(this).trigger("enterKey");
                }
            });

        }

        /** start CREATE TEST DRIVES **/
        else if(pathname.indexOf("/create-test-drives") != -1){

            var date = new Date();
            var month = date.getMonth()+1;
            var day = date.getDate();
            var hour = date.getHours();
            var minute = date.getMinutes();
            var second = date.getSeconds();
            var current_date = date.getFullYear() + '-' +
                ((''+month).length<2 ? '0' : '') + month + '-' +
                ((''+day).length<2 ? '0' : '') + day + " " + hour + ":" + minute + ":" + second ;
            $("#current-date").val(current_date);

            $("#estimated-return-time").datetimepicker({
                format: "yyyy-mm-dd hh:ii:ss",
                todayHighlight: true,
                autoclose: true,
                orientation: "bottom",
                startDate: date

            });
            $(".estimated-return-calendar").click(function(){
                $("#estimated-return-time").focus();
            });

            $("#mo-test-drive-add-vehicle-form").validate({
                rules: {
                    // estimated_return_time: "required",
                    estimated_return_time: {
                        "required": true,
                        greaterThan: "#current-date"
                    },
                    mo_selected_vehicle: "required"
                },
                errorElement: 'div',
                errorClass: "mo-error-wrapper",
                errorPlacement: function(error, element) {
                    if(element.attr("name") == "estimated_return_time"){
                        error.appendTo("#errorToShowMessage_1");
                    }
                    // else if(element.attr("name") == "mo_selected_vehicle"){
                    //     error.appendTo("#errorToShowMessage");
                    // }
                    else{
                        error.insertAfter(element.next());
                    }
                },
                messages: {
                    // estimated_return_time: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
                    estimated_return_time: {
                        "required": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
                        greaterThan: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field has time past</span>"+"</div>"
                    },
                    mo_selected_vehicle: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please select vehicle</span>"+"</div>"
                },
                submitHandler: function(form){
                    form.submit();
                }
            });

            jQuery.validator.addMethod("greaterThan",
            function(value, element, params) {
                if (!/Invalid|NaN/.test(new Date(value))) {
                    return new Date(value) > new Date($(params).val());
                }

                return isNaN(value) && isNaN($(params).val())
                    || (Number(value) > Number($(params).val()));
            },'Must be greater than {0}.');
        }

        else if(pathname.indexOf("/test-drives/complete") != -1) {

            $("#test-drive-complete-date-range").datepicker({
                format: "yyyy-mm-dd",
                todayHighlight: true,
                autoclose: true,
                orientation: "bottom",                
            });
            
            $("#test-drive-complete-to").datepicker({
                format: "yyyy-mm-dd",
                todayHighlight: true,
                autoclose: true,
                orientation: "bottom"
            });
            
            $("#test-drive-complete-to").change(function(){
                var mindate = $("#test-drive-complete-date-range").val();
                var maxdate = $("#test-drive-complete-to").val();
                if(mindate > maxdate) {
                    $("#errorShowMessage").html("<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+"<span>This field greater than date range</span>"+"</div>");
                } else {
                    $("#errorShowMessage").html("");
                }
            });
        }

        else if(pathname.indexOf("/test-drives/term-and-condition") != -1 ){
            
            $("#mo-test-drive-accept-condition").click(function(){
                $("#mo-test-drive-signature-btn").toggleClass("disabled-div");
            });
            $("#mo-test-drive-sign").click(function(e){
                var manual_vehicle = sessionStorage.getItem("manual_vehicle");
                $("#manual-vehicle").val(manual_vehicle);
                /*$("#manual-vehicle").val(JSON.parse(manual_vehicle));*/
                e.preventDefault();
                $.magnificPopup.open({
                    items: {
                        src: "#mo-test-drives-signature-popup"
                    },
                    type: "inline",
                    showCloseBtn: true,
                    closeOnBgClick: false
                });
            });
            $("#signature").jSignature({
                width: 560,
                height: 300,
                lineWidth: 1
            });

            /* test drive term and condition accept */
            $("#mo-test-drives-accept-btn").click(function(){
                if($("#signature").jSignature("getData", "native").length == 0){
                    $("#errorToShow").html("<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'> "+" <span>Please enter your signature</span> "+"</div>");
                }
                else {
                    $(".mo-loading-image").removeClass("hidden");
                    //ajax create test drive here
                    $.ajax({
                        type: "POST",
                        url: base_url+"/test-drives/create-test-drives",
                        //params: estimated_return_time, mo_selected_vehicle, customer_id
                        data: {
                            _token: $("input[name='_token']").val(),
                            vehicleId: $("input[name='vehicle_id']").val(),
                            returnedDate: $("input[name='returned_date']").val(),
                            customerId: $("input[name='customer_id']").val(),
                            manual_vehicle: $("input[name='manual_vehicle']").val()
                        },
                        success: function(res) {
                            var result = $.parseJSON(res);
                            if (typeof (result.testDriveId) != "undefined") {
                                /* upload image signature */

                                // $("#upload-signature").removeClass("hidden");
                                var token = $("input[name='_token']").val();
                                var dataurl = $("#signature").jSignature("getData", "image");
                                var customer_id = $("input[name='customer_id']").val();                 
                                $(document).ajaxStart(function() {
                                    $(".mo-loading-image").removeClass("hidden");
                                    //return false;
                                });
                                $.ajax({
                                    type:'POST',
                                    url: base_url+"/test-drives/upload-signature-photo",
                                    data: {
                                        _token: token,
                                        data_url: dataurl[0]+","+dataurl[1],
                                        test_drive_id: result.testDriveId
                                    },
                                    success:function(res) {
                                        console.log("success /test-drives/upload-signature-photo");
                                        window.location = base_url+"/customer-details/view/"+customer_id+"#test-drive";
                                    },
                                    error: function(err){
                                        console.log(err,"error /test-drives/upload-signature-photo");
                                    },
                                    complete: function() {
                                    }              
                                });
                                //convert data url to image format
                                /*
                                var i = new Image();
                                i.src = "data:" + dataurl[0] + "," + dataurl[1];
                                $(i).appendTo($("#upload-signature"));
                                */
                                /* end upload image signature */
                            }
                        },
                        error: function(err) {

                        },
                        complete: function() {

                        }
                    });
                    //end create test drive
                }
            });
            /* end test drive term and condition accept */

            var magnificPopup = $.magnificPopup.instance;
            $(".mo-cancel-btn").click(function(){
                magnificPopup.close();
                $("#signature").jSignature("clear");
                $("#upload-signature img").remove();
                $("#errorToShow").text("");
            });  
        }
        
        /** end TEST DRIVE **/

        /** start CUSTOMER DETAILS **/
        else if (pathname.indexOf("/customer-details") != -1) {

            /** Create new INTERACTION **/

            $("#date-time").datetimepicker({
                // format: "yyyy-mm-dd hh:mm:ss",
                todayHighlight: true,
                autoclose: true,
                orientation: "bottom"
            });
            
            $(".interaction-calendar").click(function(){
                $("#date-time").focus();
            });

            $("#mo-interaction-popup-form").validate({
                rules: {
                    date_time: "required",
                    type: "required",
                    note: "required"
                },
                errorElement: 'div',
                errorClass: 'mo-error-wrapper',
                errorPlacement: function(error, element){
                    if(element.attr('name') == 'date_time'){
                        error.appendTo("#errorToShow_date_time");
                    }else if(element.attr('name') == 'type'){
                        error.appendTo("#errorToShow_type");
                    }else {
                        error.insertAfter(element);
                    }
                },
                messages: {
                    date_time: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
                    type: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
                    note: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>"
                },
                submitHandler: function(form){
                    form.submit();
                }
            });

            $("#interaction_save_btn").click(function(){
                var contact_id = $("input[name='customer_id']").val();
                var contact_id_input_field = "<input type='hidden' name='contact_id' value='"+contact_id+"'>";
                $("#mo-interaction-popup-form").append(contact_id_input_field);
                $("#mo-interaction-popup-form").submit();
            });

           /** Edit INTERACTION **/ 

            // $("#interaction-edit-date-picker").datepicker({
            //     format: "dd-mm-yyyy",
            //     todayHighlight: true,
            //     orientation: "bottom",
            //     autoclose: true
            // });

            $("#edit-date-time").datetimepicker({
                // format: "yyyy-mm-dd hh:mm:ss",
                todayHighlight: true,
                autoclose: true,
                orientation: "bottom"
            });

            $(".interaction-calendar").click(function(){
                $("#edit-date-time").focus();
            });

            $("#mo-interaction-edit-popup-form").validate({
                rules: {
                    edit_date_time: "required",
                    edit_type: "required",
                    edit_note: "required"
                },
                errorElement: 'div',
                errorClass: 'mo-error-wrapper',
                errorPlacement: function(error, element){
                    if(element.attr('name') == 'edit_date_time'){
                        error.appendTo("#errorToShow_editdatetime");
                    }else{
                        error.insertAfter(element);
                    }
                },
                messages: {
                    edit_date_time: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
                    edit_type: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
                    edit_note: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>"
                }
            });
            
            $("#interaction_update_btn").click(function(){
                $("#mo-interaction-edit-popup-form").submit();
            });

            /* GET DATA-TABLE */

            /** INTERACTION **/
            $('#mo-valuations-list').hide();
            $('#mo-valuations-list').removeClass('hidden').fadeIn('slow');
            /*$('#mo-table-interaction-list').DataTable({
                "paging": true,
                "aoColumns": [
                    { className: "all", "title": "Date" },
                    { className: "desktop", "title": "Comments" },
                    { className: "all", "mRender": function (data,type,row,meta) {
                        return '<a href="/customer-details/view/'+row[0]+'"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                    }
                    }
                ],
                "columnDefs": [
                    { "orderable": false, "targets": [1,2] }
                ],
                "info": false,
                "filter": false,
                "pageLength": 25,
                "lengthChange": false,
                "responsive": true,
                "language": {
                    "emptyTable": "There are no interaction to display at this time.<br> Add an interaction by clicking the NEW icon on the bottom right."
                },
                //initialization params as usual
                fnInitComplete : function() {
                    //$(this).removeClass('hidden').fadeIn('slow');
                    /!*$('#mo-interaction-list').hide();
                    $('#mo-interaction-list').removeClass('hidden').fadeIn('slow');*!/
                    if ($(this).find('tbody tr td').length<=1) {
                        $(this).find('thead th').hide();
                        //$(this).parent().hide();
                        $('.dataTables_paginate').hide();
                    }
                }
                /!*"ajax" : "_customers",*!/
            });*/
            
            /** start TEST DRIVES **/
            
            /** TEST DRIVES **/
            // $('#mo-test-drives-list').hide();
            // $('#mo-table-test-drives-list').DataTable({
            //     "paging": true,
            //     "aoColumns": [
            //         { className: "all", "title": "Date" },
            //         { className: "desktop", "title": "Vehicles" },
            //         { className: "all", "title": "Function",
            //             "mRender": function (data,type,row,meta) {
            //                 return  '<input type="button" class="mo-email-btn text-capitalize" value="email">' +
            //                     '<input type="button" class="mo-print-btn text-capitalize" value="print">';
            //             }
            //         },
            //         {   "mRender": function (data,type,row,meta) {
            //             return '<a href="/customer-details/view/'+row[0]+'"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
            //         }
            //         }
            //     ],
            //     "columnDefs": [
            //         { "orderable": false, "targets": [1,2,3] }
            //     ],
            //     "info": false,
            //     "filter": false,
            //     "pageLength": 25,
            //     "lengthChange": false,
            //     "responsive": true,
            //     "language": {
            //         "emptyTable": "There are no test drives to display at this time.<br> Add new test drives by clicking the NEW icon on the bottom right."
            //     },
            //     //initialization params as usual
            //     fnInitComplete : function() {
            //         /*$('#mo-test-drives-list').removeClass('hidden').fadeIn('slow');*/
            //         if ($(this).find('tbody tr td').length<=1) {
            //             $(this).find('thead th').hide();
            //             $('.dataTables_paginate').hide();
            //         }
            //     }
                /*"ajax" : "_customers",*/
            // });

            /** VALUATIONS **/
            /*$('#mo-table-valuations-list').DataTable({
                "paging": true,
                "aoColumns": [
                    { className: "all", "title": "Date" },
                    { className: "desktop", "title": "Vehicles" },
                    { className: "desktop", "title": "Price" },
                    { className: "all", "title": "Status",
                        "mRender": function (data,type,row,meta) {
                            return  '<input type="button" class="mo-pending-btn text-capitalize" value="pending">' *//*+
                             '<input type="button" class="mo-requested-btn text-capitalize" value="requested">'+
                             '<input type="button" class="mo-complete-btn text-capitalize" value="complete">'*//*;
                        }
                    },
                    {   "mRender": function (data,type,row,meta) {
                        return '<a href="/customer-details/view/'+row[0]+'"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                    }
                    }
                ],
                "columnDefs": [
                    { "orderable": false, "targets": [1,2,3,4] }
                ],
                "info": false,
                "filter": false,
                "pageLength": 25,
                "lengthChange": false,
                "responsive": true,
                "language": {
                    "emptyTable": "There are no interaction to display at this time.<br> Add an interaction by clicking the NEW icon on the bottom right."
                },
                //initialization params as usual
                fnInitComplete : function() {
                    *//*$('#mo-valuations-list').removeClass('hidden').fadeIn('slow');*//*
                    if ($(this).find('tbody tr td').length<=1) {
                        $(this).find('thead th').hide();
                        $('.dataTables_paginate').hide();
                    }
                }
                *//*"ajax" : "_customers",*//*
            });*/

            /* Click moto 3 Blocks */
            if (hash == "") {
                $('.mo-block').first().toggleClass('mo-block-active');
            }
            else if (hash == "#interaction") {
                $('.mo-block-list').hide();
                $('.interaction-block').toggleClass('mo-block-active');
                $('#mo-interaction-list').hide();
                $('#mo-interaction-list').removeClass('hidden').fadeIn('slow');
            }
            else if (hash == "#test-drive"){
                $('.mo-block-list').hide();
                $('.test-drive-block').toggleClass('mo-block-active');
                $('#mo-test-drives-list').hide();
                $('#mo-test-drives-list').removeClass('hidden').fadeIn('slow');
            };
            
            $('.mo-block').click(function(){
                $('.mo-block').removeClass('mo-block-active');
                $(this).toggleClass('mo-block-active');

                var mo_txt = $(this).find('.mo-block-txt').text();
                /*$('.mo-block-heading').text(mo_txt).fadeIn("slow");*/
                /*$('.mo-block-heading').fadeOut(function() {
                 $(this).text(mo_txt)
                 }).fadeIn();*/

                $('.mo-block-list').hide();

                if(mo_txt == 'interaction') {
                    $('#mo-interaction-list').hide();
                    $('#mo-interaction-list').removeClass('hidden').fadeIn('slow');
                } else if(mo_txt == 'test drives') {
                    $('#mo-test-drives-list').hide();
                    $('#mo-test-drives-list').removeClass('hidden').fadeIn('slow');
                } else if(mo_txt == 'valuations') {
                    $('#mo-valuations-list').hide();
                    $('#mo-valuations-list').removeClass('hidden').fadeIn('slow');
                }
            });

            /* Popup */
            var magnificPopup = $.magnificPopup.instance;
            $('.open-popup-link').magnificPopup({
                type:'inline',
                showCloseBtn: false,
                closeOnBgClick: false
            });

            $('.mo-cancel-btn').click(function (){
                magnificPopup.close();
            });
        }
        /** end CUSTOMER DETAILS **/

        /** start VALIDATION **/
        else if (pathname.indexOf("/create-customer") != -1) {

            /** AUTO FILL STATE DEPEND ON SELECTED COUNTRY **/
            // $(document).ready(function(){
            //
            // });

            var country_list = jQuery.parseJSON($("#country-list").val());
            $("#state option[value]").remove();
            $.each(country_list, function(index, value) {

                $.each(value.stateList, function(index2, value2) {

                    if (parseInt($("#country").val()) == parseInt(value2.country_id)) {

                        $("#state").append($("<option>", {
                            value: value2.id,
                            text: value2.name
                        }));
                    }

                });
            });
            $("#input-country-id").val($("#country").val());
            
            // $("#country").on("change", function() {
            //
            //     var country_list = jQuery.parseJSON($("#country-list").val());
            //     $("#state option[value]").remove();
            //     $.each(country_list, function(index, value) {
            //
            //         $.each(value.stateList, function(index2, value2) {
            //
            //             if (parseInt($("#country").val()) == parseInt(value2.country_id)) {
            //
            //                 $("#state").append($("<option>", {
            //                     value: value2.id,
            //                     text: value2.name
            //                 }));
            //             }
            //
            //         });
            //     });
            //
            // });
            /** END AUTO FILL STATE DEPEND ON SELECTED COUNTRY **/

            /** AUTO FILL GENDER **/
            $("#title").on("change", function(){
                switch ($("#title").val()) {
                    case "1":
                        //male
                        $("#gender").val(1);
                        break;
                    case "2":    
                    case "3":
                    case "4":
                    case "6":
                        //female
                        $("#gender").val(2);
                        break;

                }
            });
            /** END AUTO FILL GENDER **/

            $(function() {
                $('#create-customer-form').validate({
                    rules:{
                        first_name: "required",
                        // last_name: "required",
                        address1: "required",
                        suburb: "required",
                        country: "required",
                        state: "required",
                        mobile: "required",
                        privacy: "required",
                        email:{
                            required:true,
                            email: true
                        }
                    },
                    messages:{
                        first_name: "<div class='mo-error-wrapper'><img src='images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                        // last_name: "<div class='mo-error-wrapper'><img src='images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                        company: "<div class='mo-error-wrapper'><img src='images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                        address1: "<div class='mo-error-wrapper'><img src='images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                        suburb: "<div class='mo-error-wrapper'><img src='images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                        country: "<div class='mo-error-wrapper'><img src='images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                        state: "<div class='mo-error-wrapper'><img src='images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                        mobile: "<div class='mo-error-wrapper'><img src='images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                        privacy: "<div class='mo-error-wrapper'><img src='images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",                        
                        email: {
                            required: "<div class='mo-error-wrapper'><img src='images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                            email: "<div class='mo-error-wrapper'><img src='images/svg/main/ic-warning.svg'>"+ "<span>Not a valid email address</span>"+"</div>"      
                        }
                    },
                    submitHandler: function(form){
                        form.submit();
                    }
                });
                $("#mobile").keyup(function(){
                    var $this = $(this);
                    $this.val($this.val().replace(/[^\d+]/g, ''));
                });
            }(jQuery));

            $("#mo-button-create-customer").click(function(){
                if(!$("#create-customer-form").valid()){
                    return false;
                }
                $(".mo-loading-image").removeClass("hidden");
            });

        }

        /** PERFORM VALUATION **/
        else if (pathname.indexOf("/exterior-perform-valuation") != -1) {
            /* Popup */
            var magnificPopup = $.magnificPopup.instance;
            $('.open-popup-link').magnificPopup({
                type:'inline',
                showCloseBtn: false,
                closeOnBgClick: false
            });

            $('.mo-cancel-btn').click(function (){
                magnificPopup.close();
            });

            var current_element = null;
            $("#fileToUpload").on("change", function() {
                if($("#fileToUpload").val())
                {
                    $("#mo-exterior-popup-form").submit();
                }
            });

            $("#exterior-edit-button").click(function() {

                var token = $("input[name='_token']").val();
                var estimated_cost = 0;
                if($("#estimate-cost").val() != null && $("#estimate-cost").val() != ''){
                    estimated_cost = $("#estimate-cost").val();
                }

                $.ajax({
                    type:'POST',
                    url: base_url+"/update-inspection-item",
                    headers: {'X-CSRF-TOKEN': token},
                    data: {"valuation_id" : $("#valuation_id").val(), "inspection_item_id" : inspection_item_id, "repair_cost" : estimated_cost,
                            "note" : $("#note").val()},
                    //cache:false,
                    //contentType: false,
                    //processData: false,
                    success:function(data) {
                        magnificPopup.close();
                    },
                    error: function(data){
                        console.log("error");
                        console.log(data);
                    }

                });

            });

            $("#mo-exterior-popup-form").on("submit",(function(e) {
                e.preventDefault();
                var formData = new FormData(this);

                current_element.children().attr("src", "/images/perform_valuation/img-loading.gif");

                $.ajax({
                    type:'POST',
                    url: base_url+"/upload-image",
                    data:formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success:function(data) {
                        console.log("success");
                        console.log(data);

                        current_element.children().attr("src", data);
                    },
                    error: function(data){
                        console.log("error");
                        console.log(data);
                    }

                });

            }));            

            $("label[for='fileToUpload']").on("click", function () {
                current_element = $(this);
                var sequence_number = null;
                if (current_element.parent().attr("id") == "mo-image-box-1") {
                    sequence_number = 1;
                } else if (current_element.parent().attr("id") == "mo-image-box-2") {
                    sequence_number = 2;
                } else if (current_element.parent().attr("id") == "mo-image-box-3") {
                    sequence_number = 3;
                }
                $("input[name='sequence']").val(sequence_number);
            });

            var inspection_item_id = null;
            $(".cost-estimate-row").on("click", function () {
                var valuation_id = $("#valuation_id").val();
                inspection_item_id = $(this).attr("id");

                //set images to images box
                var image_upload = base_url+"/images/perform_valuation/image_upload.png";
                var azure_account_name = $("input[name='azure-accout-name']").val();
                var valuation_current_version = $("input[name='valuation_version']").val();
                var image_1 = "https://"+azure_account_name+".blob.core.windows.net/mo-exterior/"+valuation_id+"_"+valuation_current_version+"_"+inspection_item_id+"_1.jpg?"+Math.random();
                var image_2 = "https://"+azure_account_name+".blob.core.windows.net/mo-exterior/"+valuation_id+"_"+valuation_current_version+"_"+inspection_item_id+"_2.jpg?"+Math.random();
                var image_3 = "https://"+azure_account_name+".blob.core.windows.net/mo-exterior/"+valuation_id+"_"+valuation_current_version+"_"+inspection_item_id+"_3.jpg?"+Math.random();
                $("#1").children().children().error(function() {
                    $("#1").children().children().attr("src", image_upload);
                }).attr("src", image_1);

                $("#2").children().children().error(function() {
                    $("#2").children().children().attr("src", image_upload);
                }).attr("src", image_2);

                $("#3").children().children().error(function() {
                    $("#3").children().children().attr("src", image_upload);
                }).attr("src", image_3);

                var item_detail = null;

                $.ajax({
                    type: "GET",
                    url: base_url+"/_get-inspection-item-detail/"+valuation_id+"/"+inspection_item_id,
                    data: null,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if(data != null) {
                            item_detail = JSON.parse(data);
                        }
                        //console.log(item_detail);
                        var cost = item_detail["repair_cost"];
                        var note = item_detail["note"];


                        $("#estimate-cost").val(cost);
                        $("#note").val(note);

                        $.each(item_detail["itemImage"], function(index, value) {

                        });

                        $.each(exterior_data, function(index, value) {
                                //console.log(value);
                                $.each(value["items"], function(index2, value2) {
                                    
                                    if (value2["id"] == inspection_item_id) {
                                        //console.log(value["items"]);

                                        $("#exterior-section").val(value2["section_type"]);

                                        $.each(value["items"], function(index3, value3) {
                                            $("#exterior-items").append($("<option>", {
                                                value: value3["id"],
                                                text: value3["item"]
                                            }));
                                        });

                                        $("#exterior-items").val(inspection_item_id);

                                        return false;
                                    }
                                });
                        });

                    },
                    error: function (data) {

                    }
                });

            }); 

            var exterior_data = null;
            $(function() {

                $.ajax({
                    type: "GET",
                    url: base_url+"/get-inspection-items-list/EXTERIOR_SECTION",
                    data: null,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        
                        exterior_data = JSON.parse(data);
                        //console.log(exterior_data);
                        $.each(exterior_data, function (index, value) {
                            //console.log(index + ':' + value["section_name"]);
                            $("#exterior-section").append($("<option>", {
                                value: value["section_id"],
                                text: value["section_name"]
                            }));
                        });
                    },
                    error: function (data) {

                    }
                });

            });

            $("#exterior-section").on("change", function() {
                var section_id = $("#exterior-section").val();
                $.each(exterior_data, function (index, value) {

                    if (value["section_id"] == section_id) {
                        $("#exterior-items option[value]").remove(); 
                        $.each(value["items"], function (index2, value2) {
                            $("#exterior-items").append($("<option>", {
                                value: value2["id"],
                                text: value2["item"]
                            }));
                        });
                        return false;
                    }

                });

            });

            $("#exterior-items").on("change", function() {
                var item_id = $("#exterior-items").val();
                
                //set images to images box
                var image_upload = base_url+"/images/perform_valuation/image_upload.png";
                var azure_account_name = $("input[name='azure-accout-name']").val();
                var image_1 = "https://"+azure_account_name+".blob.core.windows.net/mo-exterior/"+$("#valuation_id").val()+"_"+item_id+"_1.jpg?"+Math.random();
                var image_2 = "https://"+azure_account_name+"mo.blob.core.windows.net/mo-exterior/"+$("#valuation_id").val()+"_"+item_id+"_2.jpg?"+Math.random();
                var image_3 = "https://"+azure_account_name+".blob.core.windows.net/mo-exterior/"+$("#valuation_id").val()+"_"+item_id+"_3.jpg?"+Math.random();
                $("#1").children().children().error(function() {
                    $("#1").children().children().attr("src", image_upload);
                }).attr("src", image_1);

                $("#2").children().children().error(function() {
                    $("#2").children().children().attr("src", image_upload);
                }).attr("src", image_2);

                $("#3").children().children().error(function() {
                    $("#3").children().children().attr("src", image_upload);
                }).attr("src", image_3);

            });
        }
        /** end VALIDATION **/

        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

        //$(".sidebar-submenu").hide(); //Hide children by default
        //$(".sidebar-submenu").siblings().closest('a').append("<span class='mo-icon-swipe'><img src='images/svg/main/icon-swipe-bottom-white.svg'/></span>");
        
        //$(".sidebar-nav").children().click(function(){
        //    $(this).children('.sidebar-submenu').slideToggle('slow');
        //    var glyphion_menu = $(this).children('a').find('.mo-icon-swipe');
        //    if (glyphion_menu.hasClass('glyphicon-menu-down')) {
        //        $(this).children('a').find('.mo-icon-swipe').removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
        //    } else {
        //        $(this).children('a').find('.mo-icon-swipe').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
        //    }
        //});

        $(".sidebar-nav a").click(function(){
            $(this).next('.sidebar-submenu').slideToggle('slow');
            var glyphion_menu = $(this).find('.mo-icon-swipe');
            if (glyphion_menu.hasClass('glyphicon-menu-down')) {
                $(this).find('.mo-icon-swipe').removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
            } else {
                $(this).find('.mo-icon-swipe').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
            }
        });

        // Start EDIT CUSTOMER VALIDATION
        if(pathname.indexOf("/edit-customer") != -1){

            /** AUTO FILL STATE DEPEND ON SELECTED COUNTRY **/
            $("#country").on("change", function() {
                var country_list = jQuery.parseJSON($("#country-list").val());
                $("#state option[value]").remove();
                $.each(country_list, function(index, value) {

                    $.each(value.stateList, function(index2, value2) {

                        if (parseInt($("#country").val()) == parseInt(value2.country_id)) {

                            $("#state").append($("<option>", {
                                value: value2.id,
                                text: value2.name
                            }));
                        }

                    });
                });
            });
            $("#input-country-id").val($("#country").val());

            /** END AUTO FILL STATE DEPEND ON SELECTED COUNTRY **/

            $(function() {
                $('#create-customer-form').validate({
                    rules:{
                        first_name: "required",
                        // last_name: "required",
                        suburb: "required",
                        address1: "required",
                        country: "required",
                        state: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        email_alternate: {
                            email: true
                        },
                        mobile_phone: "required"
                    },
                    messages:{

                        first_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                        // last_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                        suburb: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                        address1: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                        country: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                        state: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                        email:  {
                            required: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                            email: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Not a valide email address</span>"+"</div>"                            
                        },
                        email_alternate: {
                            email: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Not a valide email address</span>"+"</div>"
                        },
                        mobile_phone: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>"
                    },
                    submitHandler: function(form){
                        form.submit();
                    }
                });
                $("#mobile").keyup(function(){
                    var $this = $(this);
                    $this.val($this.val().replace(/[^\d+]/g, ''));
                });
                $("#home-phone").keyup(function(){
                    var $this = $(this);
                    $this.val($this.val().replace(/[^\d+]/g, ''));
                });
                $("#work-phone").keyup(function(){
                    var $this = $(this);
                    $this.val($this.val().replace(/[^\d+]/g, ''));
                });

            }(jQuery));
            $("#mo-button-edit-customer").click(function () {
                if(!$("#create-customer-form").valid()){
                    return false;
                }
                $(".mo-loading-image").removeClass("hidden");
            })

        }

        else if(pathname.indexOf("/create-location") != -1 && pathname.indexOf("/mo-admin") == -1){
            /*$("#mo-create-location-form").validate({
                rules: {
                    dealer_location_name: "required",
                    dealer_location_code: "required",
                    dealer_location_lmct: "required", 
                    dealer_location_address: "required", 
                    dealer_location_suburb: "required",
                    dealer_location_country: "required", 
                    dealer_location_state: "required"
                },
                highlight: function(element) {
                    $(element).closest('.input-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.input-group').removeClass('has-error');
                },
                messages:{
                    dealer_location_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location name</span>"+"</div>",
                    dealer_location_code: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location code</span>"+"</div>",
                    dealer_location_lmct: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location lmct</span>"+"</div>",
                    dealer_location_address: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location address</span>"+"</div>",
                    dealer_location_suburb: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location suburb</span>"+"</div>",
                    dealer_location_country: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location country</span>"+"</div>",
                    dealer_location_state: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location state</span>"+"</div>"
                },
                submitHandler: function(form){
                    form.submit();
                }
            });
            // $("#dealer-location-code").keyup(function () {
            //     this.value = this.value.replace(/[^0-9\.]/g,'');
            // });

            // AUTO FILL STATE DEPEND ON COUNTRY
            $(document).ready(function(){
                var country_list = jQuery.parseJSON($("#country-list").val());
                $("#dealer-location-state option[value]").remove();
                $.each(country_list, function(index, value) {

                    $.each(value.stateList, function(index2, value2) {
                        
                        if (parseInt($("#dealer-location-country").val()) == parseInt(value2.country_id)) {
                            if(parseInt($("input[name='selected-state']").val()) == parseInt(value2.id))
                            {
                                $("#dealer-location-state").append($("<option>", {
                                    value: value2.id,
                                    text: value2.name,
                                    selected: true
                                }));    
                            }else{
                                $("#dealer-location-state").append($("<option>", {
                                    value: value2.id,
                                    text: value2.name
                                }));    
                            }

                        }

                    });
                });
            });
            
            $("#dealer-location-country").on("change", function() {

                var country_list = jQuery.parseJSON($("#country-list").val());
                $("#dealer-location-state option[value]").remove();
                $.each(country_list, function(index, value) {

                    $.each(value.stateList, function(index2, value2) {
                        
                        if (parseInt($("#dealer-location-country").val()) == parseInt(value2.country_id)) {
                            
                            $("#dealer-location-state").append($("<option>", {
                                value: value2.id,
                                text: value2.name
                            }));
                        }

                    });
                });
            });

            // END AUTO FILL STATE */
        }

            else if (pathname.indexOf("/mo-admin/create-dealership/new") != -1){
            $("#mo-create-dealership-form").validate({
                rules:{
                    dealer_group: "required",
                    dealer_name: "required",
                    dealer_address: "required",
                    dealer_suburb: "required",
                    dealer_country: "required",
                    dealer_state: "required",
                    first_name: "required",
                    last_name: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    mobile: "required",
                    privacy: "required"
                },
                highlight: function(element) {
                    $(element).closest('.input-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.input-group').removeClass('has-error');
                },
                messages: {
                    dealer_group: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer group</span>"+"</div>",
                    dealer_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter name</span>"+"</div>",
                    dealer_address: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter address</span>"+"</div>",
                    dealer_suburb: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter suburb</span>"+"</div>",
                    dealer_country: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter country</span>"+"</div>",
                    dealer_state: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter state</span>"+"</div>",

                    first_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter first name</span>"+"</div>",
                    last_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter last name</span>"+"</div>",
                    email:{
                        required: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter email</span>"+"</div>",
                        email: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter valid email</span>"+"</div>"
                    },
                    mobile: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter mobile number</span>"+"</div>",
                    privacy: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter privacy</span>"+"</div>"
                },
                submitHandler: function(form){
                    $(document).ajaxStart(function() {
                        $(".mo-loading-image").removeClass("hidden");
                    });
                    $.ajax({
                        type: "POST",
                        url: base_url+"/mo-admin/create-dealership",
                        data: $("#mo-create-dealership-form").serialize(),
                        dataType: 'json',
                        success: function (data) {
                            console.log('data:');
                            console.log(data);
                            if(data == 1){
                                window.location.href = base_url+"/mo-admin/dealership-list";
                            }else{
                                $(".alert .mo-error-message").remove();
                                $(".alert br").remove();
                                data.forEach(function(value, index) {
                                    var warning_message = '<span class="mo-error-message text-capitalize">'+ value +'</span><br>';
                                    $(".alert-danger").append(warning_message);
                                });

                                $(".alert-danger").removeClass("hidden");
                                $("html, body").animate({
                                    scrollTop: 0
                                }, 500);
                            }
                        },
                        error: function (data) {
                            //console.log(data);
                        },
                        complete: function(){
                            $('.mo-loading-image').addClass("hidden");
                        }
                    });
                }
            });

                $("#mobile").keyup(function(){
                    var $this = $(this);
                    $this.val($this.val().replace(/[^\d+]/g, ''));
                });
            
        }
        else if (pathname.indexOf("/create-dealership/edit") != -1 && pathname.indexOf("mo-admin") == -1){
            $("#mo-create-dealership-form").removeAttr("action").attr("action", base_url+"/mo-admin/update-dealership/"+$("#mo-dealership-id").val());
            $("#mo-create-dealership-form").removeAttr("id").attr("id", "mo-edit-dealership-form");
            $("#mo-dealership-sidebar-ul").children().children('a').removeClass('active mo-highlight');

            $("#mo-edit-dealership-form").validate({
                rules: {
                    dealer_group: "required",
                    dealer_name: "required",
                    dealer_address: "required"
                },
                highlight: function(element) {
                    $(element).closest('.input-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.input-group').removeClass('has-error');
                },
                messages: {
                    dealer_group: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter dealer group</span>" + "</div>",
                    dealer_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter name</span>" + "</div>",
                    dealer_address: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter address</span>" + "</div>"
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        }

        /* Validate for create company group */

        else if(pathname.indexOf("/create-dealer-group/new") != -1){
            $("#mo-create-company-group-form").validate({
                rules: {
                    dealer_group_name: "required"
                },
                messages:{
                    dealer_group_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter dealer group name</span>" + "</div>"
                },
                submitHandler: function(form){
                    form.submit();
                }
            });
            $("#dealer-group-create").click(function(){
                if(!$("#mo-create-company-group-form").valid()){
                    return false;
                }
                $("#mo-create-company-group-form").on("submit", function(){
                    $(".mo-loading-image").removeClass("hidden");
                });
            });
        }
        else if(pathname.indexOf("/create-dealer-group/edit")!= -1){
            $("#mo-create-company-group-form").validate({
                rules: {
                    dealer_group_name: "required"
                },
                messages:{
                    dealer_group_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter dealer group name</span>" + "</div>"
                },
                submitHandler: function(form){
                    form.submit();
                }
            });

            $("#mo-create-company-group-form").removeAttr("action").attr("action", base_url+"/mo-admin/update-dealer-group/"+$("#mo-company-group-id").val());
            $("#mo-create-company-group-form").removeAttr("id").attr("id", "mo-edit-company-group-form");
            $("#dealer-group-update").click(function(){
               if(!$("#mo-edit-company-group-form").valid()){
                   return false;
               }
               $("#mo-edit-company-group-form").on("submit", function(){
                   $(".mo-loading-image").removeClass("hidden");
               });
            });
        }

        /** end REVIEW VALUATION **/

        if (pathname.indexOf("/mo-admin/create-dealership") != -1){

            $("#mo-create-dealership-form").validate({
                rules:{
                    dealer_group: "required",
                    dealer_name: "required",
                    dealer_address: "required",
                    dealer_suburb: "required",
                    dealer_country: "required",
                    dealer_state: "required",
                    first_name: "required",
                    last_name: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    mobile: "required",
                    privacy: "required"
                },
                highlight: function(element) {
                    $(element).closest('.input-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.input-group').removeClass('has-error');
                },
                messages: {
                    dealer_group: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer group</span>"+"</div>",
                    dealer_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter name</span>"+"</div>",
                    dealer_address: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter address</span>"+"</div>",
                    dealer_suburb: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter suburb</span>"+"</div>",
                    dealer_country: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter country</span>"+"</div>",
                    dealer_state: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter state</span>"+"</div>",

                    first_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter first name</span>"+"</div>",
                    last_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter last name</span>"+"</div>",
                    email:{
                        required: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter email</span>"+"</div>",
                        email: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter valid email</span>"+"</div>"
                    },
                    mobile: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter mobile number</span>"+"</div>",
                    privacy: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter privacy</span>"+"</div>"
                },
                submitHandler: function(form){
                    $(document).ajaxStart(function() {
                        $(".mo-loading-image").removeClass("hidden");
                    });
                    var moDealershipId = $('#mo-dealership-id').val();
                    $.ajax({
                        type: "POST",
                        url: base_url+"/mo-admin/update-dealership/" + moDealershipId,
                        data: $("#mo-create-dealership-form").serialize(),
                        dataType: 'json',
                        success: function (data) {
                            console.log('data:');
                            console.log(data);
                            if(data == 1){
                                window.location.href = base_url+"/mo-admin/dealership-list";
                            }else{
                                $(".alert .mo-error-message").remove();
                                $(".alert br").remove();
                                data.forEach(function(value, index) {
                                    var warning_message = '<span class="mo-error-message text-capitalize">'+ value +'</span><br>';
                                    $(".alert-danger").append(warning_message);
                                });

                                $(".alert-danger").removeClass("hidden");
                                $("html, body").animate({
                                    scrollTop: 0
                                }, 500);
                            }
                        },
                        error: function (data) {
                            //console.log(data);
                        },
                        complete: function(){
                            $('.mo-loading-image').addClass("hidden");
                        }
                    });
                }
            });

            $("#mobile").keyup(function(){
                var $this = $(this);
                $this.val($this.val().replace(/[^\d+]/g, ''));
            });

            var state = $('#dealer-state-data').val();
            var country_list = jQuery.parseJSON($("#country-list").val());

            $("#dealer-state option[value]").remove();
            $.each(country_list, function(index, value) {
                $.each(value.stateList, function(index2, value2) {
                    if (parseInt($("#dealer-country").val()) == parseInt(value2.country_id)) {
                        if (state == value2.id) {
                            $("#dealer-state").append($("<option>", {
                                value: value2.id,
                                text: value2.name,
                                selected: true
                            }));
                        } else {
                            $("#dealer-state").append($("<option>", {
                                value: value2.id,
                                text: value2.name
                            }));
                        }


                    }
                });
            });

            $("#input-country-id").val($("#dealer-country").val());
            $("#dealer-country").on("change", function() {
                var country_list = jQuery.parseJSON($("#country-list").val());

                $("#dealer-state option[value]").remove();
                $.each(country_list, function(index, value) {
                    $.each(value.stateList, function(index2, value2) {
                        if (parseInt($("#dealer-country").val()) == parseInt(value2.country_id)) {
                            $("#dealer-state").append($("<option>", {
                                value: value2.id,
                                text: value2.name
                            }));

                        }
                    });
                });

                $("#dealer-state").val($("input[name='dealer-state-data']").val());
            });

            // Show up arrow
            $('#menu_members')
                .find('.glyphicon-menu-down')
                .removeClass('glyphicon-menu-down')
                .addClass('glyphicon-menu-up');

            // Set active menu
            $('#menu_members')
                .next('.sidebar-submenu')
                .show()
                .find('#menu_dealerships')
                .addClass('active');
        }
        else if (pathname.indexOf("/mo-admin/create-dealer-group") != -1) {
            // Show up arrow
            $('#menu_members')
                .find('.glyphicon-menu-down')
                .removeClass('glyphicon-menu-down')
                .addClass('glyphicon-menu-up');

            // Set active menu
            $('#menu_members')
                .next('.sidebar-submenu')
                .show()
                .find('#menu_dealer_groups')
                .addClass('active');
        }

        /*** start MO-ADMIN - LOCATION ***/
        /*else if(pathname.indexOf("/mo-admin/create-location") != -1 ){
            $.getScript("/js/mo-admin/location/createLocation.js");
        }*/

        /*** start DEALERSHIP - SETTING ***/
        else if (pathname.indexOf("/dealership/settings") != -1){
            $("#mo-submit-dealership-setting").click(function (e){
                e.preventDefault();
                $(document).ajaxStart(function() {
                    $(".mo-loading-image").removeClass("hidden");
                });
                var token = $("input[name='_token']").val();
                var head_valuer_approval = 0;
                var receive_external_offers = 0;
                if ($('#head-valuer-approval').is(":checked")){var head_valuer_approval = 1;}
                if ($('#receive-external-offers').is(":checked")){var receive_external_offers = 1;}
                $.ajax({
                    type: "POST",
                    url: base_url+"/dealership/save-settings",
                    data: {
                        _token: token,
                        head_valuer_approval: head_valuer_approval,
                        receive_external_offers: receive_external_offers,
                        test_drive_terms: $('#test-drive-terms-conditions').val()
                    },
                    success: function (data) {
                        if(data == 1){
                            $(".alert-success").removeClass("hidden");
                        }
                        else if(data == 0){
                            $(".alert-danger").removeClass("hidden");
                        }
                        else if(data == "access_denied"){
                            $(".alert-danger.alert-access-denied").removeClass("hidden");
                        }
                    },
                    error: function (data) {
                        //console.log(data);
                    },
                    complete: function(){
                        $('.mo-loading-image').addClass("hidden");
                    }
                });
            });
        }
        /*** end DEALERSHIP - SETTING ***/

        /** USER LISTS **/
        else if (pathname.indexOf("dealership/users-list") != -1) {
            $('.mo-block').first().toggleClass('mo-block-active');
            $('#mo-user-list').hide();
            $('#mo-user-list').removeClass('hidden').fadeIn('slow');

            /* Click moto Users 4 Blocks */
            $('.mo-block').click(function(){
                $('.mo-block').removeClass('mo-block-active');
                $(this).toggleClass('mo-block-active');

                var mo_txt = $(this).find('.mo-block-txt').text().toLowerCase();
                $('.mo-block-list').hide();

                var active = false;
                if ($("#mo-filter-user").is(":checked")) {
                    active = true;
                } else {
                    active = false;
                }

                if(mo_txt == 'all') {
                    $('#mo-role-id').val('');
                    reinitializeUserTable(active);
                } else if(mo_txt == 'super admins') {
                    $('#mo-role-id').val(7);
                    reinitializeUserTable(active);
                } else if(mo_txt == 'admins') {
                    $('#mo-role-id').val(6);
                    reinitializeUserTable(active);
                } else if(mo_txt == 'users') {
                    $('#mo-role-id').val('remaining');
                    reinitializeUserTable(active);
                }

                function reinitializeUserTable(active){
                    var destroy = true;
                    var init = true;
                    var query = null;
                    var offset = 0;
                    $.getUsers(destroy, active, init, query, offset);
                    $('#mo-user-list').hide();
                    $('#mo-user-list').removeClass('hidden').fadeIn('slow');
                }
            });
        }

        /* ADD NEW ROLE */
        /*else if(pathname.indexOf("/create-new-role/edit")!= -1){
            $("#mo-create-new-role-form").validate({
                rules: {
                    role_name: "required",
                    "permissions_list[]": { required: true }
                },
                errorElement: 'div',
                errorClass: 'mo-error-wrapper',
                errorPlacement: function(error, element) {
                    if (element.is(":checkbox")) {
                        error.appendTo("#errorToShow");
                    } else {
                        error.insertAfter(element);   // default placement
                    }
                },
                messages:{
                    role_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter role name</span>" + "</div>",
                    "permissions_list[]": {
                        "required": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please select at least one permission</span>" + "</div>"
                    }
                },
                submitHandler: function(form){
                    form.submit();
                }
            });
            $(".mo-permission").shiftcheckbox();

            $("#mo-create-new-role-form").removeAttr("action").attr("action", "/dealership/update-new-role/" + $("#mo-role-id").val());
            $("#mo-create-new-role-form").removeAttr("id").attr("id", "mo-edit-new-user-form");
            $("#mo-dealership-sidebar-ul").children().children('a').removeClass('active mo-highlight');

            $("#dealer-update").click(function(){
                if(!$("#mo-edit-new-user-form").valid()){
                    return false;
                }
                $("#mo-edit-new-user-form").on("submit", function(){
                    $(".mo-loading-image").removeClass("hidden");
                });
            });

        }*/

        /*else if(pathname.indexOf("/dealership/create-new-role/new") != -1){

            $("#mo-create-new-role-form").validate({
                rules: {
                    role_name: "required",
                    "permissions_list[]": { required: true }
                },
                errorElement: 'div',
                errorClass: 'mo-error-wrapper',
                errorPlacement: function(error, element) {
                    if (element.is(":checkbox")) {
                        error.appendTo("#errorToShow");
                    } else {
                        error.insertAfter(element);   // default placement
                    }
                },
                messages: {
                    role_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter role name</span>" + "</div>",
                    // "permissions_list[]": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please choose permission</span>" + "</div>"
                    // "permissions_list[]": $("div.mo-error-message").show()
                    "permissions_list[]": {
                        "required": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please select at least one permission</span>" + "</div>"
                    }
                },
                submitHandler: function(form){
                    form.submit();
                }
                // submitHandler: function(form){
                //     //form.submit();
                //     /!** START AJAX FOR CHECKBOX ROLE **!/
                //
                //     //$("#dealer-save").click(function(e){
                //         //e.preventDefault();
                //         $(document).ajaxStart(function(){
                //             $(".mo-loading-image").removeClass("hidden");
                //         });
                //         var token = $("input[name='_token']").val();
                //         var permission_list = [];
                //         if($(".mo-permission").is(":checked")){permission_list = 1}
                //         $.ajax({
                //             type: "POST",
                //             url: "/dealership/create-new-role",
                //             data: {
                //                 _token: token,
                //                 "permission_list[]": permission_list.join()
                //             },
                //             success: function(data){
                //                 if(data == 1){
                //                     $(".alert-success").removeClass("hidden");
                //                 }
                //             },
                //             error: function(data){
                //
                //             },
                //             complete: function(){
                //                 $(".mo-loading-image").addClass("hidden");
                //             }
                //         });
                //     //});
                // }

            });

            $(".mo-permission").shiftcheckbox();

            $("#dealer-save").click(function(){
                if(!$("#mo-create-new-role-form").valid()){
                    return false;
                }
                $("#mo-create-new-role-form").on("submit", function(){
                    $(".mo-loading-image").removeClass("hidden");
                });
            });

        }*/

        else if(pathname.indexOf("external-offer/incoming-offers") != -1 || pathname.indexOf("external-offer/outgoing-offers") != -1) {
            
            /*** EXTERNAL OFFER FILTER SECTION ***/
            /* filter dropdown */
            $(document).on('click', '.external-offer-filter-drop-down-menu', function (e) {
                e.stopPropagation();
            });

            $(document).ready(function(e){
                $("#filter-odo-range-slider").bootstrapSlider({});
                $("#filter-year-range-slider").bootstrapSlider({});
            });

            $(document).ready(function(){
                /** GET MAKE LIST - BY YEAR **/
                $.ajax({
                    type: 'POST',
                    data: {_token: $("input[name='_token']").val(), make: "ABARTH"},
                    url: base_url+"/get-glasses-data/make",
                    success: function (data) {
                        console.log('success: ' + data);
                    },
                    error: function (data) {
                        console.log('error: ' + data);
                    }
                });
            });

            $("#external-offer-custom-date-range-from").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true
            });

            $("#external-offer-custom-date-range-to").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true
            });

            $(".dropdown-menu-custom").click(function(){
                if(!$(this).hasClass("make")) {
                    $(".dropdown-submenu-custom").removeClass("block");
                }
                $(".dropdown-model-submenu-custom").removeClass("block");
                
                $(this).next().addClass("block");
            });

            $(document).click(function(){
                $(".dropdown-submenu-custom").removeClass("block");
            });

            $("#external-offer-filter-reset-btn").click(function() {
                //uncheck make
                $("input[name='external-offer-filter-make-checkbox[]']:checked").prop("checked", false);
                //uncheck model
                $("input[name='external-offer-filter-model-checkbox[]']:checked").prop("checked", false);
                //uncheck variant
                $("input[name='external-offer-filter-variant-checkbox[]']:checked").prop("checked", false);
                //uncheck location
                $("input[name='filter-location[]']:checked").prop("checked", false);
                //uncheck transmission type
                $("input[name='filter-transmission[]']:checked").prop("checked", false);
                //check all status checkbox
                $("input[name='filter-status[]']").each(function () {
                    $(this).prop("checked", true);
                })
                //check date range
                $("input#filter-custom-range").prop("checked", true);
                //refresh page
                //window.location.reload();

                var external_offer_active_label_tab = $("#mo-blocks-wrapper").children().find(".mo-block-active .mo-block-txt").data("label");

                if (external_offer_active_label_tab == "Offers Won") {
                    $('#mo-offers-won-content').empty();
                    getExterOfferDataByFilter("offer-won", $('#mo-offers-won-content'), base_url+"/external-offer/incoming-offers-by-filters/", null, true);
                } else if (external_offer_active_label_tab == "External Offers"){
                    $('#mo-incoming-offers-content').empty();
                    getExterOfferDataByFilter("incoming-offer", $('#mo-incoming-offers-content'), base_url+"/external-offer/incoming-offers-by-filters/", null, true);
                } else if (external_offer_active_label_tab == "Purchase History") {
                    $('#mo-purchase-history-content').empty();
                    getExterOfferDataByFilter("purchase-history", $('#mo-purchase-history-content'), base_url+"/external-offer/incoming-offers-by-filters/", null, true);
                }

                //out going
                else if (external_offer_active_label_tab == "Offers Received") {
                    $('#mo-offers-received-content').empty();
                    getExterOfferDataByFilter("offer-received", $('#mo-offers-received-content'), base_url+"/external-offer/outgoing-offers-by-filters/", null, true);
                } else if (external_offer_active_label_tab == "Submitted Offer") {
                    $('#mo-submitted-offers-content').empty();
                    getExterOfferDataByFilter("submitted-offer", $('#mo-submitted-offers-content'), base_url+"/external-offer/outgoing-offers-by-filters/", null, true);
                } else if (external_offer_active_label_tab == "Sold History") {
                    $('#mo-sold-offers-content').empty();
                    getExterOfferDataByFilter("sold", $('#mo-sold-offers-content'), base_url+"/external-offer/outgoing-offers-by-filters/", null, true);
                }
            });

            $("#external-offer-filter-save-btn").click(function(){
                var external_offer_active_label_tab = $("#mo-blocks-wrapper").children().find(".mo-block-active .mo-block-txt").data("label");
                
                //getExterOfferDataByFilter("offer-won", $('#mo-offers-won-content'), "/external-offer/incoming-offers-by-filter/");
                var filter_data = {
                    makeInfo: {
                        "make": []
                    }
                };

                // makeInfo: {
                //         "make": [{
                //             "name": null,
                //             "model": [{
                //                 "name": null,
                //                 "variant": [{
                //                     "name": null
                //                 }]
                //             }]
                //         }]
                //     }

                //assign make to filter_data
                $("input[name='external-offer-filter-make-checkbox[]']:checked").each(function(index, value){
                    var make_name = $(value).val();
                    filter_data.makeInfo.make[index] = {};
                    filter_data.makeInfo.make[index].name = make_name;
                    filter_data.makeInfo.make[index].model = {};
                });

                //assign model to filter data
                $.each(filter_data.makeInfo.make, function(index2,value2){
                    var index_model = 0;
                    $("input[name='external-offer-filter-model-checkbox[]']:checked").each(function(index, value){
                        
                            if (filter_data.makeInfo.make[index2].name == $(value).data("for-make")) {
                                filter_data.makeInfo.make[index2].model[index_model] = {};
                                filter_data.makeInfo.make[index2].model[index_model].name = $(value).val();
                                //filter_data.makeInfo.make[index2].model[index_model].variant = {};
                                index_model++;
                            }
                        
                    });
                });

                //assign viriant to filter data
                $.each(filter_data.makeInfo.make, function(index2,value2){
                    $.each(filter_data.makeInfo.make[index2].model, function(index3, value3){
                        filter_data.makeInfo.make[index2].model[index3].variant = {};
                        var index_variant = 0;
                        $("input[name='external-offer-filter-variant-checkbox[]']:checked").each(function(index4, value4) {
                            if (filter_data.makeInfo.make[index2].model[index3].name == $(value4).data("for-model")) {
                                
                                filter_data.makeInfo.make[index2].model[index3].variant[index_variant] = {};
                                filter_data.makeInfo.make[index2].model[index3].variant[index_variant].name = $(value4).val();
                                index_variant++;
                            }
                                
                        });
                    });

                });
 
                //get value odo range from slider and convert to array with index name
                var odo_range = $("#filter-odo-range-slider").val().split(",");
                odo_range["from"] = odo_range[0];
                delete odo_range[0];
                odo_range["to"] = odo_range[1];
                delete odo_range[1];

                filter_data["odoRange"] = {"from": odo_range["from"],
                                           "to": odo_range["to"]};

                //get value year range from slider and convert to array with index name                           
                var year_range = $("#filter-year-range-slider").val().split(",");
                year_range["from"] = year_range[0];
                delete year_range[0];
                year_range["to"] = year_range[1];
                delete year_range[1];                          
                filter_data["yearRange"] = {"from": year_range["from"],
                                            "to": year_range["to"]};

                var location_data = [];                            
                $(".filter-location-checkbox:checked").each(function() {
                    //assign location id to array
                    location_data.push($(this).val());
                });

                filter_data["vehicleLocation"] = location_data;

                var transmission_type_data = [];
                $("input[name='filter-transmission[]']:checked").each(function(){
                    transmission_type_data.push($(this).val());
                });

                filter_data["transmissionType"] = transmission_type_data;

                $("input[name='filter-date-range']").val();
                if ("custome_date_range" == $("input[name='filter-date-range']:checked").val()) {

                    filter_data["dateRange"] = {"from": $("#external-offer-custom-date-range-from").val(),
                                                "to": $("#external-offer-custom-date-range-to").val()};
                } else {
                    filter_data["dateRange"] = $("input[name='filter-date-range']:checked").val();
                }
                
                var status_data = [];
                $("input[name='filter-status[]']:checked").each(function(){
                    console.log($(this), "status");
                    status_data.push($(this).val());
                });
                filter_data["status"] = status_data;                                               

                console.log("filter data: ",filter_data);
                //clear content
                
                if (external_offer_active_label_tab == "Offers Won") {
                    $('#mo-offers-won-content').empty();
                    getExterOfferDataByFilter("offer-won", $('#mo-offers-won-content'), base_url+"/external-offer/incoming-offers-by-filters/", filter_data);
                } else if (external_offer_active_label_tab == "External Offers"){
                    $('#mo-incoming-offers-content').empty();
                    getExterOfferDataByFilter("incoming-offer", $('#mo-incoming-offers-content'), base_url+"/external-offer/incoming-offers-by-filters/", filter_data);
                } else if (external_offer_active_label_tab == "Offers Received") {
                    $('#mo-offers-received-content').empty();
                    getExterOfferDataByFilter("offer-received", $('#mo-offers-received-content'), base_url+"/external-offer/outgoing-offers-by-filters/", filter_data);
                } else if (external_offer_active_label_tab == "Submitted Offer") {
                    $('#mo-submitted-offers-content').empty();
                    getExterOfferDataByFilter("submitted-offer", $('#mo-submitted-offers-content'), base_url+"/external-offer/outgoing-offers-by-filters/", filter_data);
                }
            });

            $(document).on("click", ".external-offer-filter-model-checkbox", function(){
                if($(this).parent().parent().children().find(".external-offer-filter-model-checkbox:checked").length == 0) {
                    $(this).parent().parent().prev().children().find(".external_offer_filter_make_checkbox").prop("checked", false);
                } else if ($(this).parent().parent().children().find(".external-offer-filter-model-checkbox:checked").length == 1) {    
                    $(this).parent().parent().prev().children().find(".external_offer_filter_make_checkbox").prop("checked", true);
                }
            });

            $(".external_offer_filter_make_checkbox").click(function() {

                console.log($(this).is(":checked"));
                if(!$(this).is(":checked")) {
                    $(this).parent().parent().next().children().find("input[type='checkbox']").prop("checked", false);
                } else {
                    $(this).parent().parent().next().children().find("input[type='checkbox']").prop("checked", true);
                }

                var make_name = $(this).val();
                if (!$.trim( $("#external-offer-filter-model-of-make-"+make_name.replace(/\s/g, '')).html() ).length)
                {
                    //get model by make
                    $.ajax({
                        type: 'POST',
                        data: {_token: $("input[name='_token']").val(), make: $(this).val()},
                        url: base_url+"/get-glasses-data/family",
                        success: function (data) {
                            console.log('/get-glasses-data/family success: ' + data);
                            if (data != null){

                                $("#external-offer-filter-model-of-make-"+make_name.replace(/\s/g, '')).empty();
                                $.each(data, function(index, value){
                                    //append model list
                                    var model_name = value;
                                    var model_html = 
                                                "<div class='no-padding padding-bottom-6 padding-top-10 padding-left-7 col-xs-2'>"
                                                    +"<input type='checkbox' class='mo-square-check external-offer-filter-model-checkbox' data-for-make='"+make_name+"' id='external-offer-filter-model-"+value+"' name='external-offer-filter-model-checkbox[]' value='"+value+"' checked/>"
                                                        +"<label for='external-offer-filter-model-"+value.replace(/\s/g, '')+"' class='text-capitalize disabled'><span class=''></span></label>"
                                                    +"</div>"
                                                +"<div class='no-padding padding-bottom-16 padding-top-10 col-xs-10'>"
                                                    +value.replace(/\s/g, '')
                                                +"</div>"
                                                +"<div class='list-divider col-xs-12'></div>";
                                    $("#external-offer-filter-model-of-make-"+make_name.replace(/\s/g, '')).removeClass("hidden");
                                    $("#external-offer-filter-model-of-make-"+make_name.replace(/\s/g, '')).append(model_html);

                                    //get and append variant list by make
                                    $.ajax({
                                        type: 'POST',
                                        data: {_token: $("input[name='_token']").val(), make: make_name, model: model_name},
                                        url: base_url+"/get-glasses-data/variant",
                                        success:function(data) {
                                            console.log('/get-glasses-data/variant success: '+data);
                                            //console.log(data);
                                            if (data != null) {
                                                $("#external-filter-variants-of-model").removeClass("hidden");
                                                //set section for variant
                                                var variant_section_html = 
                                                                    "<div>"
                                                                    +"<div class='no-padding text-align-center font-family-bold padding-bottom-13 padding-top-13 col-xs-12'>"
                                                                        +make_name+" - "+model_name
                                                                    +"</div>"
                                                                    +"</div>"
                                                                    +"<div class='list-divider col-xs-12'></div>";
                                                $("#external-filter-variants-of-model").append(variant_section_html);                        
                                                $.each(data, function(index,value) {
                                                    if (value.replace(/\s/g, '') != "" && value.replace(/\s/g, '') != null) {
                                                        var variant_name = value;
                                                        var variant_html = 
                                                                        "<div>"
                                                                            +"<div class='no-padding padding-bottom-6 padding-top-10 padding-left-16 col-xs-2'>"
                                                                                +"<input type='checkbox' class='mo-square-check external-offer-filter-variant-checkbox' id='variant-"+model_name.replace(/\s/g, '')+"-"+value.replace(/\s/g, '')+"' data-for-model='"+model_name+"' name='external-offer-filter-variant-checkbox[]' value='"+value+"'/>"
                                                                                +"<label for='variant-"+model_name.replace(/\s/g, '')+"-"+value.replace(/\s/g, '')+"' class='text-capitalize disabled'><span class=''></span></label>"
                                                                                +"</div>"
                                                                            +"<div class='no-padding padding-bottom-13 padding-top-13 col-xs-10'>"
                                                                                +variant_name
                                                                            +"</div>"
                                                                            +"</div>"
                                                                        +"<div class='list-divider col-xs-12'></div>";
                                                                        
                                                        $("#external-filter-variants-of-model").append(variant_html);
                                                    }
                                                });
                                            }                    
                                        },
                                        error: function(data){
                                            console.log('/get-glasses-data/variant error: '+data);
                                        },
                                        global: false
                                    });

                                });
                            }
                        },
                        error: function (data) {
                            console.log('/get-glasses-data/family error: ' + data);
                        },
                        global: false
                    });
                }
            });
            /*** END EXTERNAL OFFER FILTER SECTION ***/
            
            //TODO: must be change text to data attribute for each tab
            /* Click moto 3 Blocks */
            $('.mo-block').first().toggleClass('mo-block-active');
            $('.mo-block').first().ready(function(){
                var mo_txt = $('.mo-block').first().find('.mo-block-txt').text();
                if (mo_txt.toLowerCase() == 'external offers') {
                    $('#mo-external-offers-list').hide();
                    $('#mo-external-offers-list').removeClass('hidden').fadeIn('slow');
                    $.changeExternalOfferFilterStatus(mo_txt.toLowerCase());
                    infinityScroll("incoming", $('#mo-incoming-offers-content'), base_url+"/external-offer/incoming-offers-by-status/");
                } else {
                    $('#mo-submitted-offers-list').hide();
                    $('#mo-submitted-offers-list').removeClass('hidden').fadeIn('slow');
                    $.changeExternalOfferFilterStatus(mo_txt.toLowerCase());
                    infinityScroll("submitted", $('#mo-submitted-offers-content'), base_url+"/external-offer/outgoing-offers-by-status/");
                }
            });
            $('#mo-external-offers-list').removeClass('hidden').fadeIn('slow');
            $('#mo-submitted-offers-list').removeClass('hidden').fadeIn('slow');

            $('.mo-block').click(function () {
                $('.mo-block').removeClass('mo-block-active');
                $(this).toggleClass('mo-block-active');

                var mo_txt = $(this).find('.mo-block-txt').text();
                var mo_label = $(this).find('.mo-block-txt').data("label");
                $('.mo-block-list').hide();

                //incoming external offer
                if (mo_txt.toLowerCase() == 'external offers') {
                    $(".filter-status-section").addClass("hidden");
                    $('#mo-external-offers-list').hide();
                    $('#mo-external-offers-list').removeClass('hidden').fadeIn('slow');
                    infinityScroll("incoming", $('#mo-incoming-offers-content'), "/external-offer/incoming-offers-by-status/");
                    $.changeExternalOfferFilterStatus(mo_txt.toLowerCase());

                } else if (mo_txt.toLowerCase() == 'offers won') {
                    $(".filter-status-section").removeClass("hidden");
                    $('#mo-offers-won-list').hide();
                    $('#mo-offers-won-list').removeClass('hidden').fadeIn('slow');
                    infinityScroll("offer-won", $('#mo-offers-won-content'), "/external-offer/incoming-offers-by-status/");
                    $.changeExternalOfferFilterStatus(mo_txt.toLowerCase());

                } else if (mo_txt.toLowerCase() == 'purchase history') {
                    $(".filter-status-section").addClass("hidden");
                    $('#mo-purchase-history-list').hide();
                    $('#mo-purchase-history-list').removeClass('hidden').fadeIn('slow');

                    infinityScroll("purchase-history", $('#mo-purchase-history-content'), "/external-offer/incoming-offers-by-status/");
                    $.changeExternalOfferFilterStatus(mo_txt.toLowerCase());

                //outgoing external offer
                } else if (mo_label.toLowerCase() == 'submitted offer') {
                    $(".filter-status-section").addClass("hidden");
                    $('#mo-submitted-offers-list').hide();
                    $('#mo-submitted-offers-list').removeClass('hidden').fadeIn('slow');
                    infinityScroll("submitted", $('#mo-submitted-offers-content'), "/external-offer/outgoing-offers-by-status/");
                    $.changeExternalOfferFilterStatus(mo_label.toLowerCase());

                } else if (mo_label.toLowerCase() == 'offers received') {
                    $(".filter-status-section").removeClass("hidden");
                    $('#mo-offers-received-list').hide();
                    $('#mo-offers-received-list').removeClass('hidden').fadeIn('slow');
                    
                    infinityScroll("received", $('#mo-offers-received-content'), "/external-offer/outgoing-offers-by-status/");
                    $.changeExternalOfferFilterStatus(mo_label.toLowerCase());

                } else if (mo_label.toLowerCase() == 'sold history') {
                    $(".filter-status-section").addClass("hidden");
                    $('#mo-sold-history-list').hide();
                    $('#mo-sold-history-list').removeClass('hidden').fadeIn('slow');
                    infinityScroll("sold", $('#mo-sold-offers-content'), "/external-offer/outgoing-offers-by-status/");
                    $.changeExternalOfferFilterStatus(mo_label.toLowerCase());
                }
            });

            //change external offer filter status based on scope
            $.changeExternalOfferFilterStatus = function(scope){
                switch (scope) {
                    case "offers received":
                        console.log("offers received");
                        console.log($('#external-offer-filter-status-items'));
                        /*$('#external-offer-filter-status-items').empty();
                        var statuses_html =
                                    "<div class='padding-left-16 padding-bottom-16 col-xs-12 no-padding'>"
                                        +"<div class='col-xs-2 no-padding'>"
                                            +"<input type='checkbox' class='mo-square-check' id='filter-status-awating-response' name='filter-status[]' value='3'/>"
                                        +"<label for='filter-status-awating-response' class='text-capitalize disabled'><span class=''></span></label>"
                                    +"</div>"
                                    +"<div class='col-xs-6 no-padding'>Awating Response</div>"
                                    +"</div>"

                                    +"<div class='list-divider col-xs-12'></div>"

                                    +"<div class='padding-left-16 padding-bottom-16 padding-top-16 col-xs-12 no-padding'>"
                                        +"<div class='col-xs-2 no-padding'>"
                                            +"<input type='checkbox' class='mo-square-check' id='filter-status-acceptanced' name='filter-status[]' value='4'/>"
                                        +"<label for='filter-status-acceptanced' class='text-capitalize disabled'><span class=''></span></label>"
                                    +"</div>"
                                    +"<div class='col-xs-6 no-padding'>Acceptanced</div>"
                                    +"</div>"

                                    +"<div class='list-divider col-xs-12'></div>"

                                    +"<div class='padding-left-16 padding-bottom-16 padding-top-16 col-xs-12 no-padding'>"
                                        +"<div class='col-xs-2 no-padding'>"
                                            +"<input type='checkbox' class='mo-square-check' id='filter-status-location-set' name='filter-status[]' value='5'/>"
                                        +"<label for='filter-status-location-set' class='text-capitalize disabled'><span class=''></span></label>"
                                    +"</div>"
                                    +"<div class='col-xs-6 no-padding'>Location Set</div>"
                                    +"</div>"

                                    +"<div class='list-divider col-xs-12'></div>"

                                    +"<div class='padding-left-16 padding-bottom-16 padding-top-16 col-xs-12 no-padding'>"
                                        +"<div class='col-xs-2 no-padding'>"
                                            +"<input type='checkbox' class='mo-square-check' id='filter-status-vehicle-ready-to-ship' name='filter-status[]' value='6'/>"
                                        +"<label for='filter-status-vehicle-ready-to-ship' class='text-capitalize disabled'><span class=''></span></label>"
                                    +"</div>"
                                    +"<div class='col-xs-10 no-padding'>Vehicle Ready To Ship</div>"
                                    +"</div>"

                                    +"<div class='list-divider col-xs-12'></div>"

                                    +"<div class='padding-left-16 padding-bottom-16 padding-top-16 col-xs-12 no-padding'>"
                                        +"<div class='col-xs-2 no-padding'>"
                                            +"<input type='checkbox' class='mo-square-check' id='filter-status-transport-triggered' name='filter-status[]' value='7'/>"
                                        +"<label for='filter-status-transport-triggered' class='text-capitalize disabled'><span class=''></span></label>"
                                    +"</div>"
                                    +"<div class='col-xs-6 no-padding'>Transport Triggered</div>"
                                    +"</div>"

                                    +"<div class='list-divider col-xs-12'></div>"

                                    +"<div class='padding-left-16 padding-bottom-16 padding-top-16 col-xs-12 no-padding'>"
                                        +"<div class='col-xs-2 no-padding'>"
                                            +"<input type='checkbox' class='mo-square-check' id='filter-status-shipped' name='filter-status[]' value='8'/>"
                                        +"<label for='filter-status-shipped' class='text-capitalize disabled'><span class=''></span></label>"
                                    +"</div>"
                                    +"<div class='col-xs-6 no-padding'>Shipped</div>"
                                    +"</div>"

                                    +"<div class='list-divider col-xs-12'></div>"

                                    +"<div class='padding-left-16 padding-top-16 col-xs-12 no-padding'>"
                                        +"<div class='col-xs-2 no-padding'>"
                                            +"<input type='checkbox' class='mo-square-check' id='filter-status-passed' name='filter-status[]' value='10'/>"
                                        +"<label for='filter-status-passed' class='text-capitalize disabled'><span class=''></span></label>"
                                    +"</div>"
                                    +"<div class='col-xs-6 no-padding'>Passed</div>"
                                    +"</div>";*/
                        $('#external-offer-filter-status-items').parent().removeClass("hidden");            
                        //$('#external-offer-filter-status-items').html(statuses_html);
                        break;
                    case "submitted offer":
                        $('#external-offer-filter-status-items').parent().addClass("hidden");
                    break;

                    case "sold history":
                        $('#external-offer-filter-status-items').parent().addClass("hidden");
                    break;

                    case "external offers":
                        $('#external-offer-filter-status-items').parent().addClass("hidden");
                    break;

                    case "purchase history":
                        $('#external-offer-filter-status-items').parent().addClass("hidden");
                    break;

                    case "offers won":
                        $('#external-offer-filter-status-items').parent().removeClass("hidden");
                    break;          
                }
            }; 

            $(document).on("click",".mo-more-info-btn",function() {
                var external_offer_id = "#mo-condition-for-"+$(this).data("external-offer-id");
                //console.log($(external_offer_id).is(":hidden"));
                if ($(external_offer_id).is(":hidden")) {
                    $(external_offer_id).slideDown("slow");
                } else {
                    $(external_offer_id).slideUp("slow");
                }
            });

            //external offer - incoming bid price popup
            //bidpopup and set location
            var magnificPopup = $.magnificPopup.instance;
            $('.open-external-offer-popup').magnificPopup({
                type:'inline',
                showCloseBtn: false,
                closeOnBgClick: false
            });

            //rebind bidpopup and set location
            $(document).on("mouseover", ".open-external-offer-popup", function(){    
                $(this).unbind();
                $(this).magnificPopup({
                    type:'inline',
                    showCloseBtn: false,
                    closeOnBgClick: false
                });
            });

            $('.mo-cancel-btn').click(function (){
                magnificPopup.close();
            });

            // var bid_price_configuration = {
            //     format: 'yyyy-mm-dd',
            //     autoclose: true,
            //     todayHighlight: true,
            //     startDate: new Date(),
            //     minDate: new Date()
            // };
            // $("#external-offer-bid-price-expiry-date").datepicker(bid_price_configuration);
            
            $(".glyphicon-calendar").click(function(){
                $("#external-offer-bid-price-expiry-date").focus();
            });

            //bid price popup validate
            $('#mo-external-offer-bid-price-form').validate({
                rules:{
                    "external-offer-bid-price-expiry-date": "required",
                    "external-offer-bid-price-input": "required"

                },
                // highlight: function(element) {
                //     $(element).closest('.input-group').addClass('has-error');
                // },
                // unhighlight: function(element) {
                //     $(element).closest('.input-group').removeClass('has-error');
                // },
                errorElement: 'div',
                errorClass: 'mo-error-wrapper',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                messages:{
                    "external-offer-bid-price-expiry-date": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter expiry date</span>"+"</div>",
                    "external-offer-bid-price-input": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter price</span>"+"</div>"
                }
            
            });
            //end bid price popup validate
            Date.prototype.addDays = function(days) {
              var dat = new Date(this.valueOf());
              dat.setDate(dat.getDate() + days);
              return dat;
            }

            var global_external_offer_id = 0;
            $(".external-offer-bid-price-btn").click(function(){
                global_external_offer_id = $(this).data("external-offer-id");
                //using ajax to call external offer info here
                $.ajax({
                    type: "POST",
                    data: {"_token": $("input[name='_token']").val(),
                           "valuation_id": $(this).data("valuation-id")},
                    url: base_url+"/external-offer/get-info",
                    success: function (data) {
                        if(data != null) {
                            data = $.parseJSON(data);
                            console.log(data);

                            var external_offer_expiry = 0;
                            //debugger;
                            if($("#external-offer-expiry").val()){
                                external_offer_expiry = $("#external-offer-expiry").val();
                            }
                            var expiry_date = new Date(data.arrival_date).addDays(parseInt(external_offer_expiry));
                            var bid_price_configuration = {
                                format: 'yyyy-mm-dd',
                                autoclose: true,
                                todayHighlight: false,
                                startDate: expiry_date,
                                minDate: expiry_date,
                                defaultDate: expiry_date
                            };
                            $("#external-offer-bid-price-expiry-date").datepicker("remove");
                            $("#external-offer-bid-price-expiry-date").datepicker(bid_price_configuration);
                            $("#external-offer-bid-price-expiry-date").datepicker("update");
                        }
                    },
                    error: function(data){
                        console.log("error /external-offer/bid-price");
                    },
                    complete: function(){
                        
                    }
                });
            }); 

            $("#mo-external-offer-bid-price-submit-btn").click(function(){
                
                var external_offer_id = global_external_offer_id;
                var price = $("input[name='external-offer-bid-price-input']").val();
                var expiry_date = $("input[name='external-offer-bid-price-expiry-date']").val();

                //console.log("external_offer_id", external_offer_id);
                //console.log("price", price);
                //console.log("expiry_date", expiry_date);

                if ($('#mo-external-offer-bid-price-form').valid())
                {
                    //bidpriceajax
                    $.ajax({
                        type: "POST",
                        url: base_url+"/external-offer/bid-price",
                        data: {
                                "_token": $("input[name='_token']").val(),
                                "external_offer_id": external_offer_id,
                                "price": price,
                                "expiry_date": expiry_date
                               },
                        success: function (data) {
                            data = $.parseJSON(data);
                            if(data.success) {
                                console.log("bid successful");
                                magnificPopup.close();
                                window.location.reload();
                            } else {
                                //show error message
                                $(".bid-price-info").removeClass("hidden");
                                setInterval(function(){
                                    window.location.reload();
                                }, 5000);
                                console.log("/external-offer/bid-price", data);
                            }
                        },
                        error: function(data){
                            console.log("error /external-offer/bid-price");
                        },
                        complete: function(){
                            
                        }
                    });
                }

            });

            //delete external offer (incoming)
            var global_external_offer_id = 0;
            var global_vehicle_id = 0;
            $(document).on("click", ".mo-external-offer-reject", function(){
                global_external_offer_id = $(this).data("external-offer-id");
                global_vehicle_id = $(this).data("vehicle-id");
            });
            $("#mo-external-offer-incoming-delete-btn").click(function(){
                //rejectajax here
                var vehicle_id = global_vehicle_id;
                var token = $("input[name='_token']").val();
                $.ajax({
                    type:'POST',
                    url: base_url+'/external-offer/block',
                    data: {"_token": token, "vehicle_id": vehicle_id},
                    success:function(data) {
                        if (data != null)
                        {
                            data = $.parseJSON(data);
                            console.log(data);
                            if (data.success) {
                                window.location.reload();
                            } else {
                                //show message null here
                            }
                        }
                        

                    },
                    error: function(data) {
                        console.log("/external-offer/reject error");
                    },
                    complete: function() {
                        
                    },
                    global: true
                });
            });

            $(document).on("click", ".mo-external-offer-set-location-btn", function(){
                global_external_offer_id = $(this).data("external-offer-id");
            })
            //external offer won set location
            $(document).on("click", "#mo-external-offer-won-location-submit-btn", function(){

                var external_offer_id = global_external_offer_id;

                var location_id = $("#current-user-location").val();

                var token = $("input[name='_token']").val();

                $(document).ajaxStart(function(){
                    $(".mo-loading-image").removeClass("hidden");
                });

                $.ajax({
                    type:'POST',
                    url: base_url+'/external-offer/set-location',
                    data: {"_token": token, "external_offer_id": external_offer_id, "location_id": location_id},
                    success:function(data) {
                        if (data != null)
                        {
                            data = $.parseJSON(data);
                            if(data.success) {
                                console.log("set location successful");
                                window.location.reload();
                            } else {
                                magnificPopup.close();
                                console.log(data);
                            }
                        }
                    },
                    error: function(data) {
                        console.log("/external-offer/set-location error");
                    },
                    complete: function() {
                        $(".mo-loading-image").addClass("hidden");
                    },
                    global: true
                });
            });
            
            /** USING DOCUMENT BIND EVENT FOR INFINITE SCROLL **/
            $(document).on("click",".mo-go-to-review",function() {
                var valuation_id = +$(this).data("valuation-id");
                if(valuation_id != ''){
                    window.location.href = "/review-valuation/"+valuation_id;
                }
            });

            $(document).on("click",".mo-approve-offer",function() {
                var external_offer_id = $(this).data("external-offer-id");
                var url = base_url+"/external-offer/accept";
                updateOfferStatus(external_offer_id, url);
            });

            $(document).on("click",".mo-reject-offer",function() {
                var external_offer_id = $(this).data("external-offer-id");
                $("#mo-offer-id").val(external_offer_id);
            });

            function updateOfferStatus(external_offer_id, url){
                $(document).off(".scroll");
                $(document).on("ajaxStart.updateStatus", function () {
                    $(".mo-loading-image").removeClass("hidden");
                });

                var token = $("input[name='_token']").val();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        _token: token,
                        external_offer_id: external_offer_id
                    },
                    success: function (data) {
                        data = $.parseJSON(data);
                        console.log(data);
                        if (data.success) {
                            window.location.reload();
                        }
                    },
                    error: function(data){
                        //console.log(data);
                    },
                    complete: function(){
                        //$(".mo-loading-image").addClass("hidden");
                    }
                });

                $(document).on("ajaxStop.updateStatus", function () {
                    $(".mo-loading-image").addClass("hidden");
                });
            }

            function infinityScroll(status, content, url) {
                //console.log("content "+content.children().length);
                var win = $(window);
                win.off('scroll.windowscroll');

                if (typeof(Storage) !== "undefined") {
                    localStorage.setItem("content_length", 0);
                }

                // Each time the user scrolls
                var stop = false;
                win.on('scroll.windowscroll', $.scroll = function() {
                    // End of the document reached?
                    
                    if ($(document).height() - win.height() == win.scrollTop()) {
                        
                        if (!stop) {
                            stop = true;
                            content.children().find(".mo-loading-icon").parent().not(":last").remove();
                            //console.log(localStorage.getItem("content_length"));
                            if(content.children().length > localStorage.getItem("content_length")){
                                localStorage.setItem("content_length", content.children().length - 1);
                                var token = $("input[name='_token']").val();
                                $(document).off(".updateStatus");
                                $(document).on("ajaxStart.scroll", function () {
                                    $('.mo-loading-icon').show();
                                });

                                $.ajax({
                                    type: "POST",
                                    url: url+status,
                                    data: {
                                        _token: token,
                                        limit: 20,
                                        offset: content.children().length
                                    },
                                    success: function(html) {
                                        //console.log(html, "html_content");
                                        if(html == ""){
                                            win.off('scroll.windowscroll');
                                        } else {
                                            content.append(html);
                                        }
                                    },
                                    complete: function(){
                                        stop = false;
                                        //$('.mo-loading-icon').hide();
                                        /*$('#mo-confirm-reject-offer').off('click');
                                        $('.mo-reject-offer').off('click');
                                        $(".mo-reject-offer").bind('click', function(){
                                            var external_offer_id = $(this).data("external-offer-id");
                                            $("#mo-offer-id").val(external_offer_id);
                                        });*/
                                        suspendPopup();
                                    }
                                });

                                $(document).on("ajaxStop.scroll", function () {
                                    $('.mo-loading-icon').hide();
                                });
                            }
                        }
                    }
                });
            }

            function getExterOfferDataByFilter(status, content, url, filter_data, reset) {
                if (reset === "undefined") {
                    reset = false;
                }
                // $(document).on("ajaxStart.updateStatus", function () {
                    // $(".mo-loading-image").removeClass("hidden");
                // });
                
                var token = $("input[name='_token']").val();
                $.ajax({
                    type: "POST",
                    url: url+status,
                    data: {
                        _token: token,
                        limit: 20,
                        offset: content.children().length,
                        filter_data: JSON.stringify(filter_data),
                        reset_filter: reset
                    },
                    success: function(html) {
    
                        if(html == "no-data") {
                            html = "<div class='col-xs-12'>"
                                        +"<div class='mo-row text-center text-uppercase font-weight-bold'>no item to display now</div>"
                                    +"</div>";      
                            content.append(html);
                            console.log(content);        
                                  
                        } else {
                            content.append(html);
                        }
                    },
                    complete: function(){
                        suspendPopup();
                        $(".mo-loading-image").addClass("hidden");
                    }
                });

                //console.log("content "+content.children().length);
                var win = $(window);
                win.off('scroll.windowscroll');

                if (typeof(Storage) !== "undefined") {
                    localStorage.setItem("content_length", 0);
                }

                win.on('scroll.windowscroll', function() {
                    //console.log(" status "+status);
                    //console.log(localStorage.getItem("content_length"));
                    //console.log("scroll");
                    // End of the document reached?
                    if ($(document).height() - win.height() == win.scrollTop()) {
                        content.children().find(".mo-loading-icon").parent().not(":last").remove();
                        if(content.children().length > localStorage.getItem("content_length")){
                             //console.log("scroll inside if");
                             localStorage.setItem("content_length", content.children().length-1);
                             var token = $("input[name='_token']").val();
                             $(document).off(".updateStatus");
                             $(document).on("ajaxStart.scroll", function () {
                                 $('.mo-loading-icon').show();
                             });

                             $.ajax({
                                 type: "POST",
                                 url: url+status,
                                 data: {
                                     _token: token,
                                     limit: 20,
                                     offset: content.children().length,
                                     filter_data: JSON.stringify(filter_data),
                                     reset_filter: reset
                                 },
                                 success: function(html) {
                                     if(html == "no-data") {      
                                        win.off('scroll.windowscroll');
                                              
                                     } else {
                                        content.append(html);
                                     }
                                     
                                 },
                                 complete: function(){
                                     suspendPopup();
                                     //$(".mo-loading-image").addClass("hidden");
                                 }
                             });

                             $(document).on("ajaxStop.scroll", function () {
                                 $('.mo-loading-icon').hide();
                             });
                        }
                    }
                 //console.log(localStorage.getItem("content_length"));
                 });
            }

            var suspendPopup = function() {
                var magnificPopup = $.magnificPopup.instance;
                $('.mo-suspend-popup').magnificPopup({
                    type:'inline',
                    showCloseBtn: true,
                    closeOnBgClick: false
                });

                $(document).on("click",".mo-cancel-btn",function() {
                    magnificPopup.close();
                });
                $('#mo-confirm-reject-offer').off('click');

                $(document).off("click","#mo-confirm-reject-offer");/** PREVENT CLICK EVENT RUN MANY TIMES **/
                $(document).on("click","#mo-confirm-reject-offer",function() {
                    var url = base_url+"/external-offer/reject";
                    var external_offer_id = $("#mo-offer-id").val();
                    //console.log("external_offer_id "+external_offer_id);
                    updateOfferStatus(external_offer_id, url);
                });
            }
            suspendPopup();
        }

        /******************** START DEALERSHIP SETTINGS *********************/

        else if(pathname.indexOf("/dealership/profile-setting") != -1){

            (function ($) {
                $.toggleShowPassword = function (options) {

                    var settings = $.extend(options);

                    var control = $(settings.control);
                    var hide_control = $(settings.hide_control);
                    var field = $(settings.field);

                    control.hide();

                    control.bind('click', function () {
                            hide_control.show();
                            control.hide();
                            field.attr('type', 'password');
                    });

                    hide_control.bind('click', function () {
                        if (field.is(':password')) {
                            hide_control.hide();
                            control.show();
                            field.attr('type', 'text');
                        }
                    });
                };
            }(jQuery));

            $.toggleShowPassword({
                field: '#profile-quick-code',
                control: '.mo-profile-password',
                hide_control: '.mo-profile-show-password'
            });

            $.toggleShowPassword({
                field: "#profile-password",
                control: '.mo-user-password',
                hide_control: '.mo-user-show-password'
            });

            /** Validate Quick Code **/

            $("#mo-profile-setting-form").validate({
                rules: {
                    profile_quick_code: {
                        minlength: 4
                    }
                },
                highlight: function(element) {
                    $(element).closest('.input-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.input-group').removeClass('has-error');
                },
                messages: {
                    profile_quick_code: {
                        minlength: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter 4 number</span>" + "</div>"
                    }
                }

            });

            $("#mo-update-profile-setting").click(function(e){
                e.preventDefault();
                if($("#mo-profile-setting-form").valid()){
                    if(!$(".mo-notification-setting-check").is(":checked")){
                        $("#error-message").html("<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please check at least one checkbox</span>" + "</div>");
                    }
                    else{
                        $("#error-message").html("");
                        $(document).ajaxStart(function() {
                            $(".mo-loading-image").removeClass("hidden");
                        });

                        var notification_obj={};
                        notification_obj.event_codes=[];

                        var config = "none";
                        //get notification setting
                        $("input.mo-notification-setting-check").each(function(){
                            var $this = $(this);
                            var id = $this.attr("id");


                            if($this.is(":checked"))
                            {
                                switch (id){
                                    case "noti_only":
                                        config = "noti_only";
                                        break;
                                    case "email_only":
                                        config = "email_only";
                                        break;
                                }
                            }

                            if($('input.mo-notification-setting-check:checked').length == 2){
                                config = "both";
                            }
                        });



                        $("input.mo-notification-check").each(function(e){
                            var $this = $(this);
                            var event_obj={};
                            event_obj.code = $this.attr("id");
                            if($this.is(":checked")){
                                event_obj.config = config;
                            }else{
                                event_obj.config = "none";
                            }
                            notification_obj.event_codes.push(event_obj);
                        });


                        //debugger;
                        var data = $('#mo-profile-setting-form').serializeArray();
                        data.push({name: 'notification_obj', value: JSON.stringify(notification_obj)});

                        //var formData = $("#mo-profile-setting-form").serialize();
                        $.ajax({
                            type: "POST",
                            url: base_url+"/dealership/update-profile-setting",
                            data: data,
                            success: function (data) {
                                if(data.length != 0){
                                    $(".alert-success").removeClass("hidden");
                                }
                            },
                            error: function(data){
                                //console.log(data);
                            },
                            complete: function(){
                                $(".mo-loading-image").addClass("hidden");
                            }
                        });
                    }

                }else{
                    return false;
                }
            });

            $("#profile-quick-code").keyup(function(){
                var $this = $(this);
                $this.val($this.val().replace(/[^\d+]/g, ''));
            });
        }

        /******************** END DEALERSHIP SETTINGS *********************/

        /*** EXTERNAL OFFER GROUPS ***/
        else if (pathname.indexOf("/external-offer-groups") != -1){
            /** AJAX CALL to REMOVE **/
            $("#mo-remove-external-offer-group").click(function(e){
                e.preventDefault();
                $(document).bind("ajaxStart.mine", function() {
                    $(".mo-loading-image").removeClass("hidden");
                });
                $.ajax({
                    type: "POST",
                    url: base_url+"/dealership/remove-external-offer-group/"+ $("#mo-group-id").val(),
                    data: {
                        _token: $("input[name='_token']").val(),
                        group_name: $("#group-name").val(),
                        group_desc: $("#group-desc").val(),
                        select_location: $('#location-select').val()
                    },
                    success: function (data) {
                        //console.log(data);
                        if(data == 1) {
                            $(".alert").removeClass("hidden");
                            $("html, body").animate({
                                scrollTop: 0
                            }, 500);
                            window.location.href = base_url+"/dealership/external-offer-groups";
                        }
                    },
                    error: function (data) {
                        //console.log(data);
                    },
                    complete: function(){
                        $('.mo-loading-image').addClass("hidden");
                    }
                });

                $(document).unbind(".mine");
            });
        }

        else if (pathname.indexOf("/init-external-offer-group") != -1){
            /** INITIALIZE MULTI-SELECT **/
            var inputSearch = "<label for='mo-txt-search' class='text-capitalize'>search dealers/location</label>"+
                "<input type='text' class='search-input mo-txt-quick-search' autocomplete='off' placeholder='Search' id='mo-txt-search'>";
            $('#location-select').multiSelect({
                selectableOptgroup: true,
                selectableHeader: inputSearch,
                afterInit: function(ms){
                    var labelDealer = '<label for="name" class="text-capitalize" style="margin-bottom: 60px;">dealers/location*</label>' +
                                        '<input type="hidden" id="mo-dealer-locations" name="mo_dealer_locations">';
                    $(".ms-selection").prepend(labelDealer);
                    var that = this,
                        $selectableSearch = that.$selectableUl.prev(),
                        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)';

                    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                        .on('keydown', function(e){
                            if (e.which === 40){
                                that.$selectableUl.focus();
                                return false;
                            }
                        });
                },
                afterSelect: function(){
                    this.qs1.cache();
                },
                afterDeselect: function(){
                    this.qs1.cache();
                }
            });
            if($('#location-select').val()){
                //console.log('load '+$('#location-select').val());
                $('#mo-dealer-locations').val($('#location-select').val());
            }
            $('#location-select').change(function(){
                //console.log($('#location-select').val());
                $('#mo-dealer-locations').val($('#location-select').val());
            });

            $("#mo-external-group-form").validate({
                ignore: [],
                rules: {
                    group_name: "required",
                    mo_dealer_locations: "required"
                },
                highlight: function(element) {
                    $(element).closest('.input-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.input-group').removeClass('has-error');
                },
                messages: {
                    group_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter group name</span>" + "</div>",
                    mo_dealer_locations: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter dealers/location</span>" + "</div>"
                },
                submitHandler: function (form) {
                    $(document).bind("ajaxStart.mine", function() {
                        $(".mo-loading-image").removeClass("hidden");
                    });

                    if (pathname.indexOf("/init-external-offer-group/new") != -1){
                        /** AJAX CALL to CREATE **/
                        $.ajax({
                            type: "POST",
                            url: base_url+"/dealership/create-external-offer-group",
                            data: {
                                _token: $("input[name='_token']").val(),
                                group_name: $("#group-name").val(),
                                group_desc: $("#group-desc").val(),
                                select_location: $('#location-select').val()
                            },
                            success: function (data) {
                                //console.log(data);
                                if(data == 1) {
                                    $(".alert").removeClass("hidden");
                                    $("html, body").animate({
                                        scrollTop: 0
                                    }, 500);
                                    window.location.href = base_url+"/dealership/external-offer-groups";
                                }
                            },
                            error: function (data) {
                                //console.log(data);
                            },
                            complete: function(){
                                $('.mo-loading-image').addClass("hidden");
                            }
                        });
                    }else if(pathname.indexOf("/init-external-offer-group/edit") != -1){
                        /** AJAX CALL to UPDATE **/
                        $("#mo-external-group-form").removeAttr("action").attr("action", "/dealership/update-external-offer-group/" + $("#mo-group-id").val());
                        $.ajax({
                            type: "POST",
                            url: base_url+"/dealership/update-external-offer-group/" + $("#mo-group-id").val(),
                            data: {
                                _token: $("input[name='_token']").val(),
                                group_name: $("#group-name").val(),
                                group_desc: $("#group-desc").val(),
                                select_location: $('#location-select').val()
                            },
                            success: function (data) {
                                //console.log(data);
                                if(data == 1) {
                                    $(".alert").removeClass("hidden");
                                    $("html, body").animate({
                                        scrollTop: 0
                                    }, 500);
                                    window.location.href = base_url+"/dealership/external-offer-groups";
                                }
                            },
                            error: function (data) {
                                //console.log(data);
                            },
                            complete: function(){
                                $('.mo-loading-image').addClass("hidden");
                            }
                        });
                    }

                    $(document).unbind(".mine");
                }
            });
        }
        /*** EXTERNAL OFFER GROUPS ***/

        /******************** START EDIT PERMISSION *********************/

        else if(pathname.indexOf("mo-admin/edit-permission") != -1){

            /** toggle EDIT PERMISSION ICON **/
            $(".mo-open-edit-permission, .mo-permission-name, .mo-permission-description").click(function(e) {
                e.preventDefault();
                toogleInlineEdit($(this));
            });

            function toogleInlineEdit(this_element){
                this_element.parent().parent().find('.mo-permission-name').toggleClass('hidden');
                this_element.parent().parent().find('.mo-permission-description').toggleClass('hidden');
                this_element.parent().parent().find('input[type="text"]').toggleClass('hidden');
                if(this_element.attr('class').indexOf("mo-open-edit-permission") != -1){
                    $(this_element).parent().css('padding-top','15px');
                    $(this_element).addClass('hidden');
                    $(this_element).next().toggleClass('hidden');
                }else{
                    this_element.parent().parent().find('div').last().css('padding-top','15px');
                    this_element.parent().parent().find('div').last().children('.mo-open-edit-permission').addClass('hidden');
                    this_element.parent().parent().find('div').last().children('.mo-open-edit-permission').next().toggleClass('hidden');
                }
            }
            /*************** Show and hide button and input field ******************/

            $(".mo-icon-edit").click(function(e){
                var permission_name_span = $(this).parent().parent().find('.mo-permission-name');
                permission_name_span.toggleClass('hidden');
                permission_name_span.text($(this).parent().parent().find("#permission-name").val());

                var permission_description_span = $(this).parent().parent().find('.mo-permission-description');
                permission_description_span.toggleClass('hidden');
                permission_description_span.text($(this).parent().parent().find("#permission-description").val());

                $(this).parent().parent().find('input[type="text"]').toggleClass('hidden');
                $(this).addClass('hidden');
                $(this).prev().toggleClass('hidden');
            });

            $(".mo-edit-permission input").change(function() {
                var last_div_mo_icon_edit = $(this).parent().parent().find('div').last().find(".mo-icon-edit");
                var permission_code = last_div_mo_icon_edit.data('permission-code');
                last_div_mo_icon_edit.click(function(e){
                    e.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: base_url+"/mo-admin/update-permission",
                        data: {
                            _token: $("input[name='_token']").val(),
                            permission_code: permission_code,
                            permission_name: $(this).parent().parent().find("#permission-name").val(),
                            permission_description: $(this).parent().parent().find("#permission-description").val()
                        },
                        success: function(data){
                            console.log(data);
                            if(data == 1 ){
                                $(".alert").removeClass('hidden');
                            }
                        },
                        error: function(data){
                            //console.log(data);
                        },
                        complete: function(){
                            return false;
                        }
                    });
                });
            });
        }

        /******************** END EDIT PERMISSION *********************/

        /******************** start TRADING *********************/
        else if(pathname.indexOf("/trading") != -1){
            
            $.getScript("/js/trading/mo-trading-master.js").done(function() {

                /****** start TRADING - STOCK LIST ******/
                if(pathname.indexOf("trading/stock-list") != -1){
                    $.getScript("/js/trading/stocklist/viewStockList.js");
                }
                /****** end TRADING - STOCK LIST ******/


                /****** start TRADING - VEHICLE DETAILS ******/
                else if(pathname.indexOf("/trading/showroom-vehicle-detail") != -1){
                    $.getScript("/js/trading/stocklist/viewVehicleDetails.js");
                }
                /****** end TRADING - VEHICLE DETAILS ******/


                /****** start TRADING - AUCTION LIST ******/
                else if (pathname.indexOf("trading/auction") != -1) {
                    $.getScript("/js/trading/auction/viewAuctionList.js");
                }
                /****** end TRADING - AUCTION LIST ******/


                /****** start TRADING - WISHLIST ******/
                else if(pathname.indexOf("/trading/wishlist/new") != -1 || pathname.indexOf("/trading/wishlist/edit") != -1) {
                    if(pathname.indexOf("/trading/wishlist/new") != -1) {
                        $.getScript("/js/trading/wishlist/createWishList.js");
                    } else if ( pathname.indexOf("/trading/wishlist/edit") != -1) {
                        $.getScript("/js/trading/wishlist/createWishList.js").done(function(){
                            $.getScript("/js/trading/wishlist/editWishList.js");
                        });
                    }
                }
                /****** end TRADING - WISHLIST ******/

                /****** start MO-ADMIN - REQUEST LIST ******/
                else if (pathname.indexOf("/mo-admin/trading/request-list") != -1) {
                    $.getScript(base_url+"/js/mo-admin/trading/auction-session/showRequestList.js");
                }
                /****** END MO-ADMIN - REQUEST LIST ******/


                /****** start TRADING - LIVE AUCTION - BOTH DEALERSHIP & MO ADMIN ******/
                else if (pathname.indexOf("/trading/live-auction") != -1 || pathname.indexOf("/mo-admin/trading/live-auction") != -1){
                    $.getScript(base_url+"/js/mo-admin/trading/auction-session/viewLiveAuctionList.js");
                }
                /****** end TRADING - LIVE AUCTION ******/


                /************ start NEW AUCTION SESSION ***************/
                else if(pathname.indexOf("/mo-admin/trading/create-auction-session/new") != -1) {
                    $.getScript(base_url+"/js/mo-admin/trading/auction-session/createAuctionSession.js");
                }

                /************ start EDIT AUCTION SESSION ***************/
                else if(pathname.indexOf("/mo-admin/trading/create-auction-session/edit") != -1) {
                    $.getScript(base_url+"/js/mo-admin/trading/auction-session/editAuctionSession.js");
                }
            });

            /*********** start TRADING - AUCTION LIST ***********/

            /*if (pathname.indexOf("trading/auction") != -1) {
                /!** SHOW SORT MENU **!/
                $(".mo-sort-auction-list").show();

                //console.log(JSON.parse(localStorage.getItem("elem_to_filter")));

                var tabName = '';
                if (hash) {
                    tabName = hash.slice(1, hash.length);
                }
                $('#mo-blocks-wrapper a').click(function() {
                    var token = $("input[name='_token']").val();
                    var tab = $(this).attr('tab');
                    $('#mo-blocks-wrapper .mo-block').removeClass('mo-block-active');
                    $('.data-block').hide();

                    $(this).find('.mo-block').addClass('mo-block-active');
                    $('#' + tab + '-block').show();

                    var content_id = '#'+tab+'-block';
                    var url = '/trading/get-'+tab+'-item';
                    var container = $(content_id);

                    /!** SET LOCAL STORAGE TO APPLY FILTER **!/
                    var data = {
                        content_id: content_id,
                        url: url
                    };
                    localStorage.setItem("elem_to_filter", JSON.stringify(data));

                    container.html('');
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            _token: token,
                            limit: 21,
                            offset: 0
                        },
                        success: function(html) {
                            container.html(html);
                            infinityScroll("");
                            preBidPopup();
                            buyNowPopup();
                        },
                        complete: function(){
                        }
                    });
                });

                if (tabName) {
                    $('a[tab="'+tabName+'"]').click();
                } else {
                    $('#mo-blocks-wrapper a').first().click();
                }

                /!** INFINITE SCROLL **!/
                var content = $("#mo-auction-grid-wrapper #all-block");
                var content_id = '#all-block';
                var url = '/trading/get-all-item';

                /!** SET LOCAL STORAGE TO APPLY FILTER **!/
                var data = {
                    content_id: content_id,
                    url: url
                };
                localStorage.setItem("elem_to_filter", JSON.stringify(data));
                infinityScroll("");

                $(document).on('click', '.mo-watch-icon', function() {
                    var _this = $(this);
                    var auctionId = $(this).parents('.auction-item').attr('item');
                    if ( _this.find('.watch-light').length == 1) {
                        $.ajax({
                            type: "DELETE",
                            url: '/trading/watching-list',
                            data: {
                                _token: token,
                                auction_id: auctionId
                            },
                            success: function(result) {
                                if (result) {
                                    _this.find('.watch-light').removeClass('watch-light').addClass('watch-dim');
                                }

                                var pathname = window.location.pathname;
                                var hash = window.location.hash;
                                if (hash) {
                                    var tab = hash.slice(1, hash.length);
                                }

                                if (tab == 'watching') {
                                    _this.parents('.auction-item').remove();
                                }
                            },
                            complete: function(){
                            }
                        });

                    } else {
                        $.ajax({
                            type: "POST",
                            url: '/trading/watching-list',
                            data: {
                                _token: token,
                                auction_id: auctionId
                            },
                            success: function(result) {
                                if (result) {
                                    _this.find('.watch-dim').removeClass('watch-dim').addClass('watch-light');
                                }
                            },
                            complete: function(){
                            }
                        });
                    }
                    return false;
                });

                function infinityScroll(data_obj) {
                    var data_storage = JSON.parse(localStorage.getItem("elem_to_filter"));
                    var content = $(data_storage.content_id);
                    var url = data_storage.url;

                    var win = $(window);
                    win.off('scroll.windowscroll');

                    if (typeof(Storage) !== "undefined") {
                        localStorage.setItem("vehicle_grid_content_length", 0);
                    }

                    // Each time the user scrolls
                    win.on('scroll.windowscroll', function() {
                        // End of the document reached?
                        if ($(document).height() - win.height() == win.scrollTop()) {
                            var content_length = content.children(":not(.mo-loading-wrapper)").length;
                            if(content_length > localStorage.getItem("vehicle_grid_content_length" )){
                                localStorage.setItem("vehicle_grid_content_length", content_length);
                                var token = $("input[name='_token']").val();
                                $(document).off(".updateStatus");
                                $(document).on("ajaxStart.scroll", function () {
                                    $('.mo-loading-wrapper').show();
                                    $('.mo-loading-icon').show();
                                });

                                var data = {
                                    _token: token,
                                    limit: 21,
                                    offset: content_length
                                };
                                if(data_obj != ''){
                                    data = $.extend({}, data, data_obj);
                                }

                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function(html) {
                                        if(html == ""){
                                            win.off('scroll.windowscroll');
                                        } else {
                                            content.append(html);
                                        }
                                    },
                                    complete: function(){
                                        sendToAuctionPopup();
                                        approveRequestPopup();
                                        rejectRequestPopup();
                                        liveAuctionPopup();
                                        preBidPopup();
                                        buyNowPopup();
                                        $(".mo-loading-icon").parent().not(":last").remove();
                                    }
                                });

                                $(document).on("ajaxStop.scroll", function () {
                                    $('.mo-loading-icon').hide();
                                    $('.mo-loading-wrapper').hide();
                                });
                            }
                        }
                    });
                }
            }*/
            /*********** end TRADING - AUCTION LIST ***********/
        }

        /******************** end TRADING *********************/

        /**** start ARRIVING VEHICLE *****/
        else if(pathname.indexOf("/arriving-vehicle/additional-detail") != -1) {
            $(".mo-dispute-toggle").bootstrapToggle({
                on: '',
                off: ''
            });

            var magnificPopup = $.magnificPopup.instance;
            $('.open-popup-link').magnificPopup({
                type:'inline',
                showCloseBtn: false,
                closeOnBgClick: false
            });

            $('.mo-cancel-btn').click(function (){
                magnificPopup.close();
            });

            $("#mo-arriving-vehicle-additional-detail-popup-form").validate({
                rules: {
                    arriving_vehicle_category: "required",
                    arriving_vehicle_section: "required",
                    arriving_vehicle_item: "required",
                    arriving_vehicle_item_cost: "required"
                    //arriving_vehicle_dispute_note: "required"
                },
                highlight: function(element){
                    $(element).closest(".input-group").addClass('has-error');
                },
                unhighlight: function(element){
                    $(element).closest(".input-group").removeClass('has-error');
                },
                messages: {
                    arriving_vehicle_category: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
                    arriving_vehicle_section: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
                    arriving_vehicle_item: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
                    arriving_vehicle_item_cost: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>"
                    //arriving_vehicle_dispute_note: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>"
                }
            });

            $("#mo-dispute-additional-details-contact-form").validate({
                rules: {
                    dispute_contact_first_name: "required",
                    //dispute_contact_last_name: "required",
                    dispute_contact_phone: "required",
                    dispute_contact_email: "required"
                },
                errorElement: 'span',
                errorClass: "mo-error-wrapper",
                errorPlacement: function(error, element) {

                    if(element.attr('name') == 'dispute_contact_first_name'){
                        error.insertAfter($("input[name='dispute_contact_last_name']"));
                    }else if (element.attr('name') == 'dispute_contact_last_name') {
                        //does not show
                    }    
                    else{
                        error.insertAfter(element);
                    }
                    
                },
                messages: {
                    dispute_contact_first_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
                    //dispute_contact_last_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
                    dispute_contact_phone: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
                    dispute_contact_email: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
                },
            });

            $("#mo-dispute-additional-details-comment-form").validate({
                rules: {
                    arriving_vehicle_dispute_comment: "required"
                },
                errorElement: 'div',
                errorClass: "mo-error-wrapper",
                errorPlacement: function(error, element){
                    error.insertAfter(element);
                },
                messages: {
                    arriving_vehicle_dispute_comment: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>"
                }
            });

            //$("#arriving_vehicle_save_btn").click(function(){
                //$("#mo-arriving-vehicle-additional-detail-popup-form").submit();
            //});

        }

        /************ start ACCOUNTING DOCUMENTS *************/
        else if(pathname.indexOf("/accounting/documents") != -1){
            $(".mo-block").first().toggleClass("mo-block-active");
            $("#mo-accounting-document-buying-list").removeClass("hidden").fadeIn("slow");
            $(".mo-block").click(function(){
                $(".mo-block").removeClass("mo-block-active");
                $(this).toggleClass("mo-block-active");
                var mo_txt = $(this).find('.mo-block-txt').text();
                $(".mo-block-list").hide();
                if(mo_txt == "buying"){
                    $("#mo-accounting-document-buying-list").hide();
                    $("#mo-accounting-document-buying-list").removeClass("hidden").fadeIn("slow");
                }
                else if(mo_txt == "selling"){
                    $("#mo-accounting-document-selling-list").hide();
                    $("#mo-accounting-document-selling-list").removeClass("hidden").fadeIn("slow");
                }

            });
        }

        /************** end ACCOUNTING DOCUMENT ***************/


        /************** start DEALERSHIP DASHBOARD ***************/
        else if(pathname.indexOf("/dashboard") != -1){

            $.getScript("/js/dashboard/dashboard.js");

        }

        /******************** end MO-ADMIN AUCTION TRADING *********************/

        else if(pathname.indexOf("/mo-admin/user") != -1) {
            $.getScript("/js/admin/user/user-master.js").done(function () {
                if (pathname.indexOf("/mo-admin/user/create") != -1) {
                    $.getScript("/js/admin/user/create.js");
                }
                else if (pathname.indexOf("/mo-admin/user/edit") != -1) {
                    $.getScript("/js/admin/user/create.js");
                }
                else if (pathname.indexOf("/mo-admin/user/list") != -1) {
                    $.getScript("/js/admin/user/viewUsersList.js");
                }
            });
        }
        else if(pathname.indexOf("/mo-admin/transaction-current") != -1){
            $(".mo-block").first().toggleClass("mo-block-active");
            $("#mo-transaction-list-wrapper").removeClass("hidden").fadeIn("slow");
            $(".mo-block").click(function(){
                $(".mo-block").removeClass("mo-block-active");
                $(this).toggleClass("mo-block-active");
                var mo_id = $(this).attr('id');
                // alert(mo_id);
                $("#mo-transaction-list-wrapper").hide();
                if(mo_id == "mo-transaction-all"){
                    $("#mo-transaction-list-wrapper").hide();
                    $("#mo-transaction-list-wrapper").removeClass("hidden").fadeIn("slow");
                }                              
            });
        }   

        $(".mo-close-message").click(function (){
            $('.alert').addClass('hidden');

        });

        $('[data-toggle="tooltip"]').tooltip();

    });
}(jQuery));

function preBidPopup(){

    var token = $("input[name='_token']").val();

    $(document).on("click",".cancel-but",function() {
        var magnificPopup = $.magnificPopup.instance;
        magnificPopup.close();
    });

    $(document).on("click",".pre-bid-btn",function() {
        var $auction = $(this).parents('.auction-item');
        $('#mo-max-value').val(0);
        $('#pre_bid_auction_session_id').val($auction.find('.auction-session-id').text());
        $('#pre_bid_auction_id').val($auction.attr('item'));
        return false;
    });

    $(".mo-popup").magnificPopup({
        type:'inline',
        showCloseBtn: false,
        closeOnBgClick: false
    });

    /** VALIDATE FORM - POPUP **/
    $('#mo-pre-bid-popup-form').validate({
        rules:{
            max_value: {
                required: true,
                digits: true
            }
        },
        errorElement: 'div',
        errorClass: 'mo-error-wrapper',

        messages:{
            max_value: {
                required: "<div class='mo-error-wrapper'><img src='../images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                digits: "<div class='mo-error-wrapper'><img src='../images/svg/main/ic-warning.svg'>"+ "<span>Please enter a valid number.</span>"+"</div>"
            }

        },
        submitHandler: function(form){
            var max_value = $('#mo-max-value').val();
            var pre_bid_auction_session_id = $('#pre_bid_auction_session_id').val();
            var pre_bid_auction_id = $('#pre_bid_auction_id').val();

            $(document).ajaxStart(function(){
                $(".mo-loading-image").removeClass("hidden");
            });
            $.ajax({
                type: "POST",
                url: base_url+'/trading/save-max-value',
                data: {
                    _token: token,
                    max_price: max_value,
                    session_id: pre_bid_auction_session_id,
                    auction_id: pre_bid_auction_id

                },
                success: function(result) {
                    if (result) {
                        var magnificPopup = $.magnificPopup.instance;
                        magnificPopup.close();
                    }
                },
                complete: function(){
                    $(".mo-loading-image").addClass("hidden");
                }
            });
        }
    });
}

function buyNowPopup() {
    var token = $("input[name='_token']").val();

    $(document).on("click", ".cancel-but",function() {
        var magnificPopup = $.magnificPopup.instance;
        magnificPopup.close();
    });

    $(document).on("click", ".buy-now-btn", function () {
        var $auction = $(this).parents('.auction-item');
        var auctionId = $auction.attr('item');
        var buy_now_price = $auction.find('.buy-now-price').text();
        var delivery_cost = $auction.find('.delivery-cost').text();
        var location_text = $auction.find('.location-text').text();

        $('#buy_now_auction_id').val(auctionId);
        $('#buy-now-price').text(buy_now_price);
        $('#delivery-cost').text(delivery_cost);
        $('#delivery-location').text(location_text);
    });

    $(document).on("click", '#mo-buy-now-popup-form input[type="submit"]', function () {
        var auctionId = $('#buy_now_auction_id').val();
        $(document).ajaxStart(function(){
            $(".mo-loading-image").removeClass("hidden");
        });
        $.ajax({
            type: "POST",
            url: base_url+'/trading/buy-now',
            data: {
                _token: token,
                auction_id: auctionId
            },
            success: function(result) {
                if (result == 1) {
                    var magnificPopup = $.magnificPopup.instance;
                    magnificPopup.close();
                    // window.location.replace("/trading/auction");
                    // location.reload();
                }
            },
            complete: function(){
                $(".mo-loading-image").addClass("hidden");
                window.location.replace("/trading/auction");
            }
        });
        return false;
    });

    $(".mo-popup").magnificPopup({
        type:'inline',
        showCloseBtn: false,
        closeOnBgClick: false
    });
}

/**
 * Get locations for Data Access to grant for an user
 */
function showLocation(token, company_group_id) {
    $.ajax({
        type: 'POST',
        data: {
            _token: token,
            company_group_id: company_group_id
        },
        url: base_url+"/mo-admin/location/dataAccessLocation",
        success:function(view) {
            $("#location_section").html(view);
            $("#range_from, #range_to, #include, #exclude").attr('disabled', false);

            var from = parseInt($('#range_from').val());
            var to = parseInt($('#range_to').val());

            var includeStr = $('#include').val();
            var includeArr = includeStr.split(',');
            var includeNum = includeArr.map(function(i) {
                return parseInt(i);
            });

            var excludeStr = $('#exclude').val();
            var excludeArr = excludeStr.split(',');
            var excludeNum = excludeArr.map(function(i) {
                return parseInt(i);
            });
            var seletedLocTmp = [];
            if (from && to && (from < to)) {
                for (var i = from; i <= to; i++) {
                    if (excludeNum.indexOf(i) === -1) {
                        seletedLocTmp.push(i);
                    }
                }
            }
            var seletedLoc = seletedLocTmp.concat(includeNum).sort();
            $('#location_section .checkbox-item').each(function() {
                var loc = parseInt($(this).attr('loc'));
                if (seletedLoc.indexOf(loc) !== -1) {
                    $(this).removeClass('unchecked-checkbox').addClass('checked-checkbox');
                }
            });
        },
        error: function(data){
            console.log('error: '+data);
        }
    });
}

/**
 * Get delader locations for Data Access to grant for an user
 */
function showDealerLocation(token) {
    $.ajax({
        type: 'POST',
        data: {
            _token: token
        },
        url: base_url+"/mo-admin/location/dealerAccessLocation",
        success:function(view) {
            $("#location_section").html(view);
            $("#range_from, #range_to, #include, #exclude").attr('disabled', false);

            var from = parseInt($('#range_from').val());
            var to = parseInt($('#range_to').val());

            var includeStr = $('#include').val();
            var includeArr = includeStr.split(',');
            var includeNum = includeArr.map(function(i) {
                return parseInt(i);
            });

            var excludeStr = $('#exclude').val();
            var excludeArr = excludeStr.split(',');
            var excludeNum = excludeArr.map(function(i) {
                return parseInt(i);
            });
            var seletedLocTmp = [];
            if (from && to && (from < to)) {
                for (var i = from; i <= to; i++) {
                    if (excludeNum.indexOf(i) === -1) {
                        seletedLocTmp.push(i);
                    }
                }
            }
            var seletedLoc = seletedLocTmp.concat(includeNum).sort();
            $('#location_section .checkbox-item').each(function() {
                var loc = parseInt($(this).attr('loc'));
                if (seletedLoc.indexOf(loc) !== -1) {
                    $(this).removeClass('unchecked-checkbox').addClass('checked-checkbox');
                }
            });
        },
        error: function(data){
            console.log('error: '+data);
        }
    });
}

/*** Accounting Library ***/
/*!
 * accounting.js v0.4.2, copyright 2014 Open Exchange Rates, MIT license, http://openexchangerates.github.io/accounting.js
 */
(function(p,z){function q(a){return!!(""===a||a&&a.charCodeAt&&a.substr)}function m(a){return u?u(a):"[object Array]"===v.call(a)}function r(a){return"[object Object]"===v.call(a)}function s(a,b){var d,a=a||{},b=b||{};for(d in b)b.hasOwnProperty(d)&&null==a[d]&&(a[d]=b[d]);return a}function j(a,b,d){var c=[],e,h;if(!a)return c;if(w&&a.map===w)return a.map(b,d);for(e=0,h=a.length;e<h;e++)c[e]=b.call(d,a[e],e,a);return c}function n(a,b){a=Math.round(Math.abs(a));return isNaN(a)?b:a}function x(a){var b=c.settings.currency.format;"function"===typeof a&&(a=a());return q(a)&&a.match("%v")?{pos:a,neg:a.replace("-","").replace("%v","-%v"),zero:a}:!a||!a.pos||!a.pos.match("%v")?!q(b)?b:c.settings.currency.format={pos:b,neg:b.replace("%v","-%v"),zero:b}:a}var c={version:"0.4.1",settings:{currency:{symbol:"$",format:"%s%v",decimal:".",thousand:",",precision:2,grouping:3},number:{precision:0,grouping:3,thousand:",",decimal:"."}}},w=Array.prototype.map,u=Array.isArray,v=Object.prototype.toString,o=c.unformat=c.parse=function(a,b){if(m(a))return j(a,function(a){return o(a,b)});a=a||0;if("number"===typeof a)return a;var b=b||".",c=RegExp("[^0-9-"+b+"]",["g"]),c=parseFloat((""+a).replace(/\((.*)\)/,"-$1").replace(c,"").replace(b,"."));return!isNaN(c)?c:0},y=c.toFixed=function(a,b){var b=n(b,c.settings.number.precision),d=Math.pow(10,b);return(Math.round(c.unformat(a)*d)/d).toFixed(b)},t=c.formatNumber=c.format=function(a,b,d,i){if(m(a))return j(a,function(a){return t(a,b,d,i)});var a=o(a),e=s(r(b)?b:{precision:b,thousand:d,decimal:i},c.settings.number),h=n(e.precision),f=0>a?"-":"",g=parseInt(y(Math.abs(a||0),h),10)+"",l=3<g.length?g.length%3:0;return f+(l?g.substr(0,l)+e.thousand:"")+g.substr(l).replace(/(\d{3})(?=\d)/g,"$1"+e.thousand)+(h?e.decimal+y(Math.abs(a),h).split(".")[1]:"")},A=c.formatMoney=function(a,b,d,i,e,h){if(m(a))return j(a,function(a){return A(a,b,d,i,e,h)});var a=o(a),f=s(r(b)?b:{symbol:b,precision:d,thousand:i,decimal:e,format:h},c.settings.currency),g=x(f.format);return(0<a?g.pos:0>a?g.neg:g.zero).replace("%s",f.symbol).replace("%v",t(Math.abs(a),n(f.precision),f.thousand,f.decimal))};c.formatColumn=function(a,b,d,i,e,h){if(!a)return[];var f=s(r(b)?b:{symbol:b,precision:d,thousand:i,decimal:e,format:h},c.settings.currency),g=x(f.format),l=g.pos.indexOf("%s")<g.pos.indexOf("%v")?!0:!1,k=0,a=j(a,function(a){if(m(a))return c.formatColumn(a,f);a=o(a);a=(0<a?g.pos:0>a?g.neg:g.zero).replace("%s",f.symbol).replace("%v",t(Math.abs(a),n(f.precision),f.thousand,f.decimal));if(a.length>k)k=a.length;return a});return j(a,function(a){return q(a)&&a.length<k?l?a.replace(f.symbol,f.symbol+Array(k-a.length+1).join(" ")):Array(k-a.length+1).join(" ")+a:a})};if("undefined"!==typeof exports){if("undefined"!==typeof module&&module.exports)exports=module.exports=c;exports.accounting=c}else"function"===typeof define&&define.amd?define([],function(){return c}):(c.noConflict=function(a){return function(){p.accounting=a;c.noConflict=z;return c}}(p.accounting),p.accounting=c)})(this);
/*** End Accounting Library ***/