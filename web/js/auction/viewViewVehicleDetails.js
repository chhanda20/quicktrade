$(function() {

    var pathname = window.location.pathname;
    var hash = window.location.hash;

    /** TOGGLE FIRST BLOCK **/
    $('.mo-block-border-bottom').first().toggleClass('mo-block-border-bottom-active');

    /** TOGGLE BY HASH IN URL **/
    function toggleTab(tab_id, tab_block){
        $('.mo-block-border-bottom').removeClass('mo-block-border-bottom-active');
        tab_id.toggleClass('mo-block-border-bottom-active');
        $('.mo-block-inside-list').hide();
        tab_block.hide();
        tab_block.removeClass('hidden').fadeIn('slow');
    }

    if(hash){
        if (hash.indexOf("#details-model") != -1) {
            toggleTab($('#details-model-tab'), $('#mo-model-specs-block'));
        } else if (hash.indexOf("#details-options") != -1){
            toggleTab($('#details-options-tab'), $('#mo-options-extras-block'));
        }
    }

    /** BLOCKS TOGGLE **/
    $('.mo-block-border-bottom').click(function() {
        $('.mo-block-border-bottom').removeClass('mo-block-border-bottom-active');
        $(this).toggleClass('mo-block-border-bottom-active');

        var mo_txt = $(this).find('.mo-block-txt').text();

        $('.mo-block-inside-list').hide();

        if(mo_txt.toLowerCase() == 'model specs & standard features') {
            $('#mo-model-specs-block').hide();
            $('#mo-model-specs-block').removeClass('hidden').fadeIn('slow');
        } else if(mo_txt.toLowerCase() == 'options & extras') {
            $('#mo-options-extras-block').hide();
            $('#mo-options-extras-block').removeClass('hidden').fadeIn('slow');
        }
    });

    /** VEHICLE THUMBS IMAGE SLIDER **/
    var light_slider = $("#bx-pager").lightSlider({
        pager: false,
        item: 5
    });

    /** VEHICLE IMAGE SLIDER **/
    $('.bxslider').bxSlider({
        adaptiveHeight: true,
        mode: 'fade',
        pagerCustom: '#bx-pager',
        onSliderLoad: function(){
            light_slider.refresh();
        }
    });



    preBidPopup();
    buyNowPopup();

    $(document).on('click', '.mo-watch-icon', function(event) {
        event.preventDefault();
        var _this = $(this).parent();
        var auctionId = $(this).parents('.auction-item').attr('item');
        var token = $("input[name='_token']").val();

        if ( _this.find('.vehicle-btn-active').length == 1) {
            $.ajax({
                type: "DELETE",
                url: '/trading/watching-list',
                data: {
                    _token: token,
                    auction_id: auctionId
                },
                success: function(result) {
                    if (result) {
                        _this.find('.vehicle-btn-active').removeClass('vehicle-btn-active').addClass('vehicle-btn-inactive');
                    }

                },
                complete: function(){
                }
            });

        } else {
            $.ajax({
                type: "POST",
                url: '/trading/watching-list',
                data: {
                    _token: token,
                    auction_id: auctionId
                },
                success: function(result) {
                    if (result) {
                        _this.find('.vehicle-btn-inactive').removeClass('vehicle-btn-inactive').addClass('vehicle-btn-active');
                    }
                },
                complete: function(){
                }
            });
        }
    });
});