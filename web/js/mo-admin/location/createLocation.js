/** AUTO FILL STATE DEPEND ON SELECTED COUNTRY **/
console.log("access");
$(document).ready(function(){
    function loadDealership(callback){
        var token = $("input[name='_token']").val();
        var company_group = $("#location-dealergroup").val();

        console.log(token);
        console.log(company_group);
        $.ajax({
            type: "POST",
            url: "/dealership/get-by-group",
            dataType: 'json',
            data: {
                _token: token,
                dealer_group: company_group
            },
            success: function(data){
                console.log(data);
                // $("#location-dealership").find("option:gt(0)").remove();
                $("#location-dealership").empty();
                $.each(data, function(key,value){
                    $('#location-dealership')
                        .append($("<option></option>")
                            .attr("value", value.id)
                            .text(value.company_name))
                });

            },
            error: function(data){
                console.log("error: " + data);
            },
            complete: function () {
                if(typeof callback === 'function' && callback()){
                    callback();
                };
            }

        });

    }

    $("#location-code").keyup(function(e){
        var $this = $(this);
        $this.val($this.val().replace(/^[^1-9]+/,''));
        if($this.val() > 100000 && e.keyCode != 46 && e.keyCode != 8){
            $this.val(100000);
        }
    });

    var country_list = jQuery.parseJSON($("#country-list").val());
    $("#location-state option[value]").remove();
    $.each(country_list, function(index, value){
        $.each(value.stateList, function(index2, value2){
            if(parseInt($("#location-country").val()) == parseInt(value2.country_id)){
                if(parseInt($("input[name='selected-state']").val()) == parseInt(value2.id)){
                    $("#location-state").append($("<option>", {
                        value: value2.id,
                        text: value2.name,
                        selected: true
                    }));
                }
                else{
                    $("#location-state").append($("<option>", {
                        value: value2.id,
                        text: value2.name
                    }));
                }
            }
        });
    });

    //SET COUNTRY ID FOR HIDDEN INPUT
    $("#input-country-id").val($("#location-country").val());


    loadDealership(function () {
        if($("#selected-dealership").val() != ""){
            $("select#location-dealership").val($("#selected-dealership").val());
        }
    });

    $("#location-dealergroup").on("change", function(){
        loadDealership();
    });

});


/*$("#location-country").on("change", function(){
    var country_list = jQuery.parseJSON($("#country-list").val());
    $("#location-state option[value]").remove();
    $.each(country_list, function(index, value) {

        $.each(value.stateList, function(index2, value2) {

            if (parseInt($("#location-country").val()) == parseInt(value2.country_id)) {

                $("#location-state").append($("<option>", {
                    value: value2.id,
                    text: value2.name
                }));
            }

        });
    });
});*/


/** VALIDATE FORM **/
$("#mo-create-location-form").validate({
    rules: {
        location_dealergroup: "required",
        location_dealership: "required",
        location_name: "required",
        location_code: "required",
        // location_lmct: "required",
        location_address: "required",
        location_suburb: "required",
        location_country: "required",
        location_state: "required"
    },
    highlight: function(element) {
        $(element).closest('.input-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.input-group').removeClass('has-error');
    },
    messages:{
        location_dealergroup: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location name</span>"+"</div>",
        location_dealership: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location name</span>"+"</div>",
        location_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location name</span>"+"</div>",
        location_code: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location code</span>"+"</div>",
        // location_lmct: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location lmct</span>"+"</div>",
        location_address: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location address</span>"+"</div>",
        location_suburb: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location suburb</span>"+"</div>",
        location_country: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location country</span>"+"</div>",
        location_state: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location state</span>"+"</div>"
    },
    // submitHandler: function(form){
    //      form.submit();
    // }
});


$("#location-save").click(function(e){

    /* Validate Form when submit */
    if(!$("#mo-create-location-form").valid()){
        return false;
    }

    e.preventDefault();
    var token = $("input[name='_token']").val();
    var location_id = $("#mo-location-id").val();
    var new_or_edit = $("#new-or-edit").val();
    var url = "";

    if(new_or_edit == "new"){
        url = "/mo-admin/save-location";
    }else if(new_or_edit == "edit"){
        url = "/mo-admin/save-location/" + location_id;
    }

    console.log(url);

    $(document).on("ajaxStart.updateLocation", function () {
        $('.mo-loading-image').removeClass("hidden");
    });
    $.ajax({
        _token: token,
        type: "POST",
        dataType: 'json',
        url: url,
        data: $("#mo-create-location-form").serialize(),
        success: function(data){
            console.log(data);
            if(data == 1){
                // $(".alert-success").removeClass("hidden");
                window.location.replace("/mo-admin/location-list");
            }else{
                //error
                $(".alert-danger").find(".mo-error-message").text(data);
                $(".alert-danger").removeClass("hidden");
            }
        },
        error: function(data){
            //console.log(data)
        },
        complete: function () {
            $('.mo-loading-image').addClass("hidden");
        }
    });
});


