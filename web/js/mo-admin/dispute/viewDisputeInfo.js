$(document).ready(function(){
    var pathname = window.location.pathname;

    /**** PRE-DEFINED FUNCTION ****/
    function formatDate(date) {
        date = new Date(date);
        var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul",
            "Aug", "Sep", "Oct",
            "Nov", "Dec"
        ];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();
        var hour = date.getHours();
        var minute = date.getMinutes();
        var second = date.getSeconds();

        return day + monthNames[monthIndex] + year + ' ' + hour + ':' + minute + ':' + second;
    }

    function calculateTotalCostVariance(){
        var total_original_variance = parseFloat($("#mo-original-cost-variance").text().replace(/,/g,''));
        var total_addtional_cost = parseFloat($("#mo-total-additional-cost").text().replace(/,/g,''));
        var total_addtional_variance = parseFloat($("#mo-additional-cost-variance").text().replace(/,/g,''));
        var total_cost_variance = (total_original_variance+total_addtional_cost-total_addtional_variance).toLocaleString();
        $("#mo-total-cost-variance").text(total_cost_variance);
    }

    /**** TOGGLE - SHOW COST VARIANCE SECTION ****/
    $('#dispute-decision').change(function (){
        if($('#dispute-decision').find(":selected").val() == 1){
            $('#mo-dispute-cost-variance').hide();
            $('#mo-dispute-cost-variance').show().fadeIn('slow');
        } else {
            $('#mo-dispute-cost-variance').hide().fadeOut('slow');
        }
    });
    //$('#dispute-decision').trigger('change');/** TRIGGER WHEN ON LOAD **/
    if($('#dispute-decision').data("decision-id") == 1){
        $('#mo-dispute-cost-variance').hide();
        $('#mo-dispute-cost-variance').show().fadeIn('slow');
    }

    /**** NOTES ****/
    var token = $("input[name='_token']").val();/** COMMON TOKEN **/

    /** CLONE NOTE **/
    $('#mo-plus-icon').click(function(){
        var first_span_clone = $(".mo-dispute-note:first").clone();
        first_span_clone.removeClass("hidden");

        var last_span = $(".mo-dispute-note:last");

        $(".mo-error-wrapper").hide();
        if(last_span.find("textarea.mo-dispute-txt").val()!='' || $(".mo-dispute-note").length == 1){
            last_span.after(first_span_clone);
            $(".mo-dispute-txt").prop('readonly', true);/** set all textarea read-only **/
            $(".mo-dispute-txt:last").prop('readonly', false);
        }else{
            $(".mo-dispute-txt:last").prop('readonly', false);
            $(".mo-error-wrapper").hide();
            last_span.find(".mo-error-wrapper").removeClass('hidden').fadeIn('slow');
        }
    });

    /** SHOW ICON OK ONLY WHEN CREATING NEW NOTE **/
    $(document).on("input",".mo-dispute-txt",function() {
        if($(this).val() != ""){
            $(this).siblings(".mo-icon-ok").show().fadeIn('slow');
        }else{
            $(this).siblings(".mo-icon-ok").hide().fadeOut('slow');
        }
    });

    /** REMOVE NOTE **/
    $(document).on("click",".mo-icon-remove",function() {
        var note_id = $(this).siblings(".mo-dispute-txt").data("dispute-note-id");
        var this_elem = $(this);

        if(note_id != ""){
            $(document).off(".createNote");
            $(document).on("ajaxStart.deleteNote", function () {
                $(".mo-loading-image").removeClass("hidden");
            });
            $.ajax({
                type: 'POST',
                data: {
                    _token: token,
                    dispute_id: $("#dispute-id").val(),
                    note_id: note_id
                },
                url: "/mo-admin/dispute/delete-note",
                success: function (data) {
                    console.log(data);
                    if(data == 1){
                        this_elem.parent().remove();
                    }
                },
                error: function (data) {
                    console.log('error: ' + data);
                },
                complete: function () {
                    $(".mo-loading-image").addClass("hidden");
                }
            });
        }else{
            this_elem.parent().remove();
        }

    });

    /** ADD NOTE **/
    $(document).on("click",".mo-icon-ok",function() {
        var note = $(this).siblings(".mo-dispute-txt").val();
        var this_elem = $(this);
        $(document).off(".deleteNote");
        $(document).on("ajaxStart.createNote", function () {
            $(".mo-loading-image").removeClass("hidden");
        });
        $.ajax({
            type: 'POST',
            data: {
                _token: token,
                dispute_id: $("#dispute-id").val(),
                note: note
            },
            url: "/mo-admin/dispute/create-note",
            success: function (data) {
                console.log(data);
                if(data != null){
                    data = $.parseJSON(data);
                    this_elem.parent(".mo-dispute-note").find(".mo-dispute-txt").attr("data-dispute-note-id", data.id);
                    this_elem.parent(".mo-dispute-note").find(".mo-dispute-note-time").text(formatDate(String(data.create_date)));
                    this_elem.parent(".mo-dispute-note").find(".mo-dispute-note-by").text("by "+data.creator_first_name+" "+data.creator_last_name);
                    this_elem.hide();
                }

            },
            error: function (data) {
                console.log('error: ' + data);
            },
            complete: function () {
                $(".mo-loading-image").addClass("hidden");
            }
        });
    });

    /**** COST VARIANCE HANDLE ****/
    /** APPLY TOTAL COST IMMEDIATELY **/
    $(".mo-original-dispute-input").on("input", function(){
        var total_repair_cost = 0;
        $(".mo-original-dispute-input").each(function() {
            if($(this).val() != ''){
                total_repair_cost += parseFloat($(this).val());
            }
        });

        $("#mo-original-cost-variance").text(parseFloat(total_repair_cost).toFixed(2));
    });

    $(".mo-additional-dispute-input").on("input", function(){
        /** SET COST FOR HIDDEN SPAN TAB - USING FOR CALCULATE **/
        var total_repair_cost = 0;
        $(".mo-additional-dispute-input").each(function() {
            if($(this).val() != ''){
                total_repair_cost += parseFloat($(this).val());
            }
        });

        $("#mo-additional-cost-variance").text(parseFloat(total_repair_cost).toFixed(2));

        /** MINUS COST **/
        var total_additional_cost = parseFloat($("#mo-total-additional-cost").text().replace(/,/g,''));
        var total_additional_repair_cost = total_additional_cost - parseFloat(total_repair_cost).toFixed(2);
        $("#mo-additional-cost-variance-display").text(parseFloat(total_additional_repair_cost).toFixed(2));
    });

    /** CALCULATE TOTAL COST VARIANCE **/
    calculateTotalCostVariance();

    $("#mo-additional-cost-variance, #mo-original-cost-variance").on('DOMSubtreeModified', function () {
        calculateTotalCostVariance();
    });

    /** VALIDATE BEFORE SUBMIT **/
    $('#dispute-resolution-form').validate({
        rules: {
            dispute_decision: "required"
        },
        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        },
        messages:{
            dispute_decision: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please choose decision</span>"+"</div>"
        }
    });

    /**** MAIN BUTTONS ****/
    /** SUBMIT FORM **/
    $("#mo-dispute-submit").click(function (e) {
        e.preventDefault();
        var items = [];
        var general_recon_cost = $("#general-reconditioning-cost").val();

        $(".mo-original-dispute-input[data-item-id!='']").each(function() {
            if($(this).val() != ''){
                items.push({
                    id: $(this).data("item-id"),
                    cost_variance: parseFloat($(this).val()).toFixed(2)
                });
            }
        });

        $(".mo-additional-dispute-input").each(function() {
            if($(this).val() != ''){
                items.push({
                    id: $(this).data("item-id"),
                    cost_variance: parseFloat($(this).val()).toFixed(2)
                });
            }
        });

        $(document).off(".deleteNote");
        $(document).off(".createNote");
        $(document).on("ajaxStart.submitDisputeCost", function () {
            $(".mo-loading-image").removeClass("hidden");
        });

        if($("#dispute-resolution-form").valid()){
            var user_fault_checked = []
            $("input[name='user_fault[]']:checked").each(function ()
            {
                user_fault_checked.push(parseInt($(this).val()));
            });
            //console.log(user_fault_checked);
            $.ajax({
                type: 'POST',
                data: {
                    _token: token,
                    dispute_id: $("#dispute-id").val(),
                    items: items,
                    decision: $("#dispute-decision").val(),
                    general_recondition_cost: general_recon_cost,
                    transaction_id: $("#transaction-id").val(),
                    user_fault: user_fault_checked
                },
                url: "/mo-admin/dispute/resolve",
                success: function (data) {
                    console.log(data);
                    if(data == 1){
                        window.location.href = "/mo-admin/dispute/show#resolved";
                    }else{
                        $(".alert-danger").find(".mo-error-message").html(data);
                        $(".alert-danger").removeClass("hidden");
                    }
                },
                error: function (data) {
                    console.log('error: ' + data);
                },
                complete: function () {
                    $(".mo-loading-image").addClass("hidden");
                }
            });
        }

    });

    /** REQUEST UNDER REVIEW **/
    $("#mo-dispute-under-review").click(function (e) {
        e.preventDefault();
        var user_fault_checked = [];
        $("input[name='user_fault[]']:checked").each(function ()
        {
            user_fault_checked.push(parseInt($(this).val()));
        });
        console.log("user_fault_checked "+user_fault_checked);
        //debugger;

        $(document).off(".deleteNote");
        $(document).off(".createNote");
        $(document).off(".submitDisputeCost");
        $(document).on("ajaxStart.underReviewDispute", function () {
            $(".mo-loading-image").removeClass("hidden");
        });
        $.ajax({
            type: 'POST',
            data: {
                _token: token,
                dispute_id: $("#dispute-id").val(),
                user_fault: user_fault_checked
            },
            url: "/mo-admin/dispute/under-review",
            success: function (data) {
                console.log(data);
                window.location.href = "/mo-admin/dispute/show#under-review";
            },
            error: function (data) {
                console.log('error: ' + data);
            },
            complete: function () {
                $(".mo-loading-image").addClass("hidden");
            }
        });
    });

    /** VIEW VALUATION **/
    $("#mo-view-valuation").click(function (e) {
        if(pathname.indexOf("/mo-admin") != -1){
            window.location.href = "/mo-admin/review-valuation/"+$("input#valuation-id").val();
        }else{
            window.location.href = "/review-valuation/"+$("input#valuation-id").val();
        }
    });

    /**** LOAD ITEM IMAGE ****/
    $(".mo-item-img").each(function() {
        var this_elem = $(this);
        var image_upload = "/images/perform_valuation/image_upload.png";
        var azure_account_name = $("input#azure-accout-name").val();
        var valuation_id = $("input#valuation-id").val();
        var valuation_version = $("input#valuation-version").val();
        var image_url = "";
        //var image_half_url = "https://"+azure_account_name+".blob.core.windows.net/inspection-item/"+1572+"_"+7+"_"+42;
        var image_half_url = "https://"+azure_account_name+".blob.core.windows.net/inspection-item/"+valuation_id+"_"+valuation_version+"_"+this_elem.attr("data-item-id");
        //console.log("image_half_url "+this_elem.attr("data-item-id"));

        /* LOAD 1 IN 3 IMAGES */
        for(var i = 1; i < 4; i++){
            image_url = image_half_url+"_"+i+".jpg";
            $.get(image_url).done(function() {
                // Do something now you know the image exists.
                //console.log("found "+this.url);
                this_elem.attr("src", this.url);
            });
        }
    });

    $(".mo-dispute-item-img").each(function() {
        var this_elem = $(this);
        var image_upload = "/images/perform_valuation/image_upload.png";
        var azure_account_name = $("input#azure-accout-name").val();
        var dispute_id = $("input#dispute-id").val();
        var image_url = "";
        var image_half_url = "https://"+azure_account_name+".blob.core.windows.net/dispute-item/"+dispute_id+"_"+this_elem.attr("data-item-id");
        console.log("image_half_url "+image_half_url);

        /* LOAD 1 IN 3 IMAGES */
        for(var i = 1; i < 4; i++){
            image_url = image_half_url+"_"+i+".jpg";
            $.get(image_url).done(function() {
                // Do something now you know the image exists.
                //console.log("found "+this.url);
                this_elem.attr("src", this.url);
            });
        }
    });

    /** POPUP **/
    $(document).on("click",".mo-cancel-btn",function() {
        var magnificPopup = $.magnificPopup.instance;
        magnificPopup.close();
    });

    $(".mo-popup").magnificPopup({
        type:'inline',
        showCloseBtn: true,
        closeOnBgClick: false
    });

    /** POPULATE DATA - EDIT INSPECTION ITEM POPUP **/
    $(document).on("click","#mo-edit-popup",function() {
        /* GET SECTION TYPE LIST */
        var section = $(this).attr("data-section");
        var section_type = $(this).attr("data-section-type");
        $(document).off(".createNote");
        $(document).off(".deleteNote");
        $(document).off(".submitDisputeCost");
        $(document).off(".underReviewDispute");
        $.ajax({
            type: "GET",
            url: "/get-inspection-items-list/"+section,
            data: null,
            success: function(data) {
                data = $.parseJSON(data);
                //console.log(data);
                $("#inspection-sections-select").empty();
                $.each(data, function (index, value) {
                    //console.log(index + ':' + value["section_name"]);
                    $("#inspection-sections-select").append($("<option>", {
                        value: value.section_id,
                        text: value.section_name
                    }));
                });
                $("select#inspection-sections-select option[value='"+section_type+"']").attr("selected","selected");
            }
        });

        /* GET INSPECTION ITEMS LIST */
        var inspection_id = $(this).attr("data-inspection-id");
        var repair_cost = $(this).attr("data-repair-cost");
        var note = $(this).attr("data-note");
        var image_type = $(this).attr("data-image-type");
        var azure_account_name = $("input#azure-accout-name").val();
        var valuation_id = $("input#valuation-id").val();
        var valuation_current_version = $("input#valuation-version").val();
        var dispute_id = $("input#dispute-id").val();
        $.ajax({
            type: "POST",
            url: "/get-items/"+section,
            data: {
                _token: token,
                valuation_id: valuation_id
            },
            success: function(data) {
                //console.log(data);
                data = $.parseJSON(data);
                $("#inspection-items-select").empty();
                $.each(data, function (index, value) {
                    $.each(value.items, function (i, v) {
                        $("#inspection-items-select").append($("<option>", {
                            value: v.inspection_item_id,
                            text: v.item
                        }));
                    });
                });

                /* POPULATE DATA */
                $("#inspection-item-id").val(inspection_id);
                $("select#inspection-items-select option[value='"+inspection_id+"']").attr("selected","selected");
                $("#repair-cost").val(repair_cost);
                $("#note").val(note);

                /*var image_url = "https://"+azure_account_name+".blob.core.windows.net/inspection-item/"+valuation_id+"_"+valuation_current_version+"_"+inspection_id;*/
                var image_url = "https://"+azure_account_name+".blob.core.windows.net";
                if(image_type == "INSPECTION"){
                    image_url += "/inspection-item/"+valuation_id+"_"+valuation_current_version+"_"+inspection_id;
                }else if(image_type == "DISPUTE"){
                    image_url += "/dispute-item/"+dispute_id+"_"+inspection_id;
                }


                $(".mo-inspection-upload-image-icon").each(function(i, obj) {
                    i += 1;
                    var image_src = image_url+"_"+i+".jpg?"+Math.random();
                    /* CHECK IMAGE EXISTS */
                    $.get(image_src)
                        .done(function() {
                            // Do something now you know the image exists.
                            $(obj).attr("src", image_src);
                        }).fail(function() {
                        // Image doesn't exist - do something else.
                        $(obj).attr("src", "/images/main/no_photo_available.png");
                    });
                });
            }
        });
    });

    /** FAULT CHECKBOX **/
    $("#no-fault").click(function(){
        $(".mo-fault:not(#no-fault)").prop("checked", false);
    });

    $(".mo-fault:not(#no-fault)").click(function(){
        $("#no-fault").prop("checked", false);
    });

});