var pathname = window.location.pathname;
var token = $("input[name='_token']").val();
var limit = 21;

function infinityScroll(data_obj) {
    var data_storage = JSON.parse(localStorage.getItem("elem_to_filter"));
    var content = $(data_storage.content_id);
    var url = data_storage.url;
    var status = data_storage.status;

    var win = $(window);
    win.off('scroll.windowscroll');

    if (typeof(Storage) !== "undefined") {
        localStorage.setItem("vehicle_grid_content_length", 0);
    }

    // Each time the user scrolls
    win.on('scroll.windowscroll', function() {
        // End of the document reached?
        if ($(document).height() - win.height() == win.scrollTop()) {
            var content_length = content.children(":not(.mo-loading-wrapper)").length;
            if(content_length > localStorage.getItem("vehicle_grid_content_length" )){
                localStorage.setItem("vehicle_grid_content_length", content_length);
                var token = $("input[name='_token']").val();
                $(document).off(".updateStatus");
                $(document).on("ajaxStart.scroll", function () {
                    $('.mo-loading-wrapper').show();
                    $('.mo-loading-icon').show();
                });

                var data = {
                    _token: token,
                    limit: limit,
                    offset: content_length,
                    status: status
                };
                if(data_obj != ''){
                    data = $.extend({}, data, data_obj);
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(html) {
                        if(html == ""){
                            win.off('scroll.windowscroll');
                        } else {
                            content.append(html);
                        }
                    },
                    complete: function(){
                        $(".mo-loading-icon").parent().not(":last").remove();
                    }
                });

                $(document).on("ajaxStop.scroll", function () {
                    $('.mo-loading-icon').hide();
                    $('.mo-loading-wrapper').hide();
                });
            }
        }
    });
}

function callAjaxGetList(content, param_obj, url){
    if(localStorage.getItem("apply_filter")){
        var data_obj = JSON.parse(localStorage.getItem("apply_filter"));
        if(data_obj !== null && typeof data_obj === 'object'){
            var data = $.extend({}, data_obj, param_obj);
            localStorage.setItem("apply_filter", JSON.stringify(data));
        }
    }else{
        localStorage.setItem("apply_filter", JSON.stringify(param_obj));
    }
    getVehicleList(content, url);
    var scroll_data_obj = JSON.parse(localStorage.getItem("apply_filter"));
    infinityScroll(scroll_data_obj);
}

function getVehicleList(content, url){
    var token = $("input[name='_token']").val();
    var data = {
        _token: token,
        limit: limit,
        offset: 0
    };
    if(localStorage.getItem("apply_filter")){
        var data_obj = JSON.parse(localStorage.getItem("apply_filter"));

        if(data_obj !== null && typeof data_obj === 'object'){
            data = $.extend({}, data, data_obj);
        }
    }
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function(html) {
            content.empty();
            content.append(html);
        }
    });
}