/**
 * Created by Son on 6/7/2017.
 */
/************ SHOW PERMISSION ITEM FROM PERMISSION CATEGORY **************/

$(document).ready(function(){
    // $(".mo-role-category").on("change", function(){
    //     var id = $(this).data("id");
    //     if($(this).is(":checked")) {
    //         console.log(id);
    //         $(".mo-role-list-item[data-category-id = "+id+"]").removeClass("hidden");
    //     }
    //     else{
    //         $(".mo-role-list-item[data-category-id = "+id+"]").addClass("hidden");
    //     }
    // });

    $(".mo-role-category").click(function(){
        var id = $(this).data("id");
        if($(this).bind('click')){
            $(".mo-role-list-item[data-category-id = "+id+"]").toggleClass("hidden");
        }

    });

    $(".mo-menu-category").click(function(){
        var id = $(this).attr("id");
        if($(this).bind("click")){
            $(".mo-menu-list-item[data-id="+id+"]").toggleClass("hidden");
        }
    });

});

$("#mo-add-role-form").validate({
    rules: {
        role_name: "required",
        "role_permission_list[]": {
            required: true
        },
        "menu_items_list[]":{
            required: true
        }
    },
    errorElement: 'div',
    errorClass: 'mo-error-wrapper',
    errorPlacement: function(error, element) {
        if (element.is(":checkbox") && element.attr("name") == "role_permission_list[]") {
            error.appendTo("#errorToShow");
        }else if(element.is(":checkbox") && element.attr("name") == "menu_items_list[]"){
            error.appendTo("#error_To_Show");
        }
        else {
            error.insertAfter(element);   // default placement
        }
    },
    messages:{
        role_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter role name</span>" + "</div>",
        "role_permission_list[]":{
            required: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please select at least one permission</span>" + "</div>"
        },
        "menu_items_list[]": {
            required: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please select at least one item menu access</span>" + "</div>"
        }
    }
});

var permissions = $(".mo-permission").length;
var menu_item = $(".mo-menu-item").length;
$("#mo-save-role").click(function(e){
    if(!$("#mo-add-role-form").valid()){
        return false;
    }
    else{
        if($(".mo-permission:checkbox:checked").length == 0){
            e.preventDefault();
            $("#errorToShow").html("<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please select at least one permission</span>" + "</div>");
        }else if($(".mo-menu-item:checkbox:checked").length == 0){
            e.preventDefault();
            $("#error_To_Show").html("<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please select at least one item menu access</span>" + "</div>");
        } else {
            $("#mo-add-role-form").on("submit", function(){
                $(".mo-loading-image").removeClass("hidden");
            });
        }
    }
});

/*$("#mo-add-role-form").submit(function(e){
    e.preventDefault();
    var token = $("input[name='_token']").val();
    var role_id = $("#mo-role-id").val();
    $(document).ajaxStart(function(){
        $(".mo-loading-image").removeClass("hidden");
    });
    $.ajax({
        _token: token,
        type: "POST",
        url: "/mo-admin/update-role/" +  role_id,
        data: $("#mo-add-role-form").serialize(),
        success: function(data){
            if(data == 1){
                console.log("success");
                $(".alert-success").removeClass("hidden");
                // window.location.replace("/mo-admin/role-list");
            }
        },
        error: function(data){
            console.log(data);
        },
        complete: function(){
            $(".mo-loading-image").addClass("hidden");
        }
    });
});*/

