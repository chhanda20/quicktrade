/** HIDE UNUSED FILTER AND SORT - INHERITED FROM TRADING **/
$("#mo-filter-wrapper, #mo-sort-wrapper").hide();

/** SAVE SETTINGS **/
$("#auction-save-setting").click(function(e){
    e.preventDefault();
    console.log('start');
    $(document).ajaxStart(function() {
        $(".mo-loading-image").removeClass("hidden");
    });
    var token = $('input[name="_token"]').val();
    $.ajax({
        type: "POST",
        url: "/mo-admin/trading/update-trading-setting",
        data: {
            _token: token,
            auction_default_duration: $("#default-duration").val(),
            auction_price_increment: $("#price-increment").val()
        },
        success: function(data){
            if(data == 1){
                $(".alert-success").removeClass("hidden");
            }
            else{
                $(".alert-danger").removeClass("hidden").html(data);
            }

        },
        error: function(data){
            //console.log(data);
        },
        complete: function(){
            $('.mo-loading-image').addClass("hidden");
        }
    });
});