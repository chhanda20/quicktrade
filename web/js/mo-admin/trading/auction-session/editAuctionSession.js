/**
 * Created by Son on 6/8/2017.
 */

// $("#mo-auction-ad-hoc-form").removeAttr("action").attr("action", "/mo-admin/update-auction-session/"+$("#mo-auction-session-id").val());
// $("#mo-auction-ad-hoc-form").removeAttr("id").attr("id", "mo-edit-auction-ad-hoc-form");

$("#mo-auction-recurring-form").addClass('hidden');
$(".mo-ad-hoc-checkbox").addClass('hidden');
$('.mo-ad-hoc').removeClass('hidden');


var date = new Date();
date.setDate(date.getDate());
$("#ad-hoc-date-picker").datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    orientation: 'bottom',
    startDate: date
});

$(".mo-ad-hoc-duration").click(function () {
    $(".mo-ad-hoc-duration").not(this).prop('checked', false);
    if ($('input[name="checkbox_ad_hoc_custom"]').is(":checked")) {
        $("#input-ad-hoc-custom").removeAttr('disabled');
        $("#input-ad-hoc-default").attr('disabled', true);
    } else {
        $("#input-ad-hoc-custom").attr('disabled', true);
    }

    if ($('input[name="checkbox_ad_hoc_default"]').is(":checked")) {
        // $("#input-ad-hoc-custom").removeAttr('disabled');
        $("#input-ad-hoc-default").removeAttr('disabled');
    } else {
        $("#input-ad-hoc-default").attr('disabled', true);
    }
});

/** Check box for Ad-hoc Bid Increment **/
$(".mo-ad-hoc-bid-increment").click(function(){

    $(".mo-ad-hoc-bid-increment").not(this).prop('checked', false);
    if ($('input[name="checkbox_ad_hoc_bid_custom"]').is(":checked")) {
        $("#input-ad-hoc-bid-custom").removeAttr('disabled');
        $("#input-ad-hoc-bid-default").attr('disabled', true);
    } else {
        $("#input-ad-hoc-bid-custom").attr('disabled', true);
    }

    if ($('input[name="checkbox_ad_hoc_bid_default"]').is(":checked")) {
        $("#input-ad-hoc-bid-custom").attr('disabled', true);
        $("#input-ad-hoc-bid-default").removeAttr('disabled');
    } else {
        $("#input-ad-hoc-bid-default").attr('disabled', true);
    }
});

// $("#ad-hoc-time").timepicker({
//     'timeFormat': 'H:i'
// });
$('.glyphicon-time').click(function () {
    $('#ad-hoc-time').focus();
});

$("#mo-auction-ad-hoc-form").validate({
    rules: {
        start_date: "required",
        // start_time: "required",
        duration_default: "required",
        duration_custom: "required"
    },
    errorElement: 'div',
    errorClass: 'mo-error-wrapper',
    errorPlacement: function(error, element){
        if(element.attr('name') == 'start_date'){
            error.appendTo("#errorToShow_date");
        }else{
            error.insertAfter(element);
        }
    },
    messages: {
        start_date: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
        // start_time: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
        duration_default: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
        duration_custom: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>"
    }

});


// DISABLE SELECTED TIME WHEN CHOOSE DATE ON EDIT FORM
$('#ad-hoc-date').change(function(){
    console.log("access success");
    $.ajax({
        type: "POST",
        url: "/trading/get-auction-times",
        data: {
            _token: $("input[name='_token']").val(),
            session_date: $(this).val()
        },
        success: function(data) {
            console.log(data);
            if(data){
                time_data = JSON.parse(data);
                console.log(time_data);
                var arr_time_data = Object.keys(time_data).map(function (key) { return time_data[key]; });
                console.log(arr_time_data);
                var x = 30; //minutes interval
                var times = []; // time array
                var tt = 0; // start time

                // loop to increment the time and push results in array
                for (var i=0;tt<24*60; i++) {
                    var hh = Math.floor(tt/60); // getting hours of day in 0-24 format
                    var mm = (tt%60); // getting minutes of the hour in 0-55 format
                    times[i] = ("0" + (hh % 24)).slice(-2) + ':' + ("0" + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
                    tt = tt + x;
                }

                $("#start-time-select").empty();
                var disabled_option = false;
                $.each(times, function (index, value) {
                    // console.log(index + ':' + value["section_name"]);
                    for (var i=0; i<arr_time_data.length; i++){
                        if(arr_time_data[i] == value){
                            disabled_option = true;
                            break;
                        }else{
                            disabled_option = false;
                        }
                    }

                    $("#start-time-select").append($("<option>", {
                        value: value,
                        text: value,
                        disabled: disabled_option
                    }));
                });
            }
            else{
                console.log("data empty");
                var x = 30; //minutes interval
                var times = []; // time array
                var tt = 0; // start time

                // loop to increment the time and push results in array
                for (var i=0;tt<24*60; i++) {
                    var hh = Math.floor(tt/60); // getting hours of day in 0-24 format
                    var mm = (tt%60); // getting minutes of the hour in 0-55 format
                    times[i] = ("0" + (hh % 24)).slice(-2) + ':' + ("0" + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
                    tt = tt + x;
                }

                $("#start-time-select").empty();
                $.each(times, function(index, value){
                    $("#start-time-select").append($("<option>", {
                        value: value,
                        text: value
                    }));
                });
            }


        },
        complete: function(){
        }
    });
});


$(document).ready(function(){
    $.ajax({
        type: "POST",
        url: "/trading/get-auction-times",
        data: {
            _token: $("input[name='_token']").val(),
            session_date: $("#ad-hoc-date").val()
        },
        success: function(data){
            console.log(data);
            if(data){
                time_data = JSON.parse(data);
                console.log(time_data);
                var arr_time_data = Object.keys(time_data).map(function (key) { return time_data[key]; });
                console.log(arr_time_data);
                var x = 30; //minutes interval
                var times = []; // time array
                var tt = 0; // start time

                //loop to increment the time and push results in array
                for (var i=0;tt<24*60; i++) {
                    var hh = Math.floor(tt/60); // getting hours of day in 0-24 format
                    var mm = (tt%60); // getting minutes of the hour in 0-55 format
                    times[i] = ("0" + (hh % 24)).slice(-2) + ':' + ("0" + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
                    tt = tt + x;
                }
                var disabled_option = false;
                $.each(times, function(index, value){
                    for(var i=0; i<arr_time_data.length; i++){
                        if(arr_time_data[i] == value){
                            disabled_option = true;
                            break;
                        }else{
                            disabled_option = false;
                        }
                    }
                    $("#start-time-select").append($("<option>",{
                        value: value,
                        text: value,
                        disabled: disabled_option
                    }));
                });
                // console.log(times.length);
                // $.each(times, function(index, value){
                //     $("#start-time-select").append($("<option>", {
                //         value: value,
                //         text: value
                //     }));
                // });
            }
        }
    });
});


$("#auction-update").click(function(e){
    $("#mo-auction-ad-hoc-form").submit();
});

$("#mo-auction-ad-hoc-form").submit(function(e){
    console.log("access");
    e.preventDefault();
    var token = $('input[name="_token"]').val();
    var session_id = $("#mo-auction-session-id").val();
    $.ajax({
        _token: token,
        type: "POST",
        url: "/mo-admin/trading/update-auction-session/"+session_id,
        data: $("#mo-auction-ad-hoc-form").serialize(),
        success: function(data){
            if(data == 1){
                window.location.replace("/mo-admin/trading/auction-session-list");
            }
            else{
                if(data == "time_in_past"){
                    $(".alert-warning").find(".mo-edit-error-message").html("time in past");
                    $(".alert-warning").removeClass("hidden");
                }else{
                    $(".alert-warning").find(".mo-edit-error-message").html("auction session duplicate");
                    $(".alert-warning").removeClass("hidden");
                }
                // $(".alert-warning").find(".mo-edit-error-message").text(data);
                // $(".alert-warning").removeClass("hidden");
                // console.log(data);
            }
        },
        error: function(data){
            console.log("error");
        }

    });

});

