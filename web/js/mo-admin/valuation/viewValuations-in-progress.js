/** DATA TABLE **/
/** VALUATIONS IN PROGRESS - MO ADMIN **/
$.getValuations = function(destroy, active, init, query, offset) {
    $(document).off(".cancelValuation");
    var id = "#mo-table-valuations-in-progress-list";
    var columns = 	[
        {className: "never"},//id
        {className: "all"}, //make
        {className: "all"}, //model
        {className: "desktop","bSortable": false }, //year
        {className: "all","bSortable": false }, //registration
        {className: "desktop","bSortable": false},//valuer
        {className: "desktop", "bSortable": false},//salesperson
        {className: "desktop", "bSortable": false},//location/franchise
        {className: "desktop", "bSortable": false,
            "mRender": function (data, type, row, meta) {

                return  "<a href='#mo-sensitive-action-confirm-popup' class='open-popup-link'>" +
                    '<input type="button" class="mo-requested-btn text-capitalize cancel-valuation" value="cancel">' +
                    '<input type="hidden" value="'+ row[0] +'" id="in-progress-valuation-id" />' +
                    '</a>'
                    ;
            }
        }
    ];
    var request_url = "/mo-admin/get-valuations-data/in-progress";
    var error_msg = "<div>There are no valuations to display at this time.</div>";
    var scrollX = true;
    $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, active);
}

/*var destroy = true;
var active = true;
var init = true;
var query= null;
var offset=0;
$.getValuations(destroy, active, init, query, offset);*/

var id = "#mo-table-valuations-in-progress-list";
var error_msg = "<div>There are no valuation to display at this time.<br>Please search for a dealership, dealer group or location in the search box.</div>";
$.constructDataTable(id, false, error_msg);

function handleValuationFunction(){
    /** POPUP **/
    var magnificPopup = $.magnificPopup.instance;
    $('.open-popup-link').magnificPopup({
        type:'inline',
        showCloseBtn: true,
        closeOnBgClick: false
    });

    $('.mo-cancel-btn').click(function () {
        magnificPopup.close();
    });

    /** HANDLE CANCEL VALUATION **/
    $(".cancel-valuation").click(function() {
        var valuation_id = $(this).siblings('#in-progress-valuation-id').val();
        var row = $(this).parents('tr');

        $("#mo-valuation-progress-cancel-confirm-btn").data("valuation_id", valuation_id);
        $("#mo-valuation-progress-cancel-confirm-btn").data("row", row);
    });

    $("#mo-valuation-progress-cancel-confirm-btn").click(function(){
        var valuation_id = $(this).data("valuation_id");
        var row = $(this).data("row");

        $(document).on("ajaxStart.cancelValuation", function () {
            $(".mo-loading-image").removeClass("hidden");
        });

        $.ajax({
            type: "GET",
            url: "/cancel-valuation/"+valuation_id,
            success: function(result) {
                if(result == 1) {
                    //row.remove();
                    alert('success');
                    row.hide();
                    magnificPopup.close();F
                }
            },
            error: function() {
                alert('errr');
                console.log('Error occured');
            },
            complete: function () {
                $(".mo-loading-image").addClass("hidden");
            }
        });

    });
}