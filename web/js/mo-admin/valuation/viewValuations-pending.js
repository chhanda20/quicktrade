/** DATA TABLE **/
/** VALUATIONS IN PROGRESS - MO ADMIN **/
$.getValuations = function(destroy, active, init, query, offset) {
    $(document).off(".approveValuation");
    $(document).off(".rejectPrice");
    $(document).off(".approvePrice");
    var id = "#mo-table-valuations-pending-list";
    var columns = 	[
        {
            className: "all pending-more-info", "width": "15%",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                $(nTd).data("status", oData[12]);
            }
        }, //make
        {className: "all", "width": "15%"}, //model
        {className: "desktop tablet-l", "bSortable": false, "width": "10%"}, //year
        {className: "desktop tablet-l", "bSortable": false, "width": "15%"}, //registration
        {className: "desktop tablet-l", "bSortable": false, "width": "15%"},//color
        {className: "never", "bSortable": false},
        {className: "desktop", "bSortable": false, "width": "15%"},//valuer
        {className: "desktop", "bSortable": false, "width": "15%"},//sales
        {className: "none", "bSortable": false},
        {className: "none", "bSortable": false},
        {className: "none", "bSortable": false},
        {className: "none", "bSortable": false},
        {className: "never"},
        {className: "never"},//id
        {
            className: "all", "bSortable": false,
            "mRender": function (data, type, row, meta) {
                if (row[12] == 5) {
                    return '<input type="button" class="mo-approve-price-btn text-capitalize" value="approve price" data-valuation-id="'+row[13]+'">'+
                        '<input type="button" class="mo-reject-price-btn text-capitalize" value="reject" data-valuation-id="'+row[13]+'">';
                } else {
                    return '<a href="#mo-sensitive-action-confirm-popup" class="open-popup-link">' +
                        '<input type="button" class="mo-approve-valuate-btn text-capitalize" value="approve valuation" data-valuation-id="'+row[13]+'">' +
                        '</a>'
                        ;
                }

            }
        },
        {
            className: "desktop", "bSortable": false,
            "mRender": function (data, type, row, meta) {
                return '<a href="/mo-admin/review-valuation/' + row[13] + '"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
            }
        }
    ];
    var request_url = "/mo-admin/get-valuations-data/pending";
    var error_msg = "<div>There are no valuations to display at this time.</div>";
    var scrollX = true;
    $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);
}

/*var destroy = true;
var active = true;
var init = true;
var query= null;
var offset=0;
$.getValuations(destroy, active, init, query, offset);*/

var id = "#mo-table-valuations-pending-list";
var error_msg = "<div>There are no valuation to display at this time.<br>Please search for a dealership, dealer group or location in the search box.</div>";
$.constructDataTable(id, false, error_msg);

function handleValuationFunction(){
    /** POPUP **/
    var magnificPopup = $.magnificPopup.instance;
    $('.open-popup-link').magnificPopup({
        type:'inline',
        showCloseBtn: true,
        closeOnBgClick: false
    });

    $('.mo-cancel-btn').click(function () {
        magnificPopup.close();
    });

    /** HANDLE CANCEL VALUATION **/
    $(".pending-more-info").click(function(){
        if ($(this).data("status") == 4) {
            var row = $(this);
            setTimeout(function(){
                $(row.parent().next(".child").children().children().children()[3]).hide();
                console.log((row.parent().next(".child").children().children().children()[3]));
            }, 200);
        }
    });

    // APPROVE PRICE
    $(".mo-approve-price-btn").click(function() {

        var valuation_id = $(this).data("valuation-id");
        var token = $("input[name='_token']").val();
        var pending_row = $(this).parent().parent();

        $("#mo-valuation-pending-approval-confirmation-btn").data("valuation_id", valuation_id);
        $("#mo-valuation-pending-approval-confirmation-btn").data("token", token);
        $("#mo-valuation-pending-approval-confirmation-btn").data("pending_row", pending_row);
        $.magnificPopup.open({
            items: {
                src: "#mo-sensitive-action-confirm-popup"
            },
            type: 'inline',
            showCloseBtn: true,
            closeOnBgClick: false
        });
        var magnificPopup = $.magnificPopup.instance;
        $('.mo-cancel-btn').click(function () {
            magnificPopup.close();
            // $("#errorToShow").html("");
        });

        $("#mo-valuation-pending-approval-confirmation-btn").click(function(){
            var valuation_id = $(this).data("valuation_id");
            var token = $(this).data("token");
            var pending_row = $(this).data("pending_row");
            $(document).off(".approveValuation");
            $(document).off(".rejectPrice");
            $(document).on("ajaxStart.approvePrice", function () {
                $(".mo-loading-image").removeClass("hidden");
            });
            $.ajax({
                type: "POST",
                url: "/approve-revaluation",
                headers: {"X-CSRF-TOKEN": token},
                data: {"valuation_id" : valuation_id},
                success: function(result) {
                    console.log(result);
                    //pending_row.next("tr").remove();
                    magnificPopup.close();
                    pending_row.remove();
                },
                error: function() {

                },
                complete: function(){
                    $(".mo-loading-image").addClass("hidden")
                }
            });
        });


    });

    // REJECT PRICE
    $(".mo-reject-price-btn").click(function() {
        var valuation_id = $(this).data("valuation-id");
        var token = $("input[name='_token']").val();
        var pending_row = $(this).parent().parent();

        $(document).off(".approveValuation");
        $(document).off(".approvePrice");
        $(document).on("ajaxStart.rejectPrice", function () {
            $(".mo-loading-image").removeClass("hidden");
        });
        $.ajax({
            type: "POST",
            url: "/reject-revaluation",
            headers: {"X-CSRF-TOKEN": token},
            data: {"valuation_id" : valuation_id},
            success: function(result) {
                console.log(result);
                pending_row.next("tr").remove();
                pending_row.remove();

            },
            error: function() {

            }
        });

    });

    // APPROVE VALUATION
    $(".mo-approve-valuate-btn").click(function() {

        var valuation_id = $(this).data("valuation-id");
        var token = $("input[name='_token']").val();
        var pending_row = $(this).parent().parent().parent();

        $("#mo-valuation-pending-approval-confirmation-btn").data("valuation_id", valuation_id);
        $("#mo-valuation-pending-approval-confirmation-btn").data("token", token);
        $("#mo-valuation-pending-approval-confirmation-btn").data("pending_row", pending_row);

    });

    $("#mo-valuation-pending-approval-confirmation-btn").click(function(){
        var valuation_id = $(this).data("valuation_id");
        var token = $(this).data("token");
        var pending_row = $(this).data("pending_row");
        $(document).off(".rejectPrice");
        $(document).off(".approvePrice");
        $(document).on("ajaxStart.approveValuation", function () {
            $(".mo-loading-image").removeClass("hidden");
        });
        $.ajax({
            type: "POST",
            url: "/approve-valuation",
            headers: {"X-CSRF-TOKEN": token},
            data: {"valuation_id" : valuation_id},

            success: function(result) {
                console.log(result);
                var res = $.parseJSON(result);
                if (typeof (res.id) != "undefined") {
                    //pending_row.remove();
                    pending_row.hide();
                    pending_row.next(".child").hide();
                    magnificPopup.close();
                    console.log("Approve Success");
                }
            },

            error: function() {
                console.log("Approve Valuation Error");
            },
            complete: function(){
                $(".mo-loading-image").addClass('hidden');
            }
        });

    });
    // END APPROVE VALUATION
}