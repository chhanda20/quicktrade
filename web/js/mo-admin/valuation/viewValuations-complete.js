/** DATA TABLE **/
/** VALUATIONS COMPLETE - MO ADMIN **/
var path_name = window.location.pathname;
$.getValuations = function(destroy, active, init, query, offset) {

    if (path_name.indexOf("mo-admin") != -1) {
        mo_admin_url = base_url+"/mo-admin";
    }

    $(document).off(".approveValuation");
    $(document).off(".rejectPrice");
    $(document).off(".approvePrice");
    var id = "#mm-table-valuations-complete-list";
    var columns = 	[
        {className: "never"},
        {className: "all"},
        {className: "all"},
        {className: "all", "bSortable": false},
        {className: "desktop tablet-l", "bSortable": false}, //condition change to registration
        {className: "desktop", "bSortable": false},
        {className: "desktop", "bSortable": false},
        {className: "desktop", "bSortable": false},
        {className: "desktop", "bSortable": false},
        {className: "never", "bSortable": false},
        {
            className: "all", "bSortable": false,
            "mRender": function (data, type, row, meta) {
                /** check status of complete list **/
                var dropdown = '<div class="dropdown">' +
                    '<span class="glyphicon glyphicon-option-vertical dropdown-toggle" data-toggle="dropdown"></span>'+
                    '<ul class="dropdown-menu mo-extend-menu-dropdown">';
                var end_dropdown = '</ul></div>';
                if(row[9] == 3){
                    return dropdown +
                        '<li><a href="#mo-popup-valuation-version" class="mo-valuation-version" data-valuation-id="'+row[0]+'">Details</a></li>'+
                        '<li><a href="#mo-popup-changestatus" class="open-popup-link mo-trade" data-valuation-id="'+row[0]+'" data-max-price="'+row[10]+'">Trade</a></li>'+
                        '<li><a href="#mo-re-evaluate-popup" class="open-popup-link mo-re-price" data-valuation-id="'+row[0]+'">Re-Price</a></li>'+
                        end_dropdown;
                }else if (row[9] == 6) {
                    return dropdown +
                        '<li><a href="#mo-popup-valuation-version" class="mo-valuation-version" data-valuation-id="' + row[0] + '">Details</a></li>' +
                        '<li><a href="'+mo_admin_url+'/review-valuation/' + row[0] + '/true#summary">Receive Vehicle</a></li>' +
                        end_dropdown;
                }
                else if (row[9] == 7 || row[9] == 12 || row[9] == 13 || row[9] == 14) {
                    return dropdown +
                        '<li><a href="#mo-popup-valuation-version" class="mo-valuation-version" data-valuation-id="'+row[0]+'" data-valuation-status="'+row[8]+'">Details</a></li>'+
                        end_dropdown;
                }
                else{
                    return null;
                }

            }
        }
    ];
    var request_url = base_url+"/mo-admin/get-valuations-data/complete";
    var error_msg = "<div>There are no valuations to display at this time.</div>";
    var scrollX = true;
    $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);
}

/*var destroy = true;
var active = true;
var init = true;
var query= null;
var offset=0;
$.getValuations(destroy, active, init, query, offset);*/

var id = "#mm-table-valuations-complete-list";
var error_msg = "<div>There are no valuation to display at this time.<br>Please search for a dealership, dealer group or location in the search box.</div>";
$.constructDataTable(id, false, error_msg);

function handleValuationFunction(){
    /** POPUP **/
    var magnificPopup = $.magnificPopup.instance;
    $('.open-popup-link').magnificPopup({
        type:'inline',
        showCloseBtn: true,
        closeOnBgClick: false
    });

    $('.mo-cancel-btn').click(function () {
        magnificPopup.close();
    });

    $("#datetimepicker1").datepicker({
        format: 'yyyy-mm-dd',
        todayHighlight: true,
        orientation: 'bottom',
        autoclose: true
    });

    //var current_item = null;
    $.clearTradePoupForm = function(){
        $("input[name='arrival_date']").val(null);
        $("input[name='trade_in_price']").val(null);
        $("#note").val(null);
        change_status_form_validator.resetForm();
    }

    $(".mo-trade").click(function () {
        //call clear function before binding data
        $.clearTradePoupForm();
        $("#mo-update-trade-button").data("max-price", $(this).data("max-price"));
        $("#mo-popup-changestatus-form").data("max-price", $(this).data("max-price"));

        var max_price = accounting.formatMoney($(this).data("max-price"), "", 2, ",", ".");
        $(".complete-valuation-max-price-number").text(max_price);

        current_item = $(this);
        console.log($(this).data("valuation-id"));
        var valuation_id = $(this).data("valuation-id");

        $("input[name='valuation_id']").val(valuation_id);

        //ajax
        //load valuation price
        var token = $("input[name='_token']").val();
        $.ajax({
            type: "POST",
            url: base_url+"/_get-sales-info",
            headers: {"X-CSRF-TOKEN": token},
            data: {"valuation_id": valuation_id},

            success: function(data){
                data = JSON.parse(data);
                $("#mo-final-trade-in-price").text(data.valuation_price);
            },

            error: function(data){
                console.log("error");
            }
        });
    });

    /******************************* VALUATION VERSION ****************************/
    var valuation_status_selected = null;
    var valuation_id_global = null;
    $(".mo-valuation-version").click(function(e){
        e.preventDefault();
        current_item = $(this);
        console.log($(this).data("valuation-id"));
        var valuation_id = $(this).data("valuation-id");
        valuation_id_global = valuation_id;
        console.log($(this).data("valuation-status"));
        valuation_status_selected = $(this).data("valuation-status");
        // var valuation_status = $(this).data("valuation-status");
        // $("input[name=valuation_status]").val(valuation_status);
        $("input[name='valuation_id']").val(valuation_id);
        $("input[name='valuation_status']").val(valuation_status_selected);

        var token = $("input[name='_token']").val();
        $.ajax({
            type: 'POST',
            url: base_url+'/dealership/get-version',
            headers: {"X-CSRF-TOKEN": token},
            data: {
                "valuation_id": valuation_id,
                "valuation_status": valuation_status_selected
            },
            success: function(data){
                if(data.length != "" ){
                    /*console.log(data);*/
                    if(data == 1){
                        window.location.replace(mo_admin_url+"/review-valuation/"+valuation_id);
                    }
                    else{

                        /** open magnific popup manually **/
                        $.magnificPopup.open({
                            items: {
                                src: "#mo-popup-valuation-version"
                            },
                            type: 'inline',
                            showCloseBtn: true,
                            closeOnBgClick: false
                        });
                        var magnificPopup = $.magnificPopup.instance;
                        $('.mo-cancel-btn').click(function (){
                            magnificPopup.close();
                        });

                    }

                    /**** Start Datatable for valuation version ****/
                    $('#mo-table-valuation-version-list').DataTable({
                        "destroy": true,
                        "info": false,
                        "filter": false,
                        "columnDefs": [
                            {
                                "aTargets": [ 0 ],
                                "visible": true,
                                "searchable": false
                            }
                        ],
                        "order": [[ 1, "desc" ]],
                        data: $.parseJSON(data),
                        columns: [
                            { data: 'valuation_id', className: "never", "bSortable": false },
                            { data: 'version', className: "all", "bSortable": false, "width": "25%" },
                            { data: 'version_creator', className: "all", "bSortable": false, "width": "40%" },
                            { data: 'create_date', className: "all", "bSortable": false, "width": "25%" },
                            { data: 'access', className: "never", "bSortable": false, "width": "25%" },
                            { className: "all", "bSortable": false, "width": "10%",
                                "mRender": function(data, type, row, meta){
                                    //console.debug(row);
                                    return '<a href="'+mo_admin_url+'/review-valuation/'+row['valuation_id']+'" class="mo-valuation-version-redirect" ' +
                                        'data-valuation-id="'+valuation_id_global+'" data-from-version="'+row['version']+'"'+
                                        ' data-valuation-version-id="'+row['valuation_id']+'"' +
                                        ' data-access="'+row['access']+'">' +
                                        '<span class="glyphicon glyphicon-menu-right" ></span></a>'
                                }
                            }
                        ],
                        "lengthChange": false,
                        "paging": false,
                        "responsive": true,
                    });

                    $(".mo-valuation-version-redirect").click(function(e){
                        e.preventDefault();

                        var valuation_id = $(this).data('valuation-id');
                        var valuation_version_id = $(this).data('valuation-version-id');
                        var access = $(this).data('access');
                        var from_version = $(this).data('from-version');
                        $('input[name="valuation_id"]').val(valuation_id);
                        $('input[name="from_version"]').val(from_version);
                        var valuation_status = $('input[name="valuation_status"]').val();

                        var token = $('input[name="_token"]').val();

                        /** CHECK REDIRECT FOR VIEW ONLY IF DIFFERENT VALUER ACCESS **/
                        //console.log(access);
                        if(access == "view"){
                            window.location.replace(mo_admin_url+"/review-valuation/"+valuation_version_id);
                        }else if(access == "create"){
                            $(document).ajaxStart(function() {
                                $(".mo-loading-image").removeClass("hidden");
                            });
                            $.ajax({
                                type: "POST",
                                url: base_url+"/dealership/create-version",
                                headers: {"X-CSRF-TOKEN": token},
                                data: {
                                    "valuation_id": valuation_id,
                                    "valuation_status": valuation_status_selected,
                                    "from_version": from_version
                                },
                                success: function(data){
                                    console.log(data);
                                    if(data == 1) {
                                        // console.log(data);
                                        window.location.replace(mo_admin_url+"/review-valuation/"+valuation_id);
                                    }else{
                                        // location.reload();
                                        $(".alert-danger").removeClass("hidden");
                                    }
                                },
                                error: function(data){
                                    // console.log(data);
                                },
                                complete: function(){
                                    $(".mo-loading-image").addClass("hidden");
                                }
                            });
                            $(document).ajaxStop(function() {
                                $(".mo-loading-image").addClass("hidden");
                            });
                        }

                    });
                }else{
                    window.location.replace(mo_admin_url+"/review-valuation/"+ valuation_id);
                }
            },
            error: function(data){
                // console.log(data);
            }

        });
    });

    /******************************* END VALUATION VERSION ****************************/

    /* TRADE VEHICLE */
    $.validator.addMethod('lessThanEqual', function(value, element, param) {
        return this.optional(element) || parseInt(value) <= parseInt($(param).val());
    });

    var change_status_form_validator = $("#mo-popup-changestatus-form").validate({
        rules: {
            arrival_date: "required",
            trade_in_price: {
                required: true
            }
        },
        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        errorPlacement: function(error, element){

            if(element.attr('name') == 'arrival_date') {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },

        messages: {
            arrival_date: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
            trade_in_price: {
                lessThanEqual: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Price must be less than or equal to max price </span>"+"</div>",
                required: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>"
            }
        }
    });

    $("#mo-update-trade-button").click(function(e) {

        if(!$("#mo-popup-changestatus-form").valid()) {
            return false;
        }

        if($("#mo-final-trade-in-price").val() > $(this).data("max-price")) {
            var error = "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'><span>Price must be less than or equal to max price </span></div>";
            $("#mo-final-trade-in-price").parent().append(error);
            return false;
        }
        //ajax update trade
        var token = $("input[name='_token']").val();
        var valuation_id = $("input[name='valuation_id']").val();
        var note = $("#note").val();
        var trade_in_price = $("#mo-final-trade-in-price").val();
        var arrival_date = $("#arrival-date").val();

        $.ajax({
            type: "POST",
            url: base_url+"/_trade",
            headers: {"X-CSRF-TOKEN": token},
            data: {"valuation_id" : valuation_id, "note" : note,
                "trade_in_price" : trade_in_price,
                "arrival_date" : arrival_date},

            success: function(data) {
                console.log("success");
                console.log(data);
                //set status traded
                current_item.parent().parent().parent().parent().prev().text("Traded");
                magnificPopup.close();
            },

            error: function(data){
                console.log("error");
            }
        });
    });

    $("#mo-final-trade-in-price").blur(function() {
        this.value = parseFloat(this.value.replace(/,/g, ""))
            .toFixed(2)
            .toString()
            .replace(/\B(?=(\d{10})+(?!\d))/g, ",");
    });
    /* end TRADE VEHICLE */

    /* VALUATION CHANGESTATUS */
    $("#mo-re-evaluate-popup").click(function () {
        $("#mo-popup-re-evaluate").validate({
            rules: {
                re_evaluate_price: "required"
            },
            messages: {
                re_evaluate_price: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter re-evaluate</span>" + "</div>"
            },
            submitHandler: function (form) {
                form.submit();
            }

        });
    });

    $("#mo-re-evaluate-save-btn").click(function() {
        //get price
        var price = $("input[name='re_evaluate_price']").val();
        var token = $("input[name='_token']").val();
        var valuation_id = $("input[name='valuation_id']").val();

        //ajax call api
        $.ajax({
            type: "POST",
            url: base_url+"/_re-evaluate",
            headers: {"X-CSRF-TOKEN": token},
            data: {"valuation_id" : valuation_id, "re_evaluate_price" : price},

            success: function(data) {
                current_item.parent().parent().parent().parent().parent().hide()
                console.log("success");
                magnificPopup.close();
            },

            error: function(data) {
                console.log(data);
                console.log("error");
            }

        });
        //done
    });

    $("#estimate-cost").blur(function() {
        this.value = parseFloat(this.value.replace(/,/g, ""))
            .toFixed(2)
            .toString()
            .replace(/\B(?=(\d{10})+(?!\d))/g, ",");
    });
}