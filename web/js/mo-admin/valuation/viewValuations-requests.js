/** DATA TABLE **/
/** VALUATIONS REQUESTS - MO ADMIN **/
var path_name = window.location.pathname;
var mo_admin_url = "";

if (path_name.indexOf("mo-admin") != -1) {
    mo_admin_url = "/mo-admin";
}

$.getValuations = function(destroy, active, init, query, offset) {
    $(document).off(".deleteValuation");
    $(document).off(".getVin");
    var id = "#mo-table-valuations-requests-list";
    var columns = 	[
        {className: "never"},//id
        {className: "all"},//make
        {className: "all"},//model
        {className: "all", "bSortable": false},//year
        {className: "desktop tablet-l", "bSortable": false},//vin-number
        {className: "desktop", "bSortable": false},//colour
        {className: "desktop word-break", "width": "15%", "bSortable": false},//registration
        {className: "desktop", "bSortable": false},//expiry-date
        {className: "desktop", "bSortable": false},//sale-person
        {className: "never", "bSortable": false},
        {
            className: "desktop", "bSortable": false,
            "mRender": function (data, type, row, meta) {
                if(row[9] == "valuation_perform")
                    return '<a href="#mo-enter-vin-popup" class="mo-enter-vin-popup"><input type="button" class="mo-accept-valuate-btn text-capitalize" value="accept"' +
                        'data-valuation-id="'+row[0]+'" data-year="'+row[3]+'" data-vin="'+row[4]+'" data-registration="'+row[6]+'"></a>';
                else
                    return null;
            }
        },
        {
            className: "desktop", "bSortable": false,
            "mRender": function (data, type, row, meta) {
                return "<a href='#mo-sensitive-action-confirm-popup' class='open-popup-link'>" +
                    '<span class="glyphicon glyphicon-trash float-right delete-valuation"></span></a>'+
                    '<input type="hidden" value="'+ row[0] +'" class="request-valuation-id" />';
            }
        }
    ];
    var request_url = "/mo-admin/get-valuations-data/requests";
    var error_msg = "<div>There are no valuations to display at this time.</div>";
    var scrollX = true;
    $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, active);
}

/*var destroy = true;
var active = true;
var init = true;
var query= null;
var offset=0;
$.getValuations(destroy, active, init, query, offset);*/

var id = "#mo-table-valuations-requests-list";
var error_msg = "<div>There are no valuation to display at this time.<br>Please search for a dealership, dealer group or location in the search box.</div>";
$.constructDataTable(id, false, error_msg);

function handleValuationFunction(){
    /** POPUP **/
    var magnificPopup = $.magnificPopup.instance;
    $('.open-popup-link').magnificPopup({
        type:'inline',
        showCloseBtn: true,
        closeOnBgClick: false
    });

    $('.mo-cancel-btn').click(function () {
        magnificPopup.close();
    });

    /** HANDLE DELETE VALUATION **/
    $(".delete-valuation").click(function() {
        var valuation_id = $(this).parent().siblings('.request-valuation-id').val();
        var row = $(this).parents('tr');
        $("#mo-confirm-btn").data("valuation_id", valuation_id);
        $("#mo-confirm-btn").data("row", row);
    });

    //delete valuation
    $("#mo-confirm-btn").click(function(){
        var valuation_id = $(this).data("valuation_id");
        var row = $(this).data("row");

        $(document).off(".getVin");
        $(document).off(".performValuation");
        $(document).on("ajaxStart.deleteValuation", function () {
            $(".mo-loading-image").removeClass("hidden");
        });

        $.ajax({
            type: "GET",
            url: "/delete-valuation/"+valuation_id,
            success: function(result){
                if(result == 1) {
                    //row.remove();
                    row.hide();
                    magnificPopup.close();
                }
            },
            error: function() {
                console.log('Delete valuation error occured');
            },
            complete: function () {
                $(".mo-loading-image").addClass("hidden");
            }
        });

    });


    /** ENTER VIN **/
    var magnificPopup = $.magnificPopup.instance;
    $('.mo-enter-vin-popup').magnificPopup({
        type:'inline',
        showCloseBtn: false,
        closeOnBgClick: false,
        callbacks: {
            beforeClose: function(){
                enterVinValidator.resetForm();
            }
        }
    });

    $('.mo-cancel-btn').click(function (){
        magnificPopup.close();
    });

    $("#vin-confirm").click(function (){

        $("input[name='year']").rules('add', {
            required: true
        });
        $("input[name='vin']").rules('add', {
            required: true
        });
        $("input[name='registration-number']").rules('add', {
            required: true
        });

        if($("#mo-popup-enter-vin").valid()) {
            //accept valuation here

            $(document).off(".getVin");
            $(document).off(".deleteValuation");
            $(document).on("ajaxStart.performValuation", function () {
                $(".mo-loading-image").removeClass("hidden");
            });

            var token = $("input[name='_token']").val();
            $.ajax({

                type:'POST',
                url: "/accept-valuation/" + valuation_id,
                headers: {"X-CSRF-TOKEN": token},
                data: null,
                success:function(response) {
                    console.log(response);
                    response = $.parseJSON(response);
                    if (response.hasOwnProperty("id")) {
                        console.log(response);
                        document.location.href=mo_admin_url+"/review-valuation/"+valuation_id;
                    } else {
                        console.log("accept valuation error");
                    }
                },
                error: function(response){
                    console.log("error");
                },
                complete: function () {
                    $(".mo-loading-image").addClass("hidden");
                }
            });
        }

    });

    var valuation_id = null;
    var year = null;
    var vin_number = null;
    var registration_number = null

    $(".mo-accept-valuate-btn").click(function (){
        valuation_id = $(this).data("valuation-id");
        year = $(this).data("year");
        vin_number = $(this).data("vin");
        registration_number = $(this).data("registration");

        $("input[name='year']").val(year);
        $("input[name='vin']").val(vin_number);
        $("input[name='registration-number']").val(registration_number);

        $("div.vin-number").text("");
        $(".note-content").text("");
    });

    $("#get-vin").click(function(e){

        $("input[name='year']").rules('add', {
            required: true
        });
        $("input[name='vin']").rules('add', {
            required: true
        });
        $("input[name='registration-number']").rules('remove');


        if($("#mo-popup-enter-vin").valid()) {

            var vin = $("input[name='vin']").val();
            var token = $("input[name='_token']").val();
            var year_number = $("input[name='year']").val();

            if (vin.length > 0 && year_number != null)
            {
                $(document).off(".deleteValuation");
                $(document).off(".performValuation");
                $(document).on("ajaxStart.getVin", function () {
                    $(".mo-loading-image").removeClass("hidden");
                });

                $.ajax( {
                    url:'/search-vin',
                    type: 'POST',
                    data: {_token: token, vin_number: vin, year: year_number},

                    success:function(data) {

                        response = jQuery.parseJSON(data);
                        if (data.length > 0) {
                            console.log(response);
                            $("div.vin-number").text(response.vin);

                            var vehicle_details = "";
                            if(response.make !== null){
                                vehicle_details += " "+ response.make
                            }
                            if(response.model_family !== null){
                                vehicle_details += " "+ response.model_family
                            }
                            if(response.build_date !== null){
                                vehicle_details += " "+ response.build_date
                            }
                            if(response.variant !== null){
                                vehicle_details += " "+ response.variant
                            }
                            if(response.series !== null){
                                vehicle_details += " "+ response.series
                            }
                            if(response.colour !== null){
                                vehicle_details += " "+ response.colour
                            }
                            if(response.manufacturer_color !== null){
                                vehicle_details += " "+ response.manufacturer_color
                            }
                            if(response.body_type !== null){
                                vehicle_details += " "+ response.body_type
                            }
                            if(response.registered_state !== null){
                                vehicle_details += " "+ response.registered_state
                            }
                            if(response.registration_number !== null){
                                vehicle_details += " "+ response.registration_number
                            }
                            if(response.compliance_date !== null){
                                vehicle_details += " "+ response.compliance_date
                            }

                            $(".note-content").text($.trim(vehicle_details));

                            $("#vin-confirm").prop("disabled", false);
                        }

                    },
                    error: function(xhr) {

                    },
                    complete: function () {
                        $(".mo-loading-image").addClass("hidden");
                    }
                });
            }

        }
    });



    var enterVinValidator = $("#mo-popup-enter-vin").validate({
        rules: {
            //"year" : "required",
            //"vin" : "required",
            //"registration-number" : "required"
        },
        messages: {
            "year": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
            "vin": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
            "registration-number": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>"
        }
    });
    /** ENTER VIN **/
}