/** AUTO FILL STATE DEPEND ON SELECTED COUNTRY **/
console.log("access");
$(document).ready(function(){

    // $("#dealer-location-post-code").keyup(function(){
    //     var $this = $(this);
    //     $this.val($this.val().replace(/[^\d+]/g, ''));
    // });

    $("#dealer-location-code").keyup(function(e){
        var $this = $(this);
        $this.val($this.val().replace(/^[^1-9]+/,''));
        if($this.val() > 100000 && e.keyCode != 46 && e.keyCode != 8){
            $this.val(100000);
        }
    });

    $("#mo-create-location-form").validate({
        rules: {
            dealer_location_name: "required",
            dealer_location_code: "required",
            // dealer_location_lmct: "required",
            dealer_location_address: "required",
            dealer_location_suburb: "required",
            dealer_location_country: "required",
            dealer_location_state: "required"
        },
        highlight: function(element) {
            $(element).closest('.input-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.input-group').removeClass('has-error');
        },
        messages:{
            dealer_location_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location name</span>"+"</div>",
            dealer_location_code: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location code</span>"+"</div>",
            // dealer_location_lmct: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location lmct</span>"+"</div>",
            dealer_location_address: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location address</span>"+"</div>",
            dealer_location_suburb: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location suburb</span>"+"</div>",
            dealer_location_country: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location country</span>"+"</div>",
            dealer_location_state: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter dealer location state</span>"+"</div>"
        }
        // submitHandler: function(form){
        //     form.submit();
        // }
    });

    // $("#dealer-location-code").keyup(function () {
    //     this.value = this.value.replace(/[^0-9\.]/g,'');
    // });

    // AUTO FILL STATE DEPEND ON COUNTRY
    $(document).ready(function(){
        var country_list = jQuery.parseJSON($("#country-list").val());
        $("#dealer-location-state option[value]").remove();
        $.each(country_list, function(index, value) {

            $.each(value.stateList, function(index2, value2) {

                if (parseInt($("#dealer-location-country").val()) == parseInt(value2.country_id)) {
                    if(parseInt($("input[name='selected-state']").val()) == parseInt(value2.id))
                    {
                        $("#dealer-location-state").append($("<option>", {
                            value: value2.id,
                            text: value2.name,
                            selected: true
                        }));
                    }else{
                        $("#dealer-location-state").append($("<option>", {
                            value: value2.id,
                            text: value2.name
                        }));
                    }

                }

            });
        });

        //SET COUNTRY ID FOR HIDDEN INPUT
        $("#input-country-id").val($("#dealer-location-country").val());
    });

    /*$("#dealer-location-country").on("change", function() {

        var country_list = jQuery.parseJSON($("#country-list").val());
        $("#dealer-location-state option[value]").remove();
        $.each(country_list, function(index, value) {

            $.each(value.stateList, function(index2, value2) {

                if (parseInt($("#dealer-location-country").val()) == parseInt(value2.country_id)) {

                    $("#dealer-location-state").append($("<option>", {
                        value: value2.id,
                        text: value2.name
                    }));
                }

            });
        });
    });*/

    // END AUTO FILL STATE

    $("#dealer-location-save").click(function(e){

        /* Validate Form when submit */
        if(!$("#mo-create-location-form").valid()){
            return false;
        }

        e.preventDefault();
        var token = $("input[name='_token']").val();
        var location_id = $("#mo-location-id").val();
        var new_or_edit = $("#new-or-edit").val();
        var url = "";

        if(new_or_edit == "new"){
            url = "/dealership/save-location";
        }else if(new_or_edit == "edit"){
            url = "/dealership/save-location/" + location_id;
        }

        console.log(url);

        $(document).on("ajaxStart.updateDealershipLocation", function () {
            $('.mo-loading-image').removeClass("hidden");
        });
        $.ajax({
            _token: token,
            type: "POST",
            dataType: 'json',
            url: url,
            data: $("#mo-create-location-form").serialize(),
            success: function(data){
                console.log(data);
                if(data == 1){
                    // $(".alert-success").removeClass("hidden");
                    window.location.replace("/dealership/location");
                }else{
                    //error
                    $(".alert-danger").find(".mo-error-message").text(data);
                    $(".alert-danger").removeClass("hidden");
                }
            },
            error: function(data){
                //console.log(data)
            },
            complete: function () {
                $('.mo-loading-image').addClass("hidden");
            }
        });
    });

});