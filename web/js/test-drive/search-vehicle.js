/**
 * Created by Son on 9/22/2017.
 */

$(document).ready(function(){

    /***** SHOW TAB *****/
    $(".mo-block").first().toggleClass("mo-block-active");
    $(".mo-block").click(function(){
        var id = $(this).attr("id");
        $(".mo-block").removeClass("mo-block-active");
        $(this).toggleClass("mo-block-active");
        if(id == "mo-tab-enter-vehicle-detail"){
            $("#mo-add-vehicle-wrapper").removeClass('hidden');
            $("#mo-search-vehicle-list-wrapper").addClass("hidden");
        }else{
            $("#mo-add-vehicle-wrapper").addClass('hidden');
            $("#mo-search-vehicle-list-wrapper").removeClass("hidden");
        }
    });

    /***** BUILD DATE DATEPICKER *****/
    var buildDate = $("#build-date-picker").datepicker({
        format: 'mm-yyyy',
        minViewMode: 'months',
        maxViewMode: 'months',
        orientation: 'bottom'
    });

    /***** VALIDATE FORM *****/
    $("#add-vehicle-form").validate({
        rules: {
            make: "required",
            model: "required",
            variant: "required",
            registration: "required"
        },
        highlight: function(element){
            $(element).closest(".input-group").addClass("has-error");
        },
        unhighlight: function(element){
            $(element).closest(".input-group").removeClass("has-error");
        },
        messages: {
            make: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>This field is not empty</span>" + "</div>",
            model: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>This field is not empty</span>" + "</div>",
            variant: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>This field is not empty</span>" + "</div>",
            registration: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>This field is not empty</span>" + "</div>"
        }
    });

    $("#add-vehicle-btn").click(function(){
        if($("#add-vehicle-form").valid()){
            $(document).ajaxStart(function(){
                $(".mo-loading-image").removeClass('hidden');
            });
            $.ajax({
                type: "POST",
                _token: token,
                url: "/test-drives/manual-vehicle",
                data: $("#add-vehicle-form").serialize(),
                success: function(data){
                    if(data){
                        var res = $.parseJSON(data);
                        console.log(res);
                        sessionStorage.setItem("manual_vehicle", 1);
                        window.location.replace("/test-drives/select-vehicle/"+ res.id +"/"+res.make +" "+res.model_family);
                    }
                },
                error: function(data){
                    //console.log(data);
                },
                complete: function(){
                    $(".mo-loading-image").addClass('hidden');
                }
            });
        }
    });

    /***** DISABLE FIELDS STEP BY STEP *****/
    if(!$("#make").val()){
        $("#model").prop("disabled", true);
    }
    if(!$("#model").val()){
        $("#variant").prop("disabled", true);
    }
    if(!$("#year").val()) {
        $("#build-date").prop("disabled", true);
    }
    /***** YEAR CHANGE *****/
    var token = $("input[name='_token']").val();
    $("select#year").on("change", function(){
        /** GET MAKE LIST - BY YEAR **/
        $.ajax({
            type: "POST",
            url: "/get-glasses-data/make",
            data: {
                _token: token,
                year: $("#year").val()
            },
            success: function(data){
                console.log("success: " + data);
                if($("#make").val() == ''){
                    $("#make").find("option:gt(0)").remove();
                }
                $.each(data, function(key, value) {
                    $('#make')
                        .append($("<option></option>")
                            .attr("value",value)
                            .text(value));
                });
            },
            error: function(data){
                console.log("error: " + data);
            }
        });
        if($("#make").val() && $("#model").val() && $("#variant").val()){
            filterMonthList();
        }
    });


    /***** MAKE CHANGE *****/
    /** GET MODEL LIST **/
    $("select#make").on("change", function(){
        $.ajax({
            type: "POST",
            url: "/get-glasses-data/family",
            data: {_token: token, year: $("#year").val(), make: $("#make").val()},
            success: function(data){
                console.log("success model: " + data);
                $("#model").find("option:gt(0)").remove();
                $.each(data, function(key, value){
                    $("#model").append($("<option></option>").attr("value", value).text(value));
                });
                /** ENABLE MODEL **/
                $("#model").prop("disabled", false);
                $("#variant").find("option:gt(0)").remove();
                $("#build-date").val("");
                filterYearList();
            },
            error: function(data){
                console.log("error: " + data);
            }
        });
    });


    /***** MODEL CHANGE *****/
    /***** GET VARIANT LIST *****/
    $("#model").on("change", function(){
        $.ajax({
            type: "POST",
            url: "/get-glasses-data/variant",
            data: { _token: token, year: $("#year").val(), make: $("#make").val(), model: $("#model").val()},
            success: function(data){
                console.log(data);
                $("#variant").find("option:gt(0)").remove();
                $.each(data, function(key, value){
                    $("#variant").append($("<option></option>").attr("value", value).text(value));
                });
                /** ENABLE VARIANT **/
                $("#variant").prop("disabled", false);
                filterYearList();
            }
        });
    });

    $("select#variant").on('change', function(){
        filterYearList();
        filterMonthList();
    });

    function filterYearList(){
        $.ajax({
            type: 'POST',
            data: {_token: token, make: $("#make").val(), model: $("#model").val(), variant: $("#variant").val()},
            url: "/get-glasses-data/release_year",
            success:function(data) {
                console.log('success: '+data);
                var current_selected_year = $("#year").val();
                var selected = "";
                $("#year").find("option:gt(0)").remove();
                $("#year").removeProp("selected");
                $.each(data, function(key, value) {
                    if(current_selected_year == value){
                        selected = "selected";
                    }else{
                        selected = "";
                    }
                    $('#year')
                        .append($("<option></option>")
                            .attr("value",value)
                            .prop("selected",selected)
                            .text(value));
                });
            },
            error: function(data){
                console.log('error: '+data);
            }
        });
    }

    function filterMonthList(){
        $.ajax({
            type: "POST",
            data: {_token: token, year: $("#year").val(), make: $("#make").val(), model: $("#model").val(), variant: $("#variant").val()},
            url: "/get-glasses-data/release_month",
            success: function(data){
                console.log("data-month: " + data);
                if($("#year").val() != ''){
                    disableMonthPicker(data);
                    $("#build-date").prop("disabled", false);
                }
            },
            error: function(data){
                console.log("error: "+data);
            }
        });
    }

    function disableMonthPicker(monthArrayIndex){
        var monthNames =
            ["","Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        var datesToEnable = [];
        for(var i =0; i<monthArrayIndex.length; i++){
            datesToEnable.push(monthNames[monthArrayIndex[i]]);
        }
        console.log("datesToEnable "+datesToEnable);
        var buildDate = $("#build-date-picker").datepicker({
            format: 'mm-yyyy',
            minViewMode: 'months',
            maxViewMode: 'months',
            orientation: 'bottom'
        }).on("show", function(event){
            event.preventDefault();
            $(".month").each(function(index, element){
                var el = $(element);
                el.removeClass("disabled");
                if(datesToEnable.indexOf(el.text()) === -1){
                    el.addClass("disabled");
                }
            });
        });

        var years = $("#year").val();
        $('#build-date-picker').datepicker('setDate', new Date(years, monthArrayIndex[0]-1, 24));
        $('#build-date-picker').datepicker('update');
    }

});