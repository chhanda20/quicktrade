/** INTERACTION LIST **/
$.getInteraction = function (destroy, init, query, offset) {
    var contact_id = $("input[name='customer_id']").val();
    var id = "#mo-table-interaction-list";
    var destroy = destroy;
    var columns = 	[
        {className: "never"},// id
        {className: "all width-20-percent", "bSortable": false},//date time
        {className: "never"}, // 
        {className: "all width-15-percent", "bSortable": false}, //type code
        {
            className: "all max-width-1",
            "bSortable": false,
            "mRender": function(data, type, row, meta) {
                var comment = data.replace(/\r?\n/g,'<br/>').replace(/\s/g, '&nbsp; ');
                return "<div class='interaction-comment word-spacing--2'>"+comment+"</div>";
                //return "<div class='interaction-comment word-spacing--2'>".concat(comment).concat("</div>");
            }
            
        }, //comment
        {
            className: "all width-8-percent", "bSortable": false, "mRender": function(data, type, row, meta) {
            return  '<a href="#mo-interaction-edit-popup" class="open-popup-link"><span class="glyphicon glyphicon-pencil  mo-edit-interaction"  data-interaction-id="' + row[0] + '"  data-interaction-datetime="' + row[1] + '" data-interaction-type="' + row[2] + '"  data-interaction-comment=' + row[4] + '></span></a>'+
                '<a href="#mo-remove-confirm-popup" class="mo-remove-popup"><span class="glyphicon glyphicon-trash mo-remove-interaction float-right" data-interaction-id="' + row[0] + '"></span></a>';
            }
        }
    ];
    var request_url = base_url+"/view-interaction/"+contact_id;
    var query = query;
    var offset = offset;
    var init = init;
    var error_msg = "<div>There are no interaction to display at this time.</div>";
    var scrollX = true;
    $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);
}

$("#customer-detail-interaction-block").click(function() {
    var destroy = true;
    var init = true;
    var query = null;
    var offset = 0;
    $.getInteraction(destroy, init, query, offset);
});

/** END INTERACTION LIST **/

/** TEST DRIVE LIST **/
$.getTestDrive = function (destroy, init, query, offset) {
    var id = "#mo-table-testdrive-list";
    var customer_id = $("input[name='customer_id']").val();
    var destroy = destroy;
    var columns = [
        {className: "never"}, //id
        {className: "all", "width":"14%"}, //make
        {className: "all width-20-percent-force"}, //model
        {className: "all", "bSortable": false, "width":"13%"}, //year
        {className: "all", "bSortable": false, "width":"10%"}, //colour
        {className: "all", "bSortable": false}, //start date
        {className: "all", "bSortable": false}, //timeout
        {className: "all", "bSortable": false}, //estimated time
        {className: "never"}, // status
        {className: "never"}, //comment
        {
            className: "all", "bSortable": false,
                "mRender":function(data, type, row, meta){
                    if(row[8] == 1){
                        return '<a href='+base_url+'"/test-drives/'+row[0]+'"><input type="button" class="mo-test-drives-inprogress-btn text-capitalize float-right" data-test-drive-id="'+row[0]+'"  ' +
                            'data-make-name="'+row[1]+'" data-model-name="'+row[2]+'" data-year="'+row[3]+'" value="Return Vehicle"></a>';
                    }
                    else(row[0] == 2)
                    {
                        return '<a href='+base_url+'"/test-drives/'+row[0]+'"><input type="button" class="mo-test-drives-read-comment-btn text-capitalize float-right" data-test-drive-id="'+row[0]+'" data-read-comment="'+row[9]+'"' +
                            'data-make-name="'+row[1]+'" data-model-name="'+row[2]+'"'+ 
                            'data-year="'+row[3]+'" data-vehicle-rating="'+row[10]+'"'+ 
                            'data-service-rating="'+row[11]+'" data-marketing-code="'+row[12]+'"'+' data-marketing-option="'+row[13]+'"'+' value="Read Comment"></a>';
                    }

                }
        }
    ];
    var request_url = base_url+"/view-test-drives/" + customer_id;
    var query = query;
    var offset = offset;
    var init = init;
    var error_msg = "<div>There are no test drives to display at this time.</div>";
    var scrollX = true;
    $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);
}
var destroy = false;
var init = true;
var query = null;
var offset = 0;
$.getTestDrive(destroy, init, query, offset);

$("#customer-detail-test-drive-block").click(function() {
    var destroy = true;
    var init = true;
    var query = null;
    var offset = 0;
    $.getTestDrive(destroy, init, query, offset)
});
/** END TEST DRIVE LIST **/

/** VALUATION LIST **/
$.getValuation = function(destroy, init, query, offset) {

    var id = "#mo-table-valuations-list";
    var columns = 	[
        {className: "never"}, //Id
        {className: "all", }, //Date
        {className: "all", "bSortable": false}, //Vehicles
        {className: "all", "bSortable": false, "mRender": function(data,type,row,meta){
            return "$"+accounting.formatMoney(row[3], "", 2, ",", ".");
        }}, //Price
        //{className: "desktop tablet-l", "bSortable": false},
        { className: "all", "bSortable": false, //Status
            "mRender": function (data,type,row,meta) {
                switch (row[5]) {
                    case 1:
                        return  '<div class="text-center"><button class="btn btn-mo-customer text-capitalize red-button">'+data+'</button></div>'+
                        '<div class="text-center font-size20">$'+accounting.formatMoney(row[3], "", 2, ",", ".")+'</div>'; 
                        break;
                    case 4:    
                    case 2:
                    case 5:
                        //console.log(data, row[5]);
                        return  '<div class="text-center"><button class="btn btn-mo-customer text-capitalize yellow-button-2">'+data+'</button></div>'+
                        '<div class="text-center font-size20">$'+accounting.formatMoney(row[3], "", 2, ",", ".")+'</div>';  
                        break;
                    case 6:
                    case 7:
                    case 3:
                    case 12:
                    case 13:
                    case 14:
                        return  '<div class="text-center"><button class="btn btn-mo-customer text-capitalize green-button">'+data+'</button></div>'+
                        '<div class="text-center font-size20">$'+accounting.formatMoney(row[3], "", 2, ",", ".")+'</div>'; 
                        break; 
                    default:
                        return null;                
                }
            }
        },
        {
            className: "never", "bSortable": false, /* temporarily disable for review valuation */
            "mRender": function (data, type, row, meta) {
                return '<a href='+base_url+'"/review-valuation/' + row[0] + '"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
            }
        }
    ];
    var request_url = base_url+"/get-valuations-by-customer/"+$("#customer-id").val();
    var error_msg = "<div>There are no valuations to display at this time.</div>";
    $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);

}

var destroy = false;
var init = true;
var query = null;
var offset = 0;
$.getValuation(destroy, init, query, offset);

$("#customer-detail-valuation-block").click(function(){
    var destroy = true;
    var init = true;
    var query = null;
    var offset = 0;
    $.getValuation(destroy, init, query, offset);
});

//slider for vehicle rating
var vehicle_rating = $( "#slider_vehicle_rating" ).slider({
    value: 4,
    min: 0,
    max: 10,
    step: 1
})
.each(function() {

  //
  // Add labels to slider whose values 
  // are specified by min, max and whose
  // step is set to 1
  //

  // Get the options for this slider
  var opt = $(this).data().uiSlider.options;
  
  // Get the number of possible values
  var vals = opt.max - opt.min;
  
  // Space out values
  for (var i = 0; i <= vals; i++) {

    var el = $('<label>'+(i)+'</label>').css('left',(i/vals*100)+'%');

    
  
    $( "#slider_vehicle_rating" ).append(el);
    
  }
  
});

//end of slider for vehicle rating

//slider for mo rating
$( "#slider_mo_rating" ).slider({
    value: 4,
    min: 0,
    max: 10,
    step: 1
})
.each(function() {

  //
  // Add labels to slider whose values 
  // are specified by min, max and whose
  // step is set to 1
  //

  // Get the options for this slider
  var opt = $(this).data().uiSlider.options;
  
  // Get the number of possible values
  var vals = opt.max - opt.min;
  
  // Space out values
  for (var i = 0; i <= vals; i++) {
    
    var el = $('<label>'+(i)+'</label>').css('left',(i/vals*100)+'%');
  
    $( "#slider_mo_rating" ).append(el);
    
  }
  
});
//end of slider for mo rating

/** END VALUATION LIST **/