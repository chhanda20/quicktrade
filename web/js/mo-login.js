/**
 * Created by user on 11/14/2016.
 */

/** Toggle Show/Hide Password **/
(function ($) {
    $.toggleShowPassword = function (options) {
        var settings = $.extend({
            field: "#password",
            control: "#toggle_show_password"
        }, options);

        var control = $(settings.control);
        var field = $(settings.field);

        control.bind('click', function () {
            if (field.is(':password')) {
                control.find('img').attr('src',base_url+'/images/svg/login/icon-click-eye.svg');
                field.attr('type', 'text');
            } else {
                control.find('img').attr('src',base_url+'/images/svg/login/icon-click-eye.svg');
                field.attr('type', 'password');
            }
        });
    };
}(jQuery));

//Here how to call above plugin from everywhere in your application document body
$.toggleShowPassword({
    field: '#password',
    control: '.mo-show-password'
});


(function($){
   $.validateEmail = function(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
   };
})(jQuery);

(function($){
    $.validatePassword = function(password) {
        var regex = /^([a-zA-Z0-9]){6,}$/;
        return regex.test(password);
    }
})(jQuery);


/*$("#mo-login-form").validate({
    rules: {
        "email": {
            required: true,
            email: true
        },
        "password": {
            required: true,
            minlength: 6
        }
    },
    messages: {
        "email": {
            "required": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter your email address.</span>" + "</div>",
            "email": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter valid email address.</span>" + "</div>"
        },
        "password": {
            "required": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user password</span>" + "</div>",
            "minlength": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter more than 6 characters</span>" + "</div>"
        }
    }
});*/

var pathname = window.location.pathname;

console.log("pathname "+pathname);
console.log(base_url+"/forgot-password");

// if (pathname == "/login" || pathname == "/") {
if( pathname === ("/") || pathname.indexOf("/mo-admin") !== -1 || pathname.indexOf("web/login") !== -1 || pathname.indexOf("web/mo-admin") !== -1){
console.log('in login');
    var url = base_url+"/login";

    if(pathname.indexOf("/mo-admin") !== -1 ||pathname.indexOf("web/mo-admin") !== -1 ){
        var url = base_url+"/mo-admin/login";
    }
    (function($){
        $.validateLoginForm = function (options) {
            var settings = $.extend({
                email: "",
                password: "",
                control: "",
                error: ""
            },options);

            var email = $(settings.email);
            var password = $(settings.password);
            var control = $(settings.control);
            var error = $(settings.error);

            control.bind('click', function (e) {
                console.log("clicked");
                if(email.val() == "") {
                    error.attr("class", "padding-top-8 block");
                    error.children().html("Please enter your email address.");
                    return false;
                } else if(!$.validateEmail(email.val())) {
                    error.attr("class", "padding-top-8 block");
                    error.children().html("Wrong email format.");
                    return false;
                } else if (!$.validatePassword(password.val())) {
                    error.attr("class", "padding-top-8 block");
                    error.children().html("Please enter your password.");
                    return false;
                }
                e.preventDefault();
                console.log(url);
                $.ajax({
                    type: "POST",
                    url: url,
                    _token: $("input[name='_token']").val(),
                    data: $("#mo-login-form").serialize(),
                    success: function(data){

                        if(data){
                            var res = $.parseJSON(data);

                            if(res.error === true){
                                var error = '';
                                if(typeof res.error_desc === 'object'){
                                    $.each(res.error_desc, function(key, element) {
                                        error += key + ' : ' + element + "</br>";
                                    });
                                }else{
                                    error = res.error_desc;
                                }
                                $("#error").removeClass("hidden");
                                $(".mo-login-error-txt").html(error);
                            }else{

                                if(pathname.indexOf("/mo-admin") === -1 || pathname.indexOf("web/mo-admin") === -1){
                                    console.log(res.need_accept_term_and_condition);
                                    if(res.need_accept_term_and_condition === 1){
                                        $.magnificPopup.open({
                                            items: {
                                                src: res.term_html
                                            },
                                            type: "inline",
                                            showCloseBtn: true,
                                            closeOnBgClick: true
                                        });
                                        $("#mo-checkbox-term-condition").click(function(){
                                            if($(this).is(":checked")){
                                                $("#mo-term-condition-agree-btn").removeClass('disabled-div grey-btn');
                                                $("#mo-term-condition-agree-btn").click(function(){
                                                    $(document).ajaxStart(function(){
                                                        $('.mo-loading-image').removeClass('hidden');
                                                    });
                                                    $.ajax({
                                                        type: "POST",
                                                        url: base_url+"/accept-term-condition",
                                                        data: {_token: $("input[name='_token']").val()},
                                                        success: function(data){
                                                            if(data == 1){
                                                                console.log(data);
                                                                window.location.replace(base_url+"/dashboard");
                                                            }
                                                        },
                                                        error: function(data){
                                                            console.log("error" + data);
                                                        },
                                                        complete: function(){
                                                            $(".mo-loading-image").addClass("hidden");
                                                        }
                                                    });
                                                });
                                            }else{
                                                $("#mo-term-condition-agree-btn").addClass('disabled-div grey-btn');
                                            }
                                        });
                                    }else{

                                        window.location.href = base_url+"/dashboard";
                                    }
                                }else{
                                    window.location.href = base_url+"/mo-admin/dealership-list";
                                }

                            }
                        }
                    }
                });
            });
        }
    })(jQuery);

    $.validateLoginForm({
        email: "#email",
        password: "#password",
        control: "#mo-login-btn",
        error: "#error"
    });


}
else if (pathname.indexOf('forgot-password') !== -1) {

//debugger
    console.log('in forgot password');
    (function($) {
        $.validateForgotPasswordForm = function (options) {
            var settings = $.extend({
                email: "",
                control: "",
                error: ""
            },options);

            var email = $(settings.email);
            var control = $(settings.control);
            var error = $(settings.error);

            control.bind('click', function () {
                if(email.val() == "") {
                    error.attr("class", "padding-top-8 block");
                    error.children().html("Please enter your registered email address.");
                    return false;
                } else if(!$.validateEmail(email.val())) {
                    error.attr("class", "padding-top-8 block");
                    error.children().html("Wrong email format.");
                    return false;
                }
        });

    }
    })(jQuery);

    $.validateForgotPasswordForm({
        email: "#email",
        control: "#mo-login-btn",
        error: "#error"
    });

}
else if (pathname.search("/reset-password") >  0) {
  console.log('in reset password');
    (function($) {
        $.validateResetPasswordForm = function (options) {
            var settings = $.extend({
                new_password: "",
                confirm_new_password: "",
                control: "",
                error: ""
            },options);

            var new_password = $(settings.new_password);
            var confirm_new_password = $(settings.confirm_new_password);
            var control = $(settings.control);
            var error = $(settings.error);

            control.bind('click', function () {
                if(!$.validatePassword(new_password.val()) || !$.validatePassword(confirm_new_password.val()) || new_password.val() != confirm_new_password.val()) {
                    error.attr("class", "padding-top-8 block");
                    error.children().html("New Password and Confirm New Password aren't correct");
                    return false;
                }
        });

    }
    })(jQuery);

    $.validateResetPasswordForm({
        new_password: "#new-password",
        confirm_new_password: "#confirm-new-password",
        control: "#mo-login-btn",
        error: "#error"
    });
}
