var pathname = window.location.pathname;
var token = $("input[name='_token']").val();

// Show up arrow
$('#menu_users')
    .find('.glyphicon-menu-down')
    .removeClass('glyphicon-menu-down')
    .addClass('glyphicon-menu-up');

// Set active menu
$('#menu_users')
    .next('.sidebar-submenu')
    .show()
    .find('#menu_manage')
    .addClass('active');