@extends('layouts.master-login')

@section('title', 'moto - Set New Password')

@section('content')
    {{--Start - moto Set New Password Form Section--}}
    <div class="col-xs-12 col-sm-12 col-md-12 text-center padding-top-10">
        <form action="{!! url('reset-password'); !!}" class="form-horizontal" id="mo-login-form" method="post">
            {{ csrf_field() }}
            <div class="padding-bottom-18 input-group">
                <span class="mo-password"><img src="{{ asset('images/svg/login/icon-password.svg') }}" alt="Pwd Icon" onerror="this.onerror=null; this.src='{{ asset('images/login/icon-password.png') }}'"/></span>
                <input type="password" id="new-password" placeholder="New Password" name="new_password" />
            </div>
            <div class="input-group">
                <span class="mo-password"><img src="{{ asset('images/svg/login/icon-password.svg') }}" alt="Pwd Icon" onerror="this.onerror=null; this.src='{{ asset('images/login/icon-password.png') }}'"/></span>
                <input type="password" id="confirm-new-password" placeholder="Confirm New Password" name="confirm_new_password" />
            </div>
            <div class="padding-top-8 hidden" id="error">
                <span class="mo-login-error-txt">Email or Password do not match.</span>
            </div>
            <div class="padding-top-40 padding-bottom-32">
                <input type="hidden" value="{{$password_token}}" name="password_token" />
                <input type="submit" class="btn btn-lg btn-blockt" id="mo-login-btn" value="DONE" />
            </div>
        </form>
    </div>
    {{--End - moto Set New Password Form Section--}}
@endsection