@extends('layouts.master-login')

@section('title', 'moto - Login')

@section('content')
    {{--Start - moto Login Form Section--}}
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <form action="{!! url('login'); !!}" class="form-horizontal" id="mo-login-form" method="post">
            {{ csrf_field() }}
            <div class="padding-bottom-18 input-group">
                <span class="mo-email"><img src="{{ asset('images/svg/login/icon-username.svg') }}" alt="Usr Icon" onerror="this.onerror=null; this.src='{{ asset('images/login/icon-username.png') }}'"/></span>
                <input type="email" id="email" placeholder="Email" name="email"/>
            </div>
            <div class="input-group">
                <span class="mo-password"><img src="{{ asset('images/svg/login/icon-password.svg') }}" alt="Pwd Icon" onerror="this.onerror=null; this.src='{{ asset('images/login/icon-password.png') }}'"/></span>
                <input type="password" id="password" placeholder="Password" name="password" autocomplete="new-password"/>
                <span class="mo-show-password"><img src="{{ asset('images/svg/login/icon-eye.svg') }}" alt="Show Password" onerror="this.onerror=null; this.src='{{ asset('images/login/icon-eye.png') }}'"/></span>
            </div>
            @if (!isset($error))
            <div class="padding-top-8 hidden" id="error">
            @else
            <div class="padding-top-8 block" id="error">
            @endif
                <span class="mo-login-error-txt">{{isset($error) ? $error : null}}</span>
            </div>
            <div class="padding-top-40 padding-bottom-32">
                <input type="submit" class="btn btn-lg btn-blockt" id="mo-login-btn" value="LOG IN" />
            </div>
            <div class="text-center">
                <a href="{!! url('forgot-password'); !!}"><span class="mo-forgot-password-txt">Forgot Your Password?</span></a>
            </div>
        </form>
    </div>
    {{--End - moto Login Form Section--}}

    <div class="mo-white-popup mfp-hide" id="mo-term-and-condition-popup">
        <div class="mo-row padding-bottom-20">
            <span class="text-capitalize float-left mo-popup-heading">term & condition</span>
        </div>
        <div class="mo-row">
            @if(Session::has("term_and_condition"))
                <hr class="mo-form-hr">
                <textarea class="mo-textarea form-control" id="test-drive-term-condition" rows="20" style="border: none;" readonly>{{Session::get("term_and_condition")->term_and_condition}}</textarea>
                <hr class="mo-form-hr">
            @endif
        </div>
        <div class="mo-row padding-top-24">
            <div class="float-left">
                <input type="checkbox" class="mo-round-check" id="mo-checkbox-term-condition" name="mo_checkbox_term_condition">
                <label for="mo-checkbox-term-condition"><span></span>I accept these Term & Conditions</label>
            </div>
            <div class="float-right">
                <input type="button" class="mo-btn text-uppercase grey-btn disabled-div" id="mo-term-condition-agree-btn" value="accept">
            </div>
        </div>
        <div class="mo-loading-image hidden">Loading...</div>
    </div>
@endsection