<span class="padding-left-20 mo-dropdown-menu-wrapper hidden" id="mo-sort-wrapper">
    <a href="#" role="button" data-toggle="dropdown" data-target="mo-trading-filter" id="mo-badge">
        <span class="xs-hide text-uppercase" id="mo-sort-header">sort by</span>
        <span class="sm-hide xs-show padding-left-5 glyphicon glyphicon-sort"></span>
        <span class="xs-hide glyphicon glyphicon-menu-down"></span>
    </a>
    <div class="dropdown-menu no-padding" id="mo-trading-filter">
        <span class="glyphicon glyphicon-triangle-top"></span>
        <div class="dropdown-item dropdown-header text-center font-family-bold">
            <span>SORT BY: </span>
            <span class="padding-left-5 glyphicon glyphicon-sort-by-alphabet cursor-pointer"></span>
            <span class="padding-left-5 glyphicon glyphicon-sort-by-alphabet-alt cursor-pointer"></span>
        </div>
        {{--STOCK SORT MENU--}}
        <div class="mo-sort-stock-list">
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="make">Make</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="model_family">Model</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="build_date">Year Built</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="stock_number">Dealer Stock Number</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="registration_number">Registration Number</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="day_in_stock">Days in Stock</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="trade_price">Trade Price</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
        </div>


        {{--AUCTION SORT MENU--}}
        <div class="mo-sort-auction-list">
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="start_price">Start Price</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="buy_now_price">Buy Now Price</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="location_text">Location</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="odometer">Odometer</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="vehicle_name">Vehicle Name</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="rating">Rating</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="seller_rating">Seller Rating</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
        </div>

        {{--LIVE AUCTION SORT MENU--}}
        <div class="mo-sort-live-auction-list">
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="location_text">Location</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="odometer">Odometer</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="vehicle_name">Vehicle Name</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-item cursor-pointer">
                <a class="mo-sort-by">
                    <span class="col-xs-10" data-order-by="seller_rating">Seller Rating</span>
                    <span class="col-xs-2 text-right glyphicon glyphicon-ok"></span>
                </a>
            </div>
        </div>
    </div>
</span>