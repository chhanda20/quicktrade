@extends(strpos(Request::path(), 'mo-admin') === 0 ? 'mo-admin.layouts.master' : 'layouts.master')

@section('title', 'moto - Main')

@section('content')
    @include('main.review-valuation-details')
    @include('main.review-valuation-exterior')
    @include('main.review-valuation-interior')
    @include('main.review-valuation-mechanical')
    @include('main.review-valuation-summary')
    {{----------------------------------------------------------- start HEADER INFO -----------------------------------------------------------}}

    {{--Start - moto Header Text Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <div class="col-xs-12 col-sm-8 col-md-8 no-padding">
                <a href="{!! url(strpos(Request::path(), 'mo-admin') === 0?'mo-admin/'.$back_to_list_link:$back_to_list_link) !!}" class="underline-text">{{$back_to_list or ''}}</a> > {{ $header or 'Valuations' }} > <span id="mo-valuation-step-txt" class="text-capitalize yellow-text">{{ $title or 'Details' }}</span>
            </div>
            {{--@if($status == 10 || $status == 2 || $status == 4)--}}
            {{--@if(in_array($status, [10,2,3,4,5,7]))--}}
            @if(in_array($status, [10,2,3,6]))
                <div class="col-xs-12 col-sm-4 col-md-4 no-padding text-right mo-edit-txt">
                    <a href="#mo-confirm-cancel-valuation-popup" class="open-popup-link">
                        <span>Cancel </span><span class="glyphicon glyphicon-ban-circle"></span>
                    </a>
                </div>
            @endif
        </div>
    </div>
    {{--End - moto Header Text Section--}}


    {{--Start - moto Header Info Section--}}
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-3 padding-lr5" id="mo-vehicle-add-photo">
            <form class="mo-exterior-popup-form vertical-center" action="" method="post" enctype="multipart/form-data" id="mo-vehicle-image-upload-form">
                {{ csrf_field() }}
                <input type="hidden" value="{{isset($valuation_current_version) ? $valuation_current_version : null}}" name="valuation_version" />
                <div id="mo-vehicle-image-wrapper" class="vertical-center">
                    <label for="mo-vehicle-image-upload{{$disabledUploadImage or ''}}" class="cursor-pointer">
                        <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{isset($vehicle_id) ? $vehicle_id : ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/img-add-photo@3x.png')}}'" class="img-responsive">
                    </label>
                </div>
                <input type="hidden" id="valuation-id" name="valuation_id" value="{{isset($valuation_id) ? $valuation_id : null}}" />
                <input type="hidden" id="vehicle-id" name="vehicle_id" value="{{isset($vehicle_id) ? $vehicle_id : null}}" />
                <input type="file" name="mo-vehicle-photo-upload" id="mo-vehicle-image-upload" class="mo-exterior-input-file" />
            </form>
        </div>
        <div class="col-xs-12 col-sm-9 padding-lr5" id="mo-vehicle-info">
            <div class="col-xs-12 no-padding text-uppercase" id="mo-vehicle-tittle">
                {{isset($valuation_name)?$valuation_name:null}}
            </div>
            <div class="col-xs-12 no-padding padding-top-20">
                <form class="form-horizontal" id="mo-customer-details">
                    <div class="col-xs-12 padding-lr5 padding-top-5">
                        <span class="col-xs-12 col-sm-5 no-padding text-capitalize">
                            Valuation Price:
                        </span>
                        <label class="col-xs-12 col-sm-7 no-padding" for="email">
                            ${{number_format($salesInfo->valuation_price, 2)}}
                        </label>
                    </div>
                    <div class="col-xs-12 padding-lr5 padding-top-5">
                        <span class="col-xs-12 col-sm-5 no-padding text-capitalize">
                            Max. Valuation Price:
                        </span>
                        <label class="col-xs-12 col-sm-7 no-padding" for="email">
                            ${{number_format($salesInfo->max_valuation_price, 2)}}
                        </label>
                    </div>
                    <div class="col-xs-12 padding-lr5 padding-top-5">
                        <span class="col-xs-12 col-sm-5 no-padding text-capitalize">
                            Estimated Total Reconditioning Cost:
                        </span>
                        <label class="col-xs-12 col-sm-7 no-padding" for="email">
                            ${{number_format($reconditioningCost->estimated_recon_cost, 2)}}
                        </label>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 no-padding mo-5-blocks padding-top-24" id="mo-blocks-wrapper">
                @for ($i = 0; $i < count($step_array); $i++)
                <div class="col-xs-12 {{!in_array("valuation_perform", $permissions) && ($status == 10 || $status == 2)?'col-sm-2-5':'col-sm-2'}} padding-lr5">
                    <a href="#{{$step_data_array[$i]}}">
                        <div class="col-xs-12 mo-block {{/*in_array("salesperson", $roles) && */$status == 10 || $status == 2?'disabled-div':''}}" id="{{$step_data_array[$i]}}_tab">
                            <span class="col-xs-12 mo-block-txt text-capitalize no-padding" data-step="{{$step_data_array[$i]}}">{{$step_array[$i]}}</span>
                        <span class="col-xs-12 text-center mo-arrow-focus">
                            <img src="{{ asset('images/svg/main/triangle.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/triangle.png') }}'">
                        </span>
                        </div>
                    </a>
                </div>
            @endfor
        </div>
    </div>
    {{--End - moto Header Info Section--}}

    {{----------------------------------------------------------- end HEADER INFO -----------------------------------------------------------}}

    @yield('review-details')

    @yield('review-exterior')

    @yield('review-interior')

    @yield('review-mechanical')

    @yield('review-summary')

    {{--Start - moto Popup Section--}}
    {{------------ DELETE CONFIRMATION POPUP ------------}}
    <div id="mo-confirm-delete-exterior-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">delete item</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <span>Are you sure you want to delete this item?</span>
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-detele-item-record">
                </div>
            </div>
        </form>
    </div>
    {{--End - moto Popup Section--}}

    {{----------------------------------------------------------- EDIT INSPECTION ITEM POPUP -----------------------------------------------------------}}
    <div id="mo-edit-inspection-item-popup" class="mo-white-popup mo-edit-inspection-item-popup mfp-hide">
        <form class="mo-exterior-popup-form" action="/upload-image" method="post" enctype="multipart/form-data" id="mo-exterior-popup-form" >
            {{ csrf_field() }}
            <input type="hidden" value="0" name="sequence" />
            <input type="hidden" value="{{isset($valuation_id) ? $valuation_id : null}}" name="valuation_id">
            <input type="hidden" value="{{isset($valuation_current_version) ? $valuation_current_version : null}}" name="valuation_version" />
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading"><span>Edit</span> Inspection item</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row mo-exterior-popup-content padding-top-24 padding-bottom-20">
                <div class="form-group col-xs-12 col-sm-8">
                    <label for="selection">Section</label>
                    <div class="mo-dropdown-wrapper">
                        <span class="mo-icon-swipe glyphicon glyphicon-menu-down float-right"></span>
                        <select class="form-control mo-inspection-popup-selectbox" name="exterior-section" id="exterior-section" disabled>
                            <option disabled selected>-- Sections list --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-8">
                    <label for="item">Item</label>
                    <input type="hidden" value="" name="exterior-item-id" />
                    <div class="mo-dropdown-wrapper">
                        <span class="mo-icon-swipe glyphicon glyphicon-menu-down float-right"></span>
                        <select class="form-control mo-inspection-popup-selectbox" name="exterior-items" id="exterior-items" disabled>
                            <option selected disabled>-- Items list --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-8 {{!in_array("valuation_perform", $permissions)?'hidden':''}}">
                    <label for="estimate-cost">Estimate Cost</label>
                    <input {{$disableIt != ''?'disabled':''}} type="number" pattern="[0-9]*" value="0" name="estimate_cost" id="estimate-cost" class="form-control mo-inspection-popup-textbox " maxlength="13" {{--onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46"--}}/> <br/>
                </div>
                <div class="form-group col-xs-12">
                    <div class="col-xs-4 mo-inspection-upload-image-box no-padding" id="mo-image-box-1">
                        <label for="fileToUpload{{$disableIt != ''?'disabled':''}}" class="mo-inspection-input-file-label vertical-center" id="ImageBrowse">
                            <img src="" alt="Upload" onerror="this.onerror=null; this.src='{{ asset('images/perform_valuation/image_upload.png') }}'" class="mo-inspection-upload-image-icon img-responsive" />
                            <!-- <span class="mo-inspection-upload-image-label">Add Photo</span> -->
                        </label>
                        
                    </div>
                    <div class="col-xs-4 mo-inspection-upload-image-box no-padding" id="mo-image-box-2">
                        <label for="fileToUpload{{$disableIt != ''?'disabled':''}}" class="mo-inspection-input-file-label vertical-center">
                            <img src="" alt="Upload" onerror="this.onerror=null; this.src='{{ asset('images/perform_valuation/image_upload.png') }}'" class="mo-inspection-upload-image-icon img-responsive" />
                        </label>
                    </div>
                    <div class="col-xs-4 mo-inspection-upload-image-box no-padding" id="mo-image-box-3">
                        <label for="fileToUpload{{$disableIt != ''?'disabled':''}}" class="mo-inspection-input-file-label vertical-center">
                            <img src="" alt="Upload" onerror="this.onerror=null; this.src='{{ asset('images/perform_valuation/image_upload.png') }}'" class="mo-inspection-upload-image-icon img-responsive" />
                        </label>
                    </div>
                    <input type="file" name="fileToUpload" id="fileToUpload" class="mo-exterior-input-file" />
                </div>
                <div class="form-group col-xs-12 padding-top-24">
                    <label for="note">Note</label>
                    <textarea {{$disableIt != ''?'disabled':''}} class="mo-textarea form-control" rows="5" id="note" name="note"  style="box-shadow: none;"></textarea>
                </div>
            </div>
            <hr class="mo-form-hr"/>
            <div class="mo-row padding-top-24">
                <div class="col-xs-12 text-right">
                    <input type="hidden" name="azure-accout-name" value="{{Config::get('app.azure_storage_account_name')}}" />
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="CANCEL">
                    <input type="button" class="mo-btn text-uppercase {{$hideEditField or ''}}" value="UPDATE" id="exterior-edit-button" data-status="update">
                </div>
            </div>
        </form>
    </div>
    <input type="hidden" value="{{$status}}" id="valuation-status">
    <input type="hidden" value="{{$customer_id}}" id="customer_id">
    {{----------------------------------------------------------- EDIT INSPECTION ITEM POPUP -----------------------------------------------------------}}

    {{------------ DELETE CONFIRMATION POPUP ------------}}
    <div id="mo-confirm-cancel-valuation-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">cancel valuation</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    @if($status == 10)
                        <div>Are you sure you want to cancel this valuation?</div>
                        <ul>
                            <li>Press "CONFIRM" to delete this valuation.</li>
                            <li>Press "PUT IT INTO REQUEST LIST" to bring this valuation to Request List.</li>
                        </ul>
                    @elseif($status == 2)
                        <div>Are you sure you want to cancel this valuation and put it back to Request list?</div>
                    {{--@elseif(in_array($status, [4,3,5,7]))--}}
                    @elseif($status == 3 || $status == 6)
                        <div>Are you sure you want to cancel this valuation ?</div>
                    @endif
                </div>
                <div class="col-xs-12 alert alert-danger margin-top-24 hidden" role="alert">
                    <span class="error-text"></span>
                    <a class="mo-close-message close">&times;</a>
                </div>
            </div>

            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="close">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-cancel-valuation">
                    @if($status == 10)
                        <input type="button" class="mo-btn text-uppercase" value="put it into request list" id="mo-put-into-request">
                    @endif
                </div>
            </div>
        </form>
    </div>
    {{--End - moto Popup Section--}}

    <!-- AJAX - loading -->
    <div class="mo-loading-image hidden">Loading&#8230;</div>
    <!-- AJAX - end loading -->
@endsection