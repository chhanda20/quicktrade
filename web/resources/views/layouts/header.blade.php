@section('header')
    {{--Start - moto Header Section--}}
    <div class="col-xs-12 col-sm-12 col-md-12 mo-main-header no-padding vertical-center">
        <div class="col-xs-12 col-sm-12 col-md-2 mo-logo-wrapper-menu text-center no-padding xs-hide">
            <a href="{!! url('home') !!}">
               <img src="{{ asset('images/svg/moto-logo.png') }}" class="img-responsive" alt="moto Logo" onerror="this.onerror=null; this.src='{{ asset('images/moto-logo.png') }}'"/>
            </a>
        </div>

        <div class="col-xs-4 col-sm-12 col-md-8 no-padding mo-middle-menu">
            <a href="#menu-toggle" id="menu-toggle">
                <img src="{{ asset('images/svg/main/icon-menu.svg') }}" class="mo-icon-menu" alt="moto Logo" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-menu.png') }}'"/>
            </a>
            <!-- START TO ADD NOTIFICATION -->
            <a href="#" role="button" data-toggle="dropdown" data-target="#" id="mo-badge">
                <img src="{{ asset('images/svg/main/icon-notification.svg') }}" class="mo-icon-notification" alt="moto Logo" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-notification.png') }}'"/>
            </a>
            
            <span class="dropdown-menu mo-triangle"><img src="{{asset('images/svg/main/triangle-2.svg')}}" ></span>
            <ul class="dropdown-menu mo-notification" role="menu">
                <div id="mo-notification-wrapper">
                    {{--
                    <a href="#" class="mo-content">
                        <div class="mo-notification-item">
                            <h5 class="mo-item-title">Valuation complete</h5>
                            <p class="mo-item-info">
                                2014 Maserati Quattroporte White <br>
                                Today, 9:30am<br>
                                Reg: HGF292 <br>
                                Body: Sedan
                            </p>
                        </div>
                    </a> 
                    <hr class="mo-notification-hr">
                    <a href="#" class="mo-content">
                        <div class="mo-notification-item">
                            <h5 class="mo-item-title">Valuation complete</h5>
                            <p class="mo-item-info">
                                2014 Maserati Quattroporte White <br>
                                Today, 9:30am<br>
                                Reg: HGF292 <br>
                                Body: Sedan
                            </p>
                        </div>
                    </a> 
                    <hr class="mo-notification-hr">
                    <a href="#" class="mo-content">
                        <div class="mo-notification-item">
                            <h5 class="mo-item-title">Valuation complete</h5>
                            <p class="mo-item-info">
                                2014 Maserati Quattroporte White <br>
                                Today, 9:30am<br>
                                Reg: HGF292 <br>
                                Body: Sedan
                            </p>
                        </div>
                    </a>
                    <hr class="mo-notification-hr">
                    <a href="#" class="mo-content">
                        <div class="mo-notification-item">
                            <h5 class="mo-item-title">Valuation complete</h5>
                            <p class="mo-item-info">
                                2014 Maserati Quattroporte White <br>
                                Today, 9:30am<br>
                                Reg: HGF292 <br>
                                Body: Sedan
                            </p>
                        </div>
                    </a>
                    --}}
                </div>
            </ul>
            <!-- END ADD NOTIFICATION -->

            @if(!isset($breadcrumb_auction))
                {{-- START FILTER --}}
                @include("layouts.auction-filter")
                {{-- END FILTER --}}

                {{-- START SORT --}}
                @include("layouts.auction-sort")
                {{-- END SORT --}}
            @endif



        </div>
        

        <div class="col-xs-4 sm-hide xs-show mo-logo-wrapper-menu text-center no-padding">
            <a href="{!! url('home') !!}">
                <img src="{{ asset('images/svg/logo-mo.svg') }}" class="img-responsive mo-logo" alt="moto Logo" onerror="this.onerror=null; this.src='{{ asset('images/logo-mo.png') }}'"/>
            </a>
        </div>

        <div class="col-xs-4 col-sm-12 col-md-2 no-padding mo-user-name-menu">
            <!-- <span class="mo-user-name-txt">Ricardo Luz</span> -->
            <a href="#" role="button" data-toggle="dropdown" data-target="#">
                <span class="mo-user-name-txt">
                    @if(Session::has('user_info'))
                        {{ Session::get('user_info')->first_name.' '.Session::get('user_info')->email }}
                    @endif
                </span>
                <img src="{{ asset('images/svg/main/icon-swipe-bottom-white.svg') }}" class="mo-icon-swipe" alt="moto Logo" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-bottom-white.png') }}'"/>
            </a>
            <ul class="dropdown-menu float-right mo-option" role="menu">
                <div class="mo-option-wrapper">
                    {{--<a href="#">--}}
                        {{--<div class="mo-option-item">--}}
                            {{--My Profile--}}
                        {{--</div>--}}
                    {{--</a>--}}
                    <hr class="mo-option-hr">
                    <a href="{{url('/dealership/profile-setting')}}">
                        <div class="mo-option-item">
                            Settings
                        </div>
                    </a>
                    <hr class="mo-option-hr">
                    <a href="{{url('/sign-out')}}">
                        <div class="mo-option-item">
                            Log Out
                        </div>
                    </a>
                </div>
            </ul>
        </div>
    </div>
    {{--End - moto Header Section--}}
@endsection