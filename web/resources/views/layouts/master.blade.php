<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/images/mo-app-icon.png" sizes="16x16">
    {{--<link rel="icon" href="../../favicon.ico">--}}

    <title>@yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Simple Sidebar CSS -->
    <link href="{{ asset('css/simple-sidebar.css') }}" rel="stylesheet">
    <!-- Magnific Popup core CSS file -->
    <link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet">

    <link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.dataTables.min.css') }}" rel="stylesheet">
    <!-- Time Picker -->
    <link rel="stylesheet" href="{{ asset('css/jquery.timepicker.css')}}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css')}}">
    <!-- Multi-Select CSS -->
    <link href="{{ asset('css/multi-select.css') }}" rel="stylesheet">
    <!-- moto master CSS -->
    <link href="{{ URL::asset('css/mo-master.css') }}" rel="stylesheet">
    <!-- autocomple CSS -->
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <!-- slider CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-slider.css') }}">
    
    <!-- BXSlider Image CSS -->
    <link rel="stylesheet" href="{{ asset('css/jquery.bxslider.css') }}">
    <!-- LightSlider Thumbnails CSS -->
    <link rel="stylesheet" href="{{ asset('css/lightslider.css') }}">
    
    <!-- DateTime Picker -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.css') }}">
    <!-- Multiselect-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-multiselect.css') }}">
    <!-- Bootstrap Toggle -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-toggle.css') }}">
    
    <link  href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" />

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        var base_url = "{{ env('APP_URL') }}" ;
    </script>

    {{--CUSTOM CSS ADDED FROM CONTROLLER--}}
    @if (isset($css))
        @foreach($css as $cssName)
            <link rel="stylesheet" href="<?php echo asset('css/' . $cssName) ?>.css">
        @endforeach
    @endif

    <?php
        $notifications = session('notifications');
        if ($notifications) {

            $notificationsJson = json_encode($notifications);
            ?>
            <script>
                var notificationsJson = '<?php echo $notificationsJson ?>';
                localStorage.setItem("notification_content", notificationsJson);
            </script>
            <?php
        }
    ?>
</head>

<body>
@include('layouts.header')
@include('layouts.sidebar')
<div class="container-fluid">
    <div class="row vh100">
        @yield('header')
        <div id="wrapper">
            @yield('sidebar')
            <div id="page-content-wrapper" class="percent-h-100">
                <div class="col-xs-12 col-sm-12 col-md-12 no-padding percent-h-100" id="mo-content-wrapper">
                    @yield('content')
                </div>
            </div>
            <div class="clear-both"></div>
            <a id="back-to-top" href="#" class="btn btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
        </div>
    </div>
</div><!-- /.container -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<!-- Magnific Popup core JS file -->
<script src="{{ asset('js/jquery.magnific-popup.js') }}"></script>
{{--<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>--}}
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.colResize.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/moment.min.js')}}"></script>
<script src="{{ asset('js/jquery.timepicker.min.js')}}"></script>
<script src="{{ asset('js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('js/jquery.multi-select.js')}}"></script>
<script src="{{ asset('js/jquery.quicksearch.js')}}"></script>
<script src="{{ asset('js/mo-master.js')}}"></script>
<script src="{{ asset('js/mo-master-data-table.js')}}"></script>
{{--<script src="{{ asset('js/autobahn.min.js') }}"></script>--}}
<script src="{{ asset('js/bootstrap-slider.min.js') }}"></script>

<script src="{{ asset('js/jquery.bxslider.js') }}"></script>
<script src="{{ asset('js/lightslider.js') }}"></script>

<script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('js/bootstrap-multiselect.js') }}"></script>

<script src="{{ asset('js/canvas-to-blob.min.js') }}"></script>
<script src="{{ asset('js/smudge.js') }}"></script>

<script src="{{ asset('js/bootstrap-toggle.js') }}"></script>
<script src="{{ asset('js/jSignature.js') }}"></script>
<script src="{{ asset('js/jquery.shiftcheckbox.js') }}"></script>


{{--CUSTOME JS ADDED FROM CONTROLLER--}}
@if (isset($js))
    @foreach($js as $jsName)
        <script src="<?php echo asset('js/' . $jsName) ?>.js"></script>
    @endforeach
@endif

{{-------------------------------------NOTIFICATION HANDLE-------------------------------------}}
{{--<script>--}}
    {{--$(function() {--}}
        {{--var count_no = 0;--}}
        {{--@if (isset($channels))--}}
            {{--var conn = new ab.Session('{{ env('NOTIFICATION_SERVER_URL') }}',--}}
                {{--function() {--}}
                    {{--@foreach($channels as $channel)--}}
                        {{--//LIVE AUCTION CHANNEL SECTION--}}
                        {{--@if(isset($channel->live_auction))--}}
                            {{--console.log('channel: {{ $channel->name }}');--}}
                            {{--conn.subscribe('{{ $channel->name }}', function (topic, data) {--}}
                                {{----}}
                                {{--if(data.content) {--}}
                                    {{--console.log(data.content);--}}
                                    {{--$("#current_bid_"+data.content.auction_item_id+" .live-auction-bid-price").css({"color" : "red"});--}}
                                    {{--$("#current_bid_"+data.content.auction_item_id+" .live-auction-bid-price").fadeOut(1000, "linear", function(){--}}
                                        {{--//set new bid price--}}
                                        {{--$("#current_bid_"+data.content.auction_item_id+" .live-auction-bid-price").text(accounting.formatMoney(data.content.bid_price, "", 2, ",", "."));--}}
                                        {{--var auction_price_increment = $("#quick_bid_"+data.content.auction_item_id).data("auction-price-increment");--}}
                                        {{--$("#quick_bid_"+data.content.auction_item_id).data("quick-bid", parseInt(data.content.bid_price) + parseInt(auction_price_increment));--}}

                                        {{--//price change effect--}}
                                        {{--$("#current_bid_"+data.content.auction_item_id+" .live-auction-bid-price").fadeIn(1000, "linear" ,function(){--}}
                                            {{--$("#current_bid_"+data.content.auction_item_id+" .live-auction-bid-price").css({"color" : "black"});--}}
                                        {{--});--}}

                                        {{--//trophy win--}}
                                        {{--if (data.content.user_id == {{Session::get('id')}}) {--}}
                                            {{--$("#current_bid_"+data.content.auction_item_id+" .live-auction-trophy").addClass("win");--}}
                                        {{--} else {--}}
                                            {{--$("#current_bid_"+data.content.auction_item_id+" .live-auction-trophy").removeClass("win");--}}
                                        {{--}--}}
                                    {{--});--}}
                                    {{----}}
                                {{--}--}}
                            {{--});--}}
                        {{--@else--}}
                        {{--//NOTIFICATION CHANNEL SECTION   --}}
                        {{--console.log('channel: {{ $channel->name }}');--}}
                        {{--conn.subscribe('{{ $channel->name }}', function (topic, data) {--}}
                            {{--if(data.content){--}}
                                {{--console.log(data.content);--}}
                                {{--/** SHOW BADGE **/--}}
                                {{--if(count_no < 20){/** temporarily display 20 badges **/--}}
                                    {{--count_no++;--}}
                                {{--}--}}
                                {{--var newIems = count_no;--}}
                                {{--$("#mo-badge").addClass('mo-badge').attr("data-badge",count_no);--}}

                                {{--/** RETRIEVE FROM STORAGE **/--}}
                                {{--// Retrieve the object from storage--}}
                                {{--var retrievedObject = localStorage.getItem('notification_content');--}}
                                {{--if(retrievedObject) {--}}
                                    {{--var parseRetrievedObject = JSON.parse(retrievedObject);--}}
                                    {{--if(parseRetrievedObject.length == 20){--}}
{{--//                                        parseRetrievedObject.shift();/** remove oldest item out of array **/--}}
                                        {{--parseRetrievedObject.pop();--}}
                                    {{--}--}}

{{--//                                    console.log(data.content);--}}
                                    {{--parseRetrievedObject.unshift(data.content);--}}
                                    {{--/** STORE with UPDATED array **/--}}
                                    {{--localStorage.setItem("notification_content", JSON.stringify(parseRetrievedObject));--}}
                                {{--}else{--}}
                                    {{--/** STORE NEW array **/--}}
                                    {{--var notification_arr = [];--}}
                                    {{--//console.log('start to push in new array ', data.content);--}}
                                    {{--notification_arr.push(data.content);--}}
                                    {{--localStorage.setItem("notification_content", JSON.stringify(notification_arr));--}}
                                {{--}--}}

                                {{--/** CLEAR BADGE **/--}}
                                {{--$("#mo-badge").click(function(e) {--}}
                                    {{--e.preventDefault();--}}
                                    {{--$("#mo-badge").removeClass("mo-badge").removeAttr("data-badge");--}}
                                    {{--count_no = 0;--}}

                                    {{--/** APPEND NOTIFICATION **/--}}
                                    {{--var mo_content_event_code = '/valuations/requests';--}}
                                    {{--var mo_content_a_tag = '';--}}
                                    {{--var mo_content_start ='<div class="mo-notification-item"><div class="mo-notification-title vertical-center">';--}}
                                    {{--var mo_content_end = '</p></div></a><hr class="mo-notification-hr">';--}}

                                    {{--/** REVERSE and SHOW LATEST notification **/--}}
                                    {{--// Retrieve the object from storage--}}
                                    {{--var afterRetrievedObject = localStorage.getItem('notification_content');--}}
                                    {{--var afterParseRetrievedObject = JSON.parse(afterRetrievedObject);--}}
                                    {{--//console.log(parseRetrievedObject.length);--}}
{{--//                                    afterParseRetrievedObject.reverse();--}}
                                    {{--$("#mo-notification-wrapper").empty();--}}
                                    {{--var newicon = '';var hightlight = '';--}}
                                    {{--for(var i=0; i<afterParseRetrievedObject.length; i++){--}}
                                        {{--var mo_content_time = '';--}}
                                        {{--var mo_content_reg = '';--}}
                                        {{--var mo_content_res_price = '';--}}
                                        {{--var mo_content_val_price = '';--}}
                                        {{--var mo_content_max_price = '';--}}

                                        {{--switch (afterParseRetrievedObject[i].eventCode) {--}}
                                            {{--case "valuation_request":--}}
                                                {{--mo_content_event_code = '/valuations/requests';--}}
                                                {{--break;--}}
                                            {{--case "valuation_request_notify_1":--}}
                                                {{--mo_content_event_code = '/valuations/requests';--}}
                                                {{--break;--}}
                                            {{--case "valuation_request_notify_2":--}}
                                                {{--mo_content_event_code = '/valuations/requests';--}}
                                                {{--break;--}}
                                            {{--case "valuation_accepted":--}}
                                                {{--mo_content_event_code = 'javascript:void(0)';--}}
                                                {{--break;--}}
                                            {{--case "valuer_complete_valuation":--}}
                                                {{--mo_content_event_code = '/valuations/pending';--}}
                                                {{--break;--}}
                                            {{--case "adjust_price_only":--}}
                                                {{--mo_content_event_code = '/valuations/pending';--}}
                                                {{--mo_content_res_price = '<br/>Requested Price: ';--}}
                                                {{--mo_content_val_price = '<br/>Valuation Price: ';--}}
                                                {{--break;--}}
                                            {{--case "approve_price_only":--}}
                                                {{--mo_content_event_code = '/valuations/pending';--}}
                                                {{--mo_content_val_price = '<br/>Valuation Price: ';--}}
                                                {{--break;--}}
                                            {{--case "approve_price_max":--}}
                                                {{--mo_content_event_code = '/valuations/pending';--}}
                                                {{--mo_content_val_price = '<br/>Valuation Price: ';--}}
                                                {{--mo_content_max_price = '<br/>Max Valuation Price: ';--}}
                                                {{--break;--}}
                                            {{--case "adjust_price_max":--}}
                                                {{--mo_content_event_code = '/valuations/pending';--}}
                                                {{--mo_content_res_price = '<br/>Requested Price: ';--}}
                                                {{--mo_content_val_price = '<br/>Valuation Price: ';--}}
                                                {{--mo_content_max_price = '<br/>Max Valuation Price: ';--}}
                                                {{--break;--}}
                                            {{--default:--}}
                                                {{--mo_content_event_code = 'javascript:void(0)';--}}
                                        {{--}--}}
                                        {{--mo_content_a_tag = '<a href="'+mo_content_event_code+'" class="mo-content">';--}}

                                        {{--var web_message = afterParseRetrievedObject[i].message;--}}
{{--//                                        if(afterParseRetrievedObject[i].web_message){--}}
{{--//                                            web_message = afterParseRetrievedObject[i].web_message;--}}
{{--//                                        }--}}

                                        {{--/** check to show new icon **/--}}
                                        {{--if(i < newIems){--}}
                                            {{--newicon = '<div class="col-xs-1 no-padding"><span class="mo-dot-icon float-right"></span></div>';--}}
                                            {{--hightlight = 'mo-highlight';--}}
                                        {{--}else{--}}
                                            {{--newicon = '';--}}
                                            {{--hightlight = '';--}}
                                        {{--}--}}

                                        {{--if(afterParseRetrievedObject[i].time){--}}
                                            {{--mo_content_time = '<br/>'+moment(afterParseRetrievedObject[i].time).format('Do MMM YYYY h:mm a').toString();--}}
                                        {{--}--}}

                                        {{--if(afterParseRetrievedObject[i].registration_number){--}}
                                            {{--mo_content_reg = '<br/>Reg:'+afterParseRetrievedObject[i].registration_number;--}}
                                        {{--}--}}

                                        {{--if(afterParseRetrievedObject[i].requested_price){--}}
                                            {{--mo_content_res_price += afterParseRetrievedObject[i].requested_price;--}}
                                        {{--}--}}

                                        {{--if(afterParseRetrievedObject[i].offer_value){--}}
                                            {{--mo_content_val_price += afterParseRetrievedObject[i].offer_value;--}}
                                        {{--}--}}

                                        {{--if(afterParseRetrievedObject[i].valuation_value){--}}
                                            {{--mo_content_max_price += afterParseRetrievedObject[i].valuation_value;--}}
                                        {{--}--}}

                                        {{--$("#mo-notification-wrapper").append(--}}
                                                {{--mo_content_a_tag +--}}
                                                {{--mo_content_start +--}}
                                                {{--'<h5 class="mo-item-title col-xs-11 no-padding '+hightlight+'">'+afterParseRetrievedObject[i].title+'</h5>'+newicon+'</div>'+--}}
                                                {{--'<p class="mo-item-info">'+web_message +--}}
                                                {{--//' '+afterParseRetrievedObject[i].valuationId +--}}
{{--//                                                mo_content_time +--}}
{{--//                                                mo_content_reg +--}}
                                                {{--mo_content_res_price +--}}
                                                {{--mo_content_val_price +--}}
                                                {{--mo_content_max_price +--}}
                                                {{--mo_content_end--}}
                                        {{--);--}}
                                    {{--}--}}
                                    {{--newIems = 0;--}}
                                {{--});--}}
                            {{--}--}}
                            {{--//console.log(obj);--}}
                        {{--});--}}
                        {{--@endif--}}

                    {{--@endforeach--}}
                {{--},--}}
                {{--function() {--}}
                    {{--console.warn('WebSocket connection closed');--}}
                {{--},--}}
                {{--{'skipSubprotocolCheck': true}--}}
            {{--);--}}
        {{--@endif--}}
    {{--});--}}
{{--</script>--}}
{{-------------------------------------NOTIFICATION HANDLE-------------------------------------}}
</body>
</html>
