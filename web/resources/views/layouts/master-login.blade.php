<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="refresh" content="3600">
   {{-- <link rel="icon" href="/images/mo-app-icon.png" sizes="16x16">--}}
    {{--<link rel="icon" href="../../favicon.ico">--}}

    <title>@yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- moto master CSS -->
    <link href="{{ asset('css/mo-master.css') }}" rel="stylesheet">
    <!-- moto login CSS -->
    <link href="{{ asset('css/mo-login.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        var base_url = "{{ env('APP_URL') }}" ;
    </script>
</head>

<body>

<div class="login-bg-img">

    <div class="container">
        <div class="row">
            <div class="absolute-center is-responsive col-xs-10 col-sm-8 col-md-8">
                {{--Start - moto Logo Section--}}
                <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-logo-container padding-bottom-54">
                    <img src="{{ asset('images/svg/moto-logo.png') }}" class="img-responsive mo-logo" alt="moto Logo" onerror="this.onerror=null; this.src='{{ asset('images/moto-logo.png') }}'"/>
                </div>
                {{--End - moto Logo Section--}}
                @yield('content')
            </div>
        </div>
    </div><!-- /.container -->
    <div class="mo-version">1.1.7</div>
</div>

{{--<div class="mo-white-popup mfp-hide" id="mo-term-and-condition-popup">
    <div class="mo-row padding-bottom-20">
        <span class="text-capitalize float-left mo-popup-heading">term & condition</span>
    </div>
    <div class="mo-row">
        <hr class="mo-form-hr">
        <textarea class="mo-textarea form-control disabled-div" id="test-drive-term-condition" rows="20" style="border: none;" readonly>{{$term_and_condition->term_and_condition}}</textarea>
        <hr class="mo-form-hr">
    </div>
    <div class="mo-row padding-top-24">
        <div class="float-left">
            <input type="checkbox" class="mo-round-check" id="mo-checkbox-term-condition" name="mo_checkbox_term_condition">
            <label for="mo-checkbox-term-condition"><span></span>I accept these Term & Conditions</label>
        </div>
        <div class="float-right">
            <input type="button" class="mo-btn text-uppercase grey-btn disabled-div" id="mo-term-condition-agree-btn" value="accept">
        </div>
    </div>
</div>--}}



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
{{--<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>--}}
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/mo-login.js') }} "></script>
<script src="{{ asset('js/jquery.magnific-popup.js') }}"></script>
</body>
</html>
