@section('sidebar')
    {{--Start - moto Sidebar Section--}}
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand text-uppercase">
                Main Navigation
            </li>
            @if(Session::has('dealership_menus_sidebar'))
                @foreach(Session::get('dealership_menus_sidebar') as $site_menu)
                    @if($site_menu->site_menu == "dealership")
                        @foreach($site_menu->group as $group_menu)
                            <li>
                                <a href="{{isset($group_menu->group_link)?url($group_menu->group_link):"javascript:void(0);"}}" class="text-uppercase mo-highlight" id="{{$group_menu->group_name}}">{{$group_menu->group_name}}</a>
                                @if(isset($group_menu->menu_items))
                                    <ul class="sidebar-submenu">
                                        @foreach($group_menu->menu_items as $menu_item)
                                            <li>
                                                <a href="{{ url($menu_item->link) }}" class="text-capitalize {{$menu_item->link == ""?"disabled-div":""}}" id="{{$menu_item->code}}">{{$menu_item->name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    @endif
                @endforeach
            @endif
            {{--<li>
                <a href="{!! url("dashboard") !!}" class="text-uppercase disabled-div mo-highlight">Dashboard</a>
            </li>
            <li>
                <a href="javascript:void(0);" class="text-uppercase mo-highlight">Configure</a>
                <ul class="sidebar-submenu">
                    <li>
                        <a href="{!! url('dealership/location') !!}">Location</a>
                    </li>
                    <li>
                        <a href="{!! url('dealership/roles') !!}">Roles</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="disabled-div">Permissions</a>
                    </li>
                    <li>
                        <a href="{!! url('dealership/users-list') !!}">Users</a>
                    </li>
                    <li>
                        <a href="{!! url('dealership/settings') !!}">Settings</a>
                    </li>
                    <li>
                        <a href="{!! url('dealership/external-offer-groups') !!}">External Offer Group</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{!! url('customers') !!}" class="text-uppercase mo-highlight">Customers</a>
            </li>
            <li>
                <a href="javascript:void(0);" class="text-uppercase mo-highlight">Valuations</a>
                <ul class="sidebar-submenu">
                    <li>
                        <a id="mo-valuation-request" href="{!! url('valuations/requests') !!}">Requests</a>
                    </li>
                    <li>
                        <a href="{!! url('valuations/in-progress') !!}">In Progress</a>
                    </li>
                    <li>
                        <a href="{!! url('valuations/pending') !!}">Pending Approval</a>
                    </li>
                    <li>
                        <a href="{!! url('valuations/complete') !!}">Complete</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="text-uppercase mo-highlight">Place My Car</a>
                <ul class="sidebar-submenu">
                    <li>
                        <a href="{!! url('external-offer/incoming-offers') !!}" class="text-capitalize">Incoming Offers</a>
                    </li>
                    <li>
                        <a href="{!! url('external-offer/outgoing-offers') !!}" class="text-capitalize">Outgoing Offers</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="text-uppercase mo-highlight">Test Drives</a>
                <ul class="sidebar-submenu">
                    <li>
                        <a href="{!! url('test-drives/inprogress') !!}" class="text-capitalize">In Progress</a>
                    </li>
                    <li>
                        <a href="{!! url('test-drives/complete') !!}" class="text-capitalize">Complete</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="text-uppercase mo-highlight">Auction</a>
                <ul class="sidebar-submenu">
                    <li>
                        <a href="{!! url('/trading/stock-list') !!}">Stock List</a>
                    </li>
                    <li>
                        <a href="{!! url('/trading/auction') !!}">Auction Listings</a>
                    </li>
                    <li>
                        <a href="{!! url('/trading/live-auction') !!}">Live Auction</a>
                    </li>
                    <li>
                        <a href="{!! url('/trading/wishlist') !!}">Wish List</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{!! url('/arriving-vehicle/arriving-list') !!}" class="text-uppercase mo-highlight">Arriving Vehicle</a>
            </li>
            <li>
                <a href="javascript:void(0);" class="text-uppercase mo-highlight">Dispute Centre</a>
                <ul class="sidebar-submenu">
                    <li>
                        <a href="{!! url('/dispute/current') !!}">Current</a>
                    </li>
                    <li>
                        <a href="{!! url('/dispute/resolved') !!}">Resolved</a>
                    </li>
                </ul>
            </li>--}}
            {{--<li>
                <a href="javascript:void(0);" class="text-uppercase mo-highlight">Accounting</a>
                <ul class="sidebar-submenu">
                    <li>
                        <a href="javascript:void(0);" class="disabled-div">Documents</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="disabled-div">Invoices</a>
                    </li>
                </ul>
            </li>--}}
            {{--<li>
                <a href="javascript:void(0);" class="text-uppercase mo-highlight">Transactions</a>
                <ul class="sidebar-submenu">
                    <li>
                        <a href="{!! url('/transaction/view-transactions/current') !!}">Current</a>
                    </li>
                    <li>
                        <a href="{!! url('/transaction/view-transactions/complete') !!}">Complete</a>
                    </li>
                </ul>
            </li>--}}
            {{--
            <li>
                <a href="javascript:void(0);">Wishlist</a>
            </li>
            <li>
                <a href="javascript:void(0);">User History</a>
            </li>--}}
        </ul>
    </div>
    {{--End - moto Sidebar Section--}}
@endsection