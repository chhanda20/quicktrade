<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="refresh" content="3600">
    <link rel="icon" href="/images/mo-app-icon.png" sizes="16x16">

    <title>@yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- moto master CSS -->
    <link href="{{ asset('css/mo-master.css') }}" rel="stylesheet">
    <!-- moto login CSS -->
    <link href="{{ asset('css/mo-login.css') }}" rel="stylesheet">
    <!-- moto 404 CSS -->
    <link href="{{ asset('css/mo-404.css') }}" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="login-bg-img">

    <div class="container">
        <div class="row">
            <div class="absolute-center is-responsive col-xs-10 col-sm-8 col-md-8">
                @yield('content')
            </div>
        </div>
    </div><!-- /.container -->
</div>



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
{{--<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>--}}
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/mo-login.js')}}"></script>
</body>
</html>
