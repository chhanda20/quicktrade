@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <div class="col-xs-12 col-sm-8 col-md-8 no-padding">
                <a href="{!! url('customers'); !!}">{{ $header or 'Customers' }}</a> > {{isset($first_name) ? $first_name : ''}} {{isset($last_name) ? $last_name : ''}}
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 no-padding text-right mo-edit-txt">
                <a href="/edit-customer/edit/{{isset($id) ? $id : '1'}}">
                    <span>Edit <img src="{{ asset('images/svg/main/icon-edit.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-edit.png') }}'"></span>
                </a>
            </div>
        </div>
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - moto Details Section--}}
    <div class="mo-row mo-form-wrapper">
        <form class="form-horizontal" id="mo-customer-details">
            {{ csrf_field() }}
            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                <span class="col-xs-2 col-sm-1"><img src="{{ asset('images/svg/main/icon-normal-client-name.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-normal-client-name.png') }}'"></span>
                <span class="col-xs-10 col-sm-5">
                    {{isset($first_name) ? $first_name : null}} {{isset($last_name) ? $last_name : null}}
                </span>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <span class="col-xs-2 col-sm-2"><img src="{{ asset('images/svg/main/icon-normal-home-address.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-normal-home-address.png') }}'"></span>
                <span class="col-xs-10 col-sm-10">
                    {{isset($address1) ? $address1 : null}}<br>
                    {{isset($address2)&&!empty($address2) ? $address2.',' : null}}
                    {{isset($suburb)&&!empty($suburb) ? $suburb.',' : null}}
                    {{isset($postcode)&&!empty($postcode) ? $postcode.',' : null}}
                    {{--isset($state)&&!empty($state) ? $state.',' : null--}}
                    {{isset($country_name)&&!empty($country_name) ? $country_name : null}}
                </span>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <div class="form-group col-xs-12 col-sm-12 col-md-12">
                    <span class="col-xs-2 col-sm-2"><img src="{{ asset('images/svg/main/icon-normal-mobile-number.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-normal-mobile-number.png') }}'"></span>
                    <span class="col-xs-10 col-sm-10">
                        {{isset($mobile_phone) ? $mobile_phone : null}}
                    </span>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-12">
                    <span class="col-xs-2 col-sm-2"><img src="{{ asset('images/svg/main/icon-normal-email.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-normal-email.png') }}'"></span>
                    <span class="col-xs-10 col-sm-10">
                        {{isset($email) ? $email : null}}
                    </span>
                </div>
            </div>
        </form>
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding" id="mo-blocks-wrapper">
            <div class="col-xs-12 col-sm-4 col-md-4">
                <a href="#valuation" id="customer-detail-valuation-block">
                    <div class="col-xs-12 col-sm-12 col-md-12 mo-block">
                        <span class="col-xs-12 mo-block-number"><span id="valuation-count">{{isset($valuations_count)?$valuations_count:0}}</span></span>
                        <span class="col-xs-12 mo-block-txt text-capitalize">valuations</span>
                        <span class="col-xs-12 text-center mo-arrow-focus">
                            <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                        </span>
                    </div>
                </a>
            </div>

            {{--test drive block--}}
            <div class="col-xs-12 col-sm-4 col-md-4">
                <a href="#test-drive" id="customer-detail-test-drive-block">
                    <div class="col-xs-12 col-sm-12 col-md-12 mo-block test-drive-block">
                        <span class="col-xs-12 mo-block-number"><span id="test-drive-count">{{isset($test_drive_count)?$test_drive_count:0}}</span></span>
                        <span class="col-xs-12 mo-block-txt text-capitalize">test drives</span>
                        <span class="col-xs-12 text-center mo-arrow-focus">
                            <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                        </span>
                    </div>
                </a>
            </div>
            {{--end test drive block--}}

            {{-- interaction block --}}
            <div class="col-xs-12 col-sm-4 col-md-4 ">
                <a href="#interaction" class="" id="customer-detail-interaction-block">
                    <div class="col-xs-12 col-sm-12 col-md-12 mo-block interaction-block">
                        <span class="col-xs-12 mo-block-number"><span id="interaction-count">{{isset($interaction_count)?$interaction_count:0}}</span></span>
                        <span class="col-xs-12 mo-block-txt text-capitalize">interaction</span>
                        <span class="col-xs-12 text-center mo-arrow-focus">
                            <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                        </span>
                    </div>
                </a>
            </div>
            {{-- end interaction block --}}

        </div>
    </div>
    {{--End - moto Details Section--}}

    {{--Start - moto Interaction Content Section--}}
    <div class="mo-row mo-form-wrapper margin-top-32 hidden mo-block-list" id="mo-interaction-list">
        {{--<div class="col-xs-12 vertical-center">--}}
            {{--Under Construction--}}
        {{--</div>--}}
        <div class="col-xs-12 col-sm-12 col-md-12 padding-bottom-40">
            <span class="text-capitalize mo-block-heading">Interaction</span>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <table id="mo-table-interaction-list" class="table" cellspacing="0">
                <thead>
                <tr>
                    <th></th>
                    <th>Date time</th>
                    <th></th>
                    <th>Type</th>
                    <th>Comment</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
                <ul class="pagination">
                    <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                    <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
                </ul>
            </div>
        </div>
        <div class="mo-round-btn"><a href="#mo-interaction-popup" class="open-popup-link">NEW</a></div>
    </div>
    {{--End - moto Interaction Content Section--}}

    {{--Start - moto Test Drives Content Section--}}
    <div class="mo-row mo-form-wrapper margin-top-32 hidden mo-block-list" id="mo-test-drives-list">
        {{--<div class="col-xs-12 vertical-center">--}}
            {{--Under Construction--}}
        {{--</div>--}}
        <div class="col-xs-12 col-sm-12 col-md-12 padding-bottom-40">
            <span class="text-capitalize mo-block-heading">test drives</span>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <table id="mo-table-testdrive-list" class="table" cellspacing="0">
                <thead>
                <tr>
                    <th></th>
                    <th>Make</th>
                    <th>Model</th>
                    <th>Year</th>
                    <th>Colour</th>
                    <th>Start Date&Time</th>
                    <th>Time Out</th>
                    <th>Returned</th>
                    <th>Status</th>
                    <th>Comment</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
                <ul class="pagination">
                    <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                    <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
                </ul>
            </div>
        </div>
        <div class="mo-round-btn"><a href="/test-drives/create-test-drives/{{$id}}">NEW</a></div>
    </div>
    {{--End - moto Test Drives Content Section--}}


    {{--Start - moto Valuations Content Section--}}
    <div class="mo-row mo-form-wrapper margin-top-32 hidden mo-block-list" id="mo-valuations-list">
        {{--<div class="col-xs-12 vertical-center">
            Under Construction
        </div>--}}
        <div class="col-xs-12 col-sm-12 col-md-12 padding-bottom-40">
            <span class="text-capitalize mo-block-heading">valuations</span>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <table id="mo-table-valuations-list" class="table" cellspacing="0">
                <thead>
                    <tr>
                        <th></th>
                        <th>Date</th>
                        <th>Vehicles</th>
                        <th>Price</th>
                        <th class="text-center">Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
                <ul class="pagination">
                    <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                    <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
                </ul>
            </div>
        </div>
        <input type="hidden" value="{{$id}}" name="customer_id" id="customer-id">
        @if (in_array("valuation_create",$permissions))
            <div class="mo-round-btn"><a href="{{url('/create-valuation/'.$id)}}" class="">NEW</a></div>
        @endif
        
    </div>
    {{--End - moto Valuations Content Section--}}

    {{--Start - moto Edit Interaction Popup --}}
    <div id="mo-interaction-edit-popup" class="mo-white-popup mfp-hide">
        <form action="" method="" class="mo-popup-form" id="mo-interaction-edit-popup-form">
            {{csrf_field()}}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">edit interaction</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row" style="padding-left: 10px;">
                <div class="mo-row padding-top-10 padding-bottom-20">
                    <div class="form-group col-xs-10 no-padding">
                        <label for="date-time" class="text-capitalize">date time</label>
                        <div class="input-group date" id="interaction-edit-date-picker">
                            <input type="text" class="form-control" id="edit-date-time" name="edit_date_time">
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar interaction-calendar"></span>
                        </span>
                        </div>
                        <div id="errorToShow_editdatetime"></div>
                    </div>
                </div>

                <div class="mo-row padding-bottom-20">
                    <div class="col-xs-10 no-padding">
                        <label for="type" class="text-capitalize">type</label>
                        <div class="mo-dropdown-wrapper">
                            <span class="mo-icon-swipe glyphicon glyphicon-menu-down float-right"></span>
                            <select class="form-control text-capitalize" id="edit-type" name="edit_type">
                                <option  disabled selected>Please select</option>
                                @if(isset($type))
                                    @foreach($type as $interaction_type)
                                        <option value="{{$interaction_type->code}}">{{$interaction_type->value}}</option>
                                    @endforeach
                                @endif

                            </select>
                        </div>    
                    </div>
                </div>

                <div class="mo-row padding-bottom-20">
                    <div class="form-group col-xs-12 no-padding">
                        <label for="comment" class="text-capitalize">commnent</label>
                        <hr class="mo-textarea-hr" />
                        <textarea class="mo-textarea form-control" rows="5" id="edit-note" name="edit_note" placeholder="Write here…" style="box-shadow: none;"></textarea>
                        <hr class="mo-textarea-hr" />
                    </div>
                </div>
            </div>
            
        
            <hr class="mo-form-hr"/>
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" id="interaction_cancel_btn" value="cancel" />
                    <input type="button" class="mo-btn text-uppercase" id="interaction_update_btn" value="update" />
                </div>
            </div>
        </form>    
        
    {{--</div> <!-- end Interaction Edit Popup -->--}}

    {{--Start - moto Popup Section - NEW INTERACTION --}}
    <div id="mo-interaction-popup" class="mo-white-popup mfp-hide">
        <form action="{!! url('/create-interaction') !!}" method="post" class="mo-popup-form" id="mo-interaction-popup-form">
        {{csrf_field()}}
        {{--{{ var_dump(Session::get('contact_id')) }}--}}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">new interaction</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row" style="padding-left: 10px;">
                <div class="mo-row padding-top-10 padding-bottom-20">
                    <div class="form-group col-xs-10 no-padding">
                        <label for="date-time" class="text-capitalize">date time</label>
                        <div class="input-group date" id="interaction-date-picker">
                            <input type="text" class="form-control" id="date-time" name="date_time"  />
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar interaction-calendar"></span>
                        </span>
                        </div>
                        <div id="errorToShow_date_time"></div>
                    </div>
                </div>

                <div class="mo-row padding-bottom-20">
                    <div class="col-xs-10 no-padding">
                        <label for="type" class="text-capitalize">type</label>
                        <div class="mo-dropdown-wrapper">
                            <span class="mo-icon-swipe glyphicon glyphicon-menu-down float-right"></span>
                            <select class="form-control text-capitalize" id="type" name="type">
                                <option selected="" disabled="">Please select</option>
                                @if(isset($type))
                                    @foreach($type as $interaction_type)
                                        <option value="{{$interaction_type->code}}">{{$interaction_type->value}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div id="errorToShow_type"></div>
                    </div>
                </div>

                <div class="mo-row padding-bottom-20">
                    <div class="form-group col-xs-12 no-padding">
                        <label for="comment" class="text-capitalize">commnent</label>
                        <hr class="mo-textarea-hr" />
                        <textarea class="mo-textarea form-control" rows="5" id="note" name="note" placeholder="Write here…" style="box-shadow: none;"></textarea>
                        <hr class="mo-textarea-hr" />
                    </div>
                </div>
            </div>

            <hr class="mo-form-hr"/>
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" id="interaction_cancel_btn" value="cancel">
                    <input type="submit" class="mo-btn text-uppercase" id="interaction_save_btn" value="save">
                </div>
            </div>
        </form>
    </div> <!-- end Interaction -->

    {{--<!-- DELETE AN INTERACTION -->--}}
    <div id="mo-remove-confirm-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">delete interaction</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <span class="suspend-popup-content">Are you sure you want to delete the interaction?</span>
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-confirm-delete">
                </div>
            </div>
        </form>
        <div class="mo-loading-image hidden">Loading&#8230;</div>
    </div>

    {{--End - moto Popup Section--}}




    {{-- start Comment Popup Test Drive --}}
    <div id="mo-test-drives-comment-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form" id="mo-test-drive-return-vehicle-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">2014 Maserati Ghibli</span>
            </div>
            <hr class="mo-form-hr padding-bottom-10" />
            
            <span><label for=" " class="text-capitalize padding-bottom-20">feedback</label></span>
            
            <div>
                <div class="padding-bottom-10 padding-left-10">How happy were you with the vehicle?</div> 
                <div id="slider_vehicle_rating" class=""></div>

                <div class="padding-bottom-10 padding-left-10">How happy were you with us?</div>    
                <div id="slider_mo_rating" class=""></div>

                <div>
                    <div class="padding-left-10">How did you hear about us?</div>
                    <div class="col-xs-12">
                        <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}"></span>
                        <select class="form-control" name="mo_marketing_option" id="mo_marketing_option">
                            @if(isset($media_type))
                                @foreach($media_type as $media)
                                    <option value="{{$media->code}}">{{$media->value}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                <div class="col-xs-12 padding-top-10 hidden" id="mo-marketing-option">
                    <input type="text" class="form-control" name="mo_marketing_code" id="mo_marketing_code" disabled>
                </div>

                <div class="col-xs-12 padding-top-10 hidden" id="mo-marketing-option-other">
                    <input type="text" class="form-control" id="mo-option-other" name="mo_option_other">
                </div>
            </div>

            <span><label for=" " class="text-capitalize padding-top-24">comments</label></span>
            <div class="mo-row padding-bottom-24">
                <hr class="mo-dispute-hr" />
                <div class="form-group col-xs-12 no-padding">
                    <textarea class="mo-textarea form-control" rows="3" id="test-drive-note" name="test_drive_note" placeholder="Write here…" style="box-shadow: none;"></textarea>
                </div>
            </div>
            <hr class="mo-dispute-hr padding-bottom-20"/>
            <div id="errorToShow"></div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-test-drives-inprogress-change-status">
                </div>
            </div>
            <div class="mo-loading-image hidden">Loading...</div>
        </form>
    </div>
@endsection