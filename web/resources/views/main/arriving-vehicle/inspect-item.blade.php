<div class="mo-row">
    <label for="external" class="text-capitalize">external</label>
    <label for="" class="text-capitalize float-right mo-arriving-vehicle-total-cost"><span>${{isset($inspectionItems->exterior_section->total_cost)?number_format($inspectionItems->exterior_section->total_cost, 2):0}}</span></label>

    <hr class="mo-dispute-hr" />
    <div class="mo-row">
        <span class="mo-arriving-vehicle-item text-capitalize section-item">general recondition cost</span>
        <span class="mo-arriving-vehicle-price float-right">${{$generalReconditionCost or 0}}</span>
    </div>

    @if(isset($inspectionItems->exterior_section) && !empty($inspectionItems->exterior_section))
        @foreach($inspectionItems->exterior_section as $ex_item)
            @if(isset($ex_item->items))
                    <hr class="mo-dispute-hr" />
                    <div class="mo-row">
                        <span class="mo-arriving-vehicle-item force-font-black-color font-avenir-demi">{{$ex_item->section_name or ''}}</span>
                        <span class="mo-arriving-vehicle-price float-right"></span>
                    </div>
                    @if(count($ex_item->items) > 0)
                        @foreach($ex_item->items as $item)
                            <hr class="mo-dispute-hr" />
                            <div class="mo-row">
                                <span class="mo-arriving-vehicle-item section-item">{{$item->item or ''}}</span>
                                <span class="mo-arriving-vehicle-price float-right">${{number_format($item->repair_cost, 2)}}</span>
                            </div>
                        @endforeach
                    @endif
            @endif
        @endforeach
    @endif
</div>

<!-- Internal -->
<hr class="mo-dispute-hr" />
<div class="mo-row">
    <label for="internal" class="text-capitalize ">internal</label>
    <label for="" class="text-capitalize float-right mo-arriving-vehicle-total-cost"><span>${{ isset($inspectionItems->interior_section->total_cost) ? number_format($inspectionItems->interior_section->total_cost, 2) :0 }}</span></label>

    @if(isset($inspectionItems->interior_section) && !empty($inspectionItems->interior_section))
        @foreach($inspectionItems->interior_section as $in_item)
            @if(isset($in_item->items))
                @if(count($in_item->items) > 0)
                    @foreach($in_item->items as $item)
                        <hr class="mo-dispute-hr" />
                        <div class="mo-row">
                            <span class="mo-arriving-vehicle-item section-item">{{$item->item or ''}}</span>
                            <span class="mo-arriving-vehicle-price float-right">${{number_format($item->repair_cost, 2)}}</span>
                        </div>
                    @endforeach
                @endif
            @endif
        @endforeach
    @endif
</div>

<!-- Mechanical -->
<hr class="mo-dispute-hr" />
<div class="mo-row">
    <label for="internal" class="text-capitalize">mechanical</label>
    <label for="" class="text-capitalize float-right mo-arriving-vehicle-total-cost"><span>${{ isset($inspectionItems->mechanical_section->total_cost) ? number_format($inspectionItems->mechanical_section->total_cost, 2) : 0 }}</span></label>

    @if(isset($inspectionItems->mechanical_section) && !empty($inspectionItems->mechanical_section))
        @foreach($inspectionItems->mechanical_section as $me_item)
            @if(isset($me_item->items))
                @if(count($me_item->items) > 0)
                    @foreach($me_item->items as $item)
                        <hr class="mo-dispute-hr" />
                        <div class="mo-row">
                            <span class="mo-arriving-vehicle-item section-item">{{$item->item or ''}}</span>
                            <span class="mo-arriving-vehicle-price float-right">${{number_format($item->repair_cost, 2)}}</span>
                        </div>
                    @endforeach
                @endif
            @endif
        @endforeach
    @endif
</div>

<hr class="mo-dispute-hr" />
<div class="mo-row">
    <span class="float-right text-capitalize mo-arriving-vehicle-total-all"><label>total: ${{ number_format($damageTotal, 2) }}</label></span>
</div>
