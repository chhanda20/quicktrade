@if(isset($transactionList))
    @foreach($transactionList as $transaction)
        <div class="mo-row mo-arriving-vehicle-info">
            <div class="mo-row padding-bottom-24 text-uppercase font-family-bold">
                {{$transaction->vehicle_name or ''}}
            </div>
            <div class="mo-row">
                <div class="col-xs-12 col-sm-3 no-padding">
                    <div class="mo-vehicle-image-wrapper" class="vertical-center">
                        <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$transaction->vehicle_id or ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'" class="max-height-150 align-center img-responsive">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-9 no-padding" id="mo-arriving-vehicle-current-info">
                    <div class="col-xs-12 col-sm-9">
                        <div class="mo-row">
                            <span class="col-xs-12 col-sm-6 no-padding text-capitalize">registration number:</span>
                            <label class="col-xs-12 col-sm-6 no-padding text-uppercase">{{$transaction->registration_number or ''}}</label>
                        </div>
                        <div class="mo-row">
                            <span class="col-xs-12 col-sm-6 no-padding text-capitalize">status:</span>
                            <label class="col-xs-12 col-sm-6 no-padding text-capitalize">{{$transaction->status_value or ''}}</label>
                        </div>
                        <div class="mo-row">
                            <span class="col-xs-12 col-sm-6 no-padding text-capitalize">transaction:</span>
                            <label class="col-xs-12 col-sm-6 no-padding text-capitalize">{{$transaction->rid or ''}}</label>
                        </div>
                        <div class="mo-row">
                            <span class="col-xs-12 col-sm-6 no-padding text-capitalize">price:</span>
                            <label class="col-xs-12 col-sm-6 no-padding text-capitalize">${{number_format($transaction->vehicle_price, 2)}}</label>
                        </div>
                        <div class="mo-row">
                            <span class="col-xs-12 col-sm-6 no-padding text-capitalize">location:</span>
                            <label class="col-xs-12 col-sm-6 no-padding text-capitalize">{{$transaction->location_name or ''}}</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 padding-top-60 text-uppercase">
                        @if($transaction->status == 3 || $transaction->status == 8)
                            <a class="yellow-button bold text-align-center float-right mo-inspect" href="/arriving-vehicle/inspect/{{$transaction->id}}">Inspect</a>
                        @endif
                        <div class="clear-both"></div>
                        <input type="button" class="mo-btn float-right margin-top-10 mo-more-info" data-valuation-id="{{$transaction->valuation_id or ''}}" data-valuation-version="{{$transaction->valuation_version or ''}}" value="more info">
                        <div class="clear-both"></div>
                        <input type="button" class="mo-btn float-right margin-top-10 mo-close-info background-grey" value="close info">
                    </div>
                </div>
            </div>

            <div class="mo-row col-xs-12 text-center mo-loading-three-dot-wrapper">
                <img class="mo-loading-three-dot-icon" src="{{ asset('images/main/three-dot-loading.gif') }}" alt="Loading Icon">
            </div>

            {{--MORE DETAIL--}}
            <div class="mo-row" id="mo-arriving-vehicle-details">

            </div>



            <div class="mo-row">
                <hr class="mo-dispute-hr">
            </div>
        </div>
    @endforeach
    <div class="mo-row col-xs-12 text-center mo-loading-wrapper">
        <img class="mo-loading-icon" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
    </div>
@endif