@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
{{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                Arriving Vehicle > <a href="#">{{ $header or 'Current' }}</a> > Additional Details
            </div>
        </div>
    </div>
{{--End - moto Content Header Section--}}
    <div class="mo-row mo-form-wrapper">
        <form class="form-horizontal" id="mo-dispute-additional-details-contact-form">
            {{ csrf_field() }}
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                contact person
            </div>
            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                <span class="col-xs-2 col-sm-1"><img src="{{ asset('images/svg/main/icon-normal-client-name.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-normal-client-name.png') }}'"></span>
                <span class="col-xs-10 col-sm-11">
                    <input type="text" placeHolder="First Name" name="dispute_contact_first_name" />
                    <input type="text" placeHolder="Last Name" name="dispute_contact_last_name" />
                </span>
            </div>
            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                <span class="col-xs-2 col-sm-1"><img src="{{ asset('images/svg/main/icon-normal-mobile-number.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-normal-mobile-number.png') }}'"></span>
                <span class="col-xs-10 col-sm-5"> 
                    <input type="number" placeHolder="Phone" name="dispute_contact_phone" />
                </span>
            </div>
            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                <span class="col-xs-2 col-sm-1"><img src="{{ asset('images/svg/main/icon-normal-email.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-normal-email.png') }}'"></span>
                <span class="col-xs-10 col-sm-5"> 
                    <input type="text" placeHolder="Email" name="dispute_contact_email" />
                </span>
            </div>
        </form>
    </div>

    <div class="mo-row exterior mo-form-wrapper margin-top-32 mo-block-list col-xs-12" id="mo-details-block">
        
        <!-- title -->
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
            estimated reconditioning costs
        </div>
        <!-- end title -->

        <div class="mo-row" id="mo-dispute-estimated-reconditioning-cost">
            <div class="mo-row">
                <label for="dispute" class="text-capitalize mo-dispute col-xs-12 no-padding text-align-right">dispute</label>
            </div>

            <hr class="mo-dispute-hr" />
            <!-- External section -->
            <div class="mo-row">
                <label for="external" class="text-capitalize col-xs-8 no-padding">external</label>
                <label for="" class="text-capitalize col-xs-3 text-align-right no-padding">
                    <input type="hidden" name="external_price" value="{{isset($inspectionItems->exterior_section->total_cost) ? number_format($inspectionItems->exterior_section->total_cost, 2) : 0}}">
                    <span class="padding-right-80">$<span id="external-section-price">{{isset($inspectionItems->exterior_section->total_cost) ? number_format($inspectionItems->exterior_section->total_cost, 2) : 0}}</span></span>
                </label>
                
                <!-- General Reconditioning Cost -->
                <hr class="mo-dispute-hr col-xs-12" />
                <div class="mo-row exterior-section">
                    <span class="mo-dispute-item col-xs-8 no-padding force-padding-left-40">General Reconditioning Cost</span>
                    <span class="col-xs-3 no-padding text-align-right padding-right-80">${{$generalReconditionCost}}</span>
                    <span class="col-xs-1 no-padding text-align-right"><input type="checkbox" data-toggle="toggle" class="mo-dispute-toggle mo-dispute-inspect-id" name="general_recond" value=""></span>
                </div>
                <!-- End General Reconditioning Cost -->

                <!-- Append exterior section here -->
                {{--<hr class="mo-dispute-hr" />
                <div class="mo-row exterior-">
                    <span class="mo-dispute-item force-font-black-color font-avenir-demi" >New section</span>
                </div>--}}
                <!-- End append exterior section -->
                <!-- Append exterior item here -->
                {{--<hr class="mo-dispute-hr" />
                <div class="mo-row">
                    <span class="mo-dispute-item col-xs-8 no-padding force-padding-left-40">new item</span>
                    <span class="col-xs-3 text-align-right no-padding padding-right-80">$</span>
                    <span class="col-xs-1 no-padding text-align-right"><input type="checkbox" checked data-toggle="toggle" class="mo-dispute-toggle mo-dispute-inspect-id" value="" name="inspection_item_id[]"></span>
                </div>--}}
                <!-- End append item section -->
                @if(isset($inspectionItems->exterior_section) && !empty($inspectionItems->exterior_section))

                    @foreach ($inspectionItems->exterior_section as $ex_item)

                        @if(isset($ex_item->items))
                            <!-- Exterior section -->
                            <hr class="mo-dispute-hr original" />
                            <div class="mo-row dispute-items exterior-{{str_replace(" ", "-", strtolower($ex_item->section_name))}} col-xs-12 no-padding dispute-section" id="section-id-{{$ex_item->section_type}}" data-section-id="{{$ex_item->section_type}}">
                                <span class="force-font-black-color font-avenir-demi padding-left-20" >{{$ex_item->section_name}}</span>
                            </div>
                            <!-- End Exterior section -->
                            @if(count($ex_item->items) > 0)

                                @foreach($ex_item->items as $item)
                                    <!-- items -->
                                    <hr class="mo-dispute-hr original col-xs-12" />
                                    <div class="mo-row original">
                                        <a href="#mo-arriving-vehicle-additional-detail-popup" class="open-popup-link inspection-item-link original"
                                            data-category="EXTERIOR_SECTION"
                                            data-session-id="{{$ex_item->section_type}}"
                                            data-item-id="{{$item->inspection_item_id}}"
                                            data-item-name="{{$item->item}}"
                                            data-valuation-id=""
                                            data-note="{{$item->note}}"
                                            data-cost="{{$item->repair_cost}}"
                                        >
                                            <span class="mo-dispute-item col-xs-8 no-padding force-padding-left-40">{{$item->item}}</span>
                                            <span class="col-xs-3 text-align-right no-padding padding-right-80">${{number_format($item->repair_cost, 2)}}</span>
                                        </a>
                                        <span class="col-xs-1 no-padding text-align-right"><input type="checkbox" data-toggle="toggle" class="mo-dispute-toggle mo-dispute-inspect-id" value="{{$item->inspection_item_id}}" name="inspection_item_id[]"></span>
                                        
                                    </div>
                                    <!-- end items -->
                                @endforeach
                            @endif

                        @endif

                    @endforeach
                    
                @endif

            </div>
            <!-- end External section -->

            <!-- Internal section -->
            <hr class="mo-dispute-hr" />
            <div class="mo-row interior">
                <div class="dispute-items" id="section-id-100" data-section-id="100">
                    <label for="internal" class="text-capitalize col-xs-8 no-padding">internal</label>
                    
                    <label for="" class="text-capitalize col-xs-3 no-padding text-align-right">
                        <input type="hidden" name="internal_price" value="{{isset($inspectionItems->interior_section->total_cost) ? number_format($inspectionItems->interior_section->total_cost, 2) : 0}}">
                        <span class="padding-right-80">$<span id="internal-section-price">{{isset($inspectionItems->interior_section->total_cost) ? number_format($inspectionItems->interior_section->total_cost, 2) : 0}}</span></span>
                    </label>
                </div>

                <!-- append interior item here -->
                {{-- <hr class="mo-dispute-hr col-xs-12" />
                <div class="mo-row">

                    <span class="mo-dispute-item col-xs-8 no-padding force-padding-left-40">{{$item->item or ''}}:</span>
                    <span class="col-xs-3 text-align-right no-padding padding-right-80">$</span>
                    <span class="col-xs-1 no-padding text-align-right"><input type="checkbox" checked data-toggle="toggle" class="mo-dispute-toggle"></span>
                    
                </div>
                    
                <hr class="mo-dispute-hr" />
                <div class="mo-row">
                    <span class="mo-dispute-item col-xs-8 no-padding force-padding-left-40">{{$item->item or ''}}:</span>
                    <span class="col-xs-3 text-align-right no-padding padding-right-80">$</span>
                    <span class="col-xs-1 no-padding text-align-right"><input type="checkbox" checked data-toggle="toggle" class="mo-dispute-toggle"></span>
                </div> --}}
                <!-- end append -->

                @if(isset($inspectionItems->interior_section) && !empty($inspectionItems->interior_section))
                    @foreach($inspectionItems->interior_section as $in_item)
                        @if(isset($in_item->items))
                            <div class="mo-row font-warm-grey-color original">
                                @if(count($in_item->items) > 0)
                                    @foreach($in_item->items as $item)

                                        <!-- Internal item -->

                                        {{--<hr class="mo-dispute-hr" />
                                        <div class="mo-row">
                                            <span class="mo-dispute-item">{{$item->item or ''}}</span>
                                            <span class="float-right"><input type="checkbox" checked data-toggle="toggle" class="mo-dispute-toggle"></span>
                                            <span class="mo-dispute-price float-right">${{number_format($item->repair_cost, 2)}}</span>
                                        </div>--}}

                                        <!-- items -->
                                        <hr class="mo-dispute-hr original" />
                                        <div class="mo-row original">
                                            <a href="#mo-arriving-vehicle-additional-detail-popup" class="open-popup-link inspection-item-link original"
                                                data-category="INTERIOR_SECTION"
                                                data-session-id="{{$in_item->section_type}}"
                                                data-item-id="{{$item->inspection_item_id}}"
                                                data-item-name="{{$item->item}}"
                                                data-valuation-id=""
                                                data-note="{{$item->note}}"
                                                data-cost="{{$item->repair_cost}}"
                                            >
                                                <span class="mo-dispute-item col-xs-8 no-padding force-padding-left-40">{{$item->item}}</span>
                                                <span class="col-xs-3 text-align-right no-padding padding-right-80">${{number_format($item->repair_cost, 2)}}</span>
                                            </a>
                                            <span class="col-xs-1 no-padding text-align-right"><input type="checkbox" data-toggle="toggle" class="mo-dispute-toggle mo-dispute-inspect-id" value="{{$item->inspection_item_id}}" name="inspection_item_id[]"></span>
                                            
                                        </div>
                                        <!-- end items -->

                                        <!-- End Internal item -->

                                    @endforeach
                                @endif
                            </div>
                        @endif
                    @endforeach
                @endif

            </div>
            <!-- end Internal section -->

            <!-- Mechanical section -->
            <hr class="mo-dispute-hr" />
            <div class="mo-row mechanical">
                <div class="dispute-items" id="section-id-200" data-section-id="200">
                    <label for="mechanical" class="text-capitalize col-xs-8 no-padding">mechanical</label>
                    <label for="" class="text-capitalize col-xs-3 no-padding text-align-right padding-right-80">
                        <input type="hidden" name="mechanical_price" value="{{isset($inspectionItems->mechanical_section->total_cost) ? number_format($inspectionItems->mechanical_section->total_cost, 2) : 0}}">
                        <span>$<span id="mechanical-section-price">{{isset($inspectionItems->mechanical_section->total_cost) ? number_format($inspectionItems->mechanical_section->total_cost, 2) : 0}}</span></span>
                    </label>
                </div>

                <!-- Append mechanical item here -->
                {{--<hr class="mo-dispute-hr col-xs-12" />
                <div class="mo-row">

                    <span class="mo-dispute-item col-xs-8 no-padding force-padding-left-40">{{$item->item or ''}}:</span>
                    <span class="col-xs-3 text-align-right no-padding padding-right-80">$</span>
                    <span class="col-xs-1 no-padding text-align-right"><input type="checkbox" checked data-toggle="toggle" class="mo-dispute-toggle"></span>
                    
                </div>--}}
                <!-- end Append mechanical item -->

                @if(isset($inspectionItems->mechanical_section) && !empty($inspectionItems->mechanical_section))
                    @foreach($inspectionItems->mechanical_section as $me_item)
                        @if(isset($me_item->items))
                            <div class="mo-row font-warm-grey-color original">
                                @if(count($me_item->items) > 0)
                                    @foreach($me_item->items as $item)

                                        {{--<hr class="mo-dispute-hr" />
                                        <div class="mo-row">
                                            <span class="mo-dispute-item" >{{$item->item or ''}}</span>
                                            <span class="float-right"><input type="checkbox" checked data-toggle="toggle" class="mo-dispute-toggle"></span>
                                            <span class="mo-dispute-price float-right" >${{number_format($item->repair_cost, 2)}}</span>
                                        </div>--}}
                                        <!-- items -->
                                        <hr class="mo-dispute-hr original" />
                                        <div class="mo-row original">
                                            <a href="#mo-arriving-vehicle-additional-detail-popup" class="open-popup-link inspection-item-link original"
                                                data-category="MECHANICAL_SECTION"
                                                data-session-id="{{$me_item->section_type}}"
                                                data-item-id="{{$item->inspection_item_id}}"
                                                data-valuation-id=""
                                                data-item-name="{{$item->item}}"
                                                data-note="{{$item->note}}"
                                                data-cost="{{$item->repair_cost}}"
                                            >
                                                <span class="mo-dispute-item col-xs-8 no-padding force-padding-left-40">{{$item->item}}</span>
                                                <span class="col-xs-3 text-align-right no-padding padding-right-80">${{number_format($item->repair_cost, 2)}}</span>
                                            </a>
                                            <span class="col-xs-1 no-padding text-align-right"><input type="checkbox" data-toggle="toggle" class="mo-dispute-toggle mo-dispute-inspect-id" value="{{$item->inspection_item_id}}" name="inspection_item_id[]"></span>
                                            
                                        </div>
                                        <!-- end items -->

                                    @endforeach
                                @endif
                            </div>
                        @endif
                    @endforeach
                @endif

            </div>
            <!-- End Mechanical section -->

            <hr class="mo-dispute-hr" />
            <div class="mo-row">
                <span class="col-xs-8"></span>
                <span class="text-capitalize col-xs-3 no-padding text-align-right padding-right-80"><label>total: $<span id="dispute-total-price">{{isset($damageTotal) ? number_format($damageTotal, 2) : 0}}</span></label></span>
            </div>
            <hr class="mo-dispute-hr" />
            <div class="mo-row">
                <form id="mo-dispute-additional-details-comment-form" class="">
                    <span class="mo-dispute-info col-xs-12">
                        <textarea placeHolder="Comment here" class="mo-textarea form-control width-100-percent no-border" rows="5" id="arriving_vehicle_dispute_comment" name="arriving_vehicle_dispute_comment"></textarea>
                    </span>
                </form>
            </div>
            <hr class="mo-dispute-hr" />
            <div class="text-center padding-top-24" >
                <div class="alert alert-danger hidden">
                  <strong></strong> Please select at least 1 dispute item.
                </div>
                <input type="hidden" name="transaction_id" value="{{$transaction_id or ''}}" />
                <input type="hidden" name="valuation_id" value="{{$valuation_id or ''}}" />
                <input type="hidden" name="valuation_version" value="{{$valuation_version or ''}}" />
                <input type="hidden" name="azure_accout_name" value="{{Config::get('app.azure_storage_account_name')}}" />
                <input type="hidden" value="{{$user_id or ''}}" name="user_id">
                <input type="button" class="mo-btn text-uppercase" id="mo-dispute-submit" value="submit">
            </div>
        </div>

        <div class="mo-round-btn"><a href="#mo-arriving-vehicle-additional-detail-popup" class="open-popup-link new-dispute-inspection">NEW</a></div>
    </div>

    <!-- POPUP - Dispute Additional Detail -->
    <div id="mo-arriving-vehicle-additional-detail-popup" class="mo-white-popup mfp-hide">
        <form action="" method="post" class="mo-popup-form" id="mo-arriving-vehicle-additional-detail-popup-form" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <div class="mo-row padding-bottom-20">
                    <span class="text-capitalize mo-popup-heading dispute-popup-title">new issue</span>
                </div>
                <hr class="mo-form-hr">
                <div class="form-group col-xs-12 col-sm-12 col-md-8 padding-top-20">
                    <label for="arriving-vehicle-category" class="text-capitalize">category*</label>
                    <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}" ></span>
                    
                    <select class="form-control" name="arriving_vehicle_category" id="arriving-vehicle-category">
                        <option value="EXTERIOR_SECTION">Exterior</option>
                        <option value="INTERIOR_SECTION">Interior</option>
                        <option value="MECHANICAL_SECTION">Mechanical</option>
                    </select>

                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-8">
                    <label for="arriving-vehicle-section" class="text-capitalize">section*</label>
                    <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}" ></span>
                    
                    <select class="form-control" name="arriving_vehicle_section" id="arriving-vehicle-section">
                        
                    </select>

                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-8">
                    <label for="arriving-vehicle-item" class="text-capitalize">item*</label>
                    <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}" ></span>
                    
                    <select class="form-control" name="arriving_vehicle_item" id="arriving-vehicle-item">
                        
                    </select>

                </div>

                <!-- upload image -->
                <div class="form-group col-xs-12">
                    
                    <div class="col-xs-4 mo-inspection-upload-image-box no-padding" id="mo-image-box-1">
                        <label for="arriving-vehicle-dispute-inspect-item-photo" data-sequence="1" class="mo-inspection-input-file-label vertical-center" id="">
                            <img src="/images/perform_valuation/image_upload.png" id="1" alt="Upload" onerror="this.onerror=null; this.src='/images/perform_valuation/image_upload.png'" class="mo-inspection-upload-image-icon img-responsive">
                        </label>
                    </div>

                    <div class="col-xs-4 mo-inspection-upload-image-box no-padding" id="mo-image-box-2">
                        <label for="arriving-vehicle-dispute-inspect-item-photo" data-sequence="2" class="mo-inspection-input-file-label vertical-center" id="">
                            <img src="/images/perform_valuation/image_upload.png" id="2" alt="Upload" onerror="this.onerror=null; this.src='/images/perform_valuation/image_upload.png'" class="mo-inspection-upload-image-icon img-responsive">
                        </label>
                    </div>

                    <div class="col-xs-4 mo-inspection-upload-image-box no-padding" id="mo-image-box-3">
                        <label for="arriving-vehicle-dispute-inspect-item-photo" data-sequence="3" class="mo-inspection-input-file-label vertical-center" id="">
                            <img src="/images/perform_valuation/image_upload.png" id="3" alt="Upload" onerror="this.onerror=null; this.src='/images/perform_valuation/image_upload.png'" class="mo-inspection-upload-image-icon img-responsive">
                        </label>
                    </div>
                    <input type="hidden" value="" name="sequence" />
                    <input type="hidden" value="" name="dispute_item_id">
                    <input type="hidden" value="{{$user_id or ''}}" name="user_id">
                    <input type="file" name="arriving_vehicle_dispute_inspect_item_photo" id="arriving-vehicle-dispute-inspect-item-photo" class="hidden" />

                </div>

                <!-- end upload image -->

                <div class="form-group col-xs-12">
                    <label for="arriving-vehicle-item-1" class="text-capitalize">Note</label>
                    {{--<input type="text" class="form-control" id="arriving-vehicle-item-1" name="arriving_vehicle_item_1">--}}
                    <textarea class="mo-textarea form-control width-100-percent force-border-bottom" rows="5" id="arriving-vehicle-dispute-note" name="arriving_vehicle_dispute_note"></textarea>
                </div>
                
                <!-- cost -->
                <div class="form-group col-xs-12 col-sm-12 col-md-8">
                    <label for="arriving-vehicle-item-cost" class="text-capitalize">Cost*</label>
                    <input type="number" class="form-control" id="arriving-vehicle-item-cost" name="arriving_vehicle_item_cost">
                </div>
                <!-- end cost -->
                
            </div>

            <hr class="mo-form-hr"/>

            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" id="arriving_vehicle_cancel_btn" value="cancel" />
                    <input type="button" class="mo-btn text-uppercase" id="arriving_vehicle_save_btn" value="save" />
                </div>
            </div>

        </form>
    </div>
    <!-- POPUP - end Dispute Additional Detail -->

    <!-- AJAX - loading -->
    <div class="mo-loading-image hidden">Loading&#8230;</div>
    <!-- AJAX - end loading -->

@endsection