@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
{{--Start - moto Content Header Section--}}
    <input type="hidden" name="version" id="version" value="{{ $version }}"/>
    <input type="hidden" name="valuationId" id="valuationId" value="{{ $valuationId }}"/>
    <input type="hidden" name="vehicleId" id="vehicleId" value="{{ $vehicleId }}"/>
    <input type="hidden" name="transactionId" id="transactionId" value="{{ $transactionId }}"/>
    <input type="hidden" name="userId" id="userId" value="{{ Session::get('user_info')->id }}">
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                Arriving Vehicle > Inspect
            </div>
        </div>
    </div>
{{--End - moto Content Header Section--}}
    <div class="mo-row mo-form-wrapper">

        @if (isset($error))
        <div class="alert alert-danger">
            {{ $error }}
        </div>
        @endif

        <div class="col-xs-12 col-sm-3 padding-lr5" id="mo-vehicle-add-photo">
            <form class="vertical-center" action="" method="post" id="mo-vehicle-image-upload-form">
                <div id="mo-vehicle-image-wrapper" class="vertical-center">
                    <label for="mo-vehicle-image-upload" class="cursor-pointer">
                        <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$vehicleId or ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/img-add-photo@3x.png')}}'" class="img-responsive">
                    </label>
                </div>
                <input type="file" name="mo-vehicle-photo-upload" id="mo-vehicle-photo-upload" class="mo-exterior-input-file">
            </form>
        </div>
        <div class="col-xs-12 col-sm-9 padding-lr5" id="mo-vehicle-info">
            <div class="col-xs-12 col-sm-8 no-padding text-uppercase" id="mo-vehicle-title">{{ $valuationDetail->valuation_name }}</div>
            <div class="col-xs-12 no-padding padding-top-20">
                <form class="form-horizontal">
                    <div class="col-xs-12 padding-lr5 padding-top-5">
                        <span class="col-xs-12 col-sm-2 no-padding text-capitalize">
                            overall rating:
                        </span>
                        <label class="col-xs-12 col-sm-10 no-padding text-capitalize">{{ $valuationDetail->overall_rating_text }}</label>
                    </div>
                    <div class="col-xs-12 padding-lr5 padding-top-5">
                        <span class="col-xs-12 col-sm-2 no-padding text-capitalize">
                            odometer:
                        </span>
                        <label class="col-xs-12 col-sm-10 no-padding text-capitalize">{{ $valuationDetail->odometer }} km</label>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 no-padding" id="mo-blocks-wrapper">
            {{-- tab 1 --}}
            <div class="col-xs-12 col-sm-5 padding-lr5 tab" id="inspect-item-tab" tab="inspect-item">
                <div class="col-xs-12 mo-block mo-offer-blocks">
                    <span class="col-xs-12 no-padding mo-block-txt text-capitalize padding-top-8" data-label="Estimated Damage">estimated damage</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div>
            {{-- tab 2 --}}
            <div class="col-xs-12 col-sm-5 padding-lr5 tab" id="option-extra-tab" tab="option-extra">
                <div class="col-xs-12 mo-block mo-offer-blocks">
                    <span class="col-xs-12 no-padding mo-block-txt text-capitalize padding-top-8" data-label="Options & Extras">options & extras</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div>
        </div>

        <!-- START DISPUTE ESTIMATED DAMAGE -->
        <div class="mo-row margin-top-32 hidden mo-block-list padding-lr5" id="inspect-item"></div>

        <!-- START DISPUTE OPTION & EXTRAS -->
        <div class="mo-row margin-top-32 hidden mo-block-list padding-lr5" id="option-extra"></div>

        <div class="text-center padding-top-24" >
            <input type="button" class="mo-btn text-uppercase" id="accept_btn" value="accept">
            <a class="log-btn" href="/arriving-vehicle/additional-detail/{{ $transactionId }}">log dispute</a>
        </div>
    </div>

@endsection