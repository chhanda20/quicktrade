@extends('layouts.master')

@section('title', 'moto - Arriving List')

@section('content')
{{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                {{ $header or 'Arriving Vehicle' }}
            </div>
        </div>
    </div>
{{--End - moto Content Header Section--}}

    <div class="mo-row mo-form-wrapper" id="mo-arriving-list">
        {{ csrf_field() }}
        @include("main.arriving-vehicle.arriving-data-gird")
    </div>

@endsection