{{--MORE DETAIL--}}
@if(isset($inspectionItems))
    <div class="col-xs-12 col-sm-4 no-padding padding-top-15">
        <div class="mo-row padding-bottom-15 text-uppercase">
            <label>estimated reconditioning costs</label>
        </div>
        <div class="mo-row padding-left-20 text-capitalize">
            {{--EXTERNAL--}}
            <div class="mo-row padding-bottom-10">
                <label class="col-xs-8 no-padding">external:</label>
                <label class="col-xs-4 no-padding text-right"><span>${{isset($inspectionItems->exterior_section->total_cost)?number_format($inspectionItems->exterior_section->total_cost, 2):0}}</span></label>

                <div class="mo-row padding-left-20 font-warm-grey-color">
                    <span class="col-xs-8 no-padding text-capitalize">general reconditioning cost</span>
                    <span class="col-xs-4 no-padding text-right">${{$generalReconditionCost or 0}}</span>
                </div>

                @if(isset($inspectionItems->exterior_section) && !empty($inspectionItems->exterior_section))
                    @foreach($inspectionItems->exterior_section as $ex_item)
                        @if(isset($ex_item->items))
                            <label class="padding-left-10">{{$ex_item->section_name or ''}}:</label>
                            <div class="mo-row font-warm-grey-color padding-left-20">
                                @if(count($ex_item->items) > 0)
                                    @foreach($ex_item->items as $item)
                                        <span class="col-xs-8 no-padding">{{$item->item or ''}}:</span>
                                        <span class="col-xs-4 no-padding text-right">${{number_format($item->repair_cost, 2)}}</span>
                                    @endforeach
                                @endif
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>


            {{--INTERNAL--}}
            <div class="mo-row padding-bottom-10">
                <label>internal:</label>
                <label class="float-right"><span>${{isset($inspectionItems->interior_section->total_cost)?number_format($inspectionItems->interior_section->total_cost, 2):0}}</span></label>
                @if(isset($inspectionItems->interior_section) && !empty($inspectionItems->interior_section))
                    @foreach($inspectionItems->interior_section as $in_item)
                        @if(isset($in_item->items))
                            <div class="mo-row font-warm-grey-color padding-left-20">
                                @if(count($in_item->items) > 0)
                                    @foreach($in_item->items as $item)
                                        <span class="col-xs-8 no-padding">{{$item->item or ''}}:</span>
                                        <span class="col-xs-4 no-padding text-right">${{number_format($item->repair_cost, 2)}}</span>
                                    @endforeach
                                @endif
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>

            {{--MECHANICAL--}}
            <div class="mo-row padding-bottom-10">
                <label>mechanical:</label>
                <label class="float-right"><span>${{isset($inspectionItems->mechanical_section->total_cost)?number_format($inspectionItems->mechanical_section->total_cost, 2):0}}</span></label>
                @if(isset($inspectionItems->mechanical_section) && !empty($inspectionItems->mechanical_section))
                    @foreach($inspectionItems->mechanical_section as $me_item)
                        @if(isset($me_item->items))
                            <div class="mo-row font-warm-grey-color padding-left-20">
                                @if(count($me_item->items) > 0)
                                    @foreach($me_item->items as $item)
                                        <span class="col-xs-8 no-padding">{{$item->item or ''}}:</span>
                                        <span class="col-xs-4 no-padding text-right">${{number_format($item->repair_cost, 2)}}</span>
                                    @endforeach
                                @endif
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>

        </div>
    </div>
@endif