<div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
    options
</div>

@if (isset($optionalCollection))
    @foreach ($optionalCollection->chunk(2) as $chunk)
        <div class="mo-row padding-bottom-24">
            @foreach ($chunk as $key => $option)
                <div class="col-xs-12 col-sm-5">
                    <input disabled type="checkbox" class="mo-round-check" id="{{ $key }}" name="arriving_vehicle_airconditioning" {{ $option->selected ? 'checked' : '' }} >
                    <label for="{{ $key }}" class="text-capitalize check-icon"><span></span> {{ $option->name }}</label>
                </div>
            @endforeach
        </div>
    @endforeach
@endif


<div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
    extras
</div>

<div class="mo-row">
    <hr class="mo-dispute-hr" />
    @if (isset($extraOptions))
        @foreach ($extraOptions as $extraOption)
            <label for="external" class="text-capitalize">{{ $extraOption->description }}</label>
            <hr class="mo-dispute-hr" />
        @endforeach
    @endif
</div>

