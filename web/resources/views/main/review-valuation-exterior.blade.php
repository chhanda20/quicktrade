@section('review-exterior')

    {{----------------------------------------------------------- start EXTERIOR -----------------------------------------------------------}}

    {{--Start - moto Exterior Content Section--}}
    <div class="hidden mo-block-list" id="mo-exterior-block">
        <div class="mo-row mo-form-wrapper margin-top-32">
            <div class="col-xs-12 no-padding text-center">
                <div class="mo-butterfly-diagram">
                    <img src="{{ asset('images/svg/perform_valuation/diagram.svg') }}" alt="Diagram" onerror="this.onerror=null; this.src='{{ asset('images/perform_valuation/diagram.png') }}'" id="mo-vehicle-diagram" class="img-responsive" width="882">
                    <div class="grid">
                        <div class="grid-item grid-item--w1">
                            <div class="vertical-center grid-item grid-item--w0">
                                <a href="#mo-edit-inspection-item-popup" class="open-popup-link">
                                    <input type="button" class="mo-round-number {{isset($sections["6"])?'highlight':''}} mo-section-number" value="6" >
                                </a>
                            </div>
                        </div>
                        <div class="grid-item grid-item--w2">
                            <div class="vertical-center grid-item grid-item--w0 grid-item--h5">
                                <a href="#mo-edit-inspection-item-popup" class="open-popup-link">
                                    <input type="button" class="mo-round-number {{isset($sections["7"])?'highlight':''}} mo-section-number" value="7" >
                                </a>
                            </div>
                            <div class="vertical-center grid-item grid-item--w0 grid-item--h5">
                                <a href="#mo-edit-inspection-item-popup" class="open-popup-link">
                                    <input type="button" class="mo-round-number {{isset($sections["5"])?'highlight':''}} mo-section-number" value="5" >
                                </a>
                            </div>
                        </div>
                        <div class="grid-item grid-item--w3-5">
                            <div class="grid-item grid-item--w0 grid-item--h4">
                                <div class="vertical-center grid-item grid-item--w5">
                                    <a href="#mo-edit-inspection-item-popup" class="open-popup-link">
                                        <input type="button" class="mo-round-number {{isset($sections["8"])?'highlight':''}} mo-section-number" value="8" >
                                    </a>
                                </div>
                                <div class="vertical-center grid-item grid-item--w5">
                                    <a href="#mo-edit-inspection-item-popup" class="open-popup-link">
                                        <input type="button" class="mo-round-number {{isset($sections["9"])?'highlight':''}} mo-section-number" value="9" >
                                    </a>
                                </div>
                            </div>
                            <div class="vertical-center grid-item grid-item--w0 grid-item--h2">
                                <a href="#mo-edit-inspection-item-popup" class="open-popup-link">
                                    <input type="button" class="mo-round-number {{isset($sections["10"])?'highlight':''}} mo-section-number" value="10" >
                                </a>
                            </div>
                            <div class="grid-item grid-item--w0 grid-item--h4">
                                <div class="vertical-center grid-item grid-item--w5">
                                    <a href="#mo-edit-inspection-item-popup" class="open-popup-link">
                                        <input type="button" class="mo-round-number {{isset($sections["4"])?'highlight':''}} mo-section-number" value="4" >
                                    </a>
                                </div>
                                <div class="vertical-center grid-item grid-item--w5">
                                    <a href="#mo-edit-inspection-item-popup" class="open-popup-link">
                                        <input type="button" class="mo-round-number {{isset($sections["3"])?'highlight':''}} mo-section-number" value="3" >
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item grid-item--w2">
                            <div class="vertical-center grid-item grid-item--w0 grid-item--h5">
                                <a href="#mo-edit-inspection-item-popup" class="open-popup-link">
                                    <input type="button" class="mo-round-number {{isset($sections["11"])?'highlight':''}} mo-section-number" value="11" >
                                </a>
                            </div>
                            <div class="vertical-center grid-item grid-item--w0 grid-item--h5">
                                <a href="#mo-edit-inspection-item-popup" class="open-popup-link">
                                    <input type="button" class="mo-round-number {{isset($sections["2"])?'highlight':''}} mo-section-number" value="2" >
                                </a>
                            </div>
                        </div>
                        <div class="grid-item grid-item--w1-5">
                            <div class="vertical-center grid-item grid-item--w0">
                                <a href="#mo-edit-inspection-item-popup" class="open-popup-link">
                                    <input type="button" class="mo-round-number {{isset($sections["1"])?'highlight':''}} mo-section-number" value="1" >
                                </a>
                            </div>
                            <div class="mo-round-btn2 {{$hideEditField or ''}}"><a href="#mo-edit-inspection-item-popup" class="open-popup-link padding-top-30 mo-add-new-item" id="">NEW</a></div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
        <div class="col-xs-12 no-padding" id="mo-prev-next-button">
            <button type="button" id="mo-prev-exterior" class="mo-prev text-uppercase float-left">
                <span class="glyphicon glyphicon-menu-left text-uppercase"></span><span class="mo-span-txt">previous</span>
            </button>
            <button type="button" id="mo-next-exterior" class="mo-next text-uppercase float-right">
                <span class="mo-span-txt">next</span><span class="glyphicon glyphicon-menu-right text-uppercase"></span>
            </button>
        </div>
        <div class="col-xs-12 no-padding" id="mo-estimated-cost">
            <ul class="mo-main-content no-padding">
                <li data-toggle="collapse" data-target="#mo-exterior-sub-content" class="collapsed active">

                    <a href="javascript:void(0);">
                        <div id="mo-heading" class="vertical-center col-xs-12 no-padding">
                            <div class="mo-estimated-cost-wrapper col-xs-12 no-padding">
                                <div class="col-xs-12 col-sm-8 no-padding">
                                    Total Estimated Cost
                                </div>
                                <div class="col-xs-12 col-sm-4 no-padding text-right">
                                    <span class="col-xs-5 col-sm-6 no-padding">$<span id="mo-exterior-total-estimate-cost">{{number_format($total_cost + $general_cost->generalReconditionCost, 2)}}</span></span>
                                    <span class="col-xs-2 col-sm-4 padding-lr5 mo-expand-hide-label">Expand</span>
                                    <span class="col-xs-5 col-sm-2 no-padding mo-icon-swipe glyphicon glyphicon-menu-down"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                    <script type="text/javascript">
                        function maxLengthCheck(object) {
                            if (object.value.length > object.maxLength)
                              object.value = object.value.slice(0, object.maxLength)
                        }

                        function isNumeric (evt) {
                            var theEvent = evt || window.event;
                            var key = theEvent.keyCode || theEvent.which;
                            key = String.fromCharCode (key);
                            var regex = /^\d{0,2}(\.\d{0,2}){0,1}$/;
                            if ( !regex.test(key) ) {
                              theEvent.returnValue = false;
                              if(theEvent.preventDefault) theEvent.preventDefault();
                            }
                        }
                    </script>
                    {{--<ul class="mo-general-cost">
                        <li>
                            <div class="col-xs-12 no-padding border-bottom padding-tb15 font-size16">
                                
                                <div class="col-xs-12 col-sm-8 no-padding">
                                    General Reconditioning Costs
                                </div>
                                <div class="col-xs-12 col-sm-4 text-right no-padding {{in_array("salesperson", $roles)?'disabled-div':''}}">
                                    <span class="col-xs-6 no-padding font-avenir-demi">
                                        $<span class="font-size16 mo-cost-number cursor-pointer">{{number_format($general_cost->generalReconditionCost, 2)}}</span>
                                        <span class='mo-cost-number-input hide'><input type='number' min="0" max="9999999999" name="estimate_cost" maxlength = "13" oninput="maxLengthCheck(this)" value="" data-recost="recost" /></span>
                                    </span>
                                    <span class="col-xs-6 no-padding"></span>
                                </div>
                            </div>
                        </li>
                    </ul> --}}

                    <ul class="mo-sub-content collapse" id="mo-exterior-sub-content">
                        <input type="hidden" value="{{isset($valuation_id) ? $valuation_id : null}}" id="valuation_id">
                        @if (isset($inspection_item_list))
                            <input type="hidden" value="{{json_encode($inspection_item_list)}}" id="mo-item-data">
                            <li>
                                <div class="col-xs-12 no-padding border-bottom padding-tb15 font-size16">
                                    
                                    <div class="col-xs-12 col-sm-8 no-padding">
                                        General Reconditioning Costs
                                    </div>
                                    <div class="col-xs-12 col-sm-4 text-right no-padding {{!in_array("valuation_perform",$permissions)?'disabled-div':''}}">
                                        <span class="col-xs-6 no-padding font-avenir-demi">
                                            <span class="{{$disableEditField or ''}}">$</span><span class="font-size16 mo-cost-number cursor-pointer {{$disableEditField or ''}}">{{number_format($general_cost->generalReconditionCost, 2)}}</span>
                                            <span class='mo-cost-number-input hide'><input type='number' min="0" max="9999999999" name="estimate_cost" maxlength = "13" oninput="maxLengthCheck(this)" value="" data-recost="recost" /></span>
                                        </span>
                                        <span class="col-xs-6 no-padding"></span>
                                    </div>
                                </div>
                            </li>
                            
                            @foreach ($inspection_item_list as $inspection_item)
                                <li>
                                    <div class="mo-form-header col-xs-12 padding-tb20 border-bottom no-padding mo-cost-estimate-row">
                                        <div class="col-xs-12 col-sm-8 no-padding">
                                            {{$inspection_item->section_name}}
                                        </div>
                                        <div class="col-xs-12 col-sm-4 text-right no-padding">
                                            <span class="col-xs-6 no-padding" id="section_cost_{{$inspection_item->section_type}}">
                                                $<span>{{number_format($all_section_cost["section".$inspection_item->section_type], 2)}}</span>
                                            </span>
                                            <span class="col-xs-6 no-padding"></span>
                                        </div>
                                    </div>
                                </li>
                                @if (isset($inspection_item))
                                    <?php $inspection_item_no = 1; ?>
                                    @foreach ($inspection_item->items as $item)
                                        <li class="{{$inspection_item->section_type}}">

                                            <div class="col-xs-12 padding-tb10 border-bottom no-padding mo-cost-estimate-row" id="{{$item->inspection_item_id}}">
                                                
                                                <div class="col-xs-12 col-sm-8 no-padding name">
                                                    {{$inspection_item_no++}}. {{$item->item}}
                                                    <div class="padding-top-5 font-size14 font-grey-color font-style-italic note">{{$item->note}}</div>
                                                </div>

                                                <div class="col-xs-12 col-sm-4 no-padding text-right">
                                                    <label class="col-xs-6 no-padding cursor-pointer {{!in_array("valuation_perform",$permissions)?'disabled-div':''}}" id="item_cost_{{$inspection_item->section_type}}_{{$item->inspection_item_id}}">
                                                        <span class="{{$disableEditField or ''}}">$</span><span class="mo-cost-number {{$disableEditField or ''}}" >{{ number_format($item->repair_cost , 2) }}
                                                        </span>
                                                        <span class='mo-cost-number-input hide'><input type='number' name="estimate-cost" min="0" max="9999999999" maxlength = "13" oninput="maxLengthCheck(this)" value="" /></span>
                                                    </label>
                                                        <a href="#mo-edit-inspection-item-popup" class="col-xs-4 padding-lr5 open-popup-link">
                                                            <span class="mo-view-edit-label ">View/Edit</span>
                                                        </a>
                                                    <span class="col-xs-2 no-padding {{$hideEditField or ''}}">
                                                        <a href="#mo-confirm-delete-exterior-popup" class="open-popup-link float-left padding-left-5">
                                                            <img src="{{ asset('images/svg/main/icon-remove.svg') }}" alt="Remove Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-remove.png') }}'" class="mo-delete-item">
                                                        </a>
                                                    </span>
                                                </div>

                                            </div>

                                        </li>
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    {{--End - moto Exterior Content Section--}}

    {{----------------------------------------------------------- end EXTERIOR -----------------------------------------------------------}}

@endsection