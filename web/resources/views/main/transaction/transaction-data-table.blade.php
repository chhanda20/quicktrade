{{--Start - moto List Transactions Section--}}
<div class="mo-row percent-h-100" id="mo-list-wrapper">
    <div class="mo-row percent-h-100">
        <table id="mo-table-transactions-list-{{ $tab['id'] }}" class="table" cellspacing="0">
            <thead>
            <tr>
                <th></th>
                <th class="text-capitalize">Bought/ Sold</th>
                <th>Trans. Ref #</th>
                <th>Vehicle Details</th>
                <th>Rego</th>
                <th>Location</th>
                <th>Status</th>
                <th></th>{{--status id--}}
                <th></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <div class="col-xs-12 text-center mo-pagination-wrapper">
            <ul class="pagination">
                <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
            </ul>
        </div>
    </div>
</div>
{{--End - moto List Transactions Section--}}