@extends('layouts.master')

@section('title', 'moto - Transaction')

@section('content')
    <div class="mo-row padding-bottom-24">
        <div class="col-xs-12 col-sm-12 no-padding" id="mo-content-header">
            {{ $header or 'Transaction' }} > <a class="text-capitalize underline-text" href="{!! url('transaction/view-transactions/current') !!}"><span class="text-capitalize">{{ $breadcrumb or 'Current' }}</span></a> > <span class="yellow-text">Transaction {{ $transactionDetails->rid or '' }}</span>
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 no-padding">
            {{--<form action="" method="post" id="mo-transaction-form">--}}
                {{--HIDDEN FIELDS--}}
                <input type="hidden" id="transaction-id" value="{{$transactionDetails->id or ''}}">
                <input type="hidden" id="transaction-status" value="{{$transactionDetails->status or ''}}">
                <input type="hidden" id="type" value="{{$type or ''}}">
                {{--HIDDEN FIELDS--}}

                <div class="mo-row padding-bottom-24">
                    <div class="col-xs-12 col-sm-12 no-padding padding-bottom-24 mo-header text-capitalize">
                        transaction information
                    </div>
                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="mo-row padding-bottom-10">
                                <label class="col-xs-12 col-sm-4 no-padding">Transaction Ref: </label>
                                <span class="col-xs-12 col-sm-8 no-padding">{{ $transactionDetails->rid or '' }}</span>
                            </div>
                            <div class="mo-row padding-bottom-10">
                                <label class="col-xs-12 col-sm-4 no-padding">Vehicle Description: </label>
                                <span class="col-xs-12 col-sm-8 no-padding">{{ $transactionDetails->vehicle_name or '' }}</span>
                            </div>
                            <div class="mo-row">
                                <label class="col-xs-12 col-sm-4 no-padding">Rego: </label>
                                <span class="">{{ $transactionDetails->rego or '' }}</span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="mo-row padding-bottom-10">
                                <label class="col-xs-12 col-sm-4 no-padding">Status: </label>
                                <span class="col-xs-12 col-sm-8 no-padding yellow-text">{{ $transactionDetails->status_value or '' }}</span>
                            </div>
                            <div class="mo-row">
                                <label class="col-xs-12 col-sm-4 no-padding">Total Price: </label>
                                <span class="col-xs-12 col-sm-8 no-padding yellow-text">
                                    $<span id="transaction-total-price">{{$transactionDetails->total_price or 0}}</span>
                                </span>
                            </div>
                            <div class="mo-row">
                                @if(isset($transactionDetails->action_button) && !empty($transactionDetails->action_button))
                                    <div>
                                        <input type="button" id="transaction-btn" class="width-180 margin-top-10 mo-btn-yellow text-uppercase font-family-bold" value="{{ $transactionDetails->action_button }}">
                                    </div>
                                @endif
                            </div>
                            <div class="mo-row margin-top-24 float-right alert alert-danger hidden" role="alert">
                                <span class="mo-error-message text-capitalize"></span>
                                <a class="mo-close-message close">&times;</a>
                            </div>
                        </div>
                    </div>
                </div> <!--  Transaction Information -->
                <hr class="mo-dispute-hr">


                <div class="mo-row padding-bottom-24">
                    <div class="col-xs-12 col-sm-12 no-padding padding-bottom-24 mo-header text-capitalize">
                        transaction summary
                    </div>
                    <div class="mo-row ">
                        <div class="col-xs-12 col-sm-12">
                            <div class="mo-row">
                                <div class="mo-row padding-bottom-10">
                                    <label>Vehicle Price</label><span class="float-right">${{$transactionDetails->vehicle_price_format or 0}}</span>
                                </div>
                                @if($seller_buyer == "in")
                                    <div class="mo-row padding-bottom-10">
                                        <label>Transport</label><span class="float-right">${{$transactionDetails->buy_transport_format or 0}}</span>
                                    </div>
                                @endif
                                <div class="mo-row padding-bottom-10">
                                    <label>moto Fee</label><span class="float-right">${{$transactionDetails->mo_fee_format or 0}}</span>
                                </div>
                                {{--HAS DISPUTE--}}
                                @if(isset($transactionDetails->dispute_id) && !empty($transactionDetails->dispute_id))
                                    <div class="mo-row">
                                        <label>Dispute Adjustment</label><span class="float-right">- ${{$transactionDetails->dispute_adjust_format or 0}}</span>
                                    </div>
                                @endif

                                <div class="col-xs-12 col-sm-2 no-padding float-right">
                                    <hr class="mo-dispute-hr border-top">
                                </div>

                                <div class="mo-row text-right">
                                    <div>
                                        <label class="col-xs-12 col-sm-11 no-padding">Total:</label>
                                        <span class="col-xs-12 col-sm-1 no-padding">${{$transactionDetails->total_after_resolve_dispute_format or 0}}</span>
                                    </div>
                                    <div>
                                        <label class="col-xs-12 col-sm-11 no-padding">Paid:</label>
                                        <span class="col-xs-12 col-sm-1 no-padding">
                                            $<span id="transaction-paid">{{$transactionDetails->buy_payment_made_format or 0}}</span>
                                        </span>
                                    </div>
                                    {{--HAS DISPUTE--}}
                                    @if(isset($transactionDetails->dispute_id) && !empty($transactionDetails->dispute_id))
                                        <div>
                                            <label class="col-xs-12 col-sm-11 no-padding">Owing:</label>
                                            <span class="col-xs-12 col-sm-1 no-padding">- ${{$transactionDetails->dispute_adjust_format or 0}}</span>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- Transaction Summary -->
                <hr class="mo-dispute-hr">


                <div class="mo-row padding-bottom-24">
                    <div class="col-xs-12 col-sm-12 no-padding padding-bottom-24 mo-header text-capitalize">
                        transaction documents
                    </div>
                    <form id="mo-upload-download-documents-form" action="" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}


                        {{--<input type="file" accept='image/*' name="mo_vehicle_photo_upload" id="mo-vehicle-image-upload" class="mo-exterior-input-file" />--}}

                        <input type="hidden" id="file-extension" name="file_extension">
                        <input type="hidden" id="document-code" name="document_code">
                        <div class="col-xs-12 col-sm-12">
                            {{--VEHICLE TRANSFER DOCUMENT--}}
                            <div class="mo-row padding-bottom-10">
                                <?php $span_text = "Choose a file..."; ?>
                                @if(isset($documents))
                                    @foreach($documents as $document)
                                        @if(strpos($document, $transactionDetails->id."_vehicle_transfer_form") !== false)
                                            <?php $span_text = $document; ?>
                                            @break
                                        @endif
                                    @endforeach
                                @endif
                                <label  class="text-capitalize"><span>Vehicle Transfer Documentation</span></label>
                                {{--<input type="button" class="mo-btn mo-upload-download-button float-right" value="{{$seller_buyer == "in"?"Upload":"Download"}}" data-document-code="transfer">--}}
                                <input type="button" class="mo-btn mo-upload-download-button float-right" value="{{$seller_buyer == "out"?"Upload":"Download"}}" data-document-code="transfer">
                                @if($transactionDetails->status == 26)
                                    <div class="float-right padding-right-5">
                                        <input type="file" name="vehicle_transfer_file" id="vehicle-transfer-file" class="inputfile" />
                                        <label for="vehicle-transfer-file"><span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;
                                            <span title="Click to upload new file" data-toggle="tooltip" data-placement="top" class="choose-a-file">{{$span_text}}</span>
                                        </label>
                                    </div>
                                @endif

                            </div>

                            {{--VEHICLE TRANSFER DOCUMENT (ADDTIONAL)--}}
                            <div class="mo-row padding-bottom-10">
                                <?php $span_text = "Choose a file..."; ?>
                                @if(isset($documents))
                                    @foreach($documents as $document)
                                        @if(strpos($document, $transactionDetails->id."_vehicle_transfer_additional") !== false)
                                            <?php $span_text = $document; ?>
                                            @break
                                        @endif
                                    @endforeach
                                @endif
                                <label  class="text-capitalize"><span>Vehicle Transfer Documentation (Additional)</span></label>
                                {{--<input type="button" class="mo-btn mo-upload-download-button float-right" value="{{$seller_buyer == "in"?"Upload":"Download"}}" data-document-code="transfer_additional">--}}
                                    <input type="button" class="mo-btn mo-upload-download-button float-right" value="{{$seller_buyer == "out"?"Upload":"Download"}}" data-document-code="transfer_additional">
                                @if($transactionDetails->status == 26)
                                    <div class="float-right padding-right-5">
                                        <input type="file" name="vehicle_transfer_file_additional" id="vehicle-transfer-file-additional" class="inputfile" />
                                        <label for="vehicle-transfer-file-additional"><span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;
                                            <span title="Click to upload new file" data-toggle="tooltip" data-placement="top" class="choose-a-file">{{$span_text}}</span>
                                        </label>
                                    </div>
                                @endif
                            </div>

                            {{--INVOICE MINUS DISPUTE COST--}}
                            <div class="mo-row padding-bottom-10">
                                <?php $span_text = "Choose a file..."; ?>
                                @if(isset($documents))
                                    @foreach($documents as $document)
                                        @if(strpos($document, $transactionDetails->id."_invoice_vehicle_minus_dispute") !== false)
                                            <?php $span_text = $document; ?>
                                            @break
                                        @endif
                                    @endforeach
                                @endif
                                <label  class="text-capitalize"><span></span>Invoice for Vehicle minus any dispute costs</label>
                                <input type="button" class="mo-btn mo-upload-download-button float-right" value="{{$seller_buyer == "in"?"Download":"Upload"}}" data-document-code="dispute">
                                @if($transactionDetails->status == 26)
                                    <div class="float-right padding-right-5">
                                        <input type="file" name="invoice_dispute_cost_file" id="invoice-dispute-cost-file" class="inputfile" />
                                        <label for="invoice-dispute-cost-file"><span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;
                                            <span title="Click to upload new file" data-toggle="tooltip" data-placement="top" class="choose-a-file">{{$span_text}}</span>
                                        </label>
                                    </div>
                                @endif
                            </div>

                            {{--PPSR/FINANCIAL LETTER--}}
                            <div class="mo-row padding-bottom-10">
                                <?php $span_text = "Choose a file..."; ?>
                                @if(isset($documents))
                                    @foreach($documents as $document)

                                        @if(strpos($document, $transactionDetails->id."_ppsr_financial_payout") !== false)
                                            <?php $span_text = $document; ?>
                                            @break
                                        @endif
                                    @endforeach
                                @endif
                                <label  class="text-capitalize"><span></span>PPSR/Financial Payout Letter</label>
                                {{--<input type="button" class="mo-btn mo-upload-download-button float-right" value="{{$seller_buyer == "in"?"Upload":"Download"}}" data-document-code="ppsr_financial">--}}
                                <input type="button" class="mo-btn mo-upload-download-button float-right" value="{{$seller_buyer == "out"?"Upload":"Download"}}" data-document-code="ppsr_financial">
                                @if($transactionDetails->status == 26)
                                    <div class="float-right padding-right-5">
                                        <input type="file" name="ppsr_finance_payout_file" id="ppsr-finance-payout-file" class="inputfile" />
                                        <label for="ppsr-finance-payout-file"><span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;
                                            <span title="Click to upload new file" data-toggle="tooltip" data-placement="top" class="choose-a-file">{{$span_text}}</span>
                                        </label>
                                    </div>
                                @endif
                            </div>

                            <div class="mo-row padding-bottom-10" >
                                <label  class="text-capitalize"><span></span>moto Fees Invoice</label>
                                @if($seller_buyer == "in")
                                    <input type="button" class="mo-btn mo-upload-download-button float-right {{!isset($invoice_buyer_document)?"disabled-div":''}}" value="Download">
                                @else
                                    <input type="button" class="mo-btn mo-upload-download-button float-right {{!isset($invoice_seller_document)?"disabled-div":''}}" value="Download">
                                @endif
                            </div>
                            @if($seller_buyer == "in")
                                <div class="mo-row">
                                    <label class="text-capitalize"><span></span>Dispute Invoice/Credit Note</label>
                                    <input type="button" class="mo-btn mo-upload-download-button float-right {{!isset($credit_note_document)?"disabled-div":''}}" value="Download">
                                </div>
                            @endif

                        </div>
                    </form>
                </div> <!-- Transaction Documents -->
                <hr class="mo-dispute-hr">
                <div class="mo-row text-center padding-top-24">
                    @if($type == "current")
                        <a href="/transaction/view-transactions/current"><input type="button" class="mo-btn" id="mo-save-transaction" value="close"></a>
                    @elseif($type == "complete")
                        <a href="/transaction/view-transactions/complete"><input type="button" class="mo-btn" id="mo-save-transaction" value="close"></a>
                    @endif
                    {{--<input type="button" class="mo-btn" id="mo-save-transaction" value="close">--}}
                </div>
            </form>
        </div>

        <!-- AJAX - loading -->
        <div class="mo-loading-image hidden">Loading&#8230;</div>
        <!-- AJAX - end loading -->

    </div>
@endsection