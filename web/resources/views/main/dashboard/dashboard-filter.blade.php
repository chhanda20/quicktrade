@section("dashboard-filter")
    <div class="col-xs-12 col-sm-6 no-padding font-size14 font-black-color font-weight-bold">
        <div data-toggle="dropdown" class="cursor-pointer padding-top-4" id="mo-dashboard-location">
            <label class="font-grey-color-important">Location </label>
            {{--<span class="glyphicon glyphicon-menu-down font-size11"></span>--}}
            {{--<button class="btn btn-default" type="button" id="btn-select-location">--}}
                <span class="padding-left-10"><img src="{{asset("images/svg/main/icon-swipe-right-copy-4.svg")}}" ></span>
                {{--<span class="mo-label float-left font-size11"></span>--}}
                {{--<span class="caret float-right"></span>--}}
            {{--</button>--}}
        </div>
        {{-- dropdown menu --}}
        <div class="dropdown-menu mo-dashboard-filter-item padding-top-10 padding-bottom-10 font-size14 font-grey-color" id="mo-dashboard-filter-location">
            <div class="no-padding padding-left-10 col-xs-12 border-bottom">
                <div class="no-padding col-xs-3">
                    <input type="checkbox" class="mo-square-check" id="filter-all" name="filter-all">
                    <label for="filter-all" class="text-capitalize"><span class=""></span></label>
                </div>
                <div class="no-padding col-xs-9 padding-top-3">
                    All
                </div>
            </div>
            @if(isset($list_location))
                @foreach($list_location as $location)
                    <div class="no-padding padding-left-10 border-bottom col-xs-12 padding-top-10">
                        <div class="no-padding col-xs-3">
                            <input type="checkbox" class="mo-square-check" id="location-{{$location->name}}" name="location_items[]" value="{{$location->id}}">
                            <label for="location-{{$location->name}}" class="text-capitalize"><span class=""></span></label>
                        </div>
                        <div class="no-padding col-xs-9 padding-top-3">
                            {{$location->name}}
                        </div>
                    </div>
                @endforeach
            @endif
            <div class="no-padding padding-left-40 col-xs-12 padding-top-10">
                <input type="button" class="mo-btn" id="mo-btn-save-location" value="save">
            </div>

        </div>
        {{-- end dropdown menu --}}
    </div>


    <div class="col-xs-12 col-sm-6 no-padding font-size14 font-black-color font-weight-bold">
        <div data-toggle="dropdown" class="cursor-pointer padding-top-4" id="mo-dashboard-show-time">
            <label class="font-grey-color-important">Show </label>
            {{--<button class="btn btn-default dropdown-toggle" id="btn-select-show-time">--}}
                <span class="padding-left-10"><img src="{{asset("images/svg/main/icon-swipe-right-copy-4.svg")}}"></span>
                {{--<span class="mo-label float-left font-size11"></span>--}}
                {{--<span class="caret float-right"></span>--}}
            {{--</button>--}}
        </div>
         {{--dropdown menu--}}
        <div class="dropdown-menu mo-dashboard-filter-item padding-top-10 padding-bottom-10 font-size14 font-grey-color">

            <div class="no-padding padding-left-10 col-xs-12 border-bottom padding-top-10">
                <div class="no-padding col-xs-3">
                    <input type="radio" class="mo-round-check mo-filter-date-range" id="filter-today" name="filter-date-range" value="today">
                    <label for="filter-today" class="text-capitalize"><span class=""></span></label>
                </div>
                <div class="no-padding col-xs-9 padding-top-3">
                    Today
                </div>
                <input type="hidden" id="mo-today" value="<?php echo date('Y-m-d')?>">
            </div>

            <div class="no-padding padding-left-10 col-xs-12 border-bottom padding-top-10">
                <div class="no-padding col-xs-3">
                    <input type="radio" class="mo-round-check mo-filter-date-range" id="filter-last-7-days" name="filter-date-range" value="last_7_days">
                    <label for="filter-last-7-days" class="text-capitalize"><span class=""></span></label>
                </div>
                <div class="no-padding col-xs-9 padding-top-3">
                    Last 7 Days
                </div>
                <input type="hidden" id="mo-last-7-days-from" value="<?php echo date('Y-m-d', strtotime('-7 days'));?>">
                <input type="hidden" id="mo-last-7-days-to" value="<?php echo date('Y-m-d');?>">
            </div>

            <div class="no-padding padding-left-10 border-bottom col-xs-12 padding-top-10">
                <div class="no-padding col-xs-3">
                    <input type="radio" class="mo-round-check mo-filter-date-range" id="filter-last-month" name="filter-date-range" value="last_month">
                    <label for="filter-last-month" class="text-capitalize"><span class=""></span></label>
                </div>
                <div class="no-padding col-xs-9 padding-top-3">
                    Last Month
                </div>
                <input type="hidden" id="mo-last-month-from" value="<?php echo date("Y-m-d", strtotime("first day of previous month"));?>">
                <input type="hidden" id="mo-last-month-to" value="<?php echo date("Y-m-d", strtotime("last day of previous month"));?>">
            </div>

            <div class="no-padding padding-left-10 border-bottom col-xs-12 padding-top-10">
                <div class="no-padding col-xs-3">
                    <input type="radio" class="mo-round-check mo-filter-date-range" id="filter-this-month" name="filter-date-range" value="this_month">
                    <label for="filter-this-month" class="text-capitalize"><span class=""></span></label>
                </div>
                <div class="no-padding col-xs-9 padding-top-3">
                    This Month
                </div>
                <input type="hidden" id="mo-this-month-from" value="<?php echo date('Y-m-d', strtotime('first day of this month'));?>">
                <input type="hidden" id="mo-this-month-to" value="<?php echo date('Y-m-d', strtotime('last day of this month'));?>">
            </div>

            <div class="padding-top-10 padding-left-10">
                <div class="no-padding dropdown-menu-custom cursor-pointer col-xs-12 padding-top-10">
                    <div class="">
                        <div class="no-padding col-xs-3">
                            <input type="radio" class="mo-round-check mo-filter-date-range" id="filter-custom-range" name="filter-date-range" value="custome_date_range">
                            <label for="filter-custom-range" class="text-capitalize"><span class=""></span></label>
                        </div>
                        <div class="no-padding col-xs-9 padding-top-3">
                            Custom Range
                        </div>
                    </div>
                </div>

                <div class="width-200 dropdown-submenu-custom no-padding padding-left-20 padding-top-20 padding-bottom-20 hidden">
                     {{--date from to here--}}
                    <div class="col-xs-12">
                        <div>From</div>
                        <div class="input-group date" id="mo-dashboard-date-range-from">
                            <input type="text" class="form-control" id="mo-date-range-from" name="mo-date-range-from" placeholder="YYYY-MM-DD">
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        </div>
                    </div>
                    <div class="col-xs-12 padding-top-20">
                        <div>To</div>
                        <div class="input-group date" id="mo-dashboard-date-range-to">
                            <input type="text" class="form-control" id="mo-date-range-to" name="mo-date-range-to" placeholder="YYYY-MM-DD">
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="no-padding padding-left-40 col-xs-12 padding-top-10">
                <input type="button" class="mo-btn" id="mo-btn-clear-show-time" value="clear">
            </div>
        </div>
         {{--end dropdown menu--}}
    </div>

@endsection
