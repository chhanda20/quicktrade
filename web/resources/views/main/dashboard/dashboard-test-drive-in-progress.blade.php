
@if(isset($test_drive_list))
    <?php $count = 0; ?>
    @if(!empty($test_drive_list))
        @foreach($test_drive_list as $test_drive)
            @if($test_drive->status == 1)
                <hr class="mo-dispute-hr">
                <div class="mo-row ">
                    <div class="col-xs-12 col-sm-3 no-padding"><input type="button" class="mo-btn mo-test-drive-return" value="Return" data-inprogress-id="{{$test_drive->id}}" data-make-name="{{$test_drive->make}}" data-model-name="{{$test_drive->model}}" data-year="{{$test_drive->build_date}}"></div>
                    {{--<div class="col-xs-12 col-sm-9"><span class="padding-lr5">{{$test_drive->build_date." ".$test_drive->make." ".$test_drive->model}}</span></div>--}}
                    <div class="col-xs-12 col-sm-9"><span class="padding-lr5">{{date('Y', strtotime($test_drive->build_date))." ".$test_drive->make." ".$test_drive->model}}</span></div>
                </div>
            @endif
            <?php $count++; ?>
        @endforeach
        <?php
            if($count >= 5){
                echo "<div class='col-xs-12 col-sm-12 text-right mo-show-more'><a href='test-drives/inprogress'><strong>Show More...</strong></a></div>";
            }
        ?>
    @else
        <hr class="mo-dispute-hr">
        <div class="mo-row ">
            <div class="col-xs-12 col-sm-12"><span>There are no data to display at this time</span></div>
        </div>
    @endif
@else
    <hr class="mo-dispute-hr">
    <div class="mo-row ">
        <div class="col-xs-12 col-sm-12"><span>There are no data to display at this time</span></div>
    </div>
@endif
