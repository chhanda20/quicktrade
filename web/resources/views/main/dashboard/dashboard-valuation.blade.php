
<!-- VALUATION REQUEST -->
<div class="mo-form-header mo-action-item">Request</div>
@if(isset($valuation_request_list))
    <?php $count = 0; ?>
    @if(!empty($valuation_request_list))
        @foreach($valuation_request_list as $valuation_request)
            @if(isset($permissions) && in_array("valuation_perform", $permissions))
                <hr class="mo-dispute-hr">
                <div class="mo-row ">
                    <div class="col-xs-12 col-sm-3 no-padding">
                        <input type="button" class="mo-btn mo-accept-valuate-btn" value="Accept" data-valuation-id="{{$valuation_request->id}}" data-vin="{{$valuation_request->vin}}"
                               data-year="{{$valuation_request->build_date}}" data-registration-number="{{$valuation_request->registration_number}}">
                    </div>
                    <div class="col-xs-12 col-sm-9"><span class="padding-lr5">{{$valuation_request->build_date." ".$valuation_request->make." ".$valuation_request->model_family}}</span></div>
                </div>
            @else
                <hr class="mo-dispute-hr">
                <div class="mo-row ">
                    <div class="col-xs-12 col-sm-12"><span class="padding-lr5">{{$valuation_request->build_date." ".$valuation_request->make." ".$valuation_request->model_family}}</span></div>
                </div>

            @endif
            <?php $count++; ?>
        @endforeach

        <?php
        if($count >= 5){
            echo "<div class='col-xs-12 col-sm-12 text-right mo-show-more'><a href='valuations/requests'><strong>Show More...</strong></a></div>";
        }
        ?>
    @else
        <hr class="mo-dispute-hr">
        <div class="mo-row ">
            <div class="col-xs-12 col-sm-12"><span>There are no data to display at this time</span></div>
        </div>
    @endif
{{--@else--}}
    {{--<hr class="mo-dispute-hr">--}}
    {{--<div class="mo-row ">--}}
        {{--<div class="col-xs-12 col-sm-12"><span>There are no data to display at this time</span></div>--}}
    {{--</div>--}}
@endif



<!-- VALUATION PENDING APPROVAL -->
<div class="mo-form-header mo-action-item padding-top-24">Pending</div>
@if(isset($valuation_pending))
    <?php $count_number = 0; ?>
    @if(!empty($valuation_pending))
        @foreach($valuation_pending as $valuation)
            <hr class="mo-dispute-hr">
            <div class="mo-row">
                @if($valuation->status == 5)
                    <div class="col-xs-12 col-sm-5 no-padding">
                        <input type="button" class="mo-approve-price-btn text-capitalize" value="approve price" data-valuation-id="{{$valuation->id}}">
                        <input type="button" class="mo-reject-price-btn text-capitalize" value="reject" data-valuation-id="{{$valuation->id}}">
                    </div>
                    <div class="col-xs-12 col-sm-7"><span class="padding-lr5">{{$valuation->build_date." ".$valuation->make." ".$valuation->model_family}}</span></div>
                @else
                    <div class="col-xs-12 col-sm-5 no-padding"><input type="button" class="mo-approve-valuate-btn text-capitalize" value="approve valuation" data-valuation-id="{{$valuation->id}}"></div>
                    <div class="col-xs-12 col-sm-7"><span class="padding-lr5">{{$valuation->build_date." ".$valuation->make." ".$valuation->model_family}}</span></div>
                @endif
            </div>
            <?php $count_number++; ?>
        @endforeach

        <?php
        if($count_number >= 5){
            echo "<div class='col-xs-12 col-sm-12 text-right mo-show-more'><a href='valuations/pending'><strong>Show More...</strong></a></div>";
        }
        ?>
    @else
        <hr class="mo-dispute-hr">
        <div class="mo-row ">
            <div class="col-xs-12 col-sm-12"><span>There are no data to display at this time</span></div>
        </div>
    @endif
{{--@else--}}
    {{--<hr class="mo-dispute-hr">--}}
    {{--<div class="mo-row ">--}}
        {{--<div class="col-xs-12 col-sm-12"><span>There are no data to display at this time</span></div>--}}
    {{--</div>--}}
@endif




