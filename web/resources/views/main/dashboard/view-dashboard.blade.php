@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <a href="#">{{ $header or 'Dashboard' }}</a>
        </div>
    </div>
    {{--End - moto Content Header Section--}}
    <div class="mo-row mo-form-wrapper" id="mo-dashboard-overview">
        <form action="" method="post" id="mo-dashboard-customize-form">
            {{csrf_field()}}
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding vertical-center">
                <div class="col-xs-12 col-sm-2 padding-lr5 text-uppercase mo-form-header">
                    overview
                </div>
                <div class="col-xs-12 col-sm-2 padding-lr5 vertical-center cursor-pointer" id="mo-dashboard-customize">
                    {{--@include("main.dashboard.dashboard-customize")--}}
                    <div class="col-xs-12 col-sm-12 padding-top-4">
                        <div data-toggle="dropdown" class="cursor-pointer padding-top-4" id="mo-dashboard-customize">
                            <span><img src="{{asset('images/svg/main/invalid-name-setting.svg')}}" alt=""></span>
                            <label class="text-capitalize font-grey-color-important">customize</label>
                        </div>
                        {{-- dropdown menu --}}
                        <div class="width-700 dropdown-menu mo-dashboard-filter-item padding-top-10 padding-bottom-10 font-size14" id="mo-dashboard-filter-customize">
                            <!-- Customize test drives -->
                            <div class="mo-row col-xs-12 col-sm-12">
                                @if(isset($dashboard_list))
                                    @foreach($dashboard_list as $dashboard)
                                        <div class="col-xs-12 col-sm-6 padding-bottom-24">
                                            {{--<label for="" class="text-capitalize">{{var_dump($dashboard["list_items"][0]->name)}}</label>--}}
                                            <label for="" class="text-capitalize">{{$dashboard["category_type"]}}</label>
                                            @foreach($dashboard["list_items"] as $item)
                                                <div class="mo-row padding-top-10">
                                                    <input type="checkbox" class="mo-square-check mo-customize-item" id="{{$item->code}}" name="customize_items_list[]" value="{{$item->code}}">
                                                    <label for="{{$item->code}}" class="text-capitalize font-family-regular"><span></span>{{$item->name}}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        {{-- end dropdown menu --}}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-3 padding-lr5 vertical-center">
                    <input type="checkbox" class="mo-round-check" id="show-only-my-stats" name="show_only_my_stats">
                    <label for="show-only-my-stats" class="padding-top-10 font-grey-color-important"><span></span>Show only my stats</label>
                </div> <!-- Show only my stats -->

                <div class="col-xs-12 col-sm-5 padding-lr5 vertical-center">
                    @include("main.dashboard.dashboard-filter")
                    @yield("dashboard-filter")
                </div>
            </div> <!-- Header Row -->

            <!-- TAB DASHBOARD -->
            <div class="mo-row">
                {{--@include("main.dashboard.dashboard-tab")--}}
                <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-top-24" id="mo-blocks-wrapper">
                    <div class="col-xs-12 col-sm-2 col-md-2 padding-bottom-10 mo-block-tab hidden" id="tab">
                        <a href="#valuation">
                            <div class="col-xs-12 col-sm-12 col-md-12 no-padding mo-block">
                                <span class="col-xs-12 mo-block-number">0</span>
                                <span class="col-xs-12 mo-block-txt text-capitalize"></span>
                            </div>
                        </a>
                    </div>{{-- <!-- total valuation -->--}}
                </div>
            </div>
        </form>

    </div>

    <!-- MY ACTION ITEMS -->
    <div class="mo-row mo-form-wrapper margin-top-24 " id="mo-dashboard-action-items">
        <div class="col-xs-12 col-sm-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
            my action items
        </div>

        <div class="col-xs-12 col-sm-12 no-padding">
            <!-- COLUMN LEFT -->
            <div class="col-xs-12 col-sm-5">
                <!-- Test Drive In Progress -->
                <div class="mo-row padding-bottom-24">
                    <label for="" class="text-capitalize">test drives in progress</label>
                    @include("main.dashboard.dashboard-test-drive-in-progress")
                </div>

                <!-- VALUATIONS -->
                <div class="mo-row padding-bottom-24">
                    <label for="" class="text-capitalize">Valuations</label>
                    @include("main.dashboard.dashboard-valuation")
                </div> <!-- Valuation Request -->

            </div>

            <!-- COLUMN RIGHT -->
            <div class="col-xs-12 col-sm-5 float-right">
                <div class="mo-row padding-bottom-24">
                    <label for="" class="text-capitalize">place my car</label>
                    @include("main.dashboard.dashboard-place-my-car")
                </div>

                <div class="mo-row">
                    <label for="" class="text-capitalize">transaction</label>
                    @include("main.dashboard.dashboard-transaction")

                </div>
            </div>
        </div>
    </div>




    <!-- POPUP TEST DRIVE IN PROGRESS -->
    <div id="mo-test-drives-comment-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form" id="mo-test-drive-return-vehicle-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading"></span>
            </div>
            <hr class="mo-form-hr padding-bottom-10" />

            <span><label for=" " class="text-capitalize padding-bottom-20">feedback</label></span>

            <div>
                <div class="padding-bottom-10 padding-left-10">How happy were you with the vehicle?</div>
                <div id="slider_vehicle_rating" class=""></div>

                <div class="padding-bottom-10 padding-left-10">How happy were you with us?</div>
                <div id="slider_mo_rating" class=""></div>

                <div>
                    <div class="padding-left-10">How did you hear about us?</div>
                    <div class="col-xs-12">
                        <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}"></span>
                        <select class="form-control" name="mo_marketing_option" id="mo_marketing_option">
                            @if(isset($marketing_option))
                                @foreach($marketing_option as $option)
                                    <option value="{{$option->code}}">{{$option->value}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                <div class="col-xs-12 padding-top-10 hidden" id="mo-marketing-option-other">
                    <input type="text" class="form-control" id="mo-option-other" name="mo_option_other">
                </div>
            </div>

            <span><label for=" " class="text-capitalize padding-top-24">comments</label></span>
            <div class="mo-row padding-bottom-24">
                <hr class="mo-dispute-hr" />
                <div class="form-group col-xs-12 no-padding">
                    <textarea class="mo-textarea form-control" rows="3" id="test-drive-note" name="test_drive_note" placeholder="Write here…" style="box-shadow: none;"></textarea>
                </div>
            </div>
            <hr class="mo-dispute-hr padding-bottom-10"/>
            <div id="errorToShow"></div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-test-drives-inprogress-change-status">
                </div>
            </div>
            <div class="mo-loading-image hidden">Loading...</div>
        </form>
    </div>


    <!-- POPUP ENTER VIN -->
    <div id="mo-enter-vin-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form" id="mo-popup-enter-vin">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">Enter VIN</span>
            </div>
            <hr class="mo-form-hr" />

            <div class="mo-row padding-top-24 padding-bottom-24">

                <div class="form-group col-xs-12 col-sm-8 padding-left-9 padding-bottom-10">
                    <div class="col-xs-11 no-padding">
                        <label for="year">Year*</label>
                        <div>
                            <input type="text" class="form-control" name="year" />
                        </div>
                    </div>
                </div>

                <div class="form-group col-xs-12 col-sm-8 padding-left-9 padding-bottom-10">


                    <div class="col-xs-11 no-padding">
                        <label for="vin">VIN*</label>
                        <input type="text" class="form-control" name="vin" maxlength="17" style="text-transform:uppercase"/>
                    </div>
                    <div class="col-xs-1 no-padding padding-top-30 padding-left-5">
                        <img src="{{ asset('/images/main/icon-download.png') }}" alt="Get VIN" class="no-padding padding-top-4 padding-left-5" id="get-vin" />
                    </div>

                </div>

                <div class="form-group col-xs-12 col-sm-8 padding-left-9 padding-bottom-10">
                    <div class="col-xs-11 no-padding">
                        <label for="registration-number">Registration Number*</label>
                        <div>
                            <input type="text" class="form-control text-uppercase" name="registration-number" />
                        </div>
                    </div>
                </div>

                <div class="form-group col-xs-12 padding-top-24">
                    <label for="note">Notes</label>
                    <hr class="mo-form-hr" />
                    <div class="padding-top-20 note-content text-uppercase">
                        {{--013 MASERATI QUATTROPORTE SPORT GT-S MC SPORTLINE MY13 4D SEDAN V8 4691 CC MPFI 6 SP AUTOMATIC WHITE--}}
                    </div>
                </div>

                <div class="form-group col-xs-12 padding-top-24">
                    <div class="col-xs-12 no-padding padding-bottom-8">
                        <div class="col-xs-3 no-padding vin">VIN:</div>
                        <div class="col-xs-9 no-padding vin vin-number">{{--JT152EEA100302159--}}</div>
                    </div>
                    <hr class="mo-form-hr clear-both" />
                </div>

            </div>

            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="vin-confirm" disabled>
                </div>
            </div>
            <div class="mo-loading-image hidden">Loading...</div>
        </form>
    </div>


    {{--start - SENSITIVE ACTION CONFIRMATION POPUP--}}
    <div id="mo-sensitive-action-confirm-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            {{ csrf_field() }}

            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">valuation approval</span>
            </div>

            <hr class="mo-form-hr" />

            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <span class="suspend-popup-content">Are you sure you want to approve this item?</span>
                </div>
            </div>

            <hr class="mo-form-hr" />

            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-valuation-pending-approval-confirmation-btn">
                </div>
            </div>
            <div class="mo-loading-image hidden">Loading...</div>
        </form>
    </div>


    {{-- Set Location External Offer (incoming) --}}
    <div id="mo-incoming-external-offer-set-location-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">set location</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <select class="col-xs-12 external-offer-won-location-selection" size="5" name="current-user-location" id="current-user-location">
                        @if(isset($user_location))
                            @foreach($user_location as $location)
                                <option value="{{$location->id}}">{{$location->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="submit" id="mo-external-offer-won-location-submit-btn">
                </div>
            </div>
            <div class="mo-loading-image hidden">Loading...</div>
        </form>

    </div>



    {{--External Offer (incoming) bid price --}}
    <div id="mo-external-offer-bid-price-popup" class="mo-white-popup mfp-hide mo-external-offer-bid-price-popup">
        <form class="mo-popup-form" id="mo-external-offer-bid-price-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">Offer Bid</span>
            </div>

            <hr class="mo-form-hr" />

            <!-- ROW1 -->
            <div class="mo-row padding-top-10">
                <div class="alert alert-info col-xs-12 hidden bid-price-info">
                    <strong>Info!</strong> Session has been expired!.
                </div>
                <div class="form-group col-xs-12 col-sm-8">
                    <label for="">Expiry Date</label>
                    <div class="input-group date" id="">
                        <input type="text" class="form-control" id="external-offer-bid-price-expiry-date" name="external-offer-bid-price-expiry-date" placeholder="YYYY-MM-DD" />
                        <input type="hidden" value="{{$external_offer_expiry or ''}}" id="external-offer-expiry">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <!-- ROW1 -->

            <!-- ROW2 -->
            <div class="mo-row padding-top-10">
                <div class="form-group col-xs-12 col-sm-8">
                    <label for="">Price</label> <br/>

                    <input type="text" class="form-control" id="external-offer-bid-price-input" name="external-offer-bid-price-input" placeholder="" />

                </div>
            </div>
            <!-- ROW2 -->


            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="submit" id="mo-external-offer-bid-price-submit-btn">
                </div>
            </div>
            <div class="mo-loading-image hidden"></div>
        </form>
    </div>
    {{--End - External Offer (incoming) bid price --}}



    {{-- Delete External Offer (incoming) --}}
    <div id="mo-incoming-external-offer-delete-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">delete item</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <span>Are you sure you want to delete this item?</span>
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-external-offer-incoming-delete-btn">
                </div>
            </div>
            <div class="mo-loading-image hidden"></div>
        </form>
    </div>
    {{-- End Delete External Offer (incoming) --}}



    {{------------ SUSPEND CONFIRMATION POPUP ------------}}
    <div id="mo-suspend-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">reject offer</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <span class="suspend-popup-content">Are you sure you want to reject this offer?</span>
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-confirm-reject-offer">
                </div>
            </div>
            <input type="hidden" id="mo-offer-id">
        </form>
    </div>
    {{--End - moto Popup Section--}}

@endsection