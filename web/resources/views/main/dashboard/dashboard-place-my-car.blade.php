

<!-- INCOMING -->
<div class="mo-form-header mo-action-item">Incoming</div>
@if(isset($place_my_car_incoming))
    <?php $count = 0; ?>
    @if(!empty($place_my_car_incoming))
        @foreach($place_my_car_incoming as $incoming)
            <hr class="mo-dispute-hr">
            <div class="mo-row">
                @if( $incoming->offer_status == 4)
                    <div class="col-xs-12 col-sm-3 no-padding">
                        <input type="button" class="mo-btn mo-external-offer-set-location-btn" value="set location" data-external-offer-id="{{$incoming->external_offer_id}}">
                    </div>
                    <div class="col-xs-12 col-sm-9"><span class="padding-lr5">{{$incoming->year_build." ".$incoming->make." ".$incoming->model_family}}</span></div>
                @elseif($incoming->offer_status == 1 || $incoming->offer_status == 2)
                    <div class="col-xs-12 col-sm-3 no-padding">
                        <input type="button" class="mo-btn-green external-offer-bid-price-btn" value="Bid" data-external-offer-id="{{$incoming->external_offer_id}}" data-valuation-id="{{$incoming->valuation_id}}">
                        <input type="button" class="mo-btn-red  mo-external-offer-reject" value="Delete" data-external-offer-id="{{$incoming->external_offer_id}}" data-vehicle-id="{{$incoming->vehicle_id}}">
                    </div>
                    <div class="col-xs-12 col-sm-9"><span class="padding-left-20">{{$incoming->year_build." ".$incoming->make}}</span></div>
                @endif
            </div>
            <?php $count++; ?>
        @endforeach
        <hr class="mo-dispute-hr">
        <?php
        if($count >=5){
            echo "<div class='col-xs-12 col-sm-12 text-right mo-show-more'><a href='external-offer/incoming-offers'><strong>Show More...</strong></a></div>";
        }
        ?>
    @else
        <div class="mo-row ">
            <div class="col-xs-12 col-sm-12"><span>There are no data to display at this time</span></div>
        </div>
    @endif
@else
    <div class="mo-row ">
        <div class="col-xs-12 col-sm-12"><span>There are no data to display at this time</span></div>
    </div>
@endif


<!-- OUTGOING -->
<div class="mo-form-header mo-action-item padding-top-24">Outgoing</div>
@if(isset($place_my_car_outgoing))
    <?php $count_number = 0; ?>
    @if(!empty($place_my_car_outgoing))
        @foreach($place_my_car_outgoing as $outgoing)
            <hr class="mo-dispute-hr">
            <div class="mo-row">
                @if($outgoing->offer_status == 3)
                    <div class="col-xs-12 col-sm-3 no-padding">
                        <input type="button" class="mo-btn-green mo-accept-offer" value="Accept" data-external-offer-id="{{$outgoing->external_offer_id}}">
                        <a href="#mo-suspend-popup" class="mo-suspend-popup">
                            <input type="button" class="mo-btn-red mo-reject-offer" value="Reject" data-external-offer-id="{{$outgoing->external_offer_id}}">
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-9"><span class="padding-left-20">{{$outgoing->year_build." ".$outgoing->make." ".$outgoing->model_family}}</span></div>
                @endif
            </div>
            <?php $count_number++; ?>
        @endforeach
        <?php
        if($count_number >=3){
            echo "<div class='col-xs-12 col-sm-12 text-right mo-show-more'><a href='external-offer/outgoing-offers'><strong>Show More...</strong></a></div>";
        }
        ?>
    @else
        <hr class="mo-dispute-hr">
        <div class="mo-row ">
            <div class="col-xs-12 col-sm-12"><span>There are no data to display at this time</span></div>
        </div>
    @endif

@else
    <hr class="mo-dispute-hr">
    <div class="mo-row ">
        <div class="col-xs-12 col-sm-12"><span>There are no data to display at this time</span></div>
    </div>
@endif
<div class="mo-loading-image hidden"></div>