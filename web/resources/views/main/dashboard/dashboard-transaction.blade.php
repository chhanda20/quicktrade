<div class="mo-row padding-bottom-48">

    @if(isset($transaction_list))
        @if(!empty($transaction_list))
            @foreach($transaction_list as $transaction)
                @if($transaction->status == 20)
                    <hr class="mo-dispute-hr">
                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-3 no-padding"><input type="button" class="mo-btn mo-transaction-btn" value="vehicle ready" data-transaction-id="{{$transaction->id}}" data-status="{{$transaction->status}}"></div>
                        <div class="col-xs-12 col-sm-9"><span class="padding-lr5">{{$transaction->vehicle_name." ".$transaction->rego}}</span></div>
                    </div>
                @endif
            @endforeach
        @else
            <hr class="mo-dispute-hr">
            <div class="mo-row ">
                <div class="col-xs-12 col-sm-12"><span>There are no data to display at this time</span></div>
            </div>
        @endif
    @else
        <hr class="mo-dispute-hr">
        <div class="mo-row ">
            <div class="col-xs-12 col-sm-12"><span>There are no data to display at this time</span></div>
        </div>
    @endif
</div>