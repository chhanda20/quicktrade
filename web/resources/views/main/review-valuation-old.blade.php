@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
    {{----------------------------------------------------------- start HEADER INFO -----------------------------------------------------------}}

    {{--Start - moto Header Text Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <div class="col-xs-12 col-sm-8 col-md-8 no-padding">
                <a href="">{{ $header or 'Valuations' }}</a> > <span id="mo-valuation-step-txt" class="text-capitalize">{{ $title or 'Details' }}</span>
            </div>
            {{--<div class="col-xs-12 col-sm-4 col-md-4 no-padding text-right mo-edit-txt">
                <a href="">
                    <span>Edit <img src="{{ asset('images/svg/main/icon-edit.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-edit.png') }}'"></span>
                </a>
            </div>--}}
        </div>
    </div>
    {{--End - moto Header Text Section--}}

    {{--Start - moto Header Info Section--}}
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-3 padding-lr5" id="mo-vehicle-photo">
            <img src="{{ asset('images/login/bg-img.png') }}" alt="Vehicle Photo">

        </div>
        <div class="col-xs-12 col-sm-9 padding-lr5" id="mo-vehicle-info">
            <div class="col-xs-12 no-padding text-uppercase" id="mo-vehicle-tittle">
                {{isset($valuation_name)?$valuation_name:null}}
            </div>
            <div class="col-xs-12 no-padding padding-top-20">
                <form class="form-horizontal" id="mo-customer-details">
                    @for ($i = 0; $i < count($price_array); $i++)
                    <div class="col-xs-12 padding-lr5 padding-top-5">
                        <span class="col-xs-12 col-sm-5 no-padding text-capitalize">
                            {{$price_array[$i]}}:
                        </span>
                        <label class="col-xs-12 col-sm-7 no-padding" for="email">
                            $190,000.00
                        </label>
                    </div>
                    @endfor
                </form>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 no-padding mo-5-blocks padding-top-24" id="mo-blocks-wrapper">
            @for ($i = 0; $i < count($step_array); $i++)
                <div class="col-xs-12 col-sm-2 padding-lr5">
                    <div class="col-xs-12 mo-block" id="{{$step_array[$i]}}_tab">
                        <span class="col-xs-12 mo-block-txt text-capitalize no-padding">{{$step_array[$i]}}</span>
                        <span class="col-xs-12 text-center mo-arrow-focus">
                            <img src="{{ asset('images/svg/main/triangle.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/triangle.png') }}'">
                        </span>
                    </div>
                </div>
            @endfor
        </div>
    </div>
    {{--End - moto Header Info Section--}}

    {{----------------------------------------------------------- end HEADER INFO -----------------------------------------------------------}}


    {{----------------------------------------------------------- start DETAILS -----------------------------------------------------------}}

    {{--Start - moto Details Content Section--}}
    <div class="mo-row mo-form-wrapper margin-top-32 mo-block-list" id="mo-details-block">
        {{--MOTORONE BLOCK INSIDE--}}
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding mo-5-blocks" id="mo-blocks-wrapper">
            @for ($i = 0; $i < count($details_step_array); $i++)
                <div class="col-xs-12 col-sm-5 padding-lr5">
                    <div class="col-xs-12 mo-block-inside">
                        <span class="col-xs-12 mo-block-txt text-capitalize no-padding">{{$details_step_array[$i]}}</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/triangle.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/triangle.png') }}'">
                    </span>
                    </div>
                </div>
            @endfor
        </div>
        {{--SPECS & STANDARD FEATURES--}}
        <div class="mo-row padding-top-36 mo-block-inside-list" id="mo-specs-features-block">
            <div class="col-xs-12 padding-lr5 padding-bottom-24 text-uppercase mo-form-header">
                model specs
            </div>
            <form action="" method="" id="create-vehicle-detail-form">
                {{ csrf_field() }}

                <div class="col-xs-12 no-padding">
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 border-top">
                        <label for="make" class="text-capitalize">make</label>
                        <div class="text-capitalize">{{isset($make) ? $make : null}}</div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 border-top float-right">
                        <label for="engine" class="text-capitalize">engine</label>
                        <div class="text-capitalize">{{isset($engine_number) ? $engine_number : null}}</div>
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="model" class="text-capitalize">model</label>
                        <div class="text-capitalize">{{isset($model) ? $model : null}}</div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="engine-size" class="text-capitalize">engine size</label>
                        <div class="text-capitalize">{{isset($engine_size) ? $engine_size : null}}</div>
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="registration" class="text-capitalize">registration</label>
                        <div class="text-capitalize">{{isset($registration) ? $registration : null}}</div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="engine-capacity" class="text-capitalize">engine capacity</label>
                        <div class="text-capitalize">{{isset($engine_capacity) ? $engine_capacity : null}}</div>
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="variant" class="text-capitalize">variant</label>
                        <div class="text-capitalize">{{isset($variant) ? $variant : null}}</div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="number-of-cylinders" class="text-capitalize">number of cylinders</label>
                        <div class="text-capitalize">{{isset($cylinder) ? $cylinder : null}}</div>
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="genneral-colour" class="text-capitalize">genneral colour</label>
                        <div class="text-capitalize">{{isset($colour) ? $colour : null}}</div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="fuel-type" class="text-capitalize">fuel type</label>
                        <div class="text-capitalize">{{isset($fuel_type) ? $fuel_type : null}}</div>
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="manufacturer-colour" class="text-capitalize">manufacturer colour</label>
                        <div class="text-capitalize">{{isset($manufacturer_color) ? $manufacturer_color : null}}</div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="transmission" class="text-capitalize">transmission</label>
                        <div class="text-capitalize">{{isset($transmission) ? $transmission : null}}</div>
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="body-type" class="text-capitalize">body type</label>
                        <div class="text-capitalize">{{isset($body_type) ? $body_type : null}}</div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="drivetrain" class="text-capitalize">drivetrain</label>
                        <div class="text-capitalize">{{isset($drive_train) ? $drive_train : null}}</div>
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="year-first-registered" class="text-capitalize">year first registered</label>
                        <div class="text-capitalize">{{isset($year_first_registered) ? $year_first_registered : null}}</div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="body-no">Body no.</label>
                        <div class="text-capitalize">{{isset($body_no) ? $body_no : null}}</div>
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="registration-expiry-date" class="text-capitalize">registration expiry date</label>
                        <div class="text-capitalize">{{isset($registration_expiry_date) ? $registration_expiry_date : null}}</div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="vin" class="text-capitalize">VIN</label>
                        <div class="text-capitalize">{{isset($vin) ? $vin : null}}</div>
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="registered-state" class="text-capitalize">registered state</label>
                        <div class="text-capitalize">{{isset($registered_state) ? $registered_state : null}}</div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="chassis-no">Chassis no.</label>
                        <div class="text-capitalize">{{isset($chassis_number) ? $chassis_number : null}}</div>
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="build-date" class="text-capitalize">build date</label>
                        <div class="text-capitalize">{{isset($build_date) ? $build_date : null}}</div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="trim-type" class="text-capitalize">trim type</label>
                        <div class="text-capitalize">{{isset($trim) ? $trim : null}}</div>
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="compliance-date" class="text-capitalize">compliance date</label>
                        <div class="text-capitalize">{{isset($compliance_date) ? $compliance_date : null}}</div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="trim-colour" class="text-capitalize">trim colour</label>
                        <div class="text-capitalize">{{isset($trim_color) ? $trim_color : null}}</div>
                    </div>
                </div>

                <!-- Standard features -->
                <div class="col-xs-12 padding-lr5">
                    <div class="padding-top-24 text-uppercase mo-form-header padding-bottom-24">standard features</div>
                </div>
                @if (isset($option_list)))
                    @for ($i = 0; $i < count($option_list); $i++)
                        <div class="col-xs-12 no-padding">
                            <div class="form-group col-xs-12 col-sm-5 padding-top-24 padding-bottom-8 {{$i==0?'border-top':null}}">
                                <label class="text-uppercase">
                                    [{{$option_list[$i]->glass_option_id}}] {{$option_list[$i]->name}}
                                </label>
                            </div>
                            <span class="hidden">{{$i++}}</span>
                            @if($i<count($option_list))
                                <div class="form-group col-xs-12 col-sm-5 padding-top-24 padding-bottom-8 {{$i==1?'border-top':null}} float-right">
                                    <label class="text-uppercase">
                                        [{{$option_list[$i]->glass_option_id}}] {{$option_list[$i]->name}}
                                    </label>
                                </div>
                            @endif
                        </div>
                    @endfor
                @endif
            </form>
        </div>

        {{-- OPTIONS & EXTRAS --}}
        <div class="mo-row padding-top-36 hidden mo-block-inside-list" id="mo-options-extras-block">
            <div class="col-xs-12 padding-lr5 text-uppercase mo-form-header padding-bottom-24">
                options
            </div>
            <form action="{!! url('save-options-and-extras/'.$valuation_id); !!}" method="post" id="create-option-and-extra-form">
                {{ csrf_field() }}
                <input type="hidden" value="{{isset($vehicle_id) ? $vehicle_id : null}}" name="vehicle_id">
                {{--Options--}}
                @foreach ($optional_options_list as $key => $optional_option)
                    <div class="form-group col-xs-12 col-sm-6 col-md-6 padding-bottom-20">
                        <input type="checkbox" id="{{$optional_option->name}}" name="optional_options[]" {{$optional_option->selected!=false?'checked':null}} value="{{$key}}">
                        <label for="{{$optional_option->name}}" class="text-capitalize"><span></span>{{$optional_option->name}}</label>
                    </div>
                @endforeach
                {{--Extras--}}
                <div class="col-xs-12 padding-lr5">
                    <div class="padding-bottom-20 text-uppercase mo-form-header border-bottom">
                        <span>extras</span>
                        <img id="mo-plus-icon" class="icon-add float-right" src="{{asset('images/svg/main/plus.svg')}}" alt="Plus Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/plus.png') }}'">
                    </div>
                    <!-- EMPTY SPAN TO CLONE LATER -->
                    <span class="col-xs-12 no-padding padding-top-24 padding-bottom-24 text-capitalize mo-extras-options hidden"></span>

                    @foreach ($extras_options_list as $extras_option)
                        <span class="col-xs-12 no-padding padding-top-24 padding-bottom-24 text-capitalize mo-extras-options">
                            <textarea id="{{$extras_option->id}}" class="form-control mo-extras-txt" rows="1" placeholder="Write here…" style="box-shadow: none;" readonly>{{$extras_option->description}}</textarea>
                            <input type="hidden" name="" value="{{$extras_option->id}}">
                            <div class="mo-icon-remove float-right">
                                <img src="{{asset('images/svg/main/icon-remove.svg')}}" alt="Remove Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-remove.png') }}'">
                            </div>
                            <div class="mo-error-wrapper hidden">
                                <img src="{{asset('images/svg/main/ic-warning.svg')}}" alt="Plus Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/ic-warning.png') }}'">
                                <span class="error">Please enter content</span>
                            </div>
                        </span>
                    @endforeach

                </div>
                {{--Save Button--}}
                <div class="form-group col-xs-12 col-sm-12 col-md-12 padding-top-36 text-center">
                    <input type="submit" id="mo-save-options-extras" class="mo-btn text-uppercase" value="save">
                </div>
            </form>
        </div>
    </div>
    {{--End - moto Details Content Section--}}

    {{----------------------------------------------------------- end DETAILS -----------------------------------------------------------}}



    {{----------------------------------------------------------- start SUMMARY -----------------------------------------------------------}}

    {{--Start - moto Content Valuation Summary--}}
    <div class="mo-row mo-form-wrapper hidden margin-top-32 mo-block-list" id="mo-summary-block">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                vehicle details
            </div>
        </div>
        <form action="{!! url('save-summary-info/'.$valuation_id); !!}" method="post" id="create-valuations-summary-form">
            {{ csrf_field() }}

            <!--  -->
            <div class="col-xs-12 col-sm-6 col-md-5">
                <div class="form-group col-xs-12 border-bottom">
                    <label for="year" class="text-capitalize">year</label>
                    <div class="text-uppercase">{{$year or null}}</div>
                </div>
                <div class="form-group col-xs-12 border-bottom">
                    <label for="make" class="text-capitalize">make</label>
                    <div class="text-uppercase">{{$make or null}}</div>
                </div>
                <div class="form-group col-xs-12 border-bottom">
                    <label for="model" class="text-capitalize">model</label>
                    <div class="text-uppercase">{{$model or null}}</div>
                </div>
                <div class="form-group col-xs-12 border-bottom">
                    <label for="badge" class="text-capitalize">badge</label>
                    <div class="text-uppercase">{{$badge or null}}</div>
                </div>
                <div class="form-group col-xs-12 border-bottom">
                    <label for="variant" class="text-capitalize">variant</label>
                    <div class="text-uppercase">{{$variant or null}}</div>
                </div>
                <div class="form-group col-xs-12 border-bottom">
                    <label for="series" class="text-capitalize">series</label>
                    <div class="text-uppercase">{{$series or null}}</div>
                </div>
            </div>

            <!--  -->
            <div class="col-xs-12 col-sm-6 col-md-5 no-padding float-right">
                <div class="form-group col-xs-12">
                    <label for="registration-no" class="text-capitalize">registration no</label>
                    <div class="field-show text-uppercase">{{$registration or null}}</div>
                    <input type="text" class="form-control field-hidden" id="registration-no" name="registration_no" placeholder="{{$registration or null}}">
                </div>
                <div class="form-group col-xs-12">
                    <label for="service-history" class="text-capitalize">service-history</label>
                    <div class="field-show text-capitalize">{{isset($has_service_history) && $has_service_history == 1?'yes':'no'}}</div>
                    <span class="field-hidden"><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                    <select class="form-control text-capitalize field-hidden" name="service_history" id="service-history">
                        <option {{isset($has_service_history) && $has_service_history == 1?'selected':''}} value="1">Yes</option>
                        <option {{isset($has_service_history) && $has_service_history == 0?'selected':''}} value="0">No</option>
                    </select>
                </div>
                <div class="form-group col-xs-12">
                    <label for="service-books" class="text-capitalize">service-books</label>
                    <div class="field-show text-capitalize">{{isset($has_service_books) && $has_service_books == 1?'yes':'no'}}</div>
                    <span class="field-hidden"><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                    <select class="form-control text-capitalize field-hidden" name="service_books" id="service-books">
                        <option {{isset($has_service_books) && $has_service_books == 1?'selected':''}} value="1">Yes</option>
                        <option {{isset($has_service_books) && $has_service_books == 0?'selected':''}} value="0">No</option>
                    </select>
                </div>
                <div class="form-group col-xs-12">
                    <label for="one-owner" class="text-capitalize">one owner</label>
                    <div class="field-show text-capitalize">{{isset($is_one_owner) && $is_one_owner == 1?'yes':'no'}}</div>
                    <span class="field-hidden"><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                    <select class="form-control text-capitalize field-hidden" name="service_history" id="service-history">
                        <option {{isset($is_one_owner) && $is_one_owner == 1?'selected':''}} value="1">Yes</option>
                        <option {{isset($is_one_owner) && $is_one_owner == 0?'selected':''}} value="0">No</option>
                    </select>
                </div>
                <div class="form-group col-xs-12">
                    <label for="" class="text-capitalize">odometer reading</label>
                    <div class="field-show">{{$odometer or null}}</div>
                    <input type="text" class="form-control field-hidden" name="odometer_reading" id="odometer-reading" placeholder="120000">
                </div>
            </div>

            <!-- RATING -->
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                <div class="col-xs-12 no-padding padding-top-24 padding-bottom-24 text-uppercase mo-form-header">
                    rating
                </div>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-5">
                <label for="overall-rating" class="text-capitalize">overall rating</label>
                <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                <select class="form-control text-capitalize" name="overall_rating" id="overall-rating">
                    <option selected disabled>very good</option>
                    <option value="1">bad</option>
                    <option value="2">crazy</option>
                </select>
            </div>

            <!-- CURRENT WHOLESALE -->
            <div class="mo-row">
                <div class="col-xs-12 no-padding padding-top-24 padding-bottom-24 text-uppercase mo-form-header">
                    current wholesale value guide
                </div>
            </div>
            <div class="col-xs-12 no-padding">
                <table class="table">
                    <thead>
                    <tr class="mo-grey-header-wrapper">
                        <th>Pricing</th>
                        <th>Trade Low</th>
                        <th>Trade High </th>
                        <th>Retail</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Glass’s Value</td>
                        @if (isset($wholesale_glass))
                            <td>{{$wholesale_glass->below}}</td>
                            <td>{{$wholesale_glass->above}}</td>
                            <td>{{$wholesale_glass->retail}}</td>
                        @endif
                    </tr>
                    <tr>
                        <td>KM Adjustment</td>
                        @if (isset($wholesale_km_adjustment))
                            <td>{{$wholesale_km_adjustment->below}}</td>
                            <td>{{$wholesale_km_adjustment->above}}</td>
                            <td>{{$wholesale_km_adjustment->retail}}</td>
                        @endif
                    </tr>
                    <tr>
                        <td>Value of Options</td>
                        @if (isset($wholesale_value_options))
                            <td>{{$wholesale_value_options->below}}</td>
                            <td>{{$wholesale_value_options->above}}</td>
                            <td>{{$wholesale_value_options->retail}}</td>
                        @endif
                    </tr>
                    <tr>
                        <td>Adjusted Values</td>
                        @if (isset($wholesale_glass) && isset($wholesale_km_adjustment) && isset($wholesale_value_options))
                            <td>{{$wholesale_glass->below + $wholesale_km_adjustment->below + $wholesale_value_options->below}}</td>
                            <td>{{$wholesale_glass->above + $wholesale_km_adjustment->above + $wholesale_value_options->above}}</td>
                            <td>{{$wholesale_glass->retail + $wholesale_km_adjustment->retail + $wholesale_value_options->retail}}</td>
                        @endif
                    </tr>
                    </tbody>
                </table>
            </div>

            <!-- ESTIMATED COST -->
            <div class="col-xs-12 col-sm-6 col-md-5 no-padding">
                <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header border-bottom">
                    estimated reconditioning cost
                </div>
                <div class="col-xs-12 border-bottom padding-top-24 padding-bottom-8">
                    <label for="exterior" class="text-capitalize">exterior</label>
                    <span class="text-uppercase float-right">${{$reconditioningCost->exterior}}</span>
                </div>
                <div class="col-xs-12 border-bottom padding-top-20 padding-bottom-8">
                    <label for="interior-electronics" class="text-capitalize">interior/electronics</label>
                    <span class="text-uppercase float-right">${{$reconditioningCost->interior}}</span>
                </div>
                <div class="col-xs-12 padding-top-20 padding-bottom-8">
                    <label for="mechanical" class="text-capitalize">mechanical</label>
                    <span class="text-uppercase float-right">${{$reconditioningCost->mechanical}}</span>
                </div>
                <div class="col-xs-12 padding-top-20 padding-bottom-8 mo-grey-header-wrapper">
                    <label for="total" class="text-capitalize">total estimated cost</label>
                    <span class="text-uppercase float-right">${{$reconditioningCost->estimated_recon_cost}}</span>
                </div>
                <div class="col-xs-12 border-bottom padding-top-20 padding-bottom-8 vertical-center" id="mo-overall-cost-wrapper">
                    <label for="overall" class="col-xs-6 no-padding text-capitalize">overall reconditioning cost</label>
                    <div class="col-xs-6 form-inline text-right no-padding">
                        <input type="text" class="hidden mo-overall-cost-input form-control" value="{{$reconditioningCost->estimated_recon_cost}}" name="overall_estimated_cost"/>
                        $<span class="mo-overall-cost-span text-uppercase">{{$reconditioningCost->estimated_recon_cost}}</span>
                        <span class="mo-icon-edit glyphicon glyphicon-pencil"></span>
                        <span class="hidden mo-icon-edit glyphicon glyphicon-ok"></span>
                        {{--<span class="mo-icon-edit glyphicon glyphicon-edit"></span>
                        <span class="hidden mo-icon-edit glyphicon glyphicon-check"></span>--}}
                    </div>
                </div>
            </div>

            <!-- SALE INFO -->

            <div class="col-xs-12 col-sm-6 col-md-5 no-padding float-right">
                <div class="form-group col-xs-12 col-sm-6 col-md-5 no-padding padding-bottom-24 text-uppercase mo-form-header">
                    sales info.
                </div>
                <div class=" form-group col-xs-12 col-sm-12 col-md-12 ">
                    <label for="interior-electronics" class="text-capitalize">vehicle*</label>
                    <span class="field-show text-capitalize float-right ">vehicle</span>
                    <span class="field-hidden"><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                    <select class="form-control text-capitalize field-hidden" name="vehicle" id="vehicle">
                        @if (isset($salesInfoStatuses))
                            @foreach ($salesInfoStatuses as $salesInfoStatus)
                                <option {{isset($salesInfo) && $salesInfo->sale_status == $salesInfoStatus->code?'selected':''}} value="{{$salesInfoStatus->code}}">{{$salesInfoStatus->value}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class=" form-group col-xs-12 col-sm-12 col-md-12  ">
                    <label for="interior-electronics" class="text-capitalize">valuation price*</label>
                    <span class="field-show text-capitalize float-right ">${{$salesInfo->valuation_price or 0}}</span>
                    <p class="info-detail field-show"><span></span></p>
                    <input type="text" class="form-control field-hidden" name="valuation_price" id="valuation-price" value="{{$salesInfo->valuation_price or 0}}">
                </div>
                <div class=" form-group col-xs-12 col-sm-12 col-md-12  ">
                    <label for="interior-electronics" class="text-capitalize">maximum valuation price*</label>
                    <span class="field-show text-capitalize float-right ">${{$salesInfo->max_valuation_price or 0}}</span>
                    <p class="info-detail field-show"><span></span></p>
                    <input type="text" class="form-control field-hidden" name="maximum_valuation_price" id="maximum-valuation-price" value="{{$salesInfo->max_valuation_price or 0}}">
                </div>
            </div>

            <!-- CONFIRM VALUATION -->
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-top-24 padding-bottom-24 text-uppercase mo-form-header">
                    confirm valuations details
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-5">
                    <input type="checkbox" id="all-detail" name="all_detail">
                    <label for="all-detail" class="text-capitalize"><span></span>all details are entered accurately*</label>
                </div>
            </div>

            <div class="form-group col-xs-12 col-sm-12 col-md-12 no-padding padding-top-24">
                <input type="submit" class="mo-btn text-uppercase field-hidden" style="{{isset($status) && $status == 6 ? 'width: 209px' : ''}}" value="{{isset($status) && $status == 6 ? 'Receive Vehicle' : 'Save'}}">
            </div>
        </form>
    </div><!-- mo wrapper -->
    {{--End - moto Content Valuation Summary--}}

    {{----------------------------------------------------------- end SUMMARY -----------------------------------------------------------}}


    {{----------------------------------------------------------- start INTERIOR -----------------------------------------------------------}}

    {{--Start - moto Interior Content Section--}}
    <div class="hidden margin-top-32 mo-block-list" id="mo-interior-block">
        <div class="mo-row mo-form-wrapper">
            <div class="col-xs-12 no-padding" id="mo-empty-holder"></div>
        </div>
        <div class="col-xs-12 no-padding" id="mo-estimated-cost">
            <ul class="mo-main-content no-padding">
                <li data-toggle="collapse" data-target="#mo-interior-sub-content" class="collapsed active">
                    <a href="javascript:void(0);">
                        <div id="mo-heading" class="vertical-center col-xs-12 no-padding">
                            <div class="mo-estimated-cost-wrapper col-xs-12 no-padding">
                                <div class="col-xs-12 col-sm-9 no-padding">
                                    Total Estimated Cost
                                </div>
                                <div class="col-xs-12 col-sm-3 no-padding text-right">
                                    <span class="col-xs-12 col-sm-10 no-padding">$<span id="mo-interior-total-estimate-cost">{{number_format($interior_total_cost, 2)}}</span></span>
                                    <span class="col-xs-12 col-sm-2 no-padding mo-icon-swipe glyphicon glyphicon-menu-down float-right"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                    <ul class="mo-sub-content collapse" id="mo-interior-sub-content">

                        @if (isset($interior_item_list))
                        @foreach ($interior_item_list as $items)
                            @foreach($items->items as $item)
                                <li class="{{$items->section_type}}">
                                    <div class="col-xs-12 padding-tb10 border-bottom no-padding mo-cost-estimate-row" id="{{$item->inspection_item_id}}">
                                        <a href="#mo-edit-inspection-item-popup" class="open-popup-link">
                                            <div class="col-xs-12 col-sm-9 no-padding">
                                                {{$interior_item_no++}}.{{$item->item}}
                                            </div>
                                        </a>
                                        <div class="col-xs-12 col-sm-3 no-padding text-right">
                                            <label class="col-xs-10 no-padding" id="item_cost_{{$items->section_type}}_{{$item->inspection_item_id}}">$<span>{{number_format($item->repair_cost, 2)}}<span></label>

                                            <a href="#mo-confirm-delete-exterior-popup" class="open-popup-link">
                                                <span class="col-xs-2 no-padding">
                                                    <img src="{{ asset('images/svg/main/icon-remove.svg') }}" alt="Remove Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-remove.png') }}'" class="mo-delete-item">
                                                </span>
                                            </a>

                                        </div>
                                    </div>
                                </li><!-- ROW 1 -->
                            @endforeach
                        @endforeach
                        @endif
                    </ul>
                </li>

            </ul>
        </div>
    </div>
    {{--End - moto Interior Content Section--}}

    {{----------------------------------------------------------- end INTERIOR -----------------------------------------------------------}}



    {{----------------------------------------------------------- start MECHANICAL -----------------------------------------------------------}}

    {{--Start - moto Interior Content Section--}}
    <div class="hidden margin-top-32 mo-block-list" id="mo-mechanical-block">
        <div class="mo-form-wrapper">
            <form action="{!! url('save-mechanical-details/'.$valuation_id); !!}" method="post" id="">
                {{ csrf_field() }}
                <div class="mo-row padding-bottom-32">
                    <div class="col-xs-12 padding-bottom-24 text-uppercase mo-form-header">
                        vehicle condition report
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                        <input type="checkbox" class="check-icon" id="test-driven" name="test_driven" {{isset($mechanicalDetails->test_driven)&&$mechanicalDetails->test_driven == 1?'checked':''}} value="{{isset($mechanicalDetails->test_driven)?'has_value':''}}">
                        <label for="test-driven" class="check-icon"><span></span> Have you test driven the vehicle?</label>
                    </div>
                </div>
                <div class="mo-row">
                    <div class="col-xs-12 padding-bottom-24 text-uppercase mo-form-header">
                        mechanical report
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                        <textarea class="mo-textarea form-control" rows="3" name="mechanical_report" placeholder="Add additional details" style="box-shadow: none;">{{isset($mechanicalDetails->mechanical_additional_detail)&&$mechanicalDetails->mechanical_additional_detail!=''?$mechanicalDetails->mechanical_additional_detail:null}}</textarea>
                    </div>
                    {{--Save Button--}}
                    <div class="form-group col-xs-12 padding-top-20">
                        <input type="submit" id="mo-save-options-extras" class="mo-btn text-uppercase" value="save">
                    </div>
                </div>
            </form>
        </div>
        <div class="col-xs-12 no-padding" id="mo-estimated-cost">
            <ul class="mo-main-content no-padding">
                <li data-toggle="collapse" data-target="#mo-mechanical-sub-content" class="collapsed active">
                    <a href="javascript:void(0);">
                        <div id="mo-heading" class="vertical-center col-xs-12 no-padding">
                            <div class="mo-estimated-cost-wrapper col-xs-12 no-padding">

                                <div class="col-xs-12 col-sm-9 no-padding">
                                    Total Estimated Cost 
                                </div>
                                <div class="col-xs-12 col-sm-3 no-padding text-right">
                                    <span class="col-xs-12 col-sm-10 no-padding">$<span id="mo-mechanical-total-estimate-cost">{{number_format($mechanical_total_cost, 2)}}</span></span>
                                    <span class="col-xs-12 col-sm-2 no-padding mo-icon-swipe glyphicon glyphicon-menu-down float-right"></span>
                                </div>

                            </div>
                        </div>
                    </a>
                    <ul class="mo-sub-content collapse" id="mo-mechanical-sub-content">
                        @if (isset($mechanical_item_list))
                            @foreach ($mechanical_item_list as $items)
                                @foreach($items->items as $item)
                                    <li class="{{$items->section_type}}">
                                        <div class="col-xs-12 padding-tb10 border-bottom no-padding mo-cost-estimate-row" id="{{$item->inspection_item_id}}">
                                            <a href="#mo-edit-inspection-item-popup" class="open-popup-link">
                                                <div class="col-xs-12 col-sm-9 no-padding">
                                                    {{$mechanical_item_no++}}.{{$item->item}}
                                                </div>
                                            </a>
                                            <div class="col-xs-12 col-sm-3 no-padding text-right">
                                                <label class="col-xs-10 no-padding" id="item_cost_{{$items->section_type}}_{{$item->inspection_item_id}}">$ <span>{{number_format($item->repair_cost, 2)}}</span></label>

                                                <a href="#mo-confirm-delete-exterior-popup" class="open-popup-link">

                                                    <span class="col-xs-2 no-padding">
                                                        <img src="{{ asset('images/svg/main/icon-remove.svg') }}" alt="Remove Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-remove.png') }}'" class="mo-delete-item">
                                                    </span>
                                                </a>

                                            </div>
                                        </div>
                                    </li><!-- ROW 1 -->
                                @endforeach
                            @endforeach
                        @endif
                    </ul>
                </li>

            </ul>
        </div>
    </div>
    {{--End - moto Interior Content Section--}}

    {{----------------------------------------------------------- end MECHANICAL -----------------------------------------------------------}}




    {{----------------------------------------------------------- start EXTERIOR -----------------------------------------------------------}}

    {{--Start - moto Exterior Content Section--}}
    <div class="hidden mo-block-list" id="mo-exterior-block">
        <div class="mo-row mo-form-wrapper margin-top-32">
            <div class="col-xs-12 no-padding text-center">
                <img src="{{ asset('images/svg/perform_valuation/diagram.svg') }}" alt="Diagram" onerror="this.onerror=null; this.src='{{ asset('images/perform_valuation/diagram.png') }}'" id="mo-vehicle-diagram" class="img-responsive">
            </div>
        </div>
        <div class="col-xs-12 no-padding" id="mo-estimated-cost">
            <ul class="mo-main-content no-padding">
                <li data-toggle="collapse" data-target="#mo-exterior-sub-content" class="collapsed active">
                    <a href="javascript:void(0);">
                        <div id="mo-heading" class="vertical-center col-xs-12 no-padding">
                            <div class="mo-estimated-cost-wrapper col-xs-12 no-padding">
                                <div class="col-xs-12 col-sm-9 no-padding">
                                    Total Estimated Cost
                                </div>
                                <div class="col-xs-12 col-sm-3 no-padding text-right">
                                    <span class="col-xs-12 col-sm-10 no-padding">$<span id="mo-exterior-total-estimate-cost">{{number_format($total_cost, 2)}}</span></span>
                                    <span class="col-xs-12 col-sm-2 no-padding mo-icon-swipe glyphicon glyphicon-menu-down float-right"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                    <ul class="mo-sub-content collapse" id="mo-exterior-sub-content">
                        <input type="hidden" value="{{isset($valuation_id) ? $valuation_id : null}}" id="valuation_id">
                        @if (isset($inspection_item_list))
                            @foreach ($inspection_item_list as $inspection_item)
                                <li>
                                    <div class="mo-form-header col-xs-12 padding-tb10 border-bottom no-padding mo-cost-estimate-row">
                                        <div class="col-xs-12 col-sm-9 no-padding">
                                            {{$inspection_item->section_name}}
                                        </div>
                                        <div class="col-xs-12 col-sm-3 text-right no-padding">
                                            <span class="col-xs-10 no-padding" id="section_cost_{{$inspection_item->section_type}}">
                                                   $<span>{{number_format($all_section_cost["section".$inspection_item->section_type], 2)}}</span>
                                            </span>
                                            <span class="col-xs-2 no-padding"></span>
                                        </div>
                                    </div>
                                </li>
                                @if (isset($inspection_item))
                                    <?php $inspection_item_no = 1; ?>
                                    @foreach ($inspection_item->items as $item)
                                        <li class="{{$inspection_item->section_type}}">
                                            <div class="col-xs-12 padding-tb10 border-bottom no-padding mo-cost-estimate-row" id="{{$item->inspection_item_id}}">
                                                <a href="#mo-edit-inspection-item-popup" class="open-popup-link">
                                                <div class="col-xs-12 col-sm-9 no-padding">
                                                        {{$inspection_item_no++}}. {{$item->item}}
                                                    </div>
                                                </a>
                                                <div class="col-xs-12 col-sm-3 no-padding text-right">
                                                    <label class="col-xs-10 no-padding" id="item_cost_{{$inspection_item->section_type}}_{{$item->inspection_item_id}}">$<span>{{ number_format($item->repair_cost , 2) }}</span></label>
                                                    <span class="col-xs-2 no-padding">
                                                        <a href="#mo-confirm-delete-exterior-popup" class="open-popup-link">
                                                            <img src="{{ asset('images/svg/main/icon-remove.svg') }}" alt="Remove Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-remove.png') }}'" class="mo-delete-item">
                                                        </a>
                                                    </span>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                    </ul>
                </li>
            </ul>
        </div>

        {{--Start - moto Popup Section--}}
        {{------------ DELETE CONFIRMATION POPUP ------------}}
        <div id="mo-confirm-delete-exterior-popup" class="mo-white-popup mfp-hide">
            <form class="mo-popup-form">
                <div class="mo-row padding-bottom-20">
                    <span class="text-capitalize mo-popup-heading">delete item</span>
                </div>
                <hr class="mo-form-hr" />
                <div class="mo-row padding-top-24 padding-bottom-24">
                    <div class="form-group col-xs-12 no-padding">
                        <span>Are you sure you want to delete this item?</span>
                    </div>
                </div>
                <hr class="mo-form-hr" />
                <div class="mo-row">
                    <div class="col-xs-12 padding-top-24 text-right">
                        <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                        <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-detele-item-record">
                    </div>
                </div>
            </form>
        </div>
        {{--End - moto Popup Section--}}
    </div>
    {{--End - moto Exterior Content Section--}}

    {{----------------------------------------------------------- end EXTERIOR -----------------------------------------------------------}}

    {{----------------------------------------------------------- EDIT INSPECTION ITEM POPUP -----------------------------------------------------------}}
    <div id="mo-edit-inspection-item-popup" class="mo-white-popup mo-edit-inspection-item-popup mfp-hide">
        <form class="mo-exterior-popup-form" action="/upload-image" method="post" enctype="multipart/form-data" id="mo-exterior-popup-form" >
            {{ csrf_field() }}
            <input type="hidden" value="0" name="sequence" />
            <input type="hidden" value="{{isset($valuation_id) ? $valuation_id : null}}" name="valuation_id">
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">Edit Inspection item</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row mo-exterior-popup-content padding-top-24 padding-bottom-20">
                <div class="form-group col-xs-12 col-sm-8">
                    <label for="selection">Section</label>
                    <div class="mo-dropdown-wrapper">
                        <span class="mo-icon-swipe glyphicon glyphicon-menu-down float-right"></span>
                        <select class="form-control mo-inspection-popup-selectbox" name="exterior-section" id="exterior-section" disabled>
                            <option disabled selected>-- Sections list --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-8">
                    <label for="item">Item</label>
                    <input type="hidden" value="" name="exterior-item-id" />
                    <div class="mo-dropdown-wrapper">
                        <span class="mo-icon-swipe glyphicon glyphicon-menu-down float-right"></span>
                        <select class="form-control mo-inspection-popup-selectbox" name="exterior-items" id="exterior-items" disabled>
                            <option selected disabled>-- Items list --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-8">
                    <label for="estimate-cost">Estimate Cost</label>
                    <input type="number" pattern="[0-9]*" value="" name="estimate_cost" id="estimate-cost" placeholder="$3,500.00" class="form-control mo-inspection-popup-textbox " maxlength="13" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46"/> <br/>
                </div>
                <div class="form-group col-xs-12">
                    <div class="col-xs-4 mo-inspection-upload-image-box" id="mo-image-box-1">
                        <label for="fileToUpload" class="mo-inspection-input-file-label" id="ImageBrowse">
                            <img src="https://mo.blob.core.windows.net/mo-exterior/32_3_1.png" alt="Upload" onerror="this.onerror=null; this.src='{{ asset('images/perform_valuation/image_upload.png') }}'" class="mo-inspection-upload-image-icon" />
                            <!-- <span class="mo-inspection-upload-image-label">Add Photo</span> -->
                        </label>
                        <input type="file" name="fileToUpload" id="fileToUpload" class="mo-exterior-input-file" />
                    </div>
                    <div class="col-xs-4 mo-inspection-upload-image-box" id="mo-image-box-2">
                        <label for="fileToUpload" class="mo-inspection-input-file-label">
                            <img src="https://mo.blob.core.windows.net/mo-exterior/32_3_2.png" alt="Upload" onerror="this.onerror=null; this.src='{{ asset('images/perform_valuation/image_upload.png') }}'" class="mo-inspection-upload-image-icon" />
                        </label>
                    </div>
                    <div class="col-xs-4 mo-inspection-upload-image-box" id="mo-image-box-3">
                        <label for="fileToUpload" class="mo-inspection-input-file-label">
                            <img src="https://mo.blob.core.windows.net/mo-exterior/32_3_3.png" alt="Upload" onerror="this.onerror=null; this.src='{{ asset('images/perform_valuation/image_upload.png') }}'" class="mo-inspection-upload-image-icon" />
                        </label>
                    </div>
                </div>
                <div class="form-group col-xs-12 padding-top-24">
                    <label for="note">Note</label>
                    <textarea class="mo-textarea form-control" rows="5" id="note" name="note" placeholder="Write here…" style="box-shadow: none;"></textarea>
                </div>
            </div>
            <hr class="mo-form-hr"/>
            <div class="mo-row padding-top-24">
                <div class="col-xs-12 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="CANCEL">
                    <input type="button" class="mo-btn text-uppercase" value="UPDATE" id="exterior-edit-button">
                </div>
            </div>
        </form>
    </div>
    {{----------------------------------------------------------- EDIT INSPECTION ITEM POPUP -----------------------------------------------------------}}
@endsection