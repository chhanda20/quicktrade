@section('review-summary')
    @if($errors->any())
        <h4>{{$errors->first()}}</h4>
    @endif
    {{----------------------------------------------------------- start SUMMARY -----------------------------------------------------------}}

    {{--Start - moto Content Valuation Summary--}}
    <div class="mo-row mo-form-wrapper hidden margin-top-32 mo-block-list" id="mo-summary-block">
        <div class="col-xs-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
            vehicle details
        </div>


        @if (isset ($status) && $status == 6 && isset($enable_receive_vehicle))
            <form action="{!! url('receive-vehicle/'.$valuation_id) !!}" method="post" id="receive-vehicle-form">
        @else
            <form action="{!! url('save-summary-info/'.$valuation_id) !!}" method="post" id="create-valuations-summary-form">
        @endif

            {{ csrf_field() }}
            {{--Allocate dealer stock number--}}
            <input type="hidden" value="" name="stock-number" />
            <!--  -->

            <div class="col-xs-12 col-sm-6 col-md-5">
                <div class="form-group col-xs-12 border-bottom padding-bottom-8">
                    <label for="year" class="text-capitalize">ownership type</label>
                    <div class="text-capitalize">{{$ownership_type}}</div>
                </div>
                <div class="form-group col-xs-12 border-bottom padding-bottom-8">
                    <label for="year" class="text-capitalize">special disclosures</label>
                    <div>{{implode(",",$special_disclosures)}}</div>
                </div>
            </div>

            {{--<div class="col-xs-12 col-sm-6 col-md-5">
                <div class="form-group col-xs-12 border-bottom padding-bottom-8">
                    <label for="year" class="text-capitalize">year</label>
                    <div class="text-uppercase">{{$year or null}}</div>
                </div>
                <div class="form-group col-xs-12 border-bottom padding-bottom-8">
                    <label for="make" class="text-capitalize">make</label>
                    <div class="text-uppercase">{{$make or null}}</div>
                </div>
                <div class="form-group col-xs-12 border-bottom padding-bottom-8">
                    <label for="model" class="text-capitalize">model</label>
                    <div class="text-uppercase">{{$model or null}}</div>
                </div>
                <div class="form-group col-xs-12 border-bottom padding-bottom-8">
                    <label for="badge" class="text-capitalize">badge</label>
                    <div class="text-uppercase">{{$badge or null}}</div>
                </div>
                <div class="form-group col-xs-12 border-bottom padding-bottom-8">
                    <label for="variant" class="text-capitalize">variant</label>
                    <div class="text-uppercase">{{$variant or null}}</div>
                </div>
                <div class="form-group col-xs-12 border-bottom padding-bottom-8">
                    <label for="series" class="text-capitalize">series</label>
                    <div class="text-uppercase">{{$series or null}}</div>
                </div>
            </div>--}}



            <!--  -->
            <div class="col-xs-12 col-sm-6 col-md-5 no-padding float-right">
                <div class="form-group col-xs-12 {{$hideBorderBottom != ''?'border-bottom':''}} padding-bottom-8">
                    <label for="registration-no" class="text-capitalize">registration no</label>
                    <div class="{{$hideViewField or ''}} text-uppercase">{{$registration or null}}</div>
                    <input type="text" class="form-control text-uppercase {{$hideEditField or ''}}" id="registration-no" name="registration_no" value="{{$registration or null}}">
                </div>
                <div class="form-group col-xs-12 {{$hideBorderBottom != ''?'border-bottom':''}} padding-bottom-8">
                    <label for="service-history" class="text-capitalize">service-history</label>
                    <div class="{{$hideViewField or ''}} text-capitalize">{{isset($has_service_history) && $has_service_history == 1?'yes':'no'}}</div>
                    <span class="{{$hideEditField or ''}}"><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                    <select class="form-control text-capitalize {{$hideEditField or ''}}" name="service_history" id="service-history">
                        <option {{isset($has_service_history) && $has_service_history == 1?'selected':''}} value="1">Yes</option>
                        <option {{isset($has_service_history) && $has_service_history == 0?'selected':''}} value="0">No</option>
                    </select>
                </div>
                <div class="form-group col-xs-12 {{$hideBorderBottom != ''?'border-bottom':''}} padding-bottom-8">
                    <label for="service-books" class="text-capitalize">service-books</label>
                    <div class="{{$hideViewField or ''}} text-capitalize">{{isset($has_service_book) && $has_service_book == 1?'yes':'no'}}</div>
                    <span class="{{$hideEditField or ''}}"><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                    <select class="form-control text-capitalize {{$hideEditField or ''}}" name="service_books" id="service-books">
                        <option {{isset($has_service_book) && $has_service_book == 1?'selected':''}} value="1">Yes</option>
                        <option {{isset($has_service_book) && $has_service_book == 0?'selected':''}} value="0">No</option>
                    </select>
                </div>
                <div class="form-group col-xs-12 {{$hideBorderBottom != ''?'border-bottom':''}} padding-bottom-8">
                    <label for="one-owner" class="text-capitalize">one owner</label>
                    <div class="{{$hideViewField or ''}} text-capitalize">{{isset($has_one_owner) && $has_one_owner == 1?'yes':'no'}}</div>
                    <span class="{{$hideEditField or ''}}"><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                    <select class="form-control text-capitalize {{$hideEditField or ''}}" name="one_owner" id="one-owner">
                        <option {{isset($has_one_owner) && $has_one_owner == 1?'selected':''}} value="1">Yes</option>
                        <option {{isset($has_one_owner) && $has_one_owner == 0?'selected':''}} value="0">No</option>
                    </select>
                </div>
                <div class="form-group col-xs-12 {{$hideBorderBottom != ''?'border-bottom':''}} padding-bottom-8">
                    <label for="" class="text-capitalize">odometer reading</label>
                    <div class="{{$hideViewField or ''}}">{{number_format($odometer)}}</div>
                    <input type="number" pattern="[0-9]*" class="form-control {{$hideEditField or ''}}" name="odometer_reading" id="odometer-reading" value="{{$odometer}}">
                </div>
            </div>

            <!-- RATING -->
            <div class="col-xs-12 no-padding padding-top-24 padding-bottom-24 text-uppercase mo-form-header">
                rating
            </div>
            <div class="col-xs-12 col-sm-6 col-md-5">
                <div class="form-group col-xs-12 {{$hideBorderBottom != ''?'border-bottom':''}} padding-bottom-8">
                    <label for="overall-rating" class="text-capitalize">overall rating*</label>
                    <span class="{{$hideEditField or ''}}"><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                    <select class="{{$hideEditField or ''}} form-control text-capitalize" name="overall_rating" id="overall-rating">
                        @if (isset($overall_rating_values))
                            @if($status == 2 || $status == 10)
                                <option selected disabled>Overal Rating list</option>
                                @foreach ($overall_rating_values as $overall_rating_value)
                                    <option value="{{$overall_rating_value->code}}">{{$overall_rating_value->value}}</option>
                                @endforeach
                            @else
                                @foreach ($overall_rating_values as $overall_rating_value)
                                    <option {{isset($overall_rating_value) && $overall_rating_value->code == $overall_rating?'selected':''}} value="{{$overall_rating_value->code}}">{{$overall_rating_value->value}}</option>
                                @endforeach
                            @endif    
                        @endif
                    </select>
                    <div class="{{$hideViewField or ''}} text-capitalize">{{$overall_rating_selected_value or ''}}</div>
                    <div id="errorShowMessage"></div>
                </div>

            </div>

            <!-- CURRENT WHOLESALE -->
            <div class="mo-row">
                @if(in_array("wholesale_value_update",$permissions))
                    <div class="mo-row">
                        <div class="col-xs-12 no-padding padding-top-24 padding-bottom-24 text-uppercase mo-form-header">
                            current wholesale value guide
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="padding-lr5">
                            <table class="table">
                                <thead>
                                <tr class="mo-grey-header-wrapper">
                                    <th>Pricing</th>
                                    <th class="text-right">Trade Low</th>
                                    <th class="text-right">Trade High </th>
                                    <th class="text-right">Retail</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><label>Glass’s Value</label></td>
                                    @if (isset($wholesale_glass))
                                        <td class="text-right">${{number_format($wholesale_glass->below)}}</td>
                                        <td class="text-right">${{number_format($wholesale_glass->above)}}</td>
                                        <td class="text-right">${{number_format($wholesale_glass->retail)}}</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td><label>KM Adjustment</label></td>
                                    @if (isset($wholesale_km_adjustment))
                                        <td class="text-right">${{number_format($wholesale_km_adjustment->below)}}</td>
                                        <td class="text-right">${{number_format($wholesale_km_adjustment->above)}}</td>
                                        <td class="text-right">${{number_format($wholesale_km_adjustment->retail)}}</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td><label>Value of Options</label></td>
                                    @if (isset($wholesale_value_options))
                                        <td class="text-right">${{number_format($wholesale_value_options->below)}}</td>
                                        <td class="text-right">${{number_format($wholesale_value_options->above)}}</td>
                                        <td style="width: 25%;" class="text-right">
                                            <input class="{{$hideEditField or ''}}" type="number" pattern="[0-9]*" class="form-control" name="wholesale_value_options" value="{{$wholesale_value_options->retail}}" >
                                            <span class="{{$hideViewField or ''}}">${{number_format($wholesale_value_options->retail)}}</span>
                                        </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td><label>Adjusted Values</label></td>
                                    @if (isset($wholesale_glass) && isset($wholesale_km_adjustment) && isset($wholesale_value_options))
                                        <td class="text-right">${{number_format($wholesale_glass->below - $wholesale_km_adjustment->below + $wholesale_value_options->below)}}</td>
                                        <td class="text-right">${{number_format($wholesale_glass->above - $wholesale_km_adjustment->above + $wholesale_value_options->above)}}</td>
                                        <td class="text-right">${{number_format($wholesale_glass->retail - $wholesale_km_adjustment->retail + $wholesale_value_options->retail)}}</td>
                                    @endif
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
            </div>
            <!-- END CURRENT WHOLESALE -->

            <!-- EXTERNAL OFFERING -->
            <div class="mo-row">
                @if(in_array("external_offer_create",$permissions) && !in_array($status, array(12,13,14)))
                    <div class="col-xs-12 no-padding padding-top-24 padding-bottom-24 padding-left-24 margin-bottom-24 text-uppercase mo-form-header border">
                        <input type="hidden" value="" name="external_offer_id"/>
                        <span>place my car: <span class="external-offer-currency-symbol {{!isset($best_offer) ? 'hidden' : ''}}">$</span> <span class="bid_price">{{isset($best_offer) ? number_format($best_offer->bid_price, 2) : "" }}</span></span>

                        @if (isset ($external_offer_status) && $external_offer_status != 'begin_send_request' && isset($remaining_time) && $remaining_time !='time_is_up')
                            <span class="fa fa-clock-o float-right mo-external-offer-remaining-clock margin-right-24"></span>
                            <span class="float-right time-remaining margin-right-12"><span class="remaining-time-label">{{isset($remaining_time) ? $remaining_time : ''}}</span> minutes left</span>
                            <span class="float-right external-offer-expiring margin-right-20 font-black-color font-lowercase font-weight-light hidden">Expiring: <span class="expiring-day">{{isset($best_offer) ? $best_offer->expiry_date : ""}}</span></span>
                        @elseif (isset($best_offer))
                            <span class="float-right external-offer-expiring margin-right-20 font-black-color font-lowercase font-weight-light">Expiring: <span class="expiring-day">{{isset($best_offer) ? $best_offer->expiry_date : ""}}</span></span>
                        @else
                            <span class="float-right external-offer-expiring margin-right-20 font-black-color font-lowercase font-weight-light hidden">Expiring: <span class="expiring-day"></span></span>
                            <span class="fa fa-clock-o float-right mo-external-offer-remaining-clock margin-right-24 hidden"></span>
                            <span class="float-right time-remaining margin-right-12 hidden"><span class="remaining-time-label">00:00</span> minutes left</span>
                        @endif

                        <div class="text-center padding-top-37">

                            @if (isset ($external_offer_status) && $external_offer_status == 'begin_send_request')
                                {{--<a href="#mo-external-offering-popup" class="" id="mo-external-offer-open-popup">--}}
                                    <input type="button" value="send offer request" class="mo-btn-black {{$disableEditButton or ''}}" id="mo-external-offer-send-btn"/>
                                {{--</a>--}}
                                <input type="button" value="accept" class="mo-btn width-150 hidden" id="mo-external-offer-accept-btn"/>
                                <input type="button" value="reject" class="mo-btn mo-black-btn width-150 hidden" id="mo-external-offer-reject-btn"/>
                                <span class="hidden mo-external-offer-null-message">There is no best bid price</span>
                            @elseif ($remaining_time != 'time_is_up')
                                <a href="#mo-external-offering-popup" class="" id="mo-external-offer-open-popup">
                                    <input type="button" value="awaiting offer..." class="mo-btn-black" id="mo-external-offer-send-btn" disabled/>
                                </a>
                                <input type="button" value="accept" class="mo-btn width-150 hidden" id="mo-external-offer-accept-btn"/>
                                <input type="button" value="reject" class="mo-btn mo-black-btn width-150 hidden" id="mo-external-offer-reject-btn"/>
                                <span class="hidden mo-external-offer-null-message">There is no best bid price</span>
                            @elseif ($remaining_time == 'time_is_up')

                                @if(isset($best_offer))
                                    {{-- external offer accepted --}}

                                    @if ($external_offer_status->status == 3)
                                        <input type="button" value="accept" class="mo-btn width-150" id="mo-external-offer-accept-btn"/>
                                        <input type="button" value="reject" class="mo-btn mo-black-btn width-150" id="mo-external-offer-reject-btn"/>
                                    @elseif ($external_offer_status->status != 1 || $external_offer_status->status != 2 || $external_offer_status->status != 3 || $external_offer_status->status != 10)
                                        <input type="button" value="ACCEPTED" class="mo-btn-black" id="mo-external-offer-send-btn" disabled/>
                                    @endif

                                @elseif (!isset($best_offer))
                                <!-- <span>There is no best bid price</span>   -->
                                    <a href="#mo-external-offering-popup" class="" id="mo-external-offer-open-popup">
                                        <input type="button" value="send offer request" class="mo-btn-black" id="mo-external-offer-send-btn"/>
                                    </a>
                                    <input type="button" value="accept" class="mo-btn width-150 hidden" id="mo-external-offer-accept-btn"/>
                                    <input type="button" value="reject" class="mo-btn mo-black-btn width-150 hidden" id="mo-external-offer-reject-btn"/>
                                    <span class="hidden mo-external-offer-null-message">There is no best bid price</span>
                                @endif

                            @endif

                        </div>
                    </div>
                @endif
            </div>

            <!-- END EXTERNAL OFFERING -->

            <!-- ESTIMATED COST -->
            <div class="col-sm-6 col-md-5 no-padding">
                <div class="col-xs-12 no-padding text-uppercase mo-form-header">
                    estimated reconditioning cost
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-12 border-bottom padding-top-24 padding-bottom-8">
                        <label for="exterior" class="text-capitalize">exterior</label>
                        <span id="mo-exterior-cost" class="text-uppercase float-right">${{number_format($reconditioningCost->exterior + $general_cost->generalReconditionCost, 2)}}</span>
                    </div>
                    <div class="col-xs-12 border-bottom padding-top-20 padding-bottom-8">
                        <label for="interior-electronics" class="text-capitalize">interior/electronics</label>
                        <span id="mo-interior-cost" class="text-uppercase float-right">${{number_format($reconditioningCost->interior)}}</span>
                    </div>
                    <div class="col-xs-12 padding-top-20 padding-bottom-8">
                        <label for="mechanical" class="text-capitalize">mechanical</label>
                        <span id="mo-mechanical-cost" class="text-uppercase float-right">${{number_format($reconditioningCost->mechanical)}}</span>
                    </div>
                    <div class="col-xs-12 padding-top-20 padding-bottom-8 mo-grey-header-wrapper">
                        <label for="total" class="text-capitalize">total estimated cost</label>
                        <span id="mo-total-estimated-cost" class="text-uppercase float-right">${{number_format($reconditioningCost->exterior + $reconditioningCost->interior + $reconditioningCost->mechanical)}}</span>
                    </div>
                    <div class="col-xs-12 border-bottom padding-top-20 padding-bottom-8 vertical-center" id="mo-overall-cost-wrapper">
                        <label for="overall" class="col-xs-6 no-padding text-capitalize">override reconditioning cost</label>
                        <div class="col-xs-6 form-inline text-right no-padding">
                            <?php
                                $recon_cost = $reconditioningCost->estimated_recon_cost;
                                $total_cost = $reconditioningCost->exterior + $reconditioningCost->interior + $reconditioningCost->mechanical;

                            ?>

                            @if($status != 2)
                                <input type="hidden" class="estimated-recon-cost" name="estimated_recon_cost" value="{{$recon_cost}}">
                                <input type="number" pattern="[0-9]*" class="hidden mo-overall-cost-input form-control" value="{{$recon_cost}}" name="overall_estimated_cost"/>
                                $<span class="mo-overall-cost-span text-uppercase">{{$recon_cost != 0 ? number_format($recon_cost): number_format($total_cost)}}</span>
                            @else
                                <input type="hidden" class="estimated-recon-cost" name="estimated_recon_cost" value="{{empty($recon_cost) || $recon_cost == 0 ? $total_cost : $recon_cost }}">
                                <input type="number" pattern="[0-9]*" class="hidden mo-overall-cost-input form-control" value="{{$recon_cost}}" name="overall_estimated_cost"/>
                                $<span class="mo-overall-cost-span text-uppercase">{{empty($recon_cost)?"0":$recon_cost}}</span>
                            @endif


                            {{--<input type="number" pattern="[0-9]*" class="hidden mo-overall-cost-input form-control" value="{{$reconditioningCost->estimated_recon_cost}}" name="overall_estimated_cost"/>--}}
                            {{--$<span class="mo-overall-cost-span text-uppercase">{{number_format($reconditioningCost->estimated_recon_cost)}}</span>--}}
                            <span class="{{$hideEditField or ''}} mo-icon-edit glyphicon glyphicon-pencil"></span>
                            <span class="hidden mo-icon-edit glyphicon glyphicon-ok"></span>
                            {{--<span class="mo-icon-edit glyphicon glyphicon-edit"></span>
                            <span class="hidden mo-icon-edit glyphicon glyphicon-check"></span>--}}
                        </div>
                    </div>
                </div>
            </div>

            <!-- SALE INFO -->
            @if(in_array("sales_info_update",$permissions))
                <div class="col-sm-6 col-md-5 no-padding float-right">
                    <div class="col-xs-12 no-padding text-uppercase mo-form-header">
                        sales info.
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-12 {{$hideBorderBottom != ''?'border-bottom':''}} padding-top-24 padding-bottom-8">
                            <label for="interior-electronics" class="text-capitalize">vehicle*</label>
                            <span class="{{$hideViewField or ''}} text-capitalize float-right ">{{$sales_status_selected_value or ''}}</span>
                            <span class="{{$hideEditField or ''}}"><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                            <select class="form-control text-capitalize {{$hideEditField or ''}}" name="vehicle" id="vehicle">
                                <option selected disabled>Vehicle list</option>
                                @if (isset($salesInfoStatuses))
                                    @if ($status == 10 || $status == 1 || $status == 2)
                                        @foreach ($salesInfoStatuses as $salesInfoStatus)
                                            <option value="{{$salesInfoStatus->code}}">{{$salesInfoStatus->value}}</option>
                                        @endforeach
                                    @else
                                        @foreach ($salesInfoStatuses as $salesInfoStatus)
                                            <option {{isset($salesInfo) && $salesInfo->sale_status == $salesInfoStatus->code ?'selected':''}} value="{{$salesInfoStatus->code}}">{{$salesInfoStatus->value}}</option>
                                        @endforeach
                                    @endif
                                @endif
                            </select>
                        </div>
                        <div class="col-xs-12 {{$hideBorderBottom != ''?'border-bottom':''}} padding-top-20 padding-bottom-8">
                            <label for="interior-electronics" class="text-capitalize">valuation price*</label>
                            <span class="{{$hideViewField or ''}} text-capitalize float-right ">${{$salesInfo->valuation_price or 0}}</span>
                            {{--<div class="{{$hideViewField or ''}}"></div>{{$salesInfo->valuation_price}}--}}
                            <div class="{{$hideViewField or ''}}"></div>
                            <input type="number" pattern="[0-9]" class="form-control {{$hideEditField or ''}}" name="valuation_price" id="valuation-price" value="{{$salesInfo->valuation_price or ''}}" maxlength="10">
                        </div>
                        <div class="col-xs-12 {{$hideBorderBottom != ''?'border-bottom':''}} padding-top-20 padding-bottom-8">
                            <label for="interior-electronics" class="text-capitalize">maximum valuation price*</label>
                            <span class="{{$hideViewField or ''}} text-capitalize float-right ">${{$salesInfo->max_valuation_price or 0}}</span>
                            <div class="{{$hideViewField or ''}}"></div>
                            <input type="number" pattern="[0-9]*" class="form-control {{$hideEditField or ''}}" name="maximum_valuation_price" id="maximum-valuation-price" value="{{$salesInfo->max_valuation_price or ''}}">
                        </div>
                        <div class="col-xs-12 {{$hideBorderBottom != ''?'border-bottom':''}} padding-top-20 padding-bottom-8">
                            <label for="interior-electronics" class="text-capitalize">price offered</label>
                            <span class="{{$hideViewField or ''}} text-capitalize float-right ">${{$offer_price or 0}}</span>
                            <div class="{{$hideViewField or ''}}"></div>
                            <input type="number" pattern="[0-9]*" class="form-control {{$hideEditField or ''}}" name="offer_price" id="offer-price" value="{{$offer_price or ""}}">
                        </div>
                    </div>
                </div>
            @endif

            <!-- PPSR -->
            <div class="mo-row">
                @if(in_array("ppsr_get",$permissions))
                    <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-top-24 padding-bottom-24 text-uppercase mo-form-header">
                            <span class="col-xs-2">PPSR CHECK</span>
                            <span class="col-xs-10">
                                <img src="{{ asset('/images/main/ppsr-website-logo.png') }}" alt="PPSR Check" class="ppsr-logo" />
                            </span>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-8 border-top padding-top-29">
                            <div class="col-xs-7 col-md-7 padding-top-5">
                                <label>PPSR Check will send the result to your email</label>
                            </div>
                            <div class="col-xs-1 col-md-1 float-right margin-right-60 {{$disableEditButton or ''}}">
                                <a href="#mo-ppsr-check-popup" class="" id="mo-ppsr-check-open-popup">
                                    <input type="button" class="ppsr-check" value="Check PPSR" />
                                </a>
                            </div>

                        </div>
                    </div>
                @endif
            </div>
            <!-- END PPSR -->

            <!-- CONFIRM VALUATION -->
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding {{$hideEditField or ''}}">
                <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-top-24 padding-bottom-24 text-uppercase mo-form-header">
                    confirm valuations details
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-5">
                    @if ($status == 10 || $status == 1 || $status == 2)
                        <input type="checkbox" id="all-detail" name="all_detail" class="all-detail mo-round-check">
                    @else
                        <input type="checkbox" id="all-detail" name="all_detail" class="all-detail mo-round-check" checked disabled>
                    @endif        
                    <label for="all-detail" class="text-capitalize"><span></span>all details are entered accurately*</label>
                    {{--<div id="messages" class="hidden">
                        <span><img src="{{asset('/images/svg/main/ic-warning.svg')}}"><strong>Please ensure all details are accurately</strong></span>
                    </div>--}}
                        <div id="errorToShow"></div>
                </div>
            </div>

            <span><input type="hidden" id="version-locker-user-id" name="version_locker_user_id" value="{{$version_locker_user_id or ''}}"></span>
            
            <div class="form-group col-xs-12 col-sm-12 col-md-12 no-padding padding-top-24">
                @if (session("info"))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Error!</strong> {{session("info")}}
                    </div>
                @endif
                @if ($status == 6 && isset($enable_receive_vehicle))
                    <a href="#mo-receive-vehicle-popup" class="open-popup-link">
                        <input type="submit" class="mo-btn text-uppercase {{$hideEditField or ''}} width-210" value="Receive Vehicle">
                    </a>
                @else
                    <input type="hidden" name="valuation_status" value="{{$status}}">
                    <input type="submit" name="submit_button" id="mo-btn-valuation-summary" class="mo-btn {{($status == 10 || $status == 1 || $status == 2) ? 'mo-btn-complete-valuation' : ''}} text-uppercase {{$hideEditField or ''}}" value="{{($status == 10 || $status == 1 || $status == 2) ? 'Complete Valuation' : 'Save' }}" {{($status == 10 || $status == 1 || $status == 2) ? 'disabled' : '' }}>
                @endif
            </div>
                <input type="hidden" value="{{strpos(Request::path(), 'mo-admin') === 0?'mo-admin':''}}" name="mo_admin">
                <div class="mo-loading-image hidden">Loading...</div>
        </form>
    </div><!-- mo wrapper -->
    {{--End - moto Content Valuation Summary--}}

    {{----------------------------------------------------------- EXTERNAL OFFERING POPUP ----------------------------------------------------------------------}}
    <div id="mo-external-offering-popup" class="mo-white-popup mfp-hide max-width-688 overflow-scroll">
        <form action="" method="post" id="mo-external-offering-form">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $valuation_id }}" name="valuation-id" />
            <!-- ROW 1 -->
            <div class="mo-row padding-bottom-20">
                <div class="col-xs-12 no-padding">

                    <span class="text-capitalize mo-popup-heading col-xs-11 force-no-padding">Offer Request</span>

                </div> 
            </div>
            <!-- END ROW 1 -->

            <hr class="mo-form-hr padding-bottom-48" />

            <!-- ROW 2 -->
            <div class="mo-row no-padding">
                <div class="form-group col-xs-12 col-sm-8">
                    <label for="external-offer-arrival-date" class="text-capitalize">Expected Arrival Date*</label>
                    <div class="input-group date" id="external-offer-arrival-date-picker">
                        <input type="text" class="form-control" id="external-offer-arrival-date" name="external-offer-arrival-date" placeholder="YYYY-MM-DD" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <!-- END ROW 2 -->

            <!-- ROW 3 -->
            <div class="mo-row no-padding">
                <div class="form-group col-xs-12 col-sm-8">
                    <label>Send to:</label><br/>

                    <input type="radio" class="mo-round-check margin-left--5" id="external-offer-send-to-public" name="external-offer-send-to" value="external-offer-send-to-public" checked/>
                    <label for="external-offer-send-to-public" class="text-capitalize padding-top-8"><span class="margin-bottom-2"></span>Public</label><br/>

                    <input type="radio" class="mo-round-check margin-left--5" id="external-offer-send-to-group" name="external-offer-send-to" value="external-offer-send-to-group" />
                    <label for="external-offer-send-to-group" class="text-capitalize padding-top-24"><span class="margin-bottom-2"></span>Group</label><br/>

                    <!-- check box -->

                    @if(isset($external_offer_group))
                        <div class="mo-external-group-wrapper hidden" id="mo-external-group">
                        @foreach ($external_offer_group as $group)
                            <input type="checkbox" class="external-offer-group-check mo-square-check margin-left--5" id="external-offer-group-{{$group->id}}" name="external-offer-group[]" value="{{$group->id}}"/>
                            <label for="external-offer-group-{{ $group->id }}" class="text-capitalize padding-top-24 padding-left-30"><span class="margin-bottom-2"></span>{{$group->name}}</label><br/>
                        @endforeach
                        </div>
                    @endif
                    <!-- end check box -->

                </div>
            </div>
            <!-- END ROW 3 -->

            <hr class="mo-form-hr margin-top-48" />

            <!-- ROW 4 -->
            <div class="mo-row padding-top-24 col-xs-12">
                <div class="text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="CANCEL">
                    <input type="button" class="mo-btn text-uppercase" value="SUBMIT" id="external-offering-send">
                </div>
                
            </div>
            <!-- END ROW 4 -->
            <span class="mo-loading-image hidden"></span>
        </form>
    </div>
    {{----------------------------------------------------------- END EXTERNAL OFFERING POPUP -----------------------------------------------------}}

    {{----------------------------------------------------------- PPSR CHECK ----------------------------------------------------------------------}}
    <div id="mo-ppsr-check-popup" class="mo-white-popup mfp-hide max-width-688">
        <form action="" method="post" id="mo-ppsr-check-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <div class="col-xs-12 no-padding">
                    <span class="text-capitalize mo-popup-heading col-xs-11 force-no-padding">Check PPSR</span>
                    {{--<span class="col-xs-1">
                        <img src="{{asset('/images/main/ppsr-status.png')}}" />
                    </span>--}}
                </div> 
            </div>
            <hr class="mo-form-hr padding-bottom-48" />
            <div class="mo-row">

                <div class="col-xs-12 ppsr-document no-padding">
                    PPSR charges of $4 apply?
                </div>
                
            </div>
            <hr class="mo-form-hr margin-top-48" />
            <div class="mo-row padding-top-24">
                <div class="col-xs-12 text-right">
                            <div class="col-xs-6 col-sm-10">
                                <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="NO">
                            </div>
                            <div class="col-xs-6 col-sm-2">
                                <input type="button" class="mo-btn text-uppercase" value="YES" id="ppsr-yes" data-vin="{{$vin}}" data-year="{{$year}}">
                            </div>  
                            <div class="col-xs-6 col-sm-2"> 

                                <div class="mo-btn-loading hide">
                                    <img src="{{ asset('/images/main/ajax-loader.gif') }}" class="mo-btn-loading" />
                                </div>

                            </div>

                </div>
            </div>
            
        </form>
    </div>
    {{----------------------------------------------------------- END PPSR CHECK ------------------------------------------------------------------}}

    {{----------------------------------------------------------- RECEIVE VEHICLE POPUP -----------------------------------------------------------}}
    <div id="mo-receive-vehicle-popup" class="mo-white-popup mfp-hide">
        <form action="{!! url('receive-vehicle/'.$valuation_id) !!}" method="post" id="mo-trade-in-vehicle-form">
        <!-- <form class="mo-popup-form" action="{!! url('upload-image') !!}" method="post" id="mo-recieve-vehicle-form" > -->
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">receive vehicle</span>
            </div>
            <hr class="mo-form-hr padding-bottom-24" />
            <div class="mo-row">
                <div class="col-xs-12">
                    <div class="vertical-center col-xs-12 border-bottom border-top padding-top-10 padding-bottom-8">
                        <label class="col-xs-8 no-padding">PPSR Check</label>
                        <span class="col-xs-4 no-padding">
                            <input type="checkbox" class="ppsr-check mo-round-check" id="ppsr-check" name="ppsr_check">
                            <label class="float-right" for="ppsr-check"><span></span></label>
                        </span>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="vertical-center col-xs-12 border-bottom padding-top-10 padding-bottom-8">
                        <label class="col-xs-6 no-padding">PPSR Website</label>
                        <span class="col-xs-6 no-padding">
                            <a href="https://www.ppsr.gov.au/" target="_blank">
                                <input type="button" class="float-right mo-btn text-uppercase mo-black-btn" value="open website">
                            </a>
                        </span>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-8 padding-top-10 padding-bottom-20">
                        <label>Allocate dealer stock number*</label>
                        <input type="text" class="form-control" id="stock-number" name="stock-number">
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-8 padding-top-10 padding-bottom-20">
                        <label>Location*</label>
                        <div class="mo-dropdown-wrapper">
                            @if (isset($locations) && !empty($locations))
                                <span class="mo-icon-swipe glyphicon glyphicon-menu-down"></span>
                                <select class="form-control" name="receive-vehicle-location" id="receive-vehicle-location">
                                    <option selected disabled>-- Locations list --</option>
                                    @foreach($locations as $location)
                                        <option value="{{$location->id}}">{{$location->name}} {{isset($location->state_text) ? ' - '.$location->state_text : ''}}</option>
                                    @endforeach
                                </select>
                            @else
                                <span class="col-xs-12 no-padding">There is no Location at this time or you don't have access into Location list</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-top-24">
                <div class="col-xs-12">
                    <div class="col-xs-12 text-right">
                        <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                        <input type="submit" class="mo-btn text-uppercase {{$hideEditField or ''}} width-210" id="mo-receive-vehicle" value="receive vehicle">
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{----------------------------------------------------------- RECEIVE VEHICLE POPUP -----------------------------------------------------------}}

    {{----------------------------------------------------------- end SUMMARY -----------------------------------------------------------}}

@endsection