@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            {{ $header or 'Dealership'}} > <a href="{!! url('dealership/external-offer-groups') !!}" class="underline-text">{{$breadcrumb or ''}}</a> > <span class="yellow-text">{{ $action or 'New'}}</span>
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
    	<div class="col-xs-12 no-padding">
    		<div class="col-xs-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                location details
            </div>
            <form action="{!! url('dealership/create-external-offer-group') !!}" method="post" id="mo-external-group-form">
                {{ csrf_field() }}
                <input type="hidden" id="mo-group-id" value="{{isset($groupInfo->externalGroupId)?$groupInfo->externalGroupId:''}}">
                <div class="col-xs-12 alert alert-success hidden" role="alert">
                    You've saved External Offer Group successfully!
                    <a class="mo-close-message close">&times;</a>
                </div>
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-5 padding-bottom-24">
                        <label for="group-name" class="text-capitalize">group name*</label>
                        <input type="text" class="form-control" id="group-name" name="group_name" value="{{isset($groupInfo->groupName)?$groupInfo->groupName:''}}">
                    </div>
                    <div class="col-xs-12 col-sm-5 float-right padding-bottom-24">
                        <label for="group-desc" class="text-capitalize">group description</label>
                        <input type="text" class="form-control" id="group-desc" name="group_desc" value="{{isset($groupInfo->groupDescription)?$groupInfo->groupDescription:''}}">
                    </div>
                </div> <!-- Row 1 -->

                {{--<div class="mo-row pos-relative">
                    <div class="col-xs-12 col-sm-5 pos-absolute bottom-zero">
                        <label for="name" class="text-capitalize">dealers/location*</label>
                    </div>
                    <div class="col-xs-12 col-sm-5 float-right padding-bottom-24">
                        <label for="mo-txt-search" class="text-capitalize">search dealers/location</label>
                        <div class="col-xs-12 no-padding mo-search-wrapper" id="mo-search-dealer">
                            <input type="text" placeholder="Search" id="mo-txt-search">
                            <span class="glyphicon glyphicon-search mo-txt-search cursor-pointer"></span>
                        </div>
                    </div>
                </div>--}} <!-- Row 2 -->
                {{--<span class="fa fa-exchange" aria-hidden="true"></span>--}}
                <div class="mo-row">
                    <div class="col-xs-12">
                        <select id='location-select' name="location_select" multiple='multiple'>
                            @if(isset($dealersList) && !empty($dealersList))
                                @foreach($dealersList as $dealer)
                                    <optgroup label='{{$dealer->dealerName}}'>
                                        @if(!empty($dealer->locationList))
                                            @foreach($dealer->locationList as $location)
                                                <option {{ !empty($location_ids) && in_array($location->locationId, $location_ids)?'selected':'' }} value='{{$location->locationId}}'>{{$location->locationName}}</option>
                                            @endforeach
                                        @endif
                                    </optgroup>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                <div class="mo-row padding-top-100">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-top-24">
                        @if (strtolower($action) == "new")
                            <input type="submit" class="mo-btn text-uppercase" value="create" id="mo-create-external-group" name="submit_button">
                        @elseif(strtolower($action) == "edit")
                            <input type="submit" class="mo-btn text-uppercase" value="update" id="mo-update-external-group" name="submit_button">
                        @endif
                    </div>
                </div>
            </form>
            <div class="mo-loading-image hidden">Loading&#8230;</div>
    	</div>
    </div>
@endsection

