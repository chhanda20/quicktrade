@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            {{ $header or 'Dealerships'}} > <span class="yellow-text">{{ $breadcrumb or 'Settings'}}</span>
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding ">
            <form action="" method="post" id="mo-dealership-settings">
                {{ csrf_field() }}
                <div class="col-xs-12 col-sm-6 alert alert-success hidden" role="alert">
                    You've set setting successfully!
                    <a class="mo-close-message close">&times;</a>
                </div>
                <div class="col-xs-12 col-sm-6 alert alert-danger hidden" role="alert">
                    Saved settings failed! Please try again.
                    <a class="mo-close-message close">&times;</a>
                </div>
                <div class="col-xs-12 col-sm-6 alert alert-danger alert-access-denied hidden" role="alert">
                    Saved settings failed! You don't have access to save these settings.
                    <a class="mo-close-message close">&times;</a>
                </div>
                <div class="mo-row">
                    <div class="col-xs-12 no-padding col-sm-6 col-md-6 padding-bottom-24">
                        <input type="checkbox" class="mo-round-check" id="head-valuer-approval" name="head_valuer_approval"
                                {{isset($dealerSettings->head_valuer_approval) && $dealerSettings->head_valuer_approval == 1?'checked':''}}>
                        <label for="head-valuer-approval" class="text-capitalize cursor-pointer"><span></span>Require Head Valuer Approval</label>
                    </div>
                </div> <!-- Row 1 -->

                <div class="mo-row">
                    <div class="col-xs-12 no-padding padding-bottom-24">
                        <input type="checkbox" class="mo-round-check" id="receive-external-offers" name="receive_external_offers"
                                {{isset($dealerSettings->receive_external_offer) && $dealerSettings->receive_external_offer == 1?'checked':''}}>
                        <label for="receive-external-offers" class="text-capitalize cursor-pointer"><span></span>Receive External Offers</label>
                    </div>
                </div> <!-- Row 2-->

                <div class="mo-row">
                    <div class="col-xs-12 no-padding padding-bottom-24">
                        <label for="test-drive-terms-conditions">Test Drive Terms & Conditions</label>
                        <textarea class="form-control" name="test_drive_terms" id="test-drive-terms-conditions" rows="7">{{$dealerSettings->test_drive_terms}}</textarea>
                    </div>
                </div> <!-- Row 3-->

                <div class="mo-row">
                    <div class="col-xs-12 no-padding padding-bottom-30">
                        <input type="submit" class="mo-btn text-uppercase" value="submit" id="mo-submit-dealership-setting">
                    </div>
                </div>
                <div class="mo-loading-image hidden">Loading&#8230;</div>
            </form>
        </div>
    </div>

    {{--start - SETTINGS ACTION CONFIRMATION POPUP--}}
    <div id="mo-settings-action-confirm-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            {{ csrf_field() }}

            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">warning</span>
            </div>

            <hr class="mo-form-hr" />

            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <div class="col-xs-12">Please notice that some pending valuations waiting for approval will be move to Complete List</div>
                </div>
            </div>

            <hr class="mo-form-hr" />

            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="close">
                </div>
            </div>

        </form>
    </div>
    {{--End - SETTINGS ACTION CONFIRMATION POPUP--}}
@endsection