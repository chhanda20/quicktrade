@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            {{$header or "Dealership"}} > <a href="{!! url('dealership/roles'); !!}" class="underline-text">{{$breadcrumb or 'Roles'}}</a> > <span class="yellow-text">{{$action or "New"}}</span>
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                roles details
            </div>

            @if(Session::has("error_message"))
                @if(session("error_message") == "role_code_duplicate")
                    <div class="col-xs-12 col-sm-6 alert alert-danger"><span></span>Role Name Duplicate
                        <a class="mo-close-message close">&times;</a>
                    </div>
                @endif
            @endif
            
            @if(isset($role_detail))
            <form action="{!! url('/dealership/update-new-role') !!}/{{$role_detail->id}}" method="post" id="mo-create-new-role-form">
            @else
            <form action="{!! url('/dealership/create-new-role'); !!}" method="post" id="mo-create-new-role-form">
            @endif

                {{csrf_field()}}

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="name" class="text-capitalize">name*</label>
                        <input type="text" class="form-control" id="role-name" name="role_name"
                               value="{{isset($role_detail->name)? $role_detail->name:''}}">
                    </div>
                </div>

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-12 col-md-12 mo-category-list">
                        <div><label for="permissions" class="text-capitalize">permissions*</label></div>
                        {{--{{var_dump($permission_detail)}}--}}
                        @if(isset($permissions))
                            <?php $count = 0; ?>
                            @foreach($permissions as $permission)
                                <div class="col-xs-12 col-sm-12 col-md-4 {{$count % 3 === 0?"":"border-grey-left"}}">
                                    <input type="checkbox" class="mo-square-check mo-permission" id="permission_{{$permission->id}}" value="{{$permission->id}}" name="permissions_list[]"
                                           {{isset($permission_detail) && in_array((int)$permission->id, $permission_detail) ? "checked": ''}}/>
                                    <label for="permission_{{$permission->id}}" class="text-capitalize mo-category"><span></span>{{$permission->name}}</label>
                                </div>
                                    <?php $count++; ?>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div id="errorToShow"></div>

                <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-top-24 text-uppercase mo-form-header">
                    menu access
                </div>
                <div class="mo-row padding-top-10 mo-category-list">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        @if(isset($menu_group))
                            @foreach($menu_group as $menu)
                                <div class="mo-row padding-top-24"><label for="" class="text-capitalize">{{$menu["group_name"]}}</label></div>
                                @foreach($menu["menu_items"] as $menu_item)
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <input type="checkbox" class="mo-square-check mo-item" id="menu_item_{{$menu_item->id}}" data-id="{{$menu_item->id}}" name="menu_items_list[]" value="{{$menu_item->id}}"
                                            {{isset($menu_role_detail) && in_array($menu_item->id, $menu_role_detail) ? "checked" : ""}}>
                                        <label for="menu_item_{{$menu_item->id}}" class="text-capitalize mo-category"><span></span>{{$menu_item->name}}</label>
                                    </div>
                                @endforeach
                            @endforeach
                        @endif
                    </div>
                </div>
                <div id="error_To_Show"></div>

                <input type="hidden" id="mo-role-id" name="mo_role_id" value="{{isset($role_detail->id)?$role_detail->id:''}}">
                <div class="mo-row padding-top-20">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        @if(strtolower($action) == 'new')
                        <input type="submit" class="mo-btn text-uppercase" id="dealer-save" name="dealer_save" value="save">
                        @elseif(strtolower($action) == 'edit')
                        <input type="submit" class="mo-btn text-uppercase" id="dealer-update" name="dealer_update" value="update">
                        @endif
                    </div>
                </div>
                <div class="mo-loading-image hidden">Loading...</div>
            </form>
        </div>
    </div>

@endsection