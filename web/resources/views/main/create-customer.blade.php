@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <a href="{!! url('customers'); !!}">{{ $header or 'Customers' }}</a> > {{ $breadcrumb or 'Add Customer' }}
        </div>
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - moto Create Form Section--}}
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                personal details
            </div>
            <div class="alert alert-danger alert-dismissible hidden" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Error!</strong> {{$info or ''}}.
            </div>
            <form action="{!! url('create-customer'); !!}" method="post" id="create-customer-form">
                {{ csrf_field() }}
                <div class="mo-row">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="title" class="text-capitalize">title</label>
                        <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}" ></span>
                        <select class="form-control text-capitalize" id="title" name="title">
                            <option selected disabled>title list</option>
                            @if (isset($titles))
                                @foreach ($titles as $title)
                                    <option value="{{$title->code}}">{{$title->value}}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="gender" class="text-capitalize">gender</label>
                        {{--<span><img class="imgicon" src="images/main/icon-swipe-right-copy-4.png"></span>--}}
                        <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}" ></span>
                        <select class="form-control text-capitalize" id="gender" name="gender">
                            <option selected disabled>gender list</option>

                            @if (isset($genders))
                                @foreach ($genders as $gender)
                                    <option value="{{$gender->code}}">{{$gender->value}}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div> <!-- row 1 -->

                <div class="mo-row">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="first-name" class="text-capitalize">first name*</label>
                        <input type="text" class="form-control" id="first-name" name="first_name">
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="last-name" class="text-capitalize">last name</label>
                        <input type="text" class="form-control" id="last-name" name="last_name">
                    </div>
                </div> <!-- row 2 -->

                <div class="mo-row">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="company" class="text-capitalize">company</label>
                        <input type="text" class="form-control" id="company" name="company">
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="address1" class="text-capitalize">address 1*</label>
                        <input type="text" class="form-control" id="address1" name="address1">
                    </div>
                </div> <!-- row 3 -->

                <div class="mo-row">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="address2" class="text-capitalize">address 2</label>
                        <input type="text" class="form-control" id="address2" name="address2">
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="suburb" class="text-capitalize">suburb*</label>
                        <input type="text" class="form-control" id="suburb" name="suburb">
                    </div>
                </div> <!-- row 4 -->

                <div class="mo-row">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="postcode" class="text-capitalize">postcode</label>
                        <input type="number" class="form-control" id="postcode" name="postcode" >
                    </div>

                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="state" class="text-capitalize">state*</label>
                        {{--<span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>--}}
                        <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}" ></span>
                        <select class="form-control text-capitalize" id="state" name="state" value="">
                            <option selected disabled>state list</option>
                        </select>
                    </div>
                    <!--  End -->
                </div> <!-- row 5 -->

                <div class="mo-row">
                    <!-- Add Country Field -->
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="title" class="text-capitalize">Country*</label>
                        <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}" ></span>
                        <select class="form-control text-capitalize" id="country" name="country" disabled="true">
                            <option selected disabled>country list</option>

                            @if (isset($country_list))
                                @foreach ($country_list as $country)
                                    <option {{$country->id == 13 ? "selected":" "}} value="{{$country->id}}">{{$country->name}}</option>
                                @endforeach
                            @endif

                        </select>
                        <input type="hidden" id="input-country-id" name="input_country_id">
                        <input type="hidden" value="{{isset($country_list) ? json_encode($country_list) : null}}" name="country-list" id="country-list" />
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="mobile" class="text-capitalize">mobile number*</label>
                        <input type="text" class="form-control" id="mobile" name="mobile" maxlength="20">
                    </div>
                </div> <!-- row 6 -->

                <div class="mo-row">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="email" class="text-capitalize">email address*</label>
                        <input type="email" class="form-control" id="email" name="email">
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <!-- <label for="privacy" class="text-capitalize">privacy</label>
                        <input type="text" class="form-control" id="privacy" name="privacy"> -->
                        <label for="state" class="text-capitalize">privacy*</label>
                        {{--<span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>--}}
                        <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}" ></span>
                        <select class="form-control text-capitalize" id="privacy" name="privacy" value="">
                            <option selected disabled>Please select</option>
                            @if (isset($privacy_flag))
                                @foreach ($privacy_flag as $privacy)
                                    <option value="{{$privacy->code}}" class="text-capitalize">{{$privacy->value}}</option>
                                    @endforeach
                                    @endif
                                            <!-- <option value="1" class="text-capitalize">consent given</option> -->
                                    <!-- <option value="2" class="text-capitalize">unconsent given </option> -->
                                    <!-- <option value="3" class="text-capitalize">not set</option> -->
                        </select>
                    </div>
                </div> <!-- row 7 -->

                <div class="mo-row">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="note" class="text-capitalize">note</label>
                        <textarea class="form-control" rows="3" id="note" name="note"></textarea>
                    </div>
                </div> <!-- row 8 -->

                <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-top-24 text-uppercase mo-form-header">
                    other details
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6 padding-top-24">
                    <label for="new-used-vehicle">New or Used Vehicle</label>
                    {{--<span><img class="imgicon" src="images/main/icon-swipe-right-copy-4.png"></span>--}}
                    <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}" ></span>
                    <select class="form-control text-capitalize" id="new-used-vehicle" name="new_used_vehicle">
                        <option selected disabled>New or Used Vehicle List</option>
                        @if (isset($vehicle_type))
                            @foreach ($vehicle_type as $vehicle)
                                <option value="{{$vehicle->code}}">{{$vehicle->value}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6 padding-top-24">
                    <label for="contact-method" class="text-capitalize">Preferred Contact Method</label>
                    {{--<span><img class="imgicon" src="images/main/icon-swipe-right-copy-4.png"></span>--}}
                    <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}" ></span>
                    <select class="form-control text-capitalize" id="contact-method" name="contact_method">
                        <option selected disabled>Preferred Contact Method List</option>
                        @if (isset($contact_method))
                            @foreach ($contact_method as $method)
                                <option value="{{$method->code}}">{{$method->value}}</option>
                            @endforeach
                        @endif

                    </select>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-12 padding-top-36 text-center">
                    <input type="submit" class="mo-btn text-uppercase" value="done" id="mo-button-create-customer">
                </div>
                <div class="mo-loading-image hidden">Loading...</div>
            </form>
        </div>
    </div>
    {{--End - moto Create Form Section--}}
@endsection