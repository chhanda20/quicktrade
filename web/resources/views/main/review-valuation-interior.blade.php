@section('review-interior')

    {{----------------------------------------------------------- start INTERIOR -----------------------------------------------------------}}

    {{--Start - moto Interior Content Section--}}
    <div class="hidden margin-top-32 mo-block-list" id="mo-interior-block">
        <div class="mo-row mo-form-wrapper">
			{{--<div class="col-xs-12 no-padding" id="mo-empty-holder"></div>--}}
            <div class="col-xs-12 vertical-center" id="mo-empty-item-text-holder">
                <div id="mo-empty-item-text" class="{{isset($interior_item_list) ? 'hide' : ''}}">
                    There are no interior/electronics items to display.<br>Add inspection items by clicking the NEW icon
                </div>
            </div>
            <div class="col-xs-12 no-padding">
                <div class="col-xs-8"></div>
                <div class="col-xs-4">
                    <div class="mo-round-btn2 right-0 {{$hideEditField or ''}}"><a href="#mo-edit-inspection-item-popup" class="open-popup-link mo-add-new-item" id="">NEW</a></div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 no-padding" id="mo-prev-next-button">
            <button type="button" id="mo-prev-interior" class="mo-prev text-uppercase float-left">
                <span class="glyphicon glyphicon-menu-left text-uppercase"></span><span class="mo-span-txt">previous</span>
            </button>
            <button type="button" id="mo-next-interior" class="mo-next text-uppercase float-right">
                <span class="mo-span-txt">next</span><span class="glyphicon glyphicon-menu-right text-uppercase"></span>
            </button>
        </div>
        <div class="col-xs-12 no-padding" id="mo-estimated-cost">
            <ul class="mo-main-content no-padding">
                <li data-toggle="collapse" data-target="#mo-interior-sub-content" class="collapsed active">
                    <a href="javascript:void(0);">
                        <div id="mo-heading" class="vertical-center col-xs-12 no-padding">
                            <div class="mo-estimated-cost-wrapper col-xs-12 no-padding">
                                <div class="col-xs-12 col-sm-8 no-padding">
                                    Total Estimated Cost
                                </div>
                                <div class="col-xs-12 col-sm-4 no-padding text-right">
                                    <span class="col-xs-5 col-sm-6 no-padding">$<span id="mo-interior-total-estimate-cost">{{number_format($interior_total_cost, 2)}}</span></span>
                                    <span class="col-xs-2 col-sm-4 padding-lr5 mo-expand-hide-label">Expand</span>
                                    <span class="col-xs-5 col-sm-2 no-padding mo-icon-swipe glyphicon glyphicon-menu-down"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                    <ul class="mo-sub-content collapse" id="mo-interior-sub-content">

                        @if (isset($interior_item_list))
                            <input type="hidden" value="{{json_encode($interior_item_list)}}" id="mo-interior-item-data">
                            {{--<li>
                                <div class="col-xs-12 no-padding border-bottom padding-tb15 font-size16">
                                    
                                    <div class="col-xs-12 col-sm-8 no-padding">
                                        General Reconditioning Costs
                                    </div>
                                    <div class="col-xs-12 col-sm-4 text-right no-padding">
                                        <span class="col-xs-6 no-padding font-avenir-demi">
                                            $<span class="font-size16">0</span>
                                        </span>
                                        <span class="col-xs-6 no-padding"></span>
                                    </div>
                                </div>
                            </li>--}}
                            @foreach ($interior_item_list as $items)
                                @foreach($items->items as $item)
                                    <li class="{{$items->section_type}}">

                                        <div class="col-xs-12 padding-tb10 border-bottom no-padding mo-cost-estimate-row" id="{{$item->inspection_item_id}}">
                              
                                                <div class="col-xs-12 col-sm-8 no-padding name">
                                                    {{$interior_item_no++}}.{{$item->item}}
                                                    <div class="padding-top-5 font-size14 font-grey-color font-style-italic">{{$item->note}}</div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 no-padding text-right">
                                                    <label class="col-xs-6 no-padding cursor-pointer {{!in_array("valuation_perform",$permissions)?'disabled-div':''}}" id="item_cost_{{$items->section_type}}_{{$item->inspection_item_id}}">
                                                        <span class="{{$disableEditField or ''}}">$</span><span class="mo-cost-number {{$disableEditField or ''}}">{{number_format($item->repair_cost, 2)}}</span>
                                                        <span class='mo-cost-number-input hide'><input type='number' name="estimate-cost" min="0" max="9999999999" maxlength = "13" oninput="maxLengthCheck(this)" value="" /></span>
                                                    </label>
                                                    <a href="#mo-edit-inspection-item-popup" class="col-xs-4 padding-lr5 open-popup-link">
                                                        <span class="mo-view-edit-label">View/Edit</span>
                                                    </a>
                                                    <span class="col-xs-2 no-padding">
                                                        <a href="#mo-confirm-delete-exterior-popup" class="open-popup-link float-left padding-left-5 {{$hideEditField or ''}}">
                                                            <img src="{{ asset('images/svg/main/icon-remove.svg') }}" alt="Remove Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-remove.png') }}'" class="mo-delete-item">
                                                        </a>
                                                    </span>

                                                </div>
                                                
                                        </div>

                                    </li><!-- ROW 1 -->
                                @endforeach
                            @endforeach
                        @endif
                    </ul>
                </li>

            </ul>
        </div>
    </div>
    {{--End - moto Interior Content Section--}}

    {{----------------------------------------------------------- end INTERIOR -----------------------------------------------------------}}

@endsection