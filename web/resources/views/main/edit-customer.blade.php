@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <a href="{!! url('customers'); !!}">{{ $header or 'Customers' }}</a> > {{ $breadcrumb or 'Edit Customer' }}
        </div>
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - moto Edit Form Section--}}
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                personal details
            </div>
            <form action="/edit-customer" method="post" id="create-customer-form">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{isset($id) ? $id : null}}" />
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="title" class="text-capitalize">title</label>
                    <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                    <select class="form-control text-capitalize" id="title" name="title">
                        <option selected disabled>title list</option>

                        {{--
                        <option value="mr" {{$title == "mr" ? "selected" : null}}>mr</option>
                        <option value="ms" {{$title == "ms" ? "selected" : null}}>ms</option>
                        <option value="mrs" {{$title == "mrs" ? "selected" : null}}>mrs</option>
                        --}}

                        @if (isset($titles))
                        @foreach ($titles as $title)
                            <option value="{{$title->code}}" {{$title->code == $title_code ? "selected" : ""}}>{{$title->value}}</option>
                        @endforeach
                        @endif

                    </select>
                </div>

                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="gender" class="text-capitalize">gender</label>
                    <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                    <select class="form-control text-capitalize" id="gender" name="gender" value="male">
                        <option selected disabled>gender list</option>

                        {{--
                        <option value="1" {{$gender == 1 ? "selected" : null}}>male</option>
                        <option value="2" {{$gender == 2 ? "selected" : null}}>female</option>
                        <option value="3" {{$gender == 3 ? "selected" : null}}>other</option>
                        --}}

                        @if (isset($genders))
                        @foreach ($genders as $gender)
                            <option value="{{$gender->code}}" {{$gender->code == $gender_code ? "selected" : ""}}>{{$gender->value}}</option>
                        @endforeach
                        @endif

                    </select>

                </div>
                <div class="mo-row">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="first-name" class="text-capitalize">first name*</label>
                        <input type="text" class="form-control" id="first-name" name="first_name" maxlength="100"  value="{{isset($first_name) ? $first_name : null}}">
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="last-name" class="text-capitalize">last name</label>
                        <input type="text" class="form-control" id="last-name" name="last_name"  value="{{isset($last_name) ? $last_name : null}}">
                    </div>    
                </div>
                
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="company" class="text-capitalize">company name</label>
                    <input type="text" class="form-control" id="company" name="company" value="{{isset($company_name) ? $company_name : null}}">
                </div>
                
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="company-type" class="text-capitalize">company type</label>
                    <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                    <select class="form-control text-capitalize" id="company-type" name="company_type" value="{{isset($company_type) ? $company_type : null}}">
                        <option selected disabled>company type list</option>

                        @if (isset($company_type_list))
                        @foreach ($company_type_list as $company)
                            <option value="{{$company->code}}" {{$company->code == $company_type_code ? "selected" : ""}}>{{$company->value}}</option>
                        @endforeach
                        @endif

                    </select>
                    
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="abn" class="text-uppercase">abn</label>
                    <input type="text" class="form-control" id="abn" name="ABN" value="{{isset($ABN) ? $ABN : null}}">
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="acn" class="text-uppercase">acn</label>
                    <input type="text" class="form-control" id="acn" name="ACN" value="{{isset($ACN) ? $ACN : null}}">
                </div>

                <div class="mo-row">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="tax-type" class="text-capitalize">tax type</label>
                        <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                        <select class="form-control text-capitalize" id="tax-type" name="tax_type" value="{{isset($tax_type) ? $tax_type : null}}">
                            <option selected disabled>tax type list</option>
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="suburb" class="text-capitalize">suburb*</label>
                        <input type="text" class="form-control" id="suburb" name="suburb" value="{{isset($suburb) ? $suburb : null}}">
                    </div>    
                </div>
                
                <div class="mo-row">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="address1" class="text-capitalize">address 1*</label>
                        <input type="text" class="form-control" id="address1" name="address1" value="{{isset($address1) ? $address1 : null}}">
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="address2" class="text-capitalize">address 2</label>
                        <input type="text" class="form-control" id="address2" name="address2" value="{{isset($address2) ? $address2 : null}}">
                    </div>
                </div>

                <div class="mo-row">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="state" class="text-capitalize">state*</label>
                        <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                        <select class="form-control text-capitalize" id="state" name="state" value="{{isset($state) ? $state : null}}">

                        <option selected disabled>state list</option>

                        @if (isset($state_list))
                            <!-- <option value="{{$state_list->id}}" selected>{{$state_list->name}}</option> -->
                            @foreach($country_list as $country2)
                                @if($country2->id == $country_code)

                                    @foreach($country2->stateList as $state)
                                        <option value="{{$state->id}}" {{$state->id == $state_list->id ? "selected" : ""}}>{{$state->name}}</option>
                                    @endforeach

                                @endif
                            @endforeach
                        @endif

                        </select>
                    </div> <!-- State Field -->
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="postal-suburb" class="text-capitalize">postal suburb</label>
                        <input type="text" class="form-control" id="postal-suburb" name="postal_suburb" value="{{isset($postal_suburb) ? $postal_suburb : null}}">
                    </div>
                </div>
                
                <div class="mo-row">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="country" class="text-capitalize">country*</label>
                        <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>

                        <select class="form-control text-capitalize" id="country" name="country" value="{{isset($country) ? $country : null}}" disabled="true">

                            <option selected disabled>country list</option>
                            @if (isset($country_list))
                            @foreach($country_list as $country)
                                <option {{$country->id == 13 ? "selected" : ''}} value="{{$country->id}}" {{$country->id == $country_code ? "selected" : ""}}>{{$country->name}}</option>
                            @endforeach
                            @endif

                        </select>
                        <input type="hidden" id="input-country-id" name="input_country_id" />
                        <input type="hidden" value="{{isset($country_list) ? json_encode($country_list) : null}}" name="country-list" id="country-list" />
                    </div> <!-- Country Field -->
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="postcode" class="text-capitalize">postcode</label>
                        <input type="text" class="form-control" id="postcode" name="postcode" value="{{isset($postcode) ? $postcode : null}}">
                    </div>
                </div>
                
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="postal-address1" class="text-capitalize">postal address 1</label>
                    <input type="text" class="form-control" id="postal-address1" name="postal_address1" value="{{isset($postal_address1) ? $postal_address1 : null}}">
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="postal-address2" class="text-capitalize">postal address 2</label>
                    <input type="text" class="form-control" id="postal-address2" name="postal_address2" value="{{isset($postal_address2) ? $postal_address2 : null}}">
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="postal-state" class="text-capitalize">postal state</label>
                    <input type="text" class="form-control" id="postal-state" name="postal_state" value="{{isset($postal_state) ? $postal_state : null}}">
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="postal-postcode" class="text-capitalize">postal postcode</label>
                    <input type="text" class="form-control" id="postal-postcode" name="postal_postcode" value="{{isset($postal_postcode) ? $postal_postcode : null}}">
                </div>

                <div class="mo-row">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="postal-country" class="text-capitalize">postal country</label>
                        <input type="text" class="form-control" id="postal-country" name="postal_country" value="{{isset($postal_country) ? $postal_country : null}}">
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="email" class="text-capitalize">email address*</label>
                        <input type="email" class="form-control" id="email" name="email" value="{{isset($email) ? $email : null}}">
                    </div>    
                </div>
                
                <div class="mo-row">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="email-alter" class="text-capitalize">email alternative</label>
                        <input type="email" class="form-control" id="email-alter" name="email_alternate" value="{{isset($email_alternate) ? $email_alternate : null}}">
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="mobile" class="text-capitalize">mobile phone*</label>
                        <input type="text" class="form-control" id="mobile" name="mobile_phone" maxlength="20" value="{{isset($mobile_phone) ? $mobile_phone : null}}">
                    </div>    
                </div>
                
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="home-phone" class="text-capitalize">home phone</label>
                    <input type="text" class="form-control" id="home-phone" name="home_phone" maxlength="20"  value="{{isset($home_phone) ? $home_phone : null}}">
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="work-phone" class="text-capitalize">work phone</label>
                    <input type="text" class="form-control" id="work-phone" name="work_phone" maxlength="20" value="{{isset($work_phone) ? $work_phone : null}}">
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="fax" class="text-capitalize">fax number</label>
                    <input type="number" class="form-control" id="fax" name="fax" value="{{isset($fax_number) ? $fax_number : null}}">
                </div>
                {{--<div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="preferred-method" class="text-capitalize">preferred contact method</label>
                    <select class="form-control text-capitalize" id="preferred-method" name="preferred_method" value="{{isset($preferred_method) ? $preferred_method : null}}">
                        <option selected disabled>Preferred Contact Method List</option>
                    </select>
                </div>--}}
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="driver-number" class="text-capitalize">driver license number</label>
                    <input type="text" class="form-control" id="driver-number" name="driver_number" value="{{isset($driver_license_number) ? $driver_license_number : null}}">
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="dealer-number" class="text-capitalize">dealer license number</label>
                    <input type="text" class="form-control" id="dealer-number" name="dealer_number" value="{{isset($dealer_license_number) ? $dealer_license_number : null}}">
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="fleet-number" class="text-capitalize">fleet number</label>
                    <input type="number" class="form-control" id="fleet-number" name="fleet_number" value="{{isset($fleet_number) ? $fleet_number : null}}">
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="bank-branch-name" class="text-capitalize">bank branch name</label>
                    <input type="text" class="form-control" id="bank-branch-name" name="bank_branch_name" value="{{isset($bank_branch_name) ? $bank_branch_name : null}}">
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="bank-account-name" class="text-capitalize">bank account name</label>
                    <input type="text" class="form-control" id="bank-account-name" name="bank_account_name" value="{{isset($bank_account_name) ? $bank_account_name : null}}">
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="bank-bsb-number" class="text-capitalize">bank BSB number</label>
                    <input type="number" class="form-control" id="bank-bsb-number" name="bank_bsb_number" value="{{isset($bank_bsb_number) ? $bank_bsb_number : null}}">
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="bank-account-number" class="text-capitalize">bank account number</label>
                    <input type="number" class="form-control" id="bank-account-number" name="bank_account_number" value="{{isset($bank_account_number) ? $bank_account_number : null}}">
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="prospect-level" class="text-capitalize">prospect level</label>
                    <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                    <select class="form-control text-capitalize" id="prospect-level" name="prospect_level">
                        <option selected disabled>prospect level list</option>
                    </select>
                </div>

                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="privacy" class="text-capitalize">privacy*</label>
                    <span><img src="{{asset('images/main/icon-swipe-right-copy-4.png')}}" class="imgicon"></span>
                    <select name="privacy" id="privacy" class="form-control text-capitalize">
                        <option selected disabled>please select</option>
                        @if (isset($privacy_flag_list))
                        @foreach ($privacy_flag_list as $privacy)
                            <option value="{{$privacy->code}}" {{$privacy->code == $privacy_flag ? "selected" : ""}}>{{$privacy->value}}</option>
                        @endforeach
                        @endif

                        <!-- <option value="1">consent given</option> -->
                        <!-- <option value="2">unconsent given</option> -->
                        <!-- <option value="3">not set</option> -->

                    </select>
                </div>
                
                <div class="form-group col-xs-12 col-sm-6 col-md-6">
                    <label for="note" class="text-capitalize">note</label>
                    <textarea class="form-control" rows="3" id="note" name="note" >{{isset($note) ? $note : null}}</textarea>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-top-24 text-uppercase mo-form-header">
                    other details
                </div>

                <div class="mo-row">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6 padding-top-24">
                        <label for="new-used-vehicle">New or Used Vehicle</label>
                        <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                        <select class="form-control text-capitalize" id="new-used-vehicle" name="new_used_vehicle">
                            <option selected disabled>New or Used Vehicle List</option>
                            @if (isset($vehicle_type))
                            @foreach ($vehicle_type as $vehicle)
                            <option value="{{$vehicle->code}}" {{$vehicle->code == $interested_vehicle ? "selected" : ""}}>{{$vehicle->value}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-6 padding-top-24">
                        <label for="contact-method" class="text-capitalize">Preferred Contact Method</label>
                        <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                        <select class="form-control text-capitalize" id="contact-method" name="contact_method">
                            <option selected disabled>Preferred Contact Method List</option>
                            @if (isset($contact_method))
                            @foreach ($contact_method as $method)
                            <option value="{{$method->code}}" {{$method->code == $preferred_contact_method ? "selected" : ""}}>{{$method->value}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-12 padding-top-36 text-center">
                    <input type="submit" class="mo-btn text-uppercase" value="done" id="mo-button-edit-customer">
                </div>
                <div class="mo-loading-image hidden">Loading...</div>
            </form>
        </div>
    </div>
    {{--End - moto Edit Form Section--}}
@endsection