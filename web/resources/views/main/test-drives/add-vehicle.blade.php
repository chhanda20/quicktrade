<div class="col-xs-12 no-padding padding-top-24 hidden" id="mo-add-vehicle-wrapper">
    <div class="col-xs-12 padding-bottom-24 text-uppercase mo-form-header">
        vehicle details
    </div>
    <form action="" method="post" id="add-vehicle-form">
        {{ csrf_field() }}
        <div class="col-xs-12 col-sm-6">
            <div class="mo-row">
                <label for="year" class="text-capitalize">year</label>
                <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                <select name="year" id="year" class="form-control">
                    <option selected value="">Year List</option>
                    @for($i=date("Y"); $i>=1900; $i--)
                        <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
            </div>
            <div class="mo-row padding-top-24">
                <label for="make" class="text-capitalize">make*</label>
                <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                <select name="make" id="make" class="form-control">
                    <option selected value="">Please Select</option>
                    @if(isset($make_list))
                        @foreach($make_list as $make)
                            <option value="{{$make->name}}">{{$make->name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="mo-row padding-top-24">
                <label for="model" class="text-capitalize">model*</label>
                <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                <select name="model" id="model" class="form-control">
                    <option selected value="">Please Select</option>
                </select>
            </div>
            <div class="mo-row padding-top-24">
                <label for="variant" class="text-capitalize">variant*</label>
                <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                <select name="variant" id="variant" class="form-control">
                    <option selected value="">Please Select</option>
                </select>
            </div>
            <div class="mo-row padding-top-24">
                <label for="build-date" class="text-capitalize">build date</label>
                <div class="input-group date" id="build-date-picker">
                    <input type="text" class="form-control" id="build-date" name="build_date" placeholder="MM-YYYY" readonly>
                    <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                </div>
            </div>

        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="mo-row">
                <label for="colour" class="text-capitalize">colour</label>
                <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                <select name="colour" id="colour" class="form-control">
                    <option disabled selected>Please Select</option>
                    @if(isset($colour_list))
                        @foreach($colour_list as $colour)
                            <option value="{{$colour->code}}">{{$colour->value}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="mo-row padding-top-24">
                <label for="registration" class="text-capitalize">registration/trade plate no*</label>
                <input type="text" class="form-control text-uppercase" id="registration" name="registration">
            </div>
            {{--<div class="mo-row padding-top-24">
                <label for="trade-no" class="text-capitalize">trade plate no</label>
                <input type="text" class="form-control" id="trade-no" name="trade_no">
            </div>--}}
            <div class="mo-row padding-top-24">
                <label for="stock-number" class="text-capitalize">stock number</label>
                <input type="text" class="form-control" id="stock-number" name="stock_number">
            </div>
            {{--<div class="mo-row padding-top-24">
                <label for="vin-number" class="text-capitalize">VIN number</label>
                <input type="text" class="form-control text-uppercase" maxlength="17" id="vin-number" name="vin_number">
            </div>--}}
        </div>

        <div class="col-xs-12 col-sm-12 padding-top-32 text-align-center">
            <input type="button" class="mo-btn " id="add-vehicle-btn" value="ADD VEHICLE">
        </div>

        <div class="mo-loading-image hidden">Loading...</div>
    </form>
</div>