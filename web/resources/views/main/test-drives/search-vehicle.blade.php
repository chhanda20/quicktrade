@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')

    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-8 no-padding" id="mo-content-header">
            {{ $header or 'Search Vehicle' }}
        </div>

    </div>

    {{--End - moto Content Header Section--}}

    {{--Start - moto List Customers Section--}}
    <div class="mo-row padding-top-24 percent-h-100" id="mo-list-wrapper">

        <div class="mo-row mo-form-wrapper percent-h-100" >
                <!-- tab 1 -->
            <div class="mo-row" id="mo-blocks-wrapper">
                <div class="col-xs-12 col-sm-6 padding-lr5 mo-text">
                    <div class="col-xs-12 mo-block mo-offer-blocks" id="mo-tab-search">
                        <span class="col-xs-12 no-padding mo-block-txt text-uppercase padding-top-8" data-label="Search">search</span>
                        <span class="col-xs-12 text-center mo-arrow-focus">
                            <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" class="padding-bottom-10" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                        </span>
                    </div>
                </div>
                <!-- tab 2 -->
                <div class="col-xs-12 col-sm-6 padding-lr5 mo-text">
                    <div class="col-xs-12 mo-block mo-offer-blocks" id="mo-tab-enter-vehicle-detail">
                        <span class="col-xs-12 no-padding mo-block-txt text-uppercase padding-top-8" data-label="Enter Vehicle Detail">enter vehicle detail</span>
                        <span class="col-xs-12 text-center mo-arrow-focus">
                            <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" class="padding-bottom-10" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                        </span>
                    </div>
                </div>
            </div>

            <div class="mo-row padding-top-24" id="mo-search-vehicle-list-wrapper">
                {{--Start - moto Search Section--}}
                <div class="col-xs-12 col-sm-4 no-padding float-right padding-bottom-10">
                    {{ csrf_field() }}
                    <div class="col-xs-12 no-padding mo-search-wrapper">
                        <input type="text" placeholder="Search" id="mo-txt-search">
                        <span class="glyphicon glyphicon-search mo-txt-search cursor-pointer"></span>
                    </div>
                </div>
                {{--End - moto Search Section--}}

                <table id="mo-search-vehicle-list" class="table" cellspacing="0">
                    <thead>
                    <tr>
                        {{--<th>First Name <span class="float-right"><img src="{{ asset('images/svg/main/icon-swipe-bottom.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-bottom.png') }}'"></span></th>--}}
                        <th></th>
                        <th>Make</th>
                        <th>Model</th>
                        <th>Registration Number</th>
                        <th>VIN</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
                    <ul class="pagination">
                        <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                        <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
                    </ul>
                </div>
            </div>

            <!-- Start Add Vehicle -->
            @include("main.test-drives.add-vehicle")

        </div>
    </div>
    {{--End - moto List Customers Section--}}



@endsection