@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            {{ $header or 'Enter Vehicle Details' }}
            {{--Enter Vehicle Details--}}
        </div>
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - moto Create Form Section--}}
        <div class="mo-row mo-form-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                    license
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24">
                    <span for="">Click on the outlined shapes to upload a photo for the front and back of the driver’s license.</span>
                </div>

                {{-- test-drive form --}}
                <form action="/test-drives/term-and-condition/{{$customer_id or ''}}" method="GET" id="mo-test-drive-add-vehicle-form">
                    {{csrf_field()}}
                    <div class="mo-row col-xs-12 col-sm-12 padding-bottom-24">
                        <input type="file" name="test_drive_driver_licences_photo" id="test-drive-driver-licences-photo" class="hidden" />
                        
                        {{--font--}}
                        <div class="col-xs-6 col-sm-6 no-padding">
                            <div class="mo-vehicle-image-wrapper float-right padding-right-10" class="vertical-center">
                                <label for="test-drive-driver-licences-photo">
                                    <div id="mo-front-license">
                                        <span id="license_front_err_msg" class="hidden">FRONT</span>
                                        <img src="" id="license_front" class="img-responsive max-height-140 img-rounded verticle-center" />
                                    </div>
                                </label>
                            </div>
                        </div>
                        {{--end font--}}

                        {{--back--}}
                        <div class="col-xs-6 col-sm-6 no-padding">
                            <div class="mo-vehicle-image-wrapper padding-left-10" class="vertical-center">
                                <label for="test-drive-driver-licences-photo">
                                    <div id="mo-back-license" class="">
                                        <span class="hidden" id="license_back_err_msg">BACK</span>
                                        <img src="" id="license_back" class="img-responsive max-height-140 img-rounded verticle-center"/>
                                    </div>
                                </label>    
                            </div>
                        </div>
                        {{--end back--}}

                    </div>

                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-5 no-padding ">
                            <div class="col-xs-12 col-sm-12 no-padding  text-uppercase mo-form-header">
                                select vehicle
                            </div>
                            <div class="col-xs-12 col-sm-12 no-padding">
                                <hr class="mo-dispute-hr padding-bottom-20">
                                <div class="mo-row padding-bottom-20">
                                    {{--<label for="" class="text-capitalize">select vehicle</label>--}}
                                    {{--<span>{{ session('vehicle_name') }}</span>--}}
                                    <span>{{Session::pull("vehicle_name")}}</span>
                                    <input type="text" class="no-border opacity-99" id="mo-selected-vehicle" value="{{Session::pull("vehicle_id")}}" name="mo_selected_vehicle" placeholder="Selected Vehicle" />
                                    <a href="/test-drives/search-vehicle"><span class="glyphicon glyphicon-menu-right float-right"></span></a>
                                </div>
                                <div id="errorToShowMessage"></div>
                                <hr class="mo-dispute-hr">
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-5 no-padding float-right">
                            <div class="col-xs-12 col-sm-12 no-padding  text-uppercase mo-form-header">
                                test drive duration
                            </div>
                            <div class="col-xs-12 col-sm-12 no-padding">
                                <hr class="mo-dispute-hr">
                                <label for="" class="text-capitalize">estimated return time</label>
                                {{--<span class="glyphicon glyphicon-menu-right float-right"></span>--}}
                                <div class="input-group date padding-bottom-6" id="estimated-return-datepicker">
                                    <input type="hidden" id="current-date">
                                    <input type="text" class="form-control" id="estimated-return-time" name="estimated_return_time" value="{!! old('estimated_return_time') !!}" placeholder="Please Select" data-date-format="dd-mm-yyyy"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar estimated-return-calendar"></span>
                                    </span>
                                </div>
                                <div id="errorToShowMessage_1"></div>
                                <hr class="mo-dispute-hr">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="customer-id" name="customer_id" value="{{$customer_id or ''}}" >
                    <input type="hidden" id="license-section" name="license_section" value="" >
                    <input type="hidden" name="azure-accout-name" value="{{Config::get('app.azure_storage_account_name')}}" />
                    @if(session('create_result'))
                    <div class="alert alert-danger">
                        Cannot create test drive.
                    </div>
                    @endif
                    <div class="mo-row text-center padding-top-24" >
                        <input type="submit" class="mo-btn text-uppercase" id="mo-test-drives-create-new" value="add" />
                    </div>

                    <input type="hidden" id="manual-vehicle" name="manual_vehicle" value="">
                </form>
                {{-- end text-drive form --}}
            </div>
        </div>
        <!-- AJAX - loading -->
        <div class="mo-loading-image hidden">Loading&#8230;</div>
        <!-- AJAX - end loading -->

@endsection