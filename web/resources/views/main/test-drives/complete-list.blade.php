@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-5 no-padding" id="mo-content-header">
            {{$header or 'Test Drives'}} > <span class="yellow-text">{{ $breadcrumb or "Completed" }}</span>
        </div>
        {{--Start - moto Search Section--}}
        <div class="col-xs-12 col-sm-7 no-padding">
            {{ csrf_field() }}
            <div class="mo-row padding-left-20 padding-right-10">
                <div class="col-xs-12 col-sm-10 no-padding mo-search-wrapper">
                    <input type="text" placeholder="Search" id="mo-txt-search" name="mo_txt_search">
                    {{--<span class="glyphicon glyphicon-search mo-txt-search cursor-pointer"></span>--}}
                </div>
                <div class="col-xs-12 col-sm-2 no-padding text-right mo-search-btn-wrapper">
                    <input type="submit" class="mo-btn" id="mo-btn-search" value="search">
                </div>
            </div>
            <div class="mo-row padding-top-10 mo-complete-filter-date">
                <div class="col-xs-12 col-sm-8 no-padding text-right">
                    <label for="" class="text-capitalize">date range:</label>
                    <input type="text" id="test-drive-complete-date-range" name="test_drive_complete_date_range">
                </div>
                <div class="col-xs-12 col-sm-4 no-padding text-right">
                    <label for="" class="text-capitalize">to:</label>
                    <input type="text" id="test-drive-complete-to" name="test_drive_complete_to">
                    <div id="errorShowMessage"></div>
                </div>
            </div>

        </div>
        {{--End - moto Search Section--}}
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - moto List Customers Section--}}
    <div class="mo-row padding-top-24 percent-h-100" id="mo-list-wrapper">

        <div class="mo-row mo-form-wrapper percent-h-100">
            <table id="mo-table-complete-list" class="table" cellspacing="0">
                <thead>
                <tr>
                    {{--<th>First Name <span class="float-right"><img src="{{ asset('images/svg/main/icon-swipe-bottom.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-bottom.png') }}'"></span></th>--}}
                    <th></th>
                    <th>Make</th>
                    <th>Model</th>
                    <th>Year</th>
                    <th>Colour</th>
                    <th>Start Date&Time</th>
                    <th>Time Out</th>
                    <th>Returned</th>
                    <th>Status</th>
                    <th>Comment</th>
                    <th>Registration/Trade No.</th>
                    <th>Stock Number</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
                <ul class="pagination">
                    <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                    <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
                </ul>
            </div>
            
        </div>
    </div>
    {{--End - moto List Customers Section--}}

    {{-- start Comment Popup--}}
    <div id="mo-test-drives-read-comment-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading"></span>
            </div>

            <hr class="mo-form-hr padding-bottom-10" />
            <span><label for=" " class="text-capitalize padding-bottom-20">feedback</label></span>
            
            <div>
                <div class="padding-bottom-10 padding-left-10">How happy were you with the vehicle?</div> 
                <div id="slider_vehicle_rating" class=""></div>

                <div class="padding-bottom-10 padding-left-10">How happy were you with us?</div>    
                <div id="slider_mo_rating" class=""></div>

                <div>
                    <div class="padding-bottom-10 padding-left-10">How did you hear about us?</div>
                    <div class="col-xs-12">
                        <input type="text" class="form-control" name="mo_marketing_option" id="mo_marketing_option" disabled>
                    </div>
                    <div class="col-xs-12 padding-top-10">
                        <input type="text" class="form-control hidden" name="mo_marketing_option_other" id="mo_marketing_option_other" disabled>
                    </div>
                </div>
            </div>

            <span><label for="" class="padding-top-24 text-capitalize">comments</label></span>
            <div class="mo-row padding-bottom-24">
                <hr class="mo-dispute-hr" />
                <div class="form-group col-xs-12 no-padding">
                    <textarea class="mo-textarea form-control" rows="3" id="test-drive-note" name="test_drive_note" placeholder="Write here…" style="box-shadow: none;"></textarea>
                </div>
            </div>
            <hr class="mo-dispute-hr padding-bottom-20"/>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    {{--<input type="button" class="mo-btn text-uppercase disabled-div" value="confirm" id="mo-test-drives-inprogress-change-status">--}}
                </div>
            </div>
            <div class="mo-loading-image hidden">Loading...</div>
        </form>
    </div>
@endsection