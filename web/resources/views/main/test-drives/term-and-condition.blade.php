@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            Test Drives > {{ $header or 'Term & Conditions' }}
            {{--Enter Vehicle Details--}}
        </div>
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - moto Create Form Section--}}
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                term & conditions
            </div>
            <form action="" method="post" id="mo-test-drive-term-condition-form">
                {{csrf_field()}}
                <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24">
                    <textarea class="mo-textarea form-control" id="test-drive-term-condition" rows="20" style="border: none;" readonly>{{$text_condition->test_drive_terms}}</textarea>
                    <hr class="mo-dispute-hr">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 padding-bottom-24">
                    <input type="checkbox" class="mo-round-check" id="mo-test-drive-accept-condition" name="mo_test_drive_accept_condition">
                    <label for="mo-test-drive-accept-condition"><span></span>I accept these Term & Conditions</label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 no-padding text-center disabled-div" id="mo-test-drive-signature-btn">
                    <input type="button" class="mo-btn text-uppercase" id="mo-test-drive-sign" value="sign">
                </div>
            </form>
        </div>
    </div>

    <!-- SIGNATURE POPUP -->
    <div id="mo-test-drives-signature-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form" id="mo-test-drive-signature-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">sign</span>
            </div>
            <hr class="mo-form-hr padding-bottom-10" />
            <div class="mo-row padding-bottom-24">
                {{--<div id="signature" class="kbw-signature"></div>--}}
                {{--<div class="js-signature"></div>--}}
                <div id="signature" class="kbw-signature"></div>
            </div>

            {{--<hr class="mo-dispute-hr padding-bottom-20"/>--}}
            <hr class="mo-form-hr" />
            <div id="upload-signature" class="hidden">

            </div>
            <div id="errorToShow"></div>
            <input type="hidden" value="{{$test_drive_id or ''}}" name="test_drive_id" />
            <input type="hidden" value="{{$customer_id or ''}}" name="customer_id" />
            <input type="hidden" value="{{$vehicle_id or ''}}" name="vehicle_id" />
            <input type="hidden" value="{{$returned_date or ''}}" name="returned_date" />
            <input type="hidden">
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="i accept" id="mo-test-drives-accept-btn">
                </div>
            </div>
            <input type="hidden" id="manual-vehicle" name="manual_vehicle">
        </form>
    </div>

    <!-- AJAX - loading -->
    <div class="mo-loading-image hidden">Loading&#8230;</div>
    <!-- AJAX - end loading -->

@endsection