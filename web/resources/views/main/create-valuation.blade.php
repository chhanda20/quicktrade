@extends('layouts.master')

@section('title', 'moto - Create Valuation')

@section('content')
{{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <a href="{!! url('customers') !!}">{{ $header or 'Valuations' }}</a> > {{ $breadcrumb or 'New Valuation' }}
        </div>
    </div>
{{--End - moto Content Header Section--}}

{{--Start - moto Create Form Section--}}
    <div class="mo-row mo-form-wrapper">

        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                vehicle details
            </div>
            <form action="{!! url('create-valuation') !!}" method="post" id="create-valuation-form">
                {{ csrf_field() }}
                <input type ="hidden" name="customer_id" value="{{$customer_id}}">
				
				<div class="mo-row padding-bottom-24">
					<div class="col-xs-12 col-sm-6 col-md-6 no-padding">
						<div class="col-xs-12 col-sm-4">
							<label for="year" class="text-capitalize">year*</label>
		                    <!-- <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span> -->
                            <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}"></span>
		                    <select class="form-control text-capitalize" id="year" name="year">
		                        <!-- <option selected disabled></option>
		                        @for ($i=1900; $i<=date("Y"); $i++)
		                        <option value="{{$i}}">{{$i}}</option>
		                        @endfor -->
								<option selected value="">Year List</option>
		                        @for ($i=date("Y"); $i>=1900; $i--)
		                        <option value="{{$i}}">{{$i}}</option>
		                        @endfor
		                    </select>
						</div>

						<div class="col-xs-12 col-sm-8">
							<label for="vin" class="text-capitalize">VIN </label>
		                    <div class="form-inline">

		                    	<input type="text" class="form-control text-uppercase " id="vin" name="vin" maxlength="17" >
		                    	<span><img class="icon-download" src="{{asset('/images/main/icon-download.png')}}" id="get_vin_number"></span>
                                <span class="hide"><img src="{{ asset('/images/main/ajax-loader.gif') }}" class="mo-btn-loading-2" /></span>
                                <span class="glyphicon glyphicon-ok-circle mo-vin-checked hide"></span>
		                    </div>
						</div>
					</div>
				</div>{{-- ROW 1 --}}

				<hr class="mo-form-hr" style="margin-left: 10px; margin-right: 10px;">
				
                <div class="mo-row padding-top-24">
               		<div class="col-xs-12 col-sm-6">
	                    <label for="registration-number" class="text-capitalize">registration number</label>
	                    <input type="text" class="form-control text-uppercase" id="registration-number" name="registration_number">
	                </div>
                    <div class="col-xs-12 col-sm-6">
                        <label for="registration-expiry-date" class="text-capitalize">registration expiry date*</label>
                        <div class="input-group date" id="registration-expiry-date-picker">
                            <input type="text" class="form-control" id="registration-expiry-date" name="registration_expiry_date" placeholder="DD-MM-YYYY" data-date-format="dd-mm-yyyy"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>{{-- ROW 2 --}}
                
                <div class="mo-row padding-top-24">
                    <div class="col-xs-12 col-sm-6">
                        <label for="make" class="text-capitalize">make*</label>
                        {{--<span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>--}}
                        <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}"></span>
                        <select class="form-control text-capitalize" id="make" name="make">
                            <option selected value="">make list</option>
                            @if (isset($makes))
                                @foreach ($makes as $make)
                                    <option value="{{$make->name}}">{{$make->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <label for="compliance-date" class="text-capitalize">compliance date*</label>
                        <div class="input-group date" id="compliance-date-picker">
                            <input type="text" class="form-control" id="compliance-date" name="compliance_date" placeholder="DD-MM-YYYY" data-date-format="dd-mm-yyyy"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>{{-- ROW 3 --}}
                
                <div class="mo-row padding-top-24">
                    <div class="col-xs-12 col-sm-6">
                        <label for="model" class="text-capitalize">model*</label>
                        {{--<input type="text" class="form-control" id="model" name="model">--}}
                        {{--<span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>--}}
                        <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}"></span>
                        <select class="form-control text-capitalize" id="model" name="model">
                            <option selected value="">model list</option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <label for="engine-number" class="text-capitalize">engine number</label>
                        <input type="text" class="form-control text-uppercase" id="engine-number" name="engine_number">
                    </div>
                </div>{{-- ROW 4 --}}

                <div class="mo-row padding-top-24">
                    <div class="col-xs-12 col-sm-6">
                        <label for="variant" class="text-capitalize">variant*</label>
                        {{--<span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>--}}
                        <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}"></span>
                        <select class="form-control text-capitalize" id="variant" name="variant">
                            <option selected value="">variant list</option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <label for="location-of-vehicle-key" class="text-capitalize">location of vehicle keys</label>
                        <input type="text" class="form-control" id="location-of-vehicle-key" name="location_of_vehicle_key">
                    </div>
                </div>{{-- ROW 5 --}}

                <div class="mo-row padding-top-24">
                    <div class="col-xs-12 col-sm-6">
                        <label for="build-date" class="text-capitalize">build date*</label>
                        <div class="input-group date" id="build-date-picker">
                            <input type="text" class="form-control" id="build-date" name="build_date" placeholder="MM-YYYY" readonly />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <label for="location-of-trade-in-vehicle" class="text-capitalize">Location of trade in vehicle</label>
                        <input type="text" class="form-control" id="location-of-trade-in-vehicle" name="location_of_trade_in_vehicle">
                    </div>
                </div>{{-- ROW 6 --}}

                <div class="mo-row padding-top-24">
                    <div class="col-xs-12 col-sm-6">
                        <label for="colour" class="text-capitalize">colour*</label>
                        {{--<span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>--}}
                        <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}"></span>
                        <select class="form-control text-capitalize" id="colour" name="colour">
                            <option selected disabled>colour list</option>
                            @if (isset($colors))
                                @foreach ($colors as $color)
                                    <option value="{{$color->code}}">{{$color->value}}</option>
                                @endforeach
                            @endif
                        </select>
                        <div class="mo-row padding-top-24">
                            <label for="odometer" class="text-capitalize">odometer*</label>
                            <input type="text" class="form-control" id="odometer" name="odometer" maxlength="10">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="mo-row padding-bottom-24">
                            <input type="checkbox" class="mo-square-check" id="one-owner" name="one_owner">
                            <label for="one-owner" class="text-capitalize"><span></span> one owner?</label>
                        </div>
                        <div class="mo-row padding-bottom-24">
                            <input type="checkbox" class="mo-square-check" id="service-book" name="service_book">
                            <label for="service-book" class="text-capitalize"><span></span> service book?</label>
                        </div>
                        <div class="mo-row padding-bottom-24">
                            <input type="checkbox" class="mo-square-check" id="service-history" name="service_history" >
                            <label for="service-history" class="text-capitalize"><span></span> service history?</label>
                        </div>
                    </div>
                </div>{{-- ROW 7 --}}

                {{-------------------------INTERESTED PURCHASING DETAILS-------------------------}}

                <div class="mo-row padding-top-24">
                    <div class="col-xs-12 col-sm-6">
                        <div class="mo-row padding-bottom-24 text-uppercase mo-form-header">
                            ADDITIONAL DETAILS
                        </div>
                        <label for="finance-owing" class="text-capitalize">finance owing?*</label>
                        <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}"></span>
                        <select class="form-control" name="finance_owing" id="finance-owing">
                            <option disabled selected>Finance Owing?</option>
                            <option value="true">Yes</option>
                            <option value="false">No</option>
                        </select>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <div class="mo-row padding-bottom-24 text-uppercase mo-form-header">
                            VEHICLE INTERESTED IN
                        </div>
                        <label for="interested" class="text-capitalize">interested vehicle?*</label>
                        <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}"></span>
                        <select class="form-control" id="interested-in-vehicle" name="interested_in_vehicle">
                            <option selected disabled>Interested Vehicle?</option>
                            @if (isset($interested_vehicles))
                                @foreach ($interested_vehicles as $vehicle)
                                    <option class="text-capitalize" value="{{$vehicle->code}}">{{$vehicle->value}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6" id="mo-finance-owing-section">
                        <div class="mo-row padding-top-24 hidden">
                            <label for="institution" class="text-capitalize">institution</label>
                            <input type="text" class="form-control" id="institution" name="institution">
                        </div>
                        <div class="mo-row padding-top-24 hidden">
                            <label for="payout" class="text-capitalize">payout</label>
                            <input type="text" class="form-control" id="payout" name="payout">
                        </div>
                        <div class="mo-row padding-top-24 hidden">
                            <label for="monthly-payment" class="text-capitalize">monthly payment</label>
                            <input type="text" class="form-control" id="monthly-payment" name="monthly_payment">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6" id="mo-interested-vehicle-section">
                        <div class="mo-row padding-top-24 hidden">
                            <label for="interested-make" class="text-capitalize">make</label>
                            {{--<span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>--}}
                            <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}"></span>
                            <select class="form-control text-capitalize" id="interested-make" name="interested_make">
                                <option selected value="">make list</option>
                                @if (isset($makes))
                                    @foreach ($makes as $make)
                                        <option value="{{$make->name}}">{{$make->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="mo-row padding-top-24 hidden">
                            <label for="interested-model" class="text-capitalize">model</label>
                            <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}"></span>
                            <select class="form-control text-capitalize" id="interested-model" name="interested_model">
                                <option selected value="">model list</option>
                            </select>
                        </div>
                        <div class="mo-row padding-top-24 hidden">
                            <label for="interested-variant" class="text-capitalize">variant</label>
                            <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}"></span>
                            <select class="form-control text-capitalize" id="interested-variant" name="interested_variant">
                                <option selected value="">variant list</option>
                            </select>
                        </div>
                    </div>
                </div>

                {{-------------------------OWNERSHIP TYPES-------------------------}}
                <div class="col-xs-6"><hr class="mo-hr"></div>
                <div class="col-xs-12 padding-bottom-10">
                    <label for="ownership" class="text-capitalize">ownership type*</label>
                </div>
                <div class="mo-row">
                    <div class="col-xs-6 no-padding" id="mo-ownership-type">
                        @foreach($ownership_types as $type)
                            <div class="form-group col-sm-12 col-md-6 font-warm-grey-color">
                                <input type="radio" class="mo-round-check" id="ownership-{{$type->code}}" name="ownership_type" value="{{$type->code}}">
                                @if($type->code == 6)
                                    <label for="ownership-{{$type->code}}" class="font-family-light text-capitalize"><span></span><input type="text" style="width: 100px;" name="ownership_type_other" placeholder="{{$type->value}}"></label>
                                @else
                                    <label for="ownership-{{$type->code}}" class="font-family-light text-capitalize"><span></span>{{$type->value}}</label>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>

                {{-------------------------SPECIAL DISCLOSURES-------------------------}}
                <div class="col-xs-6"><hr class="mo-hr"></div>
                <div class="col-xs-12 padding-bottom-10">
                    <label for="ownership" class="text-capitalize">Special Disclosures</label>
                </div>
                <div class="mo-row">
                    <div class="col-xs-6 no-padding">
                        @foreach($special_disclosures as $type)
                            <div class="form-group col-xs-12 col-sm-6 padding-bottom-10 font-warm-grey-color">
                                <input type="checkbox" class="mo-square-check" id="disclosure-{{$type->code}}" name="special_disclosure[]" value="{{$type->code}}">
                                <label for="disclosure-{{$type->code}}" class="font-family-light text-capitalize"><span></span>{{$type->value}}</label>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="mo-row hidden" id="mo-repairable-write-off">
                    <div class="col-xs-6">
                        <label for="interested-make" class="text-capitalize">please select</label>
                        <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}"></span>
                        <select class="form-control text-capitalize" id="repairable-write-off" name="repairable_write_off">
                            {{--<option disabled selected>Repairable Write-Off</option>--}}
                            @foreach ($repairable_list as $type)
                                <option  value="{{$type->code}}" {{$type->code == 1?"selected": ""}}>{{$type->value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>




                <div class="form-group col-xs-12 col-sm-12 col-md-12 padding-top-36 text-center">
                    <input type="hidden" name="submit_button" value="begin valuation">
                    <input type="submit" class="mo-btn text-uppercase" name="submit_button"
                           value="begin valuation" id="create-valuation-button"
                           data-permissions="{{ is_array($permissions) && in_array("valuation_perform", $permissions)?"valuation_perform":"" }}">
                    <br/>
                    <input type="submit" class="mo-btn text-uppercase margin-top-16" id="mo-request-btn" name="submit_button"  value="request valuation">
                </div>
                <div class="mo-loading-image hidden">Loading...</div>
            </form>
        </div>

    </div>

    {{--Start - moto Enter VIN Popup Section--}}

        <div id="mo-enter-vin-popup" class="mo-white-popup mfp-hide">
            <form class="mo-popup-form" id="mo-popup-enter-vin">
                {{ csrf_field() }}
                <div class="mo-row padding-bottom-20">
                    <span class="text-capitalize mo-popup-heading">Enter VIN</span>
                </div>
                <hr class="mo-form-hr" />

                <div class="mo-row padding-top-24 padding-bottom-24">

                    <div class="form-group col-xs-12 col-sm-8 padding-left-9 padding-bottom-10">
                        <div class="col-xs-11 no-padding">
                            <label for="year">Year*</label>
                            <div>
                                <input type="text" class="form-control" name="year" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-xs-12 col-sm-8 padding-left-9 padding-bottom-10">

                        
                        <div class="col-xs-11 no-padding">
                            <label for="vin">VIN*</label>
                            <input type="text" class="form-control" name="vin" maxlength="17" style="text-transform:uppercase"/>
                        </div>
                        <div class="col-xs-1 no-padding padding-top-30 padding-left-5">
                            <img src="{{ asset('/images/main/icon-download.png') }}" alt="Get VIN" class="no-padding padding-top-4 padding-left-5" id="get-vin" />
                        </div>

                    </div>

                    <div class="form-group col-xs-12 col-sm-8 padding-left-9 padding-bottom-10">
                        <div class="col-xs-11 no-padding">
                            <label for="registration-number">Registration Number*</label>
                            <div>
                                <input type="text" class="form-control text-uppercase" name="registration-number" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-xs-12 padding-top-24">
                        <label for="note">Notes</label>
                        <hr class="mo-form-hr" />
                        <div class="padding-top-20 note-content text-uppercase">
                            {{--013 MASERATI QUATTROPORTE SPORT GT-S MC SPORTLINE MY13 4D SEDAN V8 4691 CC MPFI 6 SP AUTOMATIC WHITE--}}
                        </div>
                    </div>

                    <div class="form-group col-xs-12 padding-top-24">
                        <div class="col-xs-12 no-padding padding-bottom-8">
                            <div class="col-xs-3 no-padding vin">VIN:</div>
                            <div class="col-xs-9 no-padding vin vin-number">{{--JT152EEA100302159--}}</div>
                        </div>
                        <hr class="mo-form-hr clear-both" />
                    </div>

                </div>

                <hr class="mo-form-hr" />
                <div class="mo-row">
                    <div class="col-xs-12 padding-top-24 text-right">
                        <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                        <input type="button" class="mo-btn text-uppercase" value="confirm" id="vin-confirm" disabled>
                    </div>
                </div>

                <div class="mo-loading-image hidden">Loading...</div>
            </form>
        </div>

    {{--End - moto Enter VIN Popup Section--}}

@endsection