@section('review-details')

    @if($errors->any())
        <h4>{{$errors->first()}}</h4>
    @endif
    {{----------------------------------------------------------- start DETAILS -----------------------------------------------------------}}

    {{--Start - moto Details Content Section--}}
    <div class="mo-row mo-form-wrapper margin-top-32 mo-block-list" id="mo-details-block">
        {{--MOTORONE BLOCK INSIDE--}}
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding mo-5-blocks" id="mo-blocks-wrapper">
            @for ($i = 0; $i < count($details_step_array); $i++)
                <div class="col-xs-12 col-sm-5 padding-lr5">
                    <a href="#details-{{explode(' ',$details_step_array[$i])[0]}}">
                        <div class="col-xs-12 mo-block-inside {{/*in_array("salesperson", $roles) && */$status == 10 || $status == 2?'disabled-div':''}}" id="{{explode(' ',$details_step_array[$i])[0].'_tab'}}">
                            <span class="col-xs-12 mo-block-txt text-capitalize no-padding" data-details-step="{{$details_data_step_array[$i]}}">{{$details_step_array[$i]}}</span>
                        <span class="col-xs-12 text-center mo-arrow-focus">
                            <img src="{{ asset('images/svg/main/triangle.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/triangle.png') }}'">
                        </span>
                        </div>
                    </a>
                </div>
            @endfor
        </div>
        {{--MODEL SPECS & STANDARD FEATURES--}}
        <div class="mo-row padding-top-36 mo-block-inside-list" id="mo-model-specs-block">
            <div class="col-xs-12 padding-lr5 padding-bottom-24 text-uppercase mo-form-header">
                model specs
            </div>
            <form action="" method="" id="mo-model-spec-form">
                {{ csrf_field() }}

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="make" class="text-capitalize">make</label>
                        <div class="text-capitalize">{{$make or ''}}</div>
                        <input type="hidden" value="{{$make or ''}}" id="make" name="make">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="engine" class="text-capitalize">engine</label>
                        <div class="text-capitalize" id="engine-txt">{{$engine or ''}}</div>
                        <input type="hidden" value="{{$engine or ''}}" id="engine" name="engine">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="model" class="text-capitalize">model</label>
                        <div class="text-capitalize">{{$model or ''}}</div>
                        <input type="hidden" value="{{$model or ''}}" id="model" name="model">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="engine-size" class="text-capitalize">engine size</label>
                        <div class="text-capitalize" id="engine-size-txt">{{$engine_size or ''}}</div>
                        <input type="hidden" value="{{$engine_size or ''}}" id="engine-size" name="engine_size">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="variant" class="text-capitalize">variant</label>
                        <div class="text-capitalize">{{$variant or ''}}</div>
                        <input type="hidden" value="{{$variant or ''}}" id="variant" name="variant">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="engine-capacity" class="text-capitalize">engine capacity</label>
                        <div class="text-capitalize" id="engine-capacity-txt">{{$engine_capacity or ''}}</div>
                        <input type="hidden" value="{{$engine_capacity or ''}}" id="engine-capacity" name="engine_capacity">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="build-date" class="text-capitalize">build date</label>
                        @if (isset($build_date))
                            <div class="text-capitalize">{{date('M Y', strtotime($build_date))}}</div>
                            <input type="hidden" value="{{$build_date}}" name="build_date" id="build-date">
                        @endif
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="number-of-cylinders" class="text-capitalize">number of cylinders</label>
                        <div class="text-capitalize" id="cylinder-txt">{{$cylinder or ''}}</div>
                        <input type="hidden" value="{{$cylinder or ''}}" id="cylinder" name="cylinder">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="genneral-colour" class="text-capitalize">general colour</label>
                        @if (isset($colour))
                            <div class="text-capitalize">{{$colour}}</div>
                            <input type="hidden" value="{{$colour_code}}" name="general_colour" id="general-colour">
                        {{--@else
                            <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                            <select class="form-control {{isset($colour)?'hidden':''}}" name="general_colour" id="general-colour">
                                <option value="" disabled selected>Please select general colour</option>
                                @foreach($colors as $color)
                                    <option value="{{$color->code}}">{{$color->value}}</option>
                                @endforeach
                            </select>--}}
                        @endif
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="fuel-type" class="text-capitalize">fuel type</label>
                        @if (!in_array($status, array(10,2)))
                            <div class="text-capitalize">{{$fuel_type or ''}}</div>
                            <input type="hidden" value="{{$fuel_type_code or ''}}" name="fuel_type" id="fuel-type">
                        @else
                            <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                            <select class="form-control" name="fuel_type" id="fuel-type">
                                <option value="" disabled selected>Please select fuel type</option>
                                @foreach($fuel_types as $fuel_type)
                                    <option value="{{$fuel_type->code}}" {{$fuel_type_code==$fuel_type->code?"selected":""}}>{{$fuel_type->value}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="manufacturer-colour" class="text-capitalize">manufacturer colour</label>
                        @if (!in_array($status, array(10,2)))
                            <div class="text-capitalize">{{$manufacturer_color or ''}}</div>
                            <input type="hidden" value="{{$manufacturer_color_code or ''}}" name="manufacturer_colour" id="manufacturer-colour">
                        @else
                            <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                            <select class="form-control" name="manufacturer_colour" id="manufacturer-colour">
                                <option value="" disabled selected>Please select manufacture colour</option>
                                @foreach($colors as $color)
                                    <option value="{{$color->code}}" {{$manufacturer_color_code==$color->code?"selected":""}}>{{$color->value}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="transmission" class="text-capitalize">transmission*</label>
                        @if (!in_array($status, array(10,2)))
                            <div class="text-capitalize">{{$transmission or ''}}</div>
                            <input type="hidden" value="{{$transmission or ''}}" name="transmission" id="transmission">
                        @else
                            <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                            <select class="form-control" name="transmission" id="transmission">
                                <option value="" disabled selected>Please select transmission</option>
                            </select>
                            <input type="hidden" value="{{$transmission or ''}}" id="transmission_value">
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="body-type" class="text-capitalize">body type*</label>
                        @if (!in_array($status, array(10,2)))
                            <div class="text-capitalize">{{$body_type or ''}}</div>
                            <input type="hidden" value="{{$body_type or ''}}" name="body_type" id="body-type">
                        @else
                            <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                            <select class="form-control" name="body_type" id="body-type">
                                <option value="" disabled selected>Please select body type</option>
                            </select>
                            <input type="hidden" value="{{$body_type}}" id="body_type_value">
                        @endif
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="drivetrain" class="text-capitalize">drivetrain</label>
                        @if (!in_array($status, array(10,2)))
                            <div class="text-capitalize">{{$drive_train or ''}}</div>
                            <input type="hidden" value="{{$drive_train_code or ''}}" name="drive_train" id="drive-train">
                        @else
                            <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                            <select class="form-control" name="drive_train" id="drive-train">
                                <option value="" disabled selected>Please select drivetrain</option>
                                @foreach($drivetrains as $drivetrain)
                                    <option value="{{$drivetrain->code}}" {{$drive_train_code==$drivetrain->code?"selected":""}}>{{$drivetrain->value}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="year-first-registered" class="text-capitalize">year first registered</label>
                        @if (!in_array($status, array(10,2)))
                            <div class="text-capitalize">{{$year_first_registered or ''}}</div>
                            <input type="hidden" value="{{$year_first_registered or ''}}" name="year_first_registered" id="year-first-registered">
                        @else
                            <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                            <select class="form-control text-capitalize" id="year-first-registered" name="year_first_registered">
                                <option selected disabled></option>
                                @for ($i=date("Y"); $i>=1900; $i--)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        @endif
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="body-no">Body no.</label>
                        @if (!in_array($status, array(10,2)))
                            <div class="text-capitalize">{{$body_no or ''}}</div>
                        @endif
                        <input type="{{!in_array($status, array(10,2))?"hidden":"text"}}"
                               value="{{$body_no or ''}}" id="body-no" name="body_no" class="form-control">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="registration-expiry-date" class="text-capitalize">registration expiry date</label>
                        @if (isset($registration_expiry_date))
                            <div class="text-capitalize">{{date('d M Y', strtotime($registration_expiry_date))}}</div>
                            <input type="hidden" value="{{$registration_expiry_date}}" name="registration_expiry_date">
                        @endif
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="vin" class="text-capitalize">VIN</label>
                        @if (isset($vin))
                            <div class="text-capitalize">{{$vin}}</div>
                            <input type="hidden" value="{{$vin}}" name="vin" id="vin">
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="registered-state" class="text-capitalize">registered state</label>
                        @if (!in_array($status, array(10,2)))
                            <div class="text-capitalize">{{$registered_state or ''}}</div>
                            <input type="hidden" value="{{$registered_state_code or ''}}" name="registered_state" id="registered-state">
                        @else
                            <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                            <select class="form-control" name="registered_state" id="registered-state">
                                <option value="" disabled selected>Please select registered state</option>
                                @foreach($registered_states as $registered_state)
                                    <option value="{{$registered_state->code}}" {{$registered_state_code == $registered_state->code?"selected":""}}>{{$registered_state->value}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="chassis-no">Chassis no.</label>
                        @if (!in_array($status, array(10,2)))
                            <div class="text-capitalize">{{$chassis_number or ''}}</div>
                        @endif
                        <input type="{{!in_array($status, array(10,2))?"hidden":"text"}}"
                               value="{{$chassis_number or ''}}" id="chassis-number" name="chassis_number" class="form-control">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="registration" class="text-capitalize">registration</label>
                        <div class="text-capitalize">{{$registration or ''}}</div>
                        <input type="hidden" value="{{$registration or ''}}" name="registration" id="registration">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="trim-type" class="text-capitalize">trim type</label>
                        @if (!in_array($status, array(10,2)))
                            <div class="text-capitalize">{{$trim or ''}}</div>
                            <input type="hidden" value="{{$trim_code or ''}}" name="trim_type" id="trim-type">
                        @else
                            <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                            <select class="form-control" name="trim_type" id="trim-type">
                                <option value="" disabled selected>Please select trim type</option>
                                @foreach($trim_types as $trim_types)
                                    <option value="{{$trim_types->code}}" {{$trim_code==$trim_types->code?"selected":""}}>{{$trim_types->value}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="compliance-date" class="text-capitalize">compliance date</label>
                        @if (isset($compliance_date))
                            <div class="text-capitalize">{{date('d M Y', strtotime($compliance_date))}}</div>
                            <input type="hidden" value="{{$compliance_date}}" name="compliance_date">
                        @endif
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="trim-colour" class="text-capitalize">trim colour</label>
                        @if (!in_array($status, array(10,2)))
                            <div class="text-capitalize">{{$trim_color or ''}}</div>
                            <input type="hidden" value="{{$trim_color_code or ''}}" name="$trim_color" id="trim-color">
                        @else
                            <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                            <select class="form-control" name="trim_color" id="trim-color">
                                <option value="" disabled selected>Please select trim colour</option>
                                @foreach($colors as $color)
                                    <option value="{{$color->code}}" {{$trim_color_code==$color->code?"selected":""}}>{{$color->value}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>

                <!-- Standard features -->
                <div class="col-xs-12 padding-lr5">
                    <div class="padding-top-24 text-uppercase mo-form-header padding-bottom-24">standard features</div>
                </div>

                <div id="glass-options-wrapper">
                    @include('main.valuation.glass-options-list')
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 padding-top-36 text-center">
                    <input type="button" id="mo-model-spec-next-button" class="mo-btn text-uppercase" value="NEXT">
                </div>
                <input type="hidden" id="glass-code" name="glass_code">

            </form>
            
        </div>

        {{-- OPTIONS & EXTRAS --}}
        <div class="mo-row padding-top-36 hidden mo-block-inside-list" id="mo-options-extras-block">
            <div class="col-xs-12 padding-lr5 text-uppercase mo-form-header padding-bottom-24">
                options
            </div>
            <form action="{!! url('save-options-and-extras/'.$valuation_id) !!}" method="post" id="mo-create-option-and-extra-form">
                {{ csrf_field() }}
                <input type="hidden" value="{{isset($vehicle_id) ? $vehicle_id : null}}" name="vehicle_id" id="vehicle-id">
                <input type="hidden" value="{{isset($valuation_id) ? $valuation_id : null}}" name="valuation_id">
                {{--Options--}}

                <div id="optional-options-wrapper">
                    @include('main.valuation.optional-options')
                </div>

                {{--Extras--}}
                <div class="col-xs-12 padding-lr5">
                    <div class="padding-bottom-20 text-uppercase mo-form-header border-bottom">
                        <span>extras</span>
                    </div>
                    <!-- EMPTY SPAN TO CLONE LATER -->
                    <span class="col-xs-12 no-padding padding-top-20 padding-bottom-20 text-capitalize mo-extras-options hidden"></span>

                    @foreach ($extras_options_list as $extras_option)
                        <span class="col-xs-12 no-padding padding-top-20 padding-bottom-20 text-capitalize mo-extras-options">
                            <textarea id="{{$extras_option->id}}" class="form-control mo-extras-txt" rows="1" placeholder="Write here…" style="box-shadow: none;" readonly>{{$extras_option->description}}</textarea>
                            <input type="hidden" name="" value="{{$extras_option->id}}">
                            <div class="mo-icon-remove float-right {{$hideEditField or ''}}">
                                <img src="{{asset('images/svg/main/icon-remove.svg')}}" alt="Remove Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-remove.png') }}'">
                            </div>
                            <div class="mo-error-wrapper hidden">
                                <img src="{{asset('images/svg/main/ic-warning.svg')}}" alt="Plus Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/ic-warning.png') }}'">
                                <span class="error">Please enter content</span>
                            </div>
                        </span>
                    @endforeach
                    <span class="col-xs-12 no-padding padding-top-20 padding-bottom-20 text-capitalize mo-extras-options {{$hideEditField or ''}}">
                        <span id="mo-plus-icon" class="{{$hideEditField or ''}} glyphicon glyphicon-plus"></span>
                        <!-- <img id="mo-plus-icon" class="{{$hideEditField or ''}} icon-add" src="{{asset('images/svg/main/plus.svg')}}" alt="Plus Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/plus.png') }}'"> -->
                    </span>

                </div>
                {{--Save Button--}}
                <div class="form-group col-xs-12 col-sm-12 col-md-12 padding-top-36 text-center {{$hideEditField or ''}}">
                    <input type="button" id="mo-save-options-extras" class="mo-btn text-uppercase" value="next">
                </div>
            </form>
        </div>
    </div>
    {{--End - moto Details Content Section--}}

    {{----------------------------------------------------------- end DETAILS -----------------------------------------------------------}}

@endsection