@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
    @include('main.external-offer.external-offer-filter')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <div class="col-xs-12 col-sm-8 col-md-8 no-padding">
                {{ $header or 'Offers' }} > <span class="yellow-text">{{ $breadcrumb or 'Incoming Offers' }}</span>
            </div>
        </div>
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - moto 3 TABs Section--}}
    <div class="mo-row mo-form-wrapper">
        @yield('external-offer-filter')
        <div class="col-xs-12 no-padding" id="mo-blocks-wrapper">
            {{-- tab 1 --}}
            <div class="col-xs-12 col-sm-4 padding-lr5">
                <div class="col-xs-12 mo-block mo-offer-blocks">
                    <span class="col-xs-12 no-padding mo-block-txt text-capitalize padding-top-8" data-label="External Offers">external offers</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div>
            {{-- tab 2 --}}
            <div class="col-xs-12 col-sm-4 padding-lr5">
                <div class="col-xs-12 mo-block mo-offer-blocks">
                    <span class="col-xs-12 no-padding mo-block-txt text-capitalize padding-top-8" data-label="Offers Won">Offers Won</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div>
            {{-- tab 3 --}}
            <div class="col-xs-12 col-sm-4 padding-lr5">
                <div class="col-xs-12 mo-block mo-offer-blocks">
                    <span class="col-xs-12 no-padding mo-block-txt text-capitalize padding-top-8" data-label="Purchase History">Purchase History</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div>
        </div>


        {{--Start - moto External Offers Content Section--}}
        <div class="mo-row margin-top-32 hidden mo-block-list mo-offers-list" id="mo-external-offers-list">
            <div class="col-xs-12 no-padding" id="mo-incoming-offers-content">
            {{-- ROW --}}

            @if(isset($incoming_external_offer_list) && !empty($incoming_external_offer_list))
                @foreach($incoming_external_offer_list as $incoming_external_offer)
                <div class="col-xs-12 border-bottom margin-bottom-16">
                    <div class="mo-row">
                        {{--NAME--}}
                        <div class="col-xs-12 font-weight-bold margin-bottom-16">{{ isset($incoming_external_offer->offer_name) ? $incoming_external_offer->offer_name : "" }}</div>
                        {{--COLUMN 1 image --}}
                        <div class="col-xs-12 col-sm-3 padding-top-5 position-relative height-150"><img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{isset($incoming_external_offer->vehicle_id) ? $incoming_external_offer->vehicle_id : ''}}.jpg?{{rand()}}" width="" height="" alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/non_photo.png')}}'" class="img-responsive position-absolute-center max-height-150" /></div>
                        {{--COLUMN 2&3--}}
                        <div class="col-xs-12 col-sm-6 mo-offer-info">
                            <div class="col-xs-12 col-sm-6">Odometer: </div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">{{ isset($incoming_external_offer->odometer) ? $incoming_external_offer->odometer : "" }}</div>
                            <div class="col-xs-12 col-sm-6">Location: </div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">{{ isset($incoming_external_offer->location_text) ? $incoming_external_offer->location_text : "" }}</div>
                            <div class="col-xs-12 col-sm-6">Est Transport Cost:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">$ {{ isset($incoming_external_offer->est_transport_cost) ? number_format($incoming_external_offer->est_transport_cost, 2) : 0.00 }}</div>
                            <div class="col-xs-12 col-sm-6">Est Arrival:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold"> {{ isset($incoming_external_offer->arrival_date) ? date('d M Y', strtotime($incoming_external_offer->arrival_date)): "" }}</div>
                            <div class="col-xs-12 col-sm-6">Car Options:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold"> {{ isset($incoming_external_offer->car_option) && $incoming_external_offer->car_option == true ? "Yes" : "No" }} </div>
                            <div class="col-xs-12 col-sm-6">Total Reco Cost:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">$ {{ isset($incoming_external_offer->total_recon_cost) ? number_format($incoming_external_offer->total_recon_cost, 2) : 0.00 }} </div>
                        </div>
                        {{--COLUMN 4--}}
                        <div class="col-xs-12 col-sm-3 text-align-right mo-offer-buttons">
                            <a href="#mo-external-offer-bid-price-popup" class="open-external-offer-popup">
                                <div class="col-xs-12"><input type="button" class="mo-btn-green font-family-bold external-offer-bid-price-btn" value="BID" data-external-offer-id="{{isset($incoming_external_offer->external_offer_id)?$incoming_external_offer->external_offer_id:''}}"
                                data-valuation-id="{{isset($incoming_external_offer->valuation_id)?$incoming_external_offer->valuation_id:''}}"  /></div>
                            </a>
                            <a href="#mo-incoming-external-offer-delete-popup" class="open-external-offer-popup">
                                <div class="col-xs-12"><input type="button" class="mo-btn-red font-family-bold mo-external-offer-reject" value="DELETE"  data-external-offer-id="{{isset($incoming_external_offer->external_offer_id)?$incoming_external_offer->external_offer_id:''}}" data-vehicle-id="{{isset($incoming_external_offer->vehicle_id)?$incoming_external_offer->vehicle_id:''}}"/></div>
                            </a>
                            <div class="col-xs-12"><input type="button" class="mo-btn-yellow font-family-bold mo-more-info-btn" value="MORE INFO" data-external-offer-id="{{isset($incoming_external_offer->external_offer_id)?$incoming_external_offer->external_offer_id:''}}" /></div>
                        </div>
                    </div>
                    {{-- MORE INFO --}}
                    <div class="mo-row mo-condition-report" id="mo-condition-for-{{isset($incoming_external_offer->external_offer_id)?$incoming_external_offer->external_offer_id:''}}">
                        {{--CONDITION REPORT--}}

                        <div class="col-xs-12 col-sm-6 mo-offer-condition-report">
                            <div class="col-xs-12 no-padding font-weight-bold font-black-color margin-bottom-16 text-capitalize">condition report</div>
                            <div class="col-xs-12 col-sm-6">External:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ isset($incoming_external_offer->condition->exterior_section->total_cost) ? number_format($incoming_external_offer->condition->exterior_section->total_cost, 2) : "" }}</div>
                            
                                @if(isset($incoming_external_offer->condition->exterior_section->items))
                                    @foreach ($incoming_external_offer->condition->exterior_section->items as $exterior_section_item)
                                    <div class="col-xs-12 col-sm-6 padding-left-30">{{$exterior_section_item->name}}:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($exterior_section_item->cost, 2) }}</div>
                                    @endforeach
                                @endif
    
                            <div class="col-xs-12 col-sm-6">Internal:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ isset($incoming_external_offer->condition->interior_section->total_cost) ? number_format($incoming_external_offer->condition->interior_section->total_cost, 2) : "" }}</div>
                            
                                
                                @if(isset($incoming_external_offer->condition->interior_section->items))
                                    @foreach ($incoming_external_offer->condition->interior_section->items as $interior_section_item)
                                    <div class="col-xs-12 col-sm-6 padding-left-30">{{$interior_section_item->name}}:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($interior_section_item->cost, 2)}}</div>
                                    @endforeach
                                @endif
                                
                            
                            <div class="col-xs-12 col-sm-6">Mechanical:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ isset($incoming_external_offer->condition->mechanical_section->total_cost) ?  number_format($incoming_external_offer->condition->mechanical_section->total_cost, 2) : "" }}</div>
                            
                                @if(isset($incoming_external_offer->condition->mechanical_section->items))
                                    @foreach ($incoming_external_offer->condition->mechanical_section->items as $mechanical_section_item)
                                    <div class="col-xs-12 col-sm-6 padding-left-30">{{$mechanical_section_item->name}}:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($mechanical_section_item->cost, 2)}}</div>
                                    @endforeach
                                @endif    
                                
                            
                        </div>

                        @if(isset($incoming_external_offer->option_list) && $incoming_external_offer->option_list != null)
                        <div class="col-xs-12 col-sm-6 mo-offer-condition-report">
                                <div class="col-xs-12 no-padding font-weight-bold font-black-color margin-bottom-16 text-capitalize">Options/Extras</div>
                                @foreach($incoming_external_offer->option_list as $option)
                                    <div class="col-xs-12 col-sm-6">{{$option->name}}:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($option->price, 2)}}</div>
                                @endforeach    
                        </div>
                        @endif

                    </div>

                    
                </div>
                @endforeach
            @else
                <div class="col-xs-12">
                    <div class="mo-row text-center text-uppercase font-weight-bold height-1000">no item to display now</div>
                </div>
            @endif
            </div>
            <div class="mo-row text-center">
                <img class="mo-loading-icon" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
            </div>
            



        </div>
        {{--End - moto External Offers Content Section--}}


        {{--Start - moto Offers Won Content Section--}}
        <div class="mo-row margin-top-32 hidden mo-block-list mo-offers-list" id="mo-offers-won-list">
            <div class="col-xs-12 no-padding" id="mo-offers-won-content">
            @if(isset($external_offer_won_list) && !empty($external_offer_won_list) )
            {{-- ROW --}}
            @foreach ($external_offer_won_list as $external_offer_won)
            <div class="col-xs-12 no-padding">
                <div class="col-xs-12 border-bottom margin-bottom-16 padding-bottom-16">
                    <div class="mo-row">
                        {{--NAME--}}
                        <div class="col-xs-12 font-weight-bold font-black-color margin-bottom-16">
                            @if (isset($external_offer_won->math_wishlist) && $external_offer_won->math_wishlist)
                                <span title="wish list" class="wish-icon glyphicon glyphicon-star"></span>&nbsp;
                            @endif
                            {{isset($external_offer_won->offer_name) ? $external_offer_won->offer_name : ""}}
                        </div>
                        {{--COLUMN 1--}}
                        <div class="col-xs-12 col-sm-3 padding-top-5 position-relative height-150 hidden">
                            <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{isset($external_offer_won->vehicle_id) ? $external_offer_won->vehicle_id : ''}}.jpg?{{rand()}}"
                                 width="" height="" alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/non_photo.png')}}'"
                                 class="img-responsive position-absolute-center max-height-150" />
                        </div>
                        {{--COLUMN 2&3--}}
                        <div class="col-xs-12 col-sm-9 mo-offer-info">
                            <div class="col-xs-12 col-sm-6 no-padding">Won Bid Amount:</div>
                            <div class="col-xs-12 col-sm-6 no-padding font-weight-bold">$ {{isset($external_offer_won->bid_price) ? number_format($external_offer_won->bid_price, 2) : number_format(0.00, 2)}}</div>
                            <div class="col-xs-12 col-sm-6 no-padding">Date Bid Made:</div>
                            <div class="col-xs-12 col-sm-6 no-padding font-weight-bold">{{isset($external_offer_won->bid_date) ? date('d M Y h:i:s A', strtotime($external_offer_won->bid_date)) : ""}}</div>
                            <div class="col-xs-12 col-sm-6 no-padding">Buyer:</div>
                            <div class="col-xs-12 col-sm-6 no-padding font-weight-bold">{{isset($external_offer_won->buyer_name) ? $external_offer_won->buyer_name : ""}}</div>
                            <div class="col-xs-12 col-sm-6 no-padding">Est. Arrival Date:</div>
                            {{--<div class="col-xs-12 col-sm-6 no-padding font-weight-bold">{{isset($external_offer_won->arrival_date) ? date('d M Y', strtotime($external_offer_won->arrival_date)) : ""}}</div>--}}
                            <div class="col-xs-12 col-sm-6 no-padding font-weight-bold">{{isset($external_offer_won->expiry_date) ? date('d M Y', strtotime($external_offer_won->expiry_date)) : ""}}</div>
                        </div>
                        {{--COLUMN 4--}}
                        <div class="col-xs-12 col-sm-3 text-align-right mo-offer-buttons">
                            {{-- get name of status --}}
                            <div class="col-xs-12 font-weight-bold">{{isset($external_offer_won->offer_status_value) ? $external_offer_won->offer_status_value : ""}}</div>
                            @if($external_offer_won->offer_status == 4)
                                <div class="col-xs-12">
                                    <a href="#mo-incoming-external-offer-set-location-popup" class="open-external-offer-popup">
                                        <input type="button" class="mo-btn-yellow font-family-bold mo-external-offer-set-location-btn" data-external-offer-id="{{isset($external_offer_won->external_offer_id) ? $external_offer_won->external_offer_id:''}}" data-location-id="{{isset($external_offer_won->location_id) ? $external_offer_won->location_id:''}}" value="SET LOCATION" data-external-offer-id="{{isset($external_offer_won->external_offer_id) ? $external_offer_won->external_offer_id:''}}" />
                                    </a>
                                </div>
                            @endif
                            <div class="col-xs-12"><input type="button" class="mo-btn-yellow font-family-bold mo-more-info-btn" value="MORE INFO" data-external-offer-id="{{isset($external_offer_won->external_offer_id) ? $external_offer_won->external_offer_id:''}}" /></div>
                            
                        </div>
                    </div>
                    {{-- MORE INFO --}}
                    <div class="mo-row mo-condition-report" id="mo-condition-for-{{isset($external_offer_won->external_offer_id)?$external_offer_won->external_offer_id:''}}">
                        {{--CONDITION REPORT--}}
                        <div class="col-xs-12 col-sm-6 mo-offer-condition-report">
                            <div class="col-xs-12 no-padding font-weight-bold font-black-color margin-bottom-16 text-capitalize">condition report</div>
                            <div class="col-xs-12 col-sm-6">External:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ isset($external_offer_won->condition->exterior_section->total_cost) ? number_format($external_offer_won->condition->exterior_section->total_cost, 2) : "" }}</div>
                            
                                @if(isset($external_offer_won->condition->exterior_section->items) && !empty($external_offer_won->condition->exterior_section->items))
                                    @foreach ($external_offer_won->condition->exterior_section->items as $exterior_section_item)
                                    <div class="col-xs-12 col-sm-6 padding-left-30">{{$exterior_section_item->name}}:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($exterior_section_item->cost, 2)}}</div>
                                    @endforeach
                                @endif
    
                            <div class="col-xs-12 col-sm-6">Internal:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ isset($external_offer_won->condition->interior_section->total_cost) ? number_format($external_offer_won->condition->interior_section->total_cost, 2) : "" }}</div>
                            
                                
                                @if(isset($external_offer_won->condition->interior_section->items) && !empty($external_offer_won->condition->interior_section->items))
                                    @foreach ($external_offer_won->condition->interior_section->items as $interior_section_item)
                                    <div class="col-xs-12 col-sm-6 padding-left-30">{{$interior_section_item->name}}:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($interior_section_item->cost, 2)}}</div>
                                    @endforeach
                                @endif
                                
                            
                            <div class="col-xs-12 col-sm-6">Mechanical:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ isset($external_offer_won->condition->mechanical_section->total_cost) ? number_format($external_offer_won->condition->mechanical_section->total_cost, 2) : "" }}</div>
                            
                                @if(isset($external_offer_won->condition->mechanical_section->items) && !empty($external_offer_won->condition->mechanical_section->items))
                                    @foreach ($external_offer_won->condition->mechanical_section->items as $mechanical_section_item)
                                    <div class="col-xs-12 col-sm-6 padding-left-30">{{$mechanical_section_item->name}}:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($mechanical_section_item->cost, 2)}}</div>
                                    @endforeach
                                @endif    
                                
                            
                        </div>

                        @if(isset($external_offer_won->option_list) && !empty($external_offer_won->option_list))
                        <div class="col-xs-12 col-sm-6 mo-offer-condition-report">
                                <div class="col-xs-12 no-padding font-weight-bold font-black-color margin-bottom-16 text-capitalize">Options/Extras</div>
                                @foreach($external_offer_won->option_list as $option)
                                    <div class="col-xs-12 col-sm-6">{{$option->name}}:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($option->price, 2)}}</div>
                                @endforeach    
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
            {{-- END ROW --}}
            @else
                <div class="col-xs-12">
                    <div class="mo-row text-center text-uppercase font-weight-bold height-1000">no item to display now</div>
                </div>    
            @endif
            </div>
            {{-- TODO: must fix loading-icon --}}
            <div class="mo-row text-center">
                <img class="mo-loading-icon" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
            </div>
        </div>
        {{--End - moto Offers Won Content Section--}}


        {{--Start - moto Purchase History Content Section--}}
        <div class="mo-row margin-top-32 hidden mo-block-list mo-offers-list" id="mo-purchase-history-list">
            <div class="col-xs-12 no-padding" id="mo-purchase-history-content">
            @if(isset($external_offer_purchase_history_list) && !empty($external_offer_purchase_history_list))
            {{-- ROW --}}
            @foreach ($external_offer_purchase_history_list as $external_offer_purchase_history)
            <div class="col-xs-12 no-padding">
                <div class="col-xs-12 border-bottom margin-bottom-16 padding-bottom-16">
                    <div class="mo-row">
                        {{--NAME--}}
                        <div class="col-xs-12 font-weight-bold font-black-color margin-bottom-16">{{isset($external_offer_purchase_history->offer_name) ? $external_offer_purchase_history->offer_name : ""}}</div>
                        {{--COLUMN 1--}}
                        <div class="col-xs-12 col-sm-3 padding-top-5 position-relative height-150"><img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{isset($external_offer_purchase_history->vehicle_id) ? $external_offer_purchase_history->vehicle_id : ''}}.jpg?{{rand()}}" width="" height="" alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/non_photo.png')}}'" class="img-responsive position-absolute-center max-height-150" /></div>
                        {{--COLUMN 2&3--}}
                        <div class="col-xs-12 col-sm-6 mo-offer-info">
                            <div class="col-xs-12 col-sm-6 no-padding">Won Bid Amount:</div>
                            <div class="col-xs-12 col-sm-6 no-padding font-weight-bold">$ {{isset($external_offer_purchase_history->bid_price) ? number_format($external_offer_purchase_history->bid_price, 2) : number_format(0.00, 2)}}</div>
                            <div class="col-xs-12 col-sm-6 no-padding">Date Bid Made:</div>
                            <div class="col-xs-12 col-sm-6 no-padding font-weight-bold">{{isset($external_offer_purchase_history->bid_date) ? date('d M Y h:i:s A', strtotime($external_offer_purchase_history->bid_date)) : ""}}</div>
                            <div class="col-xs-12 col-sm-6 no-padding">Buyer:</div>
                            <div class="col-xs-12 col-sm-6 no-padding font-weight-bold">{{isset($external_offer_purchase_history->buyer_name) ? $external_offer_purchase_history->buyer_name : ""}}</div>
                            <div class="col-xs-12 col-sm-6 no-padding">Est. Arrival Date:</div>
                            <div class="col-xs-12 col-sm-6 no-padding font-weight-bold">{{isset($external_offer_purchase_history->arrival_date) ? date('d M Y', strtotime($external_offer_purchase_history->arrival_date)) : ""}}</div>
                        </div>
                        {{--COLUMN 4--}}
                        <div class="col-xs-12 col-sm-3 text-align-right mo-offer-buttons">
                            {{-- get name of status --}}
                            <div class="col-xs-12 font-weight-bold">{{isset($external_offer_purchase_history->offer_status_value) ? $external_offer_purchase_history->offer_status_value : ""}}</div>
                            <div class="col-xs-12"><input type="button" class="mo-btn-yellow font-family-bold mo-more-info-btn" value="MORE INFO" data-external-offer-id="{{isset($external_offer_purchase_history->external_offer_id) ? $external_offer_purchase_history->external_offer_id:''}}" /></div>
                        </div>
                    </div>
                    {{-- MORE INFO --}}
                    <div class="mo-row mo-condition-report" id="mo-condition-for-{{isset($external_offer_purchase_history->external_offer_id)?$external_offer_purchase_history->external_offer_id:''}}">
                        {{--CONDITION REPORT--}}
                        <div class="col-xs-12 col-sm-6 mo-offer-condition-report">
                            <div class="col-xs-12 no-padding font-weight-bold font-black-color margin-bottom-16 text-capitalize">condition report</div>
                            <div class="col-xs-12 col-sm-6">External:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ isset($external_offer_purchase_history->condition->exterior_section->total_cost) ? number_format($external_offer_purchase_history->condition->exterior_section->total_cost, 2) : "" }}</div>
                            
                                @if(isset($external_offer_purchase_history->condition->exterior_section->items) && !empty($external_offer_purchase_history->condition->exterior_section->items))
                                    @foreach ($external_offer_purchase_history->condition->exterior_section->items as $exterior_section_item)
                                    <div class="col-xs-12 col-sm-6 padding-left-30">{{$exterior_section_item->name}}:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($exterior_section_item->cost, 2)}}</div>
                                    @endforeach
                                @endif
    
                            <div class="col-xs-12 col-sm-6">Internal:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ isset($external_offer_purchase_history->condition->interior_section->total_cost) ? number_format($external_offer_purchase_history->condition->interior_section->total_cost, 2) : "" }}</div>
                            
                                
                                @if(isset($external_offer_purchase_history->condition->interior_section->items) && !empty($external_offer_purchase_history->condition->interior_section->items))
                                    @foreach ($external_offer_purchase_history->condition->interior_section->items as $interior_section_item)
                                    <div class="col-xs-12 col-sm-6 padding-left-30">{{$interior_section_item->name}}:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($interior_section_item->cost, 2)}}</div>
                                    @endforeach
                                @endif
                                
                            
                            <div class="col-xs-12 col-sm-6">Mechanical:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ isset($external_offer_purchase_history->condition->mechanical_section->total_cost) ? number_format($external_offer_purchase_history->condition->mechanical_section->total_cost, 2) : "" }}</div>
                            
                                @if(isset($external_offer_purchase_history->condition->mechanical_section->items) && !empty($external_offer_purchase_history->condition->mechanical_section->items))
                                    @foreach ($external_offer_purchase_history->condition->mechanical_section->items as $mechanical_section_item)
                                    <div class="col-xs-12 col-sm-6 padding-left-30">{{$mechanical_section_item->name}}:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($mechanical_section_item->cost, 2)}}</div>
                                    @endforeach
                                @endif    
                                
                            
                        </div>

                        @if(isset($external_offer_purchase_history->option_list) && !empty($external_offer_purchase_history->option_list))
                        <div class="col-xs-12 col-sm-6 mo-offer-condition-report">
                                <div class="col-xs-12 no-padding font-weight-bold font-black-color margin-bottom-16 text-capitalize">Options/Extras</div>
                                @foreach($external_offer_purchase_history->option_list as $option)
                                    <div class="col-xs-12 col-sm-6">{{$option->name}}:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($option->price, 2)}}</div>
                                @endforeach    
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
            {{-- END ROW --}}
            @else
                <div class="col-xs-12">
                    <div class="mo-row text-center text-uppercase font-weight-bold height-1000">no item to display now</div>
                </div>   
            @endif
            </div>
            <div class="mo-row text-center">
                <img class="mo-loading-icon" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
            </div>
        </div>
        {{--End - moto Purchase History Content Section--}}
    </div>
    {{--End - moto 3 TABs Section--}}

    {{--Start - moto Popup Section--}}
    {{--External Offer (incoming) bid price --}}
    <div id="mo-external-offer-bid-price-popup" class="mo-white-popup mfp-hide mo-external-offer-bid-price-popup">
        <form class="mo-popup-form" id="mo-external-offer-bid-price-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">Offer Bid</span>
            </div>

            <hr class="mo-form-hr" />
            
            <!-- ROW1 -->
            <div class="mo-row padding-top-10">
                <div class="alert alert-info col-xs-12 hidden bid-price-info">
                    <strong>Info!</strong> Session has been expired!.
                </div>
                <div class="form-group col-xs-12 col-sm-8">
                    <label for="">Expiry Date</label>
                    <div class="input-group date" id="">
                        <input type="text" class="form-control" id="external-offer-bid-price-expiry-date" name="external-offer-bid-price-expiry-date" placeholder="YYYY-MM-DD" />
                        <input type="hidden" value="{{$external_offer_expiry or ''}}" id="external-offer-expiry">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <!-- ROW1 -->

            <!-- ROW2 -->
            <div class="mo-row padding-top-10">
                <div class="form-group col-xs-12 col-sm-8">
                    <label for="">Price</label> <br/>
                    
                    <input type="text" class="form-control" id="external-offer-bid-price-input" name="external-offer-bid-price-input" placeholder="" />

                </div>
            </div>
            <!-- ROW2 -->


            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="submit" id="mo-external-offer-bid-price-submit-btn">
                </div>
            </div>
        </form>
    </div>
    {{--End - External Offer (incoming) bid price --}}

    {{-- Delete External Offer (incoming) --}}
    <div id="mo-incoming-external-offer-delete-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">delete item</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <span>Are you sure you want to delete this item?</span>
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-external-offer-incoming-delete-btn">
                </div>
            </div>
        </form>
    </div>
    {{-- End Delete External Offer (incoming) --}}

    {{-- Set Location External Offer (incoming) --}}
    <div id="mo-incoming-external-offer-set-location-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">set location</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">  
                    <select class="col-xs-12 external-offer-won-location-selection" size="5" name="current-user-location" id="current-user-location">
                        @if(isset($user_locations))
                            @foreach($user_locations as $location)
                                <option value="{{$location->id}}">{{$location->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="submit" id="mo-external-offer-won-location-submit-btn">
                </div>
            </div>
        </form>
    </div>
    {{-- Set Location External Offer (incoming) --}}
    <div class="mo-loading-image hidden">Loading&#8230;</div>
    {{--End - moto Popup Section--}}
@endsection