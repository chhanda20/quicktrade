{{-- MOTORONE EXTERNAL OFFER FILTER --}}
	@section('external-offer-filter')
	{{-- FILTER SECTION --}}
        <div class="col-xs-12 padding-left-6 margin-bottom-24">

            
            <div class="col-xs-12 vertical-center">

                <div class="vertical-center">
                    <div data-toggle="dropdown" class="cursor-pointer">
                        <span class="filter-icon fa fa-filter external-offer-filter-icon"></span> 
                        <span class="filter-icon no-padding padding-left-5 padding-right-20 font-size16 font-warm-grey-color font-weight-bold">Filter: </span>
                    </div>

                    {{-- dropdown menu --}}
                    <div class="col-xs-4 col-sm-5 external-offer-filter-drop-down-menu dropdown-menu no-padding padding-top-17 font-size14 font-warm-grey-color">
                        <div class="col-xs-12 no-padding">
                            <div class="padding-bottom-16 col-xs-6 no-padding padding-left-10">
                                <span class="float-right text-uppercase font-family-bold font-black-color">filter</span>
                            </div>

                            <hr class="col-xs-12 no-padding margin-bottom-16" />

                            {{-- filter row --}}
                            <div class="padding-bottom-16 col-xs-12 no-padding">
                                <div class="dropdown-submenu">
                                    <div class="cursor-pointer dropdown-menu-custom" id="">
                                        <div class="col-xs-11 no-padding padding-left-16">Make/Model</div>
                                        <div class="col-xs-1">
                                            <span class="glyphicon glyphicon-menu-right float-right"></span>
                                        </div>
                                    </div>
                                    {{----}}

                                    <div class="dropdown-menu col-xs-12 no-padding dropdown-submenu-custom background-transparent" id="mo-2nd-level">
                                        {{-- make --}}
                                        @if(isset($external_offer_filter_make_list) && !empty($external_offer_filter_make_list))
                                        <div class="col-xs-6 no-padding background-white border-top-right-bottom-grey">
                                        @foreach ($external_offer_filter_make_list as $key => $value)
                                        
                                        <div class="col-xs-12 no-padding">
                                            <div class="dropdown-submenu">
                                                <div class="dropdown-menu-custom cursor-pointer make">
                                                    <div class="no-padding padding-bottom-6 padding-top-10 padding-left-7 col-xs-2">
                                                        <input type="checkbox" class="mo-square-check external_offer_filter_make_checkbox" id="external_offer_filter_make_{{$key}}" name="external-offer-filter-make-checkbox[]" value="{{$value}}"/>
                                                        <label for="external_offer_filter_make_{{$key}}" class="text-capitalize disabled"><span class=""></span></label>
                                                    </div>
                                                    <div class="no-padding padding-bottom-14 padding-top-12 col-xs-8">{{$value}}</div>
                                                    <div class="col-xs-2 padding-top-12 float-right">
                                                        <span class="glyphicon glyphicon-menu-right"></span>
                                                    </div>
                                                </div>    
                                                {{-- model sub menu --}}
                                                <div class="dropdown-menu col-xs-12 no-padding hidden dropdown-model-submenu-custom" id="external-offer-filter-model-of-make-{{ str_replace(' ','',$value) }}">
                                                    
                                                    {{-- model submenu row --}}
                                                    {{-- <div class="no-padding padding-bottom-6 padding-top-10 padding-left-7 col-xs-2">
                                                        <input type="checkbox" class="mo-square-check" id="checkbox6" name="" value=""/>
                                                        <label for="checkbox6" class="text-capitalize disabled"><span class=""></span></label>
                                                    </div>
                                                    <div class="no-padding padding-bottom-16 padding-top-10 col-xs-10">
                                                        model 1
                                                    </div> --}}
                                                    {{-- model submenu row --}}
                                                    
                                                </div>
                                                {{-- end model submenu --}}
                                            </div>
                                        </div>
                                        <div class="list-divider col-xs-12"></div>
                                        
                                        @endforeach
                                        </div>
                                        @endif 
                                        {{-- make --}} 
                                    </div>
                                    {{-- --}}           
                                </div>
                            </div>
                            {{-- filter row --}}
                            <div class="list-divider col-xs-12"></div>
                            <div class="padding-top-16 padding-bottom-16 col-xs-12 no-padding">
                                <div class="dropdown-submenu">
                                    <div class="dropdown-menu-custom cursor-pointer">
                                        <div class="col-xs-11 no-padding padding-left-16">Variant</div>
                                        <div class="col-xs-1">
                                            <span class="glyphicon glyphicon-menu-right float-right"></span>
                                        </div>
                                    </div>
                                    {{-- variant submenu row --}}
                                    <div class="dropdown-submenu-custom dropdown-menu col-xs-12 no-padding hidden filter-variant" id="external-filter-variants-of-model">
                                        {{--<div>
                                            <div class="border-bottom no-padding padding-bottom-6 padding-top-10 padding-left-7 col-xs-2">
                                                <input type="checkbox" class="mo-square-check" id="checkbox6" name="" value=""/>
                                                <label for="checkbox6" class="text-capitalize disabled"><span class=""></span></label>
                                            </div>
                                            <div class="border-bottom no-padding padding-bottom-16 padding-top-10 col-xs-10">
                                                variant 1
                                            </div>
                                        </div>--}}
                                    </div>
                                    {{-- variant submenu row --}}

                                </div>
                                
                            </div>
                            <div class="list-divider col-xs-12"></div>
                            <div class="padding-top-16 padding-bottom-16 col-xs-12 no-padding">
                                <div class="dropdown-submenu">
                                    <div class="dropdown-menu-custom">
                                        <div class="col-xs-11 no-padding padding-left-16">Odo Range</div>
                                        <div class="col-xs-1">
                                            <span class="glyphicon glyphicon-menu-right float-right"></span>
                                        </div>
                                    </div>
                                    <div class="dropdown-submenu-custom dropdown-menu col-xs-12 no-padding padding-top-20 padding-bottom-20">
                                        <div class="position-relative col-xs-12">
                                            <div class="position-absolute-center width-100-percent text-align-center">

                                                <b class="padding-right-10">0 km</b> <input id="filter-odo-range-slider" type="text" class="" value="" data-slider-min="0" data-slider-max="1000000" data-slider-step="1" data-slider-value="[0,1000000]"/> <b class="padding-left-10">1,000,000 km</b>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="list-divider col-xs-12"></div>

                            <div class="padding-top-16 padding-bottom-16 col-xs-12 no-padding">
                                <div class="dropdown-submenu">
                                    <div class="dropdown-menu-custom">
                                        <div class="col-xs-11 no-padding padding-left-16">Year Range</div>
                                        <div class="col-xs-1">
                                            <span class="glyphicon glyphicon-menu-right float-right"></span>
                                        </div>
                                    </div>
                                    <div class="dropdown-submenu-custom dropdown-menu col-xs-12 no-padding padding-top-20 padding-bottom-20">
                                        <div class="position-relative col-xs-12">
                                            <div class="position-absolute-center width-100-percent text-align-center">
                                                <b class="padding-right-10">1975</b> <input id="filter-year-range-slider" type="text" class="" value="" data-slider-min="1975" data-slider-max="{{date("Y")}}" data-slider-step="1" data-slider-value="[1975,{{date("Y")}}]"/> <b class="padding-left-10">{{date("Y")}}</b>
                                            </div>    
                                        </div>    
                                    </div>
                                </div>
                            </div>

                            <div class="list-divider col-xs-12"></div>

                            

                            <div class="padding-top-16 padding-bottom-16 col-xs-12 no-padding">
                                <div class="dropdown-submenu">
                                    <div class="dropdown-menu-custom">
                                        <div class="col-xs-11 no-padding padding-left-16">Location</div>
                                        <div class="col-xs-1">
                                            <span class="glyphicon glyphicon-menu-right float-right"></span>
                                        </div>
                                    </div>
                                    <div class="dropdown-submenu-custom dropdown-menu col-xs-12 no-padding">
                                        <div class="form-group col-xs-12 no-padding filter-location">  
                                            @if(isset($user_locations))
                                                @foreach($user_locations as $location)
                                                <div>
                                                    <div class="no-padding padding-left-16 padding-bottom-6 padding-top-10 col-xs-2">
                                                        <input type="checkbox" class="mo-square-check filter-location-checkbox" id="location-{{$location->id}}" name="filter-location[]" value="{{$location->id}}"/>
                                                        <label for="location-{{$location->id}}" class="text-capitalize disabled"><span class=""></span></label>
                                                    </div>
                                                    <div class="no-padding padding-bottom-16 padding-top-10 col-xs-10">
                                                        {{$location->address}}
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 list-divider"></div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="list-divider col-xs-12"></div>
                            
                            <div class="col-xs-12 no-padding padding-top-16">
                                <div class="dropdown-submenu">
                                    <div class="dropdown-menu-custom">
                                        <div class="col-xs-11 no-padding padding-left-16">Transmission Type</div>
                                        <div class="col-xs-1">
                                            <span class="glyphicon glyphicon-menu-right float-right"></span>
                                        </div>
                                    </div>
                                  
                                    <div class="dropdown-submenu-custom dropdown-menu col-xs-12 no-padding">
                                        <div class="form-group col-xs-12 no-padding filter-transmission-type">  
                                            @if(isset($external_offer_filter_transmission_type))
                                                @foreach($external_offer_filter_transmission_type as $index => $value)
                                                <div>
                                                    <div class="no-padding padding-left-16 padding-bottom-6 padding-top-10 col-xs-2">
                                                        <input type="checkbox" class="mo-square-check filter-transmission-checkbox" id="transmission-{{$index}}" name="filter-transmission[]" value="{{$value}}"/>
                                                        <label for="transmission-{{$index}}" class="text-capitalize disabled"><span class=""></span></label>
                                                    </div>
                                                    <div class="no-padding padding-bottom-16 padding-top-10 col-xs-10">
                                                        {{$value}}
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 list-divider"></div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div> 

                            </div>

                            <hr class="col-xs-12 no-padding margin-top-16" />

                            <div class="col-xs-12 no-padding">
                                <div class="dropdown-item dropdown-buttons">
                                    <input type="button" id="external-offer-filter-reset-btn" class="btn col-xs-6 text-uppercase font-weight-bold no-border border-right-white" value="reset">
                                    <input type="button" id="external-offer-filter-save-btn" class="btn col-xs-6 text-uppercase font-weight-bold" value="apply">
                                </div>
                            </div>

                        </div>
                    </div>
                    {{-- end dropdown menu --}}

                </div>

                <div class="padding-right-30 font-size14 font-black-color font-weight-bold">
                    <div data-toggle="dropdown" class="cursor-pointer padding-top-4">
                        <span>Date Range</span>
                        <span class="glyphicon glyphicon-menu-down font-size11"></span>
                    </div>
                    {{-- dropdown menu --}}
                    <div class="col-xs-12 col-sm-3 col-md-2 no-padding dropdown-menu external-offer-filter-drop-down-menu padding-top-10 padding-bottom-10 font-size14">
                        
                        <div class="no-padding padding-left-10 col-xs-12 border-bottom">
                            <div class="no-padding col-xs-3">
                                <input type="radio" class="mo-round-check" id="filter-last-7-days" name="filter-date-range" value="last_7_days">
                                <label for="filter-last-7-days" class="text-capitalize"><span class=""></span></label>
                            </div>
                            <div class="no-padding col-xs-9 padding-top-3">
                                Last 7 Days
                            </div>
                        </div>

                        <div class="no-padding padding-left-10 border-bottom col-xs-12 padding-top-10">
                            <div class="no-padding col-xs-3">
                                <input type="radio" class="mo-round-check" id="filter-last-14-days" name="filter-date-range" value="last_14_days">
                                <label for="filter-last-14-days" class="text-capitalize"><span class=""></span></label>
                            </div>
                            <div class="no-padding col-xs-9 padding-top-3">
                                Last 14 Days
                            </div>
                        </div>
                        <div class="no-padding padding-left-10 border-bottom col-xs-12 padding-top-10">
                            <div class="dropdown-menu-custom">
                                <div class="no-padding col-xs-3">
                                    <input type="radio" class="mo-round-check" id="filter-last-month" name="filter-date-range" value="last_month">
                                    <label for="filter-last-month" class="text-capitalize"><span class=""></span></label>
                                </div>
                                <div class="no-padding col-xs-9 padding-top-3">
                                    Last Month
                                </div>
                            </div>
                        </div>
                        <div class="dropdown-submenu padding-top-10 padding-left-10">
                            <div class="no-padding dropdown-menu-custom cursor-pointer col-xs-12 padding-top-10">
                                <div class="">
                                    <div class="no-padding col-xs-3">
                                        <input type="radio" class="mo-round-check" id="filter-custom-range" name="filter-date-range" value="custome_date_range" checked>
                                        <label for="filter-custom-range" class="text-capitalize"><span class=""></span></label>
                                    </div>
                                    <div class="no-padding col-xs-9 padding-top-3">
                                        Custom Range
                                    </div>
                                </div>
                            </div>
                            <div class="width-400 dropdown-submenu-custom dropdown-menu no-padding padding-left-20 padding-right-20 padding-top-20 padding-bottom-20">
                                {{-- date from to here --}}
                                <div class="col-xs-12">
                                    <div>From</div>
                                    <div class="input-group date" id="external-offer-arrival-date-picker">
                                        <input type="text" class="form-control" id="external-offer-custom-date-range-from" name="external-offer-custom-date-range-from" placeholder="YYYY-MM-DD">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-xs-12 padding-top-20">
                                    <div>To</div>
                                    <div class="input-group date" id="external-offer-arrival-date-picker">
                                        <input type="text" class="form-control" id="external-offer-custom-date-range-to" name="external-offer-custom-date-range-to" placeholder="YYYY-MM-DD">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- end dropdown menu --}}
                </div>

                {{--status section--}}
                <div class="font-size14 font-black-color font-weight-bold filter-status-section hidden">
                    <div data-toggle="dropdown" class="cursor-pointer padding-top-4">
                        <span class="">Status</span>
                        <span class="glyphicon glyphicon-menu-down font-size11"></span>
                    </div>
                    {{-- dropdown menu --}}
                    <div class="external-offer-filter-drop-down-menu dropdown-menu font-size14 col-xs-12 col-sm-6 col-md-4 no-padding">
                        <div class="col-xs-12 no-padding" id="external-offer-filter-status-items">
                            @if(isset($offer_won_statuses_buyer))
                                @foreach($offer_won_statuses_buyer as $status)
                                    <div class="padding-left-16 padding-bottom-16 padding-top-16 col-xs-12 no-padding">
                                        <div class="col-xs-2 no-padding">
                                            <input type="checkbox" checked class="mo-square-check" id="filter-status-{{$status->code}}" name="filter-status[]" value="{{$status->code}}"/>
                                            <label for="filter-status-{{$status->code}}" class="text-capitalize disabled"><span class=""></span></label>
                                        </div>
                                        <div class="col-xs-6 no-padding">{{trim($status->value)}}</div>
                                    </div>
                                    <div class="list-divider col-xs-12"></div>
                                @endforeach
                            @elseif(isset($offer_received_statuses_seller))
                                @foreach($offer_received_statuses_seller as $status)
                                    <div class="padding-left-16 padding-bottom-16 padding-top-16 col-xs-12 no-padding">
                                        <div class="col-xs-2 no-padding">
                                            <input type="checkbox" checked class="mo-square-check" id="filter-status-{{$status->code}}" name="filter-status[]" value="{{$status->code}}"/>
                                            <label for="filter-status-{{$status->code}}" class="text-capitalize disabled"><span class=""></span></label>
                                        </div>
                                        <div class="col-xs-6 no-padding">{{trim($status->value)}}</div>
                                    </div>
                                    <div class="list-divider col-xs-12"></div>
                                @endforeach
                            @endif
                            {{--
                            <div class="padding-left-16 padding-bottom-16 padding-top-16 col-xs-12 no-padding">
                                <div class="col-xs-2 no-padding">
                                    <input type="checkbox" class="mo-square-check" id="filter-status-pending-acceptance" name="filter-status[]" value="3"/>
                                    <label for="filter-status-pending-acceptance" class="text-capitalize disabled"><span class=""></span></label>
                                </div>
                                <div class="col-xs-6 no-padding">Pending Acceptance</div>
                            </div>
                            <div class="list-divider col-xs-12"></div>
                            <div class="padding-left-16 padding-bottom-16 padding-top-16 col-xs-12 no-padding">
                                <div class="col-xs-2 no-padding">
                                    <input type="checkbox" class="mo-square-check" id="filter-status-accepted" name="filter-status[]" value="4"/>
                                    <label for="filter-status-accepted" class="text-capitalize disabled"><span class=""></span></label>
                                </div>
                                <div class="col-xs-6 no-padding">Accepted</div>
                            </div>
                            <div class="list-divider col-xs-12"></div>
                            <div class="padding-left-16 padding-bottom-16 padding-top-16 col-xs-12 no-padding">
                                <div class="col-xs-2 no-padding">
                                    <input type="checkbox" class="mo-square-check" id="filter-status-awaiting-payment" name="filter-status[]" value="6"/>
                                    <label for="filter-status-awaiting-payment" class="text-capitalize disabled"><span class=""></span></label>
                                </div>
                                <div class="col-xs-6 no-padding">Awaiting Payment</div>
                            </div>
                            <div class="list-divider col-xs-12"></div>
                            <div class="col-xs-12 no-padding padding-left-16 padding-top-16">
                                <div class="col-xs-2 no-padding">
                                    <input type="checkbox" class="mo-square-check" id="filter-status-awaiting-arrival" name="filter-status[]" value="7"/>
                                    <label for="filter-status-awaiting-arrival" class="text-capitalize disabled"><span class=""></span></label>
                                </div>
                                <div class="col-xs-6 no-padding">Awaiting Arrival</div>
                            </div>
                            --}}
                        </div>
                    </div>
                    {{-- end dropdown menu --}}
                </div>
                {{--end status section--}}
            </div>
            

        </div>
        {{-- END FILTER SECTION --}}
    @endsection    
{{-- END MOTORONE EXTERNAL OFFER FILTER --}}