@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
    @include('main.external-offer.external-offer-filter')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <div class="col-xs-12 col-sm-8 col-md-8 no-padding">
                {{ $header or 'Offers' }} > <span class="yellow-text">{{ $breadcrumb or 'Outgoing Offers' }}</span>
            </div>
        </div>
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - moto 3 TABs Section--}}
    <div class="mo-row mo-form-wrapper">
        {{-- FILTER HERE --}}
        @yield('external-offer-filter')
        <div class="col-xs-12 no-padding" id="mo-blocks-wrapper">
            <div class="col-xs-12 col-sm-4 padding-lr5">
                <div class="col-xs-12 mo-block mo-offer-blocks">
                    <span class="col-xs-12 no-padding mo-block-txt text-capitalize padding-top-8" data-label="Submitted Offer">Submitted Offer</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 padding-lr5">
                <div class="col-xs-12 mo-block mo-offer-blocks">
                    <span class="col-xs-12 no-padding mo-block-txt text-capitalize padding-top-8" data-label="Offers Received">Offers Received</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 padding-lr5">
                <div class="col-xs-12 mo-block mo-offer-blocks">
                    <span class="col-xs-12 no-padding mo-block-txt text-capitalize padding-top-8" data-label="Sold History">Sold History</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div>
        </div>


        {{--Start - moto Submitted Offer Content Section--}}
        <div class="mo-row margin-top-32 hidden mo-block-list mo-offers-list" id="mo-submitted-offers-list">
            <div class="col-xs-12 no-padding" id="mo-submitted-offers-content">
                @if(isset($submitted_offers_list) && is_array($submitted_offers_list) && !empty($submitted_offers_list))
                    @foreach($submitted_offers_list as $offer)
                        <div class="col-xs-12 border-bottom margin-bottom-16">
                            <div class="mo-row">
                                {{ csrf_field() }}
                                {{--NAME--}}
                                <div class="col-xs-12 font-weight-bold margin-bottom-16">{{$offer->offer_name}}</div>
                                {{--COLUMN 1--}}
                                <div class="col-xs-12 col-sm-3 padding-top-5 position-relative height-150"><img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{isset($offer->vehicle_id) ? $offer->vehicle_id : ''}}.jpg?{{rand()}}" width="" height="" alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/non_photo.png')}}'" class="img-responsive position-absolute-center max-height-150" /></div>
                                {{--COLUMN 2&3--}}
                                <div class="col-xs-12 col-sm-6 mo-offer-info">
                                    <div class="col-xs-12 col-sm-6">Odometer:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">{{$offer->odometer}}</div>
                                    <div class="col-xs-12 col-sm-6">Location:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">{{$offer->location_text}}</div>
                                    <div class="col-xs-12 col-sm-6">Est Transport Cost:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->est_transport_cost, 2)}}</div>
                                    <div class="col-xs-12 col-sm-6">Est Arrival:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">{{date('d M Y', strtotime($offer->arrival_date))}}</div>
                                    <div class="col-xs-12 col-sm-6">Car Options:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">{{($offer->car_option)?'Yes':'No'}}</div>
                                    <div class="col-xs-12 col-sm-6">Total Reco Cost:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->total_recon_cost, 2)}}</div>
                                </div>
                                {{--COLUMN 4--}}
                                <div class="col-xs-12 col-sm-3 text-align-right mo-offer-buttons">
                                    <div class="col-xs-12 font-weight-bold">{{isset($offer->offer_status_value) ? $offer->offer_status_value : ""}}</div>
                                    <div class="col-xs-12"><input type="button" class="mo-btn-black2 font-family-bold mo-go-to-review" value="VALUATION" data-valuation-id="{{isset($offer->valuation_id)?$offer->valuation_id:''}}" /></div>
                                    <div class="col-xs-12"><input type="button" class="mo-btn-yellow font-family-bold mo-more-info-btn" value="MORE INFO" data-external-offer-id="{{$offer->external_offer_id}}" /></div>
                                </div>
                                <div class="mo-row mo-condition-report" id="mo-condition-for-{{$offer->external_offer_id}}">
                                    {{--CONDITION REPORT--}}
                                    <div class="col-xs-12 col-sm-6 mo-offer-condition-report">
                                        <div class="col-xs-12 no-padding font-weight-bold font-black-color margin-bottom-16 text-capitalize">condition report</div>
                                        <div class="col-xs-12 col-sm-6">External:</div>
                                        <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->condition->exterior_section->total_cost, 2)}}</div>
                                        @if(isset($offer->condition->exterior_section->items) && !empty($offer->condition->exterior_section->items))
                                            @foreach($offer->condition->exterior_section->items as $item)
                                                <div class="col-xs-12 col-sm-6 padding-left-30">{{$item->name}}:</div>
                                                <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($item->cost, 2)}}</div>
                                            @endforeach
                                        @endif
                                        <div class="col-xs-12 col-sm-6">Internal:</div>
                                        <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->condition->interior_section->total_cost, 2)}}</div>
                                        @if(isset($offer->condition->interior_section->items) && !empty($offer->condition->interior_section->items))
                                            @foreach($offer->condition->interior_section->items as $item)
                                                <div class="col-xs-12 col-sm-6 padding-left-30">{{$item->name}}:</div>
                                                <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($item->cost, 2)}}</div>
                                            @endforeach
                                        @endif
                                        <div class="col-xs-12 col-sm-6">Mechanical:</div>
                                        <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->condition->mechanical_section->total_cost, 2)}}</div>
                                        @if(isset($offer->condition->mechanical_section->items) && !empty($offer->condition->mechanical_section->items))
                                            @foreach($offer->condition->mechanical_section->items as $item)
                                                <div class="col-xs-12 col-sm-6 padding-left-30">{{$item->name}}:</div>
                                                <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($item->cost, 2)}}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-xs-12 col-sm-6 mo-offer-condition-report">
                                        @if(isset($offer->option_list) && !empty($offer->option_list))
                                            <div class="col-xs-12 no-padding font-weight-bold font-black-color margin-bottom-16 text-capitalize">Options/Extras</div>
                                            @foreach($offer->option_list as $option_list)
                                                <div class="col-xs-12 col-sm-6">{{$option_list->name}}:</div>
                                                <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($option_list->price, 2)}}</div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-xs-12">
                        <div class="mo-row text-center text-uppercase font-weight-bold">no item to display now</div>
                    </div>
                @endif
            </div>
            <div class="mo-row text-center">
                <img class="mo-loading-icon" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
            </div>
        </div>
        {{--End - moto External Offers Content Section--}}


        {{--Start - moto Offers Received Content Section--}}
        <div class="mo-row margin-top-32 hidden mo-block-list mo-offers-list" id="mo-offers-received-list">
            <div class="col-xs-12 no-padding" id="mo-offers-received-content">
                @if(isset($received_offers_list) && is_array($received_offers_list) && !empty($received_offers_list))
                    @foreach($received_offers_list as $offer)
                        <div class="col-xs-12 border-bottom margin-bottom-16">
                            <div class="mo-row">
                                {{ csrf_field() }}
                                {{--NAME--}}
                                <div class="col-xs-12 font-weight-bold margin-bottom-16">{{$offer->offer_name}}</div>
                                {{--COLUMN 1--}}
                                <div class="col-xs-12 col-sm-3 padding-top-5 position-relative height-150"><img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{isset($offer->vehicle_id) ? $offer->vehicle_id : ''}}.jpg?{{rand()}}" width="" height="" alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/non_photo.png')}}'" class="img-responsive position-absolute-center max-height-150" /></div>
                                {{--COLUMN 2&3--}}
                                <div class="col-xs-12 col-sm-6 mo-offer-info">
                                    <div class="col-xs-12 col-sm-6">Odometer:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">{{$offer->odometer}}</div>
                                    <div class="col-xs-12 col-sm-6">Location:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">{{$offer->location}}</div>
                                    <div class="col-xs-12 col-sm-6">Est Transport Cost:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{number_format($offer->est_transport_cost, 2)}}</div>
                                    <div class="col-xs-12 col-sm-6">Est Arrival:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">{{date('d M Y', strtotime($offer->arrival_date))}}</div>
                                    <div class="col-xs-12 col-sm-6">Car Options:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">{{($offer->car_option)?'Yes':'No'}}</div>
                                    <div class="col-xs-12 col-sm-6">Total Reco Cost:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{number_format($offer->total_recon_cost, 2)}}</div>
                                </div>
                                {{--COLUMN 4--}}
                                <div class="col-xs-12 col-sm-3 text-align-right mo-offer-buttons">
                                    <div class="col-xs-12 font-weight-bold">{{isset($offer->offer_status_value) ? $offer->offer_status_value : ""}}</div>
                                    @if(isset($offer->offer_status) && $offer->offer_status == 3)
                                        <div class="col-xs-12"><input type="button" class="mo-btn-green font-family-bold mo-approve-offer" value="ACCEPT" data-external-offer-id="{{$offer->external_offer_id}}" /></div>
                                        <div class="col-xs-12">
                                            <a href="#mo-suspend-popup" class="mo-suspend-popup">
                                                <input type="button" class="mo-btn-red font-family-bold mo-reject-offer" value="REJECT" data-external-offer-id="{{$offer->external_offer_id}}" />
                                            </a>
                                        </div>
                                    @endif
                                    <div class="col-xs-12"><input type="button" class="mo-btn-black2 font-family-bold mo-go-to-review" value="VALUATION" data-valuation-id="{{isset($offer->valuation_id)?$offer->valuation_id:''}}" /></div>
                                    <div class="col-xs-12"><input type="button" class="mo-btn-yellow font-family-bold mo-more-info-btn" value="MORE INFO" data-external-offer-id="{{$offer->external_offer_id}}" /></div>
                                </div>
                            </div>
                            <div class="mo-row mo-condition-report" id="mo-condition-for-{{$offer->external_offer_id}}">
                                {{--CONDITION REPORT--}}
                                <div class="col-xs-12 col-sm-6 mo-offer-condition-report">
                                    <div class="col-xs-12 no-padding font-weight-bold font-black-color margin-bottom-16 text-capitalize">condition report</div>
                                    <div class="col-xs-12 col-sm-6">External:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->condition->exterior_section->total_cost, 2)}}</div>
                                    @if(isset($offer->condition->exterior_section->items) && !empty($offer->condition->exterior_section->items))
                                        @foreach($offer->condition->exterior_section->items as $item)
                                            <div class="col-xs-12 col-sm-6 padding-left-30">{{$item->name}}:</div>
                                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($item->cost, 2)}}</div>
                                        @endforeach
                                    @endif
                                    <div class="col-xs-12 col-sm-6">Internal:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->condition->interior_section->total_cost, 2)}}</div>
                                    @if(isset($offer->condition->interior_section->items) && !empty($offer->condition->interior_section->items))
                                        @foreach($offer->condition->interior_section->items as $item)
                                            <div class="col-xs-12 col-sm-6 padding-left-30">{{$item->name}}:</div>
                                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($item->cost, 2)}}</div>
                                        @endforeach
                                    @endif
                                    <div class="col-xs-12 col-sm-6">Mechanical:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->condition->mechanical_section->total_cost, 2)}}</div>
                                    @if(isset($offer->condition->mechanical_section->items) && !empty($offer->condition->mechanical_section->items))
                                        @foreach($offer->condition->mechanical_section->items as $item)
                                            <div class="col-xs-12 col-sm-6 padding-left-30">{{$item->name}}:</div>
                                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($item->cost, 2)}}</div>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-6 mo-offer-condition-report">
                                    @if(isset($offer->option_list) && !empty($offer->option_list))
                                        <div class="col-xs-12 no-padding font-weight-bold font-black-color margin-bottom-16 text-capitalize">Options/Extras</div>
                                        @foreach($offer->option_list as $option_list)
                                            <div class="col-xs-12 col-sm-6">{{$option_list->name}}:</div>
                                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($option_list->price, 2)}}</div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-xs-12">
                        <div class="mo-row text-center text-uppercase font-weight-bold">no item to display now</div>
                    </div>
                @endif
            </div>
            <div class="mo-row text-center">
                <img class="mo-loading-icon" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
            </div>
        </div>
        {{--End - moto Offers Received Content Section--}}


        {{--Start - moto Sold History Content Section--}}
        <div class="mo-row margin-top-32 hidden mo-block-list mo-offers-list" id="mo-sold-history-list">
            <div class="col-xs-12 no-padding" id="mo-sold-offers-content">
                @if(isset($sold_offers_list) && is_array($sold_offers_list) && !empty($sold_offers_list))
                    @foreach($sold_offers_list as $offer)
                        <div class="col-xs-12 border-bottom margin-bottom-16">
                            <div class="mo-row">
                                {{ csrf_field() }}
                                {{--NAME--}}
                                <div class="col-xs-12 font-weight-bold font-black-color margin-bottom-16">
                                    @if (isset($offer->math_wishlist) && $offer->math_wishlist)
                                        <span title="wish list" class="wish-icon glyphicon glyphicon-star"></span>&nbsp;
                                    @endif
                                    {{$offer->offer_name}}
                                </div>
                                {{--COLUMN 1--}}
                                <div class="col-xs-12 col-sm-3 padding-top-5 position-relative height-150"><img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{isset($offer->vehicle_id) ? $offer->vehicle_id : ''}}.jpg?{{rand()}}" width="" height="" alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/non_photo.png')}}'" class="img-responsive position-absolute-center max-height-150" /></div>
                                {{--COLUMN 2&3--}}
                                <div class="col-xs-12 col-sm-6 mo-offer-info">
                                    <div class="col-xs-12 col-sm-6">Won Bid Amount:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->bid_price, 2)}}</div>
                                    <div class="col-xs-12 col-sm-6">Date Bid Made:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">{{!empty($offer->bid_date)?date('d M Y', strtotime($offer->bid_date)):''}}</div>
                                    <div class="col-xs-12 col-sm-6">Buyer:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">{{$offer->buyer_name}}</div>
                                    <div class="col-xs-12 col-sm-6">Est. Arrival Date:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">{{!empty($offer->arrival_date)?date('d M Y', strtotime($offer->arrival_date)):''}}</div>
                                </div>
                                {{--COLUMN 4--}}
                                <div class="col-xs-12 col-sm-3 text-align-right mo-offer-buttons">
                                    <div class="col-xs-12 font-weight-bold">{{isset($offer->offer_status_value) ? $offer->offer_status_value : ""}}</div>
                                    <div class="col-xs-12"><input type="button" class="mo-btn-yellow font-family-bold mo-more-info-btn" value="MORE INFO" data-external-offer-id="{{$offer->external_offer_id}}" /></div>
                                </div>
                            </div>
                            <div class="mo-row mo-condition-report" id="mo-condition-for-{{$offer->external_offer_id}}">
                                {{--CONDITION REPORT--}}
                                <div class="col-xs-12 col-sm-6 mo-offer-condition-report">
                                    <div class="col-xs-12 no-padding font-weight-bold font-black-color margin-bottom-16 text-capitalize">condition report</div>
                                    <div class="col-xs-12 col-sm-6">External:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->condition->exterior_section->total_cost, 2)}}</div>
                                    @if(isset($offer->condition->exterior_section->items) && !empty($offer->condition->exterior_section->items))
                                        @foreach($offer->condition->exterior_section->items as $item)
                                            <div class="col-xs-12 col-sm-6 padding-left-30">{{$item->name}}:</div>
                                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($item->cost, 2)}}</div>
                                        @endforeach
                                    @endif
                                    <div class="col-xs-12 col-sm-6">Internal:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->condition->interior_section->total_cost, 2)}}</div>
                                    @if(isset($offer->condition->interior_section->items) && !empty($offer->condition->interior_section->items))
                                        @foreach($offer->condition->interior_section->items as $item)
                                            <div class="col-xs-12 col-sm-6 padding-left-30">{{$item->name}}:</div>
                                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($item->cost, 2)}}</div>
                                        @endforeach
                                    @endif
                                    <div class="col-xs-12 col-sm-6">Mechanical:</div>
                                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->condition->mechanical_section->total_cost, 2)}}</div>
                                    @if(isset($offer->condition->mechanical_section->items) && !empty($offer->condition->mechanical_section->items))
                                        @foreach($offer->condition->mechanical_section->items as $item)
                                            <div class="col-xs-12 col-sm-6 padding-left-30">{{$item->name}}:</div>
                                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($item->cost, 2)}}</div>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-6 mo-offer-condition-report">
                                    @if(isset($offer->option_list) && !empty($offer->option_list))
                                        <div class="col-xs-12 no-padding font-weight-bold font-black-color margin-bottom-16 text-capitalize">Options/Extras</div>
                                        @foreach($offer->option_list as $option_list)
                                            <div class="col-xs-12 col-sm-6">{{$option_list->name}}:</div>
                                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($option_list->price, 2)}}</div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @else
                        <div class="col-xs-12">
                            <div class="mo-row text-center text-uppercase font-weight-bold">no item to display now</div>
                        </div>
                    @endif
                </div>
                <div class="mo-row text-center">
                    <img class="mo-loading-icon" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
                </div>
            </div>
        </div>
        {{--End - moto Sold History Content Section--}}
        <div class="mo-loading-image hidden">Loading&#8230;</div>
        
    </div>
    {{--End - moto 3 TABs Section--}}

    {{------------ SUSPEND CONFIRMATION POPUP ------------}}
    <div id="mo-suspend-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">reject offer</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <span class="suspend-popup-content">Are you sure you want to reject this offer?</span>
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-confirm-reject-offer">
                </div>
            </div>
            <input type="hidden" id="mo-offer-id">
        </form>
    </div>
    {{--End - moto Popup Section--}}
@endsection