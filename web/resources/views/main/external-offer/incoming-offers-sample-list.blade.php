{{--Start - moto Offers Incoming Section--}}
@if(isset($offers_list) && is_array($offers_list) && !empty($offers_list))
    @foreach($offers_list as $offer)
        <div class="col-xs-12 border-bottom margin-bottom-16">
            <div class="mo-row">
                {{ csrf_field() }}
                {{--NAME--}}
                <div class="col-xs-12 font-weight-bold margin-bottom-16">{{$offer->offer_name}}</div>
                {{--COLUMN 1--}}
                <div class="col-xs-12 col-sm-3 padding-top-5 position-relative height-150"><img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{isset($offer->vehicle_id) ? $offer->vehicle_id : ''}}.jpg?{{rand()}}" width="" height="" alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/non_photo.png')}}'" class="img-responsive position-absolute-center max-height-150" /></div>
                {{--COLUMN 2&3--}}
                <div class="col-xs-12 col-sm-6 mo-offer-info">
                    @if($offer->offer_status != 3
                        && $offer->offer_status != 4
                        && $offer->offer_status != 5
                        && $offer->offer_status != 6
                        && $offer->offer_status != 7
                        && $offer->offer_status != 8)
                        <div class="col-xs-12 col-sm-6">Odometer:</div>
                        <div class="col-xs-12 col-sm-6 font-weight-bold">{{$offer->odometer}}</div>
                        <div class="col-xs-12 col-sm-6">Location:</div>
                        <div class="col-xs-12 col-sm-6 font-weight-bold">{{$offer->location_text}}</div>
                        <div class="col-xs-12 col-sm-6">Est Transport Cost:</div>
                        <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->est_transport_cost, 2)}}</div>
                        <div class="col-xs-12 col-sm-6">Est Arrival:</div>
                        <div class="col-xs-12 col-sm-6 font-weight-bold">{{date('d M Y', strtotime($offer->arrival_date))}}</div>
                        <div class="col-xs-12 col-sm-6">Car Options:</div>
                        <div class="col-xs-12 col-sm-6 font-weight-bold">{{($offer->car_option)?'Yes':'No'}}</div>
                        <div class="col-xs-12 col-sm-6">Total Reco Cost:</div>
                        <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->total_recon_cost, 2)}}</div>
                    @else
                        <div class="col-xs-12 col-sm-6">Won Bid Amount:</div>
                        <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->bid_price, 2)}}</div>
                        <div class="col-xs-12 col-sm-6">Date Bid Made:</div>
                        <div class="col-xs-12 col-sm-6 font-weight-bold">{{!empty($offer->bid_date)?date('d M Y', strtotime($offer->bid_date)):''}}</div>
                        <div class="col-xs-12 col-sm-6">Buyer:</div>
                        <div class="col-xs-12 col-sm-6 font-weight-bold">{{$offer->buyer_name}}</div>
                        <div class="col-xs-12 col-sm-6">Est. Arrival Date:</div>
                        <div class="col-xs-12 col-sm-6 font-weight-bold">{{!empty($offer->arrival_date)?date('d M Y', strtotime($offer->arrival_date)):''}}</div>
                    @endif
                </div>
                {{--COLUMN 4--}}
                <div class="col-xs-12 col-sm-3 text-align-right mo-offer-buttons">
                    @if($offer->offer_status != 1)
                        <div class="col-xs-12 font-weight-bold">{{$offer_status_array[$offer->offer_status]}}</div>
                    @endif
                    @if($offer->offer_status == 4)
                        <div class="col-xs-12">
                            <a href="#mo-incoming-external-offer-set-location-popup" class="open-external-offer-popup">
                                <input type="button" class="mo-btn-yellow font-family-bold mo-external-offer-set-location-btn" data-external-offer-id="{{isset($external_offer_won->external_offer_id) ? $external_offer_won->external_offer_id:''}}" data-location-id="{{isset($external_offer_won->location_id) ? $external_offer_won->location_id:''}}" value="SET LOCATION" data-external-offer-id="{{isset($external_offer_won->external_offer_id) ? $external_offer_won->external_offer_id:''}}" />
                            </a>
                        </div>
                    @elseif ($offer->offer_status == 1)
                        <div>
                            <a href="#mo-external-offer-bid-price-popup" class="open-external-offer-popup">
                                <div class="col-xs-12"><input type="button" class="mo-btn-green external-offer-bid-price-btn" value="BID" data-external-offer-id="{{isset($incoming_external_offer->external_offer_id)?$incoming_external_offer->external_offer_id:''}}"
                                data-valuation-id="{{isset($incoming_external_offer->valuation_id)?$incoming_external_offer->valuation_id:''}}"  /></div>
                            </a>
                            <a href="#mo-incoming-external-offer-delete-popup" class="open-external-offer-popup">
                                <div class="col-xs-12"><input type="button" class="mo-btn-red mo-external-offer-reject" value="DELETE"  data-external-offer-id="{{isset($incoming_external_offer->external_offer_id)?$incoming_external_offer->external_offer_id:''}}" data-vehicle-id="{{isset($incoming_external_offer->vehicle_id)?$incoming_external_offer->vehicle_id:''}}"/></div>
                            </a>
                        </div>    
                    @endif
                    <div class="col-xs-12"><input type="button" class="mo-btn-yellow font-family-bold mo-more-info-btn" value="MORE INFO" data-external-offer-id="{{$offer->external_offer_id}}" /></div>
                </div>
            </div>
            <div class="mo-row mo-condition-report" id="mo-condition-for-{{$offer->external_offer_id}}">
                {{--CONDITION REPORT--}}
                <div class="col-xs-12 col-sm-6 mo-offer-condition-report">
                    <div class="col-xs-12 no-padding font-weight-bold font-black-color margin-bottom-16 text-capitalize">condition report</div>
                    <div class="col-xs-12 col-sm-6">External:</div>
                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->condition->exterior_section->total_cost, 2)}}</div>
                    @if(isset($offer->condition->exterior_section->items) && !empty($offer->condition->exterior_section->items))
                        @foreach($offer->condition->exterior_section->items as $item)
                            <div class="col-xs-12 col-sm-6 padding-left-30">{{$item->name}}:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($item->cost, 2)}}</div>
                        @endforeach
                    @endif
                    <div class="col-xs-12 col-sm-6">Internal:</div>
                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->condition->interior_section->total_cost, 2)}}</div>
                    @if(isset($offer->condition->interior_section->items) && !empty($offer->condition->interior_section->items))
                        @foreach($offer->condition->interior_section->items as $item)
                            <div class="col-xs-12 col-sm-6 padding-left-30">{{$item->name}}:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($item->cost, 2) }}</div>
                        @endforeach
                    @endif
                    <div class="col-xs-12 col-sm-6">Mechanical:</div>
                    <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($offer->condition->mechanical_section->total_cost, 2)}}</div>
                    @if(isset($offer->condition->mechanical_section->items) && !empty($offer->condition->mechanical_section->items))
                        @foreach($offer->condition->mechanical_section->items as $item)
                            <div class="col-xs-12 col-sm-6 padding-left-30">{{$item->name}}:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($item->cost, 2)}}</div>
                        @endforeach
                    @endif
                </div>
                <div class="col-xs-12 col-sm-6 mo-offer-condition-report">
                    @if(isset($offer->option_list) && !empty($offer->option_list))
                        <div class="col-xs-12 no-padding font-weight-bold font-black-color margin-bottom-16 text-capitalize">Options/Extras</div>
                        @foreach($offer->option_list as $option_list)
                            <div class="col-xs-12 col-sm-6">{{$option_list->name}}:</div>
                            <div class="col-xs-12 col-sm-6 font-weight-bold">${{ number_format($option_list->price, 2)}}</div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    @endforeach
@else
    <div class="col-xs-12">
        <div class="mo-row text-center text-uppercase font-weight-bold">no item to display now</div>
    </div>
@endif
{{--End - moto Offers Incoming Content Section--}}
