@section('review-mechanical')

    {{----------------------------------------------------------- start MECHANICAL -----------------------------------------------------------}}

    {{--Start - moto Interior Content Section--}}
    <div class="hidden margin-top-32 mo-block-list" id="mo-mechanical-block">
        <div class="mo-form-wrapper">
            <form action="{!! url('save-mechanical-details/'.$valuation_id) !!}" method="post" id="mo-mechanical-form">
                {{ csrf_field() }}
                <input type="hidden" value="{{isset($valuation_id) ? $valuation_id : null}}" name="valuation_id">
                <div class="mo-row padding-bottom-32">
                    <div class="col-xs-12 padding-bottom-24 text-uppercase mo-form-header">
                        vehicle condition report
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                        <input {{$disableIt != ''?'disabled':''}} type="checkbox" class="mo-round-check" id="test-driven" name="test_driven" {{isset($mechanicalDetails->test_driven)&&$mechanicalDetails->test_driven == 1?'checked':''}} value="{{isset($mechanicalDetails->test_driven)?'has_value':''}}">
                        <label for="test-driven" class="check-icon"><span></span> Have you test driven the vehicle?</label>
                    </div>
                </div>
                <div class="mo-row">
                    <div class="col-xs-12 padding-bottom-24 text-uppercase mo-form-header">
                        mechanical report
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                        <textarea {{$disableIt != ''?'disabled':''}} class="mo-textarea form-control" rows="3" name="mechanical_report" placeholder="Add additional details" style="box-shadow: none;">{{isset($mechanicalDetails->mechanical_additional_detail)&&$mechanicalDetails->mechanical_additional_detail!=''?$mechanicalDetails->mechanical_additional_detail:null}}</textarea>
                    </div>
                    {{--Save Button--}}
                    {{--<div class="form-group col-xs-12 padding-top-20 {{$hideEditField or ''}}">
                        <input type="submit" id="mo-save-options-extras" class="mo-btn text-uppercase" value="next">
                    </div>--}}
                    @if (!in_array("valuation_perform",$permissions) && $status == 10)
                        {{--Request Valuation Button--}}
                        <div class="form-group col-xs-12 padding-top-20 {{$hideEditField or ''}}">
                            <input type="submit" class="mo-btn text-uppercase" id="mo-request-btn" name="submit" value="request valuation">
                        </div>
                    @endif
                    <div class="clear-both height20 padding-bottom-64"></div>
                </div>
            </form>
            <div class="col-xs-12 no-padding">
                <div class="col-xs-8"></div>
                <div class="col-xs-4">
                    <div class="mo-round-btn2 right-0" {{$hideEditField or ''}}><a href="#mo-edit-inspection-item-popup" class="open-popup-link mo-add-new-item" id="">NEW</a></div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 no-padding" id="mo-prev-next-button">
            {{--<input type="button" id="mo-prev" value="previous" class="text-uppercase float-left">
            <input type="button" id="mo-next" value="next" class="text-uppercase float-right">--}}
            <button type="button" id="mo-prev-mechanical" class="mo-prev text-uppercase float-left">
                <span class="glyphicon glyphicon-menu-left text-uppercase"></span><span class="mo-span-txt">previous</span>
            </button>
            @if(in_array("valuation_perform",$permissions))
                <button type="button" id="mo-next-mechanical" class="mo-next text-uppercase float-right">
                    <span class="mo-span-txt">next</span><span class="glyphicon glyphicon-menu-right text-uppercase"></span>
                </button>
            @endif
        </div>
        <div class="col-xs-12 no-padding" id="mo-estimated-cost">
            <ul class="mo-main-content no-padding">
                <li data-toggle="collapse" data-target="#mo-mechanical-sub-content" class="collapsed active">
                    <a href="javascript:void(0);">
                        <div id="mo-heading" class="vertical-center col-xs-12 no-padding">
                            <div class="mo-estimated-cost-wrapper col-xs-12 no-padding">

                                <div class="col-xs-12 col-sm-8 no-padding">
                                    Total Estimated Cost
                                </div>
                                <div class="col-xs-12 col-sm-4 no-padding text-right">
                                    <span class="col-xs-5 col-sm-6 no-padding">$<span id="mo-mechanical-total-estimate-cost">{{number_format($mechanical_total_cost, 2)}}</span></span>
                                    <span class="col-xs-2 col-sm-4 padding-lr5 mo-expand-hide-label">Expand</span>
                                    <span class="col-xs-5 col-sm-2 no-padding mo-icon-swipe glyphicon glyphicon-menu-down"></span>
                                </div>

                            </div>
                        </div>
                    </a>
                    <ul class="mo-sub-content collapse" id="mo-mechanical-sub-content">
                        @if (isset($mechanical_item_list))
                            <input type="hidden" value="{{json_encode($mechanical_item_list)}}" id="mo-mechanical-item-data">
                            {{--<li>
                                <div class="col-xs-12 no-padding border-bottom padding-tb15 font-size16">
                                    
                                    <div class="col-xs-12 col-sm-8 no-padding">
                                        General Reconditioning Costs
                                    </div>
                                    <div class="col-xs-12 col-sm-4 text-right no-padding">
                                        <span class="col-xs-6 no-padding font-avenir-demi">
                                            $<span class="font-size16">0</span>
                                        </span>
                                        <span class="col-xs-6 no-padding"></span>
                                    </div>
                                </div>
                            </li>--}}
                            @foreach ($mechanical_item_list as $items)
                                @foreach($items->items as $item)
                                    <li class="{{$items->section_type}}">
                                        <div class="col-xs-12 padding-tb10 border-bottom no-padding mo-cost-estimate-row" id="{{$item->inspection_item_id}}">
                                            
                                                <div class="col-xs-12 col-sm-8 no-padding name">
                                                    {{$mechanical_item_no++}}.{{$item->item}}
                                                    <div class="padding-top-5 font-size14 font-grey-color font-style-italic note">{{$item->note}}</div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 no-padding text-right">
                                                    <label class="col-xs-6 no-padding cursor-pointer {{!in_array("valuation_perform",$permissions)?'disabled-div':''}}" id="item_cost_{{$items->section_type}}_{{$item->inspection_item_id}}">
                                                        <span class="{{$disableEditField or ''}}">$</span><span class="mo-cost-number {{$disableEditField or ''}}">{{number_format($item->repair_cost, 2)}}</span>
                                                        <span class='mo-cost-number-input hide'><input type='number' name="estimate-cost" min="0" max="9999999999" maxlength = "13" oninput="maxLengthCheck(this)" value="" /></span>
                                                    </label>
                                                    <a href="#mo-edit-inspection-item-popup" class="col-xs-4 padding-lr5 open-popup-link">
                                                        <span class="mo-view-edit-label">View/Edit</span>
                                                    </a>
                                                    <span class="col-xs-2 no-padding {{$hideEditField or ''}}">
                                                        <a href="#mo-confirm-delete-exterior-popup" class="open-popup-link float-left padding-left-5">
                                                            <img src="{{ asset('images/svg/main/icon-remove.svg') }}" alt="Remove Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-remove.png') }}'" class="mo-delete-item">
                                                        </a>
                                                    </span>
                                                </div>
                                            
                                        </div>
                                    </li><!-- ROW 1 -->
                                @endforeach
                            @endforeach
                        @endif
                    </ul>
                </li>

            </ul>
        </div>
    </div>
    {{--End - moto Interior Content Section--}}

    {{----------------------------------------------------------- end MECHANICAL -----------------------------------------------------------}}

@endsection