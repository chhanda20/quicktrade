@if (isset($option_list))
    @for ($i = 0; $i < count($option_list); $i++)
        <div class="col-xs-12 no-padding">
            <div class="form-group col-xs-12 col-sm-5 padding-top-24 padding-bottom-8 border-bottom {{$i==0?'border-top':null}}">
                <label class="text-uppercase">
                    [{{$option_list[$i]->glass_option_id}}] {{$option_list[$i]->name}}
                </label>
            </div>
            <span class="hidden">{{$i++}}</span>
            @if($i<count($option_list))
                <div class="form-group col-xs-12 col-sm-5 padding-top-24 padding-bottom-8 border-bottom {{$i==1?'border-top':null}} float-right">
                    <label class="text-uppercase">
                        [{{$option_list[$i]->glass_option_id}}] {{$option_list[$i]->name}}
                    </label>
                </div>
            @endif
        </div>
    @endfor
@endif