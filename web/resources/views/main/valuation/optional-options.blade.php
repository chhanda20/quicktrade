@if (isset($optional_options_list))
    @foreach ($optional_options_list as $key => $optional_option)
        <div class="form-group col-xs-12 col-sm-6 col-md-6 padding-bottom-20">
            <input {{$disableIt != ''?'disabled':''}} type="checkbox" class="mo-round-check" id="{{$optional_option->name}}" name="optional_options[]" {{$optional_option->selected!=false?'checked':null}} value="{{$key}}">
            <label for="{{$optional_option->name}}" class="text-capitalize"><span></span>{{$optional_option->name}}</label>
        </div>
    @endforeach
@endif