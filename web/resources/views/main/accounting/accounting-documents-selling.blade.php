<div class="mo-row">
    <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 padding-lr5 text-uppercase bold">
        2014 maserati quattroporte white
    </div>
    <div class="col-xs-12 col-sm-3 padding-lr5">
        <form class="vertical-center" method="post" id="mo-vehicle-image-upload-form">
            <div class="mo-vehicle-image-wrapper" class="vertical-center">
                {{--<img src="http://i1.wp.com/www.inautonews.com/wp-content/uploads/2011/10/maserati-granturismo-mc-stradale-novitec-tridente-11.jpg?resize=350%2C200" alt="" class="img-responsive">--}}
                <img src="https://squadracorse.lamborghini.com/sites/lamborghinisc/files/dam/__release/Academy%20-%20Winter/Gallery/best-of-WA-Livigno-2016-0317.jpg"  alt="" class="img-responsive">
            </div>
        </form>
    </div>
    <div class="col-xs-12 col-sm-9 padding-lr5" id="mo-accounting-document-info">
        <div class="col-xs-12 col-sm-8">
            <form class="form-horizontal" id="mo-accounting-document-info-form">
                <div class="col-xs-12 padding-lr5">
                    <span class="col-xs-12 col-sm-6 no-padding text-capitalize">
                        registration number:
                    </span>
                    <label class="col-xs-12 col-sm-6 no-padding text-uppercase">2FAST4U</label>
                </div>
                <div class="col-xs-12 padding-lr5 padding-top-5">
                    <span class="col-xs-12 col-sm-6 no-padding text-capitalize">
                        status:
                    </span>
                    <label class="col-xs-12 col-sm-6 no-padding text-capitalize">Awaiting Arrival
                    </label>
                </div>
                <div class="col-xs-12 padding-lr5 padding-top-5">
                    <span class="col-xs-12 col-sm-6 no-padding text-capitalize">
                        transaction:
                    </span>
                    <label class="col-xs-12 col-sm-6 no-padding text-capitalize">...</label>
                </div>
                <div class="col-xs-12 padding-lr5 padding-top-5">
                    <span class="col-xs-12 col-sm-6 no-padding text-capitalize">
                        price:
                    </span>
                    <label class="col-xs-12 col-sm-6 no-padding text-capitalize">$</label>
                </div>
                <div class="col-xs-12 padding-lr5 padding-top-5">
                    <span class="col-xs-12 col-sm-6 no-padding text-capitalize">
                        location:
                    </span>
                    <label class="col-xs-12 col-sm-6 no-padding text-capitalize">...</label>
                </div>
            </form>
        </div>
        <div class="col-xs-12 col-sm-4 padding-top-80">
            <input type="button" class="mo-btn text-uppercase float-right" id="mo-accounting-selling-ship-vehicle" name="mo_accounting_selling_ship_vehicle" value="ship vehicle">
        </div>
    </div>
</div>
<!-- end ship vehicle -->

<hr class="mo-dispute-hr padding-bottom-10 padding-top-10">
<div class="mo-row">
    <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 padding-lr5 text-uppercase bold">
        2014 maserati quattroporte white
    </div>
    <div class="col-xs-12 col-sm-3 padding-lr5">
        <form class="vertical-center" method="post" id="mo-vehicle-image-upload-form">
            <div class="mo-vehicle-image-wrapper" class="vertical-center">
                {{--<img src="http://i1.wp.com/www.inautonews.com/wp-content/uploads/2011/10/maserati-granturismo-mc-stradale-novitec-tridente-11.jpg?resize=350%2C200" alt="" class="img-responsive">--}}
                <img src="https://squadracorse.lamborghini.com/sites/lamborghinisc/files/dam/__release/Academy%20-%20Winter/Gallery/best-of-WA-Livigno-2016-0317.jpg"  alt="" class="img-responsive">
            </div>
        </form>
    </div>
    <div class="col-xs-12 col-sm-9 padding-lr5" id="mo-accounting-document-info">
        <div class="col-xs-12 col-sm-8">
            <form class="form-horizontal" id="mo-accounting-document-info-form">
                <div class="col-xs-12 padding-lr5">
                    <span class="col-xs-12 col-sm-6 no-padding text-capitalize">
                        registration number:
                    </span>
                    <label class="col-xs-12 col-sm-6 no-padding text-uppercase">2FAST4U</label>
                </div>
                <div class="col-xs-12 padding-lr5 padding-top-5">
                    <span class="col-xs-12 col-sm-6 no-padding text-capitalize">
                        status:
                    </span>
                    <label class="col-xs-12 col-sm-6 no-padding text-capitalize">Awaiting Arrival
                    </label>
                </div>
                <div class="col-xs-12 padding-lr5 padding-top-5">
                    <span class="col-xs-12 col-sm-6 no-padding text-capitalize">
                        transaction:
                    </span>
                    <label class="col-xs-12 col-sm-6 no-padding text-capitalize">...</label>
                </div>
                <div class="col-xs-12 padding-lr5 padding-top-5">
                    <span class="col-xs-12 col-sm-6 no-padding text-capitalize">
                        price:
                    </span>
                    <label class="col-xs-12 col-sm-6 no-padding text-capitalize">$</label>
                </div>
                <div class="col-xs-12 padding-lr5 padding-top-5">
                    <span class="col-xs-12 col-sm-6 no-padding text-capitalize">
                        location:
                    </span>
                    <label class="col-xs-12 col-sm-6 no-padding text-capitalize">...</label>
                </div>
            </form>
        </div>
        <div class="col-xs-12 col-sm-4">
            <input type="button" class="mo-btn text-uppercase float-right" id="mo-accounting-selling-view-debit-note" name="mo_accounting_selling_view_debit_note" value="view debit note">
            <input type="button" class="mo-btn text-uppercase float-right margin-top-10" id="mo-accounting-selling-view-resolution" name="mo_accounting_selling_view_resolution" value="view resolution">
            <span class="float-right text-capitalize mo-selling-request-payment padding-top-10"><a href="">request payment</a></span>
        </div>
    </div>   
</div>
<!-- end selling request payment -->

<hr class="mo-dispute-hr padding-bottom-10 padding-top-10">
<div class="mo-row">
    <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 padding-lr5 text-uppercase bold">
        2014 maserati quattroporte white
    </div>
    <div class="col-xs-12 col-sm-3 padding-lr5">
        <form class="vertical-center" method="post" id="mo-vehicle-image-upload-form">
            <div class="mo-vehicle-image-wrapper" class="vertical-center">
                {{--<img src="http://i1.wp.com/www.inautonews.com/wp-content/uploads/2011/10/maserati-granturismo-mc-stradale-novitec-tridente-11.jpg?resize=350%2C200" alt="" class="img-responsive">--}}
                <img src="https://squadracorse.lamborghini.com/sites/lamborghinisc/files/dam/__release/Academy%20-%20Winter/Gallery/best-of-WA-Livigno-2016-0317.jpg"  alt="" class="img-responsive">
            </div>
        </form>
    </div>
    <div class="col-xs-12 col-sm-9 padding-lr5" id="mo-accounting-document-info">
        <div class="col-xs-12 col-sm-8">
            <form class="form-horizontal" id="mo-accounting-document-info-form">
                <div class="col-xs-12 padding-lr5">
                    <span class="col-xs-12 col-sm-6 no-padding text-capitalize">
                        registration number:
                    </span>
                    <label class="col-xs-12 col-sm-6 no-padding text-uppercase">2FAST4U</label>
                </div>
                <div class="col-xs-12 padding-lr5 padding-top-5">
                    <span class="col-xs-12 col-sm-6 no-padding text-capitalize">
                        status:
                    </span>
                    <label class="col-xs-12 col-sm-6 no-padding text-capitalize">Awaiting Arrival
                    </label>
                </div>
                <div class="col-xs-12 padding-lr5 padding-top-5">
                    <span class="col-xs-12 col-sm-6 no-padding text-capitalize">
                        transaction:
                    </span>
                    <label class="col-xs-12 col-sm-6 no-padding text-capitalize">...</label>
                </div>
                <div class="col-xs-12 padding-lr5 padding-top-5">
                    <span class="col-xs-12 col-sm-6 no-padding text-capitalize">
                        price:
                    </span>
                    <label class="col-xs-12 col-sm-6 no-padding text-capitalize">$</label>
                </div>
                <div class="col-xs-12 padding-lr5 padding-top-5">
                    <span class="col-xs-12 col-sm-6 no-padding text-capitalize">
                        location:
                    </span>
                    <label class="col-xs-12 col-sm-6 no-padding text-capitalize">...</label>
                </div>
            </form>
        </div>
        <div class="col-xs-12 col-sm-4 padding-top-80">
            <span class="float-right text-capitalize mo-selling-upload-invoice padding-top-24"><a href="">upload invoice</a></span>
        </div>
    </div>
</div>