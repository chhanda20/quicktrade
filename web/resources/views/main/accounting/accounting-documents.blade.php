@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                Accounting > <a href="#">{{ $header or 'Documents' }}</a>
            </div>
        </div>
    </div>
    {{--End - moto Content Header Section--}}
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 no-padding" id="mo-blocks-wrapper">
            {{-- tab 1 --}}
            <div class="col-xs-12 col-sm-5 padding-lr5">
                <div class="col-xs-12 mo-block mo-offer-blocks">
                    <span class="col-xs-12 no-padding mo-block-txt text-capitalize padding-top-8" data-label="Buying">buying</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div>
            {{-- tab 2 --}}
            <div class="col-xs-12 col-sm-5 padding-lr5">
                <div class="col-xs-12 mo-block mo-offer-blocks">
                    <span class="col-xs-12 no-padding mo-block-txt text-capitalize padding-top-8" data-label="Selling">selling</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div>
        </div>

        <!-- START BUYING  -->

        <div class="mo-row margin-top-32 hidden mo-block-list padding-lr5" id="mo-accounting-document-buying-list">
            @include('main.accounting.accounting-documents-buying');
        </div>
        <!-- END BUYING -->



        <!--START SELLING -->

        <div class="mo-row margin-top-32 hidden mo-block-list padding-lr5" id="mo-accounting-document-selling-list">
            @include('main.accounting.accounting-documents-selling');
        </div>

    </div>

@endsection