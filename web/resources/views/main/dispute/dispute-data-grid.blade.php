@if(isset($disputeList) && !empty($disputeList))
    @foreach($disputeList as $dispute)
        <div class="mo-row padding-bottom-24">
            <div class="col-xs-12 no-padding padding-top-24 text-capitalize font-weight-bold font-size16">
                {{$dispute->vehicle_name}}
            </div>
            <div class="mo-row padding-top-10">
                <div class="col-xs-2 no-padding">
                    <div class="max-width-100-percent">
                        <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$dispute->vehicle_id or ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@2x.png')}}'" class="img-responsive align-center">
                    </div>
                </div>
                <div class="col-xs-8 font-grey-color">
                    <div class="mo-row">
                        <span>Transaction: </span><span>{{$dispute->transaction_rid}}</span>
                    </div>
                    <div class="mo-row padding-top-5">
                        <span>Time & Date: </span><span>{{isset($dispute->create_date)?date("dMY H:i:s", strtotime($dispute->create_date)):""}}</span>
                    </div>
                </div>
                <div class="col-xs-2 no-padding text-center">
                    <div class="padding-bottom-10">
                        @if($dispute->is_seller)
                            <span><img src="{{asset('images/svg/main/undo-red.svg')}}" alt=""></span>
                        @endif
                        @if($dispute->is_buyer)
                            <span><img src="{{asset('images/svg/main/next-green.svg')}}" alt=""></span>
                        @endif
                    </div>
                    <div class="padding-bottom-10 font-family-bold">
                        <span>{{$dispute->status_value}}</span>
                    </div>
                    <div class="mo-row mo-dispute-btn-wrapper">
                        <input type="button" data-dispute-id="{{$dispute->id or ''}}" data-transaction-id="{{$dispute->transaction_id or ''}}" class="mo-btn text-uppercase align-center mo-dispute-view" name="mo_dispute_view" value="view">
                    </div>
                </div>
            </div>
            <hr class="mo-dispute-hr">
        </div>
    @endforeach
    <div class="mo-row col-xs-12 text-center mo-loading-wrapper">
        <img class="mo-loading-icon" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
    </div>
@else
    <div class="mo-row text-center padding-top-20">
        <label class="text-uppercase">There is no item to display at the moment</label>
    </div>
@endif