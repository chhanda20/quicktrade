@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
{{--Start - moto Content Header Section--}}
<div class="mo-row padding-bottom-24">
    <div class="col-xs-12 col-sm-7 no-padding" id="mo-content-header">
        {{ $header or 'Dispute Centre' }} > <a href="{!! url('dispute/'.$disputeType) !!}">{{ucfirst($disputeType)}}</a> > {{ $breadcrumb or 'Dispute Info' }}
    </div>
</div>
{{--End - moto Content Header Section--}}

<div class="mo-row mo-form-wrapper">
    <div class="col-xs-12 col-sm-6 col-md-5 no-padding padding-right-40">
        <div class="col-xs-12 no-padding">
            <form class="vertical-center" method="post" id="mo-vehicle-image-upload-form">
                <div class="mo-vehicle-image-wrapper" class="vertical-center">
                    <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$disputeInfo->vehicle_id or ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/img-add-photo@3x.png')}}'" class="img-responsive">
                </div>
            </form>
            <div class="col-xs-12 no-padding padding-top-24 padding-lr5">
                <div class="font-family-bold font-size16">
                    {{$disputeInfo->vehicle_name or ''}}
                </div>
                <div class="padding-top-10 font-grey-color">
                    <span>Transaction: </span><span>{{$transactionInfo->rid or ''}}</span>
                </div>
                <div class="padding-top-5 font-grey-color">
                    <span>Time & Date: </span><span>{{isset($disputeInfo->create_date)?date("dMY H:i:s", strtotime($disputeInfo->create_date)):""}}</span>
                </div>
            </div>
        </div>
    </div>

    @include("mo-admin.main.dispute.dispute-items")

    @if($disputeType == "resolved")
        @include("mo-admin.main.dispute.dispute-cost-variance")
    @endif


</div>
@endsection