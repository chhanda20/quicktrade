@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                {{ $header or 'Dispute Centre' }} > <span id="dispute-breadcrumb" class="yellow-text">{{ $breadcrumb or 'All' }}</span>
            </div>
        </div>
    </div>
    {{--End - moto Content Header Section--}}
    <div class="mo-row mo-form-wrapper">
        <div class="mo-row" id="mo-blocks-wrapper">
            {{-- tab 1 --}}
            <div class="col-xs-12 col-sm-4 padding-lr5 mo-text">
                <div class="col-xs-12 mo-block mo-offer-blocks">
                    <span class="col-xs-12 no-padding mo-block-txt text-uppercase padding-top-8" data-label="All">all</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div>
            {{-- tab 2 --}}
            <div class="col-xs-12 col-sm-4 padding-lr5 mo-text">
                <div class="col-xs-12 mo-block mo-offer-blocks">
                    <span class="col-xs-12 no-padding mo-block-txt text-uppercase padding-top-8" data-label="Sold">sold</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div>
            {{-- tab 3 --}}
            <div class="col-xs-12 col-sm-4 padding-lr5 mo-text">
                <div class="col-xs-12 mo-block mo-offer-blocks">
                    <span class="col-xs-12 no-padding mo-block-txt text-uppercase padding-top-8" data-label="Bought">bought</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div>
            {{-- tab 1 --}}{{--
            <div class="col-xs-12 col-sm-2 padding-lr5 mo-text">
                <span class="col-xs-12 no-padding mo-dispute-txt text-uppercase padding-top-8 bold" data-label="ALL">all</span>
            </div>
            --}}{{-- tab 2 --}}{{--
            <div class="col-xs-12 col-sm-2 padding-lr5 mo-text">
                <span class="col-xs-12 no-padding mo-dispute-txt text-uppercase padding-top-8 bold" data-label="SOLD">sold</span>
            </div>
            --}}{{-- tab 3 --}}{{--
            <div class="col-xs-12 col-sm-2 padding-lr5 mo-text">
                <span class="col-xs-12 no-padding mo-dispute-txt text-uppercase padding-top-8 bold" data-label="BOUGHT">bought</span>
            </div>--}}
        </div>

        {{--<div class="mo-row">
            <hr class="mo-form-hr">
        </div>--}}

        <div class="mo-row mo-text-list" id="mo-dispute-info">


            @include("main.dispute.dispute-data-grid")

        </div>
        {{ csrf_field() }}
        <div class="mo-loading-image hidden">Loading&#8230;</div>
    </div>

@endsection