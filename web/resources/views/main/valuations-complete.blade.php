@extends('layouts.master')

@section('title', 'moto - Valuations Complete')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-5 col-md-5 no-padding" id="mo-content-header">
            {{$header or 'Valuations'}} > <span class="yellow-text">{{ $breadcrumb or 'Complete' }}</span>
        </div>
        {{--Start - moto Search Section--}}
        <div class="col-xs-12 col-sm-7 col-md-7 no-padding">
            {{ csrf_field() }}
            <div class="col-xs-12 col-sm-10 col-md-10 no-padding mo-search-wrapper">
                <input type="text" placeholder="Search" id="mo-txt-search">
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2 no-padding text-right mo-search-btn-wrapper">
                <input type="submit" class="mo-btn" id="mo-btn-search" value="search">
            </div>
        </div>
        {{--End - moto Search Section--}}
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - moto List Valuations Request Section--}}
    <div class="mo-row padding-top-24 percent-h-100">
        <div class="mo-row mo-form-wrapper percent-h-100">
            <table id="mm-table-valuations-complete-list" class="table" cellspacing="0">
                <thead>
                    <tr>
                        <th></th>
                        <th>Wish List</th>
                        <th>Make</th>
                        <th>Model</th>
                        <th>Year</th>
                        <th>Registration</th>
                        <th>Valuer</th>
                        <th>Sales</th>
                        <th>Location/Franchise</th>
                        <th>Status</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
                <ul class="pagination">
                    <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                    <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
                </ul>
            </div>
        </div>
    </div>
    {{--End - moto List Valuations Request Section--}}

    {{--Start - moto Popup Section--}}
    {{------------ RE-EVALUATE POPUP ------------}}
    <div id="mo-re-evaluate-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form" id="mo-popup-re-evaluate">
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">re-price</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="form-group col-xs-12 padding-top-24">
                    <label for="re_evaluate_price">Add the value you would like to request for this vehicle</label>
                    <input type="number" name="re_evaluate_price" id="estimate-cost" placeholder="$" class="form-control mo-exterior-popup-textbox"/> <br/>
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel" >
                    <input type="button" class="mo-btn text-uppercase" value="save" id="mo-re-evaluate-save-btn">
                </div>
            </div>
        </form>
    </div>

    {{------------ CHANGE STATUS POPUP ------------}}
    <div id="mo-popup-changestatus" class="mo-white-popup mfp-hide">
        <form action="" id="mo-popup-changestatus-form" class="mo-popup-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">Trade Vehicle</span>
            </div>
            <hr class="mo-form-hr">
            <div class="mo-row mo-popup-content">
                {{--
                <div class="form-group col-xs-12 padding-top-24">
                    <label for="status" class="text-capitalize"> status</label>
                    <span><img class="imgicon" src="{{asset('images/main/icon-swipe-right-copy-4.png')}}"></span>
                    <select class="form-control text-capitalize mo-popup-textbox">
                        <option selected disabled>traded</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                </div>
                --}}
                <div class="form-group col-xs-12 padding-top-24">
                    <label for="proposed-arrival-date" class="text-capitalize">proposed arrival date</label>
                    <div class="form-inline input-group date" id="datetimepicker1">
                        <input type="text" class="form-control" id="arrival-date" name="arrival_date" placeholder="YYYY-DD-MM">
                        <span class="input-group-addon"><img class="datetime" src="{{asset('images/main/group-2-copy-2.png')}}"></span>
                    </div>
                </div>
                <div class="form-group col-xs-12 padding-top-24">
                    <label for="trade-in-price" class="text-capitalize">final trade in price (max price $<span class="complete-valuation-max-price-number force-no-padding">0.00</span>)</label><br>
                    <input type="number" class="form-control" name="trade_in_price" id="mo-final-trade-in-price" placeholder="$">
                    <!-- <span>$<span id="mo-final-trade-in-price">192,500.00</span></span> -->
                </div>
                <!--<div class="form-group col-xs-12 padding-top-24">
                    <label for="proposed-new-price" class="text-capitalize">proposed new price</label>
                    <input type="number" min="0.01" pattern="[0-9]*" class="form-control" name="proposed_new_price" id="proposed-new-price" placeholder="$202,500.00">
                </div>-->
                <div class="form-group col-xs-12 padding-top-24">
                    <label for="note" class="text-capitalize">Notes here</label>
                    <hr class="mo-form-hr">
                    <textarea class="form-control" name="note" id="note" rows="5" placeholder="Write here..." style="box-shadow: none;"></textarea>
                    <hr class="mo-form-hr">
                </div>
                <div>
                    <input type="hidden" name="valuation_id" value="" id="valuation_id" />
                    <!-- VALUATION STATUS HIDDEN -->
                    <input type="hidden" name="valuation_status" value="" id="valuation_status" />
                </div>
            </div>
            <hr class="mo-form-hr">
            <div class="mo-row">
                <div class="form-group col-xs-12 padding-top-24 text-right ">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="CANCEL">
                    <input type="submit" class="mo-btn text-uppercase" value="UPDATE" id="mo-update-trade-button">
                </div>
            </div>
        </form>
    </div>

    {{------------ VALUATION VERSION POPUP ------------}}

    <div id="mo-popup-valuation-version" class="mo-white-popup mfp-hide">
        <div class="mo-row padding-bottom-20 border-bottom">
            <span class="text-capitalize mo-popup-heading">valuations version</span>
        </div>
        <div class="mo-row">
            <table id="mo-table-valuation-version-list" class="table" cellspacing="0">
                <thead>
                <tr>
                    <th></th>
                    <th>Version No.</th>
                    <th>Creator</th>
                    <th>Date</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

        {{--<input type="hidden" id="current-user-id" name="current_user_id" value="">--}}

        <hr class="mo-form-hr" />
        <div class="col-xs-12 alert alert-danger hidden" role="alert">
            {{--Auction Session Duplicate!--}}
            {{--<span class="mo-error-message text-capitalize"></span>--}}
            Valuation is modifying by someone
            <a class="mo-close-message close">&times;</a>
        </div>
        <div class="mo-row">
            <div class="col-xs-12 padding-top-24 text-right">
                <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
            </div>
        </div>
        <div class="mo-loading-image hidden">Loading&#8230;</div>
    </div>


    {{--End - moto Popup Section--}}
@endsection