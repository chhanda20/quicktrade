@extends('layouts.master')

@section('title', 'moto - Valuations In Progress')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-5 col-md-5 no-padding" id="mo-content-header">
            {{$header or 'Valuations'}} > <span class="yellow-text">{{ $breadcrumb or 'In Progress' }}</span>
        </div>
        {{--Start - moto Search Section--}}
        <div class="col-xs-12 col-sm-7 col-md-7 no-padding">
            {{ csrf_field() }}
            <div class="col-xs-12 col-sm-10 col-md-10 no-padding mo-search-wrapper">
                <input type="text" placeholder="Search" id="mo-txt-search">
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2 no-padding text-right mo-search-btn-wrapper">
                <input type="submit" class="mo-btn" id="mo-btn-search" value="search">
            </div>
        </div>
        {{--End - moto Search Section--}}
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - moto List Valuations Request Section--}}
    <div class="mo-row padding-top-24 percent-h-100">
        <div class="mo-row mo-form-wrapper percent-h-100">
            <table id="mo-table-valuations-in-progress-list" class="table" cellspacing="0">
                <thead>
                <tr>
                    <th></th>
                    <th class="wish">Wish list</th>
                    <th>Make</th>
                    <th>Model</th>
                    <th>Year</th>
                    <th>Registration</th>
                    {{--<th>Condition</th>--}}
                    <th>Valuer</th>
                    <th>Salesperson</th>
                    <th>Location/Franchise</th>
                    <th>Function</th>
                    {{--<th></th>--}}
                </tr>
                </thead>

                
                <tbody>
                {{--<tr>--}}
                    {{--<td></td>--}}
                    {{--<td>Aston Martin</td>--}}
                    {{--<td>DB7</td>--}}
                    {{--<td>2010</td>--}}
                    {{--<td>JT152EEA100302159</td>--}}
                    {{--<td>Black</td>--}}
                    {{--<td>2FST4U</td>--}}
                    {{--<td>Dec 2020</td>--}}
                    {{--<td></td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td></td>--}}
                    {{--<td>Aston Martin</td>--}}
                    {{--<td>DB7</td>--}}
                    {{--<td>2010</td>--}}
                    {{--<td>JT152EEA100302159</td>--}}
                    {{--<td>Black</td>--}}
                    {{--<td>2FST4U</td>--}}
                    {{--<td>Dec 2020</td>--}}
                    {{--<td></td>--}}
                {{--</tr>--}}
                </tbody>
            </table>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
                <ul class="pagination">
                    <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                    <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
                </ul>
            </div>
        </div>
    </div>
    {{--End - moto List Valuations Request Section--}}

    {{--start - SENSITIVE ACTION CONFIRMATION POPUP--}}
    <div id="mo-sensitive-action-confirm-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            {{ csrf_field() }}

            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">cancel valuation</span>
            </div>

            <hr class="mo-form-hr" />

            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <span class="suspend-popup-content">Are you sure you want to cancel this item?</span>
                </div>
            </div>

            <hr class="mo-form-hr" />

            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-valuation-progress-cancel-confirm-btn">
                </div>
            </div>

            <div class="mo-loading-image hidden">Loading...</div>
        </form>
    </div>
    {{--End - moto Popup Section--}}

@endsection