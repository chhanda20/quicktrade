@extends(strpos(Request::path(), 'mo-admin') === 0 ? 'mo-admin.layouts.master' : 'layouts.master')

@section('title', 'moto - Trading - Stock List')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row padding-bottom-24">
        <div class="col-xs-12 col-sm-8 no-padding" id="mo-content-header">
            @if (strpos(Request::path(), 'mo-admin') === 0)
                {{ $header or 'Trading' }} > <a href="{!! url('mo-admin/trading/request-list') !!}" class="underline-text">{{ $breadcrumb or 'Request List' }}</a> > <span class="yellow-text">Vehicle Details</span>
            @else
                {{ $header or 'Trading' }} > <a href="{!! url('trading/stock-list') !!}" class="underline-text">{{ $breadcrumb or 'Request List' }}</a> > <span class="yellow-text">Vehicle Details</span>
            @endif
        </div>
    </div>
    {{--End - moto Content Header Section--}}

    {{--------------------------------------- start VEHICLE IMAGE SLIDER ---------------------------------------}}
    <div class="mo-slider-wrapper">
        <ul class="bxslider percent-h-100 vertical-center">
            @if (!empty($vehiclePhotos))
                @if(isset($vehicleDetails->id) && !array_search($vehicleDetails->id.".jpg", $vehiclePhotos))
                    <li>
                        <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$vehicleDetails->id or ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'" class="img-responsive" crossorigin="Anonymous">
                        <div class="mo-edited-icon-wrapper vertical-center">
                            <span class="glyphicon glyphicon-edit"></span>
                        </div>
                    </li>
                @endif
                @foreach($vehiclePhotos as $key=>$value)
                    <li>
                        <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$value}}?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'" class="img-responsive" crossorigin="Anonymous">
                        <div class="mo-edited-icon-wrapper vertical-center">
                            <span class="glyphicon glyphicon-edit"></span>
                        </div>
                    </li>
                @endforeach
            @else
                <li>
                    <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$vehicleDetails->id or ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'" class="img-responsive" crossorigin="Anonymous">
                    @if(stripos(get_headers("https://".Config::get('app.azure_storage_account_name').".blob.core.windows.net/vehicle-image/".$vehicleDetails->id.".jpg")[0],"200 OK"))
                        <div class="mo-edited-icon-wrapper vertical-center">
                            <span class="glyphicon glyphicon-edit"></span>
                        </div>
                    @endif
                </li>
            @endif
        </ul>
    </div>

    <div class="mo-row">
        <div class="col-xs-12 alert alert-danger hidden" role="alert">
            <span class="mo-error-message text-capitalize"></span>
            <a class="mo-close-message close">&times;</a>
        </div>
    </div>

    <input type="hidden" value="{{$vehiclePhotosCount}}" id="mo-vehicle-photos-count">
    <input type="hidden" id="vehicle-id" value="{{$vehicleDetails->id or ''}}">

    <div class="{{$vehiclePhotosCount < 10?"":"hidden"}} col-xs-2 no-padding padding-right-5" id="mo-vehicle-add-photo">
        <form id="mo-vehicle-photo-form" class="vertical-center" action="" method="post" enctype="multipart/form-data" id="mo-vehicle-image-upload-form">
            {{ csrf_field() }}

            <div id="mo-vehicle-image-wrapper" class="vertical-center mo-add-photo-wrapper">
                <label for="mo-vehicle-image-upload" class="cursor-pointer">
                    <img src="{{ asset('images/main/img-add-photo@3x.png')}}"  alt="Vehicle Photo" class="img-responsive mo-add-photo">
                </label>
            </div>
            <input type="file" accept='image/*' name="mo_vehicle_photo_upload" id="mo-vehicle-image-upload" class="mo-exterior-input-file" />
        </form>
    </div>

    <div id="bx-pager" class="">
        @if (!empty($vehiclePhotos))
            <?php $count = 0; ?>
            @if(isset($vehicleDetails->id) && !array_search($vehicleDetails->id.".jpg", $vehiclePhotos))
                <a data-slide-index="0" href="">
                    <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$vehicleDetails->id or ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'">
                    <div class="vertical-center mo-icon-loading hidden">
                        <img src="{{ asset('images/main/ajax-loader.gif')}}">
                    </div>
                    <div class="mo-icon-wrapper vertical-center">
                        <span class="glyphicon glyphicon-trash"></span>
                    </div>
                </a>
                <div class="mo-featured-photo">
                    <form id="mo-vehicle-featured-photo-form" class="mo-upload-photo-form" action="" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="vertical-center">
                            <label for="mo-vehicle-featured-photo-upload" class="cursor-pointer">
                                <span class="glyphicon glyphicon-picture"></span>
                                <input type="file" accept='image/*' name="mo_vehicle_featured_photo_upload" id="mo-vehicle-featured-photo-upload" class="mo-exterior-input-file" />
                            </label>
                        </div>
                    </form>
                    <span class="mo-row text-uppercase mo-feature-txt font-size12">set featured image</span>
                </div>
                    <?php $count++; ?>
            @endif
            @foreach($vehiclePhotos as $key=>$value)
                <a data-slide-index="{{$key+$count}}" href="">
                    <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$value}}?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'">
                    <div class="vertical-center mo-icon-loading hidden">
                        <img src="{{ asset('images/main/ajax-loader.gif')}}">
                    </div>
                    <div class="mo-icon-wrapper vertical-center">
                        <span class="glyphicon glyphicon-trash"></span>
                    </div>
                </a>
            @endforeach


        @else
            <a data-slide-index="0" href="">
                <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$vehicleDetails->id or ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'">
                <div class="vertical-center mo-icon-loading hidden">
                    <img src="{{ asset('images/main/ajax-loader.gif')}}">
                </div>
                <div class="mo-icon-wrapper vertical-center">
                    <span class="glyphicon glyphicon-trash"></span>
                </div>
            </a>
            <div class="mo-featured-photo">
                <form id="mo-vehicle-featured-photo-form" class="mo-upload-photo-form" action="" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="vertical-center">
                        <label for="mo-vehicle-featured-photo-upload" class="cursor-pointer">
                            <span class="glyphicon glyphicon-picture"></span>
                            <input type="file" accept='image/*' name="mo_vehicle_featured_photo_upload" id="mo-vehicle-featured-photo-upload" class="mo-exterior-input-file" />
                        </label>
                    </div>
                </form>
                <span class="mo-row text-uppercase mo-feature-txt font-size12">set featured image</span>
            </div>
        @endif
    </div>

    {{--------------------------------------- end VEHICLE IMAGE SLIDER ---------------------------------------}}

    {{--------------------------------------- start VEHICLE INFO ---------------------------------------}}

    <div class="mo-row margin-top-32 mo-block-list font-size18">
        <div class="col-xs-12 no-padding text-uppercase font-size24 font-weight-bold margin-bottom-16">{{$vehicleDetails->name or ''}}</div>
        <div class="mo-row">
            <div class="col-xs-12 col-sm-3 no-padding text-capitalize">Days in Stock:</div>
            <div class="col-xs-12 col-sm-9 no-padding font-weight-bold">{{$vehicleDetails->day_in_stock or ''}}</div>
        </div>
        <div class="mo-row">
            <div class="col-xs-12 col-sm-3 no-padding text-capitalize">Stock No.:</div>
            <div class="col-xs-12 col-sm-9 no-padding font-weight-bold">{{$valuationDetails->stock_number or ''}}</div>
        </div>
        <div class="mo-row">
            <div class="col-xs-12 col-sm-3 no-padding text-capitalize">Registration No.:</div>
            <div class="col-xs-12 col-sm-9 no-padding font-weight-bold">{{$vehicleDetails->registration_number or ''}}</div>
        </div>
        <div class="mo-row">
            <div class="col-xs-12 col-sm-3 no-padding text-capitalize">trade-in price:</div>
            <div class="col-xs-12 col-sm-9 no-padding font-weight-bold">${{isset($valuationDetails->trade_price) ? number_format($valuationDetails->trade_price, 2) : number_format(0.00, 2)}}</div>
        </div>
        <div class="mo-row">
            <div class="col-xs-12 col-sm-3 no-padding text-capitalize">Location:</div>
            <div class="col-xs-12 col-sm-9 no-padding font-weight-bold">{{$locationName or ''}}</div>
        </div>
    </div>

    {{--Start - moto Details Content Section--}}
    <div class="mo-row mo-form-wrapper margin-top-32 mo-block-list">
        {{--MOTORONE BLOCK INSIDE--}}
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding mo-5-blocks" id="mo-blocks-wrapper">
            <div class="col-xs-12 col-sm-5 padding-lr5">
                <a href="#details-model">
                    <div class="col-xs-12 mo-block-border-bottom" id="details-model-tab">
                        <span class="col-xs-12 mo-block-txt text-capitalize no-padding">Model Specs & Standard Features</span>
                        <span class="col-xs-12 text-center mo-arrow-focus">
                            <img src="{{ asset('images/svg/main/triangle.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/triangle.png') }}'">
                        </span>
                    </div>
                </a>
            </div>
            <div class="col-xs-12 col-sm-5 padding-lr5">
                <a href="#details-options">
                    <div class="col-xs-12 mo-block-border-bottom" id="details-options-tab">
                        <span class="col-xs-12 mo-block-txt text-capitalize no-padding">Options & Extras</span>
                        <span class="col-xs-12 text-center mo-arrow-focus">
                            <img src="{{ asset('images/svg/main/triangle.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/triangle.png') }}'">
                        </span>
                    </div>
                </a>
            </div>
        </div>
        {{--MODEL SPECS & STANDARD FEATURES--}}
        <div class="mo-row padding-top-36 mo-block-inside-list" id="mo-model-specs-block">
            <div class="col-xs-12 padding-lr5 padding-bottom-24 text-uppercase mo-form-header">
                model specs
            </div>
            <form action="" method="" id="mo-model-spec-form">

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="make" class="text-capitalize">make</label>
                            <div class="text-capitalize">{{$vehicleDetails->make or ''}}</div>
                            <input type="hidden" value="{{$vehicleDetails->make or ''}}" id="make" name="make">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="engine" class="text-capitalize">engine</label>
                        <div class="text-capitalize">{{$vehicleDetails->engine_type or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->engine_type or ''}}" id="engine" name="engine">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="model" class="text-capitalize">model</label>
                        <div class="text-capitalize">{{$vehicleDetails->model_family or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->model_family or ''}}" id="model" name="model">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="engine-size" class="text-capitalize">engine size</label>
                        <div class="text-capitalize">{{$vehicleDetails->engine_size or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->engine_size or ''}}" id="engine-size" name="engine_size">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="variant" class="text-capitalize">variant</label>
                        <div class="text-capitalize">{{$vehicleDetails->variant or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->variant or ''}}" id="variant" name="variant">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="engine-capacity" class="text-capitalize">engine capacity</label>
                        <div class="text-capitalize">{{$vehicleDetails->engine_capacity or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->engine_capacity or ''}}" id="engine-capacity" name="engine_capacity">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="build-date" class="text-capitalize">build date</label>
                        <div class="text-capitalize">{{date('M Y', strtotime($vehicleDetails->build_date))}}</div>
                        <input type="hidden" value="{{$vehicleDetails->build_date or ''}}" name="build_date" id="build-date">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="number-of-cylinders" class="text-capitalize">number of cylinders</label>
                        <div class="text-capitalize">{{$vehicleDetails->cylinder or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->cylinder or ''}}" id="cylinder" name="cylinder">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="genneral-colour" class="text-capitalize">general colour</label>
                        <div class="text-capitalize">{{$general_color or ''}}</div>
                        <input type="hidden" value="{{$general_color or ''}}" name="general_colour" id="general-colour">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="fuel-type" class="text-capitalize">fuel type</label>
                        <div class="text-capitalize">{{$fuel_type or ''}}</div>
                        <input type="hidden" value="{{$fuel_type or ''}}" name="fuel_type" id="fuel-type">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="manufacturer-colour" class="text-capitalize">manufacturer colour</label>
                        <div class="text-capitalize">{{$manufacturer_color or ''}}</div>
                        <input type="hidden" value="{{$manufacturer_color or ''}}" name="manufacturer_colour" id="manufacturer-colour">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="transmission" class="text-capitalize">transmission</label>
                        <div class="text-capitalize">{{$transmission or ''}}</div>
                        <input type="hidden" value="{{$transmission or ''}}" name="transmission" id="transmission">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="body-type" class="text-capitalize">body type</label>
                        <div class="text-capitalize">{{$vehicleDetails->body_type or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->body_type or ''}}" name="body_type" id="body-type">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="drivetrain" class="text-capitalize">drivetrain</label>
                        <div class="text-capitalize">{{$drive_train or ''}}</div>
                        <input type="hidden" value="{{$drive_train or ''}}" name="drive_train" id="drive-train">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="year-first-registered" class="text-capitalize">year first registered</label>
                        <div class="text-capitalize">{{isset ($vehicleDetails->first_reg_date) && ($vehicleDetails->first_reg_date != "0000-00-00")  ? substr($vehicleDetails->first_reg_date, 0, 4) : ""}}</div>
                        <input type="hidden" value="{{$vehicleDetails->first_reg_date or ''}}" name="year_first_registered" id="year-first-registered">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="body-no">Body no.</label>
                        <div class="text-capitalize">{{$vehicleDetails->body_no or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->body_no or ''}}" name="body_no" id="body-no">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="registration-expiry-date" class="text-capitalize">registration expiry date</label>
                        <div class="text-capitalize">{{date('d M Y', strtotime($vehicleDetails->registration_expiry_date))}}</div>
                        <input type="hidden" value="{{$vehicleDetails->registration_expiry_date or ''}}" name="registration_expiry_date">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="vin" class="text-capitalize">VIN</label>
                        <div class="text-capitalize">{{$vehicleDetails->vin or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->vin or ''}}" name="vin" id="vin">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="registered-state" class="text-capitalize">registered state</label>
                        <div class="text-capitalize">{{$registered_state or ''}}</div>
                        <input type="hidden" value="{{$registered_state or ''}}" name="registered_state" id="registered-state">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="chassis-no">Chassis no.</label>
                        <div class="text-capitalize">{{$vehicleDetails->chassis_number or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->chassis_number or ''}}" name="chassis_number" id="chassis-number">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="registration" class="text-capitalize">registration</label>
                        <div class="text-capitalize">{{$vehicleDetails->registration_number or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->registration_number or ''}}" name="registration" id="registration">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="trim-type" class="text-capitalize">trim type</label>
                        <div class="text-capitalize">{{$trim_type or ''}}</div>
                        <input type="hidden" value="{{$trim_type or ''}}" name="trim_type" id="trim-type">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="compliance-date" class="text-capitalize">compliance date</label>
                        <div class="text-capitalize">{{date('d M Y', strtotime($vehicleDetails->compliance_date))}}</div>
                        <input type="hidden" value="{{$vehicleDetails->compliance_date or ''}}" name="compliance_date">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="trim-colour" class="text-capitalize">trim colour</label>
                        <div class="text-capitalize">{{$trim_color or ''}}</div>
                        <input type="hidden" value="{{$trim_color or ''}}" name="$trim_color" id="trim-color">
                    </div>
                </div>

                {{-- STANDARD FEATURES --}}
                @if(!empty($standardFeatures) && $standardFeatures != null)
                    <div class="col-xs-12 padding-lr5">
                        <div class="padding-top-24 text-uppercase mo-form-header padding-bottom-24">standard features</div>
                    </div>
                    @for ($i = 0; $i < count($standardFeatures); $i++)
                        <div class="col-xs-12 no-padding">
                            <div class="form-group col-xs-12 col-sm-5 padding-top-24 padding-bottom-8 border-bottom {{$i==0?'border-top':null}}">
                                <label class="text-uppercase">
                                    [{{$standardFeatures[$i]->glass_option_id}}] {{$standardFeatures[$i]->name}}
                                </label>
                            </div>
                            <span class="hidden">{{$i++}}</span>
                            @if($i<count($standardFeatures))
                                <div class="form-group col-xs-12 col-sm-5 padding-top-24 padding-bottom-8 border-bottom {{$i==1?'border-top':null}} float-right">
                                    <label class="text-uppercase">
                                        [{{$standardFeatures[$i]->glass_option_id}}] {{$standardFeatures[$i]->name}}
                                    </label>
                                </div>
                            @endif
                        </div>
                    @endfor
                @endif
            </form>

        </div>

        {{-- OPTIONS & EXTRAS --}}
        <div class="mo-row padding-top-36 hidden mo-block-inside-list" id="mo-options-extras-block">
            <form id="mo-create-option-and-extra-form">
                {{--Options--}}
                <div class="col-xs-12 padding-lr5 text-uppercase mo-form-header padding-bottom-24">
                    options
                </div>

                @if (isset($optionalOptions))
                    @foreach ($optionalOptions as $key => $optional_option)
                        <div class="form-group col-xs-12 col-sm-6 col-md-6 padding-bottom-20">
                            <input type="checkbox" disabled class="mo-round-check" id="{{$optional_option->name}}" name="optional_options[]" {{$optional_option->selected!=false?'checked':null}} value="{{$key}}">
                            <label for="{{$optional_option->name}}" class="text-capitalize"><span></span>{{$optional_option->name}}</label>
                        </div>
                    @endforeach
                @endif

                {{--Extras--}}
                <div class="col-xs-12 padding-lr5">
                    <div class="padding-bottom-20 text-uppercase mo-form-header border-bottom">
                        <span>extras</span>
                    </div>

                    @if (isset($extraOptions))
                        @foreach ($extraOptions as $extras_option)
                            <span class="col-xs-12 no-padding padding-top-20 padding-bottom-20 text-capitalize mo-extras-options">
                                <textarea id="{{$extras_option->id}}" class="form-control mo-extras-txt" rows="1" placeholder="Write here…" style="box-shadow: none;" readonly>{{$extras_option->description}}</textarea>
                                <input type="hidden" name="" value="{{$extras_option->id}}">
                            </span>
                        @endforeach
                    @endif

                </div>
            </form>
        </div>
    </div>
    {{--End - moto Details Content Section--}}

    {{--------------------------------------- end VEHICLE INFO ---------------------------------------}}

    {{------------ DELETE VEHICLE IMAGE CONFIRMATION POPUP ------------}}
    {{--<div id="mo-remove-confirm-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">delete vehicle image</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <span class="suspend-popup-content">Are you sure you want to delete this image?</span>
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 alert alert-danger hidden" role="alert">
                    <span class="mo-error-message text-capitalize"></span>
                    <a class="mo-close-message close">&times;</a>
                </div>
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-confirm-cancel" data-auction-id="">
                </div>
            </div>
        </form>
        <div class="mo-loading-image hidden">Loading&#8230;</div>
    </div>--}}
    {{--End - moto Popup Section--}}

@endsection