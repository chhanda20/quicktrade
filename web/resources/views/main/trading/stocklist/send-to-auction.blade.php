@extends('layouts.master')

@section('title', 'moto - Trading - Send To Auction')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row padding-bottom-24">
        <div class="col-xs-12 col-sm-8 no-padding" id="mo-content-header">
            {{ $header or 'Trading' }} > <a href="{!! url('trading/stock-list') !!}" class="underline-text">Stock List</a> > <span class="yellow-text">{{ $breadcrumb or 'Send To Auction' }}</span>
        </div>
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - moto Details Content Section--}}
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-5" id="mo-send-to-auction-vehicle-slider">
            {{--------------------------------------- start VEHICLE IMAGE SLIDER ---------------------------------------}}
            <ul class="bxslider percent-h-100 vertical-center">
                @if (!empty($vehiclePhotos))
                    @foreach($vehiclePhotos as $key=>$value)
                        <li>
                            <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$value}}?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'" class="img-responsive" crossorigin="Anonymous">
                        </li>
                    @endforeach
                @else
                    <li>
                        <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$vehicleStockInfo->id or ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'" class="img-responsive" crossorigin="Anonymous">
                    </li>
                @endif
            </ul>

            <div id="bx-thumbnail" class="">
                @if (!empty($vehiclePhotos))
                    @foreach($vehiclePhotos as $key=>$value)
                        <a data-slide-index="{{$key}}" href="">
                            <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$value}}?{{rand()}}" class="img-responsive" alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'">
                        </a>
                    @endforeach
                @else
                    <a data-slide-index="0" href="">
                        <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$vehicleStockInfo->id or ''}}.jpg?{{rand()}}" class="img-responsive" alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'">
                    </a>
                @endif
            </div>
            {{--------------------------------------- end VEHICLE IMAGE SLIDER ---------------------------------------}}
        </div>

        {{----- start VEHICLE INFO -----}}
        <div class="col-xs-12 col-sm-7" id="mo-vehicle-info">
            <div class="mo-row">
                <div class="col-xs-12 no-padding font-weight-bold font-size20">{{$vehicleStockInfo->name or ''}}</div>
            </div>
            <div class="mo-row text-capitalize">
                <div class="col-xs-7 no-padding">days in stock:</div>
                <div class="col-xs-5 no-padding font-weight-bold">{{$vehicleStockInfo->day_in_stock or ''}}</div>
            </div>
            <div class="mo-row text-capitalize">
                <div class="col-xs-7 no-padding">stock no.:</div>
                <div class="col-xs-5 no-padding font-weight-bold">{{$vehicleStockInfo->stock_number or ''}}</div>
            </div>
            <div class="mo-row text-capitalize">
                <div class="col-xs-7 no-padding">registration no.:</div>
                <div class="col-xs-5 no-padding font-weight-bold">{{$vehicleStockInfo->registration_number or ''}}</div>
            </div>
            <div class="mo-row text-capitalize">
                <div class="col-xs-7 no-padding">trade-in price:</div>
                <div class="col-xs-5 no-padding font-weight-bold">${{$vehicleStockInfo->trade_price_format or ''}}</div>
            </div>
            <div class="mo-row text-capitalize">
                <div class="col-xs-7 no-padding">location:</div>
                <div class="col-xs-5 no-padding font-weight-bold">{{$vehicleStockInfo->location_name or ''}}</div>
            </div>
            <div class="mo-row text-capitalize">
                <div class="col-xs-7 no-padding">Owner History:</div>
                <div class="col-xs-5 no-padding font-weight-bold">{{$vehicleStockInfo->ownership_type_value or ''}}</div>
            </div>
            <div class="mo-row text-capitalize">
                <div class="col-xs-7 no-padding">Special Disclosures:</div>
                <div class="col-xs-5 no-padding font-weight-bold">{{$vehicleStockInfo->special_disclosures_name or ''}}</div>
            </div>
        </div>
        {{----- end VEHICLE INFO -----}}

        <div class="col-xs-12"><hr class="mo-hr"></div>

        <div class="col-xs-12 mo-block-list padding-top-10" id="mo-details-block">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                CONFIRM RECONDITIONING COSTS
            </div>
            @if(isset($inspectionItems))
            <div class="mo-row padding-bottom-40" id="">
                <hr class="mo-dispute-hr" />

                    {{--CLONE--}}
                    <div class="inspection-section-wrapper" id="inspection-clone" data-section-type="">
                        <label class="padding-left-20 inspection-section-name"></label>
                    </div>
                    <div class="inspection-item-wrapper" id="inspection-item-wrapper-clone">
                        <div class="mo-row font-warm-grey-color height-25 inspection-record">
                            <span class="col-xs-9 no-padding padding-left-30 inspection-name"></span>
                            <span class="col-xs-2 no-padding text-right padding-right-30">$
                                <span class="repair-cost-span cursor-pointer"></span>
                                <input type="number" data-inspection-id="" class="repair-cost-input width-80" data-cost-type="repair">
                                <img class="mo-loading-icon height-20" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
                            </span>
                            <span class="col-xs-1 no-padding">
                                <a href="#mo-edit-inspection-item-popup" class="mo-popup col-xs-6 no-padding" id="mo-edit-popup">
                                    <span class="glyphicon glyphicon-pencil"
                                          data-section=""
                                          data-section-type=""
                                          data-inspection-id=""
                                          data-repair-cost=""
                                          data-note="">
                                    </span>
                                </a>
                                <a href="#mo-remove-confirm-popup" class="mo-popup col-xs-6 no-padding text-right" id="mo-remove-popup">
                                    <span class="glyphicon glyphicon-trash" data-inspection-id=""></span>
                                </a>
                            </span>
                        </div>
                        <hr class="mo-dispute-hr" />
                    </div>
                    {{--CLONE--}}

                    <!-- External -->
                    <div class="mo-row text-capitalize section-category" data-section-category="exterior">
                        <div class="mo-row">
                            <label class="col-xs-9 no-padding">exterior</label>
                            <label class="col-xs-2 no-padding text-right">$<span id="total-cost-exterior">{{$inspectionItems->exterior_section->total_cost_format or 0}}</span></label>
                            <span class="col-xs-1 no-padding"></span>
                        </div>
                        <hr class="mo-dispute-hr" />
                        <div class="mo-row font-warm-grey-color">
                            <span class="col-xs-9 no-padding padding-left-30 text-capitalize">general reconditioning cost</span>
                            <span class="col-xs-2 no-padding text-right padding-right-30">$
                                <span class="repair-cost-span exterior-cost-span cursor-pointer">{{$generalReconditionCost or 0}}</span>
                                <input type="number" class="repair-cost-input width-80" data-cost-type="general">
                                <img class="mo-loading-icon height-20" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
                            </span>
                            {{--<span class="col-xs-2 no-padding text-right padding-right-30">${{$generalReconditionCost or 0}}</span>--}}
                            <span class="col-xs-1 no-padding"></span>
                        </div>
                        <hr class="mo-dispute-hr" />
                        @if(isset($inspectionItems->exterior_section) && !empty($inspectionItems->exterior_section))
                            @foreach($inspectionItems->exterior_section as $ex_item)
                                @if(isset($ex_item->items))
                                    <div class="inspection-section-wrapper" data-section-type="{{$ex_item->section_type or ''}}">
                                        <label class="padding-left-20 inspection-section-name">{{$ex_item->section_name or ''}}:</label>
                                        @if(count($ex_item->items) > 0)
                                            @foreach($ex_item->items as $item)
                                                <div class="inspection-item-wrapper">
                                                    <div class="mo-row font-warm-grey-color height-25 inspection-record">
                                                        <span class="col-xs-9 no-padding padding-left-30 inspection-name">{{$item->item or ''}}</span>
                                                        <span class="col-xs-2 no-padding text-right padding-right-30">$
                                                            <span class="repair-cost-span exterior-cost-span cursor-pointer">{{number_format($item->repair_cost, 2)}}</span>
                                                            <input type="number" data-inspection-id="{{$item->inspection_item_id or ''}}" class="repair-cost-input width-80" data-cost-type="repair">
                                                            <img class="mo-loading-icon height-20" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
                                                        </span>
                                                        <span class="col-xs-1 no-padding">
                                                            <a href="#mo-edit-inspection-item-popup" class="mo-popup col-xs-6 no-padding" id="mo-edit-popup">
                                                                <span class="glyphicon glyphicon-pencil"
                                                                      data-section="EXTERIOR_SECTION"
                                                                      data-section-type="{{$ex_item->section_type}}"
                                                                      data-inspection-id="{{$item->inspection_item_id or ''}}"
                                                                      data-repair-cost="{{$item->repair_cost or ''}}"
                                                                      data-note="{{$item->note or ''}}">
                                                                </span>
                                                            </a>
                                                            <a href="#mo-remove-confirm-popup" class="mo-popup col-xs-6 no-padding text-right" id="mo-remove-popup">
                                                                <span class="glyphicon glyphicon-trash" data-inspection-id="{{$item->inspection_item_id or ''}}"></span>
                                                            </a>
                                                        </span>
                                                    </div>
                                                    <hr class="mo-dispute-hr" />
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>

                    <!-- Internal -->
                    <div class="mo-row text-capitalize section-category" data-section-category="interior">
                        <div class="mo-row">
                            <label class="col-xs-9 no-padding">interior</label>
                            <label class="col-xs-2 no-padding text-right">$<span id="total-cost-interior">{{$inspectionItems->interior_section->total_cost_format or 0}}</span></label>
                            <span class="col-xs-1 no-padding"></span>
                        </div>
                        <hr class="mo-dispute-hr" />
                        @if(isset($inspectionItems->interior_section) && !empty($inspectionItems->interior_section))
                            @foreach($inspectionItems->interior_section as $in_item)
                                @if(isset($in_item->items) && count($in_item->items) > 0)
                                    <div class="inspection-section-wrapper" data-section-type="{{$in_item->section_type or ''}}">
                                    @foreach($in_item->items as $item)
                                        <div class="inspection-item-wrapper">
                                            <div class="mo-row font-warm-grey-color height-25 inspection-record">
                                                <span class="col-xs-9 no-padding padding-left-30 inspection-name">{{$item->item or ''}}</span>
                                                <span class="col-xs-2 no-padding text-right padding-right-30">$
                                                <span class="repair-cost-span interior-cost-span cursor-pointer">{{number_format($item->repair_cost, 2)}}</span>
                                                    <input type="number" data-inspection-id="{{$item->inspection_item_id or ''}}" class="repair-cost-input width-80" data-cost-type="repair">
                                                    <img class="mo-loading-icon height-20" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
                                                </span>
                                                <span class="col-xs-1 no-padding">
                                                    <a href="#mo-edit-inspection-item-popup" class="mo-popup col-xs-6 no-padding" id="mo-edit-popup">
                                                        <span class="glyphicon glyphicon-pencil"
                                                              data-section="INTERIOR_SECTION"
                                                              data-section-type="{{$in_item->section_type}}"
                                                              data-inspection-id="{{$item->inspection_item_id or ''}}"
                                                              data-repair-cost="{{$item->repair_cost or ''}}"
                                                              data-note="{{$item->note or ''}}">
                                                        </span>
                                                    </a>
                                                    <a href="#mo-remove-confirm-popup" class="mo-popup col-xs-6 no-padding text-right" id="mo-remove-popup">
                                                        <span class="glyphicon glyphicon-trash" data-inspection-id="{{$item->inspection_item_id or ''}}"></span>
                                                    </a>
                                                </span>
                                            </div>
                                            <hr class="mo-dispute-hr" />
                                        </div>
                                    @endforeach
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>

                    <!-- Mechanical -->
                    <div class="mo-row text-capitalize section-category" data-section-category="mechanical">
                        <div class="mo-row">
                            <label class="col-xs-9 no-padding">mechanical</label>
                            <label class="col-xs-2 no-padding text-right">$<span id="total-cost-mechanical">{{$inspectionItems->mechanical_section->total_cost_format or 0}}</span></label>
                            <span class="col-xs-1 no-padding"></span>
                        </div>
                        <hr class="mo-dispute-hr" />
                        @if(isset($inspectionItems->mechanical_section) && !empty($inspectionItems->mechanical_section))
                            @foreach($inspectionItems->mechanical_section as $me_item)
                                @if(isset($me_item->items) && count($me_item->items) > 0)
                                    <div class="inspection-section-wrapper" data-section-type="{{$me_item->section_type or ''}}">
                                    @foreach($me_item->items as $item)
                                        <div class="inspection-item-wrapper">
                                            <div class="mo-row font-warm-grey-color height-25 inspection-record">
                                                <span class="col-xs-9 no-padding padding-left-30 inspection-name">{{$item->item or ''}}</span>
                                                <span class="col-xs-2 no-padding text-right padding-right-30">$
                                                <span class="repair-cost-span mechanical-cost-span cursor-pointer">{{number_format($item->repair_cost, 2)}}</span>
                                                    <input type="number" data-inspection-id="{{$item->inspection_item_id or ''}}" class="repair-cost-input width-80" data-cost-type="repair">
                                                    <img class="mo-loading-icon height-20" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
                                                </span>
                                                <span class="col-xs-1 no-padding">
                                                    <a href="#mo-edit-inspection-item-popup" class="mo-popup col-xs-6 no-padding" id="mo-edit-popup">
                                                        <span class="glyphicon glyphicon-pencil"
                                                              data-section="MECHANICAL_SECTION"
                                                              data-section-type="{{$me_item->section_type}}"
                                                              data-inspection-id="{{$item->inspection_item_id or ''}}"
                                                              data-repair-cost="{{$item->repair_cost or ''}}"
                                                              data-note="{{$item->note or ''}}">
                                                        </span>
                                                    </a>
                                                    <a href="#mo-remove-confirm-popup" class="mo-popup col-xs-6 no-padding text-right" id="mo-remove-popup">
                                                        <span class="glyphicon glyphicon-trash" data-inspection-id="{{$item->inspection_item_id or ''}}"></span>
                                                    </a>
                                                </span>
                                            </div>
                                            <hr class="mo-dispute-hr" />
                                        </div>
                                    @endforeach
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>

                    <div class="mo-row">
                        <span class="col-xs-9 no-padding"></span>
                        <span class="col-xs-1 no-padding text-capitalize text-right"><label>total: </label></span>
                        <label class="col-xs-2 no-padding padding-left-20">$<span id="total-damage-cost">{{$damageTotal_format or 0}}</span></label>
                    </div>

            </div>

            <div class="mo-row">
                <a href="#mo-add-new-inspection-item-popup" class="mo-popup" id="mo-add-new-popup">
                    <input type="button" class="mo-btn text-uppercase float-right" value="new">
                </a>
            </div>

            @endif

            @if(isset($error))
                <div class="mo-row text-center font-weight-bold font-size16 indian-red-text">
                    {{$error}}
                </div>
            @endif

            <div class="mo-row"><hr class="mo-hr"></div>

            <form class="" id="mo-send-to-auction-form">
                {{ csrf_field() }}
                <input type="hidden" value="{{$vehicleStockInfo->valuation_id or ''}}" name="valuation_id" id="valuation-id">
                <input type="hidden" class="mo-send-resend" name="send_or_resend" value="{{$send_or_resend or "send"}}">
                <input type="hidden" class="mo-auction-id" name="auction_id" value="{{$auction_id or ""}}">
                <div class="mo-row padding-top-20">
                    {{-----SECTION 1-----}}
                    <div class="mo-row">
                        <div class="form-group col-xs-5 no-padding padding-bottom-24">
                            <label for="mo-vehicle-starting-price">Starting Price*</label>
                            <input type="number" pattern="[0-9]*" class="form-control" id="mo-vehicle-starting-price" name="starting_price" value="<?php echo isset($auctionInfo) ? $auctionInfo->start_price : '';  ?>" />
                        </div>
                        <div class="form-group col-xs-5 no-padding padding-bottom-24 float-right">
                            <label for="mo-vehicle-auction-date">Auction Date*</label>
                            <div class="input-group date" id="auction-date-picker">
                                <input type="hidden" value="{{$session_list_date or ''}}" id="mo-vehicle-auction-date-array">
                                <input type="text" class="form-control" id="mo-vehicle-auction-date" name="auction_date" placeholder="DD-MM-YYYY" data-date-format="dd-mm-yyyy" value="{{ $auctionInfo->auctionDate or '' }}" />
                                <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                            </div>
                            <div id="errorToShow_auction_date"></div>
                        </div>
                    </div>
                    <div class="mo-row">
                        <div class="form-group col-xs-5 no-padding padding-bottom-24">
                            <label for="mo-vehicle-buy-now-price">Buy now Price*</label>
                            <input type="number" pattern="[0-9]*" class="form-control disabled-div greaterThan" id="mo-vehicle-buy-now-price" name="buy_now_price" data-min="mo-vehicle-starting-price" value="<?php echo isset($auctionInfo) ? $auctionInfo->buy_now_price : '';  ?>" />
                        </div>
                        <div class="col-xs-5 no-padding padding-bottom-24 float-right">
                            <label for="mo-vehicle-auction-time-selecty">Auction Time*</label>
                            <span><img class="mo-auction-time imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}"></span>
                            <select class="form-control text-capitalize disabled-div" id="mo-vehicle-auction-time-select" name="auction_time_selected_id"></select>
                            <input type="hidden" name="auction_session_id" id="auction_session_id" value="{{ $auctionInfo->auction_session_id or '' }}"/>
                            <div id="errorToShow_auction_time"></div>
                        </div>
                    </div>
                </div>

                <div class="mo-row"><hr class="mo-hr"></div>

                <div class="mo-row border-bottom padding-top-20">
                    {{-----SECTION 2-----}}
                    <div class="mo-row">
                        <div class="form-group col-xs-5 no-padding padding-bottom-24">
                            <input type="checkbox" class="mo-square-check" id="correct-info" name="all_info" value="All information are correct">
                            <label for="correct-info" class="text-capitalize"><span></span>All information are true and correct*</label>
                            <div id="errorToShow_all_info"></div>
                        </div>
                        <div class="form-group col-xs-5 no-padding padding-bottom-24 float-right">
                            <input type="checkbox" class="mo-square-check" id="ppsr" name="ppsr" value="PPSR is cleared">
                            <label for="ppsr" class="text-capitalize"><span></span>PPSR is/will be cleared*</label>
                            <div id="errorToShow_ppsr"></div>
                        </div>
                    </div>
                    <div class="form-group col-xs-5 no-padding padding-bottom-24">
                        <input type="checkbox" class="mo-square-check" id="term-on-sale" name="term_on_sale" value="Agreed on Terms of Sale">
                        <label for="term-on-sale" class="text-capitalize"><span></span>Agreed on Terms of Sale*</label>
                        <div id="errorToShow_term_on_sale"></div>
                    </div>
                </div>

                {{----------FOOTER - BUTTONS----------}}
                <div class="mo-row">
                    <div class="col-xs-12 alert alert-danger hidden" role="alert">
                        <span class="mo-error-message text-capitalize"></span>
                        <a class="mo-close-message close">&times;</a>
                    </div>
                    <div class="col-xs-12 padding-top-24 text-center">
                        <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel" id="mo-cancel-send-auction">
                        @if(!isset($error))
                            <input type="submit" class="mo-btn text-uppercase" value="submit" id="mo-confirm-send-auction">
                        @endif
                    </div>
                </div>
                <input type="hidden" id="mo-offer-id">
                <div class="mo-loading-image hidden">Loading&#8230;</div>
            </form>

        </div>

    </div>
    {{--End - moto Details Content Section--}}

    {{------------ REMOVE CONFIRMATION POPUP ------------}}
    <div id="mo-remove-confirm-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            {{ csrf_field() }}
            <input type="hidden" value="{{$vehicleStockInfo->valuation_id or ''}}" id="inspection-valuation-id">
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">remove inspection item</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <span class="suspend-popup-content">Are you sure you want to remove this item?</span>
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 alert alert-danger hidden" role="alert">
                    <span class="mo-error-message text-capitalize"></span>
                    <a class="mo-close-message close">&times;</a>
                </div>
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-confirm-cancel" data-inspection-item-id="">
                </div>
            </div>
        </form>
        <div class="mo-loading-image hidden">Loading&#8230;</div>
    </div>
    {{--End - moto Popup Section--}}

    {{----------------------------------------------------------- EDIT INSPECTION ITEM POPUP -----------------------------------------------------------}}
    <div id="mo-edit-inspection-item-popup" class="mo-white-popup mo-edit-inspection-item-popup mfp-hide">
        <form class="mo-exterior-popup-form" action="/upload-image" method="post" enctype="multipart/form-data" id="mo-exterior-popup-form" >
            {{ csrf_field() }}
            <input type="hidden" value="0" name="sequence" />
            <input type="hidden" value="{{$vehicleStockInfo->valuation_id or ''}}" id="inspection-valuation-id" name="valuation_id">
            <input type="hidden" value="{{$latest_version_id or ''}}" id="inspection-valuation-version" name="valuation_version" />
            <input type="hidden" value="" id="inspection-item-id" name="exterior-item-id">
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading"><span>Edit</span> Inspection item</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row mo-exterior-popup-content padding-top-24 padding-bottom-20">
                <div class="form-group col-xs-12 col-sm-8">
                    <label for="selection">Section</label>
                    <div class="mo-dropdown-wrapper">
                        <span class="mo-icon-swipe glyphicon glyphicon-menu-down float-right"></span>
                        <select class="form-control mo-inspection-popup-selectbox" name="exterior-section" id="inspection-sections-select" disabled>
                            <option disabled selected>-- Sections list --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-8">
                    <label for="item">Item</label>
                    <div class="mo-dropdown-wrapper">
                        <span class="mo-icon-swipe glyphicon glyphicon-menu-down float-right"></span>
                        <select class="form-control mo-inspection-popup-selectbox" name="exterior-items" id="inspection-items-select" disabled>
                            <option selected disabled>-- Items list --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-8">
                    <label for="estimate-cost">Estimate Cost</label>
                    <input type="number" pattern="[0-9]*" value="0" name="estimate_cost" id="repair-cost" class="form-control mo-inspection-popup-textbox " maxlength="13" {{--onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46"--}}/> <br/>
                </div>
                <div class="form-group col-xs-12">
                    <div class="col-xs-4 mo-inspection-upload-image-box no-padding" id="mo-image-box-1">
                        <label for="fileToUpload" class="mo-inspection-input-file-label vertical-center" id="ImageBrowse">
                            <img src="" alt="Upload" onerror="this.onerror=null; this.src='{{ asset('images/perform_valuation/image_upload.png') }}'" class="mo-inspection-upload-image-icon img-responsive" />
                        </label>

                    </div>
                    <div class="col-xs-4 mo-inspection-upload-image-box no-padding" id="mo-image-box-2">
                        <label for="fileToUpload" class="mo-inspection-input-file-label vertical-center">
                            <img src="" alt="Upload" onerror="this.onerror=null; this.src='{{ asset('images/perform_valuation/image_upload.png') }}'" class="mo-inspection-upload-image-icon img-responsive" />
                        </label>
                    </div>
                    <div class="col-xs-4 mo-inspection-upload-image-box no-padding" id="mo-image-box-3">
                        <label for="fileToUpload" class="mo-inspection-input-file-label vertical-center">
                            <img src="" alt="Upload" onerror="this.onerror=null; this.src='{{ asset('images/perform_valuation/image_upload.png') }}'" class="mo-inspection-upload-image-icon img-responsive" />
                        </label>
                    </div>
                    <input type="file" name="fileToUpload" id="fileToUpload" class="mo-exterior-input-file" />
                </div>
                <div class="form-group col-xs-12 padding-top-24">
                    <label for="note">Note</label>
                    <textarea class="mo-textarea form-control" rows="5" id="note" name="note"  style="box-shadow: none;"></textarea>
                </div>
            </div>
            <hr class="mo-form-hr"/>
            <div class="mo-row padding-top-24">
                <div class="col-xs-12 text-right">
                    <input type="hidden" value="{{Config::get('app.azure_storage_account_name')}}" id="azure-accout-name"/>
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="CANCEL">
                    <input type="button" class="mo-btn text-uppercase" value="UPDATE" id="inspection-update-button">
                </div>
            </div>
        </form>
    </div>
    {{----------------------------------------------------------- EDIT INSPECTION ITEM POPUP -----------------------------------------------------------}}

    {{--POPUP - ADD NEW ITEM--}}
    <div id="mo-add-new-inspection-item-popup" class="mo-white-popup mfp-hide">
        <form action="" method="post" id="mo-add-new-inspection-item-form" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" value="0" name="sequence_add_photo" />
            <input type="hidden" value="{{$latest_version_id or ''}}" id="inspection-valuation-version" name="valuation_version" />
            <input type="hidden" value="{{$vehicleStockInfo->valuation_id or ''}}" id="inspection-valuation-id" name="valuation_id">
            <div class="mo-row padding-bottom-20">
                <div class="mo-row padding-bottom-20">
                    <span class="text-capitalize mo-popup-heading">new inspection item</span>
                </div>
                <hr class="mo-form-hr">
                <div class="form-group col-xs-12 col-sm-12 col-md-8 padding-top-20">
                    <label for="inspection-category" class="text-capitalize">category*</label>
                    <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}" ></span>

                    <select class="form-control" name="inspection_category" id="inspection-category">
                        <option value="EXTERIOR_SECTION">Exterior</option>
                        <option value="INTERIOR_SECTION">Interior</option>
                        <option value="MECHANICAL_SECTION">Mechanical</option>
                    </select>

                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-8">
                    <label for="inspection-section" class="text-capitalize">section*</label>
                    <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}" ></span>

                    <select class="form-control" name="inspection_section" id="inspection-section">

                    </select>

                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-8">
                    <label for="new-inspection-item" class="text-capitalize">item*</label>
                    <span><img class="imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}" ></span>

                    <select class="form-control" name="inspection_item_id" id="new-inspection-item">

                    </select>

                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-8">
                    <label for="estimate-cost">Estimate Cost</label>
                    <input type="number" pattern="[0-9]*" value="0" name="repair_cost" id="repair-cost" class="form-control mo-inspection-popup-textbox " maxlength="13"/> <br/>
                </div>

                <!-- upload image -->
                <div class="form-group col-xs-12">
                    <div class="col-xs-4 mo-inspection-upload-image-box no-padding" id="mo-image-box-1">
                        <label for="inspect-item-photo" data-sequence="1" class="mo-inspection-input-file-label vertical-center" id="ImageBrowse">
                            <img src="/images/perform_valuation/image_upload.png" alt="Upload" onerror="this.onerror=null; this.src='/images/perform_valuation/image_upload.png'" class="mo-inspection-upload-image-icon img-responsive">
                        </label>
                    </div>

                    <div class="col-xs-4 mo-inspection-upload-image-box no-padding" id="mo-image-box-2">
                        <label for="inspect-item-photo" data-sequence="2" class="mo-inspection-input-file-label vertical-center" id="ImageBrowse">
                            <img src="/images/perform_valuation/image_upload.png" alt="Upload" onerror="this.onerror=null; this.src='/images/perform_valuation/image_upload.png'" class="mo-inspection-upload-image-icon img-responsive">
                        </label>
                    </div>

                    <div class="col-xs-4 mo-inspection-upload-image-box no-padding" id="mo-image-box-3">
                        <label for="inspect-item-photo" data-sequence="3" class="mo-inspection-input-file-label vertical-center" id="ImageBrowse">
                            <img src="/images/perform_valuation/image_upload.png" alt="Upload" onerror="this.onerror=null; this.src='/images/perform_valuation/image_upload.png'" class="mo-inspection-upload-image-icon img-responsive">
                        </label>
                    </div>
                    <input type="hidden" value="" name="sequence" />
                    <input type="file" name="fileToUpload" id="inspect-item-photo" class="hidden" />
                </div>

                <!-- end -->
                <div class="form-group col-xs-12 padding-top-24">
                    <label for="note">Note</label>
                    <textarea class="mo-textarea form-control" rows="5" id="note" name="note"  style="box-shadow: none;"></textarea>
                </div>

            </div>

            <hr class="mo-form-hr"/>
            <div class="mo-row">
                <div class="col-xs-12 alert alert-danger hidden" role="alert">
                    <span class="mo-error-message text-capitalize"></span>
                    <a class="mo-close-message close">&times;</a>
                </div>
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel" />
                    <input type="button" class="mo-btn text-uppercase" id="new-item-save" data-add-or-edit="add" value="save" />
                </div>
            </div>
        </form>
    </div>

@endsection