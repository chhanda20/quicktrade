@extends('layouts.master')

@section('title', 'moto - Trading - Stock List')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row padding-bottom-24">
        <div class="col-xs-12 col-sm-8 no-padding" id="mo-content-header">
            {{ $header or 'Auction' }} > <a href="{!! url('/trading/auction') !!}" class="underline-text">{{ $breadcrumb or 'Auction Listing' }}</a> > <span class="yellow-text">Vehicle Details</span>
        </div>
    </div>
    {{--End - moto Content Header Section--}}

    {{--------------------------------------- start VEHICLE IMAGE SLIDER ---------------------------------------}}
    <ul class="bxslider">
        @if (!empty($vehiclePhotos))
            @foreach($vehiclePhotos as $key=>$value)
                <li>
                    <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$value}}?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'" class="img-responsive" crossorigin="Anonymous">
                </li>
            @endforeach
        @else
            <li>
                <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$vehicleDetails->id or ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'" class="img-responsive" crossorigin="Anonymous">
            </li>
        @endif
    </ul>

    <div id="bx-pager" class="">
        @if (!empty($vehiclePhotos))
            @foreach($vehiclePhotos as $key=>$value)
                <a data-slide-index="{{$key}}" href="">
                    <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$value}}?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'">
                </a>
            @endforeach
        @else
            <a data-slide-index="0" href="">
                <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$vehicleDetails->id or ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'">
            </a>
        @endif
    </div>

    {{--------------------------------------- end VEHICLE IMAGE SLIDER ---------------------------------------}}



    {{--------------------------------------- start VEHICLE INFO ---------------------------------------}}

    <div class="mo-row margin-top-32 mo-block-list font-size18 auction-item" item="{{ $auction_id }}">
        <div class="hidden delivery-cost">{{ $auction->delivery_cost }}</div>
        <div class="hidden auction-session-id">{{ $auction->auction_session_id }}</div>
        <div class="hidden location-text">{{ $auction->location_text }}</div>
        <div class="col-xs-10 no-padding">
            <div class="col-xs-12 no-padding text-uppercase font-size24 font-weight-bold margin-bottom-16">{{$auction->vehicle_name or ''}}</div>
            <div class="mo-row">
                <div class="col-xs-12 col-sm-12 no-padding text-capitalize font-size24 font-weight-500 font-black-color">AU $000,000</div>
                <div class="col-xs-12 col-sm-12 no-padding font-size14 font-weight-500 font-warm-grey-color">00 Bids, reserve not met</div>
            </div>

            <div class="mo-row padding-top-10">
                <div class="col-xs-12 col-sm-12 no-padding text-capitalize font-size24 font-weight-500 font-black-color buy-now-price">{{ $auction->buy_now_price_text }}</div>
                <div class="col-xs-12 col-sm-12 no-padding font-size14 font-weight-500 font-warm-grey-color">Buy it now</div>
            </div>
        </div>
        <div class="col-xs-2 no-padding">
            @if(!$isSelling)
                <div class="col-xs-12 col-sm-12 no-padding">
                    <a href="#mo-buy-now-popup" class="vehicle-btn-inactive vehicle-button border-radius-100 float-right text-align-center text-uppercase font-size14 font-weight-900 font-black-color mo-popup buy-now-btn">buy now</a>
                </div>
                <div class="col-xs-12 col-sm-12 padding-top-16 no-padding">
                    <a href="#mo-pre-bid-popup" class="vehicle-btn-inactive vehicle-button border-radius-100 float-right text-align-center text-uppercase font-size14 font-weight-900 font-black-color mo-popup pre-bid-btn">pre bid</a>
                </div>
                <div class="col-xs-12 col-sm-12 padding-top-16 no-padding watch-wrapper">
                    @if($auction->watched)
                        <?php $watch_class = 'vehicle-btn-active' ?>
                    @else
                        <?php $watch_class = 'vehicle-btn-inactive' ?>
                    @endif
                    <a href="#" class="vehicle-button border-radius-100 float-right text-align-center text-uppercase font-size14 font-weight-900 font-black-color mo-watch-icon {{ $watch_class }}">watch</a>
                </div>
            @endif
        </div>

    </div>
    <div id="mo-pre-bid-popup" class="mo-light-gray-popup mfp-hide border-radius-4">
        <form class="mo-popup-form font-size14" id="mo-pre-bid-popup-form">
            {{ csrf_field() }}
            <input type="hidden" id="pre_bid_auction_session_id">
            <input type="hidden" id="pre_bid_auction_id">
            <div class="mo-row padding-bottom-10 padding-top-10 text-align-center">
                <span class="text-uppercase mo-popup-heading">pre bid</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-bottom-40 text-align-center padding-top-40 background-white">
                <span class="font-size20 font-warm-grey-color">Enter max value for auto bid</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row text-align-center">
                <div class="form-group col-xs-12 no-padding padding-bottom-24 padding-top-24 padding-left-40 padding-right-40">
                    <input type="text" class="form-control border-radius-0" id="mo-max-value" name="max_value" value="0" />
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-6 no-padding cancel-but-wrapper">
                    <input type="button" class="cancel-but text-uppercase col-xs-12 no-border background-transparent font-black-color padding-top-10 padding-bottom-6" value="cancel">
                </div>
                <div class="col-xs-6 no-padding">
                    <input type="submit" class="text-uppercase col-xs-12 no-border background-transparent font-black-color padding-top-10 padding-bottom-6" value="submit">
                </div>
            </div>

            <div class="mo-loading-image hidden">Loading...</div>
        </form>
    </div>
    <div id="mo-buy-now-popup" class="mo-light-gray-popup mfp-hide border-radius-4">
        <form class="mo-popup-form font-size14" id="mo-buy-now-popup-form">
            {{ csrf_field() }}
            <input type="hidden" id="buy_now_auction_id">
            <div class="mo-row padding-bottom-10 padding-top-10 text-align-center">
                <span class="text-uppercase mo-popup-heading">buy now</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-bottom-40 text-align-center padding-top-40 background-white">
                <span id="buy-now-price" class="font-size24 font-black-color font-weight-900"></span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-left-16 padding-top-13 padding-right-16 padding-bottom-11 font-black-color font-size18">
                <div class="col-xs-6 no-padding font-size-18"><span>Delivery Cost</span><br><span id="delivery-location"></span></div>
                <div class="col-xs-6 no-padding font-weight-900 text-align-right padding-top-10"><span id="delivery-cost"></span></div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-left-16 padding-top-10 padding-right-16 padding-bottom-24">
                By tapping Buy Now you’re agreeing to, and entering into a binding contract to buy this vehicle from the seller.
            </div>

            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-6 no-padding cancel-but-wrapper">
                    <input type="button" class="cancel-but text-uppercase col-xs-12 no-border background-transparent font-black-color padding-top-10 padding-bottom-6" value="cancel">
                </div>
                <div class="col-xs-6 no-padding">
                    <input type="submit" class="text-uppercase col-xs-12 no-border background-transparent font-black-color padding-top-10 padding-bottom-6" value="submit">
                </div>
            </div>

            <div class="mo-loading-image hidden">Loading...</div>
        </form>
    </div>

    {{--Start - moto Details Content Section--}}
    <div class="mo-row mo-form-wrapper margin-top-32 mo-block-list">
        {{--MOTORONE BLOCK INSIDE--}}
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding mo-5-blocks" id="mo-blocks-wrapper">
            <div class="col-xs-12 col-sm-5 padding-lr5">
                <a href="#details-model">
                    <div class="col-xs-12 mo-block-border-bottom" id="details-model-tab">
                        <span class="col-xs-12 mo-block-txt text-capitalize no-padding">Model Specs & Standard Features</span>
                        <span class="col-xs-12 text-center mo-arrow-focus">
                            <img src="{{ asset('images/svg/main/triangle.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/triangle.png') }}'">
                        </span>
                    </div>
                </a>
            </div>
            <div class="col-xs-12 col-sm-5 padding-lr5">
                <a href="#details-options">
                    <div class="col-xs-12 mo-block-border-bottom" id="details-options-tab">
                        <span class="col-xs-12 mo-block-txt text-capitalize no-padding">Options & Extras</span>
                        <span class="col-xs-12 text-center mo-arrow-focus">
                            <img src="{{ asset('images/svg/main/triangle.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/triangle.png') }}'">
                        </span>
                    </div>
                </a>
            </div>
        </div>
        {{--MODEL SPECS & STANDARD FEATURES--}}
        <div class="mo-row padding-top-36 mo-block-inside-list" id="mo-model-specs-block">
            <div class="col-xs-12 padding-lr5 padding-bottom-24 text-uppercase mo-form-header">
                model specs
            </div>
            <form action="" method="" id="mo-model-spec-form">
                {{ csrf_field() }}

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="make" class="text-capitalize">make</label>
                            <div class="text-capitalize">{{$vehicleDetails->make or ''}}</div>
                            <input type="hidden" value="{{$vehicleDetails->make or ''}}" id="make" name="make">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="engine" class="text-capitalize">engine</label>
                        <div class="text-capitalize">{{$vehicleDetails->engine_type or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->engine_type or ''}}" id="engine" name="engine">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="model" class="text-capitalize">model</label>
                        <div class="text-capitalize">{{$vehicleDetails->model_family or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->model_family or ''}}" id="model" name="model">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="engine-size" class="text-capitalize">engine size</label>
                        <div class="text-capitalize">{{$vehicleDetails->engine_size or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->engine_size or ''}}" id="engine-size" name="engine_size">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="variant" class="text-capitalize">variant</label>
                        <div class="text-capitalize">{{$vehicleDetails->variant or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->variant or ''}}" id="variant" name="variant">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="engine-capacity" class="text-capitalize">engine capacity</label>
                        <div class="text-capitalize">{{$vehicleDetails->engine_capacity or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->engine_capacity or ''}}" id="engine-capacity" name="engine_capacity">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="build-date" class="text-capitalize">build date</label>
                        <div class="text-capitalize">{{date('M Y', strtotime($vehicleDetails->build_date))}}</div>
                        <input type="hidden" value="{{$vehicleDetails->build_date or ''}}" name="build_date" id="build-date">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="number-of-cylinders" class="text-capitalize">number of cylinders</label>
                        <div class="text-capitalize">{{$vehicleDetails->cylinder or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->cylinder or ''}}" id="cylinder" name="cylinder">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="genneral-colour" class="text-capitalize">general colour</label>
                        <div class="text-capitalize">{{$general_color or ''}}</div>
                        <input type="hidden" value="{{$general_color or ''}}" name="general_colour" id="general-colour">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="fuel-type" class="text-capitalize">fuel type</label>
                        <div class="text-capitalize">{{$fuel_type or ''}}</div>
                        <input type="hidden" value="{{$fuel_type or ''}}" name="fuel_type" id="fuel-type">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="manufacturer-colour" class="text-capitalize">manufacturer colour</label>
                        <div class="text-capitalize">{{$manufacturer_color or ''}}</div>
                        <input type="hidden" value="{{$manufacturer_color or ''}}" name="manufacturer_colour" id="manufacturer-colour">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="transmission" class="text-capitalize">transmission</label>
                        <div class="text-capitalize">{{$transmission or ''}}</div>
                        <input type="hidden" value="{{$transmission or ''}}" name="transmission" id="transmission">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="body-type" class="text-capitalize">body type</label>
                        <div class="text-capitalize">{{$vehicleDetails->body_type or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->body_type or ''}}" name="body_type" id="body-type">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="drivetrain" class="text-capitalize">drivetrain</label>
                        <div class="text-capitalize">{{$drive_train or ''}}</div>
                        <input type="hidden" value="{{$drive_train or ''}}" name="drive_train" id="drive-train">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="year-first-registered" class="text-capitalize">year first registered</label>
                        <div class="text-capitalize">{{isset ($vehicleDetails->first_reg_date) && ($vehicleDetails->first_reg_date != "0000-00-00")  ? substr($vehicleDetails->first_reg_date, 0, 4) : ""}}</div>
                        <input type="hidden" value="{{$vehicleDetails->first_reg_date or ''}}" name="year_first_registered" id="year-first-registered">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="body-no">Body no.</label>
                        <div class="text-capitalize">{{$vehicleDetails->body_no or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->body_no or ''}}" name="body_no" id="body-no">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="registration-expiry-date" class="text-capitalize">registration expiry date</label>
                        <div class="text-capitalize">{{date('d M Y', strtotime($vehicleDetails->registration_expiry_date))}}</div>
                        <input type="hidden" value="{{$vehicleDetails->registration_expiry_date or ''}}" name="registration_expiry_date">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="vin" class="text-capitalize">VIN</label>
                        <div class="text-capitalize">{{$vehicleDetails->vin or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->vin or ''}}" name="vin" id="vin">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="registered-state" class="text-capitalize">registered state</label>
                        <div class="text-capitalize">{{$registered_state or ''}}</div>
                        <input type="hidden" value="{{$registered_state or ''}}" name="registered_state" id="registered-state">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="chassis-no">Chassis no.</label>
                        <div class="text-capitalize">{{$vehicleDetails->chassis_number or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->chassis_number or ''}}" name="chassis_number" id="chassis-number">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="registration" class="text-capitalize">registration</label>
                        <div class="text-capitalize">{{$vehicleDetails->registration_number or ''}}</div>
                        <input type="hidden" value="{{$vehicleDetails->registration_number or ''}}" name="registration" id="registration">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="trim-type" class="text-capitalize">trim type</label>
                        <div class="text-capitalize">{{$trim_type or ''}}</div>
                        <input type="hidden" value="{{$trim_type or ''}}" name="trim_type" id="trim-type">
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8">
                        <label for="compliance-date" class="text-capitalize">compliance date</label>
                        <div class="text-capitalize">{{date('d M Y', strtotime($vehicleDetails->compliance_date))}}</div>
                        <input type="hidden" value="{{$vehicleDetails->compliance_date or ''}}" name="compliance_date">
                    </div>
                    <div class="form-group border-top col-xs-12 col-sm-6 col-md-5 padding-bottom-8 padding-top-8 float-right">
                        <label for="trim-colour" class="text-capitalize">trim colour</label>
                        <div class="text-capitalize">{{$trim_color or ''}}</div>
                        <input type="hidden" value="{{$trim_color or ''}}" name="$trim_color" id="trim-color">
                    </div>
                </div>

                {{-- STANDARD FEATURES --}}
                @if(!empty($standardFeatures) && $standardFeatures != null)
                    <div class="col-xs-12 padding-lr5">
                        <div class="padding-top-24 text-uppercase mo-form-header padding-bottom-24">standard features</div>
                    </div>
                    @for ($i = 0; $i < count($standardFeatures); $i++)
                        <div class="col-xs-12 no-padding">
                            <div class="form-group col-xs-12 col-sm-5 padding-top-24 padding-bottom-8 border-bottom {{$i==0?'border-top':null}}">
                                <label class="text-uppercase">
                                    [{{$standardFeatures[$i]->glass_option_id}}] {{$standardFeatures[$i]->name}}
                                </label>
                            </div>
                            <span class="hidden">{{$i++}}</span>
                            @if($i<count($standardFeatures))
                                <div class="form-group col-xs-12 col-sm-5 padding-top-24 padding-bottom-8 border-bottom {{$i==1?'border-top':null}} float-right">
                                    <label class="text-uppercase">
                                        [{{$standardFeatures[$i]->glass_option_id}}] {{$standardFeatures[$i]->name}}
                                    </label>
                                </div>
                            @endif
                        </div>
                    @endfor
                @endif
            </form>

        </div>

        {{-- OPTIONS & EXTRAS --}}
        <div class="mo-row padding-top-36 hidden mo-block-inside-list" id="mo-options-extras-block">
            <form id="mo-create-option-and-extra-form">
                {{--Options--}}
                <div class="col-xs-12 padding-lr5 text-uppercase mo-form-header padding-bottom-24">
                    options
                </div>

                @if (isset($optionalOptions))
                    @foreach ($optionalOptions as $key => $optional_option)
                        <div class="form-group col-xs-12 col-sm-6 col-md-6 padding-bottom-20">
                            <input type="checkbox" disabled class="mo-round-check" id="{{$optional_option->name}}" name="optional_options[]" {{$optional_option->selected!=false?'checked':null}} value="{{$key}}">
                            <label for="{{$optional_option->name}}" class="text-capitalize"><span></span>{{$optional_option->name}}</label>
                        </div>
                    @endforeach
                @endif

                {{--Extras--}}
                <div class="col-xs-12 padding-lr5">
                    <div class="padding-bottom-20 text-uppercase mo-form-header border-bottom">
                        <span>extras</span>
                    </div>

                    @if (isset($extraOptions))
                        @foreach ($extraOptions as $extras_option)
                            <span class="col-xs-12 no-padding padding-top-20 padding-bottom-20 text-capitalize mo-extras-options">
                                <textarea id="{{$extras_option->id}}" class="form-control mo-extras-txt" rows="1" placeholder="Write here…" style="box-shadow: none;" readonly>{{$extras_option->description}}</textarea>
                                <input type="hidden" name="" value="{{$extras_option->id}}">
                            </span>
                        @endforeach
                    @endif

                </div>
            </form>
        </div>
    </div>
    {{--End - moto Details Content Section--}}

    {{--------------------------------------- end VEHICLE INFO ---------------------------------------}}

@endsection