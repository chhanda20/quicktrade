@extends('layouts.master')

@section('title', 'moto - Trading - Auction Listing')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row padding-bottom-24">
        <div class="col-xs-12 col-sm-8 no-padding" id="mo-content-header">
            {{ $header or 'Trading' }} > <span class="yellow-text">{{ $breadcrumb or 'Stock List' }}</span>
        </div>
        {{--Start - moto Search Section--}}
        <div class="col-xs-12 col-sm-4 no-padding">
            {{ csrf_field() }}
            <div class="col-xs-12 no-padding mo-search-wrapper">
                <input type="text" placeholder="Search" id="mo-txt-search">
                <span class="glyphicon glyphicon-search mo-txt-search cursor-pointer"></span>
            </div>
        </div>
        {{--End - moto Search Section--}}
    </div>
    {{--End - moto Content Header Section--}}

    <div class="mo-row padding-bottom-24">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding mo-5-blocks padding-top-24" id="mo-blocks-wrapper">
            @foreach ($tabs as $tab)
                <div class="col-xs-12 col-sm-2 padding-lr5">
                    <a id="{{$tab['id']}}-link" tab="{{$tab['id']}}" href="#{{$tab['id']}}">
                        <div class="col-xs-12 mo-block" id="{{$tab['id']}}_tab">
                            <span class="col-xs-12 mo-block-txt text-capitalize no-padding text-center">{{$tab['name']}}</span>
                            <span class="col-xs-12 text-center mo-arrow-focus">
                                <img src="{{ asset('images/svg/main/triangle-eeeeee.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/triangle.png') }}'">
                            </span>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>


    {{--Start - VEHICLE GRID--}}
    <div class="mo-row" id="mo-auction-grid-wrapper">
        <div class="data-block" id="all-block">
            @include('main.trading.auction.auction-list')
        </div>
        @foreach ($tabs as $tab)
            @if ($tab['id'] == 'all')
                @continue
            @endif
            <div class="data-block" id="{{ $tab['id'] }}-block">
            </div>
        @endforeach
        {{--End - moto VEHICLE GRID--}}
        <form>
            {{ csrf_field() }}
        </form>
    </div>
    <div id="mo-pre-bid-popup" class="mo-light-gray-popup mfp-hide border-radius-4">
        <form class="mo-popup-form font-size14" id="mo-pre-bid-popup-form">
            {{ csrf_field() }}
            <input type="hidden" id="pre_bid_auction_session_id">
            <input type="hidden" id="pre_bid_auction_id">
            <div class="mo-row padding-bottom-10 padding-top-10 text-align-center">
                <span class="text-uppercase mo-popup-heading">pre bid</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-bottom-40 text-align-center padding-top-40 background-white">
                <span class="font-size20 font-warm-grey-color">Enter max value for auto bid</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row text-align-center">
                <div class="form-group col-xs-12 no-padding padding-bottom-24 padding-top-24 padding-left-40 padding-right-40">
                    <input type="text" class="form-control border-radius-0" id="mo-max-value" name="max_value" value="0" />
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-6 no-padding cancel-but-wrapper">
                    <input type="button" class="cancel-but text-uppercase col-xs-12 no-border background-transparent font-black-color padding-top-10 padding-bottom-6" value="cancel">
                </div>
                <div class="col-xs-6 no-padding">
                    <input type="submit" class="text-uppercase col-xs-12 no-border background-transparent font-black-color padding-top-10 padding-bottom-6" value="submit">
                </div>
            </div>

            <div class="mo-loading-image hidden">Loading...</div>
        </form>
    </div>
    <div id="mo-buy-now-popup" class="mo-light-gray-popup mfp-hide border-radius-4">
        <form class="mo-popup-form font-size14" id="mo-buy-now-popup-form">
            {{ csrf_field() }}
            <input type="hidden" id="buy_now_auction_id">
            <div class="mo-row padding-bottom-10 padding-top-10 text-align-center">
                <span class="text-uppercase mo-popup-heading">buy now</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-bottom-40 text-align-center padding-top-40 background-white">
                <span id="buy-now-price" class="font-size24 font-black-color font-weight-900"></span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-left-16 padding-top-13 padding-right-16 padding-bottom-11 font-black-color font-size18">
                <div class="col-xs-6 no-padding font-size-18"><span>Delivery Cost</span><br><span id="delivery-location"></span></div>
                <div class="col-xs-6 no-padding font-weight-900 text-align-right padding-top-10"><span id="delivery-cost"></span></div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-left-16 padding-top-10 padding-right-16 padding-bottom-24">
                By clicking Buy Now you’re agreeing to, and entering into a binding contract to buy this vehicle from the seller.
            </div>

            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-6 no-padding cancel-but-wrapper">
                    <input type="button" class="cancel-but text-uppercase col-xs-12 no-border background-transparent font-black-color padding-top-10 padding-bottom-6" value="cancel">
                </div>
                <div class="col-xs-6 no-padding">
                    <input type="submit" class="text-uppercase col-xs-12 no-border background-transparent font-black-color padding-top-10 padding-bottom-6" value="submit">
                </div>
            </div>

            <div class="mo-loading-image hidden">Loading...</div>
        </form>
    </div>
    <div class="mo-row col-xs-12 text-center mo-loading-wrapper">
        <img class="mo-loading-icon" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
    </div>
@endsection
