@if(isset($vehicle_auction_list) && !empty($vehicle_auction_list))
    @foreach($vehicle_auction_list as $vehicle_auction)
        <div  item="{{ $vehicle_auction->id }}" class="col-xs-12 col-sm-6 col-md-4 padding-bottom-20 auction-item">
            <div class="mo-row mo-vehicle-wrapper">
                <div class="col-xs-12 no-padding mo-image-wrapper vertical-center pos-relative">
                    <div class="mo-status-section no-background pos-absolute font-white-color vertical-center">
                        <span class="col-xs-3 no-padding padding-left-5"></span>
                        <div class="col-xs-9 no-padding">
                        <span class="float-right padding-right-5">
                            {{isset($vehicle_auction->session_date)?date("dMY", strtotime($vehicle_auction->session_date)):""}} {{$vehicle_auction->session_time or ''}}
                        </span>
                        </div>
                    </div>
                    <label>
                        <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$vehicle_auction->vehicle_id or ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/img-add-photo@3x.png')}}'" class="img-responsive">
                    </label>
                </div>
                <div class="col-xs-12 mo-content-wrapper padding-lr10">
                    <div class="mo-row col-xs-12 border-bottom padding-tb10 mo-vehicle-name font-warm-grey-color">
                        {{ $vehicle_auction->vehicle_name }}
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="col-xs-12 no-padding">
                            <span class="col-xs-6 no-padding">
                                @if($vehicle_auction->quick_seller === true)
                                    <img src="{{asset('images/main/speedy-seller.png')}}" alt="speedy seller">
                                    {{--<img src="{{asset('images/main/speedy-payer.png')}}" alt="speedy payer">--}}
                                @endif
                            </span>
                            <div class="col-xs-6 no-padding text-right">
                                <div class="text-capitalize col-xs-12 no-padding">Seller Rating:</div>
                                <span class="starRating float-right pointer-none col-xs-12 no-padding">
                                    @if($vehicle_auction->seller_rating != 0)
                                        @for($i = 5; $i >0; $i--)
                                            <input id="rating-{{$i}}-{{$vehicle_auction->id}}" type="radio" name="rating_{{$vehicle_auction->id}}" value="{{$i}}" {{$vehicle_auction->seller_rating == $i?"checked":""}}>
                                            <label for="seller-rating-{{$i}}">{{$i}}</label>
                                        @endfor
                                    @else
                                        Not Rated
                                    @endif
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Location:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-stock-no">{{ $vehicle_auction->location_text }}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Start price:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold ">${{ number_format($vehicle_auction->start_price, 0, '.', ',') }}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Buy now price:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold ">${{ number_format($vehicle_auction->buy_now_price, 0, '.', ',') }}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Odometer:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold ">{{ number_format($vehicle_auction->odometer, 0, '.', ',') }}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Car Rating:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold ">{{ $vehicle_auction->rating }}</div>
                    </div>
                    <div class="mo-row col-xs-12 no-padding padding-top-20 padding-bottom-20">
                        <div class="col-xs-8 no-padding text-uppercase font-weight-bold yellow-text"></div>
                        <div class="text-capitalize col-xs-4 no-padding text-right">
                            <div class="dropdown">
                                <span class="glyphicon glyphicon-option-horizontal dropdown-toggle cursor-pointer" data-toggle="dropdown"></span>
                                <ul class="dropdown-menu mo-extend-menu-dropdown mo-auction-extend-menu-dropdown">
                                    <li>
                                        <a href="{{ url('/auction/vehicle-detail/' . $vehicle_auction->vehicle_id . '/' . $vehicle_auction->valuation_id . '/' . $vehicle_auction->id . '/1') }}">Details</a>
                                    </li>

                                    @if ($vehicle_auction->editAllow)
                                    <li><a href="{{ url('/trading/send-to-auction/' . $vehicle_auction->vehicle_id) }}/{{ $vehicle_auction->id }}" class="mo-valuation-version" data-valuation-id="">Edit</a></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endforeach
{{--@else--}}
    {{--<div class="mo-row text-center">--}}
        {{--<label class="text-uppercase">There is no item to display at the moment</label>--}}
    {{--</div>--}}
@endif
