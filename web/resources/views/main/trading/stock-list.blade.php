@extends('layouts.master')

@section('title', 'moto - Trading - Stock List')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row padding-bottom-24">
        <div class="col-xs-12 col-sm-8 no-padding" id="mo-content-header">
            {{ $header or 'Trading' }} > <span class="yellow-text">{{ $breadcrumb or 'Stock List' }}</span>
        </div>
        {{--Start - moto Search Section--}}
        <div class="col-xs-12 col-sm-4 no-padding">
            {{ csrf_field() }}
            <div class="col-xs-12 no-padding mo-search-wrapper">
                <input type="text" placeholder="Search" id="mo-txt-search">
                <span class="glyphicon glyphicon-search mo-txt-search cursor-pointer"></span>
            </div>
        </div>
        {{--End - moto Search Section--}}
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - TAB--}}
    <div class="mo-row padding-bottom-24">
        <div class="col-xs-12 no-padding" id="mo-blocks-wrapper">
            <div class="col-xs-12 col-sm-6 padding-lr5">
                <a href="#vehicle-available">
                    <div class="col-xs-12 mo-block mo-offer-blocks text-center" id="mo-vehicle-available-tab">
                        <span class="col-xs-12 no-padding mo-block-txt text-capitalize padding-top-8" data-label="Available">Available</span>
                        <span class="col-xs-12 mo-arrow-focus mo-stock-arrow-focus">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </span>
                    </div>
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 padding-lr5">
                <a href="#vehicle-submitted">
                    <div class="col-xs-12 mo-block mo-offer-blocks text-center" id="mo-vehicle-submitted-tab">
                        <span class="col-xs-12 no-padding mo-block-txt text-capitalize padding-top-8" data-label="Submitted For Auction">Submitted for auction</span>
                        <span class="col-xs-12 mo-arrow-focus mo-stock-arrow-focus">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </span>
                    </div>
                </a>
            </div>
        </div>
    </div>
    {{--End - TAB--}}

    <div class="mo-row mo-block-inside-list" id="mo-vehicle-grid-wrapper">
        @include('main.trading.stock-list-grid')
    </div>

    <div class="mo-row mo-block-inside-list hidden" id="mo-vehicle-submitted-grid-wrapper">
        @include('main.trading.submitted-list-grid')
    </div>

    {{------------ SEND TO AUCTION POPUP ------------}}
    <div id="mo-send-to-auction-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form font-size14" id="mo-send-to-auction-form">
            {{ csrf_field() }}
            <input type="hidden" class="mo-valuation-id" name="valuation_id">
            <input type="hidden" class="mo-send-resend" name="send_or_resend" value="send">
            <input type="hidden" class="mo-auction-id" name="auction_id">
            {{----------HEADER - TITLE----------}}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">send to auction</span>
            </div>
            <hr class="mo-form-hr" />
            {{----------CONTENT----------}}
            <div class="mo-row border-bottom padding-top-24 padding-bottom-24">
                <div class="col-xs-12 col-sm-6 vertical-center" id="mo-auction-image-wrapper">
                    <label>
                        {{--onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'"--}}
                        <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/no_photo.jpg" alt="Vehicle Photo" class="img-responsive" id="mo-vehicle-img">
                    </label>
                </div>
                {{-----VEHICLE INFO-----}}
                <div class="col-xs-12 col-sm-6" id="mo-vehicle-info">
                    <div class="mo-row">
                        <div class="col-xs-12 no-padding font-weight-bold font-size20" id="mo-vehicle-name"></div>
                    </div>
                    <div class="mo-row">
                        <div class="col-xs-7 no-padding text-capitalize">days in stock:</div>
                        <div class="col-xs-5 no-padding text-capitalize font-weight-bold" id="mo-vehicle-days-stock"></div>
                    </div>
                    <div class="mo-row">
                        <div class="col-xs-7 no-padding text-capitalize">stock no.:</div>
                        <div class="col-xs-5 no-padding text-capitalize font-weight-bold" id="mo-vehicle-stock-no"></div>
                    </div>
                    <div class="mo-row">
                        <div class="col-xs-7 no-padding text-capitalize">registration no.:</div>
                        <div class="col-xs-5 no-padding text-capitalize font-weight-bold" id="mo-vehicle-reg-no"></div>
                    </div>
                    <div class="mo-row">
                        <div class="col-xs-7 no-padding text-capitalize">trade-in price:</div>
                        <div class="col-xs-5 no-padding text-capitalize font-weight-bold" id="mo-vehicle-total-cost"></div>
                    </div>
                    <div class="mo-row">
                        <div class="col-xs-7 no-padding text-capitalize">location:</div>
                        <div class="col-xs-5 no-padding text-capitalize font-weight-bold" id="mo-vehicle-location"></div>
                    </div>
                </div>
            </div>
            <div class="mo-row border-bottom padding-top-20">
                {{-----SECTION 1-----}}
                <div class="mo-row">
                    <div class="form-group col-xs-5 no-padding padding-bottom-24">
                        <label for="mo-vehicle-starting-price">Starting Price*</label>
                        <input type="number" pattern="[0-9]*" class="form-control" id="mo-vehicle-starting-price" name="starting_price">
                    </div>
                    <div class="form-group col-xs-5 no-padding padding-bottom-24 float-right">
                        <label for="mo-vehicle-auction-date">Auction Date*</label>
                        <div class="input-group date" id="auction-date-picker">
                            <input type="hidden" value="{{$session_list_date or ''}}" id="mo-vehicle-auction-date-array">
                            <input type="text" class="form-control" id="mo-vehicle-auction-date" name="auction_date" placeholder="DD-MM-YYYY" data-date-format="dd-mm-yyyy"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        </div>
                        <div id="errorToShow_auction_date"></div>
                    </div>
                </div>
                <div class="mo-row">
                    <div class="form-group col-xs-5 no-padding padding-bottom-24">
                        <label for="mo-vehicle-buy-now-price">Buy now Price*</label>
                        <input type="number" pattern="[0-9]*" class="form-control" id="mo-vehicle-buy-now-price" name="buy_now_price">
                    </div>
                    <div class="col-xs-5 no-padding padding-bottom-24 float-right">
                        <label for="mo-vehicle-auction-time-selecty">Auction Time*</label>
                        <span><img class="mo-auction-time imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}"></span>
                        <select class="form-control text-capitalize disabled-div" id="mo-vehicle-auction-time-select" name="auction_time_selected_id"></select>
                        <div id="errorToShow_auction_time"></div>
                    </div>
                </div>
            </div>
            <div class="mo-row border-bottom padding-top-20">
                {{-----SECTION 2-----}}
                <div class="mo-row">
                    <div class="form-group col-xs-5 no-padding padding-bottom-24">
                        <input type="checkbox" class="mo-round-check" id="correct-info" name="all_info" value="All information are correct">
                        <label for="correct-info" class="text-capitalize"><span></span>All information are correct*</label>
                        <div id="errorToShow_all_info"></div>
                    </div>
                    <div class="form-group col-xs-5 no-padding padding-bottom-24 float-right">
                        <input type="checkbox" class="mo-round-check" id="ppsr" name="ppsr" value="PPSR is cleared">
                        <label for="ppsr" class="text-capitalize"><span></span>PPSR is cleared*</label>
                        <div id="errorToShow_ppsr"></div>
                    </div>
                </div>
                <div class="form-group col-xs-5 no-padding padding-bottom-24">
                    <input type="checkbox" class="mo-round-check" id="term-on-sale" name="term_on_sale" value="Agreed on Terms of Sale">
                    <label for="term-on-sale" class="text-capitalize"><span></span>Agreed on Terms of Sale*</label>
                    <div id="errorToShow_term_on_sale"></div>
                </div>
            </div>
            <div class="mo-row padding-top-20">
                {{-----SECTION 3-----}}
                <div class="form-group col-xs-5 no-padding padding-bottom-24">
                    <input type="checkbox" class="mo-round-check" id="extended" name="extended" value="Extended">
                    <label for="extended" class="text-capitalize"><span></span>Extended</label>
                </div>
                <div class="form-group col-xs-5 no-padding padding-bottom-24 float-right">
                    <input type="checkbox" class="mo-round-check" id="ex-rental" name="ex_rental" value="Ex rental">
                    <label for="ex-rental" class="text-capitalize"><span></span>Ex rental</label>
                </div>
                <div class="form-group col-xs-5 no-padding padding-bottom-24">
                    <input type="checkbox" class="mo-round-check" id="ex-hire" name="ex_hire" value="Ex hire">
                    <label for="ex-hire" class="text-capitalize"><span></span>Ex hire</label>
                </div>
            </div>
            <hr class="mo-form-hr" />
            {{----------FOOTER - BUTTONS----------}}
            <div class="mo-row">
                <div class="col-xs-12 alert alert-danger hidden" role="alert">
                    <span class="mo-error-message text-capitalize"></span>
                    <a class="mo-close-message close">&times;</a>
                </div>
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="submit" id="mo-confirm-send-auction">
                </div>
            </div>
            <input type="hidden" id="mo-offer-id">
            <div class="mo-loading-image hidden">Loading&#8230;</div>
        </form>
    </div>
    {{--End - moto Popup Section--}}

    {{------------ REMOVE CONFIRMATION POPUP ------------}}
    <div id="mo-remove-confirm-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">cancel request auction</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <span class="suspend-popup-content">Are you sure you want to cancel this request?</span>
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 alert alert-danger hidden" role="alert">
                    <span class="mo-error-message text-capitalize"></span>
                    <a class="mo-close-message close">&times;</a>
                </div>
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-confirm-cancel" data-auction-id="">
                </div>
            </div>
        </form>
        <div class="mo-loading-image hidden">Loading&#8230;</div>
    </div>
    {{--End - moto Popup Section--}}

    {{------------ REJECT REASON POPUP ------------}}
    <div id="mo-reject-reason-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading" id="mo-reject-reason"></span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <span class="suspend-popup-content" id="mo-reject-content"></span>
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 alert alert-danger hidden" role="alert">
                    <span class="mo-error-message text-capitalize"></span>
                    <a class="mo-close-message close">&times;</a>
                </div>
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="close">
                </div>
            </div>
        </form>
    </div>
    {{--End - moto Popup Section--}}
@endsection