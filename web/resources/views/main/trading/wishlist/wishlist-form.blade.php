@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')

    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <a href="#">{{ $header or 'Wishlist'}}</a> > {{ $breadcrumb or 'New'}}
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">

        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
            
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                wishlist details
            </div>

            @if ($type == 'new')
            <form action="{!! url("/trading/create-wishlist"); !!}" method="post" id="mo-wishlist-form">
            @else
            <form action="{!! url("/trading/update-wishlist"); !!}/{{$wish_list_detail->id or null}}" method="post" id="mo-wishlist-form">
            @endif
                @if (isset($wish_list_detail) && !empty($wish_list_detail))
                    <input type="hidden" value="{{isset($wish_list_detail) ? json_encode($wish_list_detail) : null }}" name="wish_list_detail">
                @endif
                {{csrf_field()}}

                <!-- MAKE -->
                <div class="mo-row">
                    @if(isset($error_result) && !empty($error_result))
                        <div class="alert alert-danger">
                            <strong>Error!</strong> {{$error_result}}.
                        </div>
                    @endif
                    @if (session("info"))
                        <div class="alert alert-success alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <strong>Success!</strong> {{session("info")}}
                        </div>
                    @endif
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <div>
                            <label for="make-model" class="text-capitalize">make</label>
                        </div>    
                        <div>
                            <select class="form-control" name="wishlist_make" id="wishlist-make" multiple="multiple">
                                @if (isset($make_list))
                                    @foreach ($make_list as $make)
                                        <option value="{{$make->name}}">{{$make->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <!-- end MAKE -->

                <!-- MODEL -->
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        
                        <div>
                            <label for="model" class="text-capitalize">model</label>
                        </div>
                        
                        <div>
                            <select class="form-control" name="wishlist_model[]" id="wishlist-model" multiple="multiple">

                            </select>
                        </div>    
                    </div>
                </div>
                <!-- end MODEL -->

                <!-- VARIANT -->
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <div>
                            <label for="variant" class="text-capitalize">variant</label>
                        </div>
                        
                        <div>
                            <select name="wishlist_variant" id="wishlist-variant" class="form-control" multiple="multiple">
                            </select>
                        </div>
                    </div>
                </div> 
                <!-- end VARIANT -->

                <!-- ODO RANGE -->
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <div class="col-xs-6 no-padding text-right">
                            <span class="slide-txt-display hidden mo-filter" id="wishlist-odo-filter"></span>
                        </div>
                        <span class="mo-wishlist-range">
                            <p class="mo-row">
                                <label for="wishlist-odo-amount" class="col-xs-4 col-sm-4 no-padding text-capitalize">Odo Range:</label>
                                <input type="text" id="wishlist-odo-amount" name="wishlist_odo_amount" class="slide-txt-display col-xs-12 col-sm-8" readonly>
                                <input type="hidden" id="wishlist-odo-range-from">
                                <input type="hidden" id="wishlist-odo-range-to">
                            </p>
                            <div id="wishlist-odo-slider-range" class="col-xs-12"></div>
                        </span>

                    </div>
                </div>
                <!-- end ODO RANGE -->

                <!-- YEAR RANGE -->
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <div class="col-xs-6 no-padding text-right">
                            <span class="slide-txt-display hidden mo-filter" id="wishlist-year-filter"></span>
                        </div>
                        <span class="mo-wishlist-range">
                            <p class="mo-row">
                                <label for="wishlist-year-amount" class="col-xs-4 col-sm-4 no-padding text-capitalize">year range:</label>
                                <input type="text" id="wishlist-year-amount" name="wishlist_year_amount" class="slide-txt-display col-xs-12 col-sm-8" readonly>
                                <input type="hidden" id="wishlist-year-range-from">
                                <input type="hidden" id="wishlist-year-range-to">
                            </p>
                            <div id="wishlist-year-slider-range" class="col-xs-12"></div>
                        </span>
                    </div>
                </div>
                <!-- end YEAR RANGE -->

                <!-- LOCATION -->
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <div>
                            <label for="location" class="text-capitalize">location</label>
                        </div>

                        <div>
                            <select class="form-control" name="wishlist_location[]" id="wishlist-location" multiple="multiple">
                                @if (isset($location_list))
                                    @foreach ($location_list as $location)
                                        <option value="{{$location->id}}">{{$location->name}} - {{$location->state_text}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                    </div>
                </div>
                <!-- end LOCATION -->

                <!-- TRANSMISSION TYPE -->
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <div>
                            <label for="transmission-type" class="text-capitalize">transmission type</label>
                        </div>
                        <div>
                            <select class="form-control" name="wishlist_transmission_type[]" id="wishlist-transmission-type" multiple="multiple">
                                {{--<option disabled selected></option>--}}
                                @if (isset($transmission_type_list))
                                    @foreach ($transmission_type_list as $key=>$value)
                                        <option value="{{$value}}">{{$value}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <!-- end TRANSMISSION TYPE -->

                <!-- PRICE RANGE -->
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <div class="col-xs-6 no-padding text-right">
                            <span class="slide-txt-display hidden mo-filter" id="wishlist-price-filter"></span>
                        </div>
                        <span class="mo-wishlist-range">
                            <p class="mo-row">
                                <label for="wishlist-price-amount" class="col-xs-4 col-sm-4 no-padding text-capitalize">price range:</label>
                                <input type="text" id="wishlist-price-amount" name="wishlist_price_amount" class="slide-txt-display col-xs-12 col-sm-8" readonly>
                                <input type="hidden" id="wishlist-price-range-from">
                                <input type="hidden" id="wishlist-price-range-to">
                            </p>
                            <div id="wishlist-price-slider-range" class="col-xs-12"></div>
                        </span>
                    </div>
                </div> 
                <!-- end PRICE RANGE -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        @if(strtolower($breadcrumb)=="new")
                            <input type="button" class="mo-btn text-uppercase" value="create" id="wishlist-create-btn">
                        @elseif(strtolower($breadcrumb)=="edit")
                            <input type="button" class="mo-btn text-uppercase" value="update" id="wishlist-update-btn">
                        @endif
                    </div>
                </div>

            </form>
        </div>

    </div>
    <div class="mo-loading-image hidden">Loading&#8230;</div>

@endsection