{{--Start - VEHICLE GRID--}}
@if(isset($vehicle_submitted_list) && !empty($vehicle_submitted_list))
    @foreach($vehicle_submitted_list as $vehicle_submitted)
        <div class="col-xs-12 col-sm-6 col-md-4 padding-bottom-20">
            <div class="mo-row mo-vehicle-wrapper">
                <div class="col-xs-12 no-padding mo-image-wrapper vertical-center">
                    <?php
                    switch ($vehicle_submitted->status) {
                        case 1:
                            $statusText = 'Pending';
                            $statusClass = 'mo-status-pending';
                            $cancelAction = TRUE;
                            $resendAction = FALSE;
                            break;
                        case 2:
                            $statusText = 'Approved';
                            $statusClass = 'mo-status-approved';
                            $cancelAction = FALSE;
                            $resendAction = FALSE;
                            break;
                        case 3:
                            $statusText = 'Rejected';
                            $statusClass = 'mo-status-rejected';
                            $resendAction = TRUE;
                            $cancelAction = FALSE;
                            break;
                    }
                    ?>
                    <div class="mo-status-section {{ $statusClass }} pos-absolute font-white-color vertical-center">
                        <span class="col-xs-3 no-padding padding-left-5">{{ $statusText }}</span>
                        @if ($vehicle_submitted->status == 3 && $vehicle_submitted->comment != '')
                            <div class="col-xs-9 no-padding">
                                <a href="#mo-reject-reason-popup" data-comment="{{$vehicle_submitted->comment}}" data-reject-reason="{{$vehicle_submitted->reject_reason}}" class="mo-stock-popup float-right" id="mo-reject-info-popup">
                                    <span class="glyphicon glyphicon-info-sign float-right padding-right-5"></span>
                                </a>
                            </div>
                        @elseif ($vehicle_submitted->status == 2)
                            <div class="col-xs-9 no-padding">
                                <span class="float-right padding-right-5">
                                    {{isset($vehicle_submitted->session_date)?date("dMY", strtotime($vehicle_submitted->session_date)):""}} {{$vehicle_submitted->session_time or ''}}
                                </span>
                            </div>
                        @endif
                    </div>
                    <label>
                        <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$vehicle_submitted->vehicle_id or ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'" class="img-responsive">
                    </label>
                </div>
                <div class="col-xs-12 mo-content-wrapper padding-lr10">
                    <input type="hidden" value="{{$vehicle_submitted->valuation_id or ''}}" id="mo-valuation-id">
                    <input type="hidden" value="{{$vehicle_submitted->vehicle_id or ''}}" id="mo-vehicle-id">
                    <input type="hidden" value="{{$vehicle_submitted->id or ''}}" id="mo-auction-id">
                    <div class="mo-row col-xs-12 border-bottom padding-tb10 font-weight-bold mo-vehicle-name">
                        {{$vehicle_submitted->vehicle_name or ''}}
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Location:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-location">{{$vehicle_submitted->location_text or ''}}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Start price:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-start-price">${{isset($vehicle_submitted->start_price) ? number_format($vehicle_submitted->start_price, 2) : number_format(0.00, 2)}}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Buy now price:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-buy-now-price">${{isset($vehicle_submitted->buy_now_price) ? number_format($vehicle_submitted->buy_now_price, 2) : number_format(0.00, 2)}}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Odometer:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-odometer">{{isset($vehicle_submitted->odometer) ? number_format($vehicle_submitted->odometer) : 0}}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Car Rating:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-car-rating">{{$vehicle_submitted->rating or ''}}</div>
                    </div>
                    <div class="mo-row col-xs-12 no-padding padding-top-20 padding-bottom-20">
                        <div class="col-xs-6 no-padding text-uppercase font-weight-bold yellow-text">
                            {{--<a href="#mo-send-to-auction-popup" class="mo-popup">send to auction</a>--}}
                            @if ($cancelAction)
                                <a href="#mo-remove-confirm-popup" class="mo-stock-popup" id="mo-cancel-popup">cancel</a>
                            @endif
                            @if ($resendAction)
                                {{--POPUP--}}
                                {{--<a href="#mo-send-to-auction-popup" class="mo-stock-popup" id="mo-resend-popup">re-send</a>--}}
                                {{--NEW PAGE--}}
                                <a href="{!! url('/trading/send-to-auction') !!}/{{$vehicle_submitted->vehicle_id or ''}}/{{$vehicle_submitted->id}}">send to auction</a>
                            @endif
                        </div>
                        <div class="text-capitalize col-xs-6 no-padding text-right">
                            <a href="{!! url('/trading/showroom-vehicle-detail') !!}/{{$vehicle_submitted->vehicle_id or ''}}/{{$vehicle_submitted->valuation_id or ''}}">
                                <span class="glyphicon glyphicon-option-horizontal"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@else
    <div class="col-xs-12 text-center text-uppercase font-family-bold">there are no items to display at this time</div>
@endif

<div class="mo-row col-xs-12 text-center mo-loading-wrapper">
    <img class="mo-loading-icon" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
</div>
{{--End - VEHICLE GRID--}}