{{--Start - VEHICLE GRID--}}
@if(isset($vehicle_stock_list) && !empty($vehicle_stock_list))
    @foreach($vehicle_stock_list as $vehicle_stock)
        <div class="col-xs-12 col-sm-6 col-md-4 padding-bottom-20">
            <div class="mo-row mo-vehicle-wrapper">
                <div class="col-xs-12 no-padding mo-image-wrapper vertical-center">
                    <label>
                        <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{isset($vehicle_stock->id) ? $vehicle_stock->id : ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'" class="img-responsive">
                    </label>
                </div>
                <div class="col-xs-12 mo-content-wrapper padding-lr10">
                    <input type="hidden" value="{{$vehicle_stock->valuation_id or ''}}" id="mo-valuation-id">
                    <input type="hidden" value="{{$vehicle_stock->id or ''}}" id="mo-vehicle-id">
                    <div class="mo-row col-xs-12 border-bottom padding-tb10 font-weight-bold mo-vehicle-name">
                        {{$vehicle_stock->name or ''}}
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Days in Stock:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-days-stock">{{$vehicle_stock->day_in_stock or ''}}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">stock no:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-stock-no">{{$vehicle_stock->stock_number or ''}}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">registration no.:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-reg-no">{{$vehicle_stock->registration_number or ''}}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">trade-in price:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-total-cost">${{isset($vehicle_stock->trade_price) ? number_format($vehicle_stock->trade_price, 2) : number_format(0.00, 2)}}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">location:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-location">{{$vehicle_stock->location_name or ''}}</div>
                    </div>
                    <div class="mo-row col-xs-12 no-padding padding-top-20 padding-bottom-20">
                        <div class="col-xs-6 no-padding text-uppercase font-weight-bold yellow-text">
                            {{--POPUP--}}
                            {{--<a href="#mo-send-to-auction-popup" class="mo-stock-popup" id="mo-send-popup">send to auction</a>--}}
                            {{--NEW PAGE--}}
                            <a href="{!! url('/trading/send-to-auction') !!}/{{$vehicle_stock->id or ''}}">send to auction</a>
                        </div>
                        <div class="text-capitalize col-xs-6 no-padding text-right">
                            <a href="{!! url('/trading/showroom-vehicle-detail') !!}/{{$vehicle_stock->id or ''}}/{{$vehicle_stock->valuation_id or ''}}">
                                <span class="glyphicon glyphicon-option-horizontal"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@else
    <div class="col-xs-12 text-center text-uppercase font-family-bold">there are no items to display at this time</div>
@endif

<div class="mo-row col-xs-12 text-center mo-loading-wrapper">
    <img class="mo-loading-icon" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
</div>
{{--End - VEHICLE GRID--}}