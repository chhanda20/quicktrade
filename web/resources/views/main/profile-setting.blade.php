@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')

    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <a href="{!! url('dealership/profile-setting') !!}">{{ $header or Session::get('user_info')->first_name.' '.Session::get('user_info')->last_name }} Profile</a>
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-md-12 no-padding">
            <div class="col-xs-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                profile details
            </div>
        </div>
        <form action="{!! url("/dealership/update-profile-setting"); !!}" method="post" id="mo-profile-setting-form">
            {{csrf_field()}}
            <div class="col-xs-12 col-sm-6 alert alert-success hidden" role="alert">
                You've update profile setting successfully !!!
                <a class="mo-close-message close">&times;</a>
            </div>
            <div class="mo-row">
                <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                    <label for="profile-password" class="text-capitalize">password</label>
                    <span class="mo-user-password fa fa-eye cursor-pointer"></span>
                    <input type="password" class="form-control" id="profile-password"  name="profile_password" autocomplete="new-password"/>
                    <span class="mo-user-show-password fa fa-eye-slash cursor-pointer"></span>
                </div>
            </div>
            <div class="mo-row">
                <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                    <label for="profile-quick-code" class="text-capitalize">quick code</label>
                    <span class="mo-profile-password fa fa-eye cursor-pointer"></span>
                    <input type="password" class="form-control" id="profile-quick-code"  name="profile_quick_code" autocomplete="new-password" maxlength="4" minlength="4"/>
                    <span class="mo-profile-show-password fa fa-eye-slash cursor-pointer"></span>
                </div>
            </div>

            {{--checkbox--}}
            <div class="col-xs-12 col-md-12 no-padding">
                <div class="col-xs-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                    notifications list
                </div>
            </div>
            <div class="mo-row padding-bottom-24">
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <input type="checkbox" class="mo-square-check mo-notification-setting-check" id="email_only" name="email_notification"
                           {{--{{var_dump($profile_settings)}}--}}
                        {{isset($profile_settings->receive_email) && $profile_settings->receive_email == 1 ? "checked":''}}>
                    <label for="email_only" class="text-capitalize"><span></span>email notification?</label>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <input type="checkbox" class="mo-square-check mo-notification-setting-check" id="noti_only" name="push_notification"
                            {{isset($profile_settings->receive_noti) && $profile_settings->receive_noti == 1 ? "checked":''}}>
                    <label for="noti_only" class="text-capitalize"><span></span>push notification?</label>
                </div>
                <input type="hidden" id="notification-checked" name="notification_checked">
                <div class="col-xs-12 col-sm-12 col-md-12" id="error-message"></div>
            </div>

            {{--<div class="mo-row padding-bottom-24">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <input type="checkbox" class="mo-round-check" id="push-notification" name="push_notification"
                        {{isset($profile_settings->receive_noti) && $profile_settings->receive_noti == 1 ? "checked":''}}>
                    <label for="push-notification" class="text-capitalize"><span></span>push notification?</label>
                </div>
            </div>--}}

            @if(isset($user_event))
                @foreach($user_event as $user)
                    <div class="col-xs-12 col-md-12 no-padding">
                        <div class="col-xs-12 col-md-12 no-padding padding-bottom-24 text-uppercase font-size16 yellow-text font-weight-900">
                            {{$user["category_type"]}}
                        </div>
                    </div>
                    <div class="mo-row padding-bottom-24">
                        @foreach($user["event_list"] as $event)
                            <div class="col-xs-12 col-sm-4 col-md-4 padding-bottom-24">
                                <input type="checkbox" class="mo-square-check mo-notification-check" id="{{$event->code}}" name="event_codes[]"
                                    {{isset($event->config) && $event->config != "none" ?"checked":""}}>
                                <label for="{{$event->code}}" class="text-capitalize"><span></span>{{$event->code}}</label>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            @endif

            <div class="mo-row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <input type="submit" class="mo-btn text-uppercase" id="mo-update-profile-setting" value="update">
                </div>
            </div>
            <div class="mo-loading-image hidden">Loading&#8230;</div>
        </form>
    </div>
@endsection