@extends('layouts.master')

@section('title', 'moto - Location Form')

@section('content')
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            Configure > <a class="underline-text" href="{!! url('/dealership/location') !!}">{{ $header or 'Location'}}</a> > <span class="yellow-text">{{ $breadcrumb or 'New'}}</span>
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
    	<div class="col-xs-12 no-padding">
    		<div class="col-xs-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                location details
            </div>

            @if((isset($location_details) && !empty($location_details)) || $mode == "new")
                <form action="" method="post" id="mo-create-location-form">
                    {{ csrf_field() }}

                    @if($mode == "edit")
                        <div class="mo-row">
                            <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                                {{--SELLER RATING--}}
                                <div class="col-xs-12 col-sm-4 no-padding">
                                    <label for="seller_rating" class="text-capitalize col-xs-12 no-padding">Seller Rating:</label>
                                    <span class="starRating pointer-none col-xs-12 no-padding">
                                    @if($location_details->seller_rating != 0)
                                            @for($i = 5; $i >0; $i--)
                                                <input id="seller-rating-{{$i}}" type="radio" value="{{$i}}" {{$location_details->seller_rating == $i ? "checked" : ""}}>
                                                <label for="seller-rating-{{$i}}">{{$i}}</label>
                                            @endfor
                                        @else
                                            Not Rated
                                        @endif
                            </span>
                                </div>
                                {{--BUYER RATING--}}
                                <div class="col-xs-12 col-sm-4">
                                    <label for="buyer_rating" class="text-capitalize col-xs-12 no-padding">Buyer Rating:</label>
                                    <span class="starRating pointer-none col-xs-12 no-padding">
                                    @if($location_details->buyer_rating != null)
                                            @for($i = 5; $i >0; $i--)
                                                <input id="seller-rating-{{$i}}" type="radio" value="{{$i}}" {{$location_details->buyer_rating == $i ? "checked" : ""}}>
                                                <label for="seller-rating-{{$i}}">{{$i}}</label>
                                            @endfor
                                        @else
                                            Not Rated
                                        @endif
                            </span>
                                </div>
                                {{--SPEEDY SELLER--}}
                                <div class="col-xs-12 col-sm-2 no-padding text-center">
                                    @if($location_details->quick_seller === true)
                                        <img src="{{asset('images/main/speedy-seller.png')}}" alt="speedy seller">
                                    @endif
                                </div>
                                {{--SPEEDY BUYER--}}
                                <div class="col-xs-12 col-sm-2 no-padding text-center">
                                    @if($location_details->quick_buyer === true)
                                        <img src="{{asset('images/main/speedy-payer.png')}}" alt="speedy payer">
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                            <label for="lmct" class="text-uppercase">lmct</label>
                            <input type="text" class="form-control" id="dealer-location-lmct" name="dealer_location_lmct"
                                   value="{{$location_details->lmct or ''}}">
                        </div>
                    </div> <!-- Add Row LMCT -->

                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                            <label for="name" class="text-capitalize">name*</label>
                            <input type="text" class="form-control" id="dealer-location-name" name="dealer_location_name"
                                   value="{{$location_details->name or ''}}">
                        </div>
                    </div> <!-- Row 1 -->

                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                            <label for="address" class="text-capitalize">code*</label>
                            <input type="text" class="form-control" id="dealer-location-code" name="dealer_location_code"
                                   value="{{$location_details->location_code or ''}}">
                        </div>
                    </div> <!-- Row 2 -->

                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                            <label for="address" class="text-capitalize">address*</label>
                            <input type="text" class="form-control" id="dealer-location-address" name="dealer_location_address"
                                   value="{{$location_details->address or ''}}">
                        </div>
                    </div> <!-- Add Row Address -->

                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                            <label for="suburb" class="text-capitalize">suburb*</label>
                            <input type="text" class="form-control" id="dealer-location-suburb" name="dealer_location_suburb"
                                   value="{{$location_details->suburb or ''}}">

                        </div>
                    </div> <!-- Add Row Suburb -->

                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6 padding-bottom-24">
                            <label for="post-code" class="text-capitalize">postcode</label>
                            <input type="number" class="form-control" id="dealer-location-post-code" name="dealer_location_post_code"
                                   value="{{$location_details->postcode or ''}}">
                        </div>
                    </div>

                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                            <label for="state" class="text-capitalize">state*</label>
                            {{--<input type="text" class="form-control" id="dealer-location-suburb" name="dealer_location_suburb" value="">--}}
                            <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                            <select class="form-control" name="dealer_location_state" id="dealer-location-state">
                                <option selected disabled>Please select</option>

                            </select>
                            <input type="hidden" value="{{$selected_state or ''}}" name="selected-state">
                        </div>
                    </div> <!-- Add Row State -->

                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                            <label for="country" class="text-capitalize">country*</label>
                            {{--<input type="text" class="form-control" id="dealer-location-suburb" name="dealer_location_suburb" value="">--}}
                            <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                            <select class="form-control" name="dealer_location_country" id="dealer-location-country" disabled="true">
                                {{--<option selected disabled>Please select</option>--}}

                                @if(isset($country_list))
                                    @foreach($country_list as $country)
                                        <option {{$country->id == 13 ? "selected": ''}} value="{{$country->id}}" {{isset($location_details->country) && $country->id == $location_details->country ? "selected" : ""}}>{{$country->name}}</option>
                                    @endforeach
                                @endif

                            </select>
                            <input type="hidden" value="" id="input-country-id" name="dealer_location_country_input">
                            <input type="hidden" value="{{isset($country_list) ? json_encode($country_list) : null}}" name="country-list" id="country-list" />
                        </div>
                    </div> <!-- Add Row Country -->

                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                            <label for="address" class="text-capitalize">status</label>
                            <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                            <select class="form-control text-capitalize" id="dealer-location-status" name="dealer_location_status">
                                {{--<option selected disabled>status list</option>--}}
                                @if (isset($location_status_list))
                                    @if (isset($location_details))
                                        @foreach ($location_status_list as $status)
                                            <option {{isset($location_details->status_code) && $location_details->status_code == $status->code ? 'selected' : ''}} value="{{$status->code}}">{{$status->value}}</option>
                                        @endforeach
                                    @else
                                        @foreach ($location_status_list as $status)
                                            <option {{$status->code == 2 ? 'selected' : ''}} value="{{$status->code}}">{{$status->value}}</option>
                                        @endforeach
                                    @endif
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 alert alert-danger hidden" role="alert">
                        <span class="mo-error-message text-capitalize"></span>
                        <a class="mo-close-message close">&times;</a>
                    </div>
                    <div class="mo-loading-image hidden">Loading&#8230;</div>

                    <input type="hidden" id="mo-dealership-id" value="{{$company_details->id or ''}}">
                    <input type="hidden" id="mo-location-id" value="{{$location_details->id or ''}}">
                    <input type="hidden" name="dealer_status" value="{{$company_details->status_code or 2}}">
                    <input type="hidden" id="new-or-edit" value="{{$type or ''}}" name="new_or_edit">

                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-30">
                            <input type="button" class="mo-btn text-uppercase" value="save" id="dealer-location-save">
                        </div>
                    </div>

                </form>
            @else
                <div class="mo-row text-capitalize ">
                    This Location doesn't exist or you don't have access to view Location Details
                </div>
            @endif

    	</div>
    </div>
@endsection

