@extends('layouts.master')

@section('title', 'moto - Main')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-5 col-md-5 no-padding" id="mo-content-header">
            {{ $header or 'Customers' }}
        </div>
        {{--Start - moto Search Section--}}
        <div class="col-xs-12 col-sm-7 col-md-7 no-padding">
            {{ csrf_field() }}
            <div class="col-xs-12 col-sm-10 col-md-10 no-padding mo-search-wrapper">
                <input type="text" placeholder="Search" id="mo-txt-search">
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2 no-padding text-right mo-search-btn-wrapper">
                <input type="submit" class="mo-btn" id="mo-btn-search" value="search">
            </div>
        </div>
        {{--End - moto Search Section--}}
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - moto List Customers Section--}}
    <div class="mo-row padding-top-24 padding-bottom-24 percent-h-100">
        <div class="mo-row mo-form-wrapper percent-h-100">
            <table id="mo-customers-list" class="table" cellspacing="0">
                <thead>
                <tr>
                    {{--<th>First Name <span class="float-right"><img src="{{ asset('images/svg/main/icon-swipe-bottom.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-bottom.png') }}'"></span></th>--}}
                    <th></th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Company</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Location</th>
                    <th>Saleperson</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
                <ul class="pagination">
                    <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                    <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
                </ul>
            </div>
            <div class="mo-round-btn"><a href="{{url('create-customer')}}">NEW</a></div>
        </div>
    </div>
    {{--End - moto List Customers Section--}}
@endsection