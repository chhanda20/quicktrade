@extends('layouts.master-404')

@section('title', 'moto - 404 Page Not Found')

@section('content')
    {{--Start - moto Login Form Section--}}
    <div class="col-xs-12 text-center" id="mo-404-wrapper">
        <div id="mo-404-txt">404</div>
        <div id="mo-not-found-txt" class="text-uppercase">page not found</div>
        <div id="mo-404-desc-txt" class="padding-top-24">Unfortunately we're having trouble loading the page you are looking for. Please wait a moment and try again or use action below.</div>
        <div class="padding-top-40 padding-bottom-32">
            @if (Request::is('mo-admin/*'))
                <a href="{!! url('mo-admin/dealership-list'); !!}" class="btn text-uppercase" id="mo-404-btn"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;take me home</a>
            @else
                <a href="{!! url('home'); !!}" class="btn text-uppercase" id="mo-404-btn"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;take me home</a>
            @endif
        </div>
    </div>
    {{--End - moto Login Form Section--}}
@endsection