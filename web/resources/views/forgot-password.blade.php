@extends('layouts.master-login')

@section('title', 'moto - Forgot password')

@section('content')
    {{--Start - moto Forgot Password Form Section--}}
    <div class="col-xs-12 col-sm-12 col-md-12 text-center padding-top-10">
        <form action="{!! url('forgot-password'); !!}" class="form-horizontal" id="mo-login-form" method="post">
            {{ csrf_field() }}
            {{--Start - Show error message--}}
            <div class="padding-bottom-32">
                <span class="mo-guide-input-email-txt">Enter your email address below and we'll<br/>send you a new password.</span>
            </div>
            {{--End - Show error message--}}
            <div class="input-group">
                <span class="mo-email"><img src="{{ asset('images/svg/login/icon-username.svg') }}" alt="Usr Icon" onerror="this.onerror=null; this.src='{{ asset('images/login/icon-username.png') }}'"/></span>
                <input type="email" id="email" placeholder="Email" name="email" />
            </div>
            @if(isset($error))
            <div class="padding-top-8 block" id="error">
            @else
            <div class="padding-top-8 hidden" id="error">
            @endif
                <span class="mo-login-error-txt">{{isset($error) ? $error : null}}</span>
            </div>
            @if(Session::has("message_success"))
                <div class="padding-top-10 mo-message-success font-white-color">{{session("message_success")}}</div>
                <div class="font-white-color">
                    <a href="{!! url("login") !!}">Comeback to login page</a>
                </div>
            @endif
            <div class="padding-top-40">
                <input type="submit" class="btn btn-lg btn-blockt" id="mo-login-btn" value="RESET PASSWORD" />
            </div>
        </form>
    </div>
    {{--End - moto Forgot Password Form Section--}}
@endsection