@section('header')
    {{--Start - moto Header Section--}}
    <div class="col-xs-12 col-sm-12 col-md-12 mo-main-header no-padding vertical-center" id="mo-dealership-header">
        <div class="col-xs-12 col-sm-12 col-md-2 mo-logo-wrapper-menu text-center no-padding xs-hide">
            <a href="{!! url('mo-admin/dealership-list'); !!}">
                <span class="mo-logo" alt="moto Logo">MotorOne</span>
                {{--<img src="{{ asset('images/svg/logo-mo.svg') }}" class="img-responsive mo-logo" alt="moto Logo" onerror="this.onerror=null; this.src='{{ asset('images/logo-mo.png') }}'"/>--}}
            </a>
        </div>

        <div class="col-xs-4 col-sm-12 col-md-8 no-padding mo-middle-menu">
            <a href="#menu-toggle" id="menu-toggle">
                <img src="{{ asset('images/svg/main/icon-menu.svg') }}" class="mo-icon-menu" alt="Menu Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-menu.png') }}'"/>
            </a>
            <!-- START TO ADD NOTIFICATION -->
            {{--<a href="#" role="button" data-toggle="dropdown" data-target="#" id="mo-badge">
                <img src="{{ asset('images/svg/main/icon-notification.svg') }}" class="mo-icon-notification" alt="moto Logo" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-notification.png') }}'"/>
            </a>

            <span class="dropdown-menu mo-triangle"><img src="{{asset('images/svg/main/triangle-2.svg')}}" ></span>
            <ul class="dropdown-menu mo-notification" role="menu">
                <div id="mo-notification-wrapper">

                </div>
            </ul>--}}
            <!-- END ADD NOTIFICATION -->

            <span class="padding-left-45"></span>
            @if(!isset($breadcrumb_auction))
                {{-- START FILTER --}}
                @include("mo-admin.layouts.auction-filter")
                {{-- END FILTER --}}

                {{-- START SORT --}}
                @include("mo-admin.layouts.auction-sort")
                {{-- END SORT --}}
            @endif

        </div>

        <div class="col-xs-4 sm-hide xs-show mo-logo-wrapper-menu text-center no-padding">
            <a href="{!! url('home'); !!}">
                <img src="{{ asset('images/svg/logo-mo.svg') }}" class="img-responsive mo-logo" alt="moto Logo" onerror="this.onerror=null; this.src='{{ asset('images/logo-mo.png') }}'"/>
            </a>
        </div>

        <div class="col-xs-4 col-sm-12 col-md-2 no-padding mo-user-name-menu">
            <a href="#" role="button" data-toggle="dropdown" data-target="#">
                {{--<span>
                    <img src="{{ asset('images/mo-admin/svg/main/icon_user_profile.svg') }}" class="mo-icon-user-profile" alt="User Profile Icon" onerror="this.onerror=null; this.src='{{ asset('images/mo-admin/main/icon_user_profile.png') }}'"/>
                </span>--}}
                <span class="mo-user-name-txt" style="color: white">
                    @if(Session::has('user_info'))
                        {{ Session::get('user_info')->first_name.' '.Session::get('user_info')->last_name }}
                    @endif
                </span>
                <img src="{{ asset('images/mo-admin/svg/main/icon_user_profile.svg') }}" class="mo-icon-user-profile" alt="User Profile Icon" onerror="this.onerror=null; this.src='{{ asset('images/mo-admin/main/icon_user_profile.png') }}'"/>
            </a>
            <ul class="dropdown-menu float-right mo-option" role="menu">
                <div class="mo-option-wrapper">
                    <a href="#">
                        <div class="mo-option-item">
                            My Profile
                        </div>
                    </a>
                    <hr class="mo-option-hr">
                    <a href="#">
                        <div class="mo-option-item">
                            Settings
                        </div>
                    </a>
                    <hr class="mo-option-hr">
                    <a href="{!! url('mo-admin/sign-out'); !!}">
                        <div class="mo-option-item">
                            Log Out
                        </div>
                    </a>
                </div>
            </ul>

        </div>
    </div>
    {{--End - moto Header Section--}}
@endsection