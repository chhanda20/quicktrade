@section('sidebar')
    {{--Start - moto Sidebar Section--}}
    <div id="sidebar-wrapper">

        <ul id="mo-dealership-sidebar-ul" class="sidebar-nav">
            @if(Session::has('mo_menus_sidebar'))
                @foreach(Session::get('mo_menus_sidebar') as $site_menu)
                    @if($site_menu->site_menu == "mo_admin")
                        @foreach($site_menu->group as $group_menu)
                            <li>
                                <a href="{{isset($group_menu->group_link)?url($group_menu->group_link):"javascript:void(0);"}}" class="text-uppercase mo-highlight" id="{{$group_menu->group_name}}">{{$group_menu->group_name}}</a>
                                @if(isset($group_menu->menu_items))
                                    <ul class="sidebar-submenu">
                                        @foreach($group_menu->menu_items as $menu_item)
                                            <li>
                                                <a href="{!! url($menu_item->link)!!}" class="text-capitalize {{$menu_item->link == ""?"disabled-div":""}}" id="{{$menu_item->code}}">{{$menu_item->name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    @endif
                @endforeach
            @endif
                {{--<li class="">
                    <a href="javascript:void(0);" class="text-uppercase mo-highlight" id="">Valuations</a>
                    <ul class="sidebar-submenu">
                        <li>
                            <a href="{!! url('mo-admin/valuations/requests')!!}" class="text-capitalize" id="">requests</a>
                        </li>
                        <li>
                            <a href="{!! url('mo-admin/valuations/in-progress')!!}" class="text-capitalize" id="">in progress</a>
                        </li>
                        <li>
                            <a href="{!! url('mo-admin/valuations/pending') !!}" class="text-capitalize">pending approval</a>
                        </li>
                        <li>
                            <a href="{!! url('mo-admin/valuations/complete') !!}" class="text-capitalize">complete</a>
                        </li>
                    </ul>
                </li>--}}
            {{--<li>
                <a href="javascript:void(0);" class="text-uppercase mo-highlight" id="menu_members">members</a>
                <ul class="sidebar-submenu">
                    <li>
                        <a href="{!! url('mo-admin/dealer-groups-list')!!}" class="text-capitalize" id="menu_dealer_groups">dealer groups</a>
                    </li>
                    <li>
                        <a href="{!! url('mo-admin/dealership-list')!!}" class="text-capitalize" id="menu_dealerships">dealerships</a>
                    </li>
                    <li>
                        <a href="{!! url('mo-admin/location-list') !!}" class="text-capitalize">location</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="text-uppercase mo-highlight" id="menu_users">users</a>
                <ul class="sidebar-submenu">
                    <li>
                        <a href="javascript:void(0);" class="text-capitalize disabled-div">statistics</a>
                    </li>
                    <li>
                        <a href="{!! url('mo-admin/user/list')!!}" class="text-capitalize" id="menu_manage">manage</a>
                    </li>
                    <li>
                        <a href="{!! url('mo-admin/role-list') !!}" class="text-capitalize ">roles</a>
                    </li>
                    <li>
                        <a href="{!! url('mo-admin/edit-permission')!!}" class="text-capitalize">permissions</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="text-uppercase mo-highlight">settings</a>
                <ul class="sidebar-submenu">
                    <li>
                        <a href="{!! url('mo-admin/general-settings')!!}" class="text-capitalize">valuation system</a>
                    </li>
                    <li>
                        <a href="{!! url('mo-admin/general-settings')!!}" class="text-capitalize">external offering system</a>
                    </li>
                    <li>
                        <a href="{!! url('mo-admin/trading/setting') !!}" class="text-capitalize">auction system</a>
                    </li>
                    <li>
                        <a href="{!! url('mo-admin/transaction/setting-fees') !!}" class="text-capitalize">fees</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="text-uppercase mo-highlight">auction</a>
                <ul class="sidebar-submenu">
                    <li>
                        <a href="{!! url('mo-admin/trading/auction-session-list') !!}" class="text-capitalize">auction sessions</a>
                    </li>
                    <li>
                        <a href="{!! url('mo-admin/trading/request-list')!!}" class="text-capitalize">request list</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="text-capitalize disabled-div">auction listings</a>
                    </li>
                    <li>
                        <a href="{!! url('mo-admin/trading/live-auction') !!}" class="text-capitalize">live auction</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{!! url('mo-admin/dispute/show') !!}" class="text-uppercase mo-highlight">dispute centre</a>
            </li>
            <li>
                <a href="javascript:void(0);" class="text-uppercase mo-highlight">transactions</a>
                <ul class="sidebar-submenu">
                    <li>
                        <a href="{!! url('mo-admin/transaction/view-transactions/current') !!}" class="text-capitalize">current</a>
                    </li>
                    <li>
                        <a href="{!! url('mo-admin/transaction/view-transactions/complete')!!}" class="text-capitalize">complete</a>
                    </li>
                </ul>
            </li>--}}


            {{--<li>
                <a href="{!! url('mo-admin/dealer-groups-list') !!}" class="text-uppercase mo-highlight">dealer group</a>
            </li>
            <li>
                <a href="#" class="text-uppercase mo-highlight">dealership</a>
                <ul class="sidebar-submenu">
                    <li>
                        <a href="{!! url('mo-admin/dealership-list') !!}">Dealership List</a>
                    </li>
                    <li>
                        <a href="{!! url('mo-admin/edit-permission') !!}">Permissions</a>
                    </li>
                </ul>

            </li>
            <li>
                <a href="{!! url('mo-admin/user/list') !!}" class="text-uppercase mo-highlight">user</a>
            </li>
            <li>
                <a href="#" class="text-uppercase mo-highlight">trading</a>
                <ul class="sidebar-submenu">
                    <li>
                        <a href="{!! url('mo-admin/trading/setting') !!}" >Settings</a>
                    </li>
                    <li>
                        <a href="{!! url('mo-admin/auction-session-list') !!}" >Auctions Sessions</a>
                    </li>
                    <li>
                        <a href="{!! url('mo-admin/trading/request-list') !!}">Request List</a>
                    </li>
                    <li>
                        <a href="{!! url('mo-admin/trading/live-auction') !!}">Live Auction</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{!! url('mo-admin/dispute/dispute-info') !!}" class="text-uppercase mo-highlight">dispute centre</a>
            </li>
            <li>
                <a href="{!! url('mo-admin/general-settings') !!}" class="text-uppercase mo-highlight">settings</a>
            </li>--}}
        </ul>
    </div>
    {{--End - moto Sidebar Section--}}
@endsection