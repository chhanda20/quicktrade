@extends('mo-admin.layouts.master')

@section('title', 'moto - Valuations Request')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-8 col-md-8 no-padding" id="mo-content-header">
            {{$header or 'Valuations'}} > <span class="yellow-text">{{ $breadcrumb or 'Requests' }}</span>
        </div>
        {{--Start - moto Search Section--}}
        <div class="col-xs-12 col-sm-4 col-md-4 no-padding">
            {{ csrf_field() }}
            <div class="col-xs-12 no-padding mo-search-wrapper">
                <input type="text" placeholder="Search" id="mo-txt-search">
                <span class="glyphicon glyphicon-search mo-txt-search cursor-pointer"></span>
            </div>
        </div>
        {{--End - moto Search Section--}}
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - moto List Valuations Request Section--}}
    <div class="mo-row padding-top-24 percent-h-100">
        <div class="mo-row mo-form-wrapper percent-h-100">
            <table id="mo-table-valuations-requests-list" class="table" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th class="make">Make</th>
                    <th class="model">Model</th>
                    <th class="year">Year</th>
                    <th class="vin-number">VIN</th>
                    <th class="colour">Colour</th>
                    <th class="registration">Registration</th>
                    <th class="expiry-date">Rego Expiry</th>
                    <th class="sale-person">Saleperson</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
                <ul class="pagination">
                    <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                    <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
                </ul>
            </div>
        </div>
    </div>
    {{--End - moto List Valuations Request Section--}}

    {{--Start - moto Enter VIN Popup Section--}}

    <div id="mo-enter-vin-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form" id="mo-popup-enter-vin">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">Enter VIN</span>
            </div>
            <hr class="mo-form-hr" />

            <div class="mo-row padding-top-24 padding-bottom-24">

                <div class="form-group col-xs-12 col-sm-8 padding-left-9 padding-bottom-10">
                    <div class="col-xs-11 no-padding">
                        <label for="year">Year*</label>
                        <div>
                            <input type="text" class="form-control" name="year" />
                        </div>
                    </div>
                </div>

                <div class="form-group col-xs-12 col-sm-8 padding-left-9 padding-bottom-10">


                    <div class="col-xs-11 no-padding">
                        <label for="vin">VIN*</label>
                        <input type="text" class="form-control" name="vin" maxlength="17" style="text-transform:uppercase"/>
                    </div>
                    <div class="col-xs-1 no-padding padding-top-30 padding-left-5">
                        <img src="{{ asset('/images/main/icon-download.png') }}" alt="Get VIN" class="no-padding padding-top-4 padding-left-5" id="get-vin" />
                    </div>

                </div>

                <div class="form-group col-xs-12 col-sm-8 padding-left-9 padding-bottom-10">
                    <div class="col-xs-11 no-padding">
                        <label for="registration-number">Registration Number*</label>
                        <div>
                            <input type="text" class="form-control text-uppercase" name="registration-number" />
                        </div>
                    </div>
                </div>

                <div class="form-group col-xs-12 padding-top-24">
                    <label for="note">Notes</label>
                    <hr class="mo-form-hr force-no-margin" />
                    <div class="padding-top-20 note-content text-uppercase">
                        {{--013 MASERATI QUATTROPORTE SPORT GT-S MC SPORTLINE MY13 4D SEDAN V8 4691 CC MPFI 6 SP AUTOMATIC WHITE--}}
                    </div>
                </div>

                <div class="form-group col-xs-12 padding-top-24">
                    <div class="col-xs-12 no-padding padding-bottom-8">
                        <div class="col-xs-3 no-padding vin">VIN:</div>
                        <div class="col-xs-9 no-padding vin vin-number">{{--JT152EEA100302159--}}</div>
                    </div>
                    <hr class="mo-form-hr force-no-margin clear-both" />
                </div>

            </div>

            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="vin-confirm" disabled>
                </div>
            </div>
            <div class="mo-loading-image hidden">Loading...</div>
        </form>
    </div>

    {{--End - moto Enter VIN Popup Section--}}

    {{--start - SENSITIVE ACTION CONFIRMATION POPUP--}}
    <div id="mo-sensitive-action-confirm-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            {{ csrf_field() }}

            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">delete valuation</span>
            </div>

            <hr class="mo-form-hr" />

            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <span class="suspend-popup-content">Are you sure you want to delete this item?</span>
                </div>
            </div>

            <hr class="mo-form-hr" />

            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-confirm-btn">
                </div>
            </div>

        </form>
    </div>
    {{--End - SENSITIVE ACTION CONFIRMATION POPUP--}}

    <!-- AJAX - loading -->
    <div class="mo-loading-image hidden">Loading&#8230;</div>
    <!-- AJAX - end loading -->

@endsection