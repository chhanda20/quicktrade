@extends('mo-admin.layouts.master')

@section('title', 'moto - Valuations Pending')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-8 col-md-8 no-padding" id="mo-content-header">
            {{$header or 'Valuations'}} > <span class="yellow-text">{{ $breadcrumb or 'Pending' }}</span>
        </div>
        {{--Start - moto Search Section--}}
        <div class="col-xs-12 col-sm-4 col-md-4 no-padding">
            {{ csrf_field() }}
            <div class="col-xs-12 no-padding mo-search-wrapper">
                <input type="text" placeholder="Search" id="mo-txt-search">
                <span class="glyphicon glyphicon-search mo-txt-search cursor-pointer"></span>
            </div>
        </div>
        {{--End - moto Search Section--}}
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - moto List Valuations Request Section--}}
    <div class="mo-row padding-top-24 percent-h-100">
        <div class="mo-row mo-form-wrapper percent-h-100">
            <table id="mo-table-valuations-pending-list" class="table" cellspacing="0">
                <thead>
                <tr>
                    <th>Make</th>
                    <th>Model</th>
                    <th>Year</th>
                    <th>Registration</th>
                    <th>Colour</th>
                    <th>Condition</th>
                    <th>Valuer</th>
                    <th>Sales</th>
                    <th>Negotiating Price</th>
                    <th>Maximum Price</th>
                    <th>Estimated Total Reco. Cost</th>
                    <th class="mo-requested-price">Requested Approv. Price</th>
                    <th>Status</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
                <ul class="pagination">
                    <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                    <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
                </ul>
            </div>
        </div>
    </div>
    {{--End - moto List Valuations Request Section--}}

    {{--start - SENSITIVE ACTION CONFIRMATION POPUP--}}
    <div id="mo-sensitive-action-confirm-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            {{ csrf_field() }}

            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">valuation approval</span>
            </div>

            <hr class="mo-form-hr" />

            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <span class="suspend-popup-content">Are you sure you want to approve this item?</span>
                </div>
            </div>

            <hr class="mo-form-hr" />

            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-valuation-pending-approval-confirmation-btn">
                </div>
            </div>

        </form>
    </div>
    {{--End - SENSITIVE ACTION CONFIRMATION POPUP--}}

    <!-- AJAX - loading -->
    <div class="mo-loading-image hidden">Loading&#8230;</div>
    <!-- AJAX - end loading -->

@endsection