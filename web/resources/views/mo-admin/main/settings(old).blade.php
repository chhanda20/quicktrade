@extends('mo-admin.layouts.master')

@section('title', 'moto - Main')

@section('content')
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            {{ $header or 'Settings'}}
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding ">
            <form action="" method="post" id="mo-settings">
                <div class="col-xs-12 col-sm-12 col-md-12 padding-bottom-24 text-uppercase mo-form-header">
                    valuation
                </div>
                {{ csrf_field() }}
                <div class="col-xs-12 col-sm-6 alert alert-success hidden" role="alert">
                    You've successfully set setting for Valuation!
                    <a class="mo-close-message close">&times;</a>
                    {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>--}}
                </div>
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="first-notification-timeout" class="text-capitalize">First Notification Timeout (minutes)</label>
                        <input type="number" class="form-control" name="first_notification_timeout" id="first-notification-timeout"
                               value="{{isset($valuationSettings->first_notification_timeout)?$valuationSettings->first_notification_timeout:''}}">
                    </div>
                </div> <!-- Row 1 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="second-notification-timeout" class="text-capitalize">Second Notification Timeout (minutes)</label>
                        <input type="number" class="form-control" name="second_notification_timeout" id="second-notification-timeout"
                               value="{{isset($valuationSettings->second_notification_timeout)?$valuationSettings->second_notification_timeout:''}}">
                    </div>
                </div> <!-- Row 2 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="valuation-timeout" class="text-capitalize">Valuation Timeout (minutes)</label>
                        <input type="number" class="form-control" name="valuation_timeout" id="valuation-timeout"
                               value="{{isset($valuationSettings->valuation_timeout)?$valuationSettings->valuation_timeout:''}}">
                    </div>
                </div> <!-- Row 3 -->

                <div class="col-xs-12 col-sm-12 col-md-12 padding-bottom-24 text-uppercase mo-form-header">
                    external offer
                </div>
                <div class="col-xs-12 col-sm-6 alert alert-success hidden" role="alert">
                    You've successfully set setting for External Offer!
                    <a class="mo-close-message close">&times;</a>
                    {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>--}}
                </div>
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="external-offer-timeout" class="text-capitalize">external offer timeout (minutes)</label>
                        <input type="number" class="form-control" name="external_offer_timeout" id="external-offer-timeout"
                               value="{{isset($externalOfferSettings->external_offer_timeout)?$externalOfferSettings->external_offer_timeout:''}}">
                    </div>
                </div> <!-- Row 1 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="external-offer-expiry-period" class="text-capitalize">external offer expiry period (days)</label>
                        <input type="number" class="form-control" name="external_offer_expiry_period" id="external-offer-expiry-period"
                               value="{{isset($externalOfferSettings->external_offer_expiry)?$externalOfferSettings->external_offer_expiry:''}}">
                    </div>
                </div> <!-- Row 2 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="external-offer-acceptance-timeout" class="text-capitalize">external offer acceptance timeout (minutes)</label>
                        <input type="number" class="form-control" name="external_offer_acceptance_timeout" id="external-offer-acceptance-timeout"
                               value="{{isset($externalOfferSettings->external_offer_acceptance_timeout)?$externalOfferSettings->external_offer_acceptance_timeout:''}}">
                    </div>
                </div> <!-- Row 3 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-30">
                        <input type="submit" class="mo-btn text-uppercase" value="submit" id="mo-submit-setting">
                    </div>
                </div>
                <div class="mo-loading-image hidden">Loading&#8230;</div>
            </form>
        </div>
    </div>
@endsection