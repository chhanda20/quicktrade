@extends('mo-admin.layouts.master')

@section('title', 'moto - Setting Rating')

@section('content')

    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            {{ $header or 'Settings'}} > <span class="yellow-text">{{ $breadcrumb or 'Rating'}}</span>
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
            <form action="" method="post" id="mo-trading-setting-form">
                {{csrf_field()}}
                <div class="col-xs-12 padding-bottom-24 text-uppercase mo-form-header">
                    rating
                </div>
                <div class="col-xs-12 col-sm-6 alert alert-success hidden" role="alert">
                    You've successfully set rating setting!
                    <a class="mo-close-message close">&times;</a>
                </div>

                <div class="col-xs-12 col-sm-6 alert alert-danger hidden" role="alert">
                    <span class="error-text"></span>
                    <a class="mo-close-message close">&times;</a>
                </div>

                <div class="mo-row padding-bottom-24">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <label for="rating-duration" class="text-capitalize">rating duration (days)</label>
                        <input type="number" class="form-control" id="rating-duration" name="rating_duration"
                               value="{{isset($ratingSettings->rating_expired_time)?$ratingSettings->rating_expired_time:''}}">
                    </div>
                </div>
                <div class="mo-row padding-bottom-24">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <label for="speedy-seller" class="text-capitalize">speedy seller average time (hours)</label>
                        <input type="number" class="form-control" id="speedy-seller" name="speedy_seller"
                               value="{{isset($ratingSettings->speedy_seller_avg_time)?$ratingSettings->speedy_seller_avg_time:''}}">
                    </div>
                </div>
                <div class="mo-row padding-bottom-24">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <label for="speedy-payer" class="text-capitalize">speedy payer average time (hours)</label>
                        <input type="number" class="form-control" id="speedy-payer" name="speedy_payer"
                               value="{{isset($ratingSettings->speedy_buyer_avg_time)?$ratingSettings->speedy_buyer_avg_time:''}}">
                    </div>
                </div>
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <input type="submit" class="mo-btn text-uppercase" value="save" id="mo-submit-setting">
                        <input type="hidden" id="settings-type" value="rating">
                    </div>
                </div>
                <div class="mo-loading-image hidden">Loading&#8230;</div>
            </form>
        </div>
    </div>

@endsection