@extends('mo-admin.layouts.master')

@section('title', 'moto - Settings External Offering')

@section('content')
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            {{ $header or 'Settings'}} > <span class="yellow-text">{{ $breadcrumb or 'External Offering' }}</span>
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding ">
            <form action="" method="post" id="mo-settings">
                <div class="col-xs-12 col-sm-12 col-md-12 padding-bottom-24 text-uppercase mo-form-header">
                    external offer
                </div>
                <div class="col-xs-12 col-sm-6 alert alert-success hidden" role="alert">
                    You've successfully set setting for External Offer!
                    <a class="mo-close-message close">&times;</a>
                </div>
                <div class="col-xs-12 col-sm-6 alert alert-danger hidden" role="alert">
                    <span class="error-text"></span>
                    <a class="mo-close-message close">&times;</a>
                </div>
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="external-offer-timeout" class="text-capitalize">external offer timeout (minutes)</label>
                        <input type="number" class="form-control" name="external_offer_timeout" id="external-offer-timeout"
                               value="{{isset($externalOfferSettings->external_offer_timeout)?$externalOfferSettings->external_offer_timeout:''}}">
                    </div>
                </div> <!-- Row 1 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="external-offer-expiry-period" class="text-capitalize">external offer expiry period (days)</label>
                        <input type="number" class="form-control" name="external_offer_expiry_period" id="external-offer-expiry-period"
                               value="{{isset($externalOfferSettings->external_offer_expiry)?$externalOfferSettings->external_offer_expiry:''}}">
                    </div>
                </div> <!-- Row 2 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="external-offer-acceptance-timeout" class="text-capitalize">external offer acceptance timeout (minutes)</label>
                        <input type="number" class="form-control" name="external_offer_acceptance_timeout" id="external-offer-acceptance-timeout"
                               value="{{isset($externalOfferSettings->external_offer_acceptance_timeout)?$externalOfferSettings->external_offer_acceptance_timeout:''}}">
                    </div>
                </div> <!-- Row 3 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-30">
                        <input type="submit" class="mo-btn text-uppercase" value="save" id="mo-submit-setting">
                        <input type="hidden" id="settings-type" value="external_offering">
                    </div>
                </div>
                <div class="mo-loading-image hidden">Loading&#8230;</div>
            </form>
        </div>
    </div>
@endsection