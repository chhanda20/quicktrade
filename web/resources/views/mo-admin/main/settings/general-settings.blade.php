@extends("mo-admin.layouts.master")

@section("title", "moto - Setting Auction")

@section("content")
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            {{$header or "Settings"}} > <span class="yellow-text">{{ $breadcrumb or 'General' }}</span>
        </div>
    </div>
    {{--Start - moto Create Form Section--}}
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                general
            </div>
            <form action="" method="post" id="mo-settings-term-condition-form">
                {{csrf_field()}}
                <div class="col-xs-12 col-sm-6 alert alert-success hidden" role="alert">
                    You've successfully update term & condition for general setting!
                    <a class="mo-close-message close">&times;</a>
                </div>
                <div class="mo-row">
                    <label for="" class="text-capitalize">user terms & conditions</label>
                </div>
                <div class="mo-row no-padding padding-top-24 padding-bottom-24">
                    <textarea class="mo-textarea form-control" id="settings-term-condition" name="setting_term_condition" rows="20">{{$term_and_condition->term_and_condition}}</textarea>
                </div>
                <div class="mo-row text-center">
                    <input type="button" class="mo-btn text-uppercase" id="mo-save-term-condition" value="save">
                </div>
                <div class="mo-loading-image hidden">Loading...</div>
            </form>
        </div>
    </div>
@endsection