@extends('mo-admin.layouts.master')

@section('title', 'moto - Settings Valuation')

@section('content')
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            {{ $header or 'Settings'}} > <span class="yellow-text">{{ $breadcrumb or 'Valuation' }}</span>
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding ">
            <form action="" method="post" id="mo-settings">
                <div class="col-xs-12 col-sm-12 col-md-12 padding-bottom-24 text-uppercase mo-form-header">
                    valuation
                </div>
                {{ csrf_field() }}
                <div class="col-xs-12 col-sm-6 alert alert-success hidden" role="alert">
                    You've successfully set setting for Valuation!
                    <a class="mo-close-message close">&times;</a>
                </div>
                <div class="col-xs-12 col-sm-6 alert alert-danger hidden" role="alert">
                    <span class="error-text"></span>
                    <a class="mo-close-message close">&times;</a>
                </div>
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="first-notification-timeout" class="text-capitalize">First Notification Timeout (minutes)</label>
                        <input type="number" class="form-control" name="first_notification_timeout" id="first-notification-timeout"
                               value="{{isset($valuationSettings->first_notification_timeout)?$valuationSettings->first_notification_timeout:''}}">
                    </div>
                </div> <!-- Row 1 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="second-notification-timeout" class="text-capitalize">Second Notification Timeout (minutes)</label>
                        <input type="number" class="form-control" name="second_notification_timeout" id="second-notification-timeout"
                               value="{{isset($valuationSettings->second_notification_timeout)?$valuationSettings->second_notification_timeout:''}}">
                    </div>
                </div> <!-- Row 2 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="valuation-timeout" class="text-capitalize">Valuation Timeout (minutes)</label>
                        <input type="number" class="form-control" name="valuation_timeout" id="valuation-timeout"
                               value="{{isset($valuationSettings->valuation_timeout)?$valuationSettings->valuation_timeout:''}}">
                    </div>
                </div> <!-- Row 3 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-30">
                        <input type="submit" class="mo-btn text-uppercase" value="save" id="mo-submit-setting">
                        <input type="hidden" id="settings-type" value="valuation">
                    </div>
                </div>
                <div class="mo-loading-image hidden">Loading&#8230;</div>
            </form>
        </div>
    </div>
@endsection