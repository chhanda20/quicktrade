@extends('mo-admin.layouts.master')

@section('title', 'moto - Main')

@section('content')

    <div class="mo-row">
        <div class="col-xs-12 col-sm-8 no-padding" id="mo-content-header">
            <a href="/mo-admin/role-list">{{ $header or "Roles" }}</a>
        </div>
        <div class="col-xs-12 col-sm-4 no-padding">
            {{ csrf_field() }}
            <div class="col-xs-12 no-padding mo-search-wrapper">
                <input type="text" placeholder="Search" id="mo-txt-search" name="mo_txt_search">
                <span class="glyphicon glyphicon-search mo-txt-search cursor-pointer"></span>
            </div>
        </div>
    </div>
    <div class="mo-row no-padding percent-h-100">
        <div class="mo-row mo-form-wrapper percent-h-100">
            <table id="mo-role-list" class="table" cellspacing="0">
                <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Default</th>
                    <th></th>
                </tr>
                </thead>
            </table>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
                <ul class="pagination">
                    <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                    <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
                </ul>
            </div>
            <div class="mo-round-btn"><a href="/mo-admin/create-role/new">NEW</a></div>
        </div>
    </div>

    {{------------ DELETE CONFIRMATION POPUP ------------}}
    <div id="mo-delete-confirm-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">delete role</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <span class="suspend-popup-content">Are you sure you want to delete this role?</span>
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-confirm-delete">
                </div>
            </div>
        </form>
    </div>
    {{--End - moto Popup Section--}}
@endsection