@extends("mo-admin.layouts.master")

@section('title', 'moto - Main')

@section('content')
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <a href="/mo-admin/role-list"> {{ $header or 'Roles'}} </a> > {{ $breadcrumb or 'New'}}
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
            <div class="col-xs-12 col-sm-12 col-md-12 padding-bottom-24 text-uppercase mo-form-header">
                roles details
            </div>
            @if(isset($role_detail))
            <form action="{!! url('/mo-admin/update-role') !!}/{{$role_detail->id}}" method="post" id="mo-add-role-form">
            @else
            <form action="{!! url('/mo-admin/create-role') !!}" method="post" id="mo-add-role-form">
            @endif
                {{csrf_field()}}

                <div class="col-xs-12 col-sm-6 alert alert-success hidden" role="alert">
                    {{--<span class="mo-error-message text-capitalize"></span>--}}
                    Update Success!
                    <a class="mo-close-message close">&times;</a>
                </div>

                @if(Session::has("error_message"))
                    <div class="col-xs-12 col-sm-6 alert alert-danger"><span></span> {!! session('error_message') !!}
                        <a class="mo-close-message close">&times;</a>
                    </div>
                @endif

                <div class="mo-row padding-bottom-24">
                    <div class="col-xs-12 col-sm-12 col-md-6">

                        <input type="checkbox" class="mo-round-check" id="mo-role-default" name="mo_role_default"
                            {{isset($role_detail->is_default) && $role_detail->is_default == 1  ? "checked" : " "}}>
                        <label for="mo-role-default" class="text-capitalize"><span></span>default</label>
                    </div>
                </div>

                <div class="mo-row padding-bottom-24">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <label for="role-name" class="text-capitalize">name</label>
                        <input type="text" class="form-control" id="role-name" name="role_name" value="{{isset($role_detail->name) ? $role_detail->name : ""}}">
                    </div>
                </div>

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-12 col-md-12 mo-category-list">
                        <div><label for="permissions" class="text-capitalize padding-bottom-10">permissions</label></div>

                            @if(isset($permission_list))
                                @foreach($category_type as $category)
                                    @if(isset($category_group))
                                        @foreach($category_group as $categories)
                                            @if($categories->code == $category)
                                                <div class="mo-row padding-bottom-10">
                                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                                        {{--<input type="checkbox" class="mo-square-check mo-role-category" data-id="{{$categories->code}}" id="category-{{$categories->code}}" name="category_permission_list[]"/>--}}
                                                        {{--<label for="category-{{$categories->code}}" class="text-capitalize mo-category">{{$categories->value}}<span></span></label>--}}
                                                        <label for="" class="text-capitalize">{{$categories->value}}</label>
                                                        <span class="glyphicon glyphicon-triangle-bottom  mo-role-category cursor-pointer float-right"  data-id="{{$categories->code}}" id="category-{{$categories->code}}" name="category_permission_list[]"></span>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                        <div class="mo-row padding-bottom-24 col-xs-12 col-sm-12 col-md-12 mo-role-list-item hidden" data-category-id="{{$category}}">
                                            {{--<div class="col-xs-12 col-sm-12 col-md-12 mo-category-list">--}}
                                            <?php $count = 0; ?>
                                                @foreach($permission_list as $permission)
                                                    @if($permission->category == $category)
                                                        <div class="col-xs-12 col-sm-12 col-md-4 {{$count % 3 === 0?"":"border-grey-left"}}">
                                                            <input type="checkbox" class="mo-square-check mo-permission" id="item-{{$permission->id}}" name="role_permission_list[]"
                                                                   value="{{$permission->id}}" {{isset($permission_detail) && in_array((int)$permission->id, $permission_detail) ? "checked" : ''}}/>
                                                            <label for="item-{{$permission->id}}" class="text-capitalize mo-category"><span></span>{{$permission->name}}</label>
                                                        </div>
                                                    <?php $count++; ?>
                                                    @endif

                                                @endforeach

                                            {{--</div>--}}
                                        </div>
                                @endforeach
                            @endif
                        {{--</div>--}}

                    </div>
                    <div id="errorToShow"></div>

                </div>

                <!-- Show Menu Role -->
                <div class="col-xs-12 col-sm-12 col-md-12 padding-top-24 text-uppercase mo-form-header">
                    menu access
                </div>
                {{--<div class="mo-row padding-top-10 mo-category-list">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        @if(isset($menu_group))
                            @foreach($menu_group as $menu)
                                <div class="mo-row padding-top-24"><label for="" class="text-uppercase">{{$menu["group_name"]}}</label></div>
                                @foreach($menu["menu_items"] as $menu_item)
                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                        <input type="checkbox" class="mo-square-check" id="menu_item_{{$menu_item->code}}" name="menu_items_list[]" value="{{$menu_item->id}}"
                                            {{isset($menu_role_detail) && in_array($menu_item->id, $menu_role_detail) ? "checked" : ""}}>
                                        <label for="menu_item_{{$menu_item->code}}" class="text-capitalize mo-category"><span></span>{{$menu_item->name}}</label>
                                    </div>
                                @endforeach
                            @endforeach
                        @endif
                    </div>
                </div>--}}

                <div class="mo-row padding-top-24">
                    <div class="col-xs-12 col-sm-12 col-md-12 mo-category-list">
                        @if(isset($menu_items_list))
                            @foreach($site_menu as $menu)
                                <div class="mo-row padding-bottom-10">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        @if($menu == "mo_admin")
                                            <label for="" class="text-capitalize">MotorOne Admin</label>
                                        @else
                                        <label for="" class="text-capitalize">{{$menu}}</label>
                                        @endif
                                        <span class="glyphicon glyphicon-triangle-bottom  mo-menu-category cursor-pointer float-right " id="{{$menu}}"></span>
                                    </div>
                                </div>

                                <div class="mo-row padding-bottom-24 col-xs-12 col-sm-12 col-md-12 mo-menu-list-item hidden" data-id="{{$menu}}">
                                    @foreach($menu_items_list as $menu_list)
                                        @if($menu_list->site_menu == $menu)
                                            @foreach($menu_list->group as $group)
                                                <div class="mo-row padding-top-10 padding-lr5"><label for="" class="text-uppercase ">{{$group->group_name}}</label></div>
                                                @foreach($group->menu_items as $item)
                                                    <div class="col-xs-12 col-sm-12 col-md-4">
                                                        <input type="checkbox" class="mo-square-check mo-menu-item" id="menu-{{$item->id}}" name="menu_items_list[]" value="{{$item->id}}"
                                                            {{isset($menu_role_detail) && in_array($item->id, $menu_role_detail) ? "checked" : ""}}/>
                                                        <label for="menu-{{$item->id}}" class="text-capitalize mo-category"><span></span>{{$item->name}}</label>
                                                    </div>
                                                @endforeach
                                            @endforeach
                                        @endif
                                    @endforeach
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div id="error_To_Show"></div>
                </div>
                <!-- End -->

                <div><input type="hidden" id="mo-role-id" name="mo_role_id" value="{{isset($role_detail->id)?$role_detail->id:''}}"></div>

                <div class="mo-row padding-top-24 padding-left-10">
                    @if(strtolower($breadcrumb)=="new")
                    <input type="submit" class="mo-btn text-uppercase" id="mo-save-role" value="save">
                    @elseif(strtolower($breadcrumb)=="edit")
                    <input type="submit" class="mo-btn text-uppercase" id="mo-update-role" value="update">
                    @endif
                </div>
                <div class="mo-loading-image hidden">Loading...</div>
            </form>
        </div>
    </div>

@endsection