@extends('mo-admin.layouts.master')

@section('title', 'moto - Locations List')

@section('content')
    <!-- Start - moto Content Header Section -->
    <div class="mo-row">
        <div class="col-xs-12 col-sm-8 no-padding" id="mo-content-header">
            <a href="">{{ $header or "Location" }}</a>
        </div>
        <div class="col-xs-12 col-sm-4 no-padding">
            {{ csrf_field() }}
            <div class="col-xs-12 no-padding mo-search-wrapper">
                <input type="text" placeholder="Search" id="mo-txt-search" name="mo_txt_search">
                <span class="glyphicon glyphicon-search mo-txt-search cursor-pointer"></span>
            </div>
        </div>
    </div>
    <!-- End - moto Content Header Section -->


    <div class="mo-row padding-top-24 percent-h-100">
        <div class="mo-row mo-form-wrapper percent-h-100">
            <table id="mo-location-list" class="table" cellspacing="0">
                <thead>
                <tr>
                    <th></th>
                    <th>Dealergroup</th>
                    <th>Dealership</th>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
            </table>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
                <ul class="pagination">
                    <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                    <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
                </ul>
            </div>
            <div class="mo-round-btn"><a href="/mo-admin/init-location-form/new">NEW</a></div>
        </div>
    </div>

@endsection