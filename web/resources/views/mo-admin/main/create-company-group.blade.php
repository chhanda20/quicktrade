@extends('mo-admin.layouts.master')

@section('title', 'moto - Main')

@section('content')
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <a href="{!! url('mo-admin/dealer-groups-list'); !!}">{{ $header or 'Dealer Group'}}</a> > {{ $breadcrumb or 'New'}}
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding ">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                dealer group details
            </div>
            <form action="{!! url('mo-admin/create-dealer-group') !!}" method="post" id="mo-create-company-group-form">
                {{ csrf_field() }}
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="name" class="text-capitalize">name*</label>
                        <input type="text" class="form-control" name="dealer_group_name" id="dealer-group-name" value="{{isset($company_group_detail->name)?$company_group_detail->name:''}}">
                    </div>
                </div> <!-- Row 1 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="description" class="text-capitalize">description</label>
                        <input type="text" class="form-control" name="dealer_group_description" id="dealer-group-description" value="{{isset($company_group_detail->description)?$company_group_detail->description:''}}">
                    </div>
                </div> <!-- Row 2 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="status" class="text-capitalize">status</label>
                        <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}"></span>
                        <select name="dealer_group_status" id="dealer-group-status" class="text-capitalize form-control">
                            @if(isset($status_code))
                                @if(isset($company_group_detail))
                                    @foreach($status_code as $status)
                                        <option {{isset($company_group_detail->status_code) && $company_group_detail->status_code == $status->code ?'selected':''}} value="{{$status->code}}">{{$status->value}}</option>
                                        {{--<option value="{{$status->code}}">{{$status->value}}</option>--}}
                                    @endforeach    
                                @else
                                    @foreach($status_code as $status)
                                        <option {{$status->code == 2 ?'selected':''}} value="{{$status->code}}">{{$status->value}}</option>
                                        {{--<option value="{{$status->code}}">{{$status->value}}</option>--}}
                                    @endforeach
                                @endif
                                
                            @endif
                        </select>
                    </div>
                </div> <!-- Row 3 -->
                <input type="hidden" id="mo-company-group-id" value="{{isset($company_group_detail->id)?$company_group_detail->id:''}}">

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-30">
                        @if(strtolower($breadcrumb)=="new")
                        <input type="submit" class="mo-btn text-uppercase" value="create" id="dealer-group-create">
                        @elseif(strtolower($breadcrumb)=="edit")
                        <input type="submit" class="mo-btn text-uppercase" value="update" id="dealer-group-update">
                        @endif
                    </div>
                </div>
                <div class="mo-loading-image hidden">Loading...</div>
            </form>
        </div>
    </div>
@endsection