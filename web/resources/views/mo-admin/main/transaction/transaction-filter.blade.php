{{-- START FILTER --}}
<span class="padding-left-20 mo-dropdown-menu-wrapper" id="mo-filter-wrapper">
    {{ csrf_field() }}
    <a href="#" role="button" data-toggle="dropdown" data-target="#" id="mo-badge">
        <span class="text-uppercase font-family-bold" id="mo-transaction-filter-header">filter</span>
        <span class="glyphicon glyphicon-menu-down"></span>
    </a>
    <div class="dropdown-menu no-padding" id="mo-transaction-filter">
        <div class="dropdown-item dropdown-header text-uppercase text-center font-family-bold">filter</div>
        <div class="dropdown-divider"></div>
        <div class="dropdown-item">
            {{--LEVEL 1--}}
            <div class="dropdown-submenu cursor-pointer" id="mo-transaction-make-filter">
                <span class="col-xs-4">Make/Model</span>
                <div class="col-xs-6 no-padding text-right">
                    <span class="slide-txt-display hidden mo-filter" id="transaction-make-model-filter"></span>
                </div>
                <span class="col-xs-2 text-right glyphicon glyphicon-menu-right"></span>
                {{--LEVEL 2--}}
                <div id="mo-make-model-name" class="hidden">
                    <div class="dropdown-item">
                        <span class="mo-make-model-txt col-xs-12 text-center">Make/Model</span>
                    </div>
                    <div class="dropdown-divider"></div>
                </div>
                <div id="mo-2nd-level-clone" class="dropdown-menu no-padding hidden">
                    <div class="menu-item dropdown dropdown-submenu">
                        <div class="dropdown-item">
                            <span class="col-xs-10">
                                <input type="checkbox" class="mo-square-check" id="abc" value=""/>
                                <label for="abc" class="text-capitalize disabled"><span class=""></span></label>
                                <span class="mo-make-txt padding-left-5">ABC</span>
                            </span>
                            <span class="col-xs-2 padding-top-4 text-right glyphicon glyphicon-menu-right"></span>
                        </div>
                        <div class="dropdown-divider"></div>
                        {{--LEVEL 3--}}
                        <div class="dropdown-menu scrollable-menu no-padding hidden" id="mo-3rd-level">
                            <div class="menu-item dropdown dropdown-submenu">
                                <div class="dropdown-item">
                                    <span class="col-xs-12">
                                        <input type="checkbox" class="mo-square-check" id="abcs" value=""/>
                                        <label for="abcs" class="text-capitalize disabled"><span class=""></span></label>
                                        <span class="mo-model-txt padding-left-5">ABC</span>
                                    </span>
                                </div>
                                <div class="dropdown-divider"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="dropdown-menu transaction-dropdown-menu padding-bottom-0">
                    <div id="mo-2nd-level">
                        {{--<div class="menu-item dropdown dropdown-submenu"></div>--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="dropdown-divider"></div>
        <div class="dropdown-item cursor-pointer disabled-div">
            <div class="dropdown-submenu" id="mo-transaction-variant-filter">
                <span class="col-xs-4">Variant</span>
                <div class="col-xs-6 no-padding text-right">
                    <span class="slide-txt-display hidden mo-filter" id="transaction-variant-filter"></span>
                </div>
                <span class="col-xs-2 text-right glyphicon glyphicon-menu-right"></span>
                <div class="dropdown-menu transaction-dropdown-menu padding-bottom-0">
                    <div id="mo-2nd-level">
                        {{--<div class="menu-item dropdown dropdown-submenu"></div>--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="dropdown-divider"></div>
        <div class="dropdown-item cursor-pointer">
            <div class="dropdown-submenu" id="mo-transaction-statuses-filter">
                <span class="col-xs-6">Transaction Statuses</span>
                <div class="col-xs-4 no-padding text-right">
                    <span class="slide-txt-display hidden mo-filter" id="transaction-statuses-filter"></span>
                </div>
                <span class="col-xs-2 text-right glyphicon glyphicon-menu-right"></span>
                <div class="dropdown-menu transaction-dropdown-menu padding-bottom-0">
                    <div id="mo-2nd-level">
                        {{--<div class="menu-item dropdown dropdown-submenu"></div>--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="dropdown-divider"></div>
        <div class="dropdown-item cursor-pointer">
            <div class="dropdown-submenu">
                <span class="col-xs-6">Year Range</span>
                <div class="col-xs-4 no-padding text-right">
                    <span class="slide-txt-display hidden mo-filter" id="transaction-year-filter"></span>
                </div>
                <span class="col-xs-2 text-right glyphicon glyphicon-menu-right"></span>
                <div class="dropdown-menu padding-bottom-0 slider-range" id="transaction-slider-range">
                    <div class="dropdown-item">
                        <span class="col-xs-12">
                            <p class="mo-row">
                                <label for="transaction-year-amount" class="col-xs-12 col-sm-5 padding-lr5 text-right">Year Range:</label>
                                <input type="text" id="transaction-year-amount" class="slide-txt-display col-xs-12 col-sm-7 padding-lr5" readonly>
                                <input type="hidden" id="transaction-year-range-from">
                                <input type="hidden" id="transaction-year-range-to">
                            </p>
                            <div id="transaction-year-slider-range"></div>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="dropdown-divider"></div>
        <div class="dropdown-item dropdown-buttons">
            <input type="button" class="btn col-xs-6 text-uppercase font-weight-bold" value="reset" id="mo-btn-transaction-reset-filter">
            <input type="button" class="btn col-xs-6 text-uppercase font-weight-bold" value="apply" id="mo-btn-transaction-apply-filter">
        </div>
    </div>
</span>
{{-- END FILTER --}}