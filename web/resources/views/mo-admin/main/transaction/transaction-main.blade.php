@extends("mo-admin.layouts.master")

@section('title', 'moto - Transaction')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row padding-bottom-24">
        <div class="col-xs-12 col-sm-7 no-padding" id="mo-content-header">
            {{ $header or 'Transaction' }} > <span class="text-capitalize">{{ $breadcrumb or 'Current' }}</span>
        </div>
    </div>
    {{--End - moto Content Header Section--}}

    <div class="mo-row mo-form-wrapper">
        <div class="mo-row padding-bottom-24">
            <input type="hidden" value="{{$type or ''}}" id="type">
            {{ csrf_field() }}
            @if($type == "current")
                <div class="col-xs-12 no-padding mo-5-blocks padding-top-24 vertical-center" id="mo-blocks-wrapper">
                    @foreach ($tabs as $tab)
                        <div class="col-xs-12 col-sm-2 padding-lr5">
                            <a class="tran-tab" id="{{$tab['id']}}-link" tab="{{$tab['id']}}" href="#{{$tab['id']}}">
                                <div class="col-xs-12 mo-block" id="{{$tab['id']}}_tab">
                                    <span class="col-xs-12 mo-block-txt text-capitalize no-padding text-center">{{$tab['name']}}</span>
                                    <span class="col-xs-12 text-center mo-arrow-focus">
                                <img src="{{ asset('images/svg/main/triangle.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/triangle.png') }}'">
                            </span>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @endif
            <div class="col-xs-12 col-sm-12 padding-lr5 padding-top-24">
                <div class="col-xs-12 col-sm-8 no-padding">
                    @include("mo-admin.main.transaction.transaction-filter")
                </div>
                <div class="col-xs-12 col-sm-4 no-padding">
                    <input type="checkbox" class="mo-round-check" id="required-action" name="" value="">
                    @if($type == "current")
                        <label for="required-action" class="text-capitalize"><span></span>Show Required Actions Only</label>
                    @endif
                </div>
            </div>
        </div>

        <div class="mo-row" id="mo-transaction-grid-wrapper">
            @if($type == "current")
                @foreach ($tabs as $tab)
                    <div class="content-block" id="{{ $tab['id'] }}-block">
                        @include("mo-admin.main.transaction.transaction-data-table")
                    </div>
                @endforeach
            @else
                <?php $tab['id'] = "all"; ?>
                @include("mo-admin.main.transaction.transaction-data-table")
            @endif
        </div>
        <div class="mo-loading-image hidden">Loading&#8230;</div>
    </div>
@endsection