@extends("mo-admin.layouts.master")

@section('title', 'moto - Main')

@section('content')
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            Transactions > <a href="/mo-admin/transaction-current-info"> {{ $header or 'Current'}} </a>
        </div>
    </div>
    <div class=" mo-row mo-form-wrapper" id="mo-block-transaction-current" >
        <div class="col-xs-12" id="mo-blocks-wrapper">
            <div class="col-xs-12 col-sm-3 padding-lr5 tab" id="mo-transaction-tab-all" tab="mo-transaction-list-wrapper">
                <div class="col-xs-12 mo-block mo-offer-blocks" id="mo-transaction-all">
                    <span class="col-xs-12 no-padding mo-block-txt text-capitalize padding-top-8"  data-label="All">all</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div> <!-- Tab All -->

            <div class="col-xs-12 col-sm-3 padding-lr5 tab" id="mo-transaction-tab-awaiting-payment">
                <div class="col-xs-12 mo-block mo-offer-blocks" id="mo-transaction-waiting-payment">
                    <span class="col-xs-12 no-padding mo-block-txt text-capitalize padding-top-8" data-label="Awaiting Payment">awaiting payment</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div> <!-- Tab Awaiting Payment -->

            <div class="col-xs-12 col-sm-3 padding-lr5 tab" id="mo-transaction-tab-organise-transport">
                <div class="col-xs-12 mo-block mo-offer-blocks" id="mo-transaction-organise-transport">
                    <span class="col-xs-12 no-padding mo-block-txt text-capitalize padding-top-8" data-label="Organise Transport">organise transport</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div> <!-- Tab Organise Transport -->

            <div class="col-xs-12 col-sm-3 padding-lr5 tab" id="mo-transaction-tab-payouts">
                <div class="col-xs-12 mo-block mo-offer-blocks" id="mo-transaction-payouts">
                    <span class="col-xs-12 no-padding mo-block-txt text-capitalize padding-top-8" data-label="Payouts">payouts</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/svg/main/icon-focus-box.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-focus-box.png') }}'">
                    </span>
                </div>
            </div> <!-- Tab Organise Transport -->
        </div>


        <div class="mo-row margin-top-32 hidden mo-block-list padding-lr5" id="mo-transaction-list-wrapper">

            <div class="mo-row percent-h-100">
                <div class="mo-row col-xs-12 mo-filter">
                    <label for="">Filter </label><span class="glyphicon glyphicon-menu-down padding-left-20"></span>
                    <span class="float-right">
                        <input type="checkbox" class="mo-round-check" id="show-required-actions-only" name="show_required_actions_only" />
                        <label for="show-required-actions-only" class="text-capitalize"><span></span>Show Required Actions Only</label>
                    </span>
                </div>
                <table id="mo-transaction-current-list" class="table" cellspacing="0">
                    <thead>
                    <tr>
                        <th>First Name <span class="float-right"><img src="{{ asset('images/svg/main/icon-swipe-bottom.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-bottom.png') }}'"></span></th>
                        <th></th>
                        <th>Trans #</th>
                        <th>Selling Dealer</th>
                        <th>Buying Dealer</th>
                        <th>Car Description</th>
                        <th>Outstanding Amount</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
                    <ul class="pagination">
                        <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                        <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
                    </ul>
                </div>
                <div class="mo-round-btn"><a href="{!! url('mo-admin/create-dealership/new'); !!}">NEW</a></div>
            </div>
        </div>
    </div>

@endsection