@extends("mo-admin.layouts.master")

@section('title', 'moto - Transaction Details')

@section('content')
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            {{$header or 'Transaction'}} > <a class="text-capitalize underline-text" href="/mo-admin/transaction/view-transactions/{{ $breadcrumb or ''}}"> {{ $breadcrumb or ''}} </a> > <span class="yellow-text">Transaction {{ $transactionDetails->rid or '' }}</span>
        </div>
    </div>

    <div class=" mo-row mo-form-wrapper">
        {{--HIDDEN FIELDS--}}
        <input type="hidden" id="transaction-id" value="{{$transactionDetails->id or ''}}">
        <input type="hidden" id="transaction-status" value="{{$transactionDetails->status or ''}}">
        <input type="hidden" id="type" value="{{$type or ''}}">
        {{--HIDDEN FIELDS--}}

        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
            <div class="mo-row padding-bottom-24">
                <div class="col-xs-12 col-sm-12 no-padding padding-bottom-24 mo-header text-capitalize">
                    transaction information
                </div>
                <div class="mo-row padding-bottom-24">
                    <div class="col-xs-12 col-sm-6">
                        <div class="mo-row padding-bottom-10">
                            <label class="col-xs-12 col-sm-4 no-padding">Transaction No: </label>
                            <span class="col-xs-12 col-sm-8 no-padding" id="transaction-rid">{{ $transactionDetails->rid or '' }}</span>
                        </div>
                        <div class="mo-row">
                            <label class="col-xs-12 col-sm-4 no-padding">Vehicle Details: </label>
                            <span class="col-xs-12 col-sm-8 no-padding">{{ $transactionDetails->vehicle_name or '' }}</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="mo-row padding-bottom-10">
                            <label class="col-xs-12 col-sm-6 no-padding">Auction/Place My Car No.: </label>
                            <span class="col-xs-12 col-sm-6 no-padding">{{ $transactionDetails->auctionNo or '' }}</span>
                        </div>
                        <div class="mo-row padding-bottom-10">
                            <label class="col-xs-12 col-sm-6 no-padding">Rego: </label>
                            <span class="col-xs-12 col-sm-6 no-padding">{{ $transactionDetails->rego or '' }}</span>
                        </div>
                        <div class="mo-row">
                            <label class="col-xs-12 col-sm-6 no-padding">Tranport Tracking No.: </label>
                            <span class="col-xs-12 col-sm-6 no-padding">{{ $transactionDetails->transportNo or '' }}</span>
                        </div>
                    </div>
                </div>
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-5">
                        <label class="text-capitalize">selling dealer:</label>
                        {{--<div><span>{{ $transactionDetails->selling_dealer or '' }}</span></div>--}}
                        <div><span>{{ $transactionDetails->seller_address or '' }}</span></div>
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        <label class="text-capitalize">buying dealer:</label>
                        {{--<div><span>{{ $transactionDetails->buying_dealer or '' }}</span></div>--}}
                        <div><span>{{ $transactionDetails->buyer_address or '' }}</span></div>
                    </div>
                    <div class="col-xs-12 col-sm-2">
                        <label class="text-capitalize">status:</label>
                        <div class="text-capitalize yellow-text">{{ $transactionDetails->status_value or '' }}</div>
                        @if(isset($transactionDetails->action_button) && !empty($transactionDetails->action_button))
                            <div>
                                @if($transactionDetails->status == 1){{--RECEIVE PAYMENT--}}
                                    <a href="#mo-transaction-receive-payment-popup" class="mo-payment-popup">
                                @elseif($transactionDetails->status == 22){{--BOOKING TRANSPORT--}}
                                    <a href="#mo-transaction-booking-transport-popup" class="mo-booking-popup">
                                @endif
                                <input type="button" id="transaction-btn" class="width-150 margin-top-10 mo-btn-yellow text-uppercase font-family-bold" value="{{ $transactionDetails->action_button }}">
                                @if(in_array($transactionDetails->status, [1,22]))
                                    </a>
                                @endif
                            </div>
                        @endif
                    </div>
                    <div class="col-xs-6 margin-top-24 float-right alert alert-danger hidden" role="alert">
                        <span class="mo-error-message text-capitalize"></span>
                        <a class="mo-close-message close">&times;</a>
                    </div>
                </div>
            </div> <!-- moto Transaction Information -->

            <hr class="mo-dispute-hr">

            <form id="approve-transaction-form">
                <div class="mo-row padding-bottom-24">
                    <div class="col-xs-12 no-padding padding-bottom-24 mo-header text-capitalize">
                        transaction invoices & documents
                    </div>
                    <div class="mo-row padding-bottom-24">
                        <div class="col-xs-12 col-sm-6">
                            <div class="mo-form-header text-uppercase padding-bottom-24">invoices</div>
                            <div class="mo-row padding-bottom-10">
                                {{--<input type="checkbox" class="mo-round-check" id="invoice-for-vehicle" name="invoice_for_vehicle">--}}
                                <label for="invoice-for-vehicle" class="text-capitalize"><span></span>invoice for vehicle</label>
                                <a href="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/transaction-document/{{$invoice_vehicle_doc or ''}}?{{rand()}}" target="_blank">
                                    <input type="button" class="mo-btn mo-view-button float-right {{!isset($invoice_vehicle_doc)?"disabled-div":""}}" id="mo-view-invoice-vehicle" value="View">
                                </a>
                            </div>
                            <div class="mo-row padding-bottom-10">
                                {{--<input type="checkbox" class="mo-round-check" id="mo-seller-invoice" name="mo_seller_invoice">--}}
                                <label for="mo-seller-invoice" class="text-capitalize"><span></span>mo seller invoice</label>
                                <a href="{{$invoice_seller_document or ""}}" target="_blank">
                                    <input type="button" class="mo-btn mo-view-button float-right {{!isset($invoice_seller_document)?"disabled-div":""}}" id="mo-view-seller-invoice" value="View">
                                </a>
                            </div>
                            <div class="mo-row padding-bottom-10">
                                {{--<input type="checkbox" class="mo-round-check" id="mo-buyer-invoice" name="mo_buyer_invoice">--}}
                                <label for="mo-buyer-invoice" class="text-capitalize"><span></span>mo buyer invoice</label>
                                <a href="{{$invoice_buyer_document or ""}}" target="_blank">
                                    <input type="button" class="mo-btn mo-view-button float-right {{!isset($invoice_buyer_document)?"disabled-div":""}}" id="mo-view-buyer-invoice" value="View">
                                </a>
                            </div>
                            <div class="mo-row">
                                {{--<input type="checkbox" class="mo-round-check" id="mo-view-dispute-invoice" name="mo_view_dispute_invoice">--}}
                                <label for="mo-view-dispute-invoice" class="text-capitalize"><span></span>dispute invoice/credit note</label>
                                <a href="{{$credit_note_document or ""}}" target="_blank">
                                    <input type="button" class="mo-btn mo-view-button float-right {{!isset($credit_note_document)?"disabled-div":""}}" id="mo-view-credit-note" value="View">
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="mo-form-header text-uppercase padding-bottom-24">documents</div>
                            <div class="mo-row padding-bottom-10">
                                {{--<input type="checkbox" class="mo-round-check" id="vehicle-transfer-document-1" name="vehicle_transfer_document_1">--}}
                                <label for="vehicle-transfer-document-1" class="text-capitalize"><span></span>Vehicle Transfer Document</label>
                                <a href="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/transaction-document/{{$vehicle_transfer_doc or ''}}?{{rand()}}" target="_blank">
                                    <input type="button" class="mo-btn mo-view-button float-right {{!isset($vehicle_transfer_doc)?"disabled-div":""}}" id="mo-view-transfer-document-1" value="View">
                                </a>
                            </div>
                            <div class="mo-row padding-bottom-10">
                                {{--<input type="checkbox" class="mo-round-check" id="vehicle-transfer-document-2" name="vehicle_transfer_document_1">--}}
                                <label for="vehicle-transfer-document-2" class="text-capitalize"><span></span>Vehicle Transfer Document (Additional)</label>
                                <a href="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/transaction-document/{{$vehicle_transfer_additional_doc or ''}}?{{rand()}}" target="_blank">
                                    <input type="button" class="mo-btn mo-view-button mo-view-button-additional float-right {{!isset($vehicle_transfer_additional_doc)?"disabled-div":""}}" id="mo-view-transfer-document-2" value="View">
                                </a>
                            </div>
                            <div class="mo-row padding-bottom-10">
                                {{--<input type="checkbox" class="mo-round-check" id="financial-payout-letter" name="financial_payout_letter">--}}
                                <label for="financial-payout-letter" class="text-capitalize"><span></span>PPSR/Financial Payout Letter</label>
                                <a href="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/transaction-document/{{$ppsr_financial_payout_doc or ''}}?{{rand()}}" target="_blank">
                                    <input type="button" class="mo-btn mo-view-button float-right {{!isset($ppsr_financial_payout_doc)?"disabled-div":""}}" id="mo-view-ppsr" value="View">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="mo-row text-center">
                        <input type="checkbox" class="mo-round-check" id="mo-all-document" name="mo_all_document" {{isset($transactionDetails->status) && ($transactionDetails->status == 27 || $transactionDetails->status == 5)?'checked disabled':''}}>
                        <label for="mo-all-document" class="text-capitalize"><span></span>All Documentation checked and approved*</label>
                    </div>
                    <div id="errorToShow_all_document" class="col-md-4 col-md-offset-4"></div>
                </div> <!-- moto Transaction Invoices & Documents -->
            </form>


            <hr class="mo-dispute-hr">
            <form id="complete-transaction-form">
                <div class="mo-row padding-bottom-24">
                    <div class="col-xs-12 col-sm-12 no-padding padding-bottom-24 mo-header text-capitalize">
                        transaction summary
                    </div>
                    <div class="mo-row padding-bottom-24">
                        <div class="col-xs-12 col-sm-6">
                            <div class="mo-form-header text-uppercase padding-bottom-24">seller</div>
                            <div class="mo-row padding-bottom-24 margin-bottom-70">
                                <div class="mo-row padding-bottom-10">
                                    <label>Vehicle Price</label><span class="float-right">${{ $transactionDetails->sell_vehicle_price_format or 0 }}</span>
                                </div>
                                <div class="mo-row padding-bottom-10">
                                    <label>moto Fee</label><span class="float-right">- ${{ $transactionDetails->sell_mo_fee_format or 0 }}</span>
                                </div>
                                {{--HAS DISPUTE--}}
                                @if(isset($transactionDetails->dispute_id) && !empty($transactionDetails->dispute_id))
                                    <div class="mo-row">
                                        <label>Dispute Adjustment - Dispute No.</label><span class="float-right">- ${{ $transactionDetails->sell_dispute_adjust_format or 0 }}</span>
                                    </div>
                                @endif
                            </div>
                            <hr class="mo-dispute-hr">
                            <div class="mo-row mo-payout-amount padding-bottom-10">
                                <label class="text-capitalize">Payout amount:</label><span class="float-right" id="seller-pay-amount">${{$transactionDetails->sell_pay_amount_format or 0}}</span>
                            </div>
                            @if(isset($transactionDetails->status) && ($transactionDetails->status == 5 || $transactionDetails->status == 27))
                                <div class="float-right">
                                    <input type="checkbox" class="mo-round-check" id="mo-seller-mark-as-paid" name="mo_seller_mark_as_paid" {{$transactionDetails->status == 5?'checked disabled':''}}>
                                    <label for="mo-seller-mark-as-paid" class="text-uppercase"><span></span>Mark As Paid*</label>
                                    <div id="errorToShow_seller_paid"></div>
                                    {{--<input type="button" class="mo-btn mo-mark-as-paid-button float-right" id="mo-mark-as-paid-button" value="MARK AS PAID">--}}
                                </div>
                            @endif
                        </div>


                        <div class="col-xs-12 col-sm-6">
                            <div class="mo-form-header text-uppercase padding-bottom-24">buyer</div>
                            <div class="mo-row padding-bottom-24">
                                <div class="mo-row padding-bottom-10">
                                    <label>Vehicle Price</label><span class="float-right">- ${{ $transactionDetails->buy_vehicle_price_format or 0 }}</span>
                                </div>
                                <div class="mo-row padding-bottom-10">
                                    <label>Transport</label><span class="float-right">- ${{ $transactionDetails->buy_transport_format or 0 }}</span>
                                </div>
                                <div class="mo-row padding-bottom-10">
                                    <label>moto Fee</label><span class="float-right">- ${{ $transactionDetails->buy_mo_fee_format or 0 }}</span>
                                </div>
                                <div class="mo-row padding-bottom-10">
                                    <label>Payment Made</label><span class="float-right">${{ $transactionDetails->buy_payment_made_format or 0 }}</span>
                                </div>
                                {{--HAS DISPUTE--}}
                                @if(isset($transactionDetails->dispute_id) && !empty($transactionDetails->dispute_id))
                                    <div class="mo-row">
                                        <label>Dispute Adjustment - Dispute No.</label><span class="float-right">${{ $transactionDetails->buy_dispute_adjust_format or 0 }}</span>
                                    </div>
                                @endif
                            </div>
                            <hr class="mo-dispute-hr">
                            <div class="mo-row mo-payout-amount padding-bottom-10">
                                <label class="text-capitalize">Payout amount:</label><span class="float-right" id="buyer-pay-amount">${{ $transactionDetails->buy_pay_amount_format or 0 }}</span>
                            </div>
                            @if(isset($transactionDetails->status) && ($transactionDetails->status == 5 || $transactionDetails->status == 27))
                                <div class="float-right">
                                    <input type="checkbox" class="mo-round-check" id="mo-buyer-mark-as-paid" name="mo_buyer_mark_as_paid" {{$transactionDetails->status == 5?'checked disabled':''}}>
                                    <label for="mo-buyer-mark-as-paid" class="text-uppercase"><span></span>Mark As Paid*</label>
                                    <div id="errorToShow_buyer_paid"></div>
                                    {{--<input type="button" class="mo-btn mo-mark-as-paid-button float-right" id="mo-mark-as-paid-button" value="MARK AS PAID">--}}
                                </div>
                            @endif

                        </div>
                    </div>
                </div> <!-- Transaction Summary -->
            </form>

        </div>

        <!-- AJAX - loading -->
        <div class="mo-loading-image hidden">Loading&#8230;</div>
        <!-- AJAX - end loading -->

    </div>

    @include("mo-admin.main.transaction.transaction-popup")
@endsection