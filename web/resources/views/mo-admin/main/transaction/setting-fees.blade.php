@extends('mo-admin.layouts.master')

@section('title', 'moto - Main')

@section('content')
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            {{$header or 'Settings'}} > <span class="yellow-text">{{ $breadcrumb or 'Fees'}}</span>
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding ">
            <div class="alert alert-success alert-dismissable hidden transaction-fees-setting-alert">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              Transaction fees have been updated!
            </div>
            <div class="alert alert-danger alert-dismissable hidden transaction-fees-setting-error-alert">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              An error has been occured!
            </div>
            <form action="" method="post" id="mo-transaction-setting-fees">
                <div class="col-xs-12 col-sm-12 col-md-12 padding-bottom-24 text-uppercase mo-form-header">
                    transaction fee
                </div>

                {{ csrf_field() }}

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="for-seller" class="text-capitalize">for seller</label>
                        <input type="text" class="form-control" name="for_seller_fee" id="for-seller"
                               value="{{$transaction_fees->seller_mo_fee}}">
                    </div>
                </div> <!-- Row 1 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="for-buyer" class="text-capitalize">for buyer</label>
                        <input type="text" class="form-control" name="for_buyer_fee" id="for-buyer"
                               value="{{$transaction_fees->buyer_mo_fee}}">
                    </div>
                </div> <!-- Row 2 -->

                <div class="col-xs-12 col-sm-12 col-md-12 padding-bottom-24 text-uppercase mo-form-header">
                    transportation cost
                </div>

                <div class="mo-row">

                    <div class="col-xs-12 col-sm-12 col-md-12 table-setting-fees-wraper">
                    
                        <table class="table table-bordered table-setting-fees-list">

                            <tr>
                                <td><div class="font-orange-color font-weight-bold">REGION</div></td>
                                @foreach ($regions as $region)
                                    <td><div class="content">{{$region->region_name}}</div></td>
                                @endforeach

                            </tr>

                            @foreach ($regions as $region)
                            <tr>
                                <td><div class="content">{{$region->region_name}}</div></td>
                                @foreach ($regions as $cell_region)
                                    @if ($cell_region->id <= $region->id)
                                        <?php $add_more = true ?>

                                        @foreach ($transaction_fees->transport_price as $price)
                                            @if ($price->from_region == $cell_region->id && $price->to_region == $region->id)
                                                <td>
                                                    <div class="content-number edit-transaction-fees" id="{{$cell_region->id}}-{{$region->id}}">$<span class="fee-number">{{$price->price}}</span></div>
                                                    <input type="text" class="content-number width-50 edit-transaction-fees-input hidden" id="" value="0" />
                                                </td>
                                                <?php $add_more = false ?>
                                            @endif       
                                        @endforeach

                                        @if ($add_more)
                                            <td class="text-center">
                                                <div class="content-number edit-transaction-fees" id="{{$cell_region->id}}-{{$region->id}}">$<span class="fee-number">0</span></div>
                                                <input type="text" class="content-number width-50 edit-transaction-fees-input hidden" id="" value="0" />
                                            </td>
                                        @endif   

                                    @else
                                        <td class="background-light-grey">
                                            <div class="content-number "></div>
                                        </td>
                                    @endif
                                @endforeach

                            </tr>
                            @endforeach

                            {{--
                            <tr>
                                <td><div class="content">Central Coast</div></td>
                                <td><div class="content-number">0</div></td>
                                <td><div class="content-number">0</div></td>
                                <td><div class="content-number">0</div></td>
                                <td><div class="content-number">0</div></td>
                                <td><div class="content-number">0</div></td>
                                <td><div class="content-number">0</div></td>
                                <td><div class="content-number">0</div></td>
                            </tr>
                            --}}
                            

                        </table>

                    </div>

                </div>

                <div class="mo-row">
                    <input type="button" value="Save" class="mo-btn text-uppercase" id="transaction-fees-save-btn" />
                </div>

            </form>
        </div>
    </div>
    <div class="mo-loading-image hidden">Loading&#8230;</div>
@endsection