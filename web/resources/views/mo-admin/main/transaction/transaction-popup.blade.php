{{------------  BOOKING TRANSPORT POPUP ------------}}
<div id="mo-transaction-booking-transport-popup" class="mo-white-popup mfp-hide">
    <form class="font-size14" id="mo-transaction-booking-form">
        {{ csrf_field() }}
        {{----------HEADER - TITLE----------}}
        <div class="mo-row padding-bottom-20">
            <span class="text-capitalize mo-popup-heading">Booking Transport</span>
        </div>
        <hr class="mo-form-hr" />
        {{----------CONTENT----------}}

        <div class="form-inline col-xs-12 padding-top-24">
            <label for="transport-tracking-no" class="col-xs-12 col-sm-4 no-padding">Transport Tracking No*</label>
            <input type="text" id="transport-tracking-no" name="transport_tracking_no" class="form-control">
            {{--<textarea placeHolder="Comment" class="mo-textarea form-control box-shadow-none" rows="5" id="request-list-reject-comment" name="request-list-reject-comment"></textarea>--}}
        </div>

        <div class="mo-row">
            <div class="mo-row margin-top-24 float-right alert alert-danger hidden" role="alert">
                <span class="mo-error-message text-capitalize"></span>
                <a class="mo-close-message close">&times;</a>
            </div>
        </div>
        <hr class="mo-form-hr" />
        {{----------FOOTER - BUTTONS----------}}
        <div class="mo-row">
            <div class="col-xs-12 padding-top-24 text-right">
                <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                <input type="submit" class="mo-btn text-uppercase" value="submit" id="booking-transport-btn">
            </div>
        </div>
        <input type="hidden" id="popup-transaction-id" name="transaction_id">
    </form>
</div>
{{------------ End - BOOKING TRANSPORT POPUP ------------}}

{{------------  RECEIVE PAYMENT POPUP ------------}}
<div id="mo-transaction-receive-payment-popup" class="mo-white-popup mfp-hide">
    <form class="font-size14" id="mo-payment-form">
        {{ csrf_field() }}
        {{----------HEADER - TITLE----------}}
        <div class="mo-row padding-bottom-20">
            <span class="text-capitalize mo-popup-heading">Receive Payment</span>
        </div>
        <hr class="mo-form-hr" />
        {{----------CONTENT----------}}

        <div class="form-inline col-xs-12 padding-top-24">
            <div class="font-weight-bold font-size16 text-center">Please confirm that you have received <span id="outstanding-amount-payment"></span></div>
            <input type="hidden" id="payment-amount" name="payment_amount">
            {{--<label for="payment-amount" class="col-xs-12 col-sm-4 no-padding">Payment Amount*</label>
            <input type="number" id="payment-amount" name="payment_amount" class="form-control">--}}
        </div>

        <div class="mo-row">
            <div class="mo-row margin-top-24 float-right alert alert-danger hidden" role="alert">
                <span class="mo-error-message text-capitalize"></span>
                <a class="mo-close-message close">&times;</a>
            </div>
        </div>
        <hr class="mo-form-hr" />
        {{----------FOOTER - BUTTONS----------}}
        <div class="mo-row">
            <div class="col-xs-12 padding-top-24 text-right">
                <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                <input type="submit" class="mo-btn text-uppercase" value="submit" id="payment-btn">
            </div>
        </div>
        <input type="hidden" id="popup-transaction-id" name="transaction_id">
        <input type="hidden" id="popup-transaction-rid" name="transaction_rid">
    </form>
</div>
{{------------ End - RECEIVE PAYMENT POPUP ------------}}