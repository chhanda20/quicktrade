@extends('mo-admin.layouts.master')

@section('title', 'moto - Main')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-8 no-padding" id="mo-content-header">
            {{ $header or 'Dealer Group' }}
        </div>
        {{--Start - moto Search Section--}}
        <div class="col-xs-12 col-sm-4 no-padding">
            {{ csrf_field() }}
            <div class="col-xs-12 no-padding mo-search-wrapper">
                <input type="text" placeholder="Search" id="mo-txt-search">
                <span class="glyphicon glyphicon-search mo-txt-search cursor-pointer"></span>
            </div>
        </div>
        {{--End - moto Search Section--}}
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - moto List Dealer Gorups Section--}}
    <div class="mo-row padding-top-24 percent-h-100" id="mo-list-wrapper">
        <div class="mo-row mo-form-wrapper percent-h-100">
            <div class="mo-row margin-left-20 mo-filter">
                <input type="checkbox" class="mo-round-check" id="mo-filter-dealership" name="" />
                <label for="mo-filter-dealership" class="text-capitalize"><span></span>View active Dealer Group only.</label>
                <span></span>
            </div>
            <table id="mo-company-groups-list" class="table" cellspacing="0">
                <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
                <ul class="pagination">
                    <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                    <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
                </ul>
            </div>
            <div class="mo-round-btn"><a href="{!! url('mo-admin/create-dealer-group/new'); !!}">NEW</a></div>
        </div>
    </div>
    {{--End - moto List Customers Section--}}
@endsection