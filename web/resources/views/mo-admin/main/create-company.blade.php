@extends('mo-admin.layouts.master')

@section('title', 'moto - Main')

@section('content')
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <a href="{!! url('mo-admin/dealership-list') !!}">{{ $header or 'Dealership'}}</a> > {{ $breadcrumb or 'New'}}
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
    	<div class="col-xs-12 col-sm-12 col-md-12 no-padding">
    		<div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                dealership details
            </div>
            <form action="{!! url('mo-admin/create-dealership') !!}" method="post" id="mo-create-dealership-form">
                {{ csrf_field() }}
                <div class="col-xs-12 alert alert-danger hidden" role="alert">
                    <a class="mo-close-message close">&times;</a>
                </div>
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="dealer-group" class="text-capitalize">dealer group*</label>
                        <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                        <select class="form-control text-capitalize" id="delear-group" name="dealer_group">
                            <option selected disabled>dealer group list</option>
                            @if (isset($listCompanyGroups))
                                @foreach ($listCompanyGroups as $listCompanyGroup)
                                    <option {{isset($company_details->company_group_id) && $company_details->company_group_id == $listCompanyGroup->id?'selected':''}} value="{{$listCompanyGroup->id}}">{{$listCompanyGroup->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div> <!-- Row 1 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="name" class="text-capitalize">name*</label>
                        <input type="text" class="form-control" id="dealer-name" name="dealer_name" value="{{isset($company_details->name)?$company_details->name:''}}">
                    </div>
                </div> <!-- Row 2 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="address" class="text-capitalize">address*</label>
                        <input type="text" class="form-control" id="dealer-address" name="dealer_address" value="{{isset($company_details->address)?$company_details->address:''}}">
                    </div>
                </div> <!-- Row 3 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="suburb" class="text-capitalize">suburb*</label>
                        <input type="text" class="form-control" id="dealer-suburb" name="dealer_suburb" value="{{isset($company_details->suburb)?$company_details->suburb:''}}">
                    </div>
                </div>

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="dealer-state" class="text-capitalize">State*</label>
                        <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                        <select class="form-control text-capitalize" id="dealer-state" name="dealer_state" value="">
                            <option selected disabled>state list</option>
                        </select>
                        <input type="hidden" value="{{isset($company_details->state) ? $company_details->state : ''}}" name="dealer-state-data" id="dealer-state-data">
                    </div>
                </div>

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                        <label for="dealer-country" class="text-capitalize">Country*</label>
                        <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                        <select class="form-control text-capitalize" id="dealer-country" name="dealer_country" disabled="true">
                            <option selected disabled>country list</option>

                            @if (isset($country_list))
                                @foreach ($country_list as $country)
                                    <option {{$country->id == 13 ? "selected" : ''}} value="{{$country->id}}" {{isset($company_details->country) && $company_details->country == $country->id ? 'selected' : ''}} >{{$country->name}}</option>
                                @endforeach
                            @endif

                        </select>
                        <input type="hidden" id="input-country-id" name="input_country_id">
                        <input type="hidden" value="{{isset($country_list) ? json_encode($country_list) : null}}" name="country-list" id="country-list" />
                    </div>
                </div>



                <input type="hidden" id="mo-dealership-id" value="{{isset($company_details->id)?$company_details->id:''}}">
                <input type="hidden" name="dealer_status" value="{{isset($company_details->status_code)?$company_details->status_code:2}}">

                @if(strtolower($breadcrumb) == "new")
                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6 col-md-6 mo-form-header text-uppercase padding-bottom-10" id="mo-dealership-agent-header">agent</div>
                    </div>

                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-20">
                            <label for="first-name" class="text-capitalize">first name*</label>
                            <input type="text" class="form-control" id="first-name" name="first_name">
                        </div>
                    </div> <!-- Row 4 -->

                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                            <label for="last-name" class="text-capitalize">last name*</label>
                            <input type="text" class="form-control" id="last-name" name="last_name">
                        </div>
                    </div> <!-- Row 5 -->

                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                            <label for="email" class="text-capitalize">email*</label>
                            <input type="text" class="form-control" id="email" name="email">
                        </div>
                    </div> <!-- Row 6 -->

                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                            <label for="mobile" class="text-capitalize">mobile number*</label>
                            <input type="text" class="form-control" id="mobile" name="mobile">
                        </div>
                    </div>

                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-24">
                            <label for="privacy" class="text-capitalize">privacy*</label>
                            <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                            <select class="form-control text-capitalize" id="privacy" name="privacy">
                                <option selected disabled>privacy list</option>
                                @if (isset ($privacies_flag))
                                    @foreach ($privacies_flag as $privacy)
                                        <option {{ isset($company_details->privacy) && $company_details->privacy == $privacy->code ? 'selected' : '' }} value="{{ $privacy->code }}">{{ $privacy->value }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div> 
                @endif

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-bottom-30">
                        @if (strtolower($breadcrumb) == "new")
                            <input type="submit" class="mo-btn text-uppercase" value="create" id="dealer-create">
                        @elseif(strtolower($breadcrumb) == "edit")
                            <input type="submit" class="mo-btn text-uppercase" value="update" id="dealer-update">
                        @endif
                    </div>
                </div>
            </form>
            <div class="mo-loading-image hidden">Loading&#8230;</div>
    	</div>
    </div>
@endsection

