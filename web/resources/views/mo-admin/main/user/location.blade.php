<table id="location_table">
    <tr>
        <th></th>
        <th><span class="font-weight-bold">Code</span></th>
        <th><span class="font-weight-bold">Name</span></th>
    </tr>

    @foreach ($locations as $location)
        <tr>
            <td><span loc="{{ $location->location_code }}" class="float-left margin-right-20 checkbox-item unchecked-checkbox" src="{{ asset('images/svg/checked-checkbox.svg') }}"></span></td>
            <td><label for="data_{{ $location->location_code }}" class="font-warm-grey-color font-family-light ">{{ $location->location_code }}</label></td>
            <td><div class="font-warm-grey-color font-family-light name_col">{{ $location->name }}</div></td>
        </tr>
    @endforeach
</table>