@extends('mo-admin.layouts.master')

@section('title', 'moto - Main')

@section('content')
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <a href="{!! url('/mo-admin/user/list') !!}" class="underline-text">{{ $header or 'User' }}</a> > <span class="yellow-text">{{ $breadcrumb or 'New'}}</span>
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 text-uppercase mo-form-header">
                user details
            </div>
            <form action="{{ url('mo-admin/user/saveUser') }}{{ $mode == 'edit' ? '/'.$user_data->userInfo[0]->id : ''}}" method="post" id="mo-create-user-form">
                {{ csrf_field() }}

                <input type="hidden" id="user-id" value="{{isset($user_data->userInfo[0]->id)?$user_data->userInfo[0]->id:""}}">
                <input type="hidden" value="" name="contact-id" />
                <input type="hidden" id="user_role_id" name="user_role_id" value="{{isset($user_data->userInfo[0]->roleId) ? $user_data->userInfo[0]->roleId : '' }}">

                <div class="col-xs-12 alert alert-danger hidden" role="alert">
                    <a class="mo-close-message close">&times;</a>
                </div>

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24">
                        <label for="dealer-group" class="text-capitalize">group*</label>
                        <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                        <select class="form-control text-capitalize" id="dealer_group" name="dealer_group">
                            <option selected disabled>...</option>
                            @if (isset($listCompanyGroups))
                                @foreach ($listCompanyGroups as $listCompanyGroup)
                                    <option {{isset($user_data->userInfo[0]->company_group_id) && $user_data->userInfo[0]->company_group_id == $listCompanyGroup->id ? 'selected' : ''}} value="{{$listCompanyGroup->id}}">{{$listCompanyGroup->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24 float-right">
                        <label for="user-code" class="text-capitalize">Code*</label>
                        <input type="text" class="form-control" id="user-code" name="user-code" value="{{isset($user_data->userInfo[0]->user_code) ? $user_data->userInfo[0]->user_code : ''}}">

                    </div>
                </div>

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24">
                        <label for="dealer-group" class="text-capitalize">Dealership*</label>
                        <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                        <select class="form-control text-capitalize" id="dealership" name="dealership">
                            <option selected value="">...</option>
                        </select>
                        <input type="hidden" id="dealership_selected" name="dealership_selected" value="{{ $user_data->userInfo[0]->company_id or 0 }}"/>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24 float-right">
                        <label for="user-password" class="text-capitalize">Password{{$mode == "edit" ? "" : "*"}}</label>
                        <span class="mo-user-password fa fa-eye cursor-pointer"></span>
                        <input type="password" class="form-control" id="user-password"  name="user-password" autocomplete="new-password"/>
                        <span class="mo-user-show-password fa fa-eye-slash cursor-pointer"></span>

                    </div>
                </div>

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24">
                        <label for="dealer-group" class="text-capitalize">Location*</label>
                        <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                        <select class="form-control text-capitalize" id="location" name="location">
                            <option selected value="">...</option>
                        </select>
                        <input type="hidden" id="location_selected" name="location_selected" value="{{ $user_data->userInfo[0]->location_id or 0 }}"/>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24 float-right">
                        <label for="user-role" class="text-capitalize">Role</label>
                        <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>

                        <select disabled class="form-control text-capitalize" id="user-role" name="user-role">
                            <option selected></option>
                        </select>

                    </div>
                </div>

                @if ($mode == "new")
                    <div class="mo-row">
                        <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24">
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="radio" class="mo-round-check" id="existing-contact" name="user-contact" value="existing-contact" />
                                    <label for="existing-contact" class="text-uppercase mo-high-light margin-left--5"><span style="margin-bottom: 2px"></span>Existing Contact.</label>
                                </div>
                            </div>
                            <div class="row padding-top-16">
                                <div class="col-xs-12">
                                    <label for="user-search-contact" class="text-capitalize">Search Contact</label>
                                    <div class="pos-relative">
                                        <span id="search-icon" class="glyphicon glyphicon-search cursor-pointer user-search-contact user-search-contact-button"></span>
                                        <input type="text" class="form-control text-capitalize" id="user-search-contact" name="user-search-contact" value="">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24 float-right"></div>
                    </div>
                    <div class="mo-row">
                        <div class="col-xs-12 padding-bottom-24">
                            <input type="radio" class="mo-round-check margin-left--5" id="new-contact" name="user-contact" checked value="new-contact" />
                            <label for="new-contact" class="text-uppercase mo-high-light"><span style="margin-bottom: 2px"></span>New Contact.</label>

                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24 float-right"></div>
                    </div>
                @endif

                <div class="mo-row">
                    <div class="col-xs-6 col-sm-5 col-md-5 padding-bottom-24">
                        <label for="user-title" class="text-capitalize">Title</label>
                        <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                        <select class="form-control text-capitalize" id="user-title" name="user-title">
                            @if (isset($titles))
                                <option disabled selected>Title list</option>
                                @foreach ($titles as $title)
                                    <option value="{{$title->code}}" {{isset($user_data->userInfo[0]->title) && $user_data->userInfo[0]->title == $title->code ? 'selected' : ''}}>{{$title->value}}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24 float-right">
                        <label for="user-gender" class="text-capitalize">Gender</label>
                        <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                        <select class="form-control text-capitalize" id="user-gender" name="user-gender">
                            <option selected disabled>gender list</option>

                            @if (isset($genders))
                                @foreach ($genders as $gender)
                                    <option value="{{$gender->code}}" {{isset($user_data->userInfo[0]->gender) && $user_data->userInfo[0]->gender == $gender->code ? 'selected' : ''}}>{{$gender->value}}</option>
                                @endforeach
                            @endif

                        </select>
                    </div>
                </div><!-- Row 4 -->


                <div class="mo-row">
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24">
                        <label for="user-firstname" class="text-capitalize">First Name*</label>
                        <input type="text" class="form-control" id="user-firstname" name="user-firstname" value="{{isset($user_data->userInfo[0]->first_name) ? $user_data->userInfo[0]->first_name : ''}}">
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24 float-right">
                        <label for="user-lastname" class="text-capitalize">Last Name</label>
                        <input type="text" class="form-control" id="user-lastname" name="user-lastname" value="{{isset($user_data->userInfo[0]->last_name) ? $user_data->userInfo[0]->last_name : ''}}">
                    </div>
                </div><!-- Row 6 -->


                <div class="mo-row">
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24">
                        <label for="user-company-name" class="text-capitalize">Company Name</label>
                        <input type="text" class="form-control" id="user-company-name" name="user-company-name" value="{{isset($user_data->userInfo[0]->company_name) ? $user_data->userInfo[0]->company_name : ''}}">
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24 float-right">
                        <label for="user-address-1" class="text-capitalize">Address 1*</label>
                        <input type="text" class="form-control" id="user-address-1" name="user-address-1" value="{{isset($user_data->userInfo[0]->address1) ? $user_data->userInfo[0]->address1 : ''}}">
                    </div>
                </div><!-- Row 8 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24">
                        <label for="user-address-2" class="text-capitalize">Address 2</label>
                        <input type="text" class="form-control" id="user-address-2" name="user-address-2" value="{{isset($user_data->userInfo[0]->address2) ? $user_data->userInfo[0]->address2 : ''}}">
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24 float-right">
                        <label for="user-suburb" class="text-capitalize">Suburb*</label>
                        <input type="text" class="form-control" id="user-suburb" name="user-suburb" value="{{isset($user_data->userInfo[0]->suburb) ? $user_data->userInfo[0]->suburb : ''}}">
                    </div>
                </div><!-- Row 10 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24">
                        <label for="user-postcode" class="text-capitalize">Postcode</label>
                        <input type="text" class="form-control" id="user-postcode" name="user-postcode" value="{{isset($user_data->userInfo[0]->postcode) ? $user_data->userInfo[0]->postcode : ''}}">
                    </div>

                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24 float-right">

                        <label for="user-state" class="text-capitalize">State*</label>
                        <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                        <select class="form-control text-capitalize" id="user-state" name="user-state" value="">
                            <option selected disabled>state list</option>
                        </select>
                        <input type="hidden" value="{{isset($user_data->userInfo[0]->state) ? $user_data->userInfo[0]->state : ''}}" name="user-state-data">

                    </div>

                </div><!-- Row 12 -->

                <div class="mo-row">

                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24">

                        <label for="user-country" class="text-capitalize">Country*</label>
                        <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                        <select class="form-control text-capitalize" id="user-country" name="user-country" disabled>

                            @if (isset($country_list))
                                @foreach ($country_list as $country)
                                    <option value="{{$country->id}}" selected>{{$country->name}}</option>
                                @endforeach
                            @endif

                        </select>
                        <input type="hidden" value="" id="input-country-id" name="user_country_input">
                        <input type="hidden" value="{{isset($country_list) ? json_encode($country_list) : null}}" name="country-list" id="country-list" />

                    </div>

                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24 float-right">
                        <label for="user-mobile-number" class="text-capitalize">Mobile Number*</label>
                        <input type="text" class="form-control" id="user-mobile-number" name="user-mobile-number" value="{{isset($user_data->userInfo[0]->mobile_phone) ? $user_data->userInfo[0]->mobile_phone : ''}}">
                    </div>
                    
                </div><!-- Row 13 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24">
                        <label for="user-email" class="text-capitalize">Email Address*</label>
                        <input type="text" class="form-control" id="user-email" name="user-email" value="{{isset($user_data->userInfo[0]->email) ? $user_data->userInfo[0]->email : ''}}">
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24 float-right">
                        <label for="user-privacy" class="text-capitalize">Privacy*</label>
                        <span><img class="imgicon" src="{{ asset('images/svg/main/icon-swipe-right-copy-4.svg') }}" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-right-copy-4.png') }}'"></span>
                        <select class="form-control text-capitalize" id="user-privacy" name="user-privacy" value="">
                            <option selected disabled>privacy list</option>
                            @if (isset($privacies_flag))
                                @foreach ($privacies_flag as $privacy)
                                    <option value="{{$privacy->code}}" {{isset($user_data->userInfo[0]->privacy_flag) && $user_data->userInfo[0]->privacy_flag == $privacy->code ? 'selected' : ''}} >{{$privacy->value}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div><!-- Row 16 -->


                <div class="mo-row">
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24">
                        <label for="user-note" class="text-capitalize">Note</label>
                        <textarea class="form-control text-capitalize" id="user-note" name="user-note" placeHolder="note">{{isset($user_data->userInfo[0]->note) ? $user_data->userInfo[0]->note : ''}}</textarea>
                    </div>
                </div><!-- Row 18 -->

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24">
                        <img class="margin-right-12 float-left" src="{{ asset('images/svg/radio-tick-orange.svg') }}" alt=""/><span class="heading">permission</span>
                    </div>
                </div>
                @foreach ($permissions as $category)
                    <div class="mo-row mo-category-list">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <input type="checkbox" class="mo-square-check mo-role-category" category_id="{{ $category->category_id }}" id="category-{{ $category->category_id }}" name="category_permission_list[]"/>
                            <label for="category-{{ $category->category_id }}" class="text-capitalize mo-category">{{ $category->category_name }}<span></span></label>
                        </div>
                    </div>

                    <?php
                    $colNo = 3;
                    $permissionChunks = array_chunk($category->permissions, $colNo);
                    ?>
                    <div class="mo-category-list mo-row padding-bottom-24 col-xs-12 col-sm-12 col-md-12 mo-role-list-item hidden" data-category-id="{{ $category->category_id }}">
                        @foreach ($permissionChunks as $permissionChunk)
                            <?php $count = 1; ?>
                            @foreach ($permissionChunk as $permission)
                                <?php
                                $checked = '';
                                $borderClass = '';
                                if ($count % $colNo !== 1) {
                                    $borderClass = 'border-grey-left';
                                }
                                ?>
                                @if (isset($selectedPermission) && in_array($permission->id, $selectedPermission))
                                    <?php $checked = 'checked'; ?>
                                @endif
                                <div class="col-xs-12 col-sm-12 col-md-4 {{ $borderClass }}">
                                    <input type="checkbox" class="mo-square-check mo-permission" id="item-{{ $permission->id }}" name="permissionList[]" value="{{ $permission->id }}" {{ $checked }}>
                                    <label for="item-{{ $permission->id }}" class="text-capitalize mo-category"><span></span>{{ $permission->name }}</label>
                                </div>
                                <?php $count++; ?>
                            @endforeach
                        @endforeach
                    </div>
                @endforeach

                <div class="mo-row">
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24">
                        <img class="margin-right-12 float-left" src="{{ asset('images/svg/radio-tick-orange.svg') }}" alt=""/><span class="heading">data access</span>
                    </div>
                </div>


                <div class="mo-row">
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24" id="location_section">

                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24 float-right">
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24">
                                <label>From</label>
                                <input type="text" name="range_from" id="range_from" class="form-control" value="{{ isset($accessInfo) ? $accessInfo->range_from : '' }}">
                            </div>
                            <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24 float-right">
                                <label>To</label>
                                <input type="text" name="range_to" id="range_to" class="form-control greaterThan" data-min="range_from" value="{{ isset($accessInfo) ? $accessInfo->range_to : '' }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24">
                                <label>Include</label>
                                <input type="text" name="include" id="include" class="form-control" value="{{ isset($accessInfo) ? $accessInfo->include : '' }}">
                            </div>
                            <div class="col-xs-12 col-sm-5 col-md-5 padding-bottom-24 float-right">
                                <label>Exclude</label>
                                <input type="text" name="exclude" id="exclude" class="form-control" value="{{ isset($accessInfo) ? $accessInfo->exclude : '' }}">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="mo-row">
                    <div class="col-xs-12 col-sm-12 col-md-12 padding-bottom-30 vertical-center ">

                        <input type="submit" class="mo-btn text-uppercase align-center" value="save" id="user-create" name="submit_button">

                    </div>
                </div>
                <div class="mo-loading-image hidden">Loading&#8230;</div>

            </form>
        </div>
    </div>
@endsection

