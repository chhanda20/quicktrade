@extends('mo-admin.layouts.master')

@section('title', 'moto - Users Management')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <div class="col-xs-12 col-sm-8 col-md-8 no-padding">
               {{ $breadcrumb or 'Users' }}
            </div>
            {{ csrf_field() }}
        </div>
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - moto Tabs Section--}}
    <div class="mo-user-blocks-wrapper col-xs-12 no-padding" id="mo-blocks-wrapper">
        <div class="col-xs-12 col-sm-2-5 tab" id="all">
            <div class="col-xs-12 col-sm-12 col-md-12 mo-block mo-green-block">
                <span class="col-xs-12 mo-block-number">{{isset($countAll)?$countAll:0}}</span>
                <span class="col-xs-12 mo-block-txt text-capitalize">All</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/mo-admin/svg/main/grey-triangle.svg') }}" alt="Grey Triangle " onerror="this.onerror=null; this.src='{{ asset('images/mo-admin/main/grey-triangle.png') }}'">
                    </span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-2-5 tab" id="super_admins">
            <div class="col-xs-12 col-sm-12 col-md-12 mo-block mo-grey-block">
                <span class="col-xs-12 mo-block-number">{{isset($countSuperAdmin)?$countSuperAdmin:0}}</span>
                <span class="col-xs-12 mo-block-txt text-capitalize">super admins</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/mo-admin/svg/main/grey-triangle.svg') }}" alt="Grey Triangle " onerror="this.onerror=null; this.src='{{ asset('images/mo-admin/main/grey-triangle.png') }}'">
                    </span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-2-5 tab" id="admins">
            <div class="col-xs-12 col-sm-12 col-md-12 mo-block mo-black-block">
                <span class="col-xs-12 mo-block-number">{{isset($countDealerAdmin)?$countDealerAdmin:0}}</span>
                <span class="col-xs-12 mo-block-txt text-capitalize">admins</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/mo-admin/svg/main/grey-triangle.svg') }}" alt="Grey Triangle " onerror="this.onerror=null; this.src='{{ asset('images/mo-admin/main/grey-triangle.png') }}'">
                    </span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-2-5 tab" id="users">
            <div class="col-xs-12 col-sm-12 col-md-12 mo-block mo-yellow-block">
                <span class="col-xs-12 mo-block-number">{{isset($countRemainingUsers)?$countRemainingUsers:0}}</span>
                <span class="col-xs-12 mo-block-txt text-capitalize">users</span>
                    <span class="col-xs-12 text-center mo-arrow-focus">
                        <img src="{{ asset('images/mo-admin/svg/main/grey-triangle.svg') }}" alt="Grey Triangle " onerror="this.onerror=null; this.src='{{ asset('images/mo-admin/main/grey-triangle.png') }}'">
                    </span>
            </div>
        </div>

        <input type="hidden" id="mo-role-id">
    </div>
    {{--End - moto Tabs Section--}}

    {{--Start - moto Interaction Content Section--}}
    <div class="mo-row mo-form-wrapper margin-top-32 hidden mo-block-list" id="mo-user-list">
        <div class="col-xs-12 col-sm-6 padding-bottom-40">
            <span class="text-capitalize mo-block-heading">user</span>
        </div>
        {{--Start - moto Search Section--}}
        <div class="col-xs-12 col-sm-4 float-right no-padding">
            {{ csrf_field() }}
            <div class="col-xs-12 no-padding mo-search-wrapper">
                <input type="text" placeholder="Search" id="mo-txt-search">
                <span class="glyphicon glyphicon-search mo-txt-search cursor-pointer"></span>
            </div>
        </div>
        {{--End - moto Search Section--}}
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="mo-row margin-left-20 mo-filter">
                <input type="checkbox" class="mo-round-check" id="mo-filter-user" name="" />
                <label for="mo-filter-user" class="text-capitalize"><span></span>View active Users only.</label>
                <span></span>
            </div>
            <table id="mo-table-user-list" class="table" cellspacing="0">
                <thead>
                    <tr>
                        <th>Dealergroup</th>
                        <th>Dealership</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Role</th>
                        <th>Last Login</th>
                        <th>Last Logout</th>
                        <th>Location</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
            <ul class="pagination">
                <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Next Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
            </ul>
        </div>
        <div class="mo-round-btn"><a href="{!! url('mo-admin/user/create'); !!}">NEW</a></div>
    </div>
    {{--End - moto Interaction Content Section--}}

    {{------------ SUSPEND CONFIRMATION POPUP ------------}}
    <div id="mo-suspend-popup" class="mo-white-popup mfp-hide">
        <form class="mo-popup-form">
            {{ csrf_field() }}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">suspend account</span>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row padding-top-24 padding-bottom-24">
                <div class="form-group col-xs-12 no-padding">
                    <span class="suspend-popup-content">Are you sure you want to suspend this account?</span>
                </div>
            </div>
            <hr class="mo-form-hr" />
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="confirm" id="mo-user-change-status">
                </div>
            </div>
        </form>
        <div class="mo-loading-image hidden">Loading&#8230;</div>
    </div>
    {{--End - moto Popup Section--}}
@endsection