@extends('mo-admin.layouts.master')

@section('title', 'moto - Trading - Stock List')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row padding-bottom-24">
        <div class="col-xs-12 col-sm-7 no-padding" id="mo-content-header">
            {{ $header or 'Trading' }} > {{ $breadcrumb or 'Live Auction' }}
        </div>
        {{--Start - moto Search Section--}}
        <div class="col-xs-12 col-sm-5 no-padding">
            {{ csrf_field() }}
            <div class="col-xs-5 col-sm-5 no-padding font-size36">
                <input type="hidden" value="{{$live_auction_seconds_timer or ''}}" name="live_auction_seconds_timer" />
                <input type="hidden" value="{{$up_next_auction_session_seconds or ''}}" name="up_next_auction_session_seconds" />
                @if(isset($live_auction_timer))
                    <div class="live-auction-timer font-family-bold">{{ $live_auction_timer or '' }}</div>
                @endif
            </div>
            <div class="col-xs-7 no-padding mo-search-wrapper">
                {{--<form action="" method="POST" id="live-auction-list-search-form">--}}
                    {{ csrf_field() }}
                    <input type="text" placeholder="Search" class="width-100-percent-force" id="mo-txt-search" name="mo-live-auction-search">
                    <span class="glyphicon glyphicon-search mo-txt-search cursor-pointer"></span>
                {{--</form>--}}    
            </div>
        </div>
        {{--End - moto Search Section--}}
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - VEHICLE GRID--}}
    <div class="mo-row" id="mo-vehicle-grid-wrapper">
        <div id="mo-live-auction-content">

            @if(isset($live_auction_vehicle_list) && !empty($live_auction_vehicle_list))
                <div id="mo-request-list-content">
                    @include('mo-admin.main.trading.live-auction-sample-list')
                </div>
            @elseif (empty($live_auction_vehicle_list) && isset($live_auction_seconds_timer))
                <div class="col-xs-12 text-center text-uppercase font-family-bold">there are no vehicles at this time</div> 
            @elseif (!isset($live_auction_seconds_timer))
                @if (isset($up_next_auction_session_h_m_s))
                <div class="col-xs-12 text-center text-uppercase font-family-bold">upcoming auction - <span id="next-auction-session-timer">{{ $up_next_auction_session_h_m_s }}</span></div>
                @else
                    <div class="col-xs-12 text-center text-uppercase font-family-bold">there is no auction session at this time</div> 
                @endif     
            @endif

        </div>
        {{--<div class="mo-row text-center mo-loading-wrapper">
            <img class="mo-loading-icon" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
        </div>--}}
    </div>
    {{--End - moto VEHICLE GRID--}}
    <div class="mo-loading-image hidden">Loading&#8230;</div>


    {{------------ MAX BID POPUP ------------}}
    <div id="mo-auction-max-bid-popup" class="mo-white-popup mfp-hide auction-max-bid-popup force-no-padding">
        <form class="mo-popup-form font-size14" id="mo-auction-max-bid-form">
            {{ csrf_field() }}
            {{----------HEADER - TITLE----------}}
            <div class="mo-row padding-bottom-15 padding-top-15 border-bottom text-align-center">
                <span class="text-capitalize mo-popup-heading">max bid</span>
            </div>
            
            {{----------CONTENT----------}}
            <div class="mo-row padding-top-24 padding-bottom-24 padding-left-30 padding-right-30 text-align-center font-size20 font-grey-color border-bottom">
                    Enter max value for auto bid.
                    The system will bid for you until it reaches this value.   
            </div>
            <div class="mo-row text-align-center padding-top-24 padding-left-20 padding-right-20">
                <input type="number" id="live-auction-max-bid-input" name="live-auction-max-bid-input"/>
            </div>
            {{----------FOOTER - BUTTONS----------}}
            
            <div class="">
                <div class="col-xs-12 padding-top-24 text-right no-padding background-white">
                    <input type="button" class="text-uppercase col-xs-6 live-auction-cancel-btn" value="cancel">
                    <input type="button" class="text-uppercase col-xs-6" value="submit" id="live-auction-set-max-bid-btn">
                </div>
                <input type="hidden" id="mo-offer-id">
            </div>
            
            
        </form>
    </div>
    {{------------ end MAX BID POPUP ------------}}

    {{------------ QUICK BID POPUP ------------}}
    <div id="mo-auction-quick-bid-popup" class="mo-white-popup mfp-hide auction-max-bid-popup force-no-padding">
        <form class="mo-popup-form font-size14" id="mo-auction-quick-bid-form">
            <input type="hidden" value="{{isset($auction_session_id) ? $auction_session_id : ''}}" name="auction_session_id" />
            {{ csrf_field() }}
            {{----------HEADER - TITLE----------}}
            <div class="mo-row padding-bottom-15 padding-top-15 border-bottom text-align-center">
                <span class="text-capitalize mo-popup-heading">quick bid</span>
            </div>
            
            {{----------CONTENT----------}}
            <div class="mo-row padding-top-24 padding-bottom-24 padding-left-30 padding-right-30 text-align-center font-size20 font-grey-color">
                <div class="live-auction-quick-bid-desc">Place bid at $<span id="live-auction-place-bid-number"></span></div>
                <span class="alert alert-danger hidden invalid-bid-step">Price Increment Has Changed!</span>
                <span class="alert alert-danger hidden too_low">Bid Price Is Old!</span>
            </div>
            
            {{----------FOOTER - BUTTONS----------}}
            
            <div class="">
                <div class="col-xs-12 padding-top-24 text-right no-padding background-white">
                    <input type="button" class="text-uppercase col-xs-6 live-auction-cancel-btn" value="cancel">
                    <input type="button" class="text-uppercase col-xs-6" value="submit" id="live-auction-quick-bid-btn">
                </div>
                <input type="hidden" id="mo-offer-id">
            </div>
            
            
        </form>
    </div>
    {{------------ end QUICK BID POPUP ------------}}

    {{--End - moto Popup Section--}}
@endsection