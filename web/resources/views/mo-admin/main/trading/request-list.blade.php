@extends('mo-admin.layouts.master')

@section('title', 'moto - Main')

@section('content')

   {{--Start - moto Content Header Section--}}
    <div class="mo-row padding-bottom-24">
        <div class="col-xs-12 col-sm-8 no-padding" id="mo-content-header">
            {{ $header or 'Auction' }} > {{ $breadcrumb or 'Request List' }}
        </div>
        {{--Start - moto Search Section--}}
        <div class="col-xs-12 col-sm-4 no-padding">
            {{ csrf_field() }}
            <div class="col-xs-12 no-padding mo-search-wrapper">
                <form action="" method="POST" id="request-list-search-form">
                    {{ csrf_field() }}
                    <input type="text" placeholder="Search" id="mo-txt-search" name="mo-request-list-search-input" value="{{isset($query) ? $query : ""}}">
                    <span class="glyphicon glyphicon-search mo-txt-search mo-request-list-search cursor-pointer"></span>
                </form>
            </div>
        </div>
        {{--End - moto Search Section--}}
    </div>
    {{--End - moto Content Header Section--}}

    {{--Start - REQUEST LIST GRID --}}
    <div class="mo-row" id="mo-request-list-grid-wrapper">
        <div id="mo-request-list-content">
        @if(isset($request_list) && !empty($request_list))
            @include("mo-admin.main.trading.request-sample-list")
        @else
            <div class="col-xs-12 text-center text-uppercase font-family-bold">there are no items to display at this time</div>
        @endif
    </div>

        
    </div>
    {{--End - moto REQUEST LIST GRID --}}

    {{------------ APPROVE REQUEST LIST POPUP ------------}}
    <div id="mo-approve-request-list-popup" class="mo-white-popup approve-request-list-popup mfp-hide">
        <form class="mo-popup-form font-size14" id="mo-send-to-auction-form">
            {{ csrf_field() }}
            {{----------HEADER - TITLE----------}}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">select auctions</span>
            </div>
            <hr class="mo-form-hr" />
            {{----------CONTENT----------}}
            <div class="mo-row padding-top-24 padding-bottom-24">
                {{-- auction sessions --}}
                <table id="mo-table-approve-request-list" class="table" cellspacing="0">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Duration</th>
                            <th>Bid Increment</th>
                            <th>Vehicles</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                {{-- pagination --}}
                <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
                    <ul class="pagination">
                        <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                        <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
                    </ul>
                </div>
                {{-- end pagination --}}
            </div>
            <hr class="mo-form-hr" />
            {{----------FOOTER - BUTTONS----------}}
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="submit" id="mo-request-list-select-auction-session-btn">
                </div>
            </div>
            <input type="hidden" id="mo-offer-id">
        </form>
    </div>
    {{------------ End - APPROVE REQUEST LIST POPUP ------------}}

    {{------------  REJECT REQUEST LIST POPUP ------------}}
    <div id="mo-reject-request-list-popup" class="mo-white-popup reject-request-list-popup mfp-hide">
        <form class="font-size14" id="mo-request-list-reject-form">
            {{ csrf_field() }}
            {{----------HEADER - TITLE----------}}
            <div class="mo-row padding-bottom-20">
                <span class="text-capitalize mo-popup-heading">comment</span>
            </div>
            <hr class="mo-form-hr" />
            {{----------CONTENT----------}}

            <div class="form-group col-xs-12 col-sm-8 padding-top-24">
                <label for="selection">Reason</label>
                <div class="mo-dropdown-wrapper">
                    <span class="mo-icon-swipe glyphicon glyphicon-menu-down float-right"></span>
                    <select class="form-control" name="request-list-reject-reason" id="request-list-reject-reason" >
                        @if(isset($reject_reason_list) && !empty($reject_reason_list))
                            @foreach ($reject_reason_list as $reason_list)
                                <option value="{{$reason_list->code}}" >{{ $reason_list->value }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>

            <div class="form-group col-xs-12 padding-top-24">
                <label for="note">Comment*</label>
                <textarea placeHolder="Comment" class="mo-textarea form-control box-shadow-none" rows="5" id="request-list-reject-comment" name="request-list-reject-comment"></textarea>
            </div>

            <div class="mo-row"></div>
            <hr class="mo-form-hr" />
            {{----------FOOTER - BUTTONS----------}}
            <div class="mo-row">
                <div class="col-xs-12 padding-top-24 text-right">
                    <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="cancel">
                    <input type="button" class="mo-btn text-uppercase" value="submit" id="mo-request-list-reject-btn">
                </div>
            </div>
            <input type="hidden" id="mo-offer-id">
        </form>
    </div>
    {{------------ End - REJECT REQUEST LIST POPUP ------------}}

    {{--End - moto Popup Section--}}
    <div class="mo-loading-image hidden">Loading&#8230;</div>

@endsection