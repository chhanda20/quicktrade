@extends('mo-admin.layouts.master')

@section('title', 'moto - Main')

@section('content')

    <div class="mo-row">
        <div class="col-xs-12 col-sm-8 no-padding" id="mo-content-header">
            <a href="{!! url('mo-admin/trading/auction-session-list'); !!}">{{ $header or 'Auction'}}</a> > {{ $breadcrumb or 'Auctions Session'}}
        </div>
        <div class="mo-row mo-form-wrapper margin-top-16">

            @if (empty($historyList))
                <div class="col-xs-12 col-sm-12 vehicle_title">No auction histories found</div>
            @else
                @foreach ($historyList as $auction)
                    <div class="history-wrapper">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 vehicle_title">{{ $auction->vehicle_name }}</div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-3" id="mo-vehicle-add-photo" class="mo-vehicle-add-photo">
                                {{ csrf_field() }}
                                {{--<input type="hidden" value="{{isset($valuation_current_version) ? $valuation_current_version : null}}" name="valuation_version" />--}}
                                <div id="mo-vehicle-image-wrapper" class="vertical-center">
                                    <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{isset($auction->vehicle_id) ? $auction->vehicle_id : ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/img-add-photo@3x.png')}}'" class="img-responsive">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-4">
                                <div class="detail_row">
                                    <div class="info">buy now price</div>
                                    <div class="value">{{ $auction->buy_now_price }}</div>
                                </div>
                                @if(!empty($auction->final_price) && !empty($auction->buyer))
                                    <div class="detail_row">
                                        <div class="info">final price</div>
                                        <div class="value">{{ $auction->final_price }}</div>
                                    </div>
                                    <div class="detail_row">
                                        <div class="info">buyer</div>
                                        <div class="value">{{ $auction->buyer }}</div>
                                    </div>
                                @endif
                            </div>

                            @if ($auction->bids)
                                <div class="col-xs-12 col-sm-5">
                                    <div class="auction_status">Delivered</div>
                                    <a href="#" class="info_btn">bidding history</a>
                                </div>
                            @endif

                        </div>

                        @if ($auction->bids)
                            <div class="bidding_history">
                                <div class="row margin-top-24 margin-bottom-16">
                                    <div class="col-xs-12 font-weight-bold text-capitalize">bidding history</div>
                                </div>

                                @foreach ($auction->bids as $bid)
                                    <div class="row margin-bottom-5">
                                        <div class="col-xs-12 col-sm-2">{{ $bid->location }}</div>
                                        <div class="col-xs-12 col-sm-1 font-weight-bold">{{ $bid->price }}</div>
                                        {{--<div class="col-xs-12 col-sm-3">{{ $bid->time }}</div>--}}
                                        <div class="col-xs-12 col-sm-3">{{ date("H:i:s d-m-Y", strtotime($bid->time)) }}</div>
                                    </div>
                                @endforeach

                            </div>
                            <!-- .bidding_history -->
                        @endif

                    </div>
                    <!-- .history-wrapper -->
                @endforeach
            @endif
        </div>
    </div>
@endsection