@extends('mo-admin.layouts.master')

@section('title', 'moto - Main')

@section('content')

    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            {{ $header or 'Settings'}} > <span class="yellow-text">{{ $breadcrumb or 'Auction'}}</span>
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
            <form action="{!! url('mo-admin/update-trading-setting') !!}" method="post" id="mo-trading-setting-form">
                {{csrf_field()}}
                <div class="col-xs-12 col-sm-6 alert alert-success hidden" role="alert">
                    You've successfully set auction setting for trading!
                    <a class="mo-close-message close">&times;</a>
                </div>

                <div class="col-xs-12 col-sm-6 alert alert-danger hidden" role="alert">
                    {{--There are an active auction session--}}
                    <a class="mo-close-message close">&times;</a>
                </div>

                <div class="mo-row padding-bottom-24">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <label for="default-duration" class="text-capitalize">default duration (minutes)</label>
                        <input type="number" class="form-control" id="default-duration" name="default_duration"
                               value="{{isset($result->auction_default_duration)?$result->auction_default_duration:''}}">
                    </div>
                </div>
                <div class="mo-row padding-bottom-24">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <label for="price-increment" class="text-capitalize">price increment ($)</label>
                        <input type="number" class="form-control" id="price-increment" name="price_increment"
                               value="{{isset($result->auction_price_increment)?$result->auction_price_increment:''}}">
                    </div>
                </div>
                <div class="mo-row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <input type="submit" class="mo-btn text-uppercase" id="auction-save-setting" value="save" />
                    </div>
                </div>
                <div class="mo-loading-image hidden">Loading&#8230;</div>
            </form>
        </div>
    </div>

@endsection