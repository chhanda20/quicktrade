
@if(isset($live_auction_vehicle_list) && !empty($live_auction_vehicle_list))
    @foreach($live_auction_vehicle_list as $live_auction_vehicle)
        <div class="col-xs-12 col-sm-6 col-md-4 padding-bottom-20">
            <div class="mo-row mo-vehicle-wrapper">
                <div class="col-xs-12 no-padding mo-image-wrapper vertical-center">
                    <label>
                        <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{isset($live_auction_vehicle->vehicle_id) ? $live_auction_vehicle->vehicle_id : ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'" class="img-responsive">
                    </label>
                </div>
                <div class="col-xs-12 mo-content-wrapper padding-lr10">
                    <div class="mo-row col-xs-12 border-bottom padding-tb10 font-weight-bold mo-vehicle-name">
                        {{$live_auction_vehicle->vehicle_name}}
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="col-xs-12 no-padding">
                            <span class="col-xs-6 no-padding">
                                @if($live_auction_vehicle->quick_seller === true)
                                    <img src="{{asset('images/main/speedy-seller.png')}}" alt="speedy seller">
                                @endif
                            </span>
                            <div class="col-xs-6 no-padding text-right">
                                <div class="text-capitalize col-xs-12 no-padding">Seller Rating:</div>
                                <span class="starRating float-right pointer-none col-xs-12 no-padding">
                                    @if($live_auction_vehicle->seller_rating != 0)
                                        @for($i = 5; $i >0; $i--)
                                            <input id="rating-{{$i}}-{{$live_auction_vehicle->id}}" type="radio" name="rating_{{$live_auction_vehicle->id}}" value="{{$i}}" {{$live_auction_vehicle->seller_rating == $i?"checked":""}}>
                                            <label for="seller-rating-{{$i}}">{{$i}}</label>
                                        @endfor
                                    @else
                                        Not Rated
                                    @endif
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Location:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-days-stock">{{$live_auction_vehicle->location_text}}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Odometer:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-stock-no">{{$live_auction_vehicle->odometer}}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Car Rating:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold ">{{ $live_auction_vehicle->overall_rating }}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Total Recon. Cost:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-reg-no">${{isset($live_auction_vehicle->total_recon_cost) ? number_format($live_auction_vehicle->total_recon_cost, 2) : 0}}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Current Bid:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-total-cost" id="current_bid_{{isset($live_auction_vehicle->id) ? $live_auction_vehicle->id : "" }}">
                            <span class="hidden fa fa-trophy live-auction-trophy {{Session::get('id') == $live_auction_vehicle->bid_user_id ? 'win' : ''}}"></span>
                            $<span class="live-auction-bid-price">{{isset($live_auction_vehicle->bid_price) ? number_format($live_auction_vehicle->bid_price, 2) : number_format(0.00, 2)}}</span>
                            
                        </div>
                    </div>
                    <div class="mo-row col-xs-12 no-padding padding-top-20 padding-bottom-20 hidden">
                        <div class="col-xs-6 no-padding text-uppercase font-weight-bold yellow-text">
                            <a href="#mo-auction-quick-bid-popup" class="mo-live-auction-popup quick-bid-text"
                                id="quick_bid_{{isset($live_auction_vehicle->id) ? $live_auction_vehicle->id : "" }}"
                                data-quick-bid="{{isset($live_auction_vehicle->bid_price) ? $live_auction_vehicle->bid_price + $auction_price_increment : 0}}"
                                data-auction-price-increment="{{isset($auction_price_increment) ? $auction_price_increment : 0}}"
                                data-auction-item-id="{{isset($live_auction_vehicle->id) ? $live_auction_vehicle->id : "" }}"
                                >quick bid</a>
                        </div>
                        <div class="col-xs-6 no-padding text-uppercase font-weight-bold yellow-text text-right hidden">
                            <a href="#mo-auction-max-bid-popup" class="mo-live-auction-popup max-bid-text"
                                data-quick-bid="{{isset($live_auction_vehicle->bid_price) ? $live_auction_vehicle->bid_price + $auction_price_increment : 0}}"
                                data-auction-price-increment="{{isset($auction_price_increment) ? $auction_price_increment : 0}}"
                                data-auction-item-id="{{isset($live_auction_vehicle->id) ? $live_auction_vehicle->id : "" }}"
                            >max bid</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    @endforeach
    <div class="mo-row text-center mo-loading-wrapper">
        <img class="mo-loading-icon" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
    </div>
@else
    <div class="col-xs-12 text-center text-uppercase font-family-bold">there are no items to display at this time</div>
@endif
