@extends('mo-admin.layouts.master')

@section('title', 'moto - Main')

@section('content')

    <div class="mo-row">
        <div class="col-xs-12 col-sm-8 no-padding" id="mo-content-header">
            <a href="{!! url('mo-admin/trading/auction-session-list'); !!}">{{ $header or 'Auction'}}</a> > {{ $breadcrumb or 'Auctions Session'}}
        </div>
        <div class="col-xs-12 col-sm-4 no-padding">
            {{ csrf_field() }}
            {{--<div class="col-xs-12 no-padding mo-search-wrapper">--}}
                {{--<input type="text" placeholder="Search" id="mo-txt-search">--}}
                {{--<span class="glyphicon glyphicon-search mo-txt-search cursor-pointer"></span>--}}
            {{--</div>--}}
        </div>
    </div>
    <div class="mo-row padding-top-24 percent-h-100">
        <div class="mo-row mo-form-wrapper percent-h-100">
            <table id="mo-auction-session-history-list" class="table" cellspacing="0">
                <thead>
                <tr>
                    {{--<th>First Name <span class="float-right"><img src="{{ asset('images/svg/main/icon-swipe-bottom.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-swipe-bottom.png') }}'"></span></th>--}}
                    <th></th>
                    <th>Auction Name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Duration</th>
                    <th>Vehicles</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center mo-pagination-wrapper">
                <ul class="pagination">
                    <li class="mo-page-number-list"><a href="#" id="mo-page-prev"><img src="{{ asset('images/svg/main/icon-prev.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-prev.png') }}'"></a></li>
                    <li><a href="#" id="mo-page-next"><img src="{{ asset('images/svg/main/icon-next.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/icon-next.png') }}'"></a></li>
                </ul>
            </div>
            <div class="mo-round-btn hidden"><a href="create-auction-session/new">NEW</a></div>
        </div>
    </div>

@endsection