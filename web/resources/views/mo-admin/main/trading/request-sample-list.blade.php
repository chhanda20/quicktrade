@if(isset($request_list) && !empty($request_list))
    @foreach($request_list as $request)

        <div class="col-xs-12 col-sm-6 col-md-4 padding-bottom-20">
            <div class="mo-row mo-vehicle-wrapper">
                <div class="col-xs-12 no-padding mo-image-wrapper vertical-center">
                    <label>
                        <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{isset($request->vehicle_id) ? $request->vehicle_id : ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available@3x.png')}}'" class="img-responsive">
                    </label>
                </div>
                <div class="col-xs-12 mo-content-wrapper padding-lr10">
                    <div class="mo-row col-xs-12 border-bottom padding-tb10 font-weight-bold mo-vehicle-name">
                        {{$request->vehicle_name}}
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding text-capitalize">Start price:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-days-stock">{{number_format($request->start_price, 2)}}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Buy now price:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-days-stock">{{number_format($request->buy_now_price, 2)}}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Dealer:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-stock-no">{{$request->dealer_name}}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Location:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-reg-no">{{$request->location_text}}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Odometer:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-reg-no">{{$request->odometer}}</div>
                    </div>
                    <div class="mo-row col-xs-12 border-bottom padding-tb10">
                        <div class="text-capitalize col-xs-6 no-padding">Rating:</div>
                        <div class="text-capitalize col-xs-6 no-padding text-right font-weight-bold mo-vehicle-reg-no">{{$request->rating}}</div>
                    </div>

                    <div class="mo-row col-xs-12 no-padding padding-top-20 padding-bottom-15">
                        <div class="col-xs-6 no-padding text-uppercase font-weight-bold yellow-text">
                            <a href="#mo-approve-request-list-popup" class="mo-approve-request-popup request-list-approve-link" data-request-id="{{$request->id}}" data-session-id="{{$request->session_id}}">approve</a>
                        </div>
                        <div class="col-xs-6 no-padding text-uppercase font-weight-bold yellow-text text-right">
                            <a href="#mo-reject-request-list-popup" class="mo-reject-request-popup request-list-reject-link" data-request-id="{{$request->id}}">reject</a>
                        </div>
                    </div>
                    <div class="mo-row col-xs-12 no-padding padding-bottom-6 text-right">
                        <a href="{!! url('/mo-admin/trading/showroom-vehicle-detail/'); !!}/{{$request->vehicle_id}}/{{$request->valuation_id}}">
                            <span class="glyphicon glyphicon-option-horizontal"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <div class="mo-row text-center mo-loading-wrapper">
        <img class="mo-loading-icon" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
    </div>
@endif

