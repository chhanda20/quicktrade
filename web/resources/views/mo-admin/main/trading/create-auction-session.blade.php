@extends('mo-admin.layouts.master')

@section('title', 'moto - Main')

@section('content')
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            Auction > <a href="{!! url('/mo-admin/trading/auction-session-list') !!}"> {{ $header or 'Auctions Session'}} </a> > {{ $breadcrumb or 'New'}}
        </div>
    </div>
    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
<!-- RECURRING -->
            <form action="{!! url('/mo-admin/trading/create-auction-session') !!}" method="post" id="mo-auction-recurring-form">
                {{csrf_field()}}

                <div class="col-xs-12 col-sm-6 alert alert-danger hidden" role="alert">
                    {{--Auction Session Duplicate!--}}
                    <span class="mo-error-message text-capitalize"></span>
                    <a class="mo-close-message close">&times;</a>
                </div>


                <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 mo-form-header">
                    <input type="radio" class="mo-round-check mo-auction-session" id="recurring" name="recurring" checked>
                    <label for="recurring" class="text-uppercase"><span></span>recurring</label>
                </div>
                <div class="mo-row mo-recurring">
                    <div class="col-xs-12 col-sm-12 col-md-12 padding-bottom-24 padding-left-20">

                        <div class="row padding-bottom-24">
                            <div class="col-xs-12">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <label for="user-code" class="text-capitalize">Session name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="" />

                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <label for="start-from-date" class="text-capitalize">start from*</label>
                            <div class="input-group date" id="start-from-date-picker">
                                <input type="text" class="form-control" id="start-from-date" name="start_date" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            <div id="errorToShow_start_date"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 float-right">
                            <label for="end-at-date" class="text-capitalize">end at*</label>
                            <div class="input-group date" id="end-at-date-picker">
                                <input type="text" class="form-control" id="end-at-date" name="end_date" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            <div id="errorToShow_end_date"></div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 padding-left-20">
                        <div class="col-xs-12 col-sm-1-4">
                            <input type="checkbox" class="mo-square-check mo-week-days" id="mon" name="day[]" value="2"/>
                            <label for="mon" class="text-capitalize"><span></span>mon</label>
                        </div>
                        <div class="col-xs-12 col-sm-1-4">
                            <input type="checkbox" class="mo-square-check mo-week-days" id="tue" name="day[]" value="3"/>
                            <label for="tue" class="text-capitalize"><span></span>tue</label>
                        </div>
                        <div class="col-xs-12 col-sm-1-4">
                            <input type="checkbox" class="mo-square-check mo-week-days" id="wed" name="day[]" value="4"/>
                            <label for="wed" class="text-capitalize"><span></span>wed</label>
                        </div>
                        <div class="col-xs-12 col-sm-1-4">
                            <input type="checkbox" class="mo-square-check mo-week-days" id="thu" name="day[]" value="5"/>
                            <label for="thu" class="text-capitalize"><span></span>thu</label>
                        </div>
                        <div class="col-xs-12 col-sm-1-4">
                            <input type="checkbox" class="mo-square-check mo-week-days" id="fri" name="day[]" value="6"/>
                            <label for="fri" class="text-capitalize"><span></span>fri</label>
                        </div>
                        <div class="col-xs-12 col-sm-1-4">
                            <input type="checkbox" class="mo-square-check mo-week-days" id="sat" name="day[]" value="7"/>
                            <label for="sat" class="text-capitalize"><span></span>sat</label>
                        </div>
                        <div class="col-xs-12 col-sm-1-4">
                            <input type="checkbox" class="mo-square-check mo-week-days" id="sun" name="day[]" value="1"/>
                            <label for="sun" class="text-capitalize"><span></span>sun</label>
                        </div>
                    </div> 

                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="mo-row padding-left-20">
                            <div class="mo-row padding-bottom-24">
                                <label for="start-time" class="text-capitalize">start time*</label>
                                <div class="input-group time" id="start-time-picker">
                                    <input type="text" class="form-control" id="start-time" name="start_time" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                                <div id="errorToShow_start_time"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm -12 col-md-12">
                        <!-- DURATION -->
                        <div class="col-xs-12 col-sm-12 col-md-6 padding-left-20">
                            <div>
                                <label for="duration" class="text-capitalize padding-bottom-20">duration (minutes)*</label>
                            </div>
                            <div class="mo-row padding-bottom-24">
                                <input type="radio" class="mo-round-check mo-recurring-duration" id="checkbox-recurring-default" name="checkbox_recurring_default" checked>
                                <label for="checkbox-recurring-default" class="text-capitalize"><span></span>default</label>
                                <input type="text" class="form-control" id="input-recurring-default" name="duration_default" readonly
                                       value="{{$auction_default_duration or ""}}">
                            </div>
                            <div class="mo-row padding-bottom-24">
                                <input type="radio" class="mo-round-check mo-recurring-duration" id="checkbox-recurring-custom" name="checkbox_recurring_custom">
                                <label for="checkbox-recurring-custom" class="text-capitalize"><span></span>custom</label>
                                <input type="text" class="form-control" id="input-recurring-custom" name="duration_custom" disabled>
                            </div>
                        </div>

                        <!-- BID INCREMENT -->
                        <div class="col-xs-12 col-sm-12 col-md-6 padding-left-20">
                            <div>
                                <label for="bid-increment" class="text-capitalize padding-bottom-20">bid increment ($)</label>
                            </div>
                            <div class="mo-row padding-bottom-24">
                                <input type="radio" class="mo-round-check mo-recurring-bid-increment" id="checkbox-recurring-bid-default" name="checkbox_recurring_bid_default" checked>
                                <label for="checkbox-recurring-bid-default" class="text-capitalize"><span></span>default</label>
                                <input type="text" class="form-control" id="input-recurring-bid-default" name="bid_increment_default" readonly
                                       value="{{$auction_price_increment or ""}}">
                            </div>
                            <div class="mo-row padding-bottom-24">
                                <input type="radio" class="mo-round-check mo-recurring-bid-increment" id="checkbox-recurring-bid-custom" name="checkbox_recurring_bid_custom">
                                <label for="checkbox-recurring-bid-custom" class="text-capitalize"><span></span>custom</label>
                                <input type="text" class="form-control" id="input-recurring-bid-custom" name="bid_increment_custom" disabled>
                            </div>
                        </div>
                    </div>
                </div>
            </form>  <!-- End form -->
                
<!-- AUCTION SESSION AD HOC -->
            @if ($type == 'edit')
                <form action="{!! url('/mo-admin/trading/update-auction-session') !!}/{{$auction_session_detail[0]->id}}" method="post" id="mo-auction-ad-hoc-form">
            @else
                <form action="{!! url('/mo-admin/trading/create-auction-session') !!}" method="post" id="mo-auction-ad-hoc-form">
            @endif    
                {{csrf_field()}}

                <div class="col-xs-12 col-sm-6 alert alert-warning hidden" role="alert">
                    {{--Auction Session Duplicate!--}}
                    <span class="mo-edit-error-message text-capitalize"></span>
                    <a class="mo-close-message close">&times;</a>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-24 mo-ad-hoc-checkbox mo-form-header">
                    <input type="radio" class="mo-round-check mo-auction-session" id="ad-hoc" name="ad_hoc">
                    <label for="ad-hoc" class="text-uppercase"><span></span>ad hoc</label>
                </div>
                <div class="mo-row mo-ad-hoc hidden">

                    <div class="mo-row padding-bottom-24">
                        <div class="col-xs-12">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label for="user-code" class="text-capitalize">Session name</label>
                                <input type="text" class="form-control" id="name" name="name"
                                       value="{{isset($auction_session_detail[0]->name)? $auction_session_detail[0]->name:""}}" />

                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="mo-row padding-left-20">
                            <div class="mo-row padding-bottom-24">
                                <label for="ad-hoc-date" class="text-capitalize">date*</label>
                                <div class="input-group date" id="ad-hoc-date-picker">
                                    <input type="text" class="form-control" id="ad-hoc-date" name="start_date"
                                           value="{{isset($auction_session_detail[0]->date)?$auction_session_detail[0]->date:''}}" />
                                    <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                </div>
                                <div id="errorToShow_date"></div>
                            </div>
                            <div class="mo-row padding-bottom-24">
                                <label for="ad-hoc-time" class="text-capitalize">time*</label>
                                <span>
                                    <img class="mo-auction-start-time imgicon" src="{{asset('images/svg/main/icon-swipe-right-copy-4.svg')}}">
                                </span>
                                <select class="form-control text-capitalize" id="start-time-select" name="start_time">
                                    <option value="{{$auction_session_detail[0]->time or ''}}">{{isset($auction_session_detail[0]->time)?$auction_session_detail[0]->time:""}}</option>
                                    {{--<option value="{{$auction_session_detail[0]->time or ""}}">{{$auction_session_detail[0]->time}}</option>--}}
                                </select>
                                <div id="errorToShow_time"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="col-xs-12 col-sm-12 col-md-6 padding-left-20">
                            <div>
                                <label for="duration" class="text-capitalize padding-bottom-20">duration (minutes)*</label>
                            </div>
                            <div class="mo-row padding-bottom-24">
                                <input type="radio" class="mo-round-check mo-ad-hoc-duration" id="checkbox-ad-hoc-default" name="checkbox_ad_hoc_default" checked>
                                <label for="checkbox-ad-hoc-default" class="text-capitalize"><span></span>default</label>
                                <input type="text" class="form-control" id="input-ad-hoc-default" name="duration_default" readonly
                                       value="{{$auction_default_duration or ""}}">
                            </div>
                            <div class="mo-row padding-bottom-24">
                                <input type="radio" class="mo-round-check mo-ad-hoc-duration" id="checkbox-ad-hoc-custom" name="checkbox_ad_hoc_custom">
                                <label for="checkbox-ad-hoc-custom" class="text-capitalize"><span></span>custom</label>
                                <input type="text" class="form-control" id="input-ad-hoc-custom" name="duration_custom" disabled>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div>
                                <label for="bid-increment" class="text-capitalize padding-bottom-20">bid increment ($)</label>
                            </div>
                            <div class="mo-row padding-bottom-24">
                                <input type="radio" class="mo-round-check mo-ad-hoc-bid-increment" id="checkbox-ad-hoc-bid-default" name="checkbox_ad_hoc_bid_default" checked>
                                <label for="checkbox-ad-hoc-bid-default" class="text-capitalize"><span></span>default</label>
                                <input type="text" class="form-control" id="input-ad-hoc-bid-default" name="bid_increment_default" readonly
                                       value="{{$auction_price_increment or ""}}">
                            </div>
                            <div class="mo-row padding-bottom-24">
                                <input type="radio" class="mo-round-check mo-ad-hoc-bid-increment" id="checkbox-ad-hoc-bid-custom" name="checkbox_ad_hoc_bid_custom">
                                <label for="checkbox-ad-hoc-bid-custom" class="text-capitalize"><span></span>custom</label>
                                <input type="text" class="form-control" id="input-ad-hoc-bid-custom" name="bid_increment_custom" disabled>
                            </div>
                        </div>
                    </div>
                </div>

                <input type="hidden" id="mo-auction-session-id" value="{{isset($auction_session_detail[0]->id)?$auction_session_detail[0]->id:''}}">
            </form> <!-- End form -->

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="mo-row padding-left-20">
                        @if(strtolower($breadcrumb)=="new")
                            <input type="submit" class="mo-btn text-uppercase" value="save" id="auction-create">
                        @elseif(strtolower($breadcrumb) == "edit")
                            <input type="submit" class="mo-btn text-uppercase" value="update" id="auction-update">
                        @endif
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection