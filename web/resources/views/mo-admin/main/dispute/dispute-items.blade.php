<div class="col-xs-12 col-sm-6 col-md-7 no-padding">

    <input type="hidden" id="dispute-id" name="dispute_id" value="{{$disputeInfo->id or ''}}">
    <input type="hidden" id="azure-accout-name" value="{{Config::get('app.azure_storage_account_name')}}" />
    <input type="hidden" id="valuation-id" value="{{$disputeInfo->valuation_id or ''}}">
    <input type="hidden" id="valuation-version" value="{{$disputeInfo->valuation_version or ''}}">

    <div class="col-xs-6 mo-form-header text-uppercase no-padding">DISPUTE ITEMS</div>
    <div class="col-xs-6 text-right mo-form-header no-padding">Total: $<span>{{number_format($totalRepairCost, 2)}}</span></div>
    @if(isset($disputeItems) && is_object($disputeItems) && !empty($disputeItems))
        {{-- start ORGINAL --}}
        @if(isset($disputeItems->original) && !empty($disputeItems->original))
            <?php $original = $disputeItems->original ?>
            <div class="mo-row">
                <div class="mo-row padding-top-16 font-family-bold font-size16">
                    <div class="col-xs-6 no-padding">Original Damages</div>
                    <div class="col-xs-6 text-right no-padding">Total: $<span id="mo-total-original-cost">{{number_format($originalRepairCost, 2)}}</span></div>
                </div>
                {{--in case that general recondition cost exist--}}
                @if(isset($original->EXTERIOR_SECTION) || (isset($disputeInfo->general_recondition_cost) && $disputeInfo->general_recondition_cost != 0))
                {{-- start exterior section --}}
                <div class="col-xs-12 no-padding padding-top-16">
                    <div class="font-weight-bold font-size16 padding-bottom-8">Exterior</div>

                    {{-- general reconditioning cost - 1 means show --}}
                    @if(isset($disputeInfo->general_recondition_cost) && $disputeInfo->general_recondition_cost != 0)
                        <div class="list-divider col-xs-12"></div>
                        <div class="col-xs-12 no-padding height-50 vertical-center">
                            <div class="col-xs-3 no-padding">

                            </div>
                            <div class="col-xs-6">
                                <div class="padding-left-10">
                                    <div class="font-weight-bold text-capitalize">general reconditioning cost</div>
                                </div>
                            </div>
                            <div class="col-xs-3 no-padding text-right font-grey-color">
                                <div class="col-xs-9 no-padding font-weight-bold">
                                    ${{number_format($general_recondition_cost, 2)}}
                                </div>
                                <div class="col-xs-3 no-padding"></div>
                            </div>
                        </div>
                    @endif
                    @if(isset($original->EXTERIOR_SECTION))
                        {{-- exterior item --}}
                        @foreach($original->EXTERIOR_SECTION as $exterior_item)
                            <div class="list-divider col-xs-12"></div>
                            <div class="col-xs-12 no-padding height-100 vertical-center">
                                <div class="col-xs-3 no-padding">
                                    <div class="max-width-100-percent">
                                        <img data-item-id="{{$exterior_item->inspection_item_id}}" src="{{asset("images/main/no_photo_available@2x.png")}}" class="img-responsive mo-item-img max-height-100" />
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <u class="font-family-bold">{{$exterior_item->section_name}}</u>
                                    <div class="padding-left-10">
                                        <div class="font-weight-bold">{{$exterior_item->item}}</div>
                                        <div class="font-grey-color font-style-italic">{{$exterior_item->note}}</div>
                                    </div>
                                </div>
                                <div class="col-xs-3 no-padding text-right font-grey-color">
                                    <div class="col-xs-9 no-padding font-weight-bold">${{number_format($exterior_item->repair_cost, 2)}}</div>
                                    <a href="#mo-view-inspection-item-popup" class="mo-popup col-xs-3 no-padding cursor-pointer font-grey-color-important" id="mo-edit-popup"
                                        data-section="EXTERIOR_SECTION"
                                        data-section-type="{{$exterior_item->section_type}}"
                                        data-inspection-id="{{$exterior_item->inspection_item_id}}"
                                        data-repair-cost="{{$exterior_item->repair_cost}}"
                                        data-note="{{$exterior_item->note}}"
                                        data-image-type="INSPECTION">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                        {{-- exterior item --}}
                    @endif
                    <div class="list-divider col-xs-12"></div>
                </div>
                {{-- end exterior section --}}
                @endif

                @if(isset($original->INTERIOR_SECTION))
                    {{-- start interior/electronics section --}}
                    <div class="col-xs-12 no-padding padding-top-16">
                        <div class="font-weight-bold font-size16 padding-bottom-8">Interior/Electronics</div>

                        {{-- interior item --}}
                        @foreach($original->INTERIOR_SECTION as $interior_item)
                            <div class="list-divider col-xs-12"></div>
                            <div class="col-xs-12 no-padding height-100 vertical-center">
                                <div class="col-xs-3 no-padding">
                                    <div class="max-width-100-percent">
                                        <img data-item-id="{{$interior_item->inspection_item_id}}" src="{{asset("images/main/no_photo_available@2x.png")}}" class="img-responsive mo-item-img max-height-100" />
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="padding-left-10">
                                        <div class="font-weight-bold">{{$interior_item->item}}</div>
                                        <div class="font-grey-color font-style-italic">{{$interior_item->note}}</div>
                                    </div>
                                </div>
                                <div class="col-xs-3 no-padding text-right font-grey-color">
                                    <div class="col-xs-9 no-padding font-weight-bold">${{number_format($interior_item->repair_cost, 2)}}</div>
                                    <a href="#mo-view-inspection-item-popup" class="mo-popup col-xs-3 no-padding cursor-pointer font-grey-color-important" id="mo-edit-popup"
                                       data-section="INTERIOR_SECTION"
                                       data-section-type="{{$interior_item->section_type}}"
                                       data-inspection-id="{{$interior_item->inspection_item_id}}"
                                       data-repair-cost="{{$interior_item->repair_cost}}"
                                       data-note="{{$interior_item->note}}"
                                       data-image-type="INSPECTION">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                        <div class="list-divider col-xs-12"></div>
                        {{-- interior item --}}
                    </div>
                    {{-- end interior/electronics section --}}
                @endif

                @if(isset($original->MECHANICAL_SECTION))
                    {{-- start mechanical section --}}
                    <div class="col-xs-12 no-padding padding-top-16">
                        <div class="font-weight-bold font-size16 padding-bottom-8">Mechanical</div>

                        {{-- mechanical item --}}
                        @foreach($original->MECHANICAL_SECTION as $mechanical_item)
                            <div class="list-divider col-xs-12"></div>
                            <div class="col-xs-12 no-padding height-100 vertical-center">
                                <div class="col-xs-3 no-padding">
                                    <div class="max-width-100-percent">
                                        <img data-item-id="{{$mechanical_item->inspection_item_id}}" src="{{asset("images/main/no_photo_available@2x.png")}}" class="img-responsive mo-item-img max-height-100" />
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="padding-left-10">
                                        <div class="font-weight-bold">{{$mechanical_item->item}}</div>
                                        <div class="font-grey-color font-style-italic">{{$mechanical_item->note}}</div>
                                    </div>
                                </div>
                                <div class="col-xs-3 no-padding text-right font-grey-color">
                                    <div class="col-xs-9 no-padding font-weight-bold">${{number_format($mechanical_item->repair_cost, 2)}}</div>
                                    <a href="#mo-view-inspection-item-popup" class="mo-popup col-xs-3 no-padding cursor-pointer font-grey-color-important" id="mo-edit-popup"
                                       data-section="MECHANICAL_SECTION"
                                       data-section-type="{{$mechanical_item->section_type}}"
                                       data-inspection-id="{{$mechanical_item->inspection_item_id}}"
                                       data-repair-cost="{{$mechanical_item->repair_cost}}"
                                       data-note="{{$mechanical_item->note}}"
                                       data-image-type="INSPECTION">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                        <div class="list-divider col-xs-12"></div>
                        {{-- mechanical item --}}
                    </div>
                    {{-- end mechanical section --}}
                @endif
            </div>
        @endif
        {{-- end ORGINAL --}}

        {{-- start ADDITIONAL --}}
        @if(isset($disputeItems->additional) && !empty($disputeItems->additional))
            <?php $additional = $disputeItems->additional ?>
            <div class="mo-row padding-top-36">
                <div class="mo-row font-family-bold font-size16">
                    <div class="col-xs-6 no-padding">Additional Damages</div>
                    <div class="col-xs-6 text-right no-padding">Total: $<span id="mo-total-additional-cost">{{number_format($additionalRepairCost, 2)}}</span></div>
                </div>
                @if(isset($additional->EXTERIOR_SECTION))
                    {{-- start exterior section --}}
                    <div class="col-xs-12 no-padding padding-top-16">
                        <div class="font-weight-bold font-size16 padding-bottom-8">Exterior</div>

                        {{-- exterior item --}}
                        @foreach($additional->EXTERIOR_SECTION as $exterior_item)
                            <div class="list-divider col-xs-12"></div>
                            <div class="col-xs-12 no-padding height-100 vertical-center">
                                <div class="col-xs-3 no-padding">
                                    <div class="max-width-100-percent">
                                        <img data-item-id="{{$exterior_item->inspection_item_id}}" src="{{asset("images/main/no_photo_available@2x.png")}}" class="img-responsive mo-dispute-item-img max-height-100" />
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <u class="font-family-bold">{{$exterior_item->section_name}}</u>
                                    <div class="padding-left-10">
                                        <div class="font-weight-bold">{{$exterior_item->item}}</div>
                                        <div class="font-grey-color font-style-italic">{{$exterior_item->note}}</div>
                                    </div>
                                </div>
                                <div class="col-xs-3 no-padding text-right font-grey-color">
                                    <div class="col-xs-9 no-padding font-weight-bold">${{number_format($exterior_item->repair_cost, 2)}}</div>
                                    <a href="#mo-view-inspection-item-popup" class="mo-popup col-xs-3 no-padding cursor-pointer font-grey-color-important" id="mo-edit-popup"
                                       data-section="EXTERIOR_SECTION"
                                       data-section-type="{{$exterior_item->section_type}}"
                                       data-inspection-id="{{$exterior_item->inspection_item_id}}"
                                       data-repair-cost="{{$exterior_item->repair_cost}}"
                                       data-note="{{$exterior_item->note}}"
                                       data-image-type="DISPUTE">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                        <div class="list-divider col-xs-12"></div>
                        {{-- exterior item --}}
                    </div>
                    {{-- end exterior section --}}
                @endif

                @if(isset($additional->INTERIOR_SECTION))
                    {{-- start interior/electronics section --}}
                    <div class="col-xs-12 no-padding padding-top-16">
                        <div class="font-weight-bold font-size16 padding-bottom-8">Interior/Electronics</div>

                        {{-- interior item --}}
                        @foreach($additional->INTERIOR_SECTION as $interior_item)
                            <div class="list-divider col-xs-12"></div>
                            <div class="col-xs-12 no-padding height-100 vertical-center">
                                <div class="col-xs-3 no-padding">
                                    <div class="max-width-100-percent">
                                        <img data-item-id="{{$interior_item->inspection_item_id}}" src="{{asset("images/main/no_photo_available@2x.png")}}" class="img-responsive mo-dispute-item-img max-height-100" />
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="padding-left-10">
                                        <div class="font-weight-bold">{{$interior_item->item}}</div>
                                        <div class="font-grey-color font-style-italic">{{$interior_item->note}}</div>
                                    </div>
                                </div>
                                <div class="col-xs-3 no-padding text-right font-grey-color">
                                    <div class="col-xs-9 no-padding font-weight-bold">${{number_format($interior_item->repair_cost, 2)}}</div>
                                    <a href="#mo-view-inspection-item-popup" class="mo-popup col-xs-3 no-padding cursor-pointer font-grey-color-important" id="mo-edit-popup"
                                       data-section="INTERIOR_SECTION"
                                       data-section-type="{{$interior_item->section_type}}"
                                       data-inspection-id="{{$interior_item->inspection_item_id}}"
                                       data-repair-cost="{{$interior_item->repair_cost}}"
                                       data-note="{{$interior_item->note}}"
                                       data-image-type="DISPUTE">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                        <div class="list-divider col-xs-12"></div>
                        {{-- interior item --}}
                    </div>
                    {{-- end interior/electronics section --}}
                @endif

                @if(isset($additional->MECHANICAL_SECTION))
                    {{-- start mechanical section --}}
                    <div class="col-xs-12 no-padding padding-top-16">
                        <div class="font-weight-bold font-size16 padding-bottom-8">Mechanical</div>

                        {{-- interior item --}}
                        @foreach($additional->MECHANICAL_SECTION as $mechanical_item)
                            <div class="list-divider col-xs-12"></div>
                            <div class="col-xs-12 no-padding height-100 vertical-center">
                                <div class="col-xs-3 no-padding">
                                    <div class="max-width-100-percent">
                                        <img data-item-id="{{$mechanical_item->inspection_item_id}}" src="{{asset("images/main/no_photo_available@2x.png")}}" class="img-responsive mo-dispute-item-img max-height-100" />
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="padding-left-10">
                                        <div class="font-weight-bold">{{$mechanical_item->item}}</div>
                                        <div class="font-grey-color font-style-italic">{{$mechanical_item->note}}</div>
                                    </div>
                                </div>
                                <div class="col-xs-3 no-padding text-right font-grey-color">
                                    <div class="col-xs-9 no-padding font-weight-bold">${{number_format($mechanical_item->repair_cost, 2)}}</div>
                                    <a href="#mo-view-inspection-item-popup" class="mo-popup col-xs-3 no-padding cursor-pointer font-grey-color-important" id="mo-edit-popup"
                                       data-section="MECHANICAL_SECTION"
                                       data-section-type="{{$mechanical_item->section_type}}"
                                       data-inspection-id="{{$mechanical_item->inspection_item_id}}"
                                       data-repair-cost="{{$mechanical_item->repair_cost}}"
                                       data-note="{{$mechanical_item->note}}"
                                       data-image-type="DISPUTE">
                                        <span class="glyphicon glyphicon-info-sign"></span>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                        <div class="list-divider col-xs-12"></div>
                        {{-- mechanical item --}}
                    </div>
                    {{-- end mechanical section --}}
                @endif
            </div>
        @endif
        {{-- end ADDITIONAL --}}
    @endif

    <div class="col-xs-12 no-padding text-right padding-top-30">
        <input type="button" value="view valuation" id="mo-view-valuation" class="mo-dispute-btn" />
    </div>
</div>

{{----------------------------------------------------------- EDIT INSPECTION ITEM POPUP -----------------------------------------------------------}}
<div id="mo-view-inspection-item-popup" class="mo-white-popup mfp-hide">
    <form class="mo-exterior-popup-form" action="/upload-image" method="post" enctype="multipart/form-data" id="mo-exterior-popup-form" >
        {{ csrf_field() }}
        <input type="hidden" value="" id="inspection-item-id" name="exterior-item-id">
        <div class="mo-row padding-bottom-20">
            <span class="text-capitalize mo-popup-heading"><span>View</span> Inspection item</span>
        </div>
        <hr class="mo-form-hr" />
        <div class="mo-row mo-exterior-popup-content padding-top-24 padding-bottom-20">
            <div class="form-group col-xs-12 col-sm-8">
                <label for="selection">Section</label>
                <div class="mo-dropdown-wrapper">
                    <span class="mo-icon-swipe glyphicon glyphicon-menu-down float-right"></span>
                    <select class="form-control mo-inspection-popup-selectbox" name="exterior-section" id="inspection-sections-select" disabled>
                        <option disabled selected>-- Sections list --</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-xs-12 col-sm-8">
                <label for="item">Item</label>
                <div class="mo-dropdown-wrapper">
                    <span class="mo-icon-swipe glyphicon glyphicon-menu-down float-right"></span>
                    <select class="form-control mo-inspection-popup-selectbox" name="exterior-items" id="inspection-items-select" disabled>
                        <option selected disabled>-- Items list --</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-xs-12 col-sm-8">
                <label for="estimate-cost">Estimate Cost</label>
                <input type="number" pattern="[0-9]*" value="0" name="estimate_cost" id="repair-cost" class="form-control mo-inspection-popup-textbox disabled-div" maxlength="13" {{--onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46"--}}/> <br/>
            </div>
            <div class="form-group col-xs-12">
                <div class="col-xs-4 mo-inspection-upload-image-box no-padding" id="mo-image-box-1">
                    <label for="fileToUpload" class="mo-inspection-input-file-label vertical-center" id="ImageBrowse">
                        <img src="" alt="Upload" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available.png') }}'" class="mo-inspection-upload-image-icon img-responsive" />
                    </label>

                </div>
                <div class="col-xs-4 mo-inspection-upload-image-box no-padding" id="mo-image-box-2">
                    <label for="fileToUpload" class="mo-inspection-input-file-label vertical-center">
                        <img src="" alt="Upload" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available.png') }}'" class="mo-inspection-upload-image-icon img-responsive" />
                    </label>
                </div>
                <div class="col-xs-4 mo-inspection-upload-image-box no-padding" id="mo-image-box-3">
                    <label for="fileToUpload" class="mo-inspection-input-file-label vertical-center">
                        <img src="" alt="Upload" onerror="this.onerror=null; this.src='{{ asset('images/main/no_photo_available.png') }}'" class="mo-inspection-upload-image-icon img-responsive" />
                    </label>
                </div>
                {{--<input type="file" name="fileToUpload" id="fileToUpload" class="mo-exterior-input-file" />--}}
            </div>
            <div class="form-group col-xs-12 padding-top-24">
                <label for="note">Note</label>
                <textarea class="mo-textarea form-control disabled-div" rows="5" id="note" name="note" style="box-shadow: none;"></textarea>
            </div>
        </div>
        <hr class="mo-form-hr"/>
        <div class="mo-row padding-top-24">
            <div class="col-xs-12 text-right">
                <input type="button" class="mo-btn text-uppercase mo-cancel-btn" value="CANCEL">
                {{--<input type="button" class="mo-btn text-uppercase" value="UPDATE" id="inspection-update-button">--}}
            </div>
        </div>
    </form>
</div>
{{----------------------------------------------------------- EDIT INSPECTION ITEM POPUP -----------------------------------------------------------}}