@if(isset($disputeList) && !empty($disputeList))
    @foreach($disputeList as $dispute)
        <div item="{{ $dispute->id }}" class="col-xs-12 padding-bottom-10 dispute-item border-bottom padding-top-20">
            <div class="mo-row mo-dispute-wrapper">
                <div class="mo-row font-size16 font-black-color">{{ $dispute->vehicle_name }}</div>
                <div class="mo-row padding-top-16">
                    <div class="col-xs-2 vertical-center mo-dispute-image-wrapper no-padding">
                        <div class="image-inner">
                            <img src="https://{{Config::get('app.azure_storage_account_name')}}.blob.core.windows.net/vehicle-image/{{$dispute->vehicle_id or ''}}.jpg?{{rand()}}"  alt="Vehicle Photo" onerror="this.onerror=null; this.src='{{ asset('images/main/img-add-photo@3x.png')}}'" class="img-responsive">
                        </div>
                    </div>
                    <div class="dispute-detail col-xs-7">
                        <div class="mo-row font-warm-grey-color">
                            <div class="col-xs-3">Reference No.</div>
                            <div class="col-xs-9 font-weight-bold">{{ $dispute->rid }}</div>
                        </div>
                        <div class="mo-row font-warm-grey-color">
                            <div class="col-xs-3">Buyer</div>
                            <div class="col-xs-9 font-weight-bold">{{ $dispute->buyer_first_name . ' ' . $dispute->buyer_last_name }}</div>
                        </div>
                        <div class="mo-row font-warm-grey-color">
                            <div class="col-xs-3">Seller</div>
                            <div class="col-xs-9 font-weight-bold">{{ $dispute->seller_first_name . ' ' . $dispute->seller_last_name }}</div>
                        </div>
                        <div class="mo-row font-warm-grey-color">
                            <div class="col-xs-3">Transaction</div>
                            <div class="col-xs-9 font-weight-bold transaction_rid" data-transaction-id="{{ $dispute->transaction_id}}">{{ $dispute->transaction_rid}}</div>
                        </div>
                        <div class="mo-row font-warm-grey-color">
                            <div class="col-xs-3">Time & Date</div>
                            <div class="col-xs-9 font-weight-bold">{{ isset($dispute->create_date) ? date("dMY", strtotime($dispute->create_date)) : "" }}</div>
                        </div>

                        @if ($dispute->status != 1)
                            <div class="mo-row font-warm-grey-color">
                                <div class="col-xs-3">Support Agent</div>
                                <div class="col-xs-9 font-weight-bold">{{ $dispute->support_agent_first_name . ' ' . $dispute->support_agent_last_name }}</div>
                            </div>
                        @endif
                    </div>
                    <?php
                        switch ($dispute->status) {
                            case 1: // new
                                ?>
                                <div class="accept_btn dispute_btn col-xs-3">
                                    <a href="#" class="background-orange-yellow border-radius-24 text-align-center font-size14 font-weight-900 float-right">Accept</a>
                                </div>
                                <?php
                                break;
                            case 3: // under review
                                ?>
                                <div class="edit_btn dispute_btn col-xs-3">
                                    <a href="{{ url('/mo-admin/dispute/info/' . $dispute->transaction_id . '/' . $dispute->id) }}" class="background-orange-yellow border-radius-24 text-align-center font-size14 font-weight-900 float-right">Edit</a>
                                </div>
                                <?php
                                break;
                            case 4: // resolved
                                ?>
                                <div class="view_btn dispute_btn col-xs-3">
                                    <a href="{{ url('/mo-admin/dispute/info/' . $dispute->transaction_id . '/' . $dispute->id) }}" class="background-orange-yellow border-radius-24 text-align-center font-size14 font-weight-900 float-right">View</a>
                                </div>
                                <?php
                                break;
                        }
                    ?>
                </div>
            </div>
        </div>
    @endforeach
@else
    {{--<div class="mo-row text-center">--}}
        {{--<label class="text-uppercase">There is no item to display at the moment</label>--}}
    {{--</div>--}}
@endif
