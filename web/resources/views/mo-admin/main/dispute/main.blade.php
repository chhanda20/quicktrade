@extends('mo-admin.layouts.master')

@section('title', 'moto - Main')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row padding-bottom-24">
        <div class="col-xs-12 col-sm-7 no-padding" id="mo-content-header">
            {{ $header }}
        </div>
        {{--Start - moto Search Section--}}
        <div class="col-xs-12 col-sm-5 no-padding">
            {{ csrf_field() }}
            <div class="col-xs-12 no-padding mo-search-wrapper">
                    {{ csrf_field() }}
                    <input type="text" placeholder="Search" class="" id="mo-txt-search" name="mo-live-auction-search">
                    <span class="glyphicon glyphicon-search mo-txt-search cursor-pointer"></span>
            </div>
        </div>
        {{--End - moto Search Section--}}
    </div>
    {{--End - moto Content Header Section--}}

    <div class="mo-row mo-form-wrapper">
        <div class="mo-row padding-bottom-24">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding mo-5-blocks padding-top-24" id="mo-blocks-wrapper">
                @foreach ($tabs as $tab)
                    <div class="col-xs-12 col-sm-3 padding-lr5">
                        <a id="{{$tab['id']}}-link" tab="{{$tab['id']}}" href="#{{$tab['id']}}" status="{{ $tab['status'] }}">
                            <div class="col-xs-12 mo-block" id="{{$tab['id']}}_tab">
                                <span class="col-xs-12 mo-block-txt text-capitalize no-padding text-center">{{$tab['name']}}</span>
                            <span class="col-xs-12 text-center mo-arrow-focus">
                                <img src="{{ asset('images/svg/main/triangle.svg') }}" alt="Prev Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/triangle.png') }}'">
                            </span>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="mo-row" id="mo-dispute-grid-wrapper">
            @foreach ($tabs as $tab)
                <div class="data-block" id="{{ $tab['id'] }}-block"></div>
            @endforeach
            <form>
                {{ csrf_field() }}
            </form>
        </div>
        <div class="mo-row col-xs-12 text-center mo-loading-wrapper">
            <img class="mo-loading-icon" src="{{ asset('images/main/ajax-loader.gif') }}" alt="Loading Icon">
        </div>
    </div>
@endsection