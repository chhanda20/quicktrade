<div class="mo-row padding-top-16">
    <div class="col-xs-12 col-sm-6 col-md-5 no-padding">
        <div class="mo-form-header">RESOLUTION</div>
        <div class="padding-top-24 col-xs-11">
            <label>Decision</label>
            @if(isset($disputeSelectedDecisionName) || $viewOrEdit == "view")
                <span class="col-xs-12 no-padding" id="dispute-decision" data-decision-id="{{$disputeSelectedDecisionId}}">{{$disputeSelectedDecisionName or 'No decision yet'}}</span>
            @else
                <form id="dispute-resolution-form">
                    <div class="mo-dropdown-wrapper">
                        <span class="mo-icon-swipe glyphicon glyphicon-menu-down"></span>
                        <select class="form-control" name="dispute_decision" id="dispute-decision">
                            <option selected disabled>-- Decision list --</option>
                            @if(isset($disputeDecisions) && is_array($disputeDecisions) && count($disputeDecisions) > 0)
                                @foreach($disputeDecisions as $disputeDecision)
                                    <option value="{{$disputeDecision->code}}" {{$disputeDecision->code == $disputeInfo->decision?"selected":""}}>{{$disputeDecision->value}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </form>
            @endif
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-7 no-padding" id="mo-dispute-cost-variance">
        <div class="mo-form-header">DISPUTE COST VARIANCE</div>

        @if(isset($disputeItems) && is_object($disputeItems) && !empty($disputeItems))
            {{-- start ORGINAL --}}
            @if(isset($disputeItems->original))
                <?php $original = $disputeItems->original ?>
                <div class="mo-row">
                    <div class="mo-row padding-top-16 font-family-bold font-size16">
                        <div class="col-xs-6 no-padding">Original Damages</div>
                        <div class="col-xs-6 text-right no-padding">Total: $<span id="mo-original-cost-variance">{{number_format($originalVarianceCost, 2)}}</span></div>
                    </div>
                    {{--in case that general recondition cost exist--}}
                    @if(isset($original->EXTERIOR_SECTION) || (isset($disputeInfo->general_recondition_cost) && $disputeInfo->general_recondition_cost != 0))
                        {{-- start exterior section --}}
                        <div class="col-xs-12 no-padding padding-top-16">
                            <div class="font-weight-bold font-size16 padding-bottom-8">Exterior</div>

                            {{-- general reconditioning cost - 1 means show --}}
                            @if(isset($disputeInfo->general_recondition_cost) && $disputeInfo->general_recondition_cost != 0)
                                <div class="list-divider col-xs-12"></div>
                                <div class="col-xs-12 no-padding height-50 vertical-center">
                                    <div class="col-xs-3 no-padding">

                                    </div>
                                    <div class="col-xs-6">
                                        <div class="padding-left-10">
                                            <div class="font-weight-bold text-capitalize">general reconditioning cost</div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3 no-padding text-right">
                                        <span class="col-xs-7 no-padding padding-right-10 font-family-bold">+</span>
                                        @if($viewOrEdit == "view")
                                            <span class="col-xs-5 no-padding">${{isset($disputeInfo->general_recondition_cost_variance)?number_format($disputeInfo->general_recondition_cost_variance, 2):number_format(0, 2)}}</span>
                                        @else
                                            <input type="text" id="general-reconditioning-cost" data-item-id="" class="col-xs-5 no-padding mo-original-dispute-input"/>
                                        @endif
                                    </div>
                                </div>
                            @endif

                            @if(isset($original->EXTERIOR_SECTION))
                                {{-- exterior item --}}
                                @foreach($original->EXTERIOR_SECTION as $exterior_item)
                                    <div class="list-divider col-xs-12"></div>
                                    <div class="col-xs-12 no-padding height-100 vertical-center">
                                        <div class="col-xs-3 no-padding">
                                            <div class="max-width-100-percent">
                                                <img data-item-id="{{$exterior_item->inspection_item_id}}" src="{{asset("images/main/no_photo_available@2x.png")}}" class="img-responsive mo-item-img max-height-100" />
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <u class="font-family-bold">{{$exterior_item->section_name}}</u>
                                            <div class="padding-left-10">
                                                <div class="font-weight-bold">{{$exterior_item->item}}</div>
                                                <div class="font-grey-color font-style-italic">{{$exterior_item->note}}</div>
                                            </div>
                                        </div>
                                        <div class="col-xs-3 no-padding text-right">
                                            <span class="col-xs-7 no-padding padding-right-10 font-family-bold">+</span>
                                            @if($viewOrEdit == "view")
                                                <span class="col-xs-5 no-padding">${{isset($exterior_item->cost_variance)?number_format($exterior_item->cost_variance, 2):number_format(0, 2)}}</span>
                                            @else
                                                <input type="text" data-item-id="{{$exterior_item->id}}" class="col-xs-5 no-padding mo-original-dispute-input"/>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            <div class="list-divider col-xs-12"></div>
                            {{-- exterior item --}}
                        </div>
                        {{-- end exterior section --}}
                    @endif

                    @if(isset($original->INTERIOR_SECTION))
                        {{-- start interior/electronics section --}}
                        <div class="col-xs-12 no-padding padding-top-16">
                            <div class="font-weight-bold font-size16 padding-bottom-8">Interior/Electronics</div>

                            {{-- interior item --}}
                            @foreach($original->INTERIOR_SECTION as $interior_item)
                                <div class="list-divider col-xs-12"></div>
                                <div class="col-xs-12 no-padding height-100 vertical-center">
                                    <div class="col-xs-3 no-padding">
                                        <div class="max-width-100-percent">
                                            <img data-item-id="{{$interior_item->inspection_item_id}}" src="{{asset("images/main/no_photo_available@2x.png")}}" class="img-responsive mo-item-img max-height-100" />
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="padding-left-10">
                                            <div class="font-weight-bold">{{$interior_item->item}}</div>
                                            <div class="font-grey-color font-style-italic">{{$interior_item->note}}</div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3 no-padding text-right">
                                        <span class="col-xs-7 no-padding padding-right-10 font-family-bold">+</span>
                                        @if($viewOrEdit == "view")
                                            <span class="col-xs-5 no-padding">${{isset($interior_item->cost_variance)?number_format($interior_item->cost_variance, 2):number_format(0, 2)}}</span>
                                        @else
                                            <input type="text" data-item-id="{{$interior_item->id}}" class="col-xs-5 no-padding mo-original-dispute-input"/>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                            <div class="list-divider col-xs-12"></div>
                            {{-- interior item --}}
                        </div>
                        {{-- end interior/electronics section --}}
                    @endif

                    @if(isset($original->MECHANICAL_SECTION))
                        {{-- start mechanical section --}}
                        <div class="col-xs-12 no-padding padding-top-16">
                            <div class="font-weight-bold font-size16 padding-bottom-8">Mechanical</div>

                            {{-- mechanical item --}}
                            @foreach($original->MECHANICAL_SECTION as $mechanical_item)
                                <div class="list-divider col-xs-12"></div>
                                <div class="col-xs-12 no-padding height-100 vertical-center">
                                    <div class="col-xs-3 no-padding">
                                        <div class="max-width-100-percent">
                                            <img data-item-id="{{$mechanical_item->inspection_item_id}}" src="{{asset("images/main/no_photo_available@2x.png")}}" class="img-responsive mo-item-img max-height-100" />
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="padding-left-10">
                                            <div class="font-weight-bold">{{$mechanical_item->item}}</div>
                                            <div class="font-grey-color font-style-italic">{{$mechanical_item->note}}</div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3 no-padding text-right">
                                        <span class="col-xs-7 no-padding padding-right-10 font-family-bold">+</span>
                                        @if($viewOrEdit == "view")
                                            <span class="col-xs-5 no-padding">${{isset($mechanical_item->cost_variance)?number_format($mechanical_item->cost_variance, 2):number_format(0, 2)}}</span>
                                        @else
                                            <input type="text" data-item-id="{{$mechanical_item->id}}" class="col-xs-5 no-padding mo-original-dispute-input"/>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                            <div class="list-divider col-xs-12"></div>
                            {{-- mechanical item --}}
                        </div>
                        {{-- end mechanical section --}}
                    @endif
                </div>
            @endif
            {{-- end ORGINAL --}}

            {{-- start ADDITIONAL --}}
            @if(isset($disputeItems->additional))
                <?php $additional = $disputeItems->additional ?>
                <div class="mo-row padding-top-36">
                    <div class="mo-row font-family-bold font-size16">
                        <div class="col-xs-6 no-padding">Additional Damages</div>
                        <div class="col-xs-6 text-right no-padding">Total: $<span id="mo-additional-cost-variance-display">{{number_format($additionalVarianceCostDisplay, 2)}}</span></div>
                        <span id="mo-additional-cost-variance" class="hidden">{{number_format($additionalVarianceCost, 2)}}</span>
                    </div>
                    @if(isset($additional->EXTERIOR_SECTION))
                        {{-- start exterior section --}}
                        <div class="col-xs-12 no-padding padding-top-16">
                            <div class="font-weight-bold font-size16 padding-bottom-8">Exterior</div>

                            {{-- exterior item --}}
                            @foreach($additional->EXTERIOR_SECTION as $exterior_item)
                                <div class="list-divider col-xs-12"></div>
                                <div class="col-xs-12 no-padding height-100 vertical-center">
                                    <div class="col-xs-3 no-padding">
                                        <div class="max-width-100-percent">
                                            <img data-item-id="{{$exterior_item->inspection_item_id}}" src="{{asset("images/main/no_photo_available@2x.png")}}" class="img-responsive mo-dispute-item-img max-height-100" />
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <u class="font-family-bold">{{$exterior_item->section_name}}</u>
                                        <div class="padding-left-10">
                                            <div class="font-weight-bold">{{$exterior_item->item}}</div>
                                            <div class="font-grey-color font-style-italic">{{$exterior_item->note}}</div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3 no-padding text-right">
                                        <span class="col-xs-7 no-padding padding-right-10 font-family-bold">-</span>
                                        @if($viewOrEdit == "view")
                                            <span class="col-xs-5 no-padding">${{isset($exterior_item->cost_variance)?number_format($exterior_item->cost_variance, 2):number_format(0, 2)}}</span>
                                        @else
                                            <input type="text" data-item-id="{{$exterior_item->id}}" class="col-xs-5 no-padding mo-additional-dispute-input"/>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                            <div class="list-divider col-xs-12"></div>
                            {{-- exterior item --}}
                        </div>
                        {{-- end exterior section --}}
                    @endif

                    @if(isset($additional->INTERIOR_SECTION))
                        {{-- start interior/electronics section --}}
                        <div class="col-xs-12 no-padding padding-top-16">
                            <div class="font-weight-bold font-size16 padding-bottom-8">Interior/Electronics</div>

                            {{-- interior item --}}
                            @foreach($additional->INTERIOR_SECTION as $interior_item)
                                <div class="list-divider col-xs-12"></div>
                                <div class="col-xs-12 no-padding height-100 vertical-center">
                                    <div class="col-xs-3 no-padding">
                                        <div class="max-width-100-percent">
                                            <img data-item-id="{{$interior_item->inspection_item_id}}" src="{{asset("images/main/no_photo_available@2x.png")}}" class="img-responsive mo-dispute-item-img max-height-100" />
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="padding-left-10">
                                            <div class="font-weight-bold">{{$interior_item->item}}</div>
                                            <div class="font-grey-color font-style-italic">{{$interior_item->note}}</div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3 no-padding text-right">
                                        <span class="col-xs-7 no-padding padding-right-10 font-family-bold">-</span>
                                        @if($viewOrEdit == "view")
                                            <span class="col-xs-5 no-padding">${{isset($interior_item->cost_variance)?number_format($interior_item->cost_variance, 2):number_format(0, 2)}}</span>
                                        @else
                                            <input type="text" data-item-id="{{$interior_item->id}}" class="col-xs-5 no-padding mo-additional-dispute-input"/>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                            <div class="list-divider col-xs-12"></div>
                            {{-- interior item --}}
                        </div>
                        {{-- end interior/electronics section --}}
                    @endif

                    @if(isset($additional->MECHANICAL_SECTION))
                        {{-- start mechanical section --}}
                        <div class="col-xs-12 no-padding padding-top-16">
                            <div class="font-weight-bold font-size16 padding-bottom-8">Mechanical</div>

                            {{-- interior item --}}
                            @foreach($additional->MECHANICAL_SECTION as $mechanical_item)
                                <div class="list-divider col-xs-12"></div>
                                <div class="col-xs-12 no-padding height-100 vertical-center">
                                    <div class="col-xs-3 no-padding">
                                        <div class="max-width-100-percent">
                                            <img data-item-id="{{$mechanical_item->inspection_item_id}}" src="{{asset("images/main/no_photo_available@2x.png")}}" class="img-responsive mo-dispute-item-img max-height-100" />
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="padding-left-10">
                                            <div class="font-weight-bold">{{$mechanical_item->item}}</div>
                                            <div class="font-grey-color font-style-italic">{{$mechanical_item->note}}</div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3 no-padding text-right">
                                        <span class="col-xs-7 no-padding padding-right-10 font-family-bold">-</span>
                                        @if($viewOrEdit == "view")
                                            <span class="col-xs-5 no-padding">${{isset($mechanical_item->cost_variance)?number_format($mechanical_item->cost_variance, 2):number_format(0, 2)}}</span>
                                        @else
                                            <input type="text" data-item-id="{{$mechanical_item->id}}" class="col-xs-5 no-padding mo-additional-dispute-input"/>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                            <div class="list-divider col-xs-12"></div>
                            {{-- mechanical item --}}
                        </div>
                        {{-- end mechanical section --}}
                    @endif
                </div>
            @endif
            {{-- end ADDITIONAL --}}
        @endif

        <div class="col-xs-12 text-right mo-form-header padding-top-24">
            <span class="">Total Cost Variance: </span>$<span id="mo-total-cost-variance">{{number_format($totalVarianceCost,2)}}</span>
        </div>

    </div>
</div>