@extends('mo-admin.layouts.master')

@section('title', 'moto - Main')

@section('content')
    {{--Start - moto Content Header Section--}}
    <div class="mo-row padding-bottom-24">
        <div class="col-xs-12 col-sm-7 no-padding" id="mo-content-header">
            <a href="{!! url('mo-admin/dispute/show') !!}">{{ $header or 'Dispute Centre' }}</a> > {{ $breadcrumb or 'Dispute Info' }}
        </div>
    </div>
    {{--End - moto Content Header Section--}}

    <div class="mo-row mo-form-wrapper">
        <div class="col-xs-12 col-sm-6 col-md-5 no-padding">
            <div class="col-xs-12 no-padding">
                <div class="mo-form-header text-uppercase">
                    BUYER CONTACT
                </div>
                <div class="padding-top-24 padding-left-10 font-weight-bold font-size16">
                    {{$disputeInfo->buyer_location_name or ''}}
                </div>
                @if(isset($disputeInfo))
                    <div class="padding-left-10 padding-top-10 font-grey-color">
                        <span>Name: </span><span>{{$disputeInfo->contact_person_first_name." ".$disputeInfo->contact_person_last_name}}</span>
                    </div>
                    <div class="padding-left-10 padding-top-10 font-grey-color">
                        <span>Phone: </span><span>{{$disputeInfo->contact_person_phone or ''}}</span>
                    </div>
                    <div class="padding-left-10 padding-top-5 font-grey-color">
                        <span>Email: </span><span>{{$disputeInfo->contact_person_email or ''}}</span>
                    </div>
                @endif
            </div>
            <div class="col-xs-12 no-padding padding-top-40">
                <div class="mo-form-header text-uppercase">
                    SELLER CONTACT
                </div>
                <div class="padding-top-24 padding-left-10 font-weight-bold font-size16">
                    {{$disputeInfo->seller_location_name or ''}}
                </div>
                @if(isset($sellerInfo))
                    <div class="padding-left-10 padding-top-10 font-grey-color">
                        <span>Name: </span><span>{{$sellerInfo->first_name." ".$sellerInfo->last_name}}</span>
                    </div>
                    <div class="padding-left-10 padding-top-10 font-grey-color">
                        <span>Phone: </span><span>{{$sellerInfo->mobile_phone or ''}}</span>
                    </div>
                    <div class="padding-left-10 padding-top-5 font-grey-color">
                        <span>Email: </span><span>{{$sellerInfo->email or ''}}</span>
                    </div>
                @endif
            </div>    
        </div>

        @include("mo-admin.main.dispute.dispute-items")

        {{-- Note section --}}
        <div class="col-xs-12 no-padding padding-top-30">
            {{ csrf_field() }}
            <input type="hidden" value="{{$disputeInfo->transaction_id or ''}}" id="transaction-id">
            <div class="mo-form-header text-uppercase padding-bottom-15">Notes</div>
            <div class="list-divider col-xs-12"></div>

            <!-- EMPTY SPAN TO CLONE LATER -->
            <span class="col-xs-12 padding-tb10 text-capitalize mo-dispute-note font-size14 hidden">
                <textarea class="col-xs-9 no-padding mo-dispute-txt" data-dispute-note-id="" rows="2" placeholder="Write here…"></textarea>
                <div class="col-xs-3 no-padding padding-right-30 text-right font-grey-color">
                    <div class="mo-dispute-note-time"></div>
                    <div class="mo-dispute-note-by"></div>
                </div>
                <div class="mo-icon-remove text-right">
                    <span class="glyphicon glyphicon-remove"></span>
                </div>
                <div class="mo-icon-ok margin-top-24 text-right">
                    <span class="glyphicon glyphicon-ok"></span>
                </div>
                <div class="col-xs-4 no-padding mo-error-wrapper hidden">
                    <img src="{{asset('images/svg/main/ic-warning.svg')}}" alt="Plus Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/ic-warning.png') }}'">
                    <span class="error">Please enter content</span>
                </div>
            </span>

            @if(isset($disputeNotes))
                @foreach ($disputeNotes as $disputeNote)
                    <span class="col-xs-12 padding-tb10 text-capitalize mo-dispute-note font-size14">
                        <textarea class="col-xs-9 no-padding mo-dispute-txt" data-dispute-note-id="{{$disputeNote->id}}" rows="2" placeholder="Write here…" readonly>{{$disputeNote->note}}</textarea>
                        <div class="col-xs-3 no-padding padding-right-30 text-right font-grey-color">
                            <div class="mo-dispute-note-time">{{date('dMY H:i:s', strtotime($disputeNote->create_date))}}</div>
                            <div class="mo-dispute-note-by">by {{$disputeNote->creator_first_name." ".$disputeNote->creator_last_name}}</div>
                        </div>
                        @if($viewOrEdit == "edit")
                            <div class="mo-icon-remove text-right">
                                <span class="glyphicon glyphicon-remove"></span>
                            </div>
                                <div class="mo-icon-ok margin-top-24 text-right">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                        @endif
                        <div class="col-xs-4 no-padding mo-error-wrapper hidden">
                            <img src="{{asset('images/svg/main/ic-warning.svg')}}" alt="Plus Icon" onerror="this.onerror=null; this.src='{{ asset('images/main/ic-warning.png') }}'">
                            <span class="error">Please enter content</span>
                        </div>
                    </span>
                @endforeach
            @endif

            @if($viewOrEdit == "edit")
                <span class="col-xs-12 padding-tb10 text-capitalize">
                    <span id="mo-plus-icon" class="glyphicon glyphicon-plus"></span>
                </span>
            @endif
        </div>
        {{-- End Note section --}}

        {{-- Fault Section --}}
        <div class="col-xs-12 no-padding padding-top-30">
            <div class="mo-form-header text-uppercase padding-bottom-15">At Fault</div>
            <div class="col-xs-12 col-sm-4 {{$viewOrEdit == "view"?"disabled-div":""}}">
                <input type="checkbox" class="mo-square-check mo-fault" id="seller-fault" value="2" name="user_fault[]" {{$who_fault == 2 || $who_fault == 3?"checked":""}}/>
                <label for="seller-fault" class="text-capitalize mo-category"><span></span>Seller</label>
            </div>
            <div class="col-xs-12 col-sm-4 {{$viewOrEdit == "view"?"disabled-div":""}}">
                <input type="checkbox" class="mo-square-check mo-fault" id="buyer-fault" value="1" name="user_fault[]" {{$who_fault == 1 || $who_fault == 3?"checked":""}}/>
                <label for="buyer-fault" class="text-capitalize mo-category"><span></span>Buyer</label>
            </div>
            <div class="col-xs-12 col-sm-4 {{$viewOrEdit == "view"?"disabled-div":""}}">
                <input type="checkbox" class="mo-square-check mo-fault" id="no-fault" value="0" name="user_fault[]" {{$who_fault == 0?"checked":""}}/>
                <label for="no-fault" class="text-capitalize mo-category"><span></span>No Fault</label>
            </div>
        </div>
        {{-- End Fault Section --}}


        @include("mo-admin.main.dispute.dispute-cost-variance")

        <div class="mo-row padding-top-24">
            <div class="col-xs-12 alert alert-danger hidden" role="alert">
                <span class="mo-error-message text-capitalize"></span>
                <a class="mo-close-message close">&times;</a>
            </div>
        </div>

        <div class="mo-row padding-top-24 text-center">
            @if($disputeInfo->status == 2)
                <input type="button" value="under review" id="mo-dispute-under-review" class="mo-dispute-btn black-button" />
                <input type="button" value="submit" id="mo-dispute-submit" class="mo-dispute-btn" />
            @elseif($disputeInfo->status == 3)
                <input type="button" value="submit" id="mo-dispute-submit" class="mo-dispute-btn" />
            @endif
        </div>

        <div class="mo-loading-image hidden">Loading&#8230;</div>
    </div>
@endsection