@extends('mo-admin.layouts.master')

@section('title', 'moto - Main')

@section('content')
    <div class="mo-row">
        <div class="col-xs-12 col-sm-12 col-md-12 no-padding padding-bottom-20" id="mo-content-header">
            <a href="{!! url('/mo-admin/auction-session-list'); !!}"> {{ $header or 'Dealership'}} </a> > {{ $breadcrumb or 'Permissions'}}
        </div>
    </div>
    <form action="" method="post" id="mo-edit-permission-form">
        {{csrf_field()}}
        <div class="mo-row mo-form-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                <div class="col-xs-12 alert alert-success hidden" role="alert">
                    You've updated permission successfully !!!
                    <a class="mo-close-message close">&times;</a>
                </div>
                @if(isset($permission_list))
                    @foreach ($categories_type as $category)
                        <div class="col-xs-12 col-sm-12 col-md-12 no-padding">
                            <label class="col-xs-12 no-padding border-bottom padding-tb10 font-size16 text-capitalize">
                                {{--Category {{$category}}--}}
                                @if(isset($category_group))
                                    @foreach($category_group as $categories)
                                        @if($categories->code == $category)
                                            {{$categories->value}}
                                        @endif
                                    @endforeach
                                @endif
                            </label>
                        </div>
                        @foreach($permission_list as $permission)
                            @if($permission->category == $category)
                                <div class="col-xs-12 no-padding border-bottom mo-edit-permission">
                                    <div class="col-xs-1 no-padding padding-tb10">
                                        <label class="mo-permission-code">{{$permission->code}}</label>
                                        <span class='mo-permission-input-code hide'><input class="form-control" type='hidden' name="permission_code" placeholder="Name" value="{{$permission->code}}"></span>
                                    </div>

                                    <div class="col-xs-5 no-padding padding-tb10 mo-edit-permission-name">
                                        <label class="mo-permission-name">{{$permission->name}}</label>
                                        <input class="form-control hidden" type='text' id="permission-name" name="permission_name" value="{{$permission->name}}" >
                                    </div>

                                    <div class="col-xs-5 no-padding padding-tb10 mo-edit-permission-description">
                                        <label class="mo-permission-description">{{$permission->description}}</label>
                                        <input class="form-control hidden" type='text' id="permission-description" name="permission_description" value="{{$permission->description}}" >
                                    </div>

                                    <div class="col-xs-1 padding-tb10 text-right">
                                        <span data-permission-code="{{$permission->code}}" class="mo-open-edit-permission cursor-pointer"><img src="{{asset('images/svg/main/edit-permission.svg')}}" alt=""></span>
                                        <span data-permission-code="{{$permission->code}}" class="hidden mo-icon-edit glyphicon glyphicon-ok"></span>
                                        {{--<span data-permission-code="{{$permission->code}}" class="mo-close-edit-permission cursor-pointer hidden"><img src="{{asset('images/svg/main/copy-9.svg')}}" alt=""></span>--}}
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endforeach
                @endif
            </div>
        </div>
    </form>
@endsection