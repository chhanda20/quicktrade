<div class="mo-white-popup" id="mo-term-and-condition-popup">
    <div class="mo-row padding-bottom-20">
        <span class="text-capitalize float-left mo-popup-heading">term & condition</span>
    </div>
    <div class="mo-row">
        @if (isset($term_and_condition))
            <hr class="mo-form-hr">
            <textarea class="mo-textarea form-control" id="test-drive-term-condition" rows="20" style="border: none;"
                      readonly>{{ $term_and_condition }}</textarea>
            <hr class="mo-form-hr">
        @endif
    </div>
    <div class="mo-row padding-top-24">
        <div class="float-left">
            <input type="checkbox" class="mo-round-check" id="mo-checkbox-term-condition"
                   name="mo_checkbox_term_condition">
            <label for="mo-checkbox-term-condition"><span></span>I accept these Term & Conditions</label>
        </div>
        <div class="float-right">
            <input type="button" class="mo-btn text-uppercase grey-btn disabled-div"
                   id="mo-term-condition-agree-btn" value="accept">
        </div>
    </div>
    <div class="mo-loading-image hidden">Loading...</div>
</div>