;(function($){
    var isLittleEndian=(function(){
        if (!("Uint8ClampedArray" in window)) return true;
        var isLittleEndian = true;
        var buf = new ArrayBuffer(8);
        var buf8 = new Uint8ClampedArray(buf);
        var data = new Uint32Array(buf);
        // Determine whether Uint32 is little- or big-endian.
        data[1] = 0x0a0b0c0d;
        if (buf[4] === 0x0a && buf[5] === 0x0b && buf[6] === 0x0c &&
            buf[7] === 0x0d) {
            isLittleEndian = false;
        }
        return isLittleEndian
    })()
    var rgbaWord =isLittleEndian ? function(r,g,b,a){ //isLittleEndian
        return	(a   << 24) |    // alpha
                (b << 16) |    // blue
                (g <<  8) |    // green
                r;            // red
    } :function(r,g,b,a){ // isBigEndian
        return	(r << 24) |    // red
                (g << 16) |    // green
                (b <<  8) |    // blue
                a;              // alpha
    }
    function rgbaToString(r, g, b,a) {
        return "rgba("+[r,g,b,~~a/256].join(',')+")"
    }
    var aDataUrl = 'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAv0lEQVQYGT2QQQuCQBhEd0uoSCroFkHeIuj//5SgU3aIOhQFpR6C7D3TFp477g7zjca6rkO3Yowj9BgiVNy9/nedEdOKwwVMQGMBF8jxvBNEaE1rZAZz6MEDZq3eJ+04kzLYwBL6YJpBBZ6rwk6OM0nTFgawgxtMIXWEfURtkqYhGOJZ4/GlAovbyXEmeX6COzyhTPwFdNBgcQ2OM0XTEc7QdGQPOXhpsp3UJmk6EPaJPNC/RbIflIKdS7hqYg9f31s9vbEEDNgAAAAASUVORK5CYII=), crosshair',
        i = $('<i></i>').css('cursor', aDataUrl);
    if (i.css('cursor').match(/data:image/)) {
        $.fn.cursor = function(src, size, defaultCursor) {
            var image = new Image,
                elements = this,
                canvas = $.extend(document.createElement('canvas'),{width:size,height:size})
            if ($.isFunction(src)){
                src(canvas,size,size/2,size/2)
                var css = 'url(' + canvas.toDataURL("image/png") + ') ' + (size / 2) + ' ' + (size / 2) + ',' + (defaultCursor || 'crosshair')
                elements.css('cursor', css)
            } else{
                image.onload = function() {
                    canvas.getContext("2d").drawImage(image, 0, 0, this.width, this.height, 0, 0, size, size)
                    var css = 'url(' + canvas.toDataURL("image/png") + ') ' + (size / 2) + ' ' + (size / 2) + ',' + (defaultCursor || 'crosshair')
                    elements.css('cursor', css)
                }
                image.src = src
            }
            return elements
        }
    } else {
        $.fn.cursor = function(src, size, defaultCursor) {
            return this.css('cursor',defaultCursor || 'crosshair')
        }
    }
    $.fn.rgbaSelector=function(settings){
        var callbacks=$.extend({},settings,{
            // defaults…
        })
        return this.each(function(){
            var that=this,
                div=$('<div class="rgbSelector" style="width: 100%;">').appendTo(this),
                red=$('<div class="red" data-color="#ef2929">').appendTo(div),
                grn=$('<div class="grn" data-color="#8ae234">').appendTo(div),
                blu=$('<div class="blu" data-color="#729fcf">').appendTo(div),
                alp=$('<div class="alp">').appendTo(div)
            div.children().css({
            float: "left",
            clear: "left",
            width: "90%",
            margin: "8px 15px 8px 15px"
            }).slider({
                orientation: "horizontal",
                range: "min",
                max: 255,
                value: 127,
                slide: refreshSwatch,
                change: refreshSwatch
            }).each(function(){
                var color=$(this).data('color')
                $('.ui-slider-range, .ui-slider-handle',this).css('background',color)
            })
            refreshSwatch({type:"slidechange"})
            function refreshSwatch(e) {
            //	console.log(e.type)
                var sliders=[
                    red.slider( "value" ),
                    grn.slider( "value" ),
                    blu.slider( "value" ),
                    alp.slider( "value" )
                ]
                $.isFunction(callbacks[e.type]) && callbacks[e.type].call(that,e,rgbaToString.apply(0,sliders),sliders);
            }
        })
    }
    $.fn.bounds=function(){
        var bounds=this.position()
        bounds.bottom=bounds.top+this.outerHeight(true)
        bounds.right=bounds.left+this.outerWidth(true)
        return bounds
    }
    $.fn.label=function(){
        return this.map(function(){
            if (this.labels){
                return Array.prototype.slice.call(this.labels)
            }else{
                var label = $('label[for="'+$(this).attr('id')+'"]')
                if (label.length==0)
                    label = $(this).closest('label')
                return label.toArray()
            }
        })
    }
    $.fn.imageSmudge=function(css){
        var delimiter="JΛ̊KE",
            uploadEdited="Edited File to Upload ",
            toolsWithrgbaSelector = /^(smudge|colorOf|square)$/,
            toolsWithCustomCursor = /^(smudge|square)$/,
            drawingColor=[0,0,0,255],
            /** CUSTOM - Minh
             *
             * tools={fadeX:fadeX,blur:blur,contrast:contrast,colorOf:colorOf,smudge:smudge,square:square},
             *
             * **/
			tools={blur:blur},
            path=$('body>section:visible').data('base')||''
        if (css.tools)
            $.extend(tools,css.tools)


        /************** CUSTOM - Minh ******************
         *                                             *
         * Start - Using this for edit current image   *
         *                                             *
         ***********************************************/
        $(document).off("click.blur", ".mo-edited-icon-wrapper");
        $(document).on("click.blur",".mo-edited-icon-wrapper",function(e) {
            var this_elem = $(this);

            var undoStack=[],
                undoIndex=0,
                canvasVersion=function(canvas,delta){
                    //		console.log(undoStack.map(function(v){return v?'t':''}),undoIndex,delta)
                    var image=new Image()
                    image.onload=function(){
                        canvas.getContext('2d').drawImage(this, 0, 0)
                    }
                    if(undoStack[undoIndex+delta])
                        image.src=undoStack[undoIndex+=delta]
                },
                undo=function(canvas){
                    canvasVersion(canvas,-1)
                },
                redo=function(canvas){
                    canvasVersion(canvas,+1)
                },
                tool=function(canvas){
                    tools[toolName].apply(canvas,arguments)
                    undoStack[++undoIndex]=canvas.toDataURL("image/jpg")
                },
                toolSize=20,
                toolName="blur"

            var image = this_elem.prev("img")[0];
            //image.crossOrigin = "Anonymous";
            var contents,
                width=image.width,
                height=image.height,
                canvas=document.createElement('canvas'),
                cWidth,
                cHeight
            if ('width' in css && 'height' in css)
            {cWidth=css.width;cHeight=css.height}
            else if ('width' in css)
            {cWidth=css.width;cHeight=height * (css.width/width)}
            else if ('height' in css)
            {cHeight=css.height;cWidth=width * (css.height/height)}
            else
            {cHeight=height;cWidth=width}
// 							console.log(image,width,height,cWidth,cHeight)
            canvas.width=cWidth
            canvas.height=cHeight
            canvas.getContext("2d").drawImage(image,0,0,canvas.width,canvas.height)
            undoStack[0]=canvas.toDataURL("image/jpg")
            //console.log(undoStack[0]);

            var div=$(canvas)
                    .click(function(e){
                        if(typeof e.offsetX === "undefined" || typeof e.offsetY === "undefined") { // http://bugs.jquery.com/ticket/8523
                            var targetOffset = $(e.target).offset();
                            e.offsetX = e.pageX - targetOffset.left;
                            e.offsetY = e.pageY - targetOffset.top;
                        }
                        tool(this,toolSize,e.offsetX,e.offsetY)
                    })

                    /*.cursor(path+toolName+'-cur.png',toolSize)*/
                    .css("cursor", "crosshair")
                    .wrap('<div>').parent(),
                buttons=[],
                buttons=buttons.concat(
                    { html:"<i title='undo' class='fa fa-undo' aria-hidden='true'></i>", click: function(){
                        undo(canvas)
                    }},
                    { html:"<i title='redo' class='fa fa-repeat' aria-hidden='true'></i>", click: function(){
                        redo(canvas)
                    }}
                )
            console.log(path+toolName+'-cur.png');
            buttons=buttons.concat(
                { text: "Discard", click:function(e){
                    $( this ).dialog( "close" ).dialog('destroy')
                    //toolPicker.dialog( "close" ).dialog('destroy')
                }},
                { text: "Save", click: function(e,u) {
                    var this_btn = $(this);
                    var vehicle_photo_src = this_elem.prev("img").attr("src");
                    var vehicle_photo_name = vehicle_photo_src.substring(vehicle_photo_src.lastIndexOf('/')+1).split(".")[0];
                    var token = $('input[name="_token"]').val();

                    $(document).on("ajaxStart.blurImage", function () {
                        console.log("running");
                        $('.mo-loading-image').removeClass("hidden");
                    });

                    $.ajax({
                        type:'POST',
                        url: "/trading/upload-edited-vehicle-photo",
                        data:{
                            _token: token,
                            vehicle_image_name: vehicle_photo_name,
                            data_uri: canvas.toDataURL("image/jpg")
                        },
                        success:function(data) {
                            if(data){
                                console.log(data);
                                this_elem.prev("img").attr("src", data+"?"+Math.random());
                                vehicleSlider.reloadSlider();
                                light_slider.refresh();
                            }
                        },
                        error: function(data){
                            console.log("error");
                            console.log(data);
                        }
                    });

                    $(document).on("ajaxStop.blurImage", function () {
                        $('.mo-loading-image').addClass("hidden");
                        this_btn.dialog( "close" ).dialog('destroy');
                    });
                }}
            );
            var handle = $( "#custom-handle" );
            var slider_size_txt = "<div class='col-xs-2 no-padding text-capitalize margin-top-10'>brush size:</div>";
            $(slider_size_txt).appendTo(div);
            var slider_div = "<div class='col-xs-10 no-padding margin-top-16'><div id='custom-handle' class='ui-slider-handle'></div></div>";
            $(slider_div).appendTo(div).slider({
                max: 100,
                min:10,
                value: toolSize,
                range: "min",
                create: function( event, ui ) {
                    $('a',this)
                        .text(Math.floor(toolSize/10))
                        .css({textAlign:'center'})
                        .attr('title',"tool size")
                },
                slide: function( event, ui ) {
                    $(ui.handle).text(Math.floor(ui.value/10))
                },
                stop: function( event, ui ) {
                    toolSize=ui.value
                    //toolPicker.find('.current-button').click()
                }
            });
            $("<div class='mo-loading-image hidden'>Loading&#8230;</div>").appendTo(div);
            div.dialog({
                open: function(){
                    contents=$(this).dialog("widget").find('.ui-dialog-titlebar-close').hide().end()
                        .find('.ui-dialog-buttonpane button:last').focus().end()
                },
                /*position:{
                    my: "left center",
                    at: "left center"
                },*/
                buttons:buttons,
                //		draggable: false,
                closeOnEscape: false,
                modal: true,
                minHeight: cHeight+20,
                minWidth: Math.max(600,cWidth+50),
                title: "Edit Image",
                resizable: false,
                dialogClass: 'fixed-dialog'
            }).mousedown(function(){

            })
        });

        /************** CUSTOM - Minh ******************
         *                                             *
         *   End - Using this for edit current image   *
         *                                             *
         ***********************************************/





        if (window.File && window.FileReader && window.FileList && window.Blob) {
            this.on({
                reset:function(){
                    $(':file',this).prop('disabled',false).map(resetFormElement).parent().show().next('label').remove()
                },
                submit:function(){
                    var formData = new FormData(),
                        someElement=false,
                        form=this,
                        append=function(name,data,fn){
                            if (data!=undefined){
                                formData.append.apply(formData,arguments)
                                someElement=true
// 									console.log('appending',name,data,fn)
                            }
                        }
                    $(this.elements).each(function(i,el){
                        if (el.value.match(/^data:image/)){
                            var bits=el.value.split(delimiter,2)
                            append(el.name, dataURLtoBlob(bits[0]), bits[1]) // dataURLtoBlob makes sure the server gets a "real" file!
                        }else if (el.type=="file" && $(el).is(':visible')){
                            $.each(el.files,function(i,f){
                                append(el.name,f)
                            })
                        }else if($(el).is(':visible') && el.type !=="submit" && el.type !=="reset"){
                            append(el.name,el.value)
                        }else if(el.type==="hidden"){
                            append(el.name,el.value)
                        }
                    })
                    if (someElement)
                        $.ajax({
                            url: this.action,
                            data: formData,
                            processData: false,
                            contentType: false,
                            beforeSend: function (x) {
                                if (x && x.overrideMimeType) {
                                    x.overrideMimeType("multipart/form-data")
                                }
                            },
                            mimeType: 'multipart/form-data',
                            type: 'POST',
                            success: function(data){
                                console.log(data)
                                $(':file',form).prop('disabled',false).map(resetFormElement).parent().show().next('label').remove()
                            }
                        });
                    return false
                }
            })
            .find(':file').change(function(e){
				console.log("inside");
                var input=$(this),
                    form=$(this.form),
                    evt=e.originalEvent,
                    label=$(this).label()
                $.each(evt.target.files,function(i,file){
                    if (!file.type.match(/^image/)) return
                    var reader = new FileReader()
                        var undoStack=[],
                        undoIndex=0,
                        canvasVersion=function(canvas,delta){
                    //		console.log(undoStack.map(function(v){return v?'t':''}),undoIndex,delta)
                            var image=new Image()
                            image.onload=function(){
                                canvas.getContext('2d').drawImage(this, 0, 0)
                            }
                            if(undoStack[undoIndex+delta])
                                image.src=undoStack[undoIndex+=delta]
                        },
                        undo=function(canvas){
                            canvasVersion(canvas,-1)
                        },
                        redo=function(canvas){
                            canvasVersion(canvas,+1)
                        },
                        tool=function(canvas){
                            tools[toolName].apply(canvas,arguments)
                            undoStack[++undoIndex]=canvas.toDataURL(file.type)
                        },
                        toolSize=20,
                        toolName="blur"
                    reader.onload = function (evt) {
                        var image = new Image()
                        image.onload = function(e) {
                    //		console.log(image,this)
                            var contents,
                                width=this.width,
                                height=this.height,
                                hidden=$('<input>',{type:'hidden',name:input.attr('name')})
                                    .insertAfter(input.parents('label'))
                                    .wrap('<div class="edited"></div>'),
                                    /** CUSTOM - Minh
                                     *
                                     * .wrap('<label class="edited">'+uploadEdited+'</label>'),
                                     *
                                     * **/
                                canvas=document.createElement('canvas'),
                                cWidth,
                                cHeight
                            if ('width' in css && 'height' in css)
                                {cWidth=css.width;cHeight=css.height}
                            else if ('width' in css)
                                {cWidth=css.width;cHeight=height * (css.width/width)}
                            else if ('height' in css)
                                {cHeight=css.height;cWidth=width * (css.height/height)}
                            else
                                {cHeight=height;cWidth=width}
// 							console.log(image,width,height,cWidth,cHeight)
                            canvas.width=cWidth
                            canvas.height=cHeight
                            canvas.getContext("2d").drawImage(image,0,0,canvas.width,canvas.height)
                            undoStack[0]=canvas.toDataURL(file.type)

                            var toolPicker='<div class="toolPicker" title="tool picker">'
                            $.each(tools,function(name){
                                toolPicker+="<i title='"+name+"'></i>"
                            })
                            toolPicker+="</div>"
                            toolPicker = $(toolPicker).dialog({
                                width:400,
                                autoOpen:false,
                                closeOnEscape: false,
                                position:{
                                    my: "right center",
                                    at: "right center"
                                    },
                                dragStop: function( event, ui ) {
                                    toolPicker.dialog( "moveToTop" )
                                }
                                ,open:function(){
                                    $(this).dialog("widget").find('.ui-dialog-titlebar-close').hide()
                                }
                            })
                            $("<div style='margin:auto'></div>").appendTo(toolPicker).width(250).slider({
                                max: 100,
                                min:10,
                                value: toolSize,
                                range: "min",
                                create: function( event, ui ) {
                                    $('a',this)
                                        .text(Math.floor(toolSize/10))
                                        .css({textAlign:'center'})
                                        .attr('title',"tool size")
                                },
                                slide: function( event, ui ) {
                                    $(ui.handle).text(Math.floor(ui.value/10))
                                },
                                stop: function( event, ui ) {
                                    toolSize=ui.value
                                    toolPicker.find('.current-button').click()
                                }
                            })
                            var rgbaSelected=function(e,rgba,color){
                                    drawingColor=color
                                    var c = $.extend(document.createElement('canvas'),{width:98,height:98})
                                    smudge(c,98,98/2,98/2)
                                    $('i[title=smudge]').css('background-image','url('+c.toDataURL('image/png')+')')
                                    square(c,98,98/2,98/2)
                                    $('i[title=square]').css('background-image','url('+c.toDataURL('image/png')+')')
                                    if (e.type=="slidechange")
                                        toolPicker.find('.current-button').click()
                                },
                                rgbaSelector=$('<div>').appendTo(toolPicker).rgbaSelector({
                                    slidechange:rgbaSelected,
                                    slide:rgbaSelected,
                                    init:rgbaSelected
                                }).hide()
                            toolPicker.find('i').each(function(){
                                if (this.title==toolName) $(this).addClass('current-button')
                                $(this).css({
                                    border:"3px solid black",
                                    borderRadius:"6px",
                                    margin:2,
                                    background:"no-repeat center center url("+path+this.title+"-cur.png)",
                                    backgroundSize: 'cover',
                                    width:50,height:50,
                                    display:'inline-block'
                                })
                            }).click(function(){
                                $(this).addClass('current-button').siblings().removeClass("current-button")
                                toolName=this.title
                                if (toolName.match(toolsWithrgbaSelector))
                                    rgbaSelector.show("blind",'slow')
                                else
                                    rgbaSelector.hide("blind",'fast')
                            //	console.log($(this).css('background-image'))
//                                 var cursor = toolName.match(toolsWithCustomCursor) ?tools[toolName]:(path+toolName+'-cur.png')
//                                 $(canvas).cursor(cursor,toolSize)
                            })

                            var div=$(canvas)
                                    .click(function(e){
                                        if(typeof e.offsetX === "undefined" || typeof e.offsetY === "undefined") { // http://bugs.jquery.com/ticket/8523
                                           var targetOffset = $(e.target).offset();
                                           e.offsetX = e.pageX - targetOffset.left;
                                           e.offsetY = e.pageY - targetOffset.top;
                                        }
                                        tool(this,toolSize,e.offsetX,e.offsetY)
                                    })
                                    .cursor(path+toolName+'-cur.png',toolSize)
                                    .wrap('<div>').parent(),
                                buttons=[],
                            buttons=buttons.concat(
                                    { html:"<span title='undo' class='ui-icon-arrowreturnthick-1-w ui-icon'></span>", click: function(){
                                        undo(canvas)
                                    }},
                                    { html:"<span title='undo' class='ui-icon-arrowreturnthick-1-e ui-icon'></span>", click: function(){
                                        redo(canvas)
                                    }}
                                )
                            buttons=buttons.concat(
                                    { text: "Discard", click:function(e){
                                        input
                                            .prop('disabled',false).map(resetFormElement)
                                            /** CUSTOM - Minh
                                             *
                                             * .parent().show().next('label').remove()
                                             *
                                             * **/
                                            .parent().show().next('div').remove()
                                        $( this ).dialog( "close" ).dialog('destroy')
                                        toolPicker.dialog( "close" ).dialog('destroy')
                                    }},
                                    { text: "Keep", click: function(e,u) {
                                        input.prop('disabled',true).parents('label').hide()
                                        hidden.val(canvas.toDataURL(file.type)+delimiter+file.name)
                                        var edited=label.siblings('.edited').show()
                                        edited.contents().slice(2).remove()
                                        edited.append('<br><button type="button" class="edited-btn">edited file</button>')
                                            /** CUSTOM - Minh
                                             *
                                             * .append($(canvas).height(16).width(16))
                                             * .append(' '+file.name.small())
                                             *
                                             * **/
                                            .find('button').click(function(){
                                                var id=contents.attr('aria-describedby'),
                                                    dialog=$('#'+id)
                                                contents.find('.ui-dialog-content').html($(canvas).height('').width(''))
                                                dialog.dialog('open')
                                            toolPicker.dialog('open')
                                            }).end()
                                        $( this ).dialog( "close" )
                                        toolPicker.dialog('close')
                                    }}
                                )
                            div.dialog({
                                open: function(){
                                    contents=$(this).dialog("widget").find('.ui-dialog-titlebar-close').hide().end()
                                    .find('.ui-dialog-buttonpane button:last').focus().end()
                                },
                                position:{
                                    my: "left center",
                                    at: "left center"
                                    },
                                buttons:buttons,
                        //		draggable: false,
                                closeOnEscape: false,
                                modal: true,
                                minHeight: cHeight+20,
                                minWidth: Math.max(600,cWidth+20),
                                title: file.name,
                                resizable: false
                            }).mousedown(function(){
                                var tpb=toolPicker.closest('.ui-dialog').bounds(),
                                    dvb=$(this).closest('.ui-dialog').bounds(),
                                    under=(
                                        tpb.top>=dvb.top &&
                                        tpb.left>=dvb.left &&
                                        tpb.bottom<=dvb.bottom &&
                                        tpb.right<=dvb.right);
                                if (under) toolPicker.closest('.ui-dialog').animate({left:0})
                            })
                            toolPicker.dialog('open')
                        }
                        image.src = reader.result
                    };
                    reader.readAsDataURL(file)
                })
            })
        } else {
            alert('The File APIs are not fully supported in this browser.');
        }
        return this
        function resetFormElement() {
            $(this).val('')
            if (this.files.length){ // ie10 kludges
                var clone=$(this).clone(true)
                $(this).replaceWith(clone)
                return clone[0]
            }else{
                return this
            }
        }
        function quads(buff){
            var a=[],
                len=buff.length/4
            for (var i=0;i<len;i++) a[i]=buff.subarray(i*4,4+i*4)
            return a
        };
        function quad(buff,i){
            return buff.subarray(i*4,(i*4)+4)
        };
        function blur(canvas,size,x,y){
            var fuzzList=[
                            [-1,-1,1],
                            [ 0,-1,2],
                            [+1,-1,1],
                            [-1, 0,2],
                    //		[ 0, 0,0],
                            [+1, 0,2],
                            [-1,+1,1],
                            [ 0,+1,2],
                            [+1,+1,1]
                    ]
            var ctx=canvas.getContext("2d")
            var
                radius=Math.ceil(size/2)-1,
                sx=x-radius,
                sy=y-radius,
                pixels=ctx.getImageData(sx, sy, size,size),
                data=pixels.data,
                odata=ctx.getImageData(sx, sy, size,size).data;
            for( var j=0;j<size;j++){
                for(var i=0;i<size;i++){
                    var d=Math.sqrt((i-radius)*(i-radius)+(j-radius)*(j-radius)),
                        q=quad(data,i+(j*size)),
                        r=0,g=0,b=0,a=0,totalScore=0
                //		,debug=[i+(j*size)]
                    if (d<=radius){
                        $.each(fuzzList,function(row,line){
                            var iPrime = i+line[0],
                                jPrime = j+line[1],
                                score = line[2],
                                point = iPrime+(jPrime*size),
                                qPrime=quad(odata,point)
                            if (iPrime<0||iPrime>size||jPrime<0||jPrime>size) return;
                            r+=qPrime[0]*score
                            g+=qPrime[1]*score
                            b+=qPrime[2]*score
                            a+=qPrime[3]*score
                            totalScore+=score
                    //		debug.push(JSON.stringify([point,score]))
                        })
                        q[0]=r/totalScore // red
                        q[1]=g/totalScore // green
                        q[2]=b/totalScore // blue
                        q[3]=a/totalScore // opacity
                    //	console.log(totalScore,q)
                    }
                }
            }
            ctx.putImageData(pixels,sx,sy)
        }
        function fuzzx(canvas,size,x,y){
            var ctx=canvas.getContext("2d")
            var
                radius=Math.ceil(size/2)-1,
                sx=x-radius,
                sy=y-radius,
                pixels=ctx.getImageData(sx, sy, size,size)
                buf = new ArrayBuffer(pixels.data.length),
                buf8 = new Uint8ClampedArray(buf),
                data = new Uint32Array(buf),
                obuf = new ArrayBuffer(pixels.data.length),
                obuf8 = new Uint8ClampedArray(obuf),
                odata = new Uint32Array(obuf),
                xy=function(x,y){return y*size+x};
            buf8.set(pixels.data)
            obuf8.set(pixels.data)
            for( var j=0;j<size;j++){
                for(var i=0;i<size;i++){
                    var d=Math.sqrt((i-radius)*(i-radius)+(j-radius)*(j-radius))
                    if (d<=radius){
                        data[xy(i,j)]=rgbaWord(255,0,0,255)
                    }
                }
            }
            pixels.data.set(buf8)
//			console.log(buf8)
            ctx.putImageData(pixels,sx,sy)
        }
        function fadeX(canvas,size,x,y){
            var ctx=canvas.getContext("2d")
            ctx.save()
            var sx=x-(size/2),
                sy=y-(size/2),
                pixels=ctx.getImageData(sx, sy, size,size).data,
                pixelCount=pixels.length/4,
                r=0,
                g=0,
                b=0,
                a=0
            for (var i=0;i< pixels.length;i+=4) {
                r+=pixels[i]
                g+=pixels[i+1]
                b+=pixels[i+2]
                a+=pixels[i+3]
            }
            r=(r/pixelCount).toFixed(0)
            g=(g/pixelCount).toFixed(0)
            b=(b/pixelCount).toFixed(0)
            a=(a/pixelCount).toFixed(0)
        //	console.log(r,g,b,a)
            var radgrad = ctx.createRadialGradient(size / 2, size / 2, 0, size / 2, size / 2, size / 2);
            radgrad.addColorStop(0, 'rgba('+r+','+g+','+b+','+a+')');
            radgrad.addColorStop(1, 'rgba('+r+','+g+','+b+','+0+')');
            ctx.fillStyle = radgrad
            ctx.translate(x-(size/2), y-(size/2))
            ctx.fillRect(0, 0, size, size)
            ctx.restore()
        }
        function smudge(canvas, size, x, y) {
            var ctx=canvas.getContext("2d")
            ctx.save()
            var radgrad = ctx.createRadialGradient(size / 2, size / 2, 0, size / 2, size / 2, size / 2);
            radgrad.addColorStop(0, rgbaToString.apply(0,drawingColor));
            radgrad.addColorStop(1, 'rgba(0,0,0,0)');
            ctx.fillStyle = radgrad
            ctx.translate(x-(size/2), y-(size/2))
            ctx.fillRect(0, 0, size, size)
            ctx.restore()
        }
        function colorOf(canvas,size,x,y){
            var ctx=canvas.getContext("2d"),
                drawingColor=ctx.getImageData(x,y, 1,1).data
            $('.rgbSelector .red').slider('value',drawingColor[0])
            $('.rgbSelector .grn').slider('value',drawingColor[1])
            $('.rgbSelector .blu').slider('value',drawingColor[2])
            $('.rgbSelector .alp').slider('value',drawingColor[3])
        }
        function square(canvas,size,x,y){
            var ctx=canvas.getContext("2d"),
                halfSize=Math.ceil(size/2)
                sx=x-halfSize-1,
                sy=y-halfSize-1
            ctx.save()
            ctx.fillStyle = rgbaToString.apply(0,drawingColor)
            ctx.fillRect (sx, sy, size, size)
        //	console.log(x,y,sx,sy,size)
            ctx.restore()
        }
        function contrast(canvas,size,x,y){
            var ctx=canvas.getContext("2d"),
                radius=Math.ceil(size/2)-1,
                sx=x-radius,
                sy=y-radius,
                pixels=ctx.getImageData(sx, sy, size,size),
                data=pixels.data
            for( var j=0;j<size;j++){
                for(var i=0;i<size;i++){
                    var d=Math.sqrt((i-radius)*(i-radius)+(j-radius)*(j-radius)),
                        q=quad(data,i+(j*size)),
                        r=0,g=0,b=0,a=0,totalScore=0
                    if (d<=radius){
                        var average = Math.round( ( q[0] + q[1] + q[2] ) / 3 ),
                            ac=1 +( (1/average)*(d/2)*(average>127?1:-1))
                        q[0]*=ac
                        q[1]*=ac
                        q[2]*=ac

                    }
                }
            }
            ctx.putImageData(pixels,sx,sy)
        }
    }
})(jQuery)
