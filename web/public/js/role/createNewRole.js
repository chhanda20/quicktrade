/**
 * Created by Son on 8/28/2017.
 */

$(document).ready(function(){
    $("#mo-create-new-role-form").validate({
        rules: {
            role_name: "required",
            "permissions_list[]": { required: true },
            "menu_items_list[]": { required: true}
        },
        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        errorPlacement: function(error, element) {
            if (element.is(":checkbox") && element.attr("name") == "permissions_list[]") {
                error.appendTo("#errorToShow");
            } else if(element.is(":checkbox") && element.attr("name") == "menu_items_list[]"){
                error.appendTo("#error_To_Show");   // default placement
            }else{
                error.insertAfter(element)
            }
        },
        messages: {
            role_name: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter role name</span>" + "</div>",
            "permissions_list[]": {
                "required": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please select at least one permission</span>" + "</div>"
            },
            "menu_items_list[]": {
                "required": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please select at least one item for menu access</span>" + "</div>"
            }
        }
    });

    /** Shift + click multiselect permission**/
    $(".mo-permission").shiftcheckbox();

    $("#dealer-save").click(function(){
        if(!$("#mo-create-new-role-form").valid()){
            return false;
        }
        $("#mo-create-new-role-form").on("submit", function(){
            $(".mo-loading-image").removeClass("hidden");
        });
    });
});