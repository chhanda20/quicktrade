/*$(document).ready(function(){*/
    var hash = window.location.hash;

    var token = $("input[name='_token']").val();

    var path_name = window.location.pathname;
    var mo_admin_url = "";

    if (path_name.indexOf("mo-admin") != -1) {
        mo_admin_url = "/mo-admin";
    }

    /** GET BODY TYPE **/
    $.ajax({
        type: 'POST',
        data: {
            _token: token,
            year: $("#build-date").val().substr(0,4),
            make: $("#make").val(),
            model: $("#model").val(),
            variant: $("#variant").val(),
            month: $("#build-date").val().substr(5,2)
        },
        url: "/get-glasses-data/body_type",
        success:function(data) {
            console.log('success: '+data);
            if(data){
                $("#body-type").find("option:gt(0)").remove();
                $.each(data, function(key, value) {
                    $('#body-type')
                        .append($("<option></option>")
                            .attr("value",value)
                            .text(value));
                });
                if($("#body_type_value").val() != ""){
                    $("#body-type").val($("#body_type_value").val());
                }
            }
        },
        error: function(data){
            console.log('error: '+data);
        }
    });

    /** GET TRANSMISSION **/
    $.ajax({
        type: 'POST',
        data: {
            _token: token,
            year: $("#build-date").val().substr(0,4),
            make: $("#make").val(),
            model: $("#model").val(),
            variant: $("#variant").val(),
            month: $("#build-date").val().substr(5,2)
        },
        url: "/get-glasses-data/transmission",
        success:function(data) {
            console.log('success: '+data);
            if(data){
                $("#transmission").find("option:gt(0)").remove();
                $.each(data, function(key, value) {
                    $('#transmission')
                        .append($("<option></option>")
                            .attr("value",value)
                            .text(value));
                });
                if($("#transmission_value").val() != ""){
                    $("#transmission").val($("#transmission_value").val());
                }
            }
        },
        error: function(data){
            console.log('error: '+data);
        }
    });

    /** GET OPTIONAL OPTIONS **/
    function getOptionalOptions(){
        $.ajax({
            type: 'GET',
            url: "/get-vehicle-optional-options/"+$("#vehicle-id").val(),
            success:function(html) {
                //console.log(html);
                $("#optional-options-wrapper").empty();
                $("#optional-options-wrapper").append(html);
            },
            error: function(data){
                console.log('error: '+data);
            }
        });
    }

    /** GET GLASSES OPTIONS LIST **/
    function getGlassesOptionsList(glass_code){
        //console.log("get glass options list");
        $.ajax({
            type: 'GET',
            url: "/get-glasses-options-list/"+glass_code,
            success:function(html) {
                //console.log(html);
                $("#glass-options-wrapper").empty();
                $("#glass-options-wrapper").append(html);
            },
            error: function(data){
                console.log('error: '+data);
            }
        });
    }

    function getGlassModelDetail(){
        $.ajax({
            type: 'GET',
            url: "/get-glasses-model-detail/"+$("#glass-code").val(),
            success:function(data) {
                console.log('success: '+data);
                data = jQuery.parseJSON(data);
                $('#engine').val(data[0].engine);
                $('#engine-txt').text(data[0].engine);
                $('#engine-size').val(data[0].engine_size);
                $('#engine-size-txt').text(data[0].engine_size);
                $('#engine-capacity').val(data[0].engine_capacity);
                $('#engine-capacity-txt').text(data[0].engine_capacity);
                $('#cylinder').val(data[0].cylinder);
                $('#cylinder-txt').text(data[0].cylinder);
            },
            error: function(data){
                console.log('error: '+data);
            }
        });
    }

    function getGlassCode(){
        $.ajax({
            type: 'POST',
            data: {
                _token: token,
                year: $("#build-date").val().substr(0,4),
                make: $("#make").val(),
                model: $("#model").val(),
                variant: $("#variant").val(),
                month: $("#build-date").val().substr(5,2),
                body_type: $("#body-type").val(),
                transmission: $("#transmission").val()
            },
            url: "/get-glasses-data/glass_code",
            success:function(data) {
                console.log('success: '+data);
                if(data.length == 1){//get single glass code
                    $("#glass-code").val(data[0]);
                    getGlassModelDetail();
                    if($("#glass-code").val() != ""){
                        getGlassesOptionsList($("#glass-code").val());
                    }
                }
            },
            error: function(data){
                console.log('error: '+data);
            }
        });
    }

    getGlassCode();
    $("select#body-type").on('change', function(){
        getGlassCode();
    });
    $("select#transmission").on('change', function(){
        getGlassCode();
    });

    /** VALIDATE MODEL SPECS **/
    $('#mo-model-spec-form').validate({
        rules: {
            body_type: "required",
            transmission: "required"
        },
        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        messages:{
            body_type: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please select body type</span>"+"</div>",
            transmission: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please select transmission</span>"+"</div>"
        }
    });
    $('#mo-model-spec-next-button').click(function(){
        $("#mo-model-spec-form").valid();
    });

    /* Original Form - using for ajax save */
    var option_extra_form_original_data = $("#mo-create-option-and-extra-form").serialize();
    var mechanical_form_original_data = $("#mo-mechanical-form").serialize();
    var model_spec_original_data = $("#mo-model-spec-form").serialize();

    /* Apply datepicker */
    $("#registration-expiry-date-picker").datepicker({
        format: 'M yyyy',
        viewMode: 'months',
        minViewMode: 'months',
        autoclose: true
    });

    $("#build-date-picker").datepicker({
        format: 'mm-yyyy',
        viewMode: 'months',
        minViewMode: 'months',
        autoclose: true
    });

    $("#external-offer-arrival-date").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        startDate: new Date(),
        minDate: new Date()
    });

    $("#compliance-date-picker").datepicker({
        format: 'dd M yyyy',
        todayHighlight: true,
        orientation: 'bottom',
        autoclose: true
    });

    /** DELETE VALUATION **/
    $(".open-popup-link").click(function() {
        $(".alert-danger").addClass("hidden");
    });

    $("#mo-cancel-valuation").click(function() {
        $(document).ajaxStart(function() {
            $(".mo-loading-image").removeClass("hidden");
            $("#mo-confirm-cancel-valuation-popup").find("input").addClass("disabled-div");
        });
        var valuation_id = $("#valuation_id").val();
        var customer_id = $("#customer_id").val();
        if($("#valuation-status").val() == 10){
            $.ajax({
                type:'GET',
                url: "/delete-valuation/"+valuation_id,
                success:function(data) {
                    //console.log('success: '+data);
                    if(customer_id){
                        window.location.href = "/customer-details/view/"+customer_id;
                    }else{
                        window.location.href = "/customers";
                    }

                },
                error: function(data){
                    //console.log('error: '+data);
                },
                complete: function(){
                    $('.mo-loading-image').addClass("hidden");
                    $("#mo-confirm-cancel-valuation-popup").find("input").removeClass("disabled-div");
                }
            });
        }else if($("#valuation-status").val() == 2){
            $.ajax({
                type:'GET',
                url: "/cancel-valuation/"+valuation_id,
                success:function(data) {
                    //console.log('success: '+data);
                    if(data == 1){
                        window.location.href = mo_admin_url+"/valuations/requests";
                    }else{
                        console.log(data);
                    }

                },
                error: function(data){
                    //console.log('error: '+data);
                },
                complete: function(){
                    $('.mo-loading-image').addClass("hidden");
                    $("#mo-confirm-cancel-valuation-popup").find("input").removeClass("disabled-div");
                }
            });
        }
        /*else if($("#valuation-status").val() == 4 || $("#valuation-status").val() == 5){
            $.ajax({
                type:'GET',
                url: "/cancel-valuation/"+valuation_id,
                success:function(data) {
                    //console.log('success: '+data);
                    if(data == 1){
                        window.location.href = mo_admin_url+"/valuations/pending";
                    }else{
                        //error
                        $(".alert-danger").removeClass("hidden").find(".error-text").html(data);
                    }
                },
                error: function(data){
                    //console.log('error: '+data);
                },
                complete: function(){
                    $('.mo-loading-image').addClass("hidden");
                    $("#mo-confirm-cancel-valuation-popup").find("input").removeClass("disabled-div");
                }
            });
        }*/
        else if($("#valuation-status").val() == 3 || $("#valuation-status").val() == 6){
            $.ajax({
                type:'GET',
                url: "/dealership/cancel-valuation-version/"+valuation_id,
                success:function(data) {
                    //console.log('success: '+data);
                    if(data == 1){
                        window.location.href = mo_admin_url+"/valuations/complete";
                    }
                },
                error: function(data){
                    //console.log('error: '+data);
                },
                complete: function(){
                    $('.mo-loading-image').addClass("hidden");
                    $("#mo-confirm-cancel-valuation-popup").find("input").removeClass("disabled-div");
                }
            });
        }

    });

    /** PUT BEGIN VAL TO REQUEST LIST **/
    $("#mo-put-into-request").click(function(){
        $(document).ajaxStart(function() {
            $(".mo-loading-image").removeClass("hidden");
            $("#mo-confirm-cancel-valuation-popup").find("input").addClass("disabled-div");
        });
        var valuation_id = $("#valuation_id").val();
        var token = $("input[name='_token']").val();
        //console.log('inside ajax');
        $.ajax({
            type: "POST",
            url: "/request-valuation",
            data: {
                _token: token,
                valuation_id: valuation_id
            },
            success: function (data) {
                if(data){
                    window.location.href = mo_admin_url+"/valuations/requests";
                }
            },
            error: function (data) {
                //console.log(data);
            },
            complete: function(){
                $('.mo-loading-image').addClass("hidden");
                $("#mo-confirm-cancel-valuation-popup").find("input").removeClass("disabled-div");
            }
        });

    });
    /** PUT BEGIN VAL TO REQUEST LIST **/

    /** Click moto 5 TABS **/
    $('.mo-block').first().toggleClass('mo-block-active').removeClass('disabled-div');
    $('.mo-block').click(function() {
        $('.mo-block').removeClass('mo-block-active');
        $(this).toggleClass('mo-block-active').removeClass('disabled-div');

        var mo_data_step = $(this).find('.mo-block-txt').attr("data-step");
        /*$('.mo-block-heading').text(mo_data_step).fadeIn("slow");*/
        /*$('.mo-block-heading').fadeOut(function() {
         $(this).text(mo_data_step)
         }).fadeIn();*/

        $('.mo-block-list').hide();
        checkFormChange($("#mo-create-option-and-extra-form"), option_extra_form_original_data, "/save-options-and-extras/"+ $('#valuation_id').val(), 'option_extra');
        checkFormChange($("#mo-mechanical-form"), mechanical_form_original_data, "/save-mechanical-details/"+ $('#valuation_id').val(), 'mechanical');
        checkFormChange($("#mo-model-spec-form"), model_spec_original_data, "/save-model-spec/"+ $('#valuation_id').val(), 'model_spec');

        if(mo_data_step === 'details') {
            $('#mo-details-block').hide();
            $('#mo-details-block').removeClass('hidden').fadeIn('slow');
        } else if(mo_data_step === 'summary') {
            /** COMPARE TOTAL ESTIMATED COST **/
            var current_price = $('#mo-total-estimated-cost').text();
            var new_price = parseFloat($('#mo-exterior-cost').text().replace(/[$,]+/g,""))+parseFloat($('#mo-interior-cost').text().replace(/[$,]+/g,""))+parseFloat($('#mo-mechanical-cost').text().replace(/[$,]+/g,""));
            new_price = "$"+new_price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            if(current_price != new_price){
                /** SET NEW PRICE **/
                $('#mo-total-estimated-cost').text(new_price);}

            $('#mo-summary-block').hide();
            $('#mo-summary-block').removeClass('hidden').fadeIn('slow');
        } else if(mo_data_step === 'interior') {
            $('#mo-interior-block').hide();
            $('#mo-interior-block').removeClass('hidden').fadeIn('slow');
        } else if(mo_data_step === 'exterior') {
            $('#mo-exterior-block').hide();
            $('#mo-exterior-block').removeClass('hidden').fadeIn('slow');
        } else if(mo_data_step === 'mechanical') {
            $('#mo-mechanical-block').hide();
            $('#mo-mechanical-block').removeClass('hidden').fadeIn('slow');
        }
        $('#mo-valuation-step-txt').text(mo_data_step);

    });

    $('.mo-block-inside').first().toggleClass('mo-block-active').removeClass('disabled-div');
    $('.mo-block-inside').click(function(){
        $('.mo-block-inside').removeClass('mo-block-active');
        $(this).toggleClass('mo-block-active').removeClass('disabled-div');

        var mo_data_details_step = $(this).find('.mo-block-txt').attr("data-details-step");

        $('.mo-block-inside-list').hide();
        checkFormChange($("#mo-create-option-and-extra-form"), option_extra_form_original_data, "/save-options-and-extras/"+ $('#valuation_id').val(), 'option_extra');
        checkFormChange($("#mo-mechanical-form"), mechanical_form_original_data, "/save-mechanical-details/"+ $('#valuation_id').val(), 'mechanical');
        checkFormChange($("#mo-model-spec-form"), model_spec_original_data, "/save-model-spec/"+ $('#valuation_id').val(), 'model_spec');

        if(mo_data_details_step === 'model_specs') {
            $('#mo-model-specs-block').hide();
            $('#mo-model-specs-block').removeClass('hidden').fadeIn('slow');
        } else if(mo_data_details_step === 'options_extras') {
            $('#mo-options-extras-block').hide();
            $('#mo-options-extras-block').removeClass('hidden').fadeIn('slow');
        }
    });

    /** CHECK STEP HAS BEEN SAVE BY LOCAL STORAGE **/
    function checkStepStorage(current_step, expected_step){
        /*var w_step_valuation = "step_valuation_"+$("#valuation_id").val();
         console.log('ls: '+w_step_valuation+' | '+localStorage.getItem(w_step_valuation));*/
        switch (current_step){
            case "1":
                console.log('step1');
                $('#options_tab').removeClass('disabled-div');
                if(expected_step > current_step){
                    toggleTab($('#details_tab'), $('#mo-details-block'));
                    toggleTabInside($('#options_tab'), $('#mo-options-extras-block'));
                }
                break;
            case "2":
                console.log('step2');
                $('#options_tab').removeClass('disabled-div');
                $('#exterior_tab').removeClass('disabled-div');
                if(expected_step > current_step){
                    toggleTab($('#exterior_tab'), $('#mo-exterior-block'));
                }
                break;
            case "3":
                console.log('step3');
                $('.mo-block').removeClass('disabled-div');
                $('.mo-block-inside').removeClass('disabled-div');
                $('#mechanical_tab').addClass('disabled-div');
                $('#summary_tab').addClass('disabled-div');
                if(expected_step > current_step){
                    toggleTab($('#interior_tab'), $('#mo-interior-block'));
                }
                break;
            case "4":
                console.log('step4');
                $('.mo-block').removeClass('disabled-div');
                $('.mo-block-inside').removeClass('disabled-div');
                $('#summary_tab').addClass('disabled-div');
                if(expected_step > current_step){
                    toggleTab($('#mechanical_tab'), $('#mo-mechanical-block'));
                }
                break;
            case "5":
                console.log('step5');
                $('.mo-block').removeClass('disabled-div');
                $('.mo-block-inside').removeClass('disabled-div');
                if(expected_step > current_step){
                    toggleTab($('#summary_tab'), $('#mo-summary-block'));
                }
                break;
            default:
                toggleTab($('#details_tab'), $('#mo-details-block'));
                break;
        }
    }

    /** HASH tag URL **/
    function toggleTab(tab_id, tab_block){
        $('.mo-block').removeClass('mo-block-active');
        tab_id.toggleClass('mo-block-active')/*.removeClass('disabled-div')*/;
        $('.mo-block-list').hide();
        tab_block.hide();
        tab_block.removeClass('hidden').fadeIn('slow');
    }

    if(hash){
        var w_step_valuation = "step_valuation_"+$("#valuation_id").val();
        var current_step = localStorage.getItem(w_step_valuation);
        var expected_step = 0;
        if (hash.indexOf("#details") != -1) {
            toggleTab($('#details_tab'), $('#mo-details-block'));
        } else if (hash.indexOf("#exterior") != -1){
            init_items("EXTERIOR_SECTION");
            toggleTab($('#exterior_tab'), $('#mo-exterior-block'));
            expected_step = 2;
            console.log("expected_step "+expected_step);
        } else if (hash.indexOf("#interior") != -1){
            init_items("INTERIOR_SECTION");
            toggleTab($('#interior_tab'), $('#mo-interior-block'));
            expected_step = 3;
        } else if (hash.indexOf("#mechanical") != -1){
            init_items("MECHANICAL_SECTION");
            toggleTab($('#mechanical_tab'), $('#mo-mechanical-block'));
            expected_step = 4;
        } else if (hash.indexOf("#summary") != -1){
            toggleTab($('#summary_tab'), $('#mo-summary-block'));
            expected_step = 5;
        }

        if($('#valuation-status').val() == 10 || $('#valuation-status').val() == 2){//status 10 for begin, 2 for in progress
            checkStepStorage(current_step, expected_step);
        }

        var active_text = $(".mo-block-active").find(".mo-block-txt").first().text();
        $('#mo-valuation-step-txt').text(active_text);
    }else{
        if($('#valuation-status').val() == 10 || $('#valuation-status').val() == 2){//status 10 for begin, 2 for in progress
            var w_step_valuation = "step_valuation_"+$("#valuation_id").val();
            var current_step = localStorage.getItem(w_step_valuation);
            checkStepStorage(current_step, 0);
        }
    }

    function toggleTabInside(tab_id, tab_block){
        $('.mo-block-inside').removeClass('mo-block-active');
        tab_id.toggleClass('mo-block-active');
        $('.mo-block-inside-list').hide();
        tab_block.hide();
        tab_block.removeClass('hidden').fadeIn('slow');
    }

    if(hash && hash.split("-")[1]){
        var w_step_valuation = "step_valuation_"+$("#valuation_id").val();
        var current_step = localStorage.getItem(w_step_valuation);
        var expected_step = 0;
        if (hash.split("-")[1].indexOf("model") != -1) {
            toggleTabInside($('#model_tab'), $('#mo-model-specs-block'));
        } else if (hash.split("-")[1].indexOf("options") != -1) {
            toggleTabInside($('#options_tab'), $('#mo-options-extras-block'));
            expected_step = 1;
        }
        if($('#valuation-status').val() == 10 || $('#valuation-status').val() == 2){//status 10 for begin, 2 for in progress
            checkStepStorage(current_step, expected_step);
        }
    }

    /** PREV & NEXT - handle **/
    /* 5 BLOCKS */
    switchTab($("#mo-prev-exterior"), $("#details_tab"));
    switchTab($("#mo-next-exterior"), $("#interior_tab"), 3);
    switchTab($("#mo-prev-interior"), $("#exterior_tab"));
    switchTab($("#mo-next-interior"), $("#mechanical_tab"), 4);
    switchTab($("#mo-prev-mechanical"), $("#interior_tab"));
    switchTab($("#mo-next-mechanical"), $("#summary_tab"), 5);

    /* 2 BLOCKS */
    switchTab($("#mo-model-spec-next-button"), $("#options_tab"), 1);
    switchTab($("#mo-save-options-extras"), $("#exterior_tab"), 2);

    /** PREV & NEXT - function **/
    function switchTab(mo_button, mo_tab, step){
        mo_button.click(function(){
            if(step){
                // Store local storage
                var w_step_valuation = "step_valuation_"+$("#valuation_id").val();
                var current_step = localStorage.getItem(w_step_valuation);
                if((current_step && (step > current_step)) || current_step == null){
                    localStorage.setItem(w_step_valuation, step);
                    checkStepStorage(current_step, 0);
                }
            }

            if(mo_button.selector == "#mo-model-spec-next-button"){
                if($("#mo-model-spec-form").valid()){
                    mo_tab.trigger("click");

                    $("html, body").animate({
                        scrollTop: $("#mo-content-wrapper").offset().top
                    }, 500);
                }else{
                    if($("#body_type").val() == ''){
                        $("#body_type").focus();
                    }else{
                        $("#transmission").focus();
                    }
                }
            }else{
                mo_tab.trigger("click");

                $("html, body").animate({
                    scrollTop: $("#mo-content-wrapper").offset().top
                }, 500);
            }

        });
    }

    $("form#mo-mechanical-form").find("input[id=mo-request-btn]").click(function (){
        var w_step_valuation = "step_valuation_"+$("#valuation_id").val();
        localStorage.removeItem(w_step_valuation);//remove local storage
    });

    localStorage.removeItem("step_valuation");//this line would be remove later

    /** Using for ajax save **/
    var saveFlag = true;
    function checkFormChange(moForm, moOrignialForm, url, formType){
        if (moForm.serialize() != moOrignialForm && saveFlag) {
            console.log('changed');
            saveFlag=false;
            $.ajax({
                type:'POST',
                url: url,
                data:moForm.serialize(),
                success:function(data) {
                    if(!saveFlag){
                        saveFlag = true;
                    }
                    console.log("success: "+data);
                    /* update original form for checking next time */
                    if(formType == 'option_extra'){
                        option_extra_form_original_data = moForm.serialize();
                    }
                    else if(formType == 'mechanical'){
                        mechanical_form_original_data = moForm.serialize();
                    }
                    else if(formType == 'model_spec'){
                        model_spec_original_data = moForm.serialize();
                        /* get optional options */
                        getOptionalOptions();
                    }
                },
                error: function(data){
                    console.log("error");
                }
            });
        }
    }

    /** EXTRAS OPTIONS **/

    $('#mo-plus-icon').click(function(){
        var span_html = $(".mo-extras-options:first").clone().removeClass('hidden');
        var remove_icon_html =
            '<div class="mo-icon-remove float-right">'+
            '<img src="/images/main/icon-remove.png" alt="Remove Icon">'+
            '</div>';

        var textarea_html =
            '<textarea name="extras_options[]" class="form-control mo-extras-txt" rows="1" placeholder="Write here…" style="box-shadow: none;"></textarea>' +
            '<input type="hidden" name="" value="">'+
            remove_icon_html+
            '<div class="mo-error-wrapper hidden">'+
            '<img src="/images/main/ic-warning.png" alt="Plus Icon">'+
            '<span class="error">Please enter content</span>'+
            '</div>';

        $(".mo-error-wrapper").hide();/** hide all errors **/
        if($(".mo-extras-options textarea").eq(-1).val()!=''){
            $(".mo-extras-options:last").before(span_html.html(textarea_html));
            $(".mo-extras-txt").prop('readonly', true);/** set all textarea read-only **/
            $(".mo-extras-txt:last").prop('readonly', false);
        }else{
            $(".mo-extras-txt:last").prop('readonly', false);
            $(".mo-error-wrapper").hide();
            $(".mo-extras-options").eq(-2).find(".mo-error-wrapper").removeClass('hidden').fadeIn('slow');
        }
    });

    $("#mo-create-option-and-extra-form").on('click', ".mo-icon-remove", function () {
        /** If hidden input don't have value, it's newly created item, remove it
         *  otherwise, hide it **/
        if($(this).parent().find("input").val() != ''){
            $(this).parent().find("input").attr("name", "deleted_extras_options[]");
            $(this).parent().addClass("hidden");
        }else{
            $(this).parent().remove();
        }

    });

    /** Prevent Collapse when clicking on children **/
    $('.mo-sub-content li').click(function(e) {
        e.stopPropagation(); // prevents event e from going to parent
    });
    $(".mo-main-content").click(function(){
        var glyphion_menu = $(this).find('.mo-icon-swipe');
        //expand
        if (glyphion_menu.hasClass('glyphicon-menu-down')) {
            $(this).find('.mo-icon-swipe').removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
            $(this).find('.mo-expand-hide-label').html("Hide");
        }
        //hide
        else {
            $(this).find('.mo-expand-hide-label').html("Expand");
            $(this).find('.mo-icon-swipe').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
        }
    });

    $(".mo-main-content .mo-general-cost").click(function(){
        return false;
    });
/*
});*/
