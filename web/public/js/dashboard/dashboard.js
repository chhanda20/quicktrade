
$(document).ready(function(){

    $(document).on("click", ".mo-dashboard-filter-item", function(e){
        e.stopPropagation();
    });

    /** CUSTOMIZE LOCATION **/
    $("#filter-all").click(function(){
        $("#mo-dashboard-filter-location input:checkbox").not(this).prop("checked", this.checked);
    });

    /** CUSTOMIZE SHOW TIME **/

    $(".dropdown-menu-custom").click(function(){
        $(".dropdown-submenu-custom").removeClass("hidden");
    });

    $("input[name='filter-date-range']").click(function(){
        $(".dropdown-submenu-custom").addClass("hidden");
    });

    $("#mo-dashboard-date-range-from").datepicker({
        format: "yyyy-mm-dd",
        todayHighlight: true,
        autoclose: true,
        orientation: "bottom"
    });

    $("#mo-dashboard-date-range-to").datepicker({
        format: "yyyy-mm-dd",
        todayHighlight: true,
        autoclose: true,
        orientation: "bottom"
    });


    /** SHOW TAB FROM CUSTOMIZE CHECKBOX **/

    var token = $("input[name='_token']").val();
    $(".mo-customize-item").click(function(){
        var label_checked = $(this).next().text();
        var id_checked = $(this).attr("id");
        console.log(label_checked);
        if($(this).prop("checked")==true){
            showCustomizeTab(token,label_checked,id_checked);
        }else{
            $(".mo-block-tab[id='mo_tab_"+id_checked+"']").remove();
        }
    });

    $("#show-only-my-stats").click(function(){
        if($(this).is(":checked")){
            console.log("success");
            showCustomizeTabByTime(token);
        }else{
            showCustomizeTabByTime(token);
        }
    });
    $("#mo-btn-save-location").click(function(){
        if($("input[type='checkbox']").is(":checked")){
            $.ajax({
                _token: token,
                type: "POST",
                url: base_url+"/dashboard",
                data: $("#mo-dashboard-customize-form").serialize(),
                success: function(data){
                    var json_data = JSON.parse(data);
                    console.log(json_data);
                    $.each(json_data, function(key, value){
                        for(key1 in value){
                            $("#mo_tab_" + key1).find(".mo-block-number").text(value[key1]);
                        }
                    });
                },
                error: function(data){
                    console.log("error" + data);
                }
            });
        }
    });

    $("#mo-btn-clear-show-time").click(function(){
        $(".dropdown-submenu-custom").addClass("hidden");
        if($("input[type='radio']").is(":checked")){
            $(".mo-filter-date-range").prop("checked", false);
            $("#mo-date-range-from").val("");
            $("#mo-date-range-to").val("");
            showCustomizeTabByTime(token);
        }
    });

    $(".mo-filter-date-range").click(function(){
        if($(this).prop("checked") == true){
            // var label = $(this).parent().next().text();
            // $("#btn-select-show-time").find("span").last().text(label);
            if($(this).attr("id") == "filter-today"){
                $("#mo-date-range-from").val($("#mo-today").val());
                $("#mo-date-range-to").val($("#mo-today").val());
                showCustomizeTabByTime(token);
            }
            if($(this).attr("id") == "filter-last-7-days"){
                $("#mo-date-range-from").val($("#mo-last-7-days-from").val());
                $("#mo-date-range-to").val($("#mo-last-7-days-to").val());
                showCustomizeTabByTime(token);
            }
            if($(this).attr("id") == "filter-last-month"){
                $("#mo-date-range-from").val($("#mo-last-month-from").val());
                $("#mo-date-range-to").val($("#mo-last-month-to").val());
                showCustomizeTabByTime(token);
            }
            if($(this).attr("id") == "filter-this-month"){
                $("#mo-date-range-from").val($("#mo-this-month-from").val());
                $("#mo-date-range-to").val($("#mo-this-month-to").val());
                showCustomizeTabByTime(token);
            }
            if($(this).attr("id") == "filter-custom-range"){
                var date_from = $("#mo-date-range-from").val();
                var date_to = $("#mo-date-range-to").val();
                if(date_from != " " && date_to != " "){
                    $(document).click(function(){
                        showCustomizeTabByTime(token);
                    });
                }
            }
        }
    });

/** HANDLE BUTTON & POPUP FOR TEST DRIVE **/
    $("#mo-test-drive-return-vehicle-form").validate({
        rules: {
            mo_marketing_option: "required",
            test_drive_note: "required",
            mo_option_other: "required"
        },
        highlight: function(element) {
            $(element).closest('.input-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.input-group').removeClass('has-error');
        },
        messages: {
            mo_marketing_option: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
            test_drive_note: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
            mo_option_other: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
        }
    });

    $(".mo-test-drive-return").click(function(){
        // slider for vehicle rating
        $("#slider_vehicle_rating").slider({
            value: 4,
            min: 0,
            max: 10,
            step: 1
        }).each(function(){
            var opt = $(this).data().uiSlider.options;
            var vals = opt.max - opt.min;
            for (var i = 0; i <= vals; i++) {
                var el = $('<label>'+(i)+'</label>').css('left',(i/vals*100)+'%');
                $( "#slider_vehicle_rating" ).append(el);
            }
        });

        // slider for mo rating
        $("#slider_mo_rating").slider({
            value: 4,
            min: 0,
            max: 10,
            step: 1
        }).each(function(){
            var opt = $(this).data().uiSlider.options;
            var vals = opt.max - opt.min;
            for (var i = 0; i <= vals; i++) {
                var el = $('<label>'+(i)+'</label>').css('left',(i/vals*100)+'%');
                $( "#slider_mo_rating" ).append(el);
            }
        });
        $("#slider_vehicle_rating").slider('value',null);
        $("#slider_mo_rating").slider('value',null);
        $("#test-drive-note").val(null);
        $("#mo_marketing_option").val(null);

        var inprogress_id = $(this).data("inprogress-id");
        console.log(inprogress_id);
        var token = $("input[name='_token']").val();
        var make = $(this).data("make-name");
        var model = $(this).data("model-name");
        var year = $(this).data("year");

        $(".mo-popup-heading").text(year + " " + make + " " + model);
        $.magnificPopup.open({
            items: {
                src: "#mo-test-drives-comment-popup"
            },
            type: 'inline',
            showCloseBtn: true,
            closeOnBgClick: false
        });
        var magnificPopup = $.magnificPopup.instance;
        $('.mo-cancel-btn').click(function () {
            $("#mo-marketing-option-other").addClass("hidden");
            magnificPopup.close();
            $("#errorToShow").html("");
        });

        $("#mo_marketing_option").change(function(){
            if($("#mo_marketing_option").find(":selected").text() == "Other"){
                $("#mo-marketing-option-other").removeClass("hidden");
            }else{
                $("#mo-marketing-option-other").addClass("hidden");
            }
        });

        $("#mo-test-drives-inprogress-change-status").click(function(e) {
            e.preventDefault();
            /*if($("#test-drive-note").val() == "") {
                $("#errorToShow").html("<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>");
            }*/
            if(!$("#mo-test-drive-return-vehicle-form").valid()){
                return false;
            }
            else {
                $(document).ajaxStart(function(){
                    $('.mo-loading-image').removeClass("hidden");
                });
                $.ajax({
                    type: "POST",
                    url: base_url+"/test-drives/complete/" + inprogress_id,
                    data: {
                        _token: token,
                        "test_drive_note": $("#test-drive-note").val(),
                        "vehicle_rating": $('#slider_vehicle_rating').slider("option", "value"),
                        "service_rating": $('#slider_mo_rating').slider("option", "value"),
                        "marketing_code": $("#mo_marketing_option").val(),
                        "marketing_option": $("#mo-option-other").val()
                    },
                    success: function(data) {
                        if(data == 1) {
                            window.location.replace("/test-drives/complete");
                        }
                    },
                    error: function(data) {
                        //console.log(data);
                    },
                    complete: function(){
                        $(".mo-loading-image").addClass("hidden")
                    }
                });
            }
        });

    });


/** HANDLE BUTTON & POPUP FOR REQUEST VALUATION **/

    $("#mo-popup-enter-vin").validate({
        rules: {
            year: "required",
            vin: "required",
            "registration-number": "required"
        },
        messages: {
            year: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
            vin: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
            "registration-number": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>"
        }
    });

    $(".mo-accept-valuate-btn").click(function(e){
        var valuation_id = $(this).data("valuation-id");
        var vin_number = $(this).data("vin");
        var year_date = $(this).data("year").split("-");
        var year = year_date[0];
        var registration_number = $(this).data("registration-number");

        $("input[name='year']").val(year);
        $("input[name='vin']").val(vin_number);
        $("input[name='registration-number']").val(registration_number);
        $.magnificPopup.open({
            items: {
                src: "#mo-enter-vin-popup"
            },
            type: 'inline',
            showCloseBtn: true,
            closeOnBgClick: false
        });
        var magnificPopup = $.magnificPopup.instance;
        $('.mo-cancel-btn').click(function () {
            magnificPopup.close();
            $("#errorToShow").html("");
            $("div.vin-number").empty();
            $("div.note-content").empty();
        });
        $("#get-vin").click(function(){
            if($("#mo-popup-enter-vin").valid()){
                /*$(document).ajaxStart(function(){
                    $(".mo-loading-image").removeClass("hidden");
                });*/
                var vin = $("input[name='vin']").val();
                var year_number = $("input[name='year']").val();
                var token = $("input[name='_token']").val();
                if(vin.length > 0 && year != null){
                    $.ajax({
                        type: "POST",
                        url: base_url+"/search-vin",
                        data:{_token: token, vin_number: vin, year: year_number},
                        success: function(data){
                            response = JSON.parse(data);
                            if(data.length > 0){
                                console.log(response);
                                $("div.vin-number").text(response.vin);
                                $("div.note-content").text(
                                    response.model_family
                                    +" "+ response.build_date
                                    +" "+ response.variant
                                    // +" "+ response.series
                                    +" "+ response.colour
                                    // +" "+ response.manufacturer_color
                                    // +" "+ response.body_type
                                    // +" "+ response.registered_state
                                    +" "+ response.registration_number
                                    +" "+ response.compliance_date
                                );
                                $("#vin-confirm").prop("disabled", false);
                            }
                        }
                        /*complete: function(){
                            $('.mo-loading-image').addClass("hidden");
                        }*/
                    });
                }
            }
        });

        /** Confirm Popup Enter Vin **/
        $("#vin-confirm").click(function(e){
            var token = $("input[name='_token']").val();
            if($("#mo-popup-enter-vin").valid()){
                $(document).ajaxStart(function(){
                    $(".mo-loading-image").removeClass('hidden');
                });
                $.ajax({
                    type: "POST",
                    url: "/accept-valuation/" + valuation_id,
                    data: {_token: token},
                    success: function(response){
                        response = $.parseJSON(response);
                        console.log(response);
                        if(response.hasOwnProperty("id")){
                            document.location.href = "/review-valuation/"+valuation_id;
                        } else {
                            console.log("accept valuation error");
                        }
                    },
                    error: function(){

                    },
                    complete: function(){
                        $(".mo-loading-image").addClass('hidden');
                    }
                });
            }
        });
    });

/** END **/
    

/** HANDLE BUTTON & POPUP PENDING VALUATION */

    $(".mo-approve-valuate-btn").click(function(e){
        var valuation_id = $(this).data("valuation-id");
        var token = $("input[name='_token']").val();
        var pending_row = $(this).parent().parent();

        $("#mo-valuation-pending-approval-confirmation-btn").data("valuation_id", valuation_id);
        $("#mo-valuation-pending-approval-confirmation-btn").data("token", token);
        $("#mo-valuation-pending-approval-confirmation-btn").data("pending_row", pending_row);
        $.magnificPopup.open({
            items: {
                src: "#mo-sensitive-action-confirm-popup"
            },
            type: 'inline',
            showCloseBtn: true,
            closeOnBgClick: false
        });
        var magnificPopup = $.magnificPopup.instance;
        $('.mo-cancel-btn').click(function () {
            magnificPopup.close();
            // $("#errorToShow").html("");
        });

        $("#mo-valuation-pending-approval-confirmation-btn").click(function(e){
            var valuation_id = $(this).data("valuation_id");
            var token = $(this).data("token");
            var pending_row = $(this).data("pending_row");
            $.ajax({
                type: "POST",
                url: base_url+"/approve-valuation",
                headers: {"X-CSRF-TOKEN": token},
                data: { "valuation_id": valuation_id},
                success: function(result){
                    console.log(result);
                    var res = $.parseJSON(result);
                    if(typeof (res.id) != "undefined"){
                        pending_row.hide();
                        pending_row.prev().hide();
                        magnificPopup.close();
                        location.reload();
                        console.log("approve success");
                    }
                },
                error: function(){
                    console.log("Approve Valuation Error");
                }

            });
        });

    });

    $(".mo-approve-price-btn").click(function(){
        var valuation_id = $(this).data("valuation-id");
        var token = $("input[name='_token']").val();
        var pending_row = $(this).parent().parent();
        $("#mo-valuation-pending-approval-confirmation-btn").data("valuation_id", valuation_id);
        $("#mo-valuation-pending-approval-confirmation-btn").data("token", token);
        $("#mo-valuation-pending-approval-confirmation-btn").data("pending_row", pending_row);
        $.magnificPopup.open({
            items: {
                src: "#mo-sensitive-action-confirm-popup"
            },
            type: 'inline',
            showCloseBtn: true,
            closeOnBgClick: false
        });
        var magnificPopup = $.magnificPopup.instance;
        $('.mo-cancel-btn').click(function () {
            magnificPopup.close();
            // $("#errorToShow").html("");
        });

        $("#mo-valuation-pending-approval-confirmation-btn").click(function(){
            var valuation_id = $(this).data("valuation_id");
            var token = $(this).data("token");
            var pending_row = $(this).data("pending_row");
            $(document).ajaxStart(function(){
                $(".mo-loading-image").removeClass("hidden");
            });
            $.ajax({
                type: "POST",
                url: base_url+"/approve-revaluation",
                headers: {"X-CSRF-TOKEN": token},
                data: {"valuation_id": valuation_id},
                success: function(result){
                    var res = $.parseJSON(result);
                    console.log(res);
                    magnificPopup.close();
                    pending_row.remove();
                    pending_row.prev().hide();
                },
                error: function(){

                },
                complete: function(){
                    $(".mo-loading-image").addClass("hidden");
                }
            });
        });
    });

    $(".mo-reject-price-btn").click(function(){
        var valuation_id = $(this).data("valuation-id");
        var token = $("input[name='_token']").val();
        var pending_row = $(this).parent().parent();
        console.log(valuation_id);
        console.log(token);
        console.log(pending_row);
        $.ajax({
            type: "POST",
            url: base_url+"/reject-revaluation",
            headers: {"X-CSRF-TOKEN": token},
            data: {"valuation_id": valuation_id},
            success: function(result){
                console.log(result);
                pending_row.remove();
            }
        });
    });



/** HANDLE BUTTON & POPUP IN PLACE MY CAR INCOMING **/

    /*$("#external-offer-bid-price-expiry-date").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        orientation: 'bottom',
        startDate: new Date(),
        minDate: new Date()
    });*/
    $(".glyphicon-calendar").click(function(){
        $("#external-offer-bid-price-expiry-date").focus();
    });

    // bid price popup validate
    $('#mo-external-offer-bid-price-form').validate({
        rules:{
            "external-offer-bid-price-expiry-date": "required",
            "external-offer-bid-price-input": "required"

        },
        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        messages:{
            "external-offer-bid-price-expiry-date": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter expiry date</span>"+"</div>",
            "external-offer-bid-price-input": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter price</span>"+"</div>"
        }
    });

    Date.prototype.addDays = function(days) {
        var dat = new Date(this.valueOf());
        dat.setDate(dat.getDate() + days);
        return dat;
    };

    var global_external_offer_id = 0;
    $(".external-offer-bid-price-btn").click(function(){
        $.magnificPopup.open({
            items: {
                src: "#mo-external-offer-bid-price-popup"
            },
            type: 'inline',
            showCloseBtn: true,
            closeOnBgClick: false
        });
        var magnificPopup = $.magnificPopup.instance;
        $('.mo-cancel-btn').click(function () {
            magnificPopup.close();
        });
        global_external_offer_id = $(this).data("external-offer-id");
        $.ajax({
            type: "POST",
            data: {
                "_token": $("input[name='_token']").val(),
                "valuation_id": $(this).data("valuation-id")
            },
            url: base_url+"/external-offer/get-info",
            success: function(data){
                if(data != null){
                    data = $.parseJSON(data);
                    console.log(data);
                    var external_offer_expiry = 0;
                    //debugger;
                    if($("#external-offer-expiry").val()){
                        external_offer_expiry = $("#external-offer-expiry").val();
                    }
                    var expiry_date = new Date(data.arrival_date).addDays(parseInt(external_offer_expiry));
                    var bid_price_configuration = {
                        format: 'yyyy-mm-dd',
                        autoclose: true,
                        todayHighlight: false,
                        startDate: expiry_date,
                        minDate: expiry_date,
                        defaultDate: expiry_date
                    };
                    $("#external-offer-bid-price-expiry-date").datepicker("remove");
                    $("#external-offer-bid-price-expiry-date").datepicker(bid_price_configuration);
                    $("#external-offer-bid-price-expiry-date").datepicker("update");
                }

            },
            error: function(data){
                console.log("error /external-offer/bid-price");
            },
            complete: function(){

            }

        });
    });

    $("#mo-external-offer-bid-price-submit-btn").click(function(){
        var external_offer_id = global_external_offer_id;
        var price = $("input[name='external-offer-bid-price-input']").val();
        var expiry_date = $("input[name='external-offer-bid-price-expiry-date']").val();
        console.log(external_offer_id);
        console.log(price);
        console.log(expiry_date);
        $(document).ajaxStart(function(){
            $(".mo-loading-image").removeClass("hidden");
        });
        if($('#mo-external-offer-bid-price-form').valid()){
            $.ajax({
                type: "POST",
                url: base_url+"/external-offer/bid-price",
                data: {
                    "_token": $("input[name='_token']").val(),
                    "external_offer_id": external_offer_id,
                    "price": price,
                    "expiry_date": expiry_date
                },
                success: function(data){
                    data = $.parseJSON(data);
                    if(data.success) {
                        console.log("bid successful");
                        // magnificPopup.close();
                        window.location.reload();
                    }else {
                        //show error message
                        $(".bid-price-info").removeClass("hidden");
                        setInterval(function(){
                            window.location.reload();
                        }, 5000);
                        console.log("/external-offer/bid-price", data);
                    }
                },
                error: function(){
                    console.log("error /external-offer/bid-price");
                },
                complete: function(){
                    $(".mo-loading-image").addClass("hidden");
                }
            });
        }

    });

    // delete popup external incoming offer
    var global_external_offer_id = 0;
    var global_vehicle_id = 0;
    $(document).on("click", ".mo-external-offer-reject", function(){
        global_external_offer_id = $(this).data("external-offer-id");
        global_vehicle_id = $(this).data("vehicle-id");
        $.magnificPopup.open({
            items: {
                src: "#mo-incoming-external-offer-delete-popup"
            },
            type: 'inline',
            showCloseBtn: true,
            closeOnBgClick: false
        });
        var magnificPopup = $.magnificPopup.instance;
        $('.mo-cancel-btn').click(function () {
            magnificPopup.close();
        });
    });

    $("#mo-external-offer-incoming-delete-btn").click(function(){
        //rejectajax here
        var vehicle_id = global_vehicle_id;
        var token = $("input[name='_token']").val();
        $(document).ajaxStart(function(){
            $(".mo-loading-image").removeClass("hidden");
        });
        $.ajax({
            type:'POST',
            url: base_url+'/external-offer/block',
            data: {"_token": token, "vehicle_id": vehicle_id},
            success:function(data) {
                if (data != null)
                {
                    data = $.parseJSON(data);
                    console.log(data);
                    if (data.success) {
                        window.location.reload();
                    } else {
                        //show message null here
                    }
                }

            },
            error: function(data) {
                console.log("/external-offer/reject error");
            },
            complete: function() {
                $(".mo-loading-image").addClass("hidden");
            },
            global: true
        });
    });

    $(".mo-external-offer-set-location-btn").click(function(){
        global_external_offer_id = $(this).data("external-offer-id");
        $.magnificPopup.open({
            items: {
                src: "#mo-incoming-external-offer-set-location-popup"
            },
            type: 'inline',
            showCloseBtn: true,
            closeOnBgClick: false
        });
        var magnificPopup = $.magnificPopup.instance;
        $('.mo-cancel-btn').click(function () {
            magnificPopup.close();
        });

        $("#mo-external-offer-won-location-submit-btn").click(function(){
            var external_offer_id = global_external_offer_id;
            var location_id = $("#current-user-location").val();
            var token = $("input[name='_token']").val();
            $(document).ajaxStart(function(){
                $(".mo-loading-image").removeClass('hidden');
            });
            $.ajax({
                type:'POST',
                url: base_url+'/external-offer/set-location',
                data: {"_token": token, "external_offer_id": external_offer_id, "location_id": location_id},
                success:function(data) {
                    if (data != null)
                    {
                        data = $.parseJSON(data);
                        if(data.success) {
                            console.log("set location successful");
                            location.reload();
                        } else {
                            magnificPopup.close();
                            console.log(data);
                        }
                    }
                },
                error: function(data) {
                    console.log("/external-offer/set-location error");
                },
                complete: function() {
                    $(".mo-loading-image").addClass('hidden');
                },
                global: true
            });
        });
    });


/** HANDLE ACCEPT BUTTON IN PLACE MY CAR OUTGOING **/

    $(".mo-accept-offer").click(function(){
        var external_offer_id = $(this).data("external-offer-id");
        var url = base_url+"/external-offer/accept";
        updateOfferStatus(external_offer_id, url);
    });

    $(".mo-reject-offer").click(function(){
        var external_offer_id = $(this).data("external-offer-id");
        $("#mo-offer-id").val(external_offer_id);
    });

    var supendpopup = function(){
        var magnificPopup = $.magnificPopup.instance;
        $('.mo-suspend-popup').magnificPopup({
            type:'inline',
            showCloseBtn: true,
            closeOnBgClick: false
        });
        $(".mo-cancel-btn").click(function(){
            magnificPopup.close();
        });
        $("#mo-confirm-reject-offer").click(function(){
            var url = base_url+"/external-offer/reject";
            var external_offer_id = $("#mo-offer-id").val();
            updateOfferStatus(external_offer_id, url);
        });
    };
    supendpopup();

/** HANDLE BUTTON IN TRANSACTION BUTTON **/
    $(".mo-transaction-btn").click(function(e){
        var transaction_status = $(this).data("status");
        var transaction_id = $(this).data("transaction-id");
        var url = "";
        if(transaction_status == 20){
            url = base_url+"/transaction/transport-ready";
            callAjax(transaction_id, url);
            location.reload();
        }
    });

/** FUNCTION FOR DASHBOARD **/

    function showCustomizeTab(token,label_checked,id_checked){
        $.ajax({
            _token: token,
            type: "POST",
            url: base_url+"/dashboard",
            data: $("#mo-dashboard-customize-form").serialize(),
            success: function(data){
                var json_data = JSON.parse(data);
                console.log(json_data);
                $.each(json_data, function(key, value){
                    for(key1 in value){
                        if(key1 == id_checked){
                            var number_checked = value[key1];
                            console.log(number_checked);
                        }
                    }
                    var $clone = $("#mo-blocks-wrapper").children("#tab").clone();
                    $clone.find("div").addClass("green-btn");
                    $clone.attr("id", "mo_tab_"+id_checked);
                    $clone.find(".mo-block-txt").text(label_checked);
                    $clone.find(".mo-block-number").text(number_checked);
                    $clone.appendTo("#mo-blocks-wrapper").removeClass("hidden");
                });
            },
            error: function(data){
                console.log("error" + data);
            }
        });
    }

    function showCustomizeTabByTime (token){
        $.ajax({
            _token: token,
            type: "POST",
            url: base_url+"/dashboard",
            data: $("#mo-dashboard-customize-form").serialize(),
            success: function(data){
                var json_data = JSON.parse(data);
                console.log(json_data);
                $.each(json_data, function(key, value){
                    for(key1 in value){
                        $("#mo_tab_" + key1).find(".mo-block-number").text(value[key1]);
                    }
                });
            },
            error: function(data){
                console.log("error: " + data);
            }
        });
    }

    function updateOfferStatus(external_offer_id, url){
        $(document).off(".scroll");
        $(document).on("ajaxStart.updateStatus", function () {
            $(".mo-loading-image").removeClass("hidden");
        });

        var token = $("input[name='_token']").val();
        $.ajax({
            type: "POST",
            url: url,
            data: {
                _token: token,
                external_offer_id: external_offer_id
            },
            success: function (data) {
                data = $.parseJSON(data);
                console.log(data);
                if (data.success) {
                    window.location.reload();
                }
            },
            error: function(data){
                //console.log(data);
            },
            complete: function(){
                //$(".mo-loading-image").addClass("hidden");
            }
        });

        $(document).on("ajaxStop.updateStatus", function () {
            $(".mo-loading-image").addClass("hidden");
        });
    }

    function callAjax(transaction_id, url){
        console.log("transaction_id "+transaction_id);
        console.log("url "+url);
        $(document).on("ajaxStart.buttonAction", function () {
            $(".mo-loading-image").removeClass("hidden");
        });

        $.ajax({
            type: 'POST',
            data: {
                _token: token,
                transaction_id: transaction_id
            },
            url: url,
            success: function (data) {
                console.log(data);
                if(data == 1){
                    if(sessionStorage.getItem("dealership_elem_to_paging")) {
                        var data_obj = JSON.parse(sessionStorage.getItem("dealership_elem_to_paging"));
                    }
                    active = null;
                    if ($("#required-action").is(":checked")) {
                        active = true;
                    }
                    $.getTransactionsList(true, init, query, offset, data_obj.request_url, data_obj.table_id, active);
                }else{
                    /*$(".alert-danger").find(".mo-error-message").html(data);
                     $(".alert-danger").removeClass("hidden");*/
                }
            },
            error: function (data) {
                console.log('error: ' + data);
            },
            complete: function () {
                $(".mo-loading-image").addClass("hidden");
            }
        });
    }

});
