
(function ($){
	var path_name = window.location.pathname;
    var hash = window.location.hash;

	/** INITIALIZE EMPTY CUSTOMER LIST **/
    $.constructDataTable = function(id, destroy, txt_mgs) {
        $(id).DataTable({
            "destroy" : destroy,
            "columnDefs": [
                {
                    "aTargets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ],
            "language": {
                "emptyTable": txt_mgs
            },
            "paging": false,
            "info": false,
            "filter": false,
            "responsive": true,
            //initialization params as usual
            fnInitComplete : function(settings, json) {

                if ($(this).find('tbody tr').length <= 1) {
                    $(this).find('thead th').hide();
                    //$(this).parent().hide();
                    $('.mo-pagination-wrapper').hide();
                }

            }
        });
    }

    /* initDataTable */
    $.initDataTable = function(id, destroy, columns, request_url, query, offset, init_complete, error_msg, scrollX, empty_msg, active, ajax_data) {

        console.info(id, "id of table");

        if (empty_msg == null)
        {
            empty_msg = "<div>There are no valuation to display at this time.<br> Create a valuation by clicking on NEW button for specific Customer Detail or search for a valuation in the search box.</div>";
        }

        var ajax_data_datatable = function (d) {
            d._token = $("input[name='_token']").val();
            d.mo_txt_search = query;
            d.offset = offset;
            if(active){
                d.active = active
            }
            if (path_name.indexOf("test-drives/complete") != -1) {
                d.date_range_from = $("input[name='test_drive_complete_date_range']").val();
                d.date_range_to = $("input[name='test_drive_complete_to']").val();
            }
        };

        if(ajax_data){
            ajax_data_datatable = ajax_data;
        }

        $(id).DataTable({
	        "destroy" : destroy,
            /*"sScrollX": true,
            "sScrollHeadInner": "140%",
            "sScrollXInner": "140%",
            "bScrollCollapse": true,*/
            "order": [[ 0, "desc" ]],
	        "language": {
	            "emptyTable": empty_msg
	        },
	        "paging": false,
	        "columnDefs": [
	            {
	                "aTargets": [ 0 ],
	                "visible": true,
	                "searchable": false
	            }
	        ],

	        "aoColumns": columns,

	        "info": false,

	        "filter": false,

	        "pageLength": 20,

	        "lengthChange": false,

	        "responsive": true,

            "dom": 'Zlfrtip',

	        "ajax" : {
	            "url" : request_url,
	            "type" : "POST",
	            "data" : ajax_data_datatable,
	            "error" : function () {
	                 $.constructDataTable(id, true, error_msg);
	             }

	        },

	        //initialization params as usual
	        fnInitComplete : function(settings, json) {

	            if ($(this).find('tbody tr td').length<=1) {
	                $(this).find('thead th').hide();
	                //$(this).parent().hide();
	                /*$('.dataTables_paginate').hide();*/
	                $('.mo-pagination-wrapper').hide();

                    /*** RESIZE WRAPPER ***/
                    var $header = $('.mo-main-header');
                    var $wrapper = $('#wrapper');
                    var $window = $(window).on('resize', function() {
                        console.log('resize');
                        $( document ).ready(function() {
                            var height = $(this).height() - $header.height();
                            $wrapper.height(height);
                        });
                    }).trigger('resize'); //on page load

                    /** vertical align DIV **/
                    $("#mo-table-valuations-requests-list_wrapper").toggleClass('percent-h-100 vertical-center');
	            } else {
	                $('.mo-pagination-wrapper').show();
                    /** remove WRAPPER height **/
                    $("#wrapper").removeAttr('style');
	            }

                var current_item;
                $(".mo-re-price").click(function () {
                        current_item = $(this);
                        console.log(current_item.parent().parent().parent().parent().parent());
                        var valuation_id = $(this).data("valuation-id");
                        $("input[name='valuation_id']").val(valuation_id);
                        $("#estimate-cost").val(null);
                });


                /* Popup for RE-EVALUATE in Complete Valuations */
                if (path_name.indexOf("valuations/complete") != -1) {
                    var magnificPopup = $.magnificPopup.instance;

                    $('.open-popup-link').magnificPopup({
                        type:'inline',
                        showCloseBtn: true,
                        closeOnBgClick: false
                    });

                    $('.mo-cancel-btn').click(function (){
                        magnificPopup.close();
                    });

                    $("#datetimepicker1").datepicker({
                        format: 'yyyy-mm-dd',
                        todayHighlight: true,
                        orientation: 'bottom',
                        autoclose: true
                    });

                    //var current_item = null;
                    $.clearTradePoupForm = function(){
                        $("input[name='arrival_date']").val(null);
                        $("input[name='trade_in_price']").val(null);
                        $("#note").val(null);
                        change_status_form_validator.resetForm();
                    }

                    $(".mo-trade").click(function () {
                        //call clear function before binding data
                        $.clearTradePoupForm();
                        $("#mo-update-trade-button").data("max-price", $(this).data("max-price"));
                        $("#mo-popup-changestatus-form").data("max-price", $(this).data("max-price"));

                        var max_price = accounting.formatMoney($(this).data("max-price"), "", 2, ",", ".");
                        $(".complete-valuation-max-price-number").text(max_price);

                        current_item = $(this);
                        console.log($(this).data("valuation-id"));
                        var valuation_id = $(this).data("valuation-id");

                        $("input[name='valuation_id']").val(valuation_id);

                        //ajax
                        //load valuation price
                        var token = $("input[name='_token']").val();
                        $.ajax({
                            type: "POST",
                            url: "/_get-sales-info",
                            headers: {"X-CSRF-TOKEN": token},
                            data: {"valuation_id": valuation_id},

                            success: function(data){
                                data = JSON.parse(data);
                                $("#mo-final-trade-in-price").text(data.valuation_price);
                            },

                            error: function(data){
                                console.log("error");
                            }
                        });
                    });

                    /******************************* VALUATION VERSION ****************************/
                    var valuation_status_selected = null;
                    var valuation_id_global = null;
                    $(".mo-valuation-version").click(function(e){
                        e.preventDefault();
                        current_item = $(this);
                        console.log($(this).data("valuation-id"));
                        var valuation_id = $(this).data("valuation-id");
                        valuation_id_global = valuation_id;
                        console.log($(this).data("valuation-status"));
                        valuation_status_selected = $(this).data("valuation-status");
                        // var valuation_status = $(this).data("valuation-status");
                        // $("input[name=valuation_status]").val(valuation_status);
                        $("input[name='valuation_id']").val(valuation_id);
                        $("input[name='valuation_status']").val(valuation_status_selected);

                        var token = $("input[name='_token']").val();
                        $.ajax({
                            type: 'POST',
                            url: '/dealership/get-version',
                            headers: {"X-CSRF-TOKEN": token},
                            data: {
                                "valuation_id": valuation_id,
                                "valuation_status": valuation_status_selected
                            },
                            success: function(data){
                                if(data.length != "" ){
                                    /*console.log(data);*/
                                    if(data == 1){
                                        window.location.replace("/review-valuation/"+valuation_id);
                                    }
                                    else{

                                        /** open magnific popup manually **/
                                        $.magnificPopup.open({
                                            items: {
                                                src: "#mo-popup-valuation-version"
                                            },
                                            type: 'inline',
                                            showCloseBtn: true,
                                            closeOnBgClick: false
                                        });
                                        var magnificPopup = $.magnificPopup.instance;
                                        $('.mo-cancel-btn').click(function (){
                                            $(".alert-danger").addClass("hidden");
                                            magnificPopup.close();
                                        });

                                    }

                                    /**** Start Datatable for valuation version ****/
                                    $('#mo-table-valuation-version-list').DataTable({
                                        "destroy": true,
                                        "info": false,
                                        "filter": false,
                                        "columnDefs": [
                                            {
                                                "aTargets": [ 0 ],
                                                "visible": true,
                                                "searchable": false
                                            }
                                        ],
                                        "order": [[ 1, "desc" ]],
                                        data: $.parseJSON(data),
                                        columns: [
                                            { data: 'valuation_id', className: "never", "bSortable": false },
                                            { data: 'version', className: "all", "bSortable": false, "width": "25%" },
                                            { data: 'version_creator', className: "all", "bSortable": false, "width": "40%" },
                                            { data: 'create_date', className: "all", "bSortable": false, "width": "25%" },
                                            { data: 'access', className: "never", "bSortable": false, "width": "25%" },
                                            { className: "all", "bSortable": false, "width": "10%",
                                                 "mRender": function(data, type, row, meta){
                                                     //console.debug(row);
                                                     return '<a href="/review-valuation/'+row['valuation_id']+'" class="mo-valuation-version-redirect" ' +
                                                         'data-valuation-id="'+valuation_id_global+'" data-from-version="'+row['version']+'"'+
                                                         ' data-valuation-version-id="'+row['valuation_id']+'"' +
                                                         ' data-access="'+row['access']+'">' +
                                                         '<span class="glyphicon glyphicon-menu-right" ></span></a>'
                                                 }
                                            }
                                        ],
                                        "lengthChange": false,
                                        "paging": false,
                                        "responsive": true,
                                    });

                                    $(".mo-valuation-version-redirect").click(function(e){
                                        e.preventDefault();

                                        var valuation_id = $(this).data('valuation-id');
                                        var valuation_version_id = $(this).data('valuation-version-id');
                                        var access = $(this).data('access');
                                        var from_version = $(this).data('from-version');
                                        $('input[name="valuation_id"]').val(valuation_id);
                                        $('input[name="from_version"]').val(from_version);
                                        var valuation_status = $('input[name="valuation_status"]').val();

                                        var token = $('input[name="_token"]').val();
                                        /** CHECK REDIRECT FOR VIEW ONLY IF DIFFERENT VALUER ACCESS **/
                                        //console.log(access);
                                        if(access == "view"){
                                            if(valuation_status == "Complete" || valuation_status == "Traded"){
                                                $(".alert-danger").removeClass("hidden");
                                            }else{
                                                window.location.replace("/review-valuation/"+valuation_version_id);
                                            }

                                        }else if(access == "create"){
                                            $(document).ajaxStart(function() {
                                                $(".mo-loading-image").removeClass("hidden");
                                            });
                                            $.ajax({
                                                type: "POST",
                                                url: "/dealership/create-version",
                                                headers: {"X-CSRF-TOKEN": token},
                                                data: {
                                                    "valuation_id": valuation_id,
                                                    "valuation_status": valuation_status_selected,
                                                    "from_version": from_version,
                                                    "access": access
                                                },
                                                success: function(data){
                                                    console.log(data);
                                                    if(data == 1) {
                                                        // console.log(data);
                                                        window.location.replace("/review-valuation/"+valuation_id);
                                                    }else{
                                                        // location.reload();
                                                        $(".alert-danger").removeClass("hidden");
                                                    }
                                                },
                                                error: function(data){
                                                    // console.log(data);
                                                },
                                                complete: function(){
                                                    $(".mo-loading-image").addClass("hidden");
                                                }
                                            });
                                            $(document).ajaxStop(function() {
                                                $(".mo-loading-image").addClass("hidden");
                                            });
                                        }

                                    });
                                }else{
                                     window.location.replace("/review-valuation/"+ valuation_id);
                                }
                            },
                            error: function(data){
                                // console.log(data);
                            }

                        });
                    });

                    /******************************* END VALUATION VERSION ****************************/

                    /* TRADE VEHICLE */
                    $.validator.addMethod('lessThanEqual', function(value, element, param) {
                        return this.optional(element) || parseInt(value) <= parseInt($(param).val());
                    });

                    var change_status_form_validator = $("#mo-popup-changestatus-form").validate({
                        rules: {
                            arrival_date: "required",
                            trade_in_price: {
                                required: true
                            }
                        },
                        errorElement: 'div',
                        errorClass: 'mo-error-wrapper',
                        errorPlacement: function(error, element){

                            if(element.attr('name') == 'arrival_date') {
                                error.insertAfter(element.parent());
                            } else {
                                error.insertAfter(element);
                            }
                        },

                        messages: {
                            arrival_date: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
                            trade_in_price: {
                                lessThanEqual: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Price must be less than or equal to max price </span>"+"</div>",
                                required: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>"
                            }
                        }
                    });

                    $("#mo-update-trade-button").click(function(e) {

                        if(!$("#mo-popup-changestatus-form").valid()) {
                            return false;
                        }

                        if($("#mo-final-trade-in-price").val() > $(this).data("max-price")) {
                            var error = "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'><span>Price must be less than or equal to max price </span></div>";
                            $("#mo-final-trade-in-price").parent().append(error);
                            return false;
                        }
                        //ajax update trade
                        var token = $("input[name='_token']").val();
                        var valuation_id = $("input[name='valuation_id']").val();
                        var note = $("#note").val();
                        var trade_in_price = $("#mo-final-trade-in-price").val();
                        var arrival_date = $("#arrival-date").val();

                        $.ajax({
                            type: "POST",
                            url: "/_trade",
                            headers: {"X-CSRF-TOKEN": token},
                            data: {"valuation_id" : valuation_id, "note" : note,
                                    "trade_in_price" : trade_in_price,
                                    "arrival_date" : arrival_date},

                            success: function(data) {
                                console.log("success");
                                console.log(data);
                                //set status traded
                                current_item.parent().parent().parent().parent().prev().text("Traded");
                                magnificPopup.close();
                            },

                            error: function(data){
                                console.log("error");
                            }
                        });
                    });

                    $("#mo-final-trade-in-price").blur(function() {
                        this.value = parseFloat(this.value.replace(/,/g, ""))
                        .toFixed(2)
                        .toString()
                        .replace(/\B(?=(\d{10})+(?!\d))/g, ",");
                    });
                    /* end TRADE VEHICLE */

                    /* VALUATION CHANGESTATUS */
                    $("#mo-re-evaluate-popup").click(function () {
                        $("#mo-popup-re-evaluate").validate({
                            rules: {
                                re_evaluate_price: "required"
                            },
                            messages: {
                                re_evaluate_price: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter re-evaluate</span>" + "</div>"
                            },
                            submitHandler: function (form) {
                                form.submit();
                            }

                        });
                    });

                    // $("#mo-popup-changestatus").click(function () {
                    //     $("#mo-popup-changestatus-form").validate({

                    //         rules: {
                    //             proposed_new_price  : "required"
                    //         },
                    //         messages: {
                    //             proposed_new_price: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter proposed new price</span>" + "</div>"
                    //         },
                    //         submitHandler: function (form) {
                    //             form.submit();

                    //         }
                    //     });
                    // });

                    $("#mo-re-evaluate-save-btn").click(function() {
                        //get price
                        var price = $("input[name='re_evaluate_price']").val();
                        var token = $("input[name='_token']").val();
                        var valuation_id = $("input[name='valuation_id']").val();

                        //ajax call api
                        $.ajax({
                            type: "POST",
                            url: "/_re-evaluate",
                            headers: {"X-CSRF-TOKEN": token},
                            data: {"valuation_id" : valuation_id, "re_evaluate_price" : price},

                            success: function(data) {
                                current_item.parent().parent().parent().parent().parent().hide()
                                console.log("success");
                                magnificPopup.close();
                            },

                            error: function(data) {
                                console.log(data);
                                console.log("error");
                            }

                        });
                        //done
                    });

                    $("#estimate-cost").blur(function() {
                        this.value = parseFloat(this.value.replace(/,/g, ""))
                        .toFixed(2)
                        .toString()
                        .replace(/\B(?=(\d{10})+(?!\d))/g, ",");
                    });
                }

                //request valuation
                if(path_name.indexOf("/valuations/requests") != -1) {
                    var magnificPopup = $.magnificPopup.instance;
                    $('.open-popup-link').magnificPopup({
                        type:'inline',
                        showCloseBtn: true,
                        closeOnBgClick: false
                    });
                }

                //in-progress valuation
                if(path_name.indexOf("/valuations/in-progress") != -1) {
                    var magnificPopup = $.magnificPopup.instance;
                    $('.open-popup-link').magnificPopup({
                        type:'inline',
                        showCloseBtn: true,
                        closeOnBgClick: false
                    });
                }

                //pending valuation
                if(path_name.indexOf("/valuations/pending") != -1) {
                    var magnificPopup = $.magnificPopup.instance;
                    $('.open-popup-link').magnificPopup({
                        type:'inline',
                        showCloseBtn: true,
                        closeOnBgClick: false
                    });

                    $(".pending-more-info").click(function(){
                        if ($(this).data("status") == 4) {
                            var row = $(this);
                            setTimeout(function(){
                                $(row.parent().next(".child").children().children().children()[3]).hide();
                                console.log((row.parent().next(".child").children().children().children()[3]));
                            }, 200);
                        }
                    });
                }

                /* users-list */
                if(path_name.indexOf("/users-list") != -1){
                    var status = null;
                    var user_id = null;
                    $(".mo-user-suspend").click(function() {
                        status = $(this).data("status");
                        user_id = $(this).data("user-id");
                        /*console.log("user_id "+user_id);
                        console.log("status "+status);*/

                        if (status == 0) {
                            $(".suspend-popup-content").text("Are you sure you want to restore this account?");
                            $(".mo-popup-heading").text("Restore");
                        } else {
                            $(".suspend-popup-content").text("Are you sure you want to suspend this account?");
                            $(".mo-popup-heading").text("Suspend");
                        }

                    });

                    $("#mo-user-change-status").click(function() {
                        $(document).ajaxStart(function() {
                            $(".mo-loading-image").removeClass("hidden");
                        });
                        var token = $("input[name=_token]").val();
                        if (status == 0) {
                            status = 1;
                            $(".suspend-popup-content").val("Are you sure you want to restore this account?");
                            $(".mo-popup-heading").val("Restore");
                        } else {
                            $(".suspend-popup-content").val("Are you sure you want to suspend this account?");
                            $(".mo-popup-heading").val("Suspend");
                            status = 0;
                        }
                        $.ajax({
                            type: "POST",
                            url: "/dealership/update-status",
                            headers: {"X-CSRF-TOKEN": token},
                            data: {"userId" : user_id,"status" : status},

                            success: function(result) {
                                console.log(result);
                                if(result == 1){
                                    magnificPopup.close();
                                    location.reload();
                                }
                            },
                            error: function() {
                                console.log("Error");
                            },
                            complete: function(){
                                $('.mo-loading-image').addClass("hidden");
                            }
                        });
                    });

                    $(".mo-cancel-btn").click(function() {
                        magnificPopup.close();
                    });

                    var magnificPopup = $.magnificPopup.instance;
                    $('.mo-suspend-popup').magnificPopup({
                        type:'inline',
                        showCloseBtn: true,
                        closeOnBgClick: false,
                        callbacks: {
                            beforeClose: function(){

                            }
                        }
                    });
                }

                /* dealership/roles */
                if(path_name.indexOf("dealership/roles") != -1){
                    var role_id = null;
                    $(".mo-delete-btn").click(function() {
                        role_id = $(this).data("role-id");
                        console.log("role_id "+role_id);
                    });
                    $("#mo-confirm-delete").click(function() {
                        $(document).ajaxStart(function() {
                            $(".mo-loading-image").removeClass("hidden");
                        });
                        var token = $("input[name=_token]").val();
                        $.ajax({
                            type: "POST",
                            url: "/dealership/delete-role/"+role_id,
                            headers: {"X-CSRF-TOKEN": token},
                            success: function(result) {
                                console.log(result);
                                if(result == 1){
                                    magnificPopup.close();
                                    location.reload();
                                }
                            },
                            error: function() {
                                console.log("Error");
                            },
                            complete: function(){
                                $('.mo-loading-image').addClass("hidden");
                            }
                        });
                    });

                    $(".mo-cancel-btn").click(function() {
                        magnificPopup.close();
                    });

                    var magnificPopup = $.magnificPopup.instance;
                    $('.mo-delete-popup').magnificPopup({
                        type:'inline',
                        showCloseBtn: true,
                        closeOnBgClick: false,
                        callbacks: {
                            beforeClose: function(){

                            }
                        }
                    });
                }

                if(path_name.indexOf("/test-drives/search-vehicle") != -1){
                    $(".mo-select-vehicle").click(function(){
                        sessionStorage.setItem("manual_vehicle", 0);
                    });
                }

                /* external-offer-groups */
                if (path_name.indexOf("external-offer-groups") != -1) {
                    var external_id = null;
                    $("#mo-remove-external-offer-group").click(function() {
                        external_id = $(this).data("external-id");
                    });
                    $("#mo-confirm-delete").click(function() {
                        $(document).bind("ajaxStart.mine", function() {
                            $(".mo-loading-image").removeClass("hidden");
                        });
                        var token = $("input[name=_token]").val();
                        $.ajax({
                            type: "POST",
                            url: "/dealership/remove-external-offer-group/"+external_id,
                            headers: {"X-CSRF-TOKEN": token},
                            data: null,
                            success: function(data) {
                                console.log(data);
                                if(data == 1) {
                                    magnificPopup.close();
                                    location.reload();
                                }
                            },
                            error: function() {
                                console.log("Error");
                            },
                            complete: function(){
                                $('.mo-loading-image').addClass("hidden");
                            }
                        });
                    });
                    $(".mo-cancel-btn").click(function() {
                        magnificPopup.close();
                    });

                    var magnificPopup = $.magnificPopup.instance;
                    $('.mo-remove-popup').magnificPopup({
                        type:'inline',
                        showCloseBtn: true,
                        closeOnBgClick: true
                    });
                }

                /********* TEST DRIVES IN PROGRESS AND COMPLETE**********/
                if(path_name.indexOf("/test-drives/inprogress") != -1) {
                    $("#mo-test-drive-return-vehicle-form").validate({
                        rules: {
                            mo_marketing_option: "required",
                            test_drive_note: "required",
                            mo_option_other: "required"
                        },
                        highlight: function(element) {
                            $(element).closest('.input-group').addClass('has-error');
                        },
                        unhighlight: function(element) {
                            $(element).closest('.input-group').removeClass('has-error');
                        },
                        messages: {
                            mo_marketing_option: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                            test_drive_note: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                            mo_option_other: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>"
                        }
                    });

                    $(".mo-test-drives-inprogress-btn").click(function(e) {
                        //clear ratting data here
                        $("#slider_vehicle_rating").slider('value',null);
                        $("#slider_mo_rating").slider('value',null);
                        $("#test-drive-note").val(null);
                        $("#mo_marketing_option").val(null);

                        e.preventDefault();
                        var inprogress_id = $(this).data("inprogress-id");
                        var token = $("input[name='_token']").val();
                        var make = $(this).data("make-name");
                        var model = $(this).data("model-name");
                        var year = $(this).data("year");
                        // var year_arr = year.split("-");
                        // $(".mo-popup-heading").text(year_arr[0] + " " + make + " " + model);
                        $(".mo-popup-heading").text(year + " " + make + " " + model);
                        $.magnificPopup.open({
                            items: {
                                src: "#mo-test-drives-comment-popup"
                            },
                            type: 'inline',
                            showCloseBtn: true,
                            closeOnBgClick: false
                        });

                        var magnificPopup = $.magnificPopup.instance;
                        $('.mo-cancel-btn').click(function () {
                            $("#mo-marketing-option-other").addClass("hidden");
                            $(".mo-error-wrapper").empty();
                            magnificPopup.close();
                            $("#errorToShow").html("");
                        });

                        $("#mo_marketing_option").change(function(){
                            if($("#mo_marketing_option").find(":selected").text() == "Other"){
                                $("#mo-marketing-option-other").removeClass("hidden");
                            }else{
                                $("#mo-marketing-option-other").addClass("hidden");
                            }
                        });

                        $("#mo-test-drives-inprogress-change-status").click(function(e) {

                            e.preventDefault();
                            // $(document).ajaxStart(function() {
                            //     $(".mo-loading-image").removeClass("hidden");
                            // });
                            /*if($("#test-drive-note").val() == "") {
                                $("#errorToShow").html("<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>");
                            }*/
                            if(!$("#mo-test-drive-return-vehicle-form").valid()){
                                return false;
                            }
                            else {
                                $(document).ajaxStart(function(){
                                    $(".mo-loading-image").removeClass("hidden");
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "/test-drives/complete/" + inprogress_id,
                                    data: {
                                        _token: token,
                                        "test_drive_note": $("#test-drive-note").val(),
                                        "vehicle_rating": $('#slider_vehicle_rating').slider("option", "value"),
                                        "service_rating": $('#slider_mo_rating').slider("option", "value"),
                                        "marketing_code": $("#mo_marketing_option").val(),
                                        "marketing_option": $("#mo-option-other").val(),
                                    },
                                    success: function(data) {
                                        if(data == 1) {
                                            window.location.replace("/test-drives/complete");
                                        }
                                    },
                                    error: function(data) {
                                        //console.log(data);
                                    },
                                    complete: function(){
                                        $(".mo-loading-image").addClass("hidden");
                                    }
                                });
                            }
                        });
                    });
                }

                if(path_name.indexOf("/test-drives/complete") != -1) {

                    $(".mo-test-drives-read-comment-btn").click(function(e) {

                        $("#test-drive-note").attr('readonly','readonly');
                        $("#test-drive-note").css('background-color','#ffffff');
                        var make = $(this).data("make-name");
                        var model = $(this).data("model-name");
                        var year = $(this).data("year");
                        // var year_arr = year.split("-");

                        var vehicle_rating = $(this).data("vehicle-rating");
                        $("#slider_vehicle_rating").slider('value',vehicle_rating);
                        $("#slider_vehicle_rating").on("slide", function (event, ui) { return false; });

                        var service_rating = $(this).data("service-rating");
                        $("#slider_mo_rating").slider('value',service_rating);
                        $("#slider_mo_rating").on("slide", function (event, ui) { return false; });

                        var marketing_code = $(this).data("marketing-code");
                        var marketing_option = $(this).data("marketing-option")
                        if(marketing_code == "Other"){
                            $("#mo_marketing_option_other").removeClass("hidden");
                            $("#mo_marketing_option_other").val(marketing_option);
                        }
                        $("#mo_marketing_option").val(marketing_code);
                        // $(".mo-popup-heading").text(year_arr[0] + " " + make + " " + model);
                        $(".mo-popup-heading").text(year + " " + make + " " + model);
                        // $(".mo-popup-heading").
                        e.preventDefault();
                        var completed_id = $(this).data("completed-id");
                        var token = $("input[name='_token']").val();
                        $.magnificPopup.open({
                            items: {
                                src: "#mo-test-drives-read-comment-popup"
                            },
                            type: "inline",
                            showCloseBtn: true,
                            closeOnBgClick: false
                        });
                        var magnificPopup = $.magnificPopup.instance;
                        $('.mo-cancel-btn').click(function(){
                            magnificPopup.close();
                            $("#mo_marketing_option_other").addClass("hidden");
                        });
                        $("#test-drive-note").val($(this).data("read-comment"));
                    });

                }

                /********* end TEST DRIVES IN PROGRESS AND COMPLETE ***********/




                /********* CUSTOMER DETAIL **********/
                /* customer-details */
                if(path_name.indexOf("customer-details") != -1)
                {
                    var interaction_id = null;
                    /** Delete interaction **/
                    $(".mo-remove-interaction").click(function(){
                        interaction_id = $(this).data("interaction-id");
                    });

                    $("#mo-confirm-delete").click(function(e){
                        e.preventDefault();
                        $(document).bind("ajaxStart.mine", function() {
                            $(".mo-loading-image").removeClass("hidden");
                        });

                        var token = $("input[name='_token']").val();
                        var contact_id =  $("input[name='customer_id']").val();
                        $.ajax({
                            type : "POST",
                            url : "/delete-interaction/"+interaction_id,
                            headers: {"X-CSRF-TOKEN": token},
                            data : {
                                "contact_id": contact_id
                            },
                            success: function(data){
                                if(data == 1){
                                    console.log(data);
                                    magnificPopup.close();
                                    location.reload();
                                }
                            },
                            error: function(data){
                                console.log(data);
                            },
                            complete: function(){
                                $('.mo-loading-image').addClass("hidden");
                            }
                        });
                    });

                    var magnificPopup = $.magnificPopup.instance;
                    $('.mo-remove-popup').magnificPopup({
                        type:'inline',
                        showCloseBtn: true,
                        closeOnBgClick: true
                    });

                    /** Edit Interaction **/
                    // var interaction_id = null;
                    //add comment text to textarea
                    $(".mo-edit-interaction").click(function(e){
                        e.preventDefault();
                        interaction_id = $(this).data("interaction-id");
                        $("#edit-date-time").val($(this).data("interaction-datetime"));
                        $("#edit-type").val($(this).data("interaction-type"));

                        var comment = $(this).parent().parent().prev().children(".interaction-comment").html();
                        comment = comment.replace(/<br>/g, "\n").replace(/&nbsp; /g, " ");

                        $("#edit-note").val(comment);
                    });

                    $("#interaction_update_btn").click(function(){
                        // e.preventDefault();
                        var token = $("input[name='_token']").val();
                        var contact_id =  $("input[name='customer_id']").val();
                        $.ajax({
                            type: "POST",
                            url: "/update-interaction/"+interaction_id,
                            headers: {"X-CSRF-TOKEN": token},
                            data: {
                                date_time: $("#edit-date-time").val(),
                                type: $("#edit-type").val(),
                                note: $("#edit-note").val(),
                                "contact_id": contact_id
                            },
                            success: function(data){
                                 if(data == 1){
                                    magnificPopup.close();
                                    location.reload();
                                }
                            },
                            error: function(data){
                                // console.log(data);
                            }
                        });

                        return false;
                    });

                    var magnificPopup = $.magnificPopup.instance;
                    $(".open-popup-link").magnificPopup({
                        type:'inline',
                        showCloseBtn: true,
                        closeOnBgClick: true
                    });

                    /** Test Drive **/
                    $("#mo-test-drive-return-vehicle-form").validate({
                        rules: {
                            mo_marketing_option: "required",
                            test_drive_note: "required",
                            mo_option_other: "required"
                        },
                        highlight: function(element) {
                            $(element).closest('.input-group').addClass('has-error');
                        },
                        unhighlight: function(element) {
                            $(element).closest('.input-group').removeClass('has-error');
                        },
                        messages: {
                            mo_marketing_option: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                            test_drive_note: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                            mo_option_other: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>"
                        }
                    });

                    $(".mo-test-drives-inprogress-btn").click(function(e){
                        $("#mo-test-drives-inprogress-change-status").removeClass("hidden");
                        $("#test-drive-note").val(null);
                        e.preventDefault();

                        //clear ratting data here
                        $("#slider_vehicle_rating").slider('value',null);
                        $("#slider_vehicle_rating").unbind("slide");
                        $("#slider_mo_rating").slider('value',null);
                        $("#slider_mo_rating").unbind("slide");
                        $("#test-drive-note").val(null);
                        $("#test-drive-note").attr('readonly', false);
                        $("#mo_marketing_option").val(null);

                        var test_drive_id = $(this).data("test-drive-id");
                        var make = $(this).data("make-name");
                        var model = $(this).data("model-name");
                        var year = $(this).data("year");
                        // var year_arr = year.split("-");
                        var token = $("input[name='_token']").val();

                        $("#mo_marketing_option").change(function(){
                            if($("#mo_marketing_option").find(":selected").text() == "Other"){
                                $("#mo-marketing-option-other").removeClass("hidden");
                            }else{
                                $("#mo-marketing-option-other").addClass("hidden");
                            }
                        });

                        // $(".mo-popup-heading").text(year_arr[0] + " " + make + " " + model);
                        $(".mo-popup-heading").text(year + " " + make + " " + model);

                        $.magnificPopup.open({
                            items: {
                                src: "#mo-test-drives-comment-popup"
                            },
                            type: 'inline',
                            showCloseBtn: true,
                            closeOnBgClick: false
                        });

                        var magnificPopup = $.magnificPopup.instance;
                        $('.mo-cancel-btn').click(function (){
                            magnificPopup.close();
                            $("#errorToShow").html("");
                        });
                        $("#mo-test-drives-inprogress-change-status").click(function(e){
                            e.preventDefault();
                            /*if($("#test-drive-note").val() == ""){
                                $("#errorToShow").html("<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>");
                            }*/
                            if(!$("#mo-test-drive-return-vehicle-form").valid()){
                                return false;
                            }
                            else{
                                $(document).ajaxStart(function(){
                                    $(".mo-loading-image").removeClass("hidden");
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "/test-drives/complete/" + test_drive_id,

                                    data: {
                                        _token: token,
                                        "test_drive_note": $("#test-drive-note").val(),
                                        "vehicle_rating": $('#slider_vehicle_rating').slider("option", "value"),
                                        "service_rating": $('#slider_mo_rating').slider("option", "value"),
                                        "marketing_code": $("#mo_marketing_option").val(),
                                        "marketing_option": $("#mo-option-other").val()
                                    },
                                    success: function(data){
                                        if(data == 1){
                                            location.reload();
                                        }
                                    },
                                    error: function(data){
                                        //console.log(data);
                                    },
                                    complete: function(){
                                        $(".mo-loading-image").addClass("hidden");
                                    }
                                });
                            }
                        });
                    });

                    $(".mo-test-drives-read-comment-btn").click(function(e){

                        e.preventDefault();
                        $("#test-drive-note").attr('readonly','readonly');
                        $("#test-drive-note").css('background-color','#ffffff');
                        $("#mo_marketing_option").addClass("hidden");
                        $("#mo-marketing-option").removeClass("hidden")

                        var test_drive_id = $(this).data("test-drive-id");
                        var token = $("input[name='_token']").val();
                        var make = $(this).data("make-name");
                        var model = $(this).data("model-name");
                        var year = $(this).data("year");
                        // var year_arr = year.split("-");

                        var vehicle_rating = $(this).data("vehicle-rating");
                        $("#slider_vehicle_rating").slider('value',vehicle_rating);
                        $("#slider_vehicle_rating").on("slide", function (event, ui) { return false; });

                        var service_rating = $(this).data("service-rating");
                        $("#slider_mo_rating").slider('value',service_rating);
                        $("#slider_mo_rating").on("slide", function (event, ui) { return false; });

                        var marketing_code = $(this).data("marketing-code");
                        var marketing_option = $(this).data("marketing-option");
                        console.log(marketing_code);
                        console.log(marketing_option);
                        if(marketing_code == "Other"){
                            $("#mo-marketing-option-other").removeClass("hidden");
                            $("#mo-option-other").prop("disabled", true);
                            $("#mo-option-other").val(marketing_option);
                        }
                        $("#mo_marketing_code").val(marketing_code);


                        // $(".mo-popup-heading").text(year_arr[0] + " " + make + " " + model);
                        $(".mo-popup-heading").text(year + " " + make + " " + model);

                        $.magnificPopup.open({
                            items: {
                                src: "#mo-test-drives-comment-popup"
                            },
                            type: 'inline',
                            showCloseBtn: true,
                            closeOnBgClick: false
                        });
                        var magnificPopup = $.magnificPopup.instance;
                        $('.mo-cancel-btn').click(function (){
                            $("#mo-marketing-option-other").addClass("hidden");
                            magnificPopup.close();
                        });
                        $("#test-drive-note").val($(this).data("read-comment"));
                        $("#mo-test-drives-inprogress-change-status").addClass("hidden");
                    });

                }

                /********* END CUSTOMER DETAIL *********/

	            if (init_complete) {

	            	console.info(json, "json while init complete");
                    if (typeof (json.test_drive_count) != "undefined") {
                        $("#test-drive-count").text(json.test_drive_count);
                    } else if (typeof (json.valuation_count) != "undefined") {
                        $("#valuation-count").text(json.valuation_count);
                    }
	                page_length = data_length = Math.ceil(json.count/20);

                    if(json.count <= 20){
                        $(".mo-pagination-wrapper").hide();
                    }

                    var activeClass = '';
                    $(".mo-page-number-wrap").remove();//remove number in pagination when reinitialize data table
	                for(var i = data_length; i >= 1; i--) {
                        if(i == 1) {
                            activeClass = 'active';
                        } else {
                            activeClass = '';
                        }
	                    $("<li class='mo-page-number-wrap'><a class='mo-page-number "+activeClass+"' href='#'>"+i+"</a></li>").insertAfter(".mo-page-number-list");
	                }

                    /** PAGE NUMBER **/
	                $(".mo-page-number").click(function() {

	                    $(".mo-page-number").removeClass("active");
	                    var clicked = $(this);
	                    clicked.addClass("active");
	                    current_page = clicked.html();

	                    var offset = (clicked.html() - 1)*20;
	                    console.log("offset "+offset);
                        if(path_name.indexOf("user") != -1){
                            var destroy = true;
                            var init = false;
                            var query = $("#mo-txt-search").val();
                            var active = true;
                            if ($("#mo-filter-user").is(":checked")) {
                                active = true;
                            } else {
                                active = false;
                            }
                            $.getUsers(destroy, active, init, query, offset);
                        }else if (path_name.indexOf("transaction/view-transactions") != -1) {
                            if(sessionStorage.getItem("dealership_elem_to_paging")) {
                                var data_obj = JSON.parse(sessionStorage.getItem("dealership_elem_to_paging"));
                                if(offset && data_obj.ajax_data){
                                    data_obj.ajax_data.offset = offset;
                                    active = data_obj.ajax_data.active;
                                }
                                $.getTransactionsList(true, false, null, offset, data_obj.request_url, data_obj.table_id, active, data_obj.ajax_data);
                            }
                        }else{
                            $.initDataTable(id,true, columns, request_url, query, offset, false, error_msg);
                        }
	                });
                    /** END PAGE NUMBER **/

	            }

                $(".cancel-valuation").click(function() {
                    var valuation_id = $(this).siblings('#in-progress-valuation-id').val();
                    var row = $(this).parents('tr');

                    $("#mo-valuation-progress-cancel-confirm-btn").data("valuation_id", valuation_id);
                    $("#mo-valuation-progress-cancel-confirm-btn").data("row", row);
                });

                $("#mo-valuation-progress-cancel-confirm-btn").click(function(){
                    var valuation_id = $(this).data("valuation_id");
                    var row = $(this).data("row");
                    $(document).ajaxStart(function(){
                        $(".mo-loading-image").removeClass("hidden");
                    });
                    $.ajax({
                        type: "GET",
                        url: "/cancel-valuation/"+valuation_id,
                        success: function(result) {
                            if(result == 1) {
                                //row.remove();
                                row.hide();
                                magnificPopup.close();
                            }
                        },
                        error: function() {
                            console.log('Error occured');
                        },
                        complete: function(){
                            $(".mo-loading-image").addClass("hidden");
                        }
                    });

                });

                $(".delete-valuation").click(function() {
                    var valuation_id = $(this).parent().siblings('.request-valuation-id').val();
                    var row = $(this).parents('tr');
                    $("#mo-confirm-btn").data("valuation_id", valuation_id);
                    $("#mo-confirm-btn").data("row", row);
                });

                //delete valuation
                $("#mo-confirm-btn").click(function(){
                    var valuation_id = $(this).data("valuation_id");
                    var row = $(this).data("row");

                    $.ajax({
                        type: "GET",
                        url: "/delete-valuation/"+valuation_id,
                        success: function(result){
                            if(result == 1) {
                                //row.remove();
                                row.hide();
                                magnificPopup.close();
                            }
                        },
                        error: function() {
                            console.log('Delete valuation error occured');
                        }
                    });

                });

                // APPROVE PRICE
                $(".mo-approve-price-btn").click(function() {

                    var valuation_id = $(this).data("valuation-id");
                    var token = $("input[name='_token']").val();
                    var pending_row = $(this).parent().parent();

                    $("#mo-valuation-pending-approval-confirmation-btn").data("valuation_id", valuation_id);
                    $("#mo-valuation-pending-approval-confirmation-btn").data("token", token);
                    $("#mo-valuation-pending-approval-confirmation-btn").data("pending_row", pending_row);
                    $.magnificPopup.open({
                        items: {
                            src: "#mo-sensitive-action-confirm-popup"
                        },
                        type: 'inline',
                        showCloseBtn: true,
                        closeOnBgClick: false
                    });
                    var magnificPopup = $.magnificPopup.instance;
                    $('.mo-cancel-btn').click(function () {
                        magnificPopup.close();
                        // $("#errorToShow").html("");
                    });

                    $("#mo-valuation-pending-approval-confirmation-btn").click(function(){
                        var valuation_id = $(this).data("valuation_id");
                        var token = $(this).data("token");
                        var pending_row = $(this).data("pending_row");
                        $(document).ajaxStart(function(){
                            $(".mo-loading-image").removeClass("hidden");
                        });
                        $.ajax({
                            type: "POST",
                            url: "/approve-revaluation",
                            headers: {"X-CSRF-TOKEN": token},
                            data: {"valuation_id" : valuation_id},
                            success: function(result) {
                                console.log(result);
                                //pending_row.next("tr").remove();
                                magnificPopup.close();
                                pending_row.remove();
                            },
                            error: function() {

                            },
                            complete: function(){
                                $(".mo-loading-image").addClass("hidden")
                            }
                        });
                    });


                });

                // REJECT PRICE
                $(".mo-reject-price-btn").click(function() {
                    var valuation_id = $(this).data("valuation-id");
                    var token = $("input[name='_token']").val();
                    var pending_row = $(this).parent().parent();

                    $.ajax({
                        type: "POST",
                        url: "/reject-revaluation",
                        headers: {"X-CSRF-TOKEN": token},
                        data: {"valuation_id" : valuation_id},
                        success: function(result) {
                            console.log(result);
                            pending_row.next("tr").remove();
                            pending_row.remove();

                        },
                        error: function() {

                        }
                    });

                });

                // APPROVE VALUATION
                $(".mo-approve-valuate-btn").click(function() {

                    var valuation_id = $(this).data("valuation-id");
                    var token = $("input[name='_token']").val();
                    var pending_row = $(this).parent().parent().parent();

                    $("#mo-valuation-pending-approval-confirmation-btn").data("valuation_id", valuation_id);
                    $("#mo-valuation-pending-approval-confirmation-btn").data("token", token);
                    $("#mo-valuation-pending-approval-confirmation-btn").data("pending_row", pending_row);

                });

                $("#mo-valuation-pending-approval-confirmation-btn").click(function(){
                    var valuation_id = $(this).data("valuation_id");
                    var token = $(this).data("token");
                    var pending_row = $(this).data("pending_row");
                    $(document).ajaxStart(function(){
                        $(".mo-loading-image").removeClass('hidden');
                    });
                    $.ajax({
                        type: "POST",
                        url: "/approve-valuation",
                        headers: {"X-CSRF-TOKEN": token},
                        data: {"valuation_id" : valuation_id},

                        success: function(result) {
                            console.log(result);
                            var res = $.parseJSON(result);
                            if (typeof (res.id) != "undefined") {
                                //pending_row.remove();
                                pending_row.hide();
                                pending_row.next(".child").hide();
                                magnificPopup.close();
                                console.log("Aprrove Success");
                            }
                        },

                        error: function() {
                            console.log("Approve Valuation Error");
                        },
                        complete: function(){
                            $(".mo-loading-image").addClass('hidden');
                        }
                    });

                });
                // END APPROVE VALUATION

                /** ENTER VIN **/
                var magnificPopup = $.magnificPopup.instance;
                $('.mo-enter-vin-popup').magnificPopup({
                    type:'inline',
                    showCloseBtn: false,
                    closeOnBgClick: false,
                    callbacks: {
                        beforeClose: function(){
                            enterVinValidator.resetForm();
                        }
                    }
                });

                $('.mo-cancel-btn').click(function (){
                    magnificPopup.close();
                });

                $("#vin-confirm").click(function (){

                    $("input[name='year']").rules('add', {
                        required: true
                    });
                    $("input[name='vin']").rules('add', {
                        required: true
                    });
                    $("input[name='registration-number']").rules('add', {
                        required: true
                    });

                    if($("#mo-popup-enter-vin").valid()) {
                        //accept valuation here

                        $(document).ajaxStart(function(){
                            $(".mo-loading-image").removeClass("hidden");
                        });
                        var token = $("input[name='_token']").val();
                        $.ajax({

                            type:'POST',
                            url: "/accept-valuation/" + valuation_id,
                            headers: {"X-CSRF-TOKEN": token},
                            data: null,
                            success:function(response) {
                                console.log(response);
                                response = $.parseJSON(response);
                                if (response.hasOwnProperty("id")) {
                                    console.log(response);
                                    document.location.href="/review-valuation/"+valuation_id;
                                } else {
                                    console.log("accept valuation error");
                                }
                            },
                            error: function(response){
                                console.log("error");
                            }
                        });

                        $(document).ajaxStop(function(){
                            $(".mo-loading-image").addClass("hidden");
                        });
                    }

                });

                var valuation_id = null;
                var year = null;
                var vin_number = null;
                var registration_number = null

                $(".mo-accept-valuate-btn").click(function (){

                    valuation_id = $(this).data("valuation-id");
                    year = $(this).parent().parent().parent().children(".year").text();
                    vin_number = $(this).parent().parent().parent().children(".vin-number").text();
                    registration_number = $(this).parent().parent().parent().children(".registration").text();

                    $("input[name='year']").val(year);
                    $("input[name='vin']").val(vin_number);
                    $("input[name='registration-number']").val(registration_number);

                });

                $("#get-vin").click(function(e){

                    $("input[name='year']").rules('add', {
                        required: true
                    });
                    $("input[name='vin']").rules('add', {
                        required: true
                    });
                    $("input[name='registration-number']").rules('remove');


                    if($("#mo-popup-enter-vin").valid()) {

                        var vin = $("input[name='vin']").val();
                        var token = $("input[name='_token']").val();
                        var year_number = $("input[name='year']").val();

                        if (vin.length > 0 && year_number != null)
                        {
                            $.ajax( {
                              url:'/search-vin',


                              type: 'POST',
                              data: {_token: token, vin_number: vin, year: year_number},

                              success:function(data) {

                                response = jQuery.parseJSON(data);
                                if (data.length > 0) {
                                    console.log(response);
                                    $("div.vin-number").text(response.vin);
                                    var vehicle_details = "";
                                    if(response.make !== null){
                                        vehicle_details += " "+ response.make
                                    }
                                    if(response.model_family !== null){
                                        vehicle_details += " "+ response.model_family
                                    }
                                    if(response.build_date !== null){
                                        vehicle_details += " "+ response.build_date
                                    }
                                    if(response.variant !== null){
                                        vehicle_details += " "+ response.variant
                                    }
                                    if(response.series !== null){
                                        vehicle_details += " "+ response.series
                                    }
                                    if(response.colour !== null){
                                        vehicle_details += " "+ response.colour
                                    }
                                    if(response.manufacturer_color !== null){
                                        vehicle_details += " "+ response.manufacturer_color
                                    }
                                    if(response.body_type !== null){
                                        vehicle_details += " "+ response.body_type
                                    }
                                    if(response.registered_state !== null){
                                        vehicle_details += " "+ response.registered_state
                                    }
                                    if(response.registration_number !== null){
                                        vehicle_details += " "+ response.registration_number
                                    }
                                    if(response.compliance_date !== null){
                                        vehicle_details += " "+ response.compliance_date
                                    }

                                    $(".note-content").text($.trim(vehicle_details));

                                    $("#vin-confirm").prop("disabled", false);
                                }

                              },
                              error: function(xhr) {

                              }
                            });
                        }

                    }
                });

                // $("#mo-popup-enter-vin").on("submit", function(){
                //     return false;
                // });

                var enterVinValidator = $("#mo-popup-enter-vin").validate({
                    rules: {
                        //"year" : "required",
                        //"vin" : "required",
                        //"registration-number" : "required"
                    },
                    messages: {
                        "year": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                        "vin": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
                        "registration-number": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>"
                    }
                });
                /** ENTER VIN **/

	        }

	    });

	}
    /* end initDataTable */

	$.moDataTable = function(id_table, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg, active, ajax_data) {
      	//$.constructDataTable(id_table, false, "<div>There are no customers to display at this time.<br> Create a customer by tapping the plus icon on the bottom right or search for a client in the search box.</div>");
        if (path_name.indexOf("/valuations") != -1
            || path_name.indexOf("/customer-details") != -1) {
            $.initDataTable(id_table, destroy, columns, request_url, query, offset, init, error_msg, scrollX);
        }

        if (path_name.indexOf("/dealership/roles") != -1
            || path_name.indexOf("/dealership/location") != -1
            || path_name.indexOf("/dealership/external-offer-groups") != -1
            || path_name.indexOf("/trading/wishlist") != -1
            || path_name.indexOf("/test-drives/inprogress") != -1
            || path_name.indexOf("/test-drives/complete") != -1
            || path_name.indexOf("/test-drives/search-vehicle") != -1
            || path_name.indexOf("/transaction/view-transactions") != -1 ) {
            $.initDataTable(id_table, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg, active, ajax_data);
        }

        if (path_name.indexOf("/dealership/users-list") != -1) {
            $.initDataTable(id_table, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg, active);
        }

	}

    $(document).ready(function(){
		var pathname = window.location.pathname;
        /** IN-PROGRESS VALUATIONS **/
        /* /valuations/in-progress */
	 	if (path_name.indexOf("in-progress") != -1) {
	 		current_page = 1;
	 		var id = "#mo-table-valuations-in-progress-list";
	    	var destroy = false;
			var columns = 	[
								{className: "never"},
                                {
                                    className: "all wish", "bSortable": false,
                                    "mRender": function (data, type, row, meta) {
                                        if(row[1] == "1")
                                            return '<span title="wish list" class="wish-icon glyphicon glyphicon-star"></span>';
                                        else
                                            return '';
                                    }
                                },
	                       		{className: "all"}, //make
	                          	{className: "all"}, //model
	                          	{className: "desktop","bSortable": false }, //year
	                          	{className: "all","bSortable": false }, //registration
	                          	{className: "desktop","bSortable": false},
	                          	{className: "desktop", "bSortable": false},
	                          	{className: "desktop", "bSortable": false},
	                          	{className: "desktop", "bSortable": false,
		                            "mRender": function (data, type, row, meta) {

		                                return  "<a href='#mo-sensitive-action-confirm-popup' class='open-popup-link'>" +
                                                    '<input type="button" class="mo-requested-btn text-capitalize cancel-valuation" value="cancel">' +
                                                    '<input type="hidden" value="'+ row[0] +'" id="in-progress-valuation-id" />' +
                                                '</a>'
                                                ;
		                            }
                                },
		                        // {className: "all", "bSortable": false,
		                        //     "mRender": function (data, type, row, meta) {
		                        //         return '<a href="/customer-details/view/' + row[0] + '"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
		                        // }}
	                     	];
			var request_url = "/_valuations/in-progress";
			var query = null;
			var offset = 0;
			var init = true;
			var error_msg = "<div>There are no valuations to display at this time.</div>";
            var scrollX = false;
			$.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);

            /* valuation in-progress search hot key */
            $("#mo-txt-search").bind("enterKey", function(){
                var destroy = true;
                var init = true;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);
            });

            $('#mo-txt-search').keyup(function(e){
                if(e.keyCode == 13)
                {
                  $(this).trigger("enterKey");
                }
            });
            /* end valuation request search hot key */
	 	}
        /* end /valuations/in-progress */
        
        /** COMPLETE VALUATIONS **/
        /* COMPLETE SCREEN */
        else if (path_name.indexOf("valuations/complete") != -1) {
            var id = "#mm-table-valuations-complete-list";
            var destroy = false;
            var columns = 	[
                {className: "never"},
                {
                    className: "all wish", "bSortable": false,
                    "mRender": function (data, type, row, meta) {
                        if(row[1] == "1")
                            return '<span title="wish list" class="wish-icon glyphicon glyphicon-star"></span>';
                        else
                            return '';
                    }
                },
                {className: "all"},
                {className: "all"},
                {className: "all", "bSortable": false},
                {className: "desktop tablet-l", "bSortable": false}, //condition change to registration
                {className: "desktop", "bSortable": false},
                {className: "desktop", "bSortable": false},
                {className: "desktop", "bSortable": false},
                {className: "desktop", "bSortable": false},
                {className: "never", "bSortable": false},
                {
                    className: "all", "bSortable": false,
                    "mRender": function (data, type, row, meta) {
                        /** check status of complete list **/
                        var dropdown = '<div class="dropdown">' +
                            '<span class="glyphicon glyphicon-option-vertical dropdown-toggle" data-toggle="dropdown"></span>'+
                            '<ul class="dropdown-menu mo-extend-menu-dropdown">';
                        var end_dropdown = '</ul></div>';
                        if(row[10] == 3){
                            return dropdown +
                                '<li><a href="#mo-popup-valuation-version" class="mo-valuation-version" data-valuation-id="'+row[0]+'" data-valuation-status="'+row[9]+'">Details</a></li>'+
                                '<li><a href="#mo-popup-changestatus" class="open-popup-link mo-trade" data-valuation-id="'+row[0]+'" data-max-price="'+row[11]+'">Trade</a></li>'+
                                '<li><a href="#mo-re-evaluate-popup" class="open-popup-link mo-re-price" data-valuation-id="'+row[0]+'">Re-Price</a></li>'+
                                end_dropdown;
                        }else if (row[10] == 6) {
                            return dropdown +
                                '<li><a href="#mo-popup-valuation-version" class="mo-valuation-version" data-valuation-id="' + row[0] + '" data-valuation-status="'+row[9]+'">Details</a></li>' +
                                '<li><a href="/review-valuation/' + row[0] + '/true#summary">Receive Vehicle</a></li>' +
                                end_dropdown;
                        }
                        else if (row[10] == 7 || row[10] == 12 || row[10] == 13 || row[10] == 14) {
                            return dropdown +
                                '<li><a href="#mo-popup-valuation-version" class="mo-valuation-version" data-valuation-id="'+row[0]+'" data-valuation-status="'+row[9]+'">Details</a></li>'+
                                end_dropdown;
                        }
                        else{
                            return null;
                        }

                    }
                }
            ];
            var request_url = "/_valuations/complete";
            var query = null;
            var offset = 0;
            var init = true;
            var error_msg = "<div>There are no valuations to display at this time.</div>";
            var scrollX = false;
            $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);
            /* valuation complete search hot key */
            $("#mo-txt-search").bind("enterKey", function(){
                var destroy = true;
                var init = true;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);
            });

            $('#mo-txt-search').keyup(function(e){
                if(e.keyCode == 13)
                {
                  $(this).trigger("enterKey");
                }
            });
            /* end valuation complete search hot key */
	 	}
        /** END COMPLETE VALUATIONS **/

        /** PENDING VALUATIONS **/
        else if (path_name.indexOf("pending") != -1) {
            var id = "#mo-table-valuations-pending-list";
            var destroy = false;
            var columns = 	[
                {
                    className: "all wish", "bSortable": false,
                    "mRender": function (data, type, row, meta) {
                        if(row[0] == "1")
                            return '<span title="wish list" class="wish-icon glyphicon glyphicon-star"></span>';
                        else
                            return '';
                    }
                },
                {
                    className: "all pending-more-info",
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        $(nTd).data("status", oData[12]);
                    }
                }, //make
                {className: "all"}, //model
                {className: "desktop tablet-l", "bSortable": false}, //year
                {className: "desktop tablet-l", "bSortable": false}, //registration
                {className: "desktop tablet-l", "bSortable": false},//color
                {className: "never", "bSortable": false},
                {className: "desktop", "bSortable": false},//valuer
                {className: "desktop", "bSortable": false},//sales
                {className: "none", "bSortable": false},
                {className: "none", "bSortable": false},
                {className: "none", "bSortable": false},
                {className: "none", "bSortable": false},
                {className: "never"},
                {className: "never"},//id
                {
                    className: "all", "bSortable": false,
                    "mRender": function (data, type, row, meta) {
                        if (row[13] == 5) {
                            return '<input type="button" class="mo-approve-price-btn text-capitalize" value="approve price" data-valuation-id="'+row[14]+'">'+
                                '<input type="button" class="mo-reject-price-btn text-capitalize" value="reject" data-valuation-id="'+row[14]+'">';
                        } else {
                            if(row[9] == 0 || row[10] == 0){
                                return "<div class='text-center'>Awaiting External Offer</div>";
                            }else{
                                return '<a href="#mo-sensitive-action-confirm-popup" class="open-popup-link">' +
                                    '<input type="button" class="mo-approve-valuate-btn text-capitalize" value="approve valuation" data-valuation-id="'+row[14]+'">' +
                                    '</a>'
                                    ;
                            }

                        }

                    }
                },
                {
                    className: "desktop", "bSortable": false,
                    "mRender": function (data, type, row, meta) {
                        return '<a href="/review-valuation/' + row[14] + '"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                    }
                }
            ];
            var request_url = "/_valuations/pending";
            var query = null;
            var offset = 0;
            var init = true;
            var error_msg = "<div>There are no valuations to display at this time.</div>";
            var scrollX = true;
            $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);

            /* valuation pending search hot key */
            $("#mo-txt-search").bind("enterKey", function(){
                var destroy = true;
                var init = true;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);
            });

            $('#mo-txt-search').keyup(function(e){
                if(e.keyCode == 13)
                {
                  $(this).trigger("enterKey");
                }
            });
            /* end valuation pending search hot key */
        }
        /** END PENDING VALUATIONS **/

        /** REQUEST VALUATIONS **/
        /* /valuations/requests */
        else if (path_name.indexOf("requests") != -1) {
            
            var id = "#mo-table-valuations-requests-list";
            var destroy = false;
            var columns = 	[
                {className: "never"},
                {
                    className: "all wish", "bSortable": false,
                    "mRender": function (data, type, row, meta) {
                        if(row[1] == "1")
                            return '<span title="wish list" class="wish-icon glyphicon glyphicon-star"></span>';
                        else
                            return '';
                    }
                },
                {className: "all make"},
                {className: "all model"},
                {className: "all year", "bSortable": false},
                {className: "desktop tablet-l vin-number", "bSortable": false},
                {className: "desktop colour", "bSortable": false},
                {className: "desktop registration word-break", "width": "15%", "bSortable": false},
                {className: "desktop expiry-date", "bSortable": false},
                {className: "desktop sale-person", "bSortable": false},
                {className: "never", "bSortable": false},
                {
                    className: "desktop", "bSortable": false,
                    "mRender": function (data, type, row, meta) {
                        if(row[10] == "valuation_perform")
                            return '<a href="#mo-enter-vin-popup" class="mo-enter-vin-popup"><input type="button" class="mo-accept-valuate-btn text-capitalize" value="accept" data-valuation-id="'+row[0]+'"></a>';
                        else
                            return null;
                    }
                },
                {
                    className: "desktop", "bSortable": false,
                    "mRender": function (data, type, row, meta) {
                        return "<a href='#mo-sensitive-action-confirm-popup' class='open-popup-link'>" +
                        '<span class="glyphicon glyphicon-trash float-right delete-valuation"></span></a>'+
                        '<input type="hidden" value="'+ row[0] +'" class="request-valuation-id" />';
                    }
                }
            ];
            var request_url = "/_valuations/requests";
            var query = null;
            var offset = 0;
            var init = true;
            var error_msg = "<div>There are no valuations to display at this time.</div>";
            var scrollX = true;
            $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);

            /* valuation request search hot key */
            $("#mo-txt-search").bind("enterKey", function(){
                var destroy = true;
                var init = true;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);
            });

            $('#mo-txt-search').keyup(function(e){
                if(e.keyCode == 13)
                {
                  $(this).trigger("enterKey");
                }
            });
            /* end valuation request search hot key */

        }
        /* end /valuations/requests */

        /** ALL VALUATIONS BY CUSTOMER ID **/
        else if (path_name.indexOf("customer-details") != -1) {
            //split file already (ref to customer/viewCustomerDetail.js)
        }
        /** END ALL VALUATIONS BY CUSTOMER ID **/

        /** TEST DRIVES **/
        else if(path_name.indexOf("/test-drives/search-vehicle") != -1){
            $.getSearchVehicle = function(destroy, init, query, offset){
                var id = "#mo-search-vehicle-list";
                var destroy = false;
                var columns = [
                    {className: "never"}, //id
                    {className: "all"}, // make
                    {className: "all", "bSortable": false}, // model
                    {className: "all", "bSortable": false}, // registration number
                    {className: "all", "bSortable": false}, // vin
                    {
                        className: "all", "bSortable": false,
                        "mRender": function (data, type, row, meta) {
                            return '<a href="/test-drives/select-vehicle/'+row[0]+'/'+row[1] +" "+row[2]+'"><span class="glyphicon glyphicon-menu-right float-right mo-select-vehicle"></span></a>';
                        }
                    }
                ];
                var request_url = "/view-search-vehicle";
                var query = null;
                var offset = 0;
                var init = true;
                var scrollX = true;
                var error_msg = "<div>There are no vehicles to display at this time.</div>";
                // var empty_msg = "<div>There are no vehicle to display at this time.<br> Create a location by clicking on NEW button or search for a location in the search box.</div>";
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);
            }
            var destroy = false;
            var init = true;
            var query = null;
            var offset = 0;
            $.getSearchVehicle(destroy, init, query, offset)
        }

        else if(path_name.indexOf("/test-drives/inprogress") != -1){

            $.getInProgressList = function(destroy, init, query, offset) {

                var id = "#mo-table-in-progress-list";
                var destroy = destroy;
                var columns = [
                    {className: "never"},
                    {className: "all", "width":"10%"}, //make
                    {className: "all", "width":"10%" }, // model
                    {className: "all", "bSortable": false, "width":"8%"}, //year
                    {className: "all", "bSortable": false, "width":"8%"}, //colour
                    {className: "all", "bSortable": false}, //start date
                    {className: "all", "bSortable": false}, //time out
                    {className: "all", "bSortable": false}, //estimated return date
                    {className: "never"}, //status
                    {className: "all", "bSortable": false}, //trade no/registration
                    {className: "all", "bSortable": false}, //stock number
                    {className: "all", "bSortable": false,
                        "mRender":function(data, type, row, meta){
                            return '<a href="/test-drives/complete/'+row[0]+'"><input type="button" class="mo-test-drives-inprogress-btn text-capitalize float-right"' +
                                'data-inprogress-id="'+row[0]+'" data-make-name="'+row[1]+'" data-model-name="'+row[2]+'" data-year="'+row[3]+'" value="Return Vehicle"></a>';
                            // return '<input type="button" class="mo-permissions-btn text-capitalize float-right" value="Return Vehicle">'
                        }
                    }
                ];
                var request_url = "/view-in-progress";
                var query = query;
                var offset = offset;
                var init = init;
                var scrollX = true;
                var error_msg = "<div>There are no test drives in progress to display at this time.</div>";
                // var empty_msg = "<div>There are no location to display at this time.<br> Create a location by clicking on NEW button or search for a location in the search box.</div>";
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);
            }
            var destroy = false;
            var init = true;
            var query = null;
            var offset = 0;
            $.getInProgressList(destroy, init, query, offset)

            $(".mo-txt-search").click(function(){
                var destroy = true;
                var init = false;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                $.getInProgressList(destroy, init, query, offset)
            });

            $("#mo-txt-search").bind("enterKey", function(){
                var destroy = true;
                var init = false;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                $.getInProgressList(destroy, init, query, offset)
            });

            $('#mo-txt-search').keyup(function(e){
                if(e.keyCode == 13)
                {
                  $(this).trigger("enterKey");
                }
            });

            //slider for vehicle rating
            var vehicle_rating = $( "#slider_vehicle_rating" ).slider({
                value: 4,
                min: 0,
                max: 10,
                step: 1
            })
            .each(function() {

              //
              // Add labels to slider whose values 
              // are specified by min, max and whose
              // step is set to 1
              //

              // Get the options for this slider
              var opt = $(this).data().uiSlider.options;
              
              // Get the number of possible values
              var vals = opt.max - opt.min;
              
              // Space out values
              for (var i = 0; i <= vals; i++) {

                var el = $('<label>'+(i)+'</label>').css('left',(i/vals*100)+'%');

                
              
                $( "#slider_vehicle_rating" ).append(el);
                
              }
              
            });

            //end of slider for vehicle rating

            //slider for mo rating
            $( "#slider_mo_rating" ).slider({
                value: 4,
                min: 0,
                max: 10,
                step: 1
            })
            .each(function() {

              //
              // Add labels to slider whose values 
              // are specified by min, max and whose
              // step is set to 1
              //

              // Get the options for this slider
              var opt = $(this).data().uiSlider.options;
              
              // Get the number of possible values
              var vals = opt.max - opt.min;
              
              // Space out values
              for (var i = 0; i <= vals; i++) {
                
                var el = $('<label>'+(i)+'</label>').css('left',(i/vals*100)+'%');
              
                $( "#slider_mo_rating" ).append(el);
                
              }
              
            });
            //end of slider for mo rating

        }

        else if(path_name.indexOf("/test-drives/complete") != -1) {

            $.getCompletedList = function(destroy, init, query, offset) {
                var id = "#mo-table-complete-list";
                var destroy = destroy;
                var columns = [
                    {className: "never"},
                    {className: "all", "width":"10%"}, //make
                    {className: "all", "width":"10%"}, // model
                    {className: "all", "bSortable": false, "width":"8%"}, //year
                    {className: "all", "bSortable": false, "width":"8%"}, //colour
                    {className: "all", "bSortable": false}, //start date
                    {className: "all", "bSortable": false}, //time out
                    {className: "all", "bSortable": false}, //estimated return date
                    {className: "never"}, // status
                    {className: "never"}, // comment
                    {className: "all", "bSortable": false}, //trade no/registration
                    {className: "all", "bSortable": false}, //stock number
                    {className: "all", "bSortable": false,
                        "mRender": function(data, type, row, meta) {
                            return '<a href="/test-drives/complete/'+row[0]+'"><input type="button" class="mo-test-drives-read-comment-btn text-capitalize float-right" ' +
                                'data-completed-id="'+row[0]+'" data-read-comment="'+row[9]+'"  data-make-name="'+row[1]+'" data-model-name="'+row[2]+'" data-year="'+row[3]+'" '+
                                'data-vehicle-rating="'+row[12]+'" data-service-rating="'+row[13]+'"'+' data-marketing-code="'+row[14]+'"'+' data-marketing-option="'+row[15]+'"'+' value="Read Comments"></a>'
                        }
                    }
                ];
                var request_url = "/test-drives/view-complete";
                var query = query;
                var offset = offset;
                var init = init;
                var scrollX = true;
                var error_msg = "<div>There are no test drives complete to display at this time.</div>";
                // var empty_msg = "<div>There are no location to display at this time.<br> Create a location by clicking on NEW button or search for a location in the search box.</div>";
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);
            }
            var destroy = false;
            var init = true;
            var query = null;
            var offset = 0;
            $.getCompletedList(destroy, init, query, offset);

            // search test drive complete list
            $("#mo-btn-search").click(function() {
                var destroy = true;
                var init = true;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                $.getCompletedList(destroy, init, query, offset);

            });

            $("#mo-txt-search").bind("enterKey", function(){
                var destroy = true;
                var init = true;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                $.getCompletedList(destroy, init, query, offset);
            });
            
            $('#mo-txt-search').keyup(function(e){
                if(e.keyCode == 13)
                {
                  $(this).trigger("enterKey");
                }
            });

            //slider for vehicle rating
            $( "#slider_vehicle_rating" ).slider({
                value: 4,
                min: 0,
                max: 10,
                step: 1
            })
            .each(function() {

              //
              // Add labels to slider whose values 
              // are specified by min, max and whose
              // step is set to 1
              //

              // Get the options for this slider
              var opt = $(this).data().uiSlider.options;
              
              // Get the number of possible values
              var vals = opt.max - opt.min;
              
              // Space out values
              for (var i = 0; i <= vals; i++) {

                var el = $('<label>'+(i)+'</label>').css('left',(i/vals*100)+'%');

                
              
                $( "#slider_vehicle_rating" ).append(el);
                
              }
              
            });
            //end of slider for vehicle rating

            //slider for mo rating
            $( "#slider_mo_rating" ).slider({
                value: 4,
                min: 0,
                max: 10,
                step: 1
            })
            .each(function() {

              //
              // Add labels to slider whose values 
              // are specified by min, max and whose
              // step is set to 1
              //

              // Get the options for this slider
              var opt = $(this).data().uiSlider.options;
              
              // Get the number of possible values
              var vals = opt.max - opt.min;
              
              // Space out values
              for (var i = 0; i <= vals; i++) {
                
                var el = $('<label>'+(i)+'</label>').css('left',(i/vals*100)+'%');
              
                $( "#slider_mo_rating" ).append(el);
                
              }
              
            });
            //end of slider for mo rating
        }

        /** end TEST DRIVES **/

        /** DEALERSHIP LOCATION **/
        else if (path_name.indexOf("dealership/location") != -1) {
            $.getLocations = function(destroy, init, query, offset) {
                var id = "#mo-location-list";
                var destroy = destroy;
                var columns = [
                    {className: "never"},
                    {className: "all", "width":"40%"},
                    {className: "all", "bSortable": false},
                    {className: "all", "bSortable": false},
                    {
                        className: "all", "bSortable": false, /* temporarily disable for review valuation */
                        "mRender": function (data, type, row, meta) {
                            return '<a href="/dealership/init-location-form/edit/' + row[0] + '"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                        }
                    }
                ];
                var request_url = "/dealership/get-locations";
                var query = query;
                var offset = offset;
                var init = init;
                var error_msg = "<div>There are no locations to display at this time.</div>";
                var scrollX = true;
                var empty_msg = "<div>There are no location to display at this time.<br> Create a location by clicking on NEW button or search for a location in the search box.</div>";
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg);
            }
            var destroy = false;
            var init = true;
            var query = null;
            var offset = 0;
            $.getLocations(destroy, init, query, offset);

            //search location
            $(".mo-txt-search").click(function(){
                var destroy = true;
                var init = false;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                $.getLocations(destroy, init, query, offset);
            });

            $("#mo-txt-search").bind("enterKey", function(e){
                var destroy = true;
                var init = false;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                $.getLocations(destroy, init, query, offset);
            });

            $('#mo-txt-search').keyup(function(e){
                if(e.keyCode == 13)
                {
                  $(this).trigger("enterKey");
                }
            });
            //end search location
        }

        /** USERS LIST **/
        else if (path_name.indexOf("/users-list") != -1) {
            $.getUsers = function(destroy, active, init, query, offset) {
                var id = "#mo-table-user-list";
                var destroy = destroy;
                var columns = [
                    {className: "never"},//ID - hidden
                    {className: "all"},//firstName
                    {className: "all"},//lastName
                    {className: "all", "bSortable": false},//role
                    {className: "all", "bSortable": false},//email
                    {className: "all", "bSortable": false},//mobileNumber
                    {className: "never", "bSortable": false},//active
                    {
                        className: "all", "bSortable": false,
                        "mRender": function (data, type, row, meta) {
                            if(row[6] == 1){
                                return '<a href="#mo-suspend-popup" class="mo-suspend-popup"><span class="fa fa-unlock-alt cursor-pointer mo-dealership-function mo-user-suspend" data-user-id="'+row[0]+'" data-status="'+row[6]+'"></span></a>' +
                                    '<a href="/dealership/edit-user/' + row[0] + '"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                            }else{
                                return '<a href="#mo-suspend-popup" class="mo-suspend-popup"><span class="fa fa-lock cursor-pointer mo-dealership-function mo-user-suspend" data-user-id="'+row[0]+'" data-status="'+row[6]+'"></span></a>' +
                                    '<a href="/dealership/edit-user/' + row[0] + '"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                            }

                        }
                    }
                ];
                var request_url = "/dealership/get-users";
                if($('#mo-role-id').val()){
                    request_url = "/dealership/get-users/"+$('#mo-role-id').val();
                }
                var query = query;
                var offset = offset;
                var init = init;
                var error_msg = "<div>There are no users to display at this time.</div>";
                var scrollX = true;
                var empty_msg = "<div>There are no users to display at this time.<br> Create new user by clicking on NEW button or search for a user in the search box.</div>";
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg, active);
            }
            var destroy = false;
            var init = true;
            var query = null;
            var offset = 0;
            var active = false;
            $.getUsers(destroy, active, init, query, offset);

            //search user
            $(".mo-txt-search").click(function(){
                current_page = 1;
                var destroy = true;
                var active = true;
                if ($("#mo-filter-user").is(":checked")) {
                    active = true;
                } else {
                    active = false;
                }
                var init = true;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                $.getUsers(destroy, active, init, query, offset);
            });

            $("#mo-txt-search").bind("enterKey", function(e){
                current_page = 1;
                var destroy = true;
                var active = true;
                if ($("#mo-filter-user").is(":checked")) {
                    active = true;
                } else {
                    active = false;
                }
                var init = true;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                $.getUsers(destroy, active, init, query, offset);
            });

            $('#mo-txt-search').keyup(function(e){
                if(e.keyCode == 13)
                {
                    $(this).trigger("enterKey");
                }
            });
            //end search user

            //filter dealership list
            $("#mo-filter-user").click(function(){
                current_page = 1;
                var destroy = true;
                var init = true;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                if ($("#mo-filter-user").is(":checked")) {
                    var active = true;
                } else {
                    var active = false;
                }
                $.getUsers(destroy, active, init, query, offset);
            });
            //end filter dealership list
        }

        /** ROLES LIST **/
        else if (path_name.indexOf("dealership/roles") != -1) {
            $.getRoles = function(destroy, init, query, offset) {
                var id = "#mo-table-roles-list";
                var destroy = destroy;
                var columns = [
                    {className: "never"},//ID - hidden
                    {className: "all", "width":"70%"},//name
                    {
                        className: "all", "bSortable": false,
                        "mRender": function (data, type, row, meta) {
                            return '<a href="/dealership/create-new-role/edit/'+row[0]+'"><input type="button" class="mo-permissions-btn text-capitalize" value="Permissions"></a>'+
                                '<a href="#mo-delete-confirm-popup" class="mo-delete-popup"><input type="button" class="mo-delete-btn text-capitalize" value="Delete" data-role-id="'+row[0]+'"></a>';

                        }
                    }
                ];
                var request_url = "/dealership/get-roles";
                var query = query;
                var offset = offset;
                var init = init;
                var error_msg = "<div>There are no roles to display at this time.</div>";
                var scrollX = true;
                var empty_msg = "<div>There are no roles to display at this time.<br> Create new roles by clicking on NEW button or search for a role in the search box.</div>";
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg);
            }
            var destroy = false;
            var init = true;
            var query = null;
            var offset = 0;
            $.getRoles(destroy, init, query, offset);

            //search roles
            $(".mo-txt-search").click(function(){
                var destroy = true;
                var init = true;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                $.getRoles(destroy, init, query, offset);
            });

            $("#mo-txt-search").bind("enterKey", function(e){
                var destroy = true;
                var init = true;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                $.getRoles(destroy, init, query, offset);
            });

            $('#mo-txt-search').keyup(function(e){
                if(e.keyCode == 13)
                {
                    $(this).trigger("enterKey");
                }
            });
            //end search user
        }

        /** EXTERNAL GROUPS LIST **/
        else if (path_name.indexOf("dealership/external-offer-groups") != -1) {
            $.getExternalGroups = function(destroy, init, query, offset) {
                var id = "#mo-table-external-groups-list";
                var destroy = destroy;
                var columns = [
                    {className: "never"},//ID - hidden
                    {className: "all", "width":"20%"},//group name
                    {className: "all", "width":"70%"},//group description
                    {
                        className: "all", "bSortable": false,
                        "mRender": function (data, type, row, meta) {
                            return  '<a href="/dealership/init-external-offer-group/edit/' + row[0] + '"><span class="glyphicon glyphicon-menu-right float-right"></span></a>'+
                                    '<a href="#mo-remove-confirm-popup" class="mo-remove-popup"><span class="glyphicon glyphicon-trash" data-external-id="' + row[0] + '" id="mo-remove-external-offer-group"></span></a>';

                        }
                    }
                ];
                var request_url = "/dealership/get-external-offer-groups";
                var query = query;
                var offset = offset;
                var init = init;
                var error_msg = "<div>There are no external offer groups to display at this time.</div>";
                var scrollX = true;
                var empty_msg = "<div>There are no external offer groups to display at this time.<br> Create new external offer groups by clicking on NEW button or search for a external offer group in the search box.</div>";
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg);
            }
            var destroy = false;
            var init = true;
            var query = null;
            var offset = 0;
            $.getExternalGroups(destroy, init, query, offset);

            //search external groups
            $(".mo-txt-search").click(function(){
                var destroy = true;
                var init = true;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                $.getExternalGroups(destroy, init, query, offset);
            });

            $("#mo-txt-search").bind("enterKey", function(e){
                var destroy = true;
                var init = true;
                var query = $("#mo-txt-search").val();
                var offset = 0;
                $.getExternalGroups(destroy, init, query, offset);
            });

            $('#mo-txt-search').keyup(function(e){
                if(e.keyCode == 13)
                {
                    $(this).trigger("enterKey");
                }
            });
            //end search user
        }

        /* start WISH LIST */
        else if(path_name.indexOf("/trading/wishlist") != -1){
            /* set current page for pagination */
            current_page = 1;
            /* getWishList function */

            $.getWishList = function(destroy, init, query, offset) {
                var id = "#mo-wishlist-list";
                var destroy = destroy;
                var columns = [
                    {className: "never"}, // ID-hidden
                    {className: "all"}, // make-model
                    {className: "all"}, // odometer
                    {className: "all"}, // year
                    {className: "all"}, // location
                    {className: "all"}, // transmission
                    {className: "all"}, // price
                    {
                        className: "all", "bSortable": false,
                        "mRender": function(data, type, row, meta){
                            return '<a href="/trading/wishlist/edit/' + row[0] + '"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                        }
                    }
                ];
                var request_url = "/trading/get-wishlist";
                var query = query;
                var offset = offset;
                var init = init;
                var error_msg = "<div>There are no wish list to display at this time.</div>";
                var scrollX = true;
                var empty_msg = "<div>There are no wish list to display at this time.<br> Create new wish list by clicking on NEW button or search for a external offer group in the search box.</div>";
                /* call moDataTable */
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg);
                /* end call moDataTable */                
            }
            /* end getWishList function */

            var destroy = false;
            var init = true;
            var query = null;
            var offset = 0;
            $.getWishList(destroy, init, query, offset);

            
            $("#mo-wishlist-txt-search").bind("enterKey", function(){
                var destroy = true;
                var init = true;
                var query = $("#mo-wishlist-txt-search").val();
                var offset = 0;
                $.getWishList(destroy, init, query, offset);
            });

            $('#mo-wishlist-txt-search').keyup(function(e){
                if(e.keyCode == 13)
                {
                    $(this).trigger("enterKey");
                }
            });
            // end search wish list
        }

        /** SEARCH COMMON **/
        $("#mo-btn-search").click(function() {

            $(".mo-page-number-wrap").remove();
            destroy = true;
            query = $("#mo-txt-search").val();
            offset = 0;
            current_page = 1;

            if(path_name.indexOf("trading/wishlist") != -1)
            {
                var destroy = true;
                var init = true;
                var query = $("#mo-wishlist-txt-search").val();
                $.getWishList(destroy, init, query, offset);
            }
            else
            {
                $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX);
            }
            
        });

        $("#mo-page-prev").click(function() {
            if (current_page > 1) {
                $(".mo-page-number").removeClass("active");
                current_page = parseInt(current_page) - 1;
                var offset = (current_page - 1)*20;

                /* user */
                if(path_name.indexOf("user") != -1)
                {
                    var destroy = true;
                    var active = true;
                    if ($("#mo-filter-user").is(":checked")) {
                        active = true;
                    } else {
                        active = false;
                    }
                    var init = false;
                    var query = $("#mo-txt-search").val();
                    $.getUsers(destroy, active, init, query, offset);
                }
                /* trading/wishlist */
                else if(path_name.indexOf("trading/wishlist") != -1)
                {
                    var destroy = true;
                    var init = false;
                    var query = $("#mo-txt-search").val();;
                    $.getWishList(destroy, init, query, offset);
                }
                else if (path_name.indexOf("transaction/view-transactions") != -1) {
                    if(sessionStorage.getItem("dealership_elem_to_paging")) {
                        var data_obj = JSON.parse(sessionStorage.getItem("dealership_elem_to_paging"));
                        if(offset && data_obj.ajax_data){
                            data_obj.ajax_data.offset = offset;
                            active = data_obj.ajax_data.active;
                        }
                        $.getTransactionsList(true, false, null, offset, data_obj.request_url, data_obj.table_id, active, data_obj.ajax_data);
                    }
                }
                else{
                    $.initDataTable(id,true, columns, request_url, query, offset, false, error_msg, scrollX);
                }

                //$(".mo-page-number")[0].addClass("active");
                $($(".mo-page-number")[current_page - 1]).addClass("active");
            }

        });

        $("#mo-page-next").click(function() {
            if (current_page < page_length) {
                $(".mo-page-number").removeClass("active");
                current_page = parseInt(current_page) + 1;
                var offset = (current_page - 1)*20;

                /* user */
                if(path_name.indexOf("user") != -1)
                {
                    var destroy = true;
                    var active = true;
                    if ($("#mo-filter-user").is(":checked")) {
                        active = true;
                    } else {
                        active = false;
                    }
                    var init = false;
                    var query = $("#mo-txt-search").val();
                    $.getUsers(destroy, active, init, query, offset);
                }
                /* trading/wishlist */
                else if(path_name.indexOf("trading/wishlist") != -1)
                {
                    var destroy = true;
                    var init = false;
                    var query = $("#mo-txt-search").val();;
                    $.getWishList(destroy, init, query, offset);
                }
                else if (path_name.indexOf("transaction/view-transactions") != -1) {
                    if(sessionStorage.getItem("dealership_elem_to_paging")) {
                        var data_obj = JSON.parse(sessionStorage.getItem("dealership_elem_to_paging"));
                        if(offset && data_obj.ajax_data){
                            data_obj.ajax_data.offset = offset;
                            active = data_obj.ajax_data.active;
                        }
                        $.getTransactionsList(true, false, null, offset, data_obj.request_url, data_obj.table_id, active, data_obj.ajax_data);
                    }
                }
                else
                {
                    $.initDataTable(id,true, columns, request_url, query, offset, false, error_msg, scrollX);
                }

                $($(".mo-page-number")[current_page - 1]).addClass("active");
            }
        });
        /** end SEARCH COMMON **/


        $("#required-action").click(function(){
            current_page = 1;
            var destroy = true;
            var init = true;
            var query = null;
            var offset = 0;
            var active = null;
            if ($("#required-action").is(":checked")) {
                active = true;
            }
            console.log('ey '+active);

            if(sessionStorage.getItem("dealership_elem_to_paging")) {
                var data_obj = JSON.parse(sessionStorage.getItem("dealership_elem_to_paging"));
                if(active && data_obj.ajax_data){
                    data_obj.ajax_data.active = true;
                }else{
                    //data_obj.ajax_data.active = null;
                }
            }
            $.getTransactionsList(destroy, init, query, offset, data_obj.request_url, data_obj.table_id, active, data_obj.ajax_data);

        });
	});
}(jQuery));