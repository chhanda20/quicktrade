$('.mo-block').first().toggleClass('mo-block-active');
$('#mo-user-list').hide();
$('#mo-user-list').removeClass('hidden').fadeIn('slow');

/* Click moto Users 4 Blocks */
$('.mo-block').click(function(){
    $('.mo-block').removeClass('mo-block-active');
    $(this).toggleClass('mo-block-active');

    var tab = $(this).parents('.tab').attr('id');
    $('.mo-block-list').hide();

    var active = false;
    if ($("#mo-filter-user").is(":checked")) {
        active = true;
    } else {
        active = false;
    }

    if(tab == 'all') {
        $('#mo-role-id').val('');
        reinitializeUserTable(active);
    } else if(tab == 'super_admins') {
        $('#mo-role-id').val(7);
        reinitializeUserTable(active);
    } else if(tab == 'admins') {
        $('#mo-role-id').val(6);
        reinitializeUserTable(active);
    } else if(tab == 'users') {
        $('#mo-role-id').val('remaining');
        reinitializeUserTable(active);
    }


    function reinitializeUserTable(active){
        var destroy = true;
        var init = true;
        var query = null;
        var offset = 0;
        $.getUsers(destroy, active, init, query, offset);
        $('#mo-user-list').hide();
        $('#mo-user-list').removeClass('hidden').fadeIn('slow');
    }
});