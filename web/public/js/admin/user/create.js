var url = base_url+'/mo-admin/user/create';

$(function() {

    $("#range_from, #range_to, #include, #exclude").attr('disabled', true);

    /*$("#range_from, #range_to").keyup(function(){
        var $this = $(this);
        $this.val($this.val().replace(/[^\d+]/g, ''));
    });*/

    $("#range_from, #range_to").keyup(function(e){
        var $this = $(this);
        $this.val($this.val().replace(/^[^1-9]+/,''));
        if($this.val() > 100000 && e.keyCode != 46 && e.keyCode != 8){
            $this.val(100000);
        }
    });

    $("#include, #exclude").keyup(function(){
        var $this = $(this);
        $this.val($this.val().replace(/[^\d,+]/g, ''));
    });

    if ($('#user-id').val()) {
        $('#user-email').attr('readonly', true);
    }

    /** VALIDATE GREATER THAN **/
    $.validator.addMethod("greaterThan", function (value, element, param) {
        var $element = $(element)
            , $min;

        if (typeof(param) === "string") {
            $min = $(param);
        } else {
            $min = $("#" + $element.data("min"));
        }

        if (this.settings.onfocusout) {
            $min.off(".validate-greaterThan").on("blur.validate-greaterThan", function () {
                $element.valid();
            });
        }
        return parseInt(value) > parseInt($min.val());
    }, "Max must be greater than min");

    $.validator.addClassRules({
        greaterThan: {
            greaterThan: true
        }
    });

    $("#mo-create-user-form").validate({
        rules: {
            "user-code": "required",
            "user-firstname": "required",
            "user-address-1": "required",
            "user-country": "required",
            "user-suburb": "required",
            "user-state": "required",
            "user-mobile-number": "required",
            "user-email": {
                "required": true,
                "email": true
            },
            "user-privacy": "required",
            "user-password": {
                "required": function(e){
                    if(pathname.indexOf(url) != -1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                },
                "minlength": 6
            },
            "dealer_group": "required",
            "dealership": "required",
            "location": "required",
            "user-role": "required",
            "range_from": "required"/*,
            "range_to": "required"*/
        },
        highlight: function(element) {
            $(element).closest('.input-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.input-group').removeClass('has-error');
        },
        messages: {
            "user-code": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter code</span>" + "</div>",
            "user-firstname": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user first name</span>" + "</div>",
            "user-address-1": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user address</span>" + "</div>",
            "user-country": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user country</span>" + "</div>",
            "user-suburb": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user suburb</span>" + "</div>",
            "user-state": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user state</span>" + "</div>",
            "user-email": {
                "required": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user email</span>" + "</div>",
                "email": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter valid email</span>" + "</div>"
            },
            "user-mobile-number": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user mobile number</span>" + "</div>",
            "user-privacy": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user privacy</span>" + "</div>",
            "user-password": {
                // "required": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user password</span>" + "</div>",
                "required": function(e){
                    if(pathname.indexOf(url) != -1)
                    {
                        return "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user password</span>" + "</div>";
                    }
                    else
                    {
                        return "";
                    }
                },
                "minlength": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter more than 6 characters</span>" + "</div>"

            },
            "range_to": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>This field cannot be empty and have to be greater than 'From'</span>" + "</div>"
        },
        submitHandler: function(form)
        {
            form.submit();
        }
    });

    /** Toggle Show/Hide Password **/
    (function ($) {
        $.toggleShowPassword = function (options) {

            var settings = $.extend(options);

            var control = $(settings.control);
            var field = $(settings.field);

            control.bind('click', function () {
                if (field.is(':password')) {
                    $(".mo-user-show-password").hide();
                    $(".mo-user-password").show();
                    field.attr('type', 'text');
                } else {
                    $(".mo-user-show-password").show();
                    $(".mo-user-password").hide();
                    control.show();
                    field.attr('type', 'password');
                }
            });
        };
    }(jQuery));

    //Here how to call above plugin from everywhere in your application document body
    $.toggleShowPassword({
        field: '#user-password',
        control: '.mo-user-show-password'
    });

    $.toggleShowPassword({
        field: '#user-password',
        control: '.mo-user-password'
    });

    $("#user-create").click(function(e) {
        e.preventDefault();
        var formData = $("#mo-create-user-form").serialize();
        $(document).bind("ajaxStart.mine", function() {
            $(".mo-loading-image").removeClass("hidden");
        });
        var requestURL = "/mo-admin/user/saveUser";
        if($("#user-id").val()!=''){
            requestURL = "/mo-admin/user/saveUser/"+$("#user-id").val();
        }

        if ($('#mo-create-user-form').valid()) {
            $.ajax({
                type: "POST",
                url: requestURL,
                data:formData,
                success: function (data) {
                    if(data != 'success'){
                        var response = jQuery.parseJSON(data);
                        $(".alert .mo-error-message").remove();
                        $(".alert br").remove();

                        if (response.hasOwnProperty('detail')) {
                            for (var property in response.detail) {
                                if (response.detail.hasOwnProperty(property)) {
                                    response.detail[property].forEach(function(value, index) {
                                        var warning_message = '<span class="mo-error-message text-capitalize">' + property + ': ' + value + '</span><br>';
                                        $(".alert").append(warning_message);
                                    });
                                }
                            }
                        } else {
                            var warning_message = '<span class="mo-error-message text-capitalize">' + response.code + '</span><br>';
                            $(".alert").append(warning_message);
                        }

                        $(".alert").removeClass("hidden");
                        $("html, body").animate({
                            scrollTop: 0
                        }, 500);
                    } else if(data == 'success') {
                        window.location.href = "/mo-admin/user/list";
                    }
                },
                error: function (data) {
                    //console.log(data);
                },
                complete: function(){
                    $('.mo-loading-image').addClass("hidden");
                }
            });
        }

        $(document).unbind(".mine");

    });

    /** DISABLE OR ENABLE SEARCH CONTACT FIELD **/
    if ($("#new-contact").is(":checked"))
    {
        $("#user-search-contact").prop("disabled", true);
    }
    $("input[name='user-contact']").click(function(){
        if ($(this).val() == "new-contact")
        {
            $("#user-search-contact").prop("disabled", true);
        }
        else
        {
            $("#user-search-contact").prop("disabled", false);
        }
    });

    /** SEARCH CONTACT **/

    $( "#user-search-contact" ).autocomplete({
        source:  function (request, response) {
            $.ajax({
                type: "POST",
                url: "/dealership/search-company-users",
                data: {
                    _token: $('input[name="_token"]').val(),
                    query: request.term
                },
                dataType: 'json',
                success: function (data) {
                    response(data);
                },
                error: function () {
                    response([]);
                }
            });
        },
        minLength: 2,
        select: function( event, ui ) {
            $("#dealer_group").val(ui.item.value2.company_group_id);
            showLocation(token, ui.item.value2.company_group_id);
            var dealershipId = ui.item.value2.company_id;
            $.ajax({
                type: 'POST',
                data: {
                    _token: token,
                    dealer_group: $("#dealer_group").val()
                },
                dataType: 'json',
                url: "/dealership/get-by-group",
                success:function(data) {
                    $("#dealership").find("option:gt(0)").remove();
                    $.each(data, function(key, obj) {
                        if (dealershipId == obj.id) {
                            $('#dealership')
                                .append($("<option></option>")
                                    .attr("value", obj.id)
                                    .attr("selected", true)
                                    .text(obj.company_name));

                            // Location
                            var locationId = ui.item.value2.location_id;
                            if (locationId) {
                                $.ajax({
                                    type: 'POST',
                                    data: {
                                        _token: token,
                                        dealership: $("#dealership").val()
                                    },
                                    dataType: 'json',
                                    url: "/location/get-by-company",
                                    success:function(data) {
                                        $("#location").find("option:gt(0)").remove();
                                        $.each(data, function(key, obj2) {
                                            if (obj2.id == locationId) {
                                                $('#location')
                                                    .append($("<option></option>")
                                                        .attr("value", obj2.id)
                                                        .attr("selected", true)
                                                        .text(obj2.name));
                                            } else {
                                                $('#location')
                                                    .append($("<option></option>")
                                                        .attr("value", obj2.id)
                                                        .text(obj2.name));
                                            }

                                        });

                                        /** ENABLE LOCATION **/
                                        $("#location").prop("disabled", false);
                                    },
                                    error: function(data){
                                        console.log('error: '+data);
                                    }
                                });
                            }


                        } else {
                            $('#dealership')
                                .append($("<option></option>")
                                    .attr("value", obj.id)
                                    .text(obj.company_name));
                        }

                    });

                    /** ENABLE DEALERSHIP **/
                    $("#dealership").prop("disabled", false);
                    $("#location").find("option:gt(0)").remove();



                },
                error: function(data){
                    console.log('error: '+data);
                }
            });

            $("#user-email").val(ui.item.value2.email).attr('readonly', true);
            $("#user-firstname").val(ui.item.value2.first_name);
            $("#user-lastname").val(ui.item.value2.last_name);
            $("#user-mobile-number").val(ui.item.value2.mobile_phone);
            $("#user-address-1").val(ui.item.value2.address1);
            $("#user-address-2").val(ui.item.value2.address2);
            $("#user-company-name").val(ui.item.value2.company_name);
            $("#user-email").val(ui.item.value2.email);
            $("input[name='contact-id']").val(ui.item.value2.id);
            $("#user-title").val(ui.item.value2.title);
            $("#user-gender").val(ui.item.value2.gender);
            $("#user-suburb").val(ui.item.value2.suburb);
            $("#user-postcode").val(ui.item.value2.postcode);

            //auto fill country and stage
            $("#user-country").val(ui.item.value2.country);
            $.autoLoadCountryState();
            $("#user-state").val(ui.item.value2.state);
            //end auto fill country and stage
            $("#user-privacy").val(ui.item.value2.privacy_flag);
            if (ui.item.value2.note == null) {
                $("#user-note").text('');
            } else {
                $("#user-note").text(ui.item.value2.note);
            }

        }
    });

    /** END SEARCH CONTACT **/

    /** AUTO FILL STATE DEPEND ON SELECTED COUNTRY **/
    $.autoLoadCountryState = function() {
        var country_list = jQuery.parseJSON($("#country-list").val());
        $("#state option[value]").remove();
        $.each(country_list, function(index, value) {
            $.each(value.stateList, function(index2, value2) {
                if (parseInt($("#user-country").val()) == parseInt(value2.country_id)) {
                    $("#user-state").append($("<option>", {
                        value: value2.id,
                        text: value2.name
                    }));
                }
            });
        });
        $("#user-state").val($("input[name='user-state-data']").val());
    }

    $(document).ready(function(){
        var country_list = jQuery.parseJSON($("#country-list").val());
        $("#state option[value]").remove();
        $.each(country_list, function(index, value) {
            $.each(value.stateList, function(index2, value2) {
                if (parseInt($("#user-country").val()) == parseInt(value2.country_id)) {
                    $("#user-state").append($("<option>", {
                        value: value2.id,
                        text: value2.name
                    }));
                }
            });
        });
        $("#user-state").val($("input[name='user-state-data']").val());

        if ($("#dealer_group").val())
        {
            showLocation(token, $("#dealer_group").val());
            var dealershipId = $('#dealership_selected').val();
            $.ajax({
                type: 'POST',
                data: {
                    _token: token,
                    dealer_group: $("#dealer_group").val()
                },
                dataType: 'json',
                url: "/dealership/get-by-group",
                success:function(data) {
                    $("#dealership").find("option:gt(0)").remove();
                    $.each(data, function(key, obj) {
                        if (dealershipId == obj.id) {
                            $('#dealership')
                                .append($("<option></option>")
                                    .attr("value", obj.id)
                                    .attr("selected", true)
                                    .text(obj.company_name));

                            // Location
                            var locationId = $('#location_selected').val();
                            if (locationId) {
                                $.ajax({
                                    type: 'POST',
                                    data: {
                                        _token: token,
                                        dealership: $("#dealership").val()
                                    },
                                    dataType: 'json',
                                    url: "/location/get-by-company",
                                    success:function(data) {
                                        $("#location").find("option:gt(0)").remove();
                                        $.each(data, function(key, obj2) {
                                            if (obj2.id == locationId) {
                                                $('#location')
                                                    .append($("<option></option>")
                                                        .attr("value", obj2.id)
                                                        .attr("selected", true)
                                                        .text(obj2.name));
                                            } else {
                                                $('#location')
                                                    .append($("<option></option>")
                                                        .attr("value", obj2.id)
                                                        .text(obj2.name));
                                            }

                                        });

                                        /** ENABLE LOCATION **/
                                        $("#location").prop("disabled", false);

                                    },
                                    error: function(data){
                                        console.log('error: ');
                                        console.log(data);
                                    }
                                });
                            }


                        } else {
                            $('#dealership')
                                .append($("<option></option>")
                                    .attr("value", obj.id)
                                    .text(obj.company_name));
                        }

                    });
                    /** ENABLE DEALERSHIP **/
                    $("#dealership").prop("disabled", false);
                    $("#location").find("option:gt(0)").remove();

                    var user_role_id = $('#user_role_id').val();
                    // Get role after selecting company group
                    $.ajax({
                        type: 'POST',
                        data: {
                            _token: token,
                            dealer_group: $("#dealer_group").val()
                        },
                        dataType: 'json',
                        url: "/mo-admin/role/getByCompanyGroup",
                        success: function (data) {

                            $("#user-role").find("option:gt(0)").remove();
                            $.each(data, function (key, obj) {
                                if (obj.id == user_role_id) {
                                    $('#user-role')
                                        .append($("<option></option>")
                                            .attr("value", obj.id)
                                            .attr("selected", true)
                                            .text(obj.name));
                                } else {
                                    $('#user-role')
                                        .append($("<option></option>")
                                            .attr("value", obj.id)
                                            .text(obj.name));
                                }


                            });
                            $("#user-role").prop("disabled", false);
                        },
                        error: function (data) {
                            console.log('error: ' + data);
                        }
                    });
                },
                error: function(data){
                    console.log('error: '+data);
                }
            });
        }

        $("#user-mobile-number").keyup(function(){
            var $this = $(this);
            $this.val($this.val().replace(/[^\d+]/g, ''));
        });

    });

    $("#user-country").on("change", function() {
        var country_list = jQuery.parseJSON($("#country-list").val());
        $("#user-state option[value]").remove();
        $.each(country_list, function(index, value) {
            $.each(value.stateList, function(index2, value2) {
                if (parseInt($("#user-country").val()) == parseInt(value2.country_id)) {
                    $("#user-state").append($("<option>", {
                        value: value2.id,
                        text: value2.name
                    }));
                }
            });
        });
    });
    /** END AUTO FILL STATE DEPEND ON SELECTED COUNTRY **/

    /** DISABLE FIELDS STEP BY STEP **/
    if(!$("#dealer_group").val()) {
        $("#dealership").prop("disabled", true);
    }
    if(!$("#dealership").val()) {
        $("#location").prop("disabled", true);
    }

    /** GET DEALER GROUP LIST **/
    $("select#dealer_group").on('change', function(){
        $.ajax({
            type: 'POST',
            data: {
                _token: token,
                dealer_group: $("#dealer_group").val()
            },
            dataType: 'json',
            url: "/dealership/get-by-group",
            success:function(data) {
                $("#dealership").find("option:gt(0)").remove();
                $.each(data, function(key, obj) {
                    $('#dealership')
                        .append($("<option></option>")
                            .attr("value", obj.id)
                            .text(obj.company_name));
                });
                /** ENABLE DEALERSHIP **/
                $("#dealership").prop("disabled", false);
                $("#location").find("option:gt(0)").remove();
            },
            error: function(data){
                console.log('error: '+data);
            },
            complete: function() {
                // Get role after selecting company group
                $.ajax({
                    type: 'POST',
                    data: {
                        _token: token,
                        dealer_group: $("#dealer_group").val()
                    },
                    dataType: 'json',
                    url: "/mo-admin/role/getByCompanyGroup",
                    success:function(data) {
                        $("#user-role").find("option:gt(0)").remove();
                        $.each(data, function(key, obj) {
                            $('#user-role')
                                .append($("<option></option>")
                                    .attr("value", obj.id)
                                    .text(obj.name));
                        });
                        $("#user-role").prop("disabled", false);
                    },
                    error: function(data){
                        console.log('error: '+data);
                    },
                    complete: function() {
                        showLocation(token, $("#dealer_group").val());
                    }
                });
            }
        });
    });

    $("select#dealership").on('change', function(){
        $.ajax({
            type: 'POST',
            data: {
                _token: token,
                dealership: $("#dealership").val()
            },
            dataType: 'json',
            url: "/location/get-by-company",
            success:function(data) {
                $("#location").find("option:gt(0)").remove();
                $.each(data, function(key, obj2) {
                    $('#location')
                        .append($("<option></option>")
                            .attr("value", obj2.id)
                            .text(obj2.name));
                });

                /** ENABLE LOCATION **/
                $("#location").prop("disabled", false);
            },
            error: function(data){
                console.log('error: '+data);
            }
        });
    });

    $(".mo-role-category").on("change", function(){
        var id = $(this).attr("category_id");
        if ($(this).is(":checked")) {
            $(".mo-role-list-item[data-category-id = "+id+"]").removeClass("hidden");
        } else {
            $(".mo-role-list-item[data-category-id = "+id+"]").addClass("hidden");
        }
    });

    //SET COUNTRY ID FOR HIDDEN INPUT
    $("#input-country-id").val($("#user-country").val());
});

