(function ($) {

    $("#mo-submit-setting").click(function (e){
        e.preventDefault();
        $(document).ajaxStart(function() {
            $(".mo-loading-image").removeClass("hidden");
        });
        var token = $("input[name='_token']").val();

        var settings_type = $("#settings-type").val();
        var data_obj = {_token: token};

        switch (settings_type){
            case "valuation":
                $.extend( data_obj, {
                    first_notification_timeout: $("#first-notification-timeout").val(),
                    second_notification_timeout: $("#second-notification-timeout").val(),
                    valuation_timeout: $("#valuation-timeout").val()
                });
                break;
            case "external_offering":
                $.extend( data_obj, {
                    external_offer_timeout: $("#external-offer-timeout").val(),
                    external_offer_expiry_period: $("#external-offer-expiry-period").val(),
                    external_offer_acceptance_timeout: $("#external-offer-acceptance-timeout").val()
                });
                break;
            case "auction":
                $.extend( data_obj, {
                    auction_default_duration: $("#default-duration").val(),
                    auction_price_increment: $("#price-increment").val(),
                    limit_time_edit_auction: $("#limit-time").val()
                });
                break;
            case "rating":
                $.extend( data_obj, {
                    rating_expired_time: $("#rating-duration").val(),
                    speedy_seller_avg_time: $("#speedy-seller").val(),
                    speedy_buyer_avg_time: $("#speedy-payer").val()
                });
                break;
        }

        $.ajax({
            type: "POST",
            url: "/mo-admin/settings/save-settings/"+settings_type,
            data: data_obj,
            success: function (data) {
                if(data == 1){
                    $(".alert-success").removeClass("hidden");
                }else{
                    $(".alert-success").addClass("hidden");
                    $(".alert-danger").removeClass("hidden").find(".error-text").html(data);
                }
            },
            error: function (data) {
                //console.log(data);
            },
            complete: function(){
                $('.mo-loading-image').addClass("hidden");
            }
        });
    });

    $("#mo-save-term-condition").click(function(){
        $(document).ajaxStart(function(){
            $(".mo-loading-image").removeClass('hidden');
        });
        $.ajax({
            type: "POST",
            _token: $("input[name='_token']").val(),
            url: "/mo-admin/settings/general",
            data: $("#mo-settings-term-condition-form").serialize(),
            success: function(data){
                if(data == 1){
                    $(".alert-success").removeClass("hidden");
                }
            },
            error: function(data){
                console.log("error: " + data);
            },
            complete: function(){
                $(".mo-loading-image").addClass("hidden");
            }
        });
    });

}(jQuery));