var pathname = window.location.pathname;

/** STOP MENU DISAPPEAR **/
$(document).on('click', '#mo-transaction-filter', function (e) {
    e.stopPropagation();
});

/** HANDLE FILTER **/
var token = $("input[name='_token']").val();
$("#mo-transaction-make-filter").hover(function(){
    /** GET MAKE LIST **/
    getMakeList(token);
    $(this).unbind('mouseenter mouseleave');//force hover run one time
});

$("#mo-transaction-statuses-filter").hover(function(){
    /** GET MAKE LIST **/
    getTransactionStatuses(token);
    $(this).unbind('mouseenter mouseleave');//force hover run one time
});

function uncheck(checkbox_elem){
    checkbox_elem.each(function(i, obj) {
        $(obj).prop('checked', false);
    });
}

function showFilterText(this_elem){
    if(this_elem.text() != ''){
        this_elem.removeClass("hidden");
    }
}

function displayFilterHandle(){
    var transaction_filter_txt = $(".mo-transaction-status-checkbox:checked:first").val();
    var variant_filter_txt = $(".mo-variant-checkbox:checked:first").val();
    var make_filter_txt = $(".mo-make-checkbox:checked:first").val();
    var model_filter_txt = $(".mo-model-checkbox:checked:first").val()!=null?"/"+$(".mo-model-checkbox:checked:first").val():"";
    var make_model_filter_txt = make_filter_txt+model_filter_txt;
    if($(".mo-transaction-status-checkbox:checked").length > 1){
        transaction_filter_txt+=",...";
    }else if($(".mo-transaction-status-checkbox:checked").length == 0){
        transaction_filter_txt="";
    }

    if($(".mo-variant-checkbox:checked").length > 1){
        variant_filter_txt+=",...";
    }else if($(".mo-variant-checkbox:checked").length == 0){
        variant_filter_txt="";
    }

    if($(".mo-model-checkbox:checked").length > 1){
        make_model_filter_txt+=",...";
    }else if($(".mo-make-checkbox:checked").length == 0){
        make_model_filter_txt="";
    }
    showFilterText($("#transaction-year-filter"));
    $("#transaction-statuses-filter").text(transaction_filter_txt);
    showFilterText($("#transaction-statuses-filter"));
    $("#transaction-variant-filter").text(variant_filter_txt);
    showFilterText($("#transaction-variant-filter"));
    $("#transaction-make-model-filter").text(make_model_filter_txt);
    showFilterText($("#transaction-make-model-filter"));

    var filter_length = $(".mo-filter").not( ".hidden" ).length;
    if(filter_length > 0){
        $("#mo-transaction-filter-header").addClass("slide-txt-display");
        $("#mo-transaction-filter-header").text("filter ("+filter_length+")");
    }
}

function resetFilterHandle(){
    /** RESET FILTER **/
    $("#transaction-year-filter").addClass("hidden");
    $("#transaction-variant-filter").addClass("hidden");
    $("#transaction-make-model-filter").addClass("hidden");
    $("#transaction-statuses-filter").addClass("hidden");

    /** UNCHECK CHECKBOX **/
    uncheck($(".mo-make-checkbox:checked"));
    uncheck($(".mo-model-checkbox:checked"));
    uncheck($(".mo-variant-checkbox:checked"));
    uncheck($(".mo-transaction-status-checkbox:checked"));
    $("#mo-transaction-variant-filter").parent().addClass("disabled-div");

    /** RESET STYLE FOR FILTER **/
    $("#mo-transaction-filter-header").removeClass("slide-txt-display");
    $("#mo-transaction-filter-header").text("filter");
}

/** RANGE SLIDER **/
function rangeDisplay(slider_elem, display_elem, min, max, step, unit, type, from, to, filter_display){
    slider_elem.slider({
        range: true,
        min: min,
        max: max,
        values: [ min, max ],
        step: step,
        slide: function( event, ui ) {
            var value_0 = ui.values[ 0 ];
            var value_1 = ui.values[ 1 ];
            if(type == "number"){
                value_0 = digits(value_0);
                value_1 = digits(value_1);
            }
            display_elem.val( unit + value_0 + " - "+ unit + value_1 );
        },
        change: function( event, ui ) {
            /** SET VALUES FOR HIDDEN FIELDS **/
            var value_0 = ui.values[ 0 ];
            var value_1 = ui.values[ 1 ];
            from.val(value_0);
            to.val(value_1);
            if(type == "number"){
                value_0 = digits(value_0);
                value_1 = digits(value_1);
            }
            filter_display.text( unit + value_0 + " - "+ unit + value_1 );
        }
    });
    var value_0 = slider_elem.slider( "values", 0 );
    var value_1 = slider_elem.slider( "values", 1 );
    if(type == "number"){
        value_0 = digits(value_0);
        value_1 = digits(value_1);
    }
    display_elem.val( unit + value_0 +
        " - "+ unit + value_1 );
}

var year = new Date().getFullYear();
var trans = $("#transaction-slider-range");
rangeDisplay($( "#transaction-year-slider-range" ), $( "#transaction-year-amount" ), 1975, year, 1, "", "", $( "#transaction-year-range-from" ), $( "#transaction-year-range-to" ), $( "#transaction-year-filter" ));

function getTransactionStatuses(token){
    $.ajax({
        type: 'POST',
        data: {_token: token},
        url: "/mo-admin/transaction/get-statuses",
        success: function (data) {
            /*console.log('success: ' + data);*/
            //$("#mo-2nd-level").empty();
            $.each(data, function(i, item) {
                /*console.log(item.value);*/
                var append_elem = $("#mo-2nd-level-clone").children(".menu-item").clone();
                var first_child_clone = append_elem.find(">:first-child");
                var last_child_clone = append_elem.find(">:last-child");

                last_child_clone.remove();
                first_child_clone.find("input").attr("id", "mo-transaction-status-"+item.code);
                first_child_clone.find("input").attr("data-transaction-status-code", item.code);
                first_child_clone.find("input").addClass("mo-transaction-status-checkbox");
                first_child_clone.find("input").attr("value", item.value);
                first_child_clone.find("label").attr("for", "mo-transaction-status-"+item.code);
                first_child_clone.find("span.mo-make-txt").attr("class", "mo-transaction-status-txt padding-left-5 text-capitalize");
                first_child_clone.find("span.mo-transaction-status-txt").text(item.value);
                first_child_clone.find("span.glyphicon-menu-right").remove();
                $("#mo-transaction-statuses-filter").find("#mo-2nd-level").append(append_elem);
            });
        },
        error: function (data) {
            console.log('error: ' + data);
        },
        complete: function (data) {

        }
    });
}

function getMakeList(token){
    $.ajax({
        type: 'POST',
        data: {_token: token},
        url: "/get-glasses-data/make",
        success: function (data) {
            //console.log('success: ' + data);
            //$("#mo-2nd-level").empty();
            $.each(data, function(key, value) {
                //console.log(value);
                var append_elem = $("#mo-2nd-level-clone").children(".menu-item").clone();
                var first_child_clone = append_elem.find(">:first-child");
                var last_child_clone = append_elem.find(">:last-child");

                last_child_clone.children(".menu-item").remove();
                first_child_clone.find("input").attr("id", "mo-make-"+key);
                first_child_clone.find("input").addClass("mo-make-checkbox");
                first_child_clone.find("input").attr("value", value);
                first_child_clone.find("label").attr("for", "mo-make-"+key);
                first_child_clone.find("span.mo-make-txt").text(value);
                $("#mo-transaction-make-filter").find("#mo-2nd-level").append(append_elem);
            });
        },
        error: function (data) {
            console.log('error: ' + data);
        },
        complete: function (data) {
            $("#mo-transaction-make-filter").find("#mo-2nd-level").children(".menu-item").hover(function() {
                /** GET MODEL LIST - BY MAKE **/
                    //console.log("hover");
                var first_child = $(this).find(">:first-child");
                getModelList($(this), token, first_child.find("input").val());
                $(this).unbind('mouseenter mouseleave');//force hover run one time
            });

            /** CHECK BOX - FILTER **/
            var menu_item_elem = $("#mo-transaction-make-filter").find("#mo-2nd-level").children(".menu-item");
            var checkbox_1st = menu_item_elem.find(">:first-child").find("input");
            checkbox_1st.change(function(){
                var parent_menu_item = $(this).parents(".menu-item");
                var last_child_elem = parent_menu_item.find(">:last-child").children(".menu-item");
                if($(this).is(':checked')){
                    last_child_elem.each(function(i, obj) {
                        $(obj).find("input").prop('checked',true);
                    });
                    checkLengthCheckBox(menu_item_elem);
                }else{
                    last_child_elem.each(function(i, obj) {
                        $(obj).find("input").prop('checked',false);
                    });
                    checkLengthCheckBox(menu_item_elem);
                }
            });
        }
    });
}

function checkLengthCheckBox(menu_item_elem){
    if(menu_item_elem.find("input:checkbox:checked").length > 0){
        $("#mo-transaction-variant-filter").parent().removeClass("disabled-div");
        var make_arr = [];var model_arr = [];var model_obj = {};
        /*menu_item_elem.find(".mo-make-checkbox:checked").each(function(i, obj) {
         //make_arr[i] = $(obj).val();
         make_arr[$(obj).data("id")] = $(obj).val();
         });*/

        var vehicle_name = "";
        menu_item_elem.find(".mo-model-checkbox:checked").each(function(i, obj) {
            //model_arr[i] = $(obj).val();
            if(vehicle_name != $(obj).data("vehicle-name")){
                vehicle_name = $(obj).data("vehicle-name");
                if(vehicle_name != -1){
                    model_obj[vehicle_name] = [];//initialize array inside object
                }
            }
            if(vehicle_name != -1){
                model_obj[vehicle_name].push($(obj).val());//add item into array in object
            }
        });
        //console.log(model_obj);
        $("#mo-transaction-variant-filter").find("#mo-2nd-level").empty();
        for (var key in model_obj) {
            if (model_obj.hasOwnProperty(key)) {
                //console.log(key + " -> " + model_obj[key]);
                for (var i = 0; i<model_obj[key].length; i++){
                    getVariantList(token, key, model_obj[key][i]);
                }
            }
        }
    }else{
        $("#mo-transaction-variant-filter").parent().addClass("disabled-div");
    }
}

function getModelList(this_elem, token, make){
    $.ajax({
        type: 'POST',
        data: {_token: token, make: make},
        url: "/get-glasses-data/family",
        success: function (data) {
            //console.log('success: ' + data);
            //this_elem.children("#mo-3rd-level").empty();
            $.each(data, function(key, value) {
                //console.log(value);
                var elem_1st = $("#mo-2nd-level-clone").children(".menu-item");
                var last_child_clone = elem_1st.find(">:last-child").children(".menu-item").clone();

                this_elem.children("#mo-3rd-level").removeClass("hidden");
                last_child_clone.find("input").attr("id", "mo-model-"+make.replace(/\s/g,'')+"-"+key);
                last_child_clone.find("input").addClass("mo-model-checkbox");
                last_child_clone.find("input").attr("value", value);
                last_child_clone.find("input").attr("data-vehicle-name", make);
                last_child_clone.find("label").attr("for", "mo-model-"+make.replace(/\s/g,'')+"-"+key);
                last_child_clone.find("span.mo-model-txt").text(value);
                this_elem.children("#mo-3rd-level").append(last_child_clone);
            });
        },
        complete: function (data){
            $(".mo-model-checkbox").change(function(){
                var parent_menu_item = $(this).parents("#mo-2nd-level").children(".menu-item");
                checkLengthCheckBox(parent_menu_item);
            });
        },
        error: function (data) {
            console.log('error: ' + data);
        }
    });
}

function getVariantList(token, make_arr, model_arr){
    /*console.log(make_arr);
     console.log(model_arr);*/
    $.ajax({
        type: 'POST',
        data: {_token: token, make: make_arr, model: model_arr},
        url: "/get-glasses-data/variant",
        success: function (data) {
            //console.log('success: ' + data);
            var newArray = data.filter(function(v){return v!==''});
            //$("#mo-transaction-variant-filter").find("#mo-2nd-level").empty();
            var total = newArray.length;
            $.each(newArray, function(key, value) {
                //console.log(value);
                if(value != ''){
                    var append_elem = $("#mo-2nd-level-clone").children(".menu-item").clone();
                    var first_child_clone = append_elem.find(">:first-child");
                    var last_child_clone = append_elem.find(">:last-child");

                    if (key === 0) {
                        // this is the first one
                        var make_model_elem = $("#mo-make-model-name").clone().removeClass("hidden");
                        make_model_elem.find("span.mo-make-model-txt").text(make_arr+"/"+model_arr);
                        append_elem.prepend(make_model_elem);
                    }

                    last_child_clone.remove();
                    first_child_clone.find("input").attr("id", "mo-variant-"+model_arr+"-"+value.replace(/\s/g,''));
                    first_child_clone.find("input").attr("value", value);
                    first_child_clone.find("input").attr("data-vehicle-name", make_arr);
                    first_child_clone.find("input").attr("data-model-name", model_arr);
                    first_child_clone.find("input").addClass("mo-variant-checkbox");
                    first_child_clone.find("label").attr("for", "mo-variant-"+model_arr+"-"+value.replace(/\s/g,''));
                    first_child_clone.find("span.mo-make-txt").attr("class", "mo-variant-txt padding-left-5");
                    first_child_clone.find("span.mo-variant-txt").text(value);
                    first_child_clone.find("span.glyphicon-menu-right").remove();
                    $("#mo-transaction-variant-filter").find("#mo-2nd-level").append(append_elem);
                }
            });
        },
        error: function (data) {
            console.log('error: ' + data);
        }
    });
}

function makeInfo(){
    var make_info = [];
    var make_lenth = $(".mo-make-checkbox:checked").length;
    var model_lenth = $(".mo-model-checkbox:checked").length;
    var variant_lenth = $(".mo-variant-checkbox:checked").length;
    //console.log("variant length: "+variant_lenth);
    if(variant_lenth > 0){
        $(".mo-variant-checkbox:checked").each(function(i, obj) {
            var make_info_obj = {
                make: $(obj).data("vehicle-name"),
                model: $(obj).data("model-name"),
                variant: $(obj).val()
            };
            make_info[i] = make_info_obj;
        });
    }
    else if(model_lenth > 0 && variant_lenth == 0){
        $(".mo-model-checkbox:checked").each(function(i, obj) {
            var make_info_obj = {
                make: $(obj).data("vehicle-name"),
                model: $(obj).val(),
                variant: ""
            };
            make_info[i] = make_info_obj;
        });
    }
    else if(make_lenth > 0 && model_lenth == 0 && variant_lenth == 0){
        $(".mo-make-checkbox:checked").each(function(i, obj) {
            var make_info_obj = {
                make: $(obj).val(),
                model: "",
                variant: ""
            };
            make_info[i] = make_info_obj;
        });
    }

    return make_info;
}

var init = true;
var query = null;
var offset = 0;
var active = null;
if ($("#required-action").is(":checked")) {
    active = true;
}

/** SET SESSION STORAGE TO PAGING **/
var ajax_data = function (d) {
    d._token = $("input[name='_token']").val();
    d.offset = offset;
    if(active){
        d.active = active;
    }
};

/*** start - APPLY FILTER ***/
$("#mo-btn-transaction-apply-filter").click(function(){
    var year_range = [$("#transaction-year-range-from").val(), $("#transaction-year-range-to").val()];
    var makeInformation = makeInfo();
    var transaction_status_arr=[];
    $(".mo-transaction-status-checkbox:checked").each(function(i, obj) {
        transaction_status_arr[i] = $(obj).data("transaction-status-code");
    });
    displayFilterHandle();

    active = null;
    if ($("#required-action").is(":checked")) {
        active = true;
    }
    console.log("active "+active);

    var ajax_data = {
        _token : $("input[name='_token']").val(),
        offset : offset,
        active : active,
        status : transaction_status_arr,
        year : year_range,
        makeInfo : makeInformation
    };

    var session_storage_name = "dealership_elem_to_paging";
    if(pathname.indexOf("/mo-admin/transaction/") != -1){
        session_storage_name = "elem_to_paging";
    }
    var datatable_obj = JSON.parse(sessionStorage.getItem(session_storage_name));


    /** SET SESSION STORAGE TO PAGING **/
    var param_obj = {ajax_data:ajax_data};
    if(datatable_obj !== null && typeof datatable_obj === 'object'){
        var data = $.extend({}, datatable_obj, param_obj);
        sessionStorage.setItem(session_storage_name, JSON.stringify(data));
    }

    if(pathname.indexOf("/mo-admin/transaction/") != -1){
        $.getTransactionsList(true, init, query, offset, datatable_obj.request_url, datatable_obj.table_id, active, datatable_obj.tab, ajax_data);
    }else{
        $.getTransactionsList(true, init, query, offset, datatable_obj.request_url, datatable_obj.table_id, active, ajax_data);
    }
});

$("#mo-btn-transaction-reset-filter").click(function(){
    if(pathname.indexOf("/mo-admin/transaction/") != -1){
        var datatable_obj = JSON.parse(sessionStorage.getItem("elem_to_paging"));
        $.getTransactionsList(true, init, query, offset, datatable_obj.request_url, datatable_obj.table_id, active, datatable_obj.tab);
    }else{
        var datatable_obj = JSON.parse(sessionStorage.getItem("dealership_elem_to_paging"));
        $.getTransactionsList(true, init, query, offset, datatable_obj.request_url, datatable_obj.table_id, active);
    }

    resetFilterHandle();
});
