$(document).ready(function(){
    var hash = window.location.hash,
        tabName = '';

    if (hash) {
        tabName = hash.slice(1, hash.length);
    }

    var token = $("input[name='_token']").val();/** COMMON TOKEN **/

    /** PRE-DEFINED AJAX FOR ACTION BUTTON **/
    function callAjax(transaction_id, url, transaction_rid, transport_no, payment_amount){
        console.log("transaction_id "+transaction_id);
        console.log("url "+url);
        $(document).on("ajaxStart.buttonAction", function () {
            $(".mo-loading-image").removeClass("hidden");
        });

        $.ajax({
            type: 'POST',
            data: {
                _token: token,
                transaction_id: transaction_id,
                transaction_rid: transaction_rid,
                transport_no: transport_no,
                payment_amount: payment_amount
            },
            url: url,
            success: function (data) {
                console.log(data);
                if(data == 1){
                    if(sessionStorage.getItem("elem_to_paging")) {
                        var data_obj = JSON.parse(sessionStorage.getItem("elem_to_paging"));
                    }
                    active = null;
                    if ($("#required-action").is(":checked")) {
                        active = true;
                    }
                    $.getTransactionsList(true, init, query, offset, data_obj.request_url, data_obj.table_id, active, data_obj.tab);
                }else{
                    /*$(".alert-danger").find(".mo-error-message").html(data);
                     $(".alert-danger").removeClass("hidden");*/
                }
            },
            error: function (data) {
                console.log('error: ' + data);
            },
            complete: function () {
                $(".mo-loading-image").addClass("hidden");
                var magnificPopup = $.magnificPopup.instance;
                magnificPopup.close();
            }
        });
    }

    var id = "#mo-table-transactions-list";
    var destroy = false;
    var init = true;
    var query = null;
    var offset = 0;
    var active = null;
    var type = $("#type").val();

    /** DATA TABLE **/
    $.getTransactionsList = function(destroy, init, query, offset, request_url, id, active, tab, ajax_data) {
        var requiredAction = sessionStorage.getItem('required-action');
        if (requiredAction) {
            active = requiredAction;
        }
        var destroy = destroy;
        var columns = [
            {className: "never"},//ID - hidden
            {className: "all", "width":"10%"},//Trans#
            {className: "all", "bSortable": false, "width":"15%"},//Selling Dealer
            {className: "all", "bSortable": false, "width":"15%"},//Buying Dealer
            {className: "all", "bSortable": false, "width":"20%"},//Vehicle Name
            {className: "all", "bSortable": false, "width":"10%"},//Outstanding Amount
            {className: "all text-capitalize", "bSortable": false, "width":"10%"},//Status
            {className: "never"},//status

            {
                className: "all group-btn", "bSortable": false,
                "mRender": function (data, type, row, meta) {
                    var menu_right = '<a href="/mo-admin/transaction/transaction-detail/'+$("#type").val()+'/'+row[0]+'"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                    var btn = '';
                    var group_btn = '';
                    if(row[7]==1){
                        btn = '<a href="#mo-transaction-receive-payment-popup" class="mo-payment-popup">' +
                            '<input type="button" class="font-family-bold mo-transaction-btn mo-btn-yellow text-uppercase" value="receive payment" data-status="'+row[7]+'" data-transaction-id="'+row[0]+'" data-transaction-rid="'+row[1]+'" data-outstanding-amount="'+row[5]+'">'+
                            '</a>';
                    }else if(row[7]==22){//booking transport
                        btn = '<a href="#mo-transaction-booking-transport-popup" class="mo-booking-popup">' +
                        '<input type="button" class="font-family-bold mo-transaction-btn mo-btn-yellow text-uppercase" value="transport booked" data-status="'+row[7]+'" data-transaction-id="'+row[0]+'">' +
                        '</a>';
                    }else if(row[7]==23){//booked transport
                        btn = '<input type="button" class="font-family-bold mo-transaction-btn mo-btn-yellow text-uppercase" value="vehicle picked up" data-status="'+row[7]+'" data-transaction-id="'+row[0]+'">';
                    }else if(row[7]==24){//in transit
                        btn = '<input type="button" class="font-family-bold mo-transaction-btn mo-btn-yellow text-uppercase" value="vehicle arrived" data-status="'+row[7]+'" data-transaction-id="'+row[0]+'">';
                    }else if(row[7]==28){//waiting to return
                        btn = '<input type="button" class="font-family-bold mo-transaction-btn mo-btn-yellow text-uppercase" value="vehicle returned" data-status="'+row[7]+'" data-transaction-id="'+row[0]+'">';
                    }
                    group_btn = menu_right + btn;
                    return group_btn;
                }
            }
        ];
        if(tab == "organise-transport" || tab == "payouts"){
            columns = [
                {className: "never"},//ID - hidden
                {className: "all", "width":"10%"},//Trans#
                {className: "all", "bSortable": false, "width":"15%"},//Selling Dealer
                {className: "all", "bSortable": false, "width":"15%"},//Buying Dealer
                {className: "all", "bSortable": false, "width":"20%"},//Vehicle Name
                {className: "all text-capitalize", "bSortable": false, "width":"10%"},//Status
                {className: "never"},//status
                {
                    className: "all group-btn", "bSortable": false,
                    "mRender": function (data, type, row, meta) {
                        var menu_right = '<a href="/mo-admin/transaction/transaction-detail/'+$("#type").val()+'/'+row[0]+'"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                        var btn = '';
                        var group_btn = '';
                        if(row[6]==1){
                            btn = '<a href="#mo-transaction-receive-payment-popup" class="mo-payment-popup">' +
                                '<input type="button" class="font-family-bold mo-transaction-btn mo-btn-yellow text-uppercase" value="receive payment" data-status="'+row[6]+'" data-transaction-id="'+row[0]+'" data-transaction-rid="'+row[1]+'">'+
                                '</a>';
                        }else if(row[6]==22){//booking transport
                            btn = '<a href="#mo-transaction-booking-transport-popup" class="mo-booking-popup">' +
                                '<input type="button" class="font-family-bold mo-transaction-btn mo-btn-yellow text-uppercase" value="transport booked" data-status="'+row[6]+'" data-transaction-id="'+row[0]+'">' +
                                '</a>';
                        }else if(row[6]==23){//booked transport
                            btn = '<input type="button" class="font-family-bold mo-transaction-btn mo-btn-yellow text-uppercase" value="vehicle picked up" data-status="'+row[6]+'" data-transaction-id="'+row[0]+'">';
                        }else if(row[6]==24){//in transit
                            btn = '<input type="button" class="font-family-bold mo-transaction-btn mo-btn-yellow text-uppercase" value="vehicle arrived" data-status="'+row[6]+'" data-transaction-id="'+row[0]+'">';
                        }else if(row[6]==28){//waiting to return
                            btn = '<input type="button" class="font-family-bold mo-transaction-btn mo-btn-yellow text-uppercase" value="vehicle returned" data-status="'+row[6]+'" data-transaction-id="'+row[0]+'">';
                        }
                        group_btn = menu_right + btn;
                        return group_btn;
                    }
                }
            ];
        }
        var error_msg = "<div>There are no transaction to display at this time.</div>";
        var scrollX = true;
        var empty_msg = "";
        $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg, active, ajax_data);
    }

    if(type == "complete"){
        /** disabled transaction status filter **/
        $("#mo-transaction-statuses-filter").addClass("disabled-div");

        var request_url = "/mo-admin/transaction/get-transactions/"+type;
        console.log("request_url "+request_url);
        var table_id = id + "-all";
        destroy = true;
        $.getTransactionsList(destroy, init, query, offset, request_url, table_id, active);
    }

    /** BLOCK - CLICK **/
    $('#mo-blocks-wrapper a').click(function () {
        /** clear filter when switching block **/
        resetFilterHandle();

        var tab = $(this).attr('tab');

        /** disabled transaction status filter **/
        $("#mo-transaction-statuses-filter").removeClass("disabled-div");
        if(tab == "awaiting-payment"){
            $("#mo-transaction-statuses-filter").addClass("disabled-div");
        }

        $('#mo-blocks-wrapper .mo-block').removeClass('mo-block-active');
        $('.content-block').hide();/** table content block **/

        $(this).find('.mo-block').addClass('mo-block-active');
        $('#' + tab + '-block').show();

        console.log("tab "+tab);

        var request_url = "/mo-admin/transaction/get-transactions/"+type+"/"+tab;
        console.log("request_url "+request_url);
        var table_id = id + "-"+tab;
        destroy = true;
        $.getTransactionsList(destroy, init, query, offset, request_url, table_id, active, tab);

        /** SET SESSION STORAGE TO PAGING **/
        var data = {
            table_id: table_id,
            request_url: request_url,
            tab: tab
        };
        sessionStorage.setItem("elem_to_paging", JSON.stringify(data));

        $('#required-action').attr('checked', false);
    });

    /** SET 1ST ACTIVE BLOCK - OR BY HASH TAG **/
    if (tabName) {
        $('a[tab="' + tabName + '"]').click();
    } else {
        $('#mo-blocks-wrapper a').first().click();
    }

    /** HANDLE BUTTON ACTION **/
    $(document).on("click", ".mo-transaction-btn", function () {
        console.log("clicked");
        var transaction_status = $(this).data("status");
        var transaction_id = $(this).data("transaction-id");
        var transaction_rid = $(this).data("transaction-rid");
        var outstanding_amount = $(this).data("outstanding-amount");
        var url = "";
        if(transaction_status == 1){
            $("form#mo-payment-form").find("#popup-transaction-id").val(transaction_id);
            $("form#mo-payment-form").find("#popup-transaction-rid").val(transaction_rid);
            $("form#mo-payment-form").find("span#outstanding-amount-payment").text("$"+outstanding_amount);
            $("form#mo-payment-form").find("#payment-amount").val(parseFloat(outstanding_amount.replace(/,/g, '')));
            /*url = "/mo-admin/transaction/receive-payment";
            callAjax(transaction_id, url, transaction_rid);*/
        }else if(transaction_status == 22){
            $("form#mo-transaction-booking-form").find("#popup-transaction-id").val(transaction_id);
            /*url = "/mo-admin/transaction/book-transport";
            callAjax(transaction_id, url);*/
        }else if(transaction_status == 23){
            url = "/mo-admin/transaction/pick-up-vehicle";
            callAjax(transaction_id, url);
        }else if(transaction_status == 24){
            url = "/mo-admin/transaction/shipped-vehicle";
            callAjax(transaction_id, url);
        }else if(transaction_status == 28){
            url = "/mo-admin/transaction/awaiting-document";
            callAjax(transaction_id, url);
        }
    });

    /** VALIDATE TRANSACTION BOOKING TRANSPORT FORM **/
    $('#mo-transaction-booking-form').validate({
        rules:{
            transport_tracking_no: "required"
        },
        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        errorPlacement: function(error, element) {
            error.insertAfter(element);// default placement
        },
        messages:{
            transport_tracking_no: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>"
        },
        submitHandler: function(form){
            //form.submit();
            url = "/mo-admin/transaction/book-transport";
            callAjax($("#popup-transaction-id").val(), url, null, $("#transport-tracking-no").val());
        }
    });

    $("#payment-btn").click(function () {
        if($('#mo-payment-form').valid()){
            url = "/mo-admin/transaction/receive-payment";
            callAjax($("#popup-transaction-id").val(), url, $("#popup-transaction-rid").val(), null, $("#payment-amount").val());
        }
    });

    /** VALIDATE RECEIVE PAYMENT FORM **/
    /*$('#mo-payment-form').validate({
        rules:{
            payment_amount: "required"
        },
        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        errorPlacement: function(error, element) {
            error.insertAfter(element);// default placement
        },
        messages:{
            payment_amount: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>"
        },
        submitHandler: function(form){
            //form.submit();
            url = "/mo-admin/transaction/receive-payment";
            callAjax($("#popup-transaction-id").val(), url, $("#popup-transaction-rid").val(), null, $("#payment-amount").val());
        }
    });*/
});