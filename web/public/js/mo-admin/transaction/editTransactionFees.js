$(document).ready(function() {

	$(".edit-transaction-fees").click(function(){

		$(this).next().val($(this).children(".fee-number").html());

		$(this).addClass("hidden");
		$(this).next().removeClass("hidden");
		$(".edit-transaction-fees-input").focus();

	});

	$(".edit-transaction-fees-input").blur(function(){
		
		var input_fee = $(this);
		var from_region = $(this).prev().attr("id").split("-")[0];
		var to_region = $(this).prev().attr("id").split("-")[1];

		input_fee.prev().removeClass("hidden");
		input_fee.prev().children(".fee-number").text(input_fee.val());
		input_fee.addClass("hidden");

	});

	$("#transaction-fees-save-btn").click(function(){

		var transport_prices = [];
		$.each($(".edit-transaction-fees"), function(index, value) {
			
			var from_region = $(value).attr("id").split("-")[0];
			var to_region = $(value).attr("id").split("-")[1];
			var price = $(value).children(".fee-number").text();

			var transport_price = {"from_region": from_region,
									"to_region": to_region,
									"price": price};

			transport_prices.push(transport_price);
		});

		$(document).ajaxStart(function() {
	        $(".mo-loading-image").removeClass("hidden");
	    });
		//ajax update transaction here
		$.ajax({
			type:'POST',
			url: "/mo-admin/transaction/edit-fees",
			data: {
				_token: $("input[name='_token']").val(),
				buyer_fee: $("input[name='for_buyer_fee']").val(),
				seller_fee: $("input[name='for_seller_fee']").val(),
				transport_prices: transport_prices
			},
			success:function(data) {

				if (data != null) {
					data = $.parseJSON(data);
					if (typeof (data.success) != "undefined" && data.success) {
						$(".transaction-fees-setting-alert").removeClass("hidden");
						$('body,html').animate({
			                scrollTop: 0
			            }, 800);
					}
				}

			},
			error: function(data){
				$(".transaction-fees-setting-error-alert").removeClass("hidden");
				$('body,html').animate({
	                scrollTop: 0
	            }, 800);
			},
			complete: function(){
				$(".mo-loading-image").addClass("hidden");
			}
		                    
		});

	});

});