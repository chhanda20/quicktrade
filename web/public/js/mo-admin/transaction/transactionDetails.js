$(document).ready(function(){
    var token = $("input[name='_token']").val();/** COMMON TOKEN **/
    var transaction_id = $("#transaction-id").val();
    var transaction_rid = $("#transaction-rid").text();

    /** BOOKING TRANSPORT POPUP **/
    $(document).on("click",".mo-cancel-btn",function() {
        var magnificPopup = $.magnificPopup.instance;
        magnificPopup.close();
    });

    $(".mo-booking-popup").magnificPopup({
        type:'inline',
        showCloseBtn: true,
        closeOnBgClick: false
    });

    $(".mo-payment-popup").magnificPopup({
        type:'inline',
        showCloseBtn: true,
        closeOnBgClick: false
    });

    /** PRE-DEFINED AJAX FOR ACTION BUTTON **/
    function callAjax(url, transport_no, payment_amount, expected_status){
        /*console.log("transaction_id "+transaction_id);
        console.log("transaction_rid "+transaction_rid);
        console.log("url "+url);*/
        var type = $("#type").val();
        $(document).on("ajaxStart.buttonAction", function () {
            $(".mo-loading-image").removeClass("hidden");
        });

        $.ajax({
            type: 'POST',
            data: {
                _token: token,
                transaction_id: transaction_id,
                transaction_rid: transaction_rid,
                transport_no: transport_no,
                payment_amount: payment_amount,
                expected_status: expected_status
            },
            url: url,
            success: function (data) {
                console.log(data);
                if(data == 1){
                    window.location.href = "/mo-admin/transaction/view-transactions/"+type;
                }else{
                    $(".alert-danger").find(".mo-error-message").html(data);
                     $(".alert-danger").removeClass("hidden");
                }
            },
            error: function (data) {
                console.log('error: ' + data);
            },
            complete: function () {
                $(".mo-loading-image").addClass("hidden");
                var magnificPopup = $.magnificPopup.instance;
                magnificPopup.close();
            }
        });
    }

    /** HANDLE BUTTON ACTION **/
    $(document).on("click", "#transaction-btn", function () {
        console.log("clicked");
        var transaction_status = $("#transaction-status").val();
        var url = "";
        if(transaction_status == 1){
            /*url = "/mo-admin/transaction/receive-payment";
            callAjax(url);*/
        }else if(transaction_status == 22){
            $("#popup-transaction-id").val(transaction_id);
            /*url = "/mo-admin/transaction/book-transport";
             callAjax(transaction_id, url);*/
        }else if(transaction_status == 23){
            url = "/mo-admin/transaction/pick-up-vehicle";
            callAjax(url);
        }else if(transaction_status == 24){
            url = "/mo-admin/transaction/shipped-vehicle";
            callAjax(url);
        }else if(transaction_status == 28){
            url = "/mo-admin/transaction/returned";
            callAjax(url);
        }else if(transaction_status == 26){
            if(!$("#mo-all-document").is(":checked")){
                $("#approve-transaction-form").valid();
            }else{
                if ($("#approve-transaction-form").valid()) {
                    console.log("valid");
                    url = "/mo-admin/transaction/complete";
                    callAjax(url, null, null, 27);//documented
                }
            }

            if($("#mo-all-document").is(":checked") && $("#mo-seller-mark-as-paid").is(":checked") && $("#mo-buyer-mark-as-paid").is(":checked")){
                if ($("#complete-transaction-form").valid() && $('#approve-transaction-form').valid()) {
                    url = "/mo-admin/transaction/complete";
                    callAjax(url, null, null, 5);
                }
            }
        }else if(transaction_status == 27){
            if ($("#complete-transaction-form").valid() && $('#approve-transaction-form').valid()) {
                url = "/mo-admin/transaction/complete";
                callAjax(url, null, null, 5);
            }
        }
    });

    /** VALIDATE TRANSACTION BOOKING TRANSPORT FORM **/
    $('#mo-transaction-booking-form').validate({
        rules:{
            transport_tracking_no: "required"
        },
        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        errorPlacement: function(error, element) {
            error.insertAfter(element);// default placement
        },
        messages:{
            transport_tracking_no: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>"
        },
        submitHandler: function(form){
            //form.submit();
            url = "/mo-admin/transaction/book-transport";
            callAjax(url, $("#transport-tracking-no").val());
        }
    });

    /** VALIDATE TRANSACTION BOOKING TRANSPORT FORM **/
    $('#mo-payment-form').validate({
        rules:{
            payment_amount: "required"
        },
        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        errorPlacement: function(error, element) {
            error.insertAfter(element);// default placement
        },
        messages:{
            payment_amount: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>"
        },
        submitHandler: function(form){
            //form.submit();
            url = "/mo-admin/transaction/receive-payment";
            callAjax(url, null, $("#payment-amount").val());
        }
    });

    /** VALIDATE - ENABLE APPROVE DOCUMENT **/
    /*$(".mo-view-button:not(.mo-view-button-additional)").each(function(i, obj) {
        if($(obj).hasClass("disabled-div")){
            $("#mo-all-document").attr("disabled", "disabled");
            $("label[for='mo-all-document']").addClass("disabled-div");
        }
    });*/

    $(".mo-view-button:not(#mo-view-seller-invoice, #mo-view-buyer-invoice, #mo-view-credit-note)").each(function(i, obj){
        if($(obj).hasClass("disabled-div")){
            $("#mo-all-document").attr("disabled", "disabled");
            $("label[for='mo-all-document']").addClass("disabled-div");
        }
    });


    /** VALIDATE - APPROVE FORM **/
    $('#approve-transaction-form').validate({
        rules:{
            mo_all_document: "required"
        },
        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        errorPlacement: function(error, element) {
            if (element.attr("name") == "mo_all_document" ) {
                error.appendTo("#errorToShow_all_document");
            }
        },
        messages:{
            mo_all_document: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This need to be checked and approved.</span>"+"</div>"
        }
    });

    /** VALIDATE - COMPLETE FORM **/
    $('#complete-transaction-form').validate({
        rules:{
            mo_seller_mark_as_paid: "required",
            mo_buyer_mark_as_paid: "required"
        },
        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        errorPlacement: function(error, element) {
            if (element.attr("name") == "mo_seller_mark_as_paid" ) {
                error.appendTo("#errorToShow_seller_paid");
            } else if (element.attr("name") == "mo_buyer_mark_as_paid" ) {
                error.appendTo("#errorToShow_buyer_paid");
            }
        },
        messages:{
            mo_seller_mark_as_paid: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This need to be checked.</span>"+"</div>",
            mo_buyer_mark_as_paid: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This need to be checked.</span>"+"</div>"
        }
    });

});