var hash = window.location.hash,
    tabName = '';

if (hash) {
    tabName = hash.slice(1, hash.length);
}

$('#mo-blocks-wrapper a').click(function () {

    $('#mo-txt-search').val('');
    var tab = $(this).attr('tab');
    var status = $(this).attr('status');

    localStorage.setItem('status', status);

    $('#mo-blocks-wrapper .mo-block').removeClass('mo-block-active');
    $('.data-block').hide();

    $(this).find('.mo-block').addClass('mo-block-active');
    $('#' + tab + '-block').show();

    var content_id = '#' + tab + '-block';
    var url = '/mo-admin/dispute/list';
    var container = $(content_id);

    /** SET LOCAL STORAGE TO APPLY FILTER **/
    var data = {
        content_id: content_id,
        url: url,
        status: status
    };
    localStorage.setItem("elem_to_filter", JSON.stringify(data));

    container.html('');
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            limit: limit,
            offset: 0,
            status: status
        },
        success: function (html) {
            container.html(html);
            infinityScroll("");
        }
    });
});

if (tabName) {
    $('a[tab="' + tabName + '"]').click();
} else {
    $('#mo-blocks-wrapper a').first().click();
}

/** INFINITE SCROLL **/
var content = $("#mo-dispute-grid-wrapper #new-block");
var content_id = '#new-block';
var url = '/mo-admin/dispute/list';
var status = 1; // Status of 'new'

/** SET LOCAL STORAGE TO APPLY FILTER **/
var data = {
    content_id: content_id,
    url: url,
    status: status
};
localStorage.setItem("elem_to_filter", JSON.stringify(data));
infinityScroll("");

$(document).on('click', '.accept_btn a', function(e) {
    e.preventDefault();
    var disputeId = $(this).parents('.dispute-item').attr('item');
    var transactionId = $(this).parents('.dispute-item').find('.transaction_rid').data('transaction-id');
    $(this).hide();
    $.ajax({
        type: "POST",
        url: '/mo-admin/dispute/accept',
        data: {
            _token: token,
            id: disputeId
        },
        success: function (res) {
            if (res) {
                window.location = '/mo-admin/dispute/info/'+transactionId+'/'+disputeId;
            }
        }
    });
});

/*** start - QUERY SEARCH ***/
$(".mo-txt-search").click(function(){
    var data = JSON.parse(localStorage.getItem("elem_to_filter"));
    var status = localStorage.getItem("status");
    var query_obj = {
        query: $("#mo-txt-search").val(),
        status: status
    };
    var content = $(data.content_id);
    var url = '/mo-admin/dispute/list';
    callAjaxGetList(content, query_obj, url);
});
$("#mo-txt-search").bind("enterKey", function(e){
    var data = JSON.parse(localStorage.getItem("elem_to_filter"));
    var status = localStorage.getItem("status");
    var query_obj = {
        query: $("#mo-txt-search").val(),
        status: status
    };
    var content = $(data.content_id);
    var url = '/mo-admin/dispute/list';
    var status = $(data.status);
    callAjaxGetList(content, query_obj, url);
});
$('#mo-txt-search').keyup(function(e){
    if(e.keyCode == 13)
    {
        $(this).trigger("enterKey");
    }
});
/*** end - APPLY FILTER ***/