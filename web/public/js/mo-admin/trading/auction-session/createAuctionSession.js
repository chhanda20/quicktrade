/**
 * Created by Son on 6/8/2017.
 */

/** Set Datepicker **/
var date = new Date();
date.setDate(date.getDate());

$("#ad-hoc-date-picker").datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    orientation: 'bottom',
    autoclose: true,
    startDate: date
});

/** WEEKDAYS ARRAY **/
var weekdays_array = ["mon","tue","wed","thu","fri","sat","sun"];

/** SET LOCAL STORAGE WEEKDAYS **/
var data = {
    start_date_index: "",
    end_date_index: "",
    diff_index: "",
    diff_date: ""
};
localStorage.setItem("weekdays", JSON.stringify(data));

$("#start-from-date-picker").datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    orientation: 'bottom',
    autoclose: true,
    startDate: date
}).on('changeDate', function (ev) {
    var start_date = $(this).datepicker('getDate');
    var end_date = $("#end-at-date-picker").datepicker('getDate');
    var selected_week_day = $.datepicker.formatDate('D', start_date).toLowerCase();
    //console.log(selected_week_day);
    var start_date_index = weekdays_array.indexOf(selected_week_day);
    //console.log(start_date_index);

    var diff_index = "";
    var diff_date = "";
    if(end_date !== null){
        diff_date = parseInt((end_date.getTime()-start_date.getTime())/(24*3600*1000));
        //console.log(diff_date);
    }

    var data_storage = JSON.parse(localStorage.getItem("weekdays"));
    data_storage.start_date_index = start_date_index;
    if(start_date_index !== "" && data_storage.end_date_index !== ""){
        diff_index = data_storage.end_date_index - start_date_index;
    }
    data_storage.diff_index = diff_index;
    data_storage.diff_date = diff_date;
    localStorage.setItem("weekdays", JSON.stringify(data_storage));

    fillWeekdays();
});




$("#end-at-date-picker").datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    orientation: 'bottom',
    autoclose: true,
    startDate: date
}).on('changeDate', function (ev) {
    var end_date = $(this).datepicker('getDate');
    var start_date = $("#start-from-date-picker").datepicker('getDate');
    var selected_week_day = $.datepicker.formatDate('D', end_date).toLowerCase();

    var end_date_index = weekdays_array.indexOf(selected_week_day);
    //console.log(end_date_index);

    var diff_index = "";
    var diff_date = "";
    if(start_date != null){
        diff_date = parseInt((end_date.getTime()-start_date.getTime())/(24*3600*1000));
        //console.log(diff_date);
    }


    var data_storage = JSON.parse(localStorage.getItem("weekdays"));
    data_storage.end_date_index = end_date_index;
    /*console.log("start_date_index "+data_storage.start_date_index);
    console.log("end_date_index "+end_date_index);*/
    if(data_storage.start_date_index !== "" && end_date_index !== ""){
        diff_index = end_date_index - data_storage.start_date_index;
    }
    data_storage.diff_index = diff_index;
    data_storage.diff_date = diff_date;
    localStorage.setItem("weekdays", JSON.stringify(data_storage));

    fillWeekdays();
});

/** Fill in weekdays **/
function fillWeekdays(){
    var updated_data_storage = JSON.parse(localStorage.getItem("weekdays"));
    console.log(updated_data_storage);

    if(updated_data_storage.diff_date !== ""){
        $(".mo-week-days").prop("checked", false);
        $(".mo-week-days").parent().addClass("disabled-div");
        if(updated_data_storage.diff_date < 7){
            if(updated_data_storage.diff_index > 0){
                for(var i=0; i<weekdays_array.length; i++){
                    if(i >= updated_data_storage.start_date_index && i <= updated_data_storage.end_date_index){
                        $('input[id="'+weekdays_array[i]+'"]').parent().removeClass("disabled-div");
                    }
                }
            }else if(updated_data_storage.diff_index < 0){
                for(var j=0; j<weekdays_array.length; j++){
                    if(j<= updated_data_storage.end_date_index || j >= updated_data_storage.start_date_index){
                        $('input[id="'+weekdays_array[j]+'"]').parent().removeClass("disabled-div");
                    }
                }
            }else{
                //same date
                $('input[id="'+weekdays_array[updated_data_storage.start_date_index]+'"]').parent().removeClass("disabled-div");
            }
        }else{
            $(".mo-week-days").parent().removeClass("disabled-div");
        }
    }
}

/** Select only one Checkbox**/

$(".mo-auction-session").click(function(e){

    $(".mo-auction-session").not(this).prop('checked', false);
    if ($('input[name="ad_hoc"]').is(':checked')) {
        $('.mo-ad-hoc').removeClass('hidden');
        $('.mo-recurring').addClass('hidden');
    } else {
        $('.mo-ad-hoc').addClass('hidden');
        // $('.mo-recurring').removeClass('hidden');
    }

    if ($('input[name="recurring"]').is(':checked')) {
        $('.mo-recurring').removeClass('hidden');
    } else {
        $('.mo-recurring').addClass('hidden');
    }

});

/** Check box for Recurring Duration **/
$(".mo-recurring-duration").click(function(){

    $(".mo-recurring-duration").not(this).prop('checked', false);
    if ($('input[name="checkbox_recurring_custom"]').is(":checked")) {
        $("#input-recurring-custom").removeAttr('disabled');
        $("#input-recurring-default").attr('disabled', true);
    } else {
        $("#input-recurring-custom").attr('disabled', true);
    }

    if ($('input[name="checkbox_recurring_default"]').is(":checked")) {
        $("#input-recurring-custom").attr('disabled', true);
        $("#input-recurring-default").removeAttr('disabled');
    } else {
        $("#input-recurring-default").attr('disabled', true);
    }
});

/** Check box for Recurring Bid Increment **/
$(".mo-recurring-bid-increment").click(function(){

    $(".mo-recurring-bid-increment").not(this).prop('checked', false);
    if ($('input[name="checkbox_recurring_bid_custom"]').is(":checked")) {
        $("#input-recurring-bid-custom").removeAttr('disabled');
        $("#input-recurring-bid-default").attr('disabled', true);
    } else {
        $("#input-recurring-bid-custom").attr('disabled', true);
    }

    if ($('input[name="checkbox_recurring_bid_default"]').is(":checked")) {
        $("#input-recurring-bid-custom").attr('disabled', true);
        $("#input-recurring-bid-default").removeAttr('disabled');
    } else {
        $("#input-recurring-bid-default").attr('disabled', true);
    }
});

/** Check box for Ad-hoc Duration **/
$(".mo-ad-hoc-duration").click(function(){

    $(".mo-ad-hoc-duration").not(this).prop('checked', false);
    if ($('input[name="checkbox_ad_hoc_custom"]').is(":checked")) {
        $("#input-ad-hoc-custom").removeAttr('disabled');
        $("#input-ad-hoc-default").attr('disabled', true);
    } else {
        $("#input-ad-hoc-custom").attr('disabled', true);
    }

    if ($('input[name="checkbox_ad_hoc_default"]').is(":checked")) {
        // $("#input-ad-hoc-custom").removeAttr('disabled');
        $("#input-ad-hoc-default").removeAttr('disabled');
    } else {
        $("#input-ad-hoc-default").attr('disabled', true);
    }
});

/** Check box for Ad-hoc Bid Increment **/
$(".mo-ad-hoc-bid-increment").click(function(){

    $(".mo-ad-hoc-bid-increment").not(this).prop('checked', false);
    if ($('input[name="checkbox_ad_hoc_bid_custom"]').is(":checked")) {
        $("#input-ad-hoc-bid-custom").removeAttr('disabled');
        $("#input-ad-hoc-bid-default").attr('disabled', true);
    } else {
        $("#input-ad-hoc-bid-custom").attr('disabled', true);
    }

    if ($('input[name="checkbox_ad_hoc_bid_default"]').is(":checked")) {
        $("#input-ad-hoc-bid-custom").attr('disabled', true);
        $("#input-ad-hoc-bid-default").removeAttr('disabled');
    } else {
        $("#input-ad-hoc-bid-default").attr('disabled', true);
    }
});

/** Time Picker **/
$('#start-time').timepicker({
    'timeFormat': 'H:i',
    'autoclose': true
});
$('.glyphicon-time').click(function () {
    $('#start-time').focus();
});

// $("#ad-hoc-time").timepicker({
//     'timeFormat': 'H:i'
// });
$('.glyphicon-time').click(function () {
    $('#ad-hoc-time').focus();
});


/** Set Validate For Recurring Form **/

$("#mo-auction-recurring-form").validate({
    rules: {
        start_date: "required",
        start_time: "required",
        end_date: {
            "required": true,
            greaterThan: "#start-from-date"
        },
        duration_default: "required",
        duration_custom: "required",
        bid_increment_custom: "required"

    },
    errorElement: 'div',
    errorClass: 'mo-error-wrapper',
    errorPlacement: function(error, element){
        if(element.attr('name') == 'start_date'){
            error.appendTo("#errorToShow_start_date");
        }else if(element.attr('name') == 'end_date'){
            error.appendTo("#errorToShow_end_date");
        }else if(element.attr('name') == 'start_time'){
            error.appendTo("#errorToShow_start_time");
        }else{
            error.insertAfter(element);
        }
    },
    messages: {
        start_date: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
        start_time: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
        end_date:{
            "required": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
            greaterThan: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field greater than start from</span>"+"</div>"
        },
        duration_default: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
        duration_custom: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
        bid_increment_custom: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>"
    }
});
jQuery.validator.addMethod("greaterThan",
    function(value, element, params) {

        if (!/Invalid|NaN/.test(new Date(value))) {
            return new Date(value) > new Date($(params).val());
        }

        return isNaN(value) && isNaN($(params).val())
            || (Number(value) > Number($(params).val()));
    },'Must be greater than {0}.');

/** Set Validate For Ad-Hoc Form **/
$("#mo-auction-ad-hoc-form").validate({
    rules: {
        start_date: "required",
        start_time: "required",
        duration_default: "required",
        duration_custom: "required",
        bid_increment_custom: "required"
    },
    errorElement: 'div',
    errorClass: 'mo-error-wrapper',
    errorPlacement: function(error, element){
        if(element.attr('name') == 'start_date'){
            error.appendTo("#errorToShow_date");
        }else if(element.attr('name') == 'start_time'){
            error.appendTo("#errorToShow_time");
        }else{
            error.insertAfter(element);
        }
    },
    messages: {

        start_date: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
        start_time: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
        duration_default: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
        duration_custom: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>",
        bid_increment_custom: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>"
    }
});


// DISABLE SELECTED TIME WHEN CHOOSE DATE ON CREATE FORM
$('#ad-hoc-date').change(function(){
    console.log("access success");
    $.ajax({
        type: "POST",
        url: "/trading/get-auction-times",
        data: {
            _token: $("input[name='_token']").val(),
            session_date: $(this).val()
        },
        success: function(data) {
            if(data){

                time_data = JSON.parse(data);
                console.log(time_data, "data");
                var arr_time_data = Object.keys(time_data).map(function (key) { return time_data[key]; });
                var x = 30; //minutes interval
                var times = []; // time array
                var tt = 0; // start time

                //loop to increment the time and push results in array
                for (var i=0;tt<24*60; i++) {
                    var hh = Math.floor(tt/60); // getting hours of day in 0-24 format
                    var mm = (tt%60); // getting minutes of the hour in 0-55 format
                    times[i] = ("0" + (hh % 24)).slice(-2) + ':' + ("0" + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
                    tt = tt + x;
                }

                $("#start-time-select").empty();
                var disabled_option = false;
                $.each(times, function (index, value) {
                    for (var i=0; i<arr_time_data.length; i++){
                        if(arr_time_data[i] == value){
                            disabled_option = true;
                            break;
                        }else{
                            disabled_option = false;
                        }
                    }
                    $("#start-time-select").append($("<option>", {
                        value: value,
                        text: value,
                        disabled: disabled_option
                    }));
                });

            }
            else{
                console.log("data empty");
                var x = 30; //minutes interval
                var times = []; // time array
                var tt = 0; // start time

                // loop to increment the time and push results in array
                for (var i=0;tt<24*60; i++) {
                    var hh = Math.floor(tt/60); // getting hours of day in 0-24 format
                    var mm = (tt%60); // getting minutes of the hour in 0-55 format
                    times[i] = ("0" + (hh % 24)).slice(-2) + ':' + ("0" + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
                    tt = tt + x;
                }
                $("#start-time-select").empty();
                $.each(times, function(index, value){
                    $("#start-time-select").append($("<option>", {
                        value: value,
                        text: value
                    }));
                });
            }

        },
        complete: function(){
        }
    });
});




/** SUBMIT FORM **/
$("#auction-create").click(function(e){
    var token = $("input[name='_token']").val();
    var startdate = $("#start_from_date").val();
    var enddate = $("#end_at_date").val();
    if(enddate <= startdate){
        $("#errorToShow_end_date").html("This field not less than start from");
    }

    if($("input[name='recurring']").is(":checked")){
        console.log("recurring success");
        $("#mo-auction-recurring-form").submit();
    }else{
        if($("input[name='ad_hoc']").is(":checked")){
            console.log("ad hoc success");
            $("#mo-auction-ad-hoc-form").submit();
        }
    }

});

$("#mo-auction-recurring-form").submit(function(e){
    var token = $('input[name="_token"]').val();
    $.ajax({
        _token: token,
        type: "POST",
        url: "/mo-admin/trading/create-auction-session",
        data: $("#mo-auction-recurring-form").serialize(),
        success: function(data){
            if(data == 1){
                console.log("success");
                window.location.replace("/mo-admin/trading/auction-session-list");
            }
            else {
                if(data == "auction_session_duplicate"){
                    $(".alert-danger").find(".mo-error-message").html("auction session duplicate");
                    $(".alert-danger").removeClass("hidden");
                }else{
                    $(".alert-danger").find(".mo-error-message").html("time in past");
                    $(".alert-danger").removeClass("hidden");
                }
                // $(".alert-danger").removeClass("hidden");
                // $(".alert-danger").find(".mo-error-message").text(data);
                // $(".alert-danger").removeClass("hidden");
            }
        },
        error: function(data){
            console.log("error");
        }

    });
    e.preventDefault();
});


$("#mo-auction-ad-hoc-form").submit(function(e){
    console.log("access");
    var token = $('input[name="_token"]').val();
    $.ajax({
        _token: token,
        type: "POST",
        url: "/mo-admin/trading/create-auction-session",
        data: $("#mo-auction-ad-hoc-form").serialize(),
        success: function(data){
            if(data == 1){
                console.log("success");
                window.location.replace("/mo-admin/trading/auction-session-list");
            }
            else {
                if(data == "auction_session_duplicate"){
                    $(".alert-danger").find(".mo-error-message").html("auction session duplicate");
                    $(".alert-danger").removeClass("hidden");
                }else{
                    $(".alert-danger").find(".mo-error-message").html("time in past");
                    $(".alert-danger").removeClass("hidden");
                }
                // $(".alert-danger").removeClass("hidden");
                // $(".alert-danger").find(".mo-error-message").text(data);
                // $(".alert-danger").removeClass("hidden");
            }
        },
        error: function(data){
            console.log("error");
        }

    });
    e.preventDefault();
});

