/** SHOW SORT MENU **/
$(".mo-sort-live-auction-list").show();

/** SET LOCAL STORAGE TO APPLY FILTER **/
var data = {
    content_id: "#mo-request-list-content",
    url: "/trading/live-auction-offset"
};
localStorage.setItem("elem_to_filter", JSON.stringify(data));

/** QUICK BID & MAX BID POPUP **/
liveAuctionPopup();

//live auction get time remaining from server when window is focus;
$(window).focus(function() {
    $.ajax({
        type: "GET",
        url: "/mo-admin/trading/get-live-auction-timer",
        success: function(res) {

            if (res != null && res != "") {
                seconds = res;
            } else {
                seconds = 1;
                $(window).unbind();
            }
            
        },
        complete: function(){
            
        },
        global: false
    });

});

/** LIVE AUCTION TIMER **/
var next_auction_session_interval = null;
$.countDownNextAuctionSessionTime = function() {
    var next_auction_seconds = $("input[name='up_next_auction_session_seconds']").val();
    if (next_auction_seconds > 0) {
        next_auction_session_interval = setInterval(function(){
            next_auction_seconds -= 1;
            console.log("next_auction_seconds: "+next_auction_seconds);
            if (next_auction_seconds > 0) {
                //convert seconds to H:m:s
                var date = new Date(null);
                date.setSeconds(next_auction_seconds); // specify value for SECONDS here
                //var result = date.toISOString().substr(11, 8);

                // extract hours
                var next_auction_hours = Math.floor(next_auction_seconds / (60 * 60));
                if(next_auction_hours.toString().length == 1) {
                    next_auction_hours = ('0' + next_auction_hours).slice(-2);
                }

                // extract minutes
                var divisor_for_minutes = next_auction_seconds % (60 * 60);
                var next_auction_minutes = Math.floor(divisor_for_minutes / 60);
                if (next_auction_minutes.toString().length == 1) {
                    next_auction_minutes = ('0' + next_auction_minutes).slice(-2);
                }

                // extract the remaining seconds
                divisor_for_seconds = divisor_for_minutes % 60;
                var seconds_remaining = Math.ceil(divisor_for_seconds);
                if(seconds_remaining.toString().length == 1) {
                    seconds_remaining = ('0' + seconds_remaining).slice(-2);
                }

                //$(".remaining-time-label").text(remaining_minutes +":"+ remaining_seconds);
                $("#next-auction-session-timer").text(next_auction_hours+":"+next_auction_minutes+":"+seconds_remaining);

            } else if (next_auction_seconds == 0) {
                $(".mo-loading-image").removeClass("hidden");
                setInterval(function(){
                    window.location.reload();
                }, 5000);
            }
        }, 1000);
    }
}

$.countDownNextAuctionSessionTime();

var seconds = 0;
$.countDownLiveAuctionSessionTime = function() {
    seconds = $("input[name='live_auction_seconds_timer']").val();
    if (seconds > 0) {
        clearInterval(next_auction_session_interval);
        setInterval(function(){
            seconds -= 1;
            console.log("second: "+seconds);
            if (seconds > 0) {
                //convert seconds to H:m:s
                var date = new Date(null);
                date.setSeconds(seconds); // specify value for SECONDS here
                var result = date.toISOString().substr(11, 8);
                //$(".remaining-time-label").text(remaining_minutes +":"+ remaining_seconds);
                $(".live-auction-timer").text(result);

            } else if (seconds == 0) {
                $(".mo-loading-image").removeClass("hidden");
                setInterval(function(){
                    window.location.reload();
                }, 5000);
            }
        }, 1000);
    }
}
$.countDownLiveAuctionSessionTime();
/** end LIVE AUCTION TIMER **/

var place_bid_price_global = 0;
var auction_session_id_global = 0;
var auction_item_id_global = 0;

$(".max-bid-text").click(function(){
    var place_bid_price = $(this).data("quick-bid");
    place_bid_price_global = place_bid_price;
    $("#live-auction-place-bid-number").text(accounting.formatMoney(place_bid_price, "", 2, ",", "."));

    var auction_session_id = $("input[name='auction_session_id']").val();
    auction_session_id_global = auction_session_id;
    var auction_item_id = $(this).data("auction-item-id");
    auction_item_id_global = auction_item_id;

    //get max bid price
    $.ajax({
        type: "POST",
        url: "/trading/live-auction/get-max-bid",
        data: {
            _token: token,
            session_id: auction_session_id_global,
            item_id: auction_item_id_global
        },
        success: function(data) {

            console.log("trading/live-auction/get-max-bid", data);
            if (data != null) {
                data = $.parseJSON(data);
                $("#live-auction-max-bid-input").val(data.max_price);
            }

        },
        complete: function(){
            $(".mo-loading-image").addClass("hidden");
        }
    });
});

$(".quick-bid-text").click(function(){
    var place_bid_price = $(this).data("quick-bid");
    place_bid_price_global = place_bid_price;
    $("#live-auction-place-bid-number").text(accounting.formatMoney(place_bid_price, "", 2, ",", "."));

    var auction_session_id = $("input[name='auction_session_id']").val();
    auction_session_id_global = auction_session_id;
    var auction_item_id = $(this).data("auction-item-id");
    auction_item_id_global = auction_item_id;
});

$("#mo-auction-max-bid-form").validate({
    rules: {
        "live-auction-max-bid-input": "required"
    },
    errorElement: 'div',
    errorClass: 'mo-error-wrapper',
    errorPlacement: function(error, element){
        error.insertAfter(element);
    },
    messages: {
        "live-auction-max-bid-input" : "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>"
    }
});

$("#live-auction-set-max-bid-btn").click(function(){
    if(!$("#mo-auction-max-bid-form").valid()) {
        return false;
    }
    var max_bid_price = $("#live-auction-max-bid-input").val();
    //trading/save-max-value
    $.ajax({
        type: "POST",
        url: "/trading/save-max-value",
        data: {
            _token: token,
            session_id: auction_session_id_global,
            auction_id: auction_item_id_global,
            max_price: max_bid_price
        },
        success: function(data) {

            console.log("/trading/save-max-value", data);
            if (data != null) {
                data = $.parseJSON(data);
                if(data == 1) {
                    magnificPopup.close();
                } else {
                    //error msg show here
                    magnificPopup.close();
                }
            }

        },
        complete: function(){
            $(".mo-loading-image").addClass("hidden");
        }
    });

});

var magnificPopup = $.magnificPopup.instance
$("#live-auction-quick-bid-btn").click(function(){
    var token = $("input[name='_token']").val();
    //ajax for quick bid here
    $(document).ajaxStart(function() {
        $(".mo-loading-image").removeClass("hidden");
    });
    $.ajax({
        type: "POST",
        url: "/trading/live-auction/quick-bid",
        data: {
            _token: token,
            session_id: auction_session_id_global,
            item_id: auction_item_id_global,
            bid_price: place_bid_price_global
        },
        success: function(data) {

            console.log("/trading/live-auction/quick-bid", data);
            if (data != null) {
                data = $.parseJSON(data);
                if (data.bid_price) {
                    magnificPopup.close();
                } else if (data.error.code == "invalid_bid_step") {

                    //error msg show here
                    $(".live-auction-quick-bid-desc").addClass("hidden");
                    $(".invalid-bid-step").removeClass("hidden");
                    setInterval(function(){
                        window.location.reload();
                    }, 3000);

                } else if (data.error.code == "too_low") {

                    $(".live-auction-quick-bid-desc").addClass("hidden");
                    $(".too_low").removeClass("hidden");
                    var too_low_interval = setInterval(function(){
                        magnificPopupOnCloseFunction = function () {
                            //restore live auction bid message description
                            $(".live-auction-quick-bid-desc").removeClass("hidden");
                            $(".too_low").addClass("hidden");
                            clearInterval(too_low_interval);
                            magnificPopupOnCloseFunction = null;
                        };
                        console.log("too_low_interval");
                        magnificPopup.close();
                    }, 3000);

                }
            }
        },
        complete: function(){
            $(".mo-loading-image").addClass("hidden");
        }
    });
});

/*function infinityScroll(content, url) {
    var win = $(window);
    win.off('scroll.windowscroll');

    if (typeof(Storage) !== "undefined") {
        localStorage.setItem("live_auction_list_content_length", 0);
    }

    // Each time the user scrolls
    win.on('scroll.windowscroll', function() {

        // End of the document reached?
        if ($(document).height() - win.height() == win.scrollTop()) {
            content.children().find(".mo-loading-icon").parent().not(":last").remove();

            if(content.children().length > localStorage.getItem("live_auction_list_content_length")) {
                localStorage.setItem("live_auction_list_content_length", content.children().length - 1);
                var token = $("input[name='_token']").val();

                $(document).on("ajaxStart.scroll", function () {
                    $('.mo-loading-icon').show();
                });

                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        _token: token,
                        limit: 20,
                        offset: content.children().length,
                        "mo-live-auction-search": $("input[name='mo-live-auction-search']").val()
                    },
                    success: function(html) {

                        if(html == "") {
                            win.off('scroll.windowscroll');
                        } else {
                            content.append(html);
                        }

                    },
                    complete: function(){
                    }
                });

                $(document).on("ajaxStop.scroll", function () {
                    $('.mo-loading-icon').hide();
                });
            }
        }

    });
}*/
//disable infinityScroll use it by infinityScroll of filter
//infinityScroll($("#mo-live-auction-content"), "/trading/live-auction-offset");

/** INFINITE SCROLL **/
infinityScroll("");