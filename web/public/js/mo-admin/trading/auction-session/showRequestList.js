/** SHOW SORT MENU **/
$(".mo-sort-auction-list").show();
$(".mo-sort-live-auction-list").hide();

/** SET LOCAL STORAGE TO APPLY FILTER **/
var data = {
    content_id: "#mo-request-list-content",
    url: "/mo-admin/trading/request-list-offset"
};
localStorage.setItem("elem_to_filter", JSON.stringify(data));

/*function infinityScroll(content, url) {
    var win = $(window);
    win.off('scroll.windowscroll');

    if (typeof(Storage) !== "undefined") {
        localStorage.setItem("request_list_content_length", 0);
    }

    // Each time the user scrolls
    win.on('scroll.windowscroll', function() {

        // End of the document reached?
        if ($(document).height() - win.height() == win.scrollTop()) {
            content.children().find(".mo-loading-icon").parent().not(":last").remove();

            if(content.children().length > localStorage.getItem("request_list_content_length")) {
                localStorage.setItem("request_list_content_length", content.children().length - 1);
                var token = $("input[name='_token']").val();

                $(document).on("ajaxStart.scroll", function () {
                    $('.mo-loading-icon').show();
                });

                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        _token: token,
                        limit: 20,
                        offset: content.children().length,
                        query: $("input[name='mo-request-list-search-input']").val()
                    },
                    success: function(html) {

                        if(html == ""){
                            win.off('scroll.windowscroll');
                        } else {
                            content.append(html);
                        }

                    },
                    complete: function(){
                    }
                });

                $(document).on("ajaxStop.scroll", function () {
                    $('.mo-loading-icon').hide();
                });
            }
        }

    });
}

infinityScroll($("#mo-request-list-content"), "/mo-admin/trading/request-list-offset");*/

/*var magnificPopup = $.magnificPopup.instance;
$(".mo-popup").magnificPopup({
    type:'inline',
    showCloseBtn: true,
    closeOnBgClick: false
});

$('.mo-cancel-btn').click(function () {
    magnificPopup.close();
});*/

/** APPROVE REQUEST - POPUP **/
approveRequestPopup();

/** REJECT REQUEST - POPUP **/
rejectRequestPopup();

var request_id_global = 0;
$(document).on("click", ".request-list-approve-link", function () {
    request_id_global = $(this).attr("data-request-id");
    $(".pagination").data("request-id", request_id_global);
    //auto check auction session
    var session_id = $(this).data("session-id");
    $(".pagination").data("session-id", session_id);
    $("input[name='request-list-select-auction-session']").each(function(index, value) {
        if ($(value).val() == session_id) {
            $(value).prop("checked", true);
            return;
        }
    });
});

$(document).on("click", ".request-list-reject-link", function () {
    request_id_global = $(this).attr("data-request-id");
});

//approve request to auction session
$(document).on("click", "#mo-request-list-select-auction-session-btn", function () {
    var request_id = request_id_global;
    var auction_session_id = $("input[name='request-list-select-auction-session']:checked").val();
    var token = $("input[name='_token']").val();

    $(document).ajaxStart(function() {
        $(".mo-loading-image").removeClass("hidden");
    });

    $.ajax({
        type: "POST",
        url: "/mo-admin/trading/approve-request-list-offset",
        data: {
            request_id: request_id,
            auction_session_id: auction_session_id,
            _token: token
        },
        success: function(html) {
            console.log("/mo-admin/trading/approve-request-list-offset success", html);
            if (html != null) {
                var data = $.parseJSON(html);
                if(data.success) {
                    window.location.reload();
                } else {
                    window.location.reload();
                }
            }
        },
        error: function(error) {
            console.log("/mo-admin/trading/approve-request-list-offset error", error);
        },
        complete: function() {
            $(".mo-loading-image").addClass("hidden");
        }
    });
});

//reject request to auction session
//validate comment here
$("#mo-request-list-reject-form").validate({
    rules: {
        "request-list-reject-comment": "required"
    },
    errorElement: 'div',
    errorClass: 'mo-error-wrapper',
    errorPlacement: function(error, element){
        error.insertAfter(element);
    },
    messages: {
        "request-list-reject-comment": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty</span>"+"</div>"
    },
    submitHandler: function(form){
        //form.submit();
    }
});

$(document).on("click", "#mo-request-list-reject-btn", function () {
    if(!$("#mo-request-list-reject-form").valid()) {
        return false;
    }
    var request_id = request_id_global;
    var token = $("input[name='_token']").val();
    var comment = $("#request-list-reject-comment").val();
    var reason = $("#request-list-reject-reason").val();

    $(document).ajaxStart(function() {
        $(".mo-loading-image").removeClass("hidden");
    });

    $.ajax({
        type: "POST",
        url: "/mo-admin/trading/reject-request-list-offset",
        data: {
            request_id: request_id,
            _token: token,
            comment: comment,
            reason: reason
        },
        success: function(html) {
            console.log("/mo-admin/trading/reject-request-list-offset success", html);
            if (html != null) {
                var data = $.parseJSON(html);
                if(data.success) {
                    window.location.reload();
                } else {
                    window.location.reload();
                }
            }
        },
        error: function(error) {
            console.log("/mo-admin/trading/approve-request-list-offset error", error);
        },
        complete: function() {
            $(".mo-loading-image").addClass("hidden");
        }
    });

});

//$("input[name='mo-request-list-search-input']").bind("enterKey", function(){
//    $("#request-list-search-form").submit();
//});

/*$(".mo-request-list-search").click(function(){
    $("#request-list-search-form").submit();
});*/

/** INFINITE SCROLL **/
infinityScroll("");