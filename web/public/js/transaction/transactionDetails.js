$(document).ready(function(){
    var token = $("input[name='_token']").val();/** COMMON TOKEN **/
    var transaction_id = $("#transaction-id").val();

    /** PRE-DEFINED AJAX FOR ACTION BUTTON **/
    function callAjax(url){
        /*console.log("transaction_id "+transaction_id);
        console.log("transaction_rid "+transaction_rid);
        console.log("url "+url);*/
        var type = $("#type").val();
        $(document).on("ajaxStart.buttonAction", function () {
            $(".mo-loading-image").removeClass("hidden");
        });

        $.ajax({
            type: 'POST',
            data: {
                _token: token,
                transaction_id: transaction_id
            },
            url: url,
            success: function (data) {
                console.log(data);
                if(data == 1){
                    window.location.href = "/transaction/view-transactions/"+type;
                }else{
                    $(".alert-danger").find(".mo-error-message").html(data);
                     $(".alert-danger").removeClass("hidden");
                }
            },
            error: function (data) {
                console.log('error: ' + data);
            },
            complete: function () {
                $(".mo-loading-image").addClass("hidden");
                var magnificPopup = $.magnificPopup.instance;
                magnificPopup.close();
            }
        });
    }

    /** HANDLE BUTTON ACTION **/
    $(document).on("click", "#transaction-btn", function () {
        console.log("clicked");
        var transaction_status = $("#transaction-status").val();
        var url = "";

        if(transaction_status == 20){
            url = "/transaction/transport-ready";
            callAjax(url);
        }else if(transaction_status == 25){
            url = "/transaction/inspect";
            callAjax(url);
            window.location.href = "/arriving-vehicle/inspect/"+transaction_id;
        }else if(transaction_status == 8){
            window.location.href = "/arriving-vehicle/inspect/"+transaction_id;
        }
    });

    /** HANDLE UPLOAD DOCUMENTS **/
    $(".mo-upload-download-button").each(function(i, obj) {
        if($(obj).val().toLowerCase() == "upload"){
            $(obj).addClass("disabled-div");
        }
    });

    $(".inputfile").change(function () {
        console.log("change");
        console.log("change "+$(this).get(0).files.length);
        if ($(this).get(0).files.length != 0) {
            $(this).parent().prev().removeClass("disabled-div");
        }
    });

    function addUploadingText(input_id, file_name){
        if(file_name){
            $(input_id).next().children("span.choose-a-file").html( file_name );
        }else{
            $(input_id).next().children("span.choose-a-file").html( "Uploading "+"<img class='height-20' src='/images/main/three-dot-loading.gif'>" );
        }
    }

    function addExtensionValue(input_id){
        var extension = $(input_id).val().split('.').pop();
        $("#file-extension").val(extension);
    }

    $(".mo-upload-download-button").click(function () {
        var document_code = $(this).data("document-code");
        $("#document-code").val(document_code);
        switch(document_code){
            case "transfer":
                addExtensionValue("#vehicle-transfer-file");
                break;
            case "transfer_additional":
                addExtensionValue("#vehicle-transfer-file-additional");
                break;
            case "dispute":
                addExtensionValue("#invoice-dispute-cost-file");
                break;
            case "ppsr_financial":
                addExtensionValue("#ppsr-finance-payout-file");
                break;
        }

        $(this).addClass("disabled-div");

        $("#mo-upload-download-documents-form").submit();
    });

    $("#mo-upload-download-documents-form").on("submit",(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        console.log('now submit form');

        $(document).on("ajaxStart.uploadDocument", function () {
            switch($("#document-code").val()){
                case "transfer":
                    addUploadingText("#vehicle-transfer-file");
                    break;
                case "transfer_additional":
                    addUploadingText("#vehicle-transfer-file-additional");
                    break;
                case "dispute":
                    addUploadingText("#invoice-dispute-cost-file");
                    break;
                case "ppsr_financial":
                    addUploadingText("#ppsr-finance-payout-file");
                    break;
            }
        });

        $.ajax({
            type:'POST',
            url: "/transaction/upload-document/"+$("#transaction-id").val()+"/"+$("#document-code").val(),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data) {
                if(data != null && data != "error"){
                    console.log(data);
                    var filename = data.split('/').pop();
                    switch($("#document-code").val()){
                        case "transfer":
                            addUploadingText("#vehicle-transfer-file", filename);
                            break;
                        case "transfer_additional":
                            addUploadingText("#vehicle-transfer-file-additional", filename);
                            break;
                        case "dispute":
                            addUploadingText("#invoice-dispute-cost-file", filename);
                            break;
                        case "ppsr_financial":
                            addUploadingText("#ppsr-finance-payout-file", filename);
                            break;
                    }

                }else if(data == "error") {
                    console.log("error");
                    /*$(".alert-danger").find(".mo-error-message").text("your uploading file is not a image. Please try again!");
                    $(".alert-danger").removeClass("hidden");*/
                }else{
                    console.log("null");
                    /*$(".alert-danger").find(".mo-error-message").text("there is some error happens when uploading image. Please try again!");
                    $(".alert-danger").removeClass("hidden");*/
                }
            },
            error: function(data){
                console.log("error");
                console.log(data);
            }
            ,
            complete: function () {
                /*switch($("#document-code").val()){
                    case "transfer":
                        $(".mo-upload-download-button[data-document-code=transfer]").removeClass("disabled-div");
                        break;
                    case "transfer_additional":
                        $(".mo-upload-download-button[data-document-code=transfer_additional]").removeClass("disabled-div");
                        break;
                    case "dispute":
                        $(".mo-upload-download-button[data-document-code=dispute]").removeClass("disabled-div");
                        break;
                    case "ppsr_financial":
                        $(".mo-upload-download-button[data-document-code=ppsr_financial]").removeClass("disabled-div");
                        break;
                }*/
            }
        });
    }));

    /** HANDLE DOWNLOAD DOCUMENTS **/
    $(".mo-upload-download-button").each(function(i, obj){
        if($(obj).val().toLowerCase() == "download"){
            $(obj).next().addClass("hidden");
            var filename = $(this).parent().find(".choose-a-file").text();
            var href_default = "https://motest2.blob.core.windows.net/transaction-document/";
            // var href_default = "https://"+ env('AZURE_STORAGE_ACCOUNT_NAME') + "blob.core.windows.net/transaction-document/";
            if(filename == "Choose a file..." || filename == ""){
                $(obj).addClass("disabled-div");
            }
            /** Download document **/
            $(this).click(function(e){
                e.preventDefault();
                // alert(filename);
                window.location.href = href_default + filename;
                $(this).removeClass("disabled-div");
            });
        }
    });

});