$(document).ready(function(){
    var hash = window.location.hash,
        tabName = '';

    if (hash) {
        tabName = hash.slice(1, hash.length);
    }

    var token = $("input[name='_token']").val();/** COMMON TOKEN **/

    function callAjax(transaction_id, url){
        console.log("transaction_id "+transaction_id);
        console.log("url "+url);
        $(document).on("ajaxStart.buttonAction", function () {
            $(".mo-loading-image").removeClass("hidden");
        });

        $.ajax({
            type: 'POST',
            data: {
                _token: token,
                transaction_id: transaction_id
            },
            url: url,
            success: function (data) {
                console.log(data);
                if(data == 1){
                    if(sessionStorage.getItem("dealership_elem_to_paging")) {
                        var data_obj = JSON.parse(sessionStorage.getItem("dealership_elem_to_paging"));
                    }
                    active = null;
                    if ($("#required-action").is(":checked")) {
                        active = true;
                    }
                    $.getTransactionsList(true, init, query, offset, data_obj.request_url, data_obj.table_id, active);
                }else{
                    /*$(".alert-danger").find(".mo-error-message").html(data);
                     $(".alert-danger").removeClass("hidden");*/
                }
            },
            error: function (data) {
                console.log('error: ' + data);
            },
            complete: function () {
                $(".mo-loading-image").addClass("hidden");
            }
        });
    }

    var id = "#mo-table-transactions-list";
    var destroy = false;
    var init = true;
    var query = null;
    var offset = 0;
    var active = null;
    var type = $("#type").val();

    if(type == "complete"){
        /** disabled transaction status filter **/
        $("#mo-transaction-statuses-filter").addClass("disabled-div");
    }

    /** DATA TABLE **/
    $.getTransactionsList = function(destroy, init, query, offset, request_url, id, active, ajax_data, never) {
        var destroy = destroy;
        var columns = [
            {className: "never"},//0. ID - hidden
            {className: "all text-uppercase "+never, "bSortable": false, "width":"5%"},//1. bought - sold
            {className: "all", "width":"10%"},//2.trans ref#
            {className: "all", "bSortable": false, "width":"35%"},//3. vehicle name
            {className: "all", "bSortable": false, "width":"10%"},//4. rego
            {className: "all", "bSortable": false, "width":"10%"},//5. location
            {className: "all", "bSortable": false, "width":"10%"},//6. status_value
            {className: "never"},//7. status
            {
                className: "all group-btn", "bSortable": false,
                "mRender": function (data, type, row, meta) {
                    var menu_right = '<a href="/transaction/transaction-detail/'+$("#type").val()+'/'+row[0]+'"><span class="glyphicon glyphicon-menu-right float-right"></span></a>';
                    var btn = '';
                    var group_btn = '';
                    if(row[1].toLowerCase()=="out" && row[7]==20){//seller & payment received
                        btn = '<input type="button" class="font-family-bold mo-transaction-btn mo-btn-yellow text-uppercase" value="vehicle ready" data-status="'+row[7]+'" data-transaction-id="'+row[0]+'">';
                    }
                    if(row[1].toLowerCase()=="in" && row[7]==25){//buyer & shipped(arrived)
                        btn = '<input type="button" class="font-family-bold mo-transaction-btn mo-btn-yellow text-uppercase" value="inspect" data-status="'+row[7]+'" data-transaction-id="'+row[0]+'">';
                    }
                    if(row[1].toLowerCase()=="in" && row[7]==8){//buyer & shipped(arrived)
                        btn = '<input type="button" class="font-family-bold mo-transaction-btn mo-btn-yellow text-uppercase" value="continue inspection" data-status="'+row[7]+'" data-transaction-id="'+row[0]+'">';
                    }
                    group_btn = menu_right + btn;
                    return group_btn;
                }
            }
        ];
        //var request_url = "/transaction/get-transactions";
        var query = query;
        var offset = offset;
        var init = init;
        var error_msg = "<div>There are no transaction to display at this time.</div>";
        var scrollX = true;
        var empty_msg = "";
        $.moDataTable(id, destroy, columns, request_url, query, offset, init, error_msg, scrollX, empty_msg, active, ajax_data);
    };

    /** BLOCK - CLICK **/
    $('#mo-blocks-wrapper a').click(function () {
        /** clear filter when switching block **/
        resetFilterHandle();

        var tab = $(this).attr('tab');

        $('#mo-blocks-wrapper .mo-block').removeClass('mo-block-active');
        $('.content-block').hide();/** table content block **/

        $(this).find('.mo-block').addClass('mo-block-active');
        $('#' + tab + '-block').show();

        console.log("tab "+tab);

        var request_url = "/transaction/get-transactions/"+type+"/"+tab;
        console.log("request_url "+request_url);
        var table_id = id + "-"+tab;
        destroy = true;

        if(tab == "all"){
            $.getTransactionsList(destroy, init, query, offset, request_url, table_id, active, null, '');
        }else{
            $.getTransactionsList(destroy, init, query, offset, request_url, table_id, active, null, 'never');
        }

        /** SET SESSION STORAGE TO PAGING **/
        var data = {
            table_id: table_id,
            request_url: request_url
        };
        sessionStorage.setItem("dealership_elem_to_paging", JSON.stringify(data));

        $('#required-action').attr('checked', false);
    });

    /** SET 1ST ACTIVE BLOCK - OR BY HASH TAG **/
    if (tabName) {
        $('a[tab="' + tabName + '"]').click();
    } else {
        $('#mo-blocks-wrapper a').first().click();
    }

    /** HANDLE BUTTON ACTION **/
    $(document).on("click", ".mo-transaction-btn", function () {
        console.log("clicked");
        var transaction_status = $(this).data("status");
        var transaction_id = $(this).data("transaction-id");
        var url = "";
        if(transaction_status == 20){
            url = "/transaction/transport-ready";
            callAjax(transaction_id, url);
        }else if(transaction_status == 25){
            url = "/transaction/inspect";
            callAjax(transaction_id, url);
            window.location.href = "/arriving-vehicle/inspect/"+transaction_id;
        }else if(transaction_status == 8){
            window.location.href = "/arriving-vehicle/inspect/"+transaction_id;
        }
    });
});