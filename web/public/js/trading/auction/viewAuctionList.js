var hash = window.location.hash;

/** SHOW SORT MENU **/
$(".mo-sort-live-auction-list").hide();
$(".mo-sort-auction-list").show();

var tabName = '';

if (hash) {
    tabName = hash.slice(1, hash.length);
}
$('#mo-blocks-wrapper a').click(function () {

    var token = $("input[name='_token']").val();
    var tab = $(this).attr('tab');
    $('#mo-blocks-wrapper .mo-block').removeClass('mo-block-active');
    $('.data-block').hide();

    $(this).find('.mo-block').addClass('mo-block-active');
    $('#' + tab + '-block').show();

    var content_id = '#' + tab + '-block';
    var url = '/trading/get-' + tab + '-item';
    var container = $(content_id);

    /** SET LOCAL STORAGE TO APPLY FILTER **/
    var data = {
        content_id: content_id,
        url: url
    };
    localStorage.setItem("elem_to_filter", JSON.stringify(data));

    container.html('');
    $.ajax({
        type: "POST",
        url: url,
        data: {
            _token: token,
            limit: 21,
            offset: 0
        },
        success: function (html) {
            container.html(html);
            infinityScroll("");
            preBidPopup();
            buyNowPopup();
        },
        complete: function () {
        }
    });

    /** CHANGE BREADCRUMB **/
    switch(tab){
        case "wish-list":
            $("span.yellow-text").text("Wish List");
            break;
        case "watching":
            $("span.yellow-text").text("Watching");
            break;
        case "recently-viewed":
            $("span.yellow-text").text("Recently Viewed");
            break;
        case "selling":
            $("span.yellow-text").text("Selling");
            break;
        default:
            $("span.yellow-text").text("Auction Listing");
            break;
    }
});

if (tabName) {
    $('a[tab="' + tabName + '"]').click();
} else {
    $('#mo-blocks-wrapper a').first().click();
}

/** INFINITE SCROLL **/
var content = $("#mo-auction-grid-wrapper #all-block");
var content_id = '#all-block';
var url = '/trading/get-all-item';

/** SET LOCAL STORAGE TO APPLY FILTER **/
var data = {
    content_id: content_id,
    url: url
};
localStorage.setItem("elem_to_filter", JSON.stringify(data));
infinityScroll("");

$(document).on('click', '.mo-watch-icon', function () {
    var _this = $(this);
    var auctionId = $(this).parents('.auction-item').attr('item');
    if (_this.find('.watch-light').length == 1) {
        $.ajax({
            type: "DELETE",
            url: '/trading/watching-list',
            data: {
                _token: token,
                auction_id: auctionId
            },
            success: function (result) {
                if (result) {
                    _this.find('.watch-light').removeClass('watch-light').addClass('watch-dim');
                }

                var pathname = window.location.pathname;
                var hash = window.location.hash;
                if (hash) {
                    var tab = hash.slice(1, hash.length);
                }

                if (tab == 'watching') {
                    _this.parents('.auction-item').remove();
                }
            },
            complete: function () {
            }
        });

    } else {
        $.ajax({
            type: "POST",
            url: '/trading/watching-list',
            data: {
                _token: token,
                auction_id: auctionId
            },
            success: function (result) {
                if (result) {
                    _this.find('.watch-dim').removeClass('watch-dim').addClass('watch-light');
                }
            },
            complete: function () {
            }
        });
    }
    return false;
});