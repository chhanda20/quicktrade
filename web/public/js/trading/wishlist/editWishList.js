
$(document).ready(function() {

	console.log("step 1: document ready");
	
	sessionStorage.setItem("wish_list_make_model_variant_data_checked", 0);
	console.log("step 2: clear local session storage");

	if($("input[name='wish_list_detail']").val() != null) {
		
		var wish_list_detail = $.parseJSON($("input[name='wish_list_detail']").val());
		console.log(wish_list_detail, "wish_list_detail");
		console.log("step 3: get wish_list_detail from JSON string", wish_list_detail);

		var edit_make_info = null;
		if (wish_list_detail.makeInfo != null) {
			edit_make_info = wish_list_detail.makeInfo;
			console.log("step 4: assign makeInfo to edit_make_info variable", edit_make_info);
		}
		console.log("step 5: edit_make_info.length", edit_make_info.length);
		//refill make info to select box
		if (edit_make_info.length >= 1) {

			console.log("step 6: make info", edit_make_info);
			
            /*
			$.each(edit_make_info, function(make_info_index, make_info_value) {
				//add make
				if (make_arr_for_select_box.children.length < 1) {
					make_arr_for_select_box.children[0].push({
						label: make_info_value.make,
						value: make_info_value.make
					})
				}
			});
			*/

			//step 1: check make
			var make_checked_for_multiselect = [];
			for (var i = edit_make_info.length - 1; i >= 0; i--) {
				make_checked_for_multiselect.push(edit_make_info[i].make);
			};

			//clear duplicate from make_checked_for_multiselect
			make_checked_for_multiselect = make_checked_for_multiselect.filter(function(item, pos) {
    			return make_checked_for_multiselect.indexOf(item) == pos;
			});

			var make_checked_for_multiselect_unique = [];
		    $.each(make_checked_for_multiselect, function(i, el){
		        if($.inArray(el, make_checked_for_multiselect_unique) === -1) make_checked_for_multiselect_unique.push(el);
		    });
			//push data checked to make (selected box) - push data list and data checked
			console.log(make_checked_for_multiselect, "make_checked_for_multiselect");
            $("#wishlist-make").multiselect('select', make_checked_for_multiselect);

			console.log("step 7: make is checked", make_checked_for_multiselect);

            //add make checked to session local storage
            sessionStorage.setItem("wish_list_make_model_variant_data_checked", 0);
            var wish_list_make_model_variant_data_checked = null;
            if (sessionStorage.getItem("wish_list_make_model_variant_data_checked") == 0) {
                wish_list_make_model_variant_data_checked = {"data_checked": []};
            } else {
                wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
                wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);
            }

            for (var i = make_checked_for_multiselect_unique.length - 1; i >= 0; i--) {
	            wish_list_make_model_variant_data_checked.data_checked.push({
	                "make_name": make_checked_for_multiselect_unique[i],
	                "model_items": []
	            });
        	}

            wish_list_make_model_variant_data_checked = JSON.stringify(wish_list_make_model_variant_data_checked);
            sessionStorage.setItem("wish_list_make_model_variant_data_checked", wish_list_make_model_variant_data_checked);
            console.log(wish_list_make_model_variant_data_checked, "after add make");

            //end step 1
			//end make multiselect box
			console.log("step 8: end make multiselect box");

			/**--break--**/

            //step 2 load model based on make checked
			console.log("step 9: begin model");

            $.each(make_checked_for_multiselect, function(index, value) {
            	var selected_make = value;
				console.log("step 10: load model based on makes are checked", selected_make);

            	$.ajax({
	                type: 'POST',
	                data: {_token: token, make: selected_make},
	                url: "/get-glasses-data/family",
	                success:function(model_data) {

	                    var model_data_arr = model_data.toString().split(',');
	                    model_data_arr.clean("");

	                    model_arr_for_select_box.push({
	                        label: selected_make,
	                        children: [
	                        ]
	                    });

	                    $.each(model_data_arr, function(key_model_data, value_model_data) {

	                        model_arr_for_select_box[model_arr_for_select_box.length-1].children.push(
	                            { label: value_model_data, value: selected_make+"_"+value_model_data }
	                        );

	                    });

	                    //push option data to model - push data list
	                    $("#wishlist-model").multiselect('dataprovider', model_arr_for_select_box);

	                    //step 3: check model
	                    //recheck model
	                    //$.reUpdateMultiselect("#wishlist-model");
						var model_checked_for_multiselect = [];
						for (var i = edit_make_info.length - 1; i >= 0; i--) {
							if (edit_make_info[i].model != null) {
								model_checked_for_multiselect.push(edit_make_info[i].make+"_"+edit_make_info[i].model);
							}
						};

						//clear duplicate
						model_checked_for_multiselect = model_checked_for_multiselect.filter(function(item, pos) {
    						return model_checked_for_multiselect.indexOf(item) == pos;
						});

						//prepare push data checked to model (set selected items for model multiselect box)
						console.log(model_checked_for_multiselect, "model_checked_for_multiselect");

			            $("#wishlist-model").multiselect('select', model_checked_for_multiselect);
						console.log("step 10.1: model checked", model_checked_for_multiselect);

			            //rebind event
			            /** MODEL SECTION **/
	                    //$("#wishlist-model").next().children().find("input[type='checkbox']").unbind();
	                    $("#wishlist-model").next().children().find("input[type='checkbox']").click(function(){
	                        
	                        /* model is checked (call event change) */
	                        if ($(this).is(":checked")) {
	                            selected_model = $(this).val().split("_");
	                        } 
	                        /* model is unchecked */
	                        else if (!$(this).is(":checked")) {
	                            /* when user uncheck model, it will work here */
	                            
	                            //get make
	                            make_of_model_selected = $(this).val().split("_")[0];

	                            var unselect_model = $(this).val();

	                            //remove model that uncheck in datachecked session local storage
	                            var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
	                            wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);
	                            console.log("uncheck-mode", wish_list_make_model_variant_data_checked);
	                            //begin remove
	                            $.each(wish_list_make_model_variant_data_checked.data_checked, function(index, value){
	                                console.log(value, "make item");

	                                /* make */
	                                if (value.make_name == make_of_model_selected) {

	                                    $.each(value.model_items, function(model_items_index, model_items_value){
	                                        /* model */
	                                        if(typeof(model_items_value) != "undefined"){

	                                            console.log(model_items_value, "model_items_value");
	                                            if (model_items_value.model_name.toString().replace(" ", "") == unselect_model.split("_")[1].toString().replace(" ", "")) {

	                                                wish_list_make_model_variant_data_checked.data_checked[index].model_items.splice(model_items_index, 1);
	                                                selected_model = false;
	                                            }
	                                        }

	                                    });
	                                }
	                            });
	                            //save remove
	                            console.log(wish_list_make_model_variant_data_checked, "wish_list_make_model_variant_data_checked after remove");
	                            
	                            wish_list_make_model_variant_data_checked = JSON.stringify(wish_list_make_model_variant_data_checked);
	                            sessionStorage.setItem("wish_list_make_model_variant_data_checked", wish_list_make_model_variant_data_checked);

	                            //console.log($.parseJSON(wish_list_make_model_variant_data_checked), "uncheck");
	                            selected_model = false;
	                            //remove variant here
	                            if(typeof (variant_arr_for_select_box) != "undefined" && variant_arr_for_select_box.length >= 1) {
	                                var unselect_model = $(this).val();
	                                $.each(variant_arr_for_select_box, function(variant_arr_for_select_box_index, variant_arr_for_select_box_value){

	                                    if(variant_arr_for_select_box_value.label == unselect_model.replace("_", " ")) {

	                                        delete variant_arr_for_select_box[variant_arr_for_select_box_index];
	                                    }
	            
	                                });
	                                //console.log(variant_arr_for_select_box, "affter delete");
	                                variant_arr_for_select_box.clean(undefined);
	                                //console.log(variant_arr_for_select_box, "affter clean");
	                                //push data to variant multi select
	                                $("#wishlist-variant").multiselect('dataprovider', variant_arr_for_select_box);
	                                //reload checked variant
	                                $.reUpdateMultiselect("#wishlist-model");
	                            }
	                        }
	                    });
	                    /** end MODEL SECTION **/

			            //update or add more model to session local storage
	                    var wish_list_make_model_variant_data_checked = null;
	                    if (sessionStorage.getItem("wish_list_make_model_variant_data_checked") == 0) {
	                        wish_list_make_model_variant_data_checked = {"data_checked": []};
	                    } else {
	                        wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
	                        wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);
	                    }
	                    
	                    for (var i = wish_list_make_model_variant_data_checked.data_checked.length - 1; i >= 0; i--) {	
	                    	
	                    	$.each(model_checked_for_multiselect, function(model_checked_for_multiselect_index, model_checked_for_multiselect_value) {
	                    		
	                    		if (model_checked_for_multiselect_value.split("_")[0] == wish_list_make_model_variant_data_checked.data_checked[i].make_name) {
	                    			
	                    			if (wish_list_make_model_variant_data_checked.data_checked[i].model_items.length < 1) {
	                    				
	                    				wish_list_make_model_variant_data_checked.data_checked[i].model_items.push({
		                    				"model_name": model_checked_for_multiselect_value.split("_")[1],
		                					"variant_items": []
	                    				});

	                    			} else {

	                    				$.each(wish_list_make_model_variant_data_checked.data_checked[i].model_items, function(index, value) {
	                    					if (value.model_name == model_checked_for_multiselect_value.split("_")[1]) {
	                    						return false;
	                    					} else {
	                    						if (wish_list_make_model_variant_data_checked.data_checked[i].model_items.length-1 == index) {
	                    							wish_list_make_model_variant_data_checked.data_checked[i].model_items.push({
		                    							"model_name": model_checked_for_multiselect_value.split("_")[1],
		                								"variant_items": []
	                    							});
	                    						}
	                    					}
	                    				});

	                    			}
	                    			
	                    		}
	                    	});
	                    }

						console.log(wish_list_make_model_variant_data_checked, "after add model");
	                    wish_list_make_model_variant_data_checked = JSON.stringify(wish_list_make_model_variant_data_checked);
	                    sessionStorage.setItem("wish_list_make_model_variant_data_checked", wish_list_make_model_variant_data_checked);
			            //end step 3

						console.log("step 11: end model section");

						/**--break--**/

			            //step 4: load variant based on model check

						console.log("step 12: begin variant");
						if (model_checked_for_multiselect.length >= 1) {
				            $.each(model_checked_for_multiselect, function(model_index, model_value){
				            	var make_of_model_selected = model_value.split("_")[0];
								console.log("step 13: make value", make_of_model_selected);
				            	var selected_model = model_value;
								console.log("step 14: model value", selected_model);
				            	// use ajax to get variant by make and model
						        $.ajax({
						            type: 'POST',
						            data: {_token: token, make: make_of_model_selected, model: selected_model.split("_")[1]},
						            url: "/get-glasses-data/variant",
						            success:function(data_variant) {
						                var data_variant_arr = data_variant.toString().split(',');
						                data_variant_arr.clean("");

						                variant_arr_for_select_box.push({
						                    label: make_of_model_selected +" "+ selected_model.split("_")[1],
						                    children: [
						                    ]
						                });

						                $.each(data_variant_arr, function(key_data_variant_arr, value_data_variant_arr) {

						                    variant_arr_for_select_box[variant_arr_for_select_box.length-1].children.push(
						                        { label: value_data_variant_arr, value: make_of_model_selected+"_"+selected_model.split("_")[1]+"_"+value_data_variant_arr }
						                    );

						                });

						                //push data to variant - data list
						                console.log(variant_arr_for_select_box, "variant_arr_for_select_box");
						                //clear duplicate
										variant_arr_for_select_box = variant_arr_for_select_box.filter((variant_arr_for_select_box, index, self) => self.findIndex((t) => {return t.label === variant_arr_for_select_box.label && t.label === variant_arr_for_select_box.label; }) === index);
						                $("#wishlist-variant").multiselect('dataprovider', variant_arr_for_select_box);

						                //step 5: check variant
						                var variant_checked_for_multiselect = [];
										for (var i = edit_make_info.length - 1; i >= 0; i--) {
											if (edit_make_info[i].model != null) {
												if (typeof (edit_make_info[i].variant) != "undefined" && edit_make_info[i].variant != null) {
													variant_checked_for_multiselect.push(edit_make_info[i].make+"_"+edit_make_info[i].model+"_"+edit_make_info[i].variant);
												}
											}
										};
										//prepare to check variant
										console.log(variant_checked_for_multiselect, "variant_checked_for_multiselect");
				            			$("#wishlist-variant").multiselect('select', variant_checked_for_multiselect);
										console.log("step 15: variant checked", variant_checked_for_multiselect);
										
				            			//update local session storage here (add variant)
				            			var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
		                        		wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);

		                        		console.log(variant_checked_for_multiselect, "add variant multiselect");
		                        		console.log(wish_list_make_model_variant_data_checked, "add variant datachecked");
		                        		
		                        		$.each(wish_list_make_model_variant_data_checked.data_checked, function(index, value) {
		                        			console.log(value, "add variant model checked");
		                        			$.each(value.model_items, function(model_items_index, model_items_value) {
		                        				console.log(model_items_value, "add variant model items value");
		                        				//compare make and model
		                        				if (model_items_value.variant_items.length < 1) {
		                        					$.each(variant_checked_for_multiselect, function(variant_checked_for_multiselect_index, variant_checked_for_multiselect_value){
		                        						console.log(variant_checked_for_multiselect_value, "add variant datachecked multiselect");
		                        						if (variant_checked_for_multiselect_value.split("_")[0] == value.make_name) {
		                        							if (variant_checked_for_multiselect_value.split("_")[1] == model_items_value.model_name) {
		                        								wish_list_make_model_variant_data_checked.data_checked[index].model_items[model_items_index].variant_items.push({
		                        									"variant_name": variant_checked_for_multiselect_value.split("_")[2]
		                        								});
		                        							}
		                        						}
		                        					});
		                        				} else {
		                        					//update here
		                        				}
		                        			});
		                        		});
										
										//save session storage
										wish_list_make_model_variant_data_checked = JSON.stringify(wish_list_make_model_variant_data_checked);
		                    			sessionStorage.setItem("wish_list_make_model_variant_data_checked", wish_list_make_model_variant_data_checked);

										var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
		                        		wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);

		                        		console.log(wish_list_make_model_variant_data_checked, "add variant session storage log");

		                        		//rebind event for variant
		                        		//bind event click for variant list
						                $("#wishlist-variant").next().children().find("input[type='checkbox']").click(function(){
						                    /* variant is checked */
						                    if($(this).is(":checked")) {

						                        var variant_value = $(this).val();

						                        var make = variant_value.split("_")[0];
						                        var model = variant_value.split("_")[1];
						                        var variant = variant_value.split("_")[2];

						                        //save variant to datachecked local session storage
						                        var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
						                        wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);
						                        
						                        //begin remove make session storage
						                        //make
						                        $.each(wish_list_make_model_variant_data_checked.data_checked, function(index, value){
						                            if (typeof (value) != "undefined") {
						                                if (value.make_name.toString() == make) {
						                                    //model
						                                    $.each(value.model_items, function(model_items_index, model_items_value) {

						                                        if(typeof(model_items_value) != "undefined") {
						                                            if (model_items_value.model_name == model) 
						                                            {
						                                                //if no any variant
						                                                if (model_items_value.variant_items.length < 1) {
						                                                    wish_list_make_model_variant_data_checked.data_checked[index].model_items[model_items_index].variant_items.push({
						                                                        "variant_name": variant
						                                                    });
						                                                } else {
						                                                    //variant
						                                                    $.each(model_items_value.variant_items, function(variant_items_index, variant_items_value) {

						                                                        if (typeof (variant_items_value) != "undefined") 
						                                                        {

						                                                            if (variant_items_value.variant_name == variant) {
						                                                                return false;
						                                                            } else {
						                                                                //assign variant index to var add_more_variant
						                                                               
						                                                                //if (parseInt(variant_items_index)+1 == parseInt(variant_items_value.length)) {
						                                                                    
						                                                                    wish_list_make_model_variant_data_checked.data_checked[index].model_items[model_items_index].variant_items.push({
						                                                                        "variant_name": variant
						                                                                    });
						                                                                    return false;
						                                                                //}
						                                                            }
						                                                        }

						                                                    });
						                                                }
						                                            }
						                                        }
						                                        if (model_items_value.variant_items.length == -1) {
						                                            wish_list_make_model_variant_data_checked.data_checked[index].model_items[model_items_index].variant_items.push({
						                                                "variant_name": variant
						                                            });
						                                        } else {

						                                        }
						                                    });

						                                }
						                            }
						                        });
						                        
						                        //save remove
						                        wish_list_make_model_variant_data_checked = JSON.stringify(wish_list_make_model_variant_data_checked);
						                        sessionStorage.setItem("wish_list_make_model_variant_data_checked", wish_list_make_model_variant_data_checked);

						                        var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
						                        wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);

						                        console.log(wish_list_make_model_variant_data_checked, "variant add");

						                    } 
						                    /* variant is uncheck */
						                    else if (!$(this).is(":checked")) {
						                    	
						                        var variant_value = $(this).val();
						                        var make = variant_value.split("_")[0];
						                        var model = variant_value.split("_")[1];
						                        var variant = variant_value.split("_")[2];
						                 
						                        //remove variant item in datachecked session local storage
						                        //save variant to datachecked local session storage
						                        var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
						                        wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);
						                        
						                        //begin remove make session storage
						                        $.each(wish_list_make_model_variant_data_checked.data_checked, function(index, value){
						                            if (typeof (value) != "undefined") {
						                                if (value.make_name.toString() == make) {
						                                    
						                                    $.each(value.model_items, function(model_items_index, model_items_value) {
						                                        if(typeof(model_items_value) != "undefined") {

						                                            if (model_items_value.model_name == model) 
						                                            {
						                                            
						                                                $.each(model_items_value.variant_items, function(variant_items_index, variant_items_value) {  
						                                                    if(typeof(variant_items_value) != "undefined") {        
						                                                        console.log(variant_items_value, "variant delete");
						                                                        console.log(variant);  
						                                                        if (variant_items_value.variant_name == variant) {
						                                                            wish_list_make_model_variant_data_checked.data_checked[index].model_items[model_items_index].variant_items.splice(variant_items_index, 1);
						                                                        }
						                                                    }
						                                                });

						                                            }

						                                        }
						                                    });
						                                }
						                            }
						                        });

						                        //save remove
						                        wish_list_make_model_variant_data_checked = JSON.stringify(wish_list_make_model_variant_data_checked);
						                        sessionStorage.setItem("wish_list_make_model_variant_data_checked", wish_list_make_model_variant_data_checked);

						                        var wish_list_make_model_variant_data_checked2 = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
						                        wish_list_make_model_variant_data_checked2 = $.parseJSON(wish_list_make_model_variant_data_checked2);
						                        console.log(wish_list_make_model_variant_data_checked2, "variant delete");
						                    }
						                });
										//end bind event click for variant list
										//end step5
										console.log("step 16: end variant");

						            },
						            error: function(data){
						                console.log('/get-glasses-data/variant error: '+data);
						            },
						            complete: function(){
						                $(".mo-loading-image").addClass("hidden");
						            }
						        });
				            });
						}
			            //end step 4

	                    //bind event click to model
	                    /** MODEL SECTION **/
	                    //$("#wishlist-model").next().children().find("input[type='checkbox']").unbind();
	                    
	                    /** end MODEL SECTION **/
	                },

	                error: function(error){
	                    console.log('/get-glasses-data/family error: '+error);
	                },
	                complete: function(){
	                    $(".mo-loading-image").addClass("hidden");
	                }
                
            	});

            });
			//end step 2
		}

		//step 15 - odometer
		console.log("step 15");
		console.log(wish_list_detail.odometer_range, "odometer_range");
		var odo_range_from = wish_list_detail.odometer_range.split("-")[0];
		var odo_range_to = wish_list_detail.odometer_range.split("-")[1];

		$("#wishlist-odo-slider-range").slider('values',0,odo_range_from);
		$("#wishlist-odo-slider-range").slider('values',1,odo_range_to);
		$("#wishlist-odo-amount").val(digits(odo_range_from) + " - " + digits(odo_range_to));
		//end step 15

		//step 16 - year
		console.log(wish_list_detail.year_range, "year_range");
		var year_range_from = wish_list_detail.year_range.split("-")[0];

		var year_range_to = wish_list_detail.year_range.split("-")[1];

		$("#wishlist-year-slider-range").slider('values',0,year_range_from);
		$("#wishlist-year-slider-range").slider('values',1,year_range_to);
		$("#wishlist-year-amount").val(year_range_from + " - " + year_range_to);
		//end step 16

		//step 17 - price
		console.log(wish_list_detail.price_range, "price_range");
		var price_range_from = wish_list_detail.price_range.split("-")[0];

		var price_range_to = wish_list_detail.price_range.split("-")[1];

		$("#wishlist-price-slider-range").slider('values',0,price_range_from);
		$("#wishlist-price-slider-range").slider('values',1,price_range_to);
		$("#wishlist-price-amount").val("$" + digits(price_range_from) + " - " + "$" + digits(price_range_to));
		//end step 17 - price

		//step 18 - location
		
		console.log(wish_list_detail.locationId, "location");
		if (wish_list_detail.locationId != null) {

			console.log("step location: location", wish_list_detail.locationId);
			var location_id_arr = [];
			if (wish_list_detail.locationId.toString().indexOf(",") == -1) {
				location_id_arr.push(wish_list_detail.locationId);
			} else {
				location_id_arr = wish_list_detail.locationId.split(",");

			}

			if (location_id_arr.length >= 1) {
				var location_checked_for_multiselect = [];
				$.each(location_id_arr, function(index, value) {
					location_checked_for_multiselect.push(value);
				});
				$("#wishlist-location").multiselect('select', location_checked_for_multiselect);
			}
		}
		
		//end step 18

		//step 19 - transmission
		
		console.log(wish_list_detail.transmission, "transmission");
		if (wish_list_detail.transmission != null && wish_list_detail.transmission.length >= 1) {
			var transmission_checked_for_multiselect = [];
			for (var i = wish_list_detail.transmission.length - 1; i >= 0; i--) {
				transmission_checked_for_multiselect.push(wish_list_detail.transmission[i]);
			};
			$("#wishlist-transmission-type").multiselect('select', transmission_checked_for_multiselect);
		}
		
		//end step 19 
	}

	//$("#mo-wishlist-form :input").change(function() {
   	//	$("#mo-wishlist-form").data("changed",true);
	//});

	$("#wishlist-update-btn").click(function() {
		console.info("wishlist-update-btn clicked");
		//var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
        //wish_list_make_model_variant_data_checked = $.parseJSON(wish_list_make_model_variant_data_checked);

        //console.log(wish_list_make_model_variant_data_checked, "wish_list_make_model_variant_data_checked");

        var wish_list_make_model_variant_data_checked = sessionStorage.getItem("wish_list_make_model_variant_data_checked");
	    var make_info = $.parseJSON(wish_list_make_model_variant_data_checked);

	    console.log(make_info, "make_info");

	    var make_info_api_format = [];

	    if(make_info != 0 && make_info.data_checked.length >= 1) {

	        $.each(make_info.data_checked, function(index, value) {
	            
	            if (value.model_items.length < 1) {
	                make_info_api_format.push({
	                    "make": value.make_name
	                });
	            } else {
	                $.each(value.model_items, function(model_items_index, model_items_value){
	                    console.log(model_items_value, "value");

	                    if(model_items_value.variant_items.length < 1) {
	                        make_info_api_format.push({
	                            "make": value.make_name,
	                            "model": model_items_value.model_name
	                        });
	                    } 
	                    else 
	                    {
	                        $.each(model_items_value.variant_items, function(variant_items_index, variant_items_value){
	                            make_info_api_format.push({
	                                "make": value.make_name,
	                                "model": model_items_value.model_name,
	                                "variant": variant_items_value.variant_name
	                            });
	                        });

	                    }
	                })
	            }
	        });

	    }

	    console.log(make_info_api_format, "make_info_api_format");
	   
	    var make_info_field = "<input type='hidden' name='make_info' value='"+JSON.stringify(make_info_api_format)+"'/>";

	    $("#mo-wishlist-form").append(make_info_field);

        $("#mo-wishlist-form").submit();

	});

});