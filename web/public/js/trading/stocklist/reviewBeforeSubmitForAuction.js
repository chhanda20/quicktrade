$(document).ready(function(){
    /** COMMON TOKEN **/
    var token = $("input[name='_token']").val();

    /** BY DEFAULT **/
    $("#inspection-clone").hide();
    $("#inspection-item-wrapper-clone").hide();

    function digits(number){
        return number.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    }

    /** VEHICLE THUMBS IMAGE SLIDER **/
    var light_slider = $("#bx-thumbnail").lightSlider({
        pager: false,
        item: 3
    });

    /** VEHICLE IMAGE SLIDER **/
    $('.bxslider').bxSlider({
        /*adaptiveHeight: true,*/
        mode: 'fade',
        pagerCustom: '#bx-thumbnail',
        onSliderLoad: function(){
            light_slider.refresh();
        }
    });

    /** AUCTION DATE PICKER **/
    var auction_date_array = $("#mo-vehicle-auction-date-array").val().split(',');
    //console.log(auction_date_array);
    $('#mo-vehicle-auction-date').datepicker({
        todayHighlight: true,
        orientation: 'bottom',
        beforeShowDay:function(Date){
            //var curr_date = Date.toJSON().substring(0,10);
            var months = Date.getMonth() + 1;
            if(months.toString().length == 1){
                months = "0"+months;
            }

            var dates = Date.getDate();
            if(dates.toString().length == 1){
                dates = "0"+dates;
            }

            var curr_date = Date.getFullYear() + '-' + months + '-' + dates;
            /**  show available dates in auction session only **/
            if (auction_date_array.indexOf(curr_date) == -1) return false;
        }
    });

    $(".glyphicon-calendar").click(function(){
        $("#mo-vehicle-auction-date").focus();/** focus to show calendar **/
    });

    $('#mo-vehicle-auction-date').on('changeDate', function(ev){
        $(this).datepicker('hide');
        $(".mo-auction-time").parent().next().removeClass("disabled-div");
    });

    $('#mo-vehicle-auction-date').change(function(){
        $(document).off(".cancelAuction");
        $(document).off(".sendToAuction");
        $(document).off(".addItem");
        $(document).off(".editItem");
        $(document).off(".inlineEdit");
        $(document).off(".deleteItem");
        $.ajax({
            type: "POST",
            url: "/trading/get-auction-times",
            data: {
                _token: $("input[name='_token']").val(),
                session_date: $(this).val()
            },
            success: function(data) {
                if(data){
                    $("#mo-vehicle-auction-time-select option[value]").remove();
                    time_data = JSON.parse(data);
                    console.log(time_data);
                    $.each(time_data, function (index, value) {
                        //console.log(index + ':' + value["section_name"]);
                        $("#mo-vehicle-auction-time-select").append($("<option>", {
                            value: index,
                            text: value
                        }));
                    });
                }
            },
            complete: function(){
            }
        });
    });

    var auction_date = $('#mo-vehicle-auction-date').val();
    var auction_session_id = $('#auction_session_id').val();
    if (auction_date && auction_session_id) {
        timeSelected = parseInt(auction_session_id);

        $.ajax({
            type: "POST",
            url: "/trading/get-auction-times",
            data: {
                _token: $("input[name='_token']").val(),
                session_date: auction_date
            },
            success: function(data) {
                if(data){
                    $("#mo-vehicle-auction-time-select option[value]").remove();
                    time_data = JSON.parse(data);
                    console.log(time_data);
                    $.each(time_data, function (index, value) {
                        if (parseInt(index) === timeSelected) {
                            $("#mo-vehicle-auction-time-select").append($("<option>", {
                                value: index,
                                text: value,
                                selected: true
                            }));
                        } else {
                            $("#mo-vehicle-auction-time-select").append($("<option>", {
                                value: index,
                                text: value
                            }));
                        }

                    });
                    $(".mo-auction-time").parent().next().removeClass("disabled-div");
                }
            },
            complete: function(){
            }
        });
    }

    /** AUCTION TIME PICKER **/
    $('#mo-vehicle-auction-time').timepicker();

    /** code to not allow any changes to be made to input field **/
    $('#mo-vehicle-auction-time, #mo-vehicle-auction-date').keydown(function() {
        return false;
    });

    /** VALIDATE FORM **/
    $.validator.addMethod("greaterThan", function (value, element, param) {
        var $element = $(element)
            , $min;

        if (typeof(param) === "string") {
            $min = $(param);
        } else {
            $min = $("#" + $element.data("min"));
        }

        if (this.settings.onfocusout) {
            $min.off(".validate-greaterThan").on("blur.validate-greaterThan", function () {
                $element.valid();
            });
        }
        return parseInt(value) > parseInt($min.val());
    }, "Max must be greater than min");

    $.validator.addClassRules({
        greaterThan: {
            greaterThan: true
        }
    });

    $('#mo-send-to-auction-form').validate({
        rules:{
            starting_price: "required",
            auction_date: "required",
            //buy_now_price: "required",
            auction_time_selected_id: "required",
            all_info: "required",
            ppsr: "required",
            term_on_sale: "required"
        },
        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        errorPlacement: function(error, element) {
            if (element.attr("name") == "auction_date" ) {
                error.appendTo("#errorToShow_auction_date");
            } else if (element.attr("name") == "auction_time" ) {
                error.appendTo("#errorToShow_auction_time");
            } else if (element.attr("name") == "all_info" ) {
                error.appendTo("#errorToShow_all_info");
            } else if (element.attr("name") == "ppsr" ) {
                error.appendTo("#errorToShow_ppsr");
            }  else if (element.attr("name") == "term_on_sale" ) {
                error.appendTo("#errorToShow_term_on_sale");
            } else {
                error.insertAfter(element);// default placement
            }
        },
        messages:{
            starting_price: "<div class='mo-error-wrapper'><img src='../../images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
            auction_date: "<div class='mo-error-wrapper'><img src='../../images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
            buy_now_price: "<div class='mo-error-wrapper'><img src='../../images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty and have to be greater than 'Starting Price'.</span>"+"</div>",
            auction_time_selected_id: "<div class='mo-error-wrapper'><img src='../../images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
            all_info: "<div class='mo-error-wrapper'><img src='../../images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
            ppsr: "<div class='mo-error-wrapper'><img src='../../images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
            term_on_sale: "<div class='mo-error-wrapper'><img src='../../images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>"
        },
        submitHandler: function(form){
            //form.submit();
        }
    });

    /** VALIDATE PRICE **/
    function validatePrice(obj){
        if(obj.val().length > 0){
            $("#mo-vehicle-buy-now-price").removeClass("disabled-div");
        } else {
            $("#mo-vehicle-buy-now-price").addClass("disabled-div");
        }
    }

    validatePrice($("#mo-vehicle-buy-now-price"));
    $("#mo-vehicle-starting-price").on("input", function () {
        validatePrice($("#mo-vehicle-starting-price"));
    });

    /** CANCEL - SEND TO AUCTION **/
    $(document).on("click","#mo-cancel-send-auction",function() {
        $(document).off(".sendToAuction");
        $(document).off(".addItem");
        $(document).off(".editItem");
        $(document).off(".inlineEdit");
        $(document).off(".deleteItem");
        $(document).on("ajaxStart.cancelAuction", function () {
            $(".mo-loading-image").removeClass("hidden");
        });
        $(document).on("ajaxStop.cancelAuction", function () {
            $(".mo-loading-image").addClass("hidden");
        });
        $.ajax({
            type: "POST",
            url: "/trading/cancel-modifying-version",
            data: {
                _token: token,
                valuation_id: $("#valuation-id").val()
            },
            success: function(data) {
                console.log(data);
                if(data == 1){
                    window.location.href = "/trading/stock-list";
                }else{
                    $(".alert-danger").find(".mo-error-message").text(data);
                    $(".alert-danger").removeClass("hidden");
                }
            }
        });
    });

    /** SUBMIT FORM - SEND TO AUCTION **/
    $(document).on("click","#mo-confirm-send-auction",function() {
        var form = $('#mo-send-to-auction-form');
        if(form.valid()){
            $(document).off(".cancelAuction");
            $(document).off(".addItem");
            $(document).off(".editItem");
            $(document).off(".inlineEdit");
            $(document).off(".deleteItem");
            $(document).on("ajaxStart.sendToAuction", function () {
                $(".mo-loading-image").removeClass("hidden");
            });
            $(document).on("ajaxStop.sendToAuction", function () {
                $(".mo-loading-image").addClass("hidden");
            });
            $.ajax({
                type: "POST",
                url: "/trading/send-to-auction",
                data: form.serialize(),
                success: function(data) {
                    console.log(data);
                    if(data == "true"){
                        window.location.href = "/trading/stock-list#vehicle-submitted";
                    }else{
                        $(".alert-danger").find(".mo-error-message").text(data);
                        $(".alert-danger").removeClass("hidden");
                        $("#mo-send-to-auction-popup").scrollTop(0);
                    }
                }
            });
        }
    });

    /** POPUP **/
    function callPopup(){
        $(document).on("click",".mo-cancel-btn",function() {
            var magnificPopup = $.magnificPopup.instance;
            magnificPopup.close();
        });

        $(".mo-popup").magnificPopup({
            type:'inline',
            showCloseBtn: true,
            closeOnBgClick: false
        });
    }
    callPopup();

    /** POPULATE DATA - REMOVE INSPECTION ITEM POPUP **/
    $(document).on("click","#mo-remove-popup",function() {
        //$(this).parents(".inspection-section-wrapper").remove();
        var inspection_id = $(this).children().data("inspection-id");
        $("#mo-remove-confirm-popup").find("#mo-confirm-cancel").attr("data-inspection-item-id", inspection_id);
    });

    $(document).on("click","#mo-confirm-cancel",function() {
        var inspection_item_id = $(this).attr("data-inspection-item-id");
        $(document).off(".cancelAuction");
        $(document).off(".sendToAuction");
        $(document).off(".addItem");
        $(document).off(".editItem");
        $(document).off(".inlineEdit");
        $(document).on("ajaxStart.deleteItem", function () {
            $(".mo-loading-image").removeClass("hidden");
        });
        $(document).on("ajaxStop.deleteItem", function () {
            $(".mo-loading-image").addClass("hidden");
        });
        $.ajax({
            type: "POST",
            url: "/delete-inspection-item",
            data: {
                _token: token,
                valuation_id: $("#inspection-valuation-id").val(),
                inspection_item_id: inspection_item_id
            },
            success: function(data) {
                data = $.parseJSON(data);
                console.log(data);
                if(data.success == true){
                    var delete_elem = $(".mo-popup").children("span[data-inspection-id="+inspection_item_id+"]");
                    if(delete_elem.parents(".inspection-section-wrapper").children(".inspection-item-wrapper").length === 1){
                        delete_elem.parents(".inspection-section-wrapper").remove();
                    }else{
                        delete_elem.parents(".inspection-item-wrapper").remove();
                    }
                    toggleAndCalculateCost();
                    // CLOSE POPUP
                    var magnificPopup = $.magnificPopup.instance;
                    magnificPopup.close();
                }else{
                    $(".alert-danger").find(".mo-error-message").text(data.error.message);
                    $(".alert-danger").removeClass("hidden");
                }
            }
        });
    });

    function populateImage(azure_account_name, valuation_id, valuation_current_version, inspection_id) {
        var image_url = "https://"+azure_account_name+".blob.core.windows.net/inspection-item/"+valuation_id+"_"+valuation_current_version+"_"+inspection_id;
        $(".mo-inspection-upload-image-icon").each(function(i, obj) {
            i += 1;
            var image_src = image_url+"_"+i+".jpg?"+Math.random();
            /* CHECK IMAGE EXISTS */
            $.get(image_src)
                .done(function() {
                    // Do something now you know the image exists.
                    $(obj).attr("src", image_src);
                }).fail(function() {
                // Image doesn't exist - do something else.
                $(obj).attr("src", "/images/perform_valuation/image_upload.png");
            });
        });
    }

    /** POPULATE DATA - EDIT INSPECTION ITEM POPUP **/
    $(document).on("click","#mo-edit-popup",function() {
        /* GET SECTION TYPE LIST */
        var section = $(this).children().attr("data-section");
        var section_type = $(this).children().attr("data-section-type");
        $(document).off(".cancelAuction");
        $(document).off(".sendToAuction");
        $(document).off(".addItem");
        $(document).off(".editItem");
        $(document).off(".inlineEdit");
        $(document).off(".deleteItem");
        $.ajax({
            type: "GET",
            url: "/get-inspection-items-list/"+section,
            data: null,
            success: function(data) {
                data = $.parseJSON(data);
                //console.log(data);
                $.each(data, function (index, value) {
                    //console.log(index + ':' + value["section_name"]);
                    $("#inspection-sections-select").append($("<option>", {
                        value: value.section_id,
                        text: value.section_name
                    }));
                });
                $("select#inspection-sections-select option[value='"+section_type+"']").attr("selected","selected");
            }
        });

        /* GET INSPECTION ITEMS LIST */
        var inspection_id = $(this).children().attr("data-inspection-id");
        var repair_cost = $(this).children().attr("data-repair-cost");
        var note = $(this).children().attr("data-note");
        var azure_account_name = $("#azure-accout-name").val();
        var valuation_id = $("#inspection-valuation-id").val();
        var valuation_current_version = $("#inspection-valuation-version").val();
        $.ajax({
            type: "POST",
            url: "/get-items/"+section,
            data: {
                _token: token,
                valuation_id: valuation_id
            },
            success: function(data) {
                //console.log(data);
                data = $.parseJSON(data);
                $("#inspection-items-select").empty();
                $.each(data, function (index, value) {
                    $.each(value.items, function (i, v) {
                        $("#inspection-items-select").append($("<option>", {
                            value: v.inspection_item_id,
                            text: v.item
                        }));
                    });
                });

                /* POPULATE DATA */
                $("#inspection-item-id").val(inspection_id);
                $("select#inspection-items-select option[value='"+inspection_id+"']").attr("selected","selected");
                $("#repair-cost").val(repair_cost);
                $("#note").val(note);
                populateImage(azure_account_name, valuation_id, valuation_current_version, inspection_id);
            }
        });
    });

    function updateInspectionItem(valuation_id, inspection_id, repair_cost, note){
        $(document).off(".cancelAuction");
        $(document).off(".sendToAuction");
        $(document).off(".addItem");
        $(document).off(".deleteItem");
        $(document).off(".inlineEdit");
        $(document).on("ajaxStart.editItem", function () {
            $(".mo-loading-image").removeClass("hidden");
        });
        $(document).on("ajaxStop.editItem", function () {
            $(".mo-loading-image").addClass("hidden");
        });
        $.ajax({
            type: "POST",
            url: "/update-inspection-item",
            data: {
                _token: token,
                valuation_id: valuation_id,
                inspection_item_id: inspection_id,
                repair_cost: repair_cost,
                note: note
            },
            success: function(data) {
                data = $.parseJSON(data);
                console.log(data);
                if(data.success == true){
                    var span_contain_inspection_id = $(".mo-popup").children("span[data-inspection-id="+inspection_id+"]");
                    span_contain_inspection_id.attr("data-note", note);
                    span_contain_inspection_id.attr("data-repair-cost", repair_cost);
                    span_contain_inspection_id.parents(".inspection-record").find(".repair-cost-span").text(digits(parseFloat(repair_cost).toFixed(2)));
                    toggleAndCalculateCost();
                    var magnificPopup = $.magnificPopup.instance;
                    magnificPopup.close();
                }else{
                    $(".alert-danger").find(".mo-error-message").text(data.error.message);
                    $(".alert-danger").removeClass("hidden");
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    /** UPDATE INSPECTION ITEM **/
    $(document).on("click","#inspection-update-button",function() {
        var valuation_id = $("#inspection-valuation-id").val();
        var inspection_id = $("#inspection-items-select").val();
        var repair_cost = $("#repair-cost").val();
        var note = $("#note").val();
        updateInspectionItem(valuation_id, inspection_id, repair_cost, note);
    });

    var current_element = null;

    $("#fileToUpload").on("change", function() {
        if($("#fileToUpload").val())
        {
            $("#mo-exterior-popup-form").submit();
        }
        $("#fileToUpload").val("");
    });

    $("label[for='fileToUpload']").on("click", function () {
        current_element = $(this);
        var sequence_number = null;
        if (current_element.parent().attr("id") == "mo-image-box-1") {
            sequence_number = 1;
        } else if (current_element.parent().attr("id") == "mo-image-box-2") {
            sequence_number = 2;
        } else if (current_element.parent().attr("id") == "mo-image-box-3") {
            sequence_number = 3;
        }
        $("input[name='sequence']").val(sequence_number);
    });

    $("#mo-exterior-popup-form").on("submit",(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        current_element.children().attr("src", "/images/perform_valuation/img-loading.gif");

        $(document).off(".cancelAuction");
        $(document).off(".sendToAuction");
        $(document).off(".addItem");
        $(document).off(".editItem");
        $(document).off(".inlineEdit");
        $(document).off(".deleteItem");
        $.ajax({
            type:'POST',
            url: "/upload-image",
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data) {
                console.log("success");
                console.log(data);
                current_element.children().attr("src", data+"?"+Math.random());
            },
            error: function(data){
                console.log("error");
                console.log(data);
            }

        });

    }));

    /** INLINE EDIT **/
    $(".repair-cost-input").hide();
    $(".repair-cost-span").click(function () {
        $(this).toggle();
        $(this).next().toggle();
        $(this).next().focus();
        $(this).next().val($(this).text().replace(/,/g,''));
    });

    $(".repair-cost-input").blur(function () {
        var valuation_id = $("#inspection-valuation-id").val();
        var repair_cost = parseFloat($(this).val()).toFixed(2);
        var cost_type = $(this).data("cost-type");
        var this_elem = $(this);

        $(document).off(".cancelAuction");
        $(document).off(".sendToAuction");
        $(document).off(".addItem");
        $(document).off(".deleteItem");
        $(document).off(".editItem");
        $(document).on("ajaxStart.inlineEdit", function () {
            this_elem.next(".mo-loading-icon").show();
        });
        $(document).on("ajaxStop.inlineEdit", function () {
            this_elem.next(".mo-loading-icon").hide();
        });

        if(cost_type == "repair"){
            var inspection_id = $(this).data("inspection-id");
            $.ajax({
                type: "POST",
                url: "/update-inspection-item",
                data: {
                    _token: token,
                    valuation_id: valuation_id,
                    inspection_item_id: inspection_id,
                    repair_cost: repair_cost
                },
                success: function(data) {
                    data = $.parseJSON(data);
                    console.log(data);
                    if(data.success == true){
                        toggleAndCalculateCost(this_elem, repair_cost);
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }else if(cost_type == "general"){
            $.ajax({
                type:'POST',
                url: "/re-conditioning-cost",
                data: {
                    _token: token,
                    valuation_id: valuation_id,
                    general_cost: repair_cost
                },
                success:function(data) {
                    data = $.parseJSON(data);
                    console.log(data);
                    if(data.success == true){
                        toggleAndCalculateCost(this_elem, repair_cost);
                    }
                }

            });
        }

    });

    function toggleAndCalculateCost(this_elem, repair_cost) {
        if(this_elem != null && repair_cost != null){
            this_elem.toggle();
            this_elem.prev().toggle();
            this_elem.prev().text(digits(repair_cost));
            this_elem.parents(".inspection-record").find("#mo-edit-popup").children("span").attr("data-repair-cost", repair_cost);
        }

        var section_arr = ["exterior", "interior", "mechanical"];
        for(var i = 0; i < section_arr.length; i++){
            var total_cost_update = 0;
            $("span."+section_arr[i]+"-cost-span").each(function(i, obj) {
                total_cost_update += parseFloat($(obj).text().replace(/,/g,''));
            });
            $("span#total-cost-"+section_arr[i]).text(digits(total_cost_update.toFixed(2)));
        }

        var total_damage_cost = parseFloat($("#total-cost-exterior").text().replace(/,/g,'')) + parseFloat($("#total-cost-interior").text().replace(/,/g,'')) + parseFloat($("#total-cost-mechanical").text().replace(/,/g,''));
        $("#total-damage-cost").text(digits(total_damage_cost.toFixed(2)));
    }


    /** TRIGGER GET DATA - NEW INSPECTION ITEM **/
    //use ajax to get inspectionItems information (EXTERIOR_SECTION, INTERIOR_SECTION, MECHANICAL_SECTION)
    $(document).off(".cancelAuction");
    $(document).off(".sendToAuction");
    $(document).off(".addItem");
    $(document).off(".editItem");
    $(document).off(".inlineEdit");
    $(document).off(".deleteItem");
    $.ajax({
        type: "GET",
        url: "/get-inspection-items-list/EXTERIOR_SECTION",
        data: {
            _token: $("input[name='_token']").val()
        },
        success: function(response) {

            var inspection_items = $.parseJSON(response);
            //store inspection_items in session local storage
            sessionStorage.setItem("inspection_items", response);
            //init
            //append data to section box
            //clear before add
            //$("#inspection-section :gt(0)").empty();
            $("#inspection-section").empty();

            $.each(inspection_items, function(index, value){

                //append sections
                $("#inspection-section").append($("<option>", {
                    value: value.section_id,
                    text: value.section_name
                }));

                //append items
                //clear before add
                if (index == 0) {
                    //$("#new-inspection-item :gt(0)").empty();
                    $("#new-inspection-item").empty();
                    $.each(value.items, function(item_index, item_value) {
                        $("#new-inspection-item").append($("<option>", {
                            value: item_value.id,
                            text: item_value.item
                        }));
                    });
                }
            });

            console.log("call /get-inspection-items-list/EXTERIOR_SECTION", "sucess");

        },
        complete: function() {

        },
        error: function(error) {
            console.log("error /get-inspection-items-list/EXTERIOR_SECTION", error);
        }
    });

    //when category change item
    $("#inspection-category").change(function() {
        var section = $("#inspection-category").val();
        $(document).off(".cancelAuction");
        $(document).off(".sendToAuction");
        $(document).off(".addItem");
        $(document).off(".editItem");
        $(document).off(".inlineEdit");
        $(document).off(".deleteItem");
        $.ajax({
            type: "GET",
            url: "/get-inspection-items-list/"+section,
            data: {
                _token: $("input[name='_token']").val()
            },
            success: function(response) {
                var inspection_items = $.parseJSON(response);
                //append data to section box
                //clear before add
                $("#inspection-section").prop("disabled", false);
                $("#inspection-section").empty();
                $.each(inspection_items, function(index, value){

                    //append sections
                    $("#inspection-section").append($("<option>", {
                        value: value.section_id,
                        text: value.section_name
                    }));

                    //append items
                    //clear before add
                    if (index == 0) {
                        $("#new-inspection-item").prop("disabled", false);
                        $("#new-inspection-item").empty();
                        $.each(value.items, function(item_index, item_value) {
                            $("#new-inspection-item").append($("<option>", {
                                value: item_value.id,
                                text: item_value.item
                            }));
                        });

                        //populate data if item already exist
                        var valuation_id = $("#valuation-id").val();
                        getInspectionItemDetails(valuation_id, $("#new-inspection-item").val());
                    }
                });

                console.log("call /get-inspection-items-list/"+section, "sucess");

            },
            complete: function() {

            },
            error: function(error) {
                console.log("error /get-inspection-items-list/ALL", error);
            }
        });

    });

    //when section change
    $("#inspection-section").change(function(){
        var section_id_selected = $(this).val().toString();
        var inspection_items = sessionStorage.getItem("inspection_items");
        inspection_items = $.parseJSON(inspection_items);
        $.each(inspection_items, function(index, value){
            if (value.section_id == section_id_selected) {
                //append items
                //clear items before add
                $("#new-inspection-item").empty();
                $.each(value.items, function(item_index, item_value){
                    $("#new-inspection-item").append($("<option>", {
                        value: item_value.id,
                        text: item_value.item
                    }));
                });

                //populate data if item already exist
                var valuation_id = $("#valuation-id").val();
                getInspectionItemDetails(valuation_id, $("#new-inspection-item").val());
            }
        });
    });

    function getInspectionItemDetails(valuation_id, inspection_item_id) {
        var azure_account_name = $("#azure-accout-name").val();
        var valuation_current_version = $("#inspection-valuation-version").val();
        $.ajax({
            type: "GET",
            url: "/_get-inspection-item-detail/"+valuation_id+"/"+inspection_item_id,
            data: null,
            success: function (data) {
                var item_detail = JSON.parse(data);
                if(!item_detail.error) {
                    var cost = item_detail["repair_cost"];
                    var note = item_detail["note"];
                    $("#mo-add-new-inspection-item-form").find("#repair-cost").val(cost);
                    $("#mo-add-new-inspection-item-form").find("#note").val(note);
                    $("#inspection-item-id").val(inspection_item_id);

                    populateImage(azure_account_name, valuation_id, valuation_current_version, inspection_item_id);

                    $("#new-item-save").attr("data-add-or-edit", "edit");
                }else{
                    $("#mo-add-new-inspection-item-form").find("#repair-cost").val("");
                    $("#mo-add-new-inspection-item-form").find("#note").val("");

                    populateImage(azure_account_name, valuation_id, valuation_current_version, inspection_item_id);

                    $("#new-item-save").attr("data-add-or-edit", "add");
                }
            },
            error: function (data) {

            }
        });
    }

    $("#new-inspection-item").change(function(){
        var valuation_id = $("#valuation-id").val();
        var inspection_item_id = $(this).val();
        getInspectionItemDetails(valuation_id, inspection_item_id);
    });

    /** ADD NEW INSPECTION ITEM **/
    $(document).on("click","#mo-add-new-popup",function() {
        $("#inspection-section").prop("disabled", true);
        $("#new-inspection-item").prop("disabled", true);
        $("#mo-add-new-inspection-item-form").find("input[type=text], input[type=number], textarea").val("");
        $("select").val([]);
        $(".mo-inspection-upload-image-icon").each(function(i, obj) {
            $(obj).attr("src", "/images/perform_valuation/image_upload.png");
        });
    });

    $("#new-item-save").click(function () {
        var valuation_id = $("#mo-add-new-inspection-item-form").find("#inspection-valuation-id").val();
        var valuation_version = $("#mo-add-new-inspection-item-form").find("#inspection-valuation-version").val();
        var inspection_id = $("#mo-add-new-inspection-item-form").find("#new-inspection-item").val();
        var repair_cost = $("#mo-add-new-inspection-item-form").find("#repair-cost").val();
        var note = $("#mo-add-new-inspection-item-form").find("#note").val();

        var add_or_edit = $(this).attr("data-add-or-edit");

        if(add_or_edit == "add"){
            $(document).off(".cancelAuction");
            $(document).off(".sendToAuction");
            $(document).off(".deleteItem");
            $(document).off(".editItem");
            $(document).off(".inlineEdit");
            $(document).on("ajaxStart.addItem", function () {
                $(".mo-loading-image").removeClass("hidden");
            });
            $(document).on("ajaxStop.addItem", function () {
                $(".mo-loading-image").addClass("hidden");
            });
            $.ajax({
                type:'POST',
                url: "/add-inspection-item-with-photo",
                data: {
                    _token: token,
                    valuation_id: valuation_id,
                    valuation_version: valuation_version,
                    inspection_item_id: inspection_id,
                    repair_cost: repair_cost,
                    note: note
                },
                success:function(data) {
                    // data = $.parseJSON(data);
                    console.log(data);
                    if(data == 1){
                        ajaxAddNewItemUI();
                        toggleAndCalculateCost();
                        callPopup();
                        var magnificPopup = $.magnificPopup.instance;
                        magnificPopup.close();
                    }else{
                        var alert_wrapper = $("#mo-add-new-inspection-item-form").find(".alert-danger");
                        alert_wrapper.find(".mo-error-message").text(data);
                        alert_wrapper.removeClass("hidden");
                    }
                },
                complete: function () {
                    $(".mo-loading-image").addClass("hidden");
                }
            });
        }else if(add_or_edit == "edit"){
            updateInspectionItem(valuation_id, inspection_id, repair_cost, note);
        }
    });

    function ajaxAddNewItemUI(){
        var form = $("#mo-add-new-inspection-item-form");

        var inspection_category = form.find("#inspection-category").val();
        var section_id = form.find("#inspection-section").val();
        var section_name = form.find("#inspection-section option:selected").text();
        var inspection_id = form.find("#new-inspection-item").val();
        var inspection_name = form.find("#new-inspection-item option:selected").text();
        var repair_cost = digits(parseFloat(form.find("#repair-cost").val()).toFixed(2));
        var note = form.find("#note").val();

        var inspection_clone = $("#inspection-clone").clone();
        inspection_clone.attr("data-section-type", section_id);
        inspection_clone.find(".inspection-section-name").text(section_name);

        var inspection_item_clone = $("#inspection-item-wrapper-clone").clone();
        inspection_item_clone.find(".inspection-name").text(inspection_name);
        inspection_item_clone.find(".repair-cost-span").text(repair_cost);
        inspection_item_clone.find(".repair-cost-input").attr("data-inspection-id", inspection_id);

        var edit_icon_clone = inspection_item_clone.find("#mo-edit-popup").children("span");
        edit_icon_clone.attr("data-section", inspection_category);
        edit_icon_clone.attr("data-section-type", section_id);
        edit_icon_clone.attr("data-inspection-id", inspection_id);
        edit_icon_clone.attr("data-repair-cost", repair_cost);
        edit_icon_clone.attr("data-note", note);

        var remove_icon_clone = inspection_item_clone.find("#mo-remove-popup").children("span");
        remove_icon_clone.attr("data-inspection-id", inspection_id);

        switch (inspection_category){
            case "EXTERIOR_SECTION":
                inspection_item_clone.find(".repair-cost-span").addClass("exterior-cost-span");
                break;
            case "INTERIOR_SECTION":
                inspection_item_clone.find(".repair-cost-span").addClass("interior-cost-span");
                break;
            case "MECHANICAL_SECTION":
                inspection_item_clone.find(".repair-cost-span").addClass("mechanical-cost-span");
                break;
        }

        var section_id_arr = [];
        $(".inspection-section-wrapper:not(#inspection-clone)").each(function(i, obj) {
            section_id_arr.push($(obj).data("section-type"));
        });

        if(section_id_arr.indexOf(parseInt(section_id)) != -1){
            inspection_item_clone.show();
            $(".inspection-section-wrapper[data-section-type='"+section_id+"']").append(inspection_item_clone);
        }else{
            inspection_item_clone.show();
            inspection_clone.show();
            switch (inspection_category){
                case "EXTERIOR_SECTION":
                    $(".section-category[data-section-category='exterior']").append(inspection_clone);
                    break;
                case "INTERIOR_SECTION":
                    inspection_clone.empty();
                    $(".section-category[data-section-category='interior']").append(inspection_clone);
                    break;
                case "MECHANICAL_SECTION":
                    inspection_clone.empty();
                    $(".section-category[data-section-category='mechanical']").append(inspection_clone);
                    break;
            }
            inspection_clone.append(inspection_item_clone);
        }

    }

    /** ADD VEHICLE INSPECTION PHOTO **/
    var current_photo = null;

    $("#inspect-item-photo").change(function (){
        if ($(this).val()) {
            $("#mo-add-new-inspection-item-form").submit();
        }
        $(this).val(null);
    });

    $("label[for='inspect-item-photo']").on("click", function () {
        current_photo = $(this);
        $("input[name='sequence_add_photo']").val($(this).data("sequence"));
    });

    $("#mo-add-new-inspection-item-form").on("submit",(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        current_photo.children().attr("src", "/images/perform_valuation/img-loading.gif");

        $(document).off(".cancelAuction");
        $(document).off(".sendToAuction");
        $(document).off(".addItem");
        $(document).off(".editItem");
        $(document).off(".inlineEdit");
        $(document).off(".deleteItem");

        var add_or_edit = $(this).find("#new-item-save").attr("data-add-or-edit");
        var url = "/upload-item-photo-tmp";
        if(add_or_edit == "edit"){
            url = "/upload-image";
        }

        $.ajax({
            type:'POST',
            url: url,
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data) {
                console.log("success");
                console.log(data);
                current_photo.children().attr("src", data);
            },
            error: function(data){
                console.log("error");
                console.log(data);
            }
        });
    }));

});