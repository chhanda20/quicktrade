//use ajax to get inspectionItems information (EXTERIOR_SECTION, INTERIOR_SECTION, MECHANICAL_SECTION)
$(document).ready(function(){
	$.ajax({
		type: "GET",
		url: "/get-inspection-items-list/EXTERIOR_SECTION",
		data: {
			_token: $("input[name='_token']").val()
		},
		success: function(response) {

			var inspection_items = $.parseJSON(response);
			//store inspection_items in session local storage
			sessionStorage.setItem("inspection_items", response);
			//init
			//append data to section box
			//clear before add
			$("#arriving-vehicle-section").empty();

			$.each(inspection_items, function(index, value) {

				//append sections
				$("#arriving-vehicle-section").append($("<option>", {
					value: value.section_id,
					text: value.section_name
				}));

				//append items
				//clear before add
				if (index == 0) {
					$("#arriving-vehicle-item").empty();
					$.each(value.items, function(item_index, item_value) {
						$("#arriving-vehicle-item").append($("<option>", {
							value: item_value.id,
							text: item_value.item
						}));
					});
				}
			});

			console.log("call /get-inspection-items-list/EXTERIOR_SECTION", "sucess");

		},
		complete: function() {
			//$(".mo-loading-image").addClass("hidden");
		},
		error: function(error) {
			console.log("error /get-inspection-items-list/EXTERIOR_SECTION", error);
		},
		global: false
	});

	//when category change item
	$("#arriving-vehicle-category").change(function() {

		var section = $("#arriving-vehicle-category").val();
		$.ajax({
			type: "GET",
			url: "/get-inspection-items-list/"+section,
			data: {
				_token: $("input[name='_token']").val()
			},
			success: function(response) {
				var inspection_items = $.parseJSON(response);
				//append data to section box
				//clear before add
				$("#arriving-vehicle-section").empty();
				$.each(inspection_items, function(index, value){

					//append sections
					$("#arriving-vehicle-section").append($("<option>", {
						value: value.section_id,
						text: value.section_name
					}));

					//append items
					//clear before add
					if (index == 0) {
						$("#arriving-vehicle-item").empty();
						$.each(value.items, function(item_index, item_value) {
							$("#arriving-vehicle-item").append($("<option>", {
								value: item_value.id,
								text: item_value.item
							}));
						});
					}

				});

				console.log("call /get-inspection-items-list/"+section, "sucess");

			},
			complete: function() {
				$(".mo-loading-image").addClass("hidden");
				$("#arriving-vehicle-item").trigger("change");
			},
			error: function(error) {
				console.log("error /get-inspection-items-list/ALL", error);
			},
			global: true
		});

	});

	//when section change
	$("#arriving-vehicle-section").change(function() {

		var section_id_selected = $(this).val().toString();
		var inspection_items = sessionStorage.getItem("inspection_items");
		inspection_items = $.parseJSON(inspection_items);

		$.each(inspection_items, function(index, value){
			if (value.section_id == section_id_selected) {
				//append items
				//clear items before add
				$("#arriving-vehicle-item").empty();
				$.each(value.items, function(item_index, item_value){
					$("#arriving-vehicle-item").append($("<option>", {
						value: item_value.id,
						text: item_value.item
					}));
				});
			}
		});

        //$("#arriving-vehicle-item").trigger("change");

	});

	//upload photo
	$("#arriving-vehicle-dispute-inspect-item-photo").change(function(){

		if ($(this).val() != null) {
			$("#mo-arriving-vehicle-additional-detail-popup-form").submit();
		}
		$(this).val(null);

	});

	function baseName(str)
	{
	   var base = new String(str).substring(str.lastIndexOf('/') + 1); 
	    if(base.lastIndexOf(".") != -1)       
	        base = base.substring(0, base.lastIndexOf("."));
	   return base;
	}

	$(document).ajaxStart(function() {
        $(".mo-loading-image").removeClass("hidden");
    });

	//prevent defaul submit and use ajax to submit data
	$("#mo-arriving-vehicle-additional-detail-popup-form").on("submit",(function(e) {
		//prevent form submit by default
        e.preventDefault();
        var dispute_add_inspect_item_form = new FormData(this);

        $.ajax({
            type: 'POST',
            url: "/arriving-vehicle/upload-dispute-item-photo-tmp",
            data: dispute_add_inspect_item_form,
            cache: false,
            contentType: false,
            processData: false,
            success:function(res) {
            	//show photo after upload successful
            	$("#"+baseName(res).split("_")[1]).prop("src", res);

                console.log("success /arriving-vehicle/upload-dispute-item-photo-tmp");
            },
            error: function(err){
                console.log("error /arriving-vehicle/upload-dispute-item-photo-tmp", err);
            },
            complete: function() {
            	$(".mo-loading-image").addClass("hidden");
            }
        });

    }));

    //prevent defaul submit and use ajax to submit data
    $("label[for='arriving-vehicle-dispute-inspect-item-photo']").click(function(){
    	$("input[name='sequence']").val($(this).data("sequence"));
    });

    $.changeEditStatusForPopup = function(){
    	$("#arriving_vehicle_save_btn").val("Update");
	    $("#arriving_vehicle_save_btn").data("status", "edit");
	    $(".dispute-popup-title").text("Edit Issue");
    };

    $.changeSaveStatusForPopup = function(){
    	$("#arriving_vehicle_save_btn").val("Save");
	    $("#arriving_vehicle_save_btn").data("status", "create");
	    $(".dispute-popup-title").text("Create Issue");
    };

    //when item change
    $("#arriving-vehicle-item").change(function(){
    	//clear cost and note
    	$("#arriving-vehicle-item-cost").val(null);
    	$("#arriving-vehicle-dispute-note").val(null);
    	$.changeSaveStatusForPopup();

    	var dispute_inspect_items_local_storage = $.parseJSON(sessionStorage.getItem("dispute_inspect_items"));
    	console.log(dispute_inspect_items_local_storage, "dispute_inspect_items_local_storage");
    	var item_id = $("#arriving-vehicle-item").val();
    	//assign item_id to hidden field
    	$("input[name='dispute_item_id']").val(item_id);
    	var category = $("#arriving-vehicle-category").val();
    	//change cost and note
    	
    	switch (category) {
    		case "EXTERIOR_SECTION":
    			var edit_mode = false;
    			$.each(dispute_inspect_items_local_storage.exterior, function(index, value) {
    				$.each(value.items, function(items_index, items_value) {
    					if (items_value.item_id == item_id) {
    						$("#arriving-vehicle-item-cost").val(items_value.item_cost);
    						$("#arriving-vehicle-dispute-note").val(items_value.item_note);
    						
    						$.changeEditStatusForPopup();
    						edit_mode = true;
    						return false;
    					} else {
    						$.changeSaveStatusForPopup();
    					}
    				});
    				if(edit_mode) {
    					return false;
    				}
    			});
    		break;
    		case "INTERIOR_SECTION":
    			var edit_mode = false;
    			$.each(dispute_inspect_items_local_storage.interior, function(index, value) {
    				$.each(value.items, function(items_index, items_value) {
    					if (items_value.item_id == item_id) {
    						$("#arriving-vehicle-item-cost").val(items_value.item_cost);
    						$("#arriving-vehicle-dispute-note").val(items_value.item_note);

    						$.changeEditStatusForPopup();
    						edit_mode = true;
    						return false;
    					} else {
    						$.changeSaveStatusForPopup();
    					}
    				});
    				if(edit_mode) {
    					return false;
    				}
    			});

    		break;
    		case "MECHANICAL_SECTION":
    			var edit_mode = false;
    			$.each(dispute_inspect_items_local_storage.mechanical, function(index, value) {
    				$.each(value.items, function(items_index, items_value) {
    					if (items_value.item_id == item_id) {
    						$("#arriving-vehicle-item-cost").val(items_value.item_cost);
    						$("#arriving-vehicle-dispute-note").val(items_value.item_note);

    						$.changeEditStatusForPopup();
    						edit_mode = true;
    						return false;
    					} else {
    						$.changeSaveStatusForPopup();
    					}

    				});
    				if(edit_mode) {
    					return false;
    				}
    			});
    		break;
    	}
    	//change photo
        var user_id = $("input[name='user_id']").val();
    	var image_1 = "/images_tmp/dispute_inspect_item_photo_tmp/"+item_id+"_1_"+user_id+".jpg?"+Math.random();
        var image_2 = "/images_tmp/dispute_inspect_item_photo_tmp/"+item_id+"_2_"+user_id+".jpg?"+Math.random();
        var image_3 = "/images_tmp/dispute_inspect_item_photo_tmp/"+item_id+"_3_"+user_id+".jpg?"+Math.random();

        var image_upload = "/images/perform_valuation/image_upload.png";
        $("#1").error(function() {
            $("#1").attr("src", image_upload);
        }).attr("src", image_1);

        $("#2").error(function() {
            $("#2").attr("src", image_upload);
        }).attr("src", image_2);

        $("#3").error(function() {
            $("#3").attr("src", image_upload);
        }).attr("src", image_3);

    });

    //when original inspection item click
    //original
    $(".inspection-item-link").click(function(){
        //clear error msg
        $("label.error").hide();
        $(".error").removeClass("error");
    	//set read only mode
    	$("#arriving-vehicle-category").attr("disabled", true);
    	$("#arriving-vehicle-section").attr("disabled", true);
    	$("#arriving-vehicle-item").attr("disabled", true);
    	$("#arriving-vehicle-dispute-inspect-item-photo").attr("disabled", true);
    	$("#arriving-vehicle-item-cost").attr("disabled", true);
    	$("#arriving-vehicle-dispute-note").attr("disabled", true);
    	$("#arriving_vehicle_save_btn").attr("disabled", true);

    	var category = $(this).data("category");
    	var section_id = $(this).data("session-id");
    	var item_id = $(this).data("item-id");
        var item_name = $(this).data("item-name");

    	$("#arriving-vehicle-category").val(category);
    	//auto fill
    	//cost and note
	 	/* section and item */
        $.ajax({
			type: "GET",
			url: "/get-inspection-items-list/"+category,
			data: {
				_token: $("input[name='_token']").val()
			},
			success: function(response) {
				var inspection_items = $.parseJSON(response);
				//append data to section box
				//clear before add
				$("#arriving-vehicle-section").empty();
				$.each(inspection_items, function(index, value){

					//append sections
					$("#arriving-vehicle-section").append($("<option>", {
						value: value.section_id,
						text: value.section_name
					}));

					//append items
					//clear before add
					if (index == 0) {
						$("#arriving-vehicle-item").empty();
						$.each(value.items, function(item_index, item_value) {
							$("#arriving-vehicle-item").append($("<option>", {
								value: item_value.id,
								text: item_value.item
							}));
						});
					}

				});

				console.log("call /get-inspection-items-list/"+category, "sucess");

			},
			complete: function() {
				$(".mo-loading-image").addClass("hidden");
				//auto fill section and item
				$("#arriving-vehicle-section").val(section_id);
				//$("#arriving-vehicle-section").trigger("change");
        		//$("#arriving-vehicle-item").val(item_id);
                $("#arriving-vehicle-item").empty();
                $("#arriving-vehicle-item").append($("<option>", {
                    value: item_id,
                    text: item_name
                }));
			},
			error: function(error) {
				console.log("error /get-inspection-items-list/ALL", error);
			},
			global: true
		});
		/* end section and item */
    	//$("#arriving-vehicle-section").val(session_id);
    	//$("#arriving-vehicle-item").val(item_id);

    	//cost and note
    	var cost = $(this).data("cost");
    	var note = $(this).data("note");

    	$("#arriving-vehicle-item-cost").val(cost);
    	$("#arriving-vehicle-dispute-note").val(note);

    	if ($(this).hasClass("original")) {

    		var azure_account_name = $("input[name='azure_accout_name']").val();
    		var valuation_id = $("input[name='valuation_id']").val();
            var valuation_current_version = $("input[name='valuation_version']").val();
            
            var image_1 = "https://"+azure_account_name+".blob.core.windows.net/inspection-item/"+valuation_id+"_"+valuation_current_version+"_"+item_id+"_1.jpg?"+Math.random();
            var image_2 = "https://"+azure_account_name+".blob.core.windows.net/inspection-item/"+valuation_id+"_"+valuation_current_version+"_"+item_id+"_2.jpg?"+Math.random();
            var image_3 = "https://"+azure_account_name+".blob.core.windows.net/inspection-item/"+valuation_id+"_"+valuation_current_version+"_"+item_id+"_3.jpg?"+Math.random();

            var image_upload = "/images/perform_valuation/image_upload.png";
            $("#1").error(function() {
                $("#1").attr("src", image_upload);
            }).attr("src", image_1);

            $("#2").error(function() {
                $("#2").attr("src", image_upload);
            }).attr("src", image_2);

            $("#3").error(function() {
                $("#3").attr("src", image_upload);
            }).attr("src", image_3);

    	}
    });

	//when dispute inspection item click


    //submit dispute
    $("#mo-dispute-submit").click(function(e){
    	//preven double click here
    	//--
    	e.preventDefault();

    	if(!$("#mo-dispute-additional-details-contact-form").valid()) {
    		$("html, body").animate({
                scrollTop: 0
            }, 500);
    		if(!$("#mo-dispute-additional-details-comment-form").valid()) {
    			return false;
    		}
    		
    		return false;
    	}
    	if(!$("#mo-dispute-additional-details-comment-form").valid()) {
    		return false;
    	}

    	//collect [inspection_items_id]
    	var inspection_items_id = [];
    	$(".mo-dispute-inspect-id").each(function() {

    		console.log(this, "dispute toggle");
    		if ($(this).prop("checked") == true) {
    			inspection_items_id.push($(this).val());
    		}

    	});
    	console.log(inspection_items_id, "inspection_items_id");
    	//end of collect inspection_items_id

    	//collect [dispute_inspect_items] here
    	var new_dispute_id = [];
    	$(".mo-new-dispute-item").each(function() {
    		if ($(this).prop("checked") == true) {
    			new_dispute_id.push($(this).val());
    		}
    	});

    	//show message please log dispute here
    	if (new_dispute_id.length < 1 && inspection_items_id.length < 1) {
    		$(".alert-danger").removeClass("hidden");
    		return false;
    	}

    	var dispute_inspect_items = []
    	var dispute_inspect_items_local_storage = $.parseJSON(sessionStorage.getItem("dispute_inspect_items"));
    	
    	$.each(dispute_inspect_items_local_storage.exterior, function(index, value) {
    		$.each(value.items, function(items_index, items_value){
    			if($.inArray(items_value.item_id, new_dispute_id) >= 0) {
    				dispute_inspect_items.push(items_value);
    			}
    			
    		});
    	});

    	$.each(dispute_inspect_items_local_storage.interior, function(index, value) {
    		$.each(value.items, function(items_index, items_value){
    			if($.inArray(items_value.item_id, new_dispute_id) >= 0) {
    				dispute_inspect_items.push(items_value);
    			}
    		});
    	});

    	$.each(dispute_inspect_items_local_storage.mechanical, function(index, value) {
    		$.each(value.items, function(items_index, items_value){
    			if($.inArray(items_value.item_id, new_dispute_id) >= 0) {
    				dispute_inspect_items.push(items_value);
    			}
    		});
    	});

    	//dispute_inspect_items = JSON.stringify(dispute_inspect_items);
    	
    	console.log(dispute_inspect_items, "dispute_inspect_items");
    	//end of collect dispute_inspect_items_id

    	//call ajax to submit here
    	$.ajax({
            type: 'POST',
            url: "/arriving-vehicle/submit-lodge-dispute",
            data: {
            	_token: $("input[name='_token']").val(),
            	inspection_items_id: inspection_items_id,
            	dispute_inspect_items: dispute_inspect_items,
            	transaction_id: $("input[name='transaction_id']").val(),
            	dispute_contact_last_name: $("input[name='dispute_contact_last_name']").val(),
            	dispute_contact_first_name: $("input[name='dispute_contact_first_name']").val(),
            	dispute_contact_phone: $("input[name='dispute_contact_phone']").val(),
            	dispute_contact_email: $("input[name='dispute_contact_email']").val(),
            	dispute_comment: $("#arriving_vehicle_dispute_comment").val(),
                general_reconditioning: $("input[name='general_recond']").prop("checked"),
                user_id: $("input[name='user_id']").val()
            },
            success:function(res) {
            	
            	var result = $.parseJSON(res);
            	console.log(result);
            	if (typeof result.transaction_id != "undefined") {
            		window.location = "/arriving-vehicle/arriving-list";
            	}
                console.log("success /arriving-vehicle/submit-lodge-dispute");
            },
            error: function(err) {
                console.log("error /arriving-vehicle/submit-lodge-dispute", err);
            },
            complete: function() {
            	$(".mo-loading-image").addClass("hidden");
            }
        });

    });

    //int dispute item
    $.appendDisputeInspectItems = function(dispute_inspect_items) {

    	if(typeof(dispute_inspect_items) == "undefined") {
    		return false;
    	} else {

    		console.log(dispute_inspect_items, "appendDisputeInspectItems: dispute_inspect_items");

            $.addNewItem = function(dispute_items) {
    			console.log(dispute_items, "add new dispute items");
        		//item

            	if (dispute_items.items.length > 0) {
            		//clear before insert
		            $("#section-id-"+dispute_items.section_id).nextUntil(".dispute-section").not(".original").addClass("hidden");

                    $.each(dispute_items.items, function(index, value){

            			var item =  
            				"<a href='#mo-arriving-vehicle-additional-detail-popup' class='open-popup-link dispute-inspection-item'"+
                                "data-category=' '"+
                                "data-session-id='"+dispute_items.section_id+"'"+
                                "data-item-id='"+value.item_id+"'"+
                                "data-cost='"+value.item_cost+"'"+
                                "data-note='"+value.item_note+"'"+
                            ">"+
		                    	"<span class='mo-dispute-item col-xs-8 no-padding force-padding-left-40 force-font-black-color'>"+value.item_name+"</span>"+
		                    	"<span class='col-xs-3 text-align-right no-padding padding-right-80'>$"+value.item_cost+"</span>"+
		                    "</a>"+
		                    "<span class='col-xs-1 no-padding text-align-right'><input type='checkbox' checked data-toggle='toggle' class='mo-dispute-toggle mo-new-dispute-item' name='mo_new_dispute_item_id[]' value='"+value.item_id+"'></span>";
            			var item_html = 
                		"<hr class='mo-dispute-hr col-xs-12 ' />"+
		                "<div class='mo-row font-avenir-demi'>"+
		                	item+
		                "</div>";
		                
                		$("#section-id-"+dispute_items.section_id).after(item_html);

                		//calc cost
                		//internal
                		if(dispute_items.section_id == "100") {

                			var section_price = $("#internal-section-price").text();
                			section_price = parseFloat(section_price.replace(/[$,]+/g,""));
                			var dispute_price = value.item_cost;
                			dispute_price = parseFloat(dispute_price.replace(/[$,]+/g,""));
                			var new_section_price = accounting.formatMoney(section_price+dispute_price, "", 2, ",", ".");
                			$("#internal-section-price").text(new_section_price);
                			
                			//total price
                			var total_price = $("#dispute-total-price").text();
                			total_price = parseFloat(total_price.replace(/[$,]+/g,""));
                			var new_total_price = dispute_price + total_price;
                			new_total_price = accounting.formatMoney(new_total_price, "", 2, ",", ".");
                			$("#dispute-total-price").text(new_total_price);
                		} 
                		//mechanical
                		else if (dispute_items.section_id == "200") {
                			
                			var section_price = $("#mechanical-section-price").text();
                			section_price = parseFloat(section_price.replace(/[$,]+/g,""));
                			var dispute_price = value.item_cost;
                			dispute_price = parseFloat(dispute_price.replace(/[$,]+/g,""));
                			var new_section_price = accounting.formatMoney(section_price+dispute_price, "", 2, ",", ".");
                			$("#mechanical-section-price").text(new_section_price);
                			
                			//total price
                			var total_price = $("#dispute-total-price").text();
                			total_price = parseFloat(total_price.replace(/[$,]+/g,""));
                			var new_total_price = dispute_price + total_price;
                			new_total_price = accounting.formatMoney(new_total_price, "", 2, ",", ".");
                			$("#dispute-total-price").text(new_total_price);
							
                		} 
                		//external
                		else {

                			var external_section_price = $("#external-section-price").text();
                			external_section_price = parseFloat(external_section_price.replace(/[$,]+/g,""));
                			var dispute_external_price = value.item_cost;
                			dispute_external_price = parseFloat(dispute_external_price.replace(/[$,]+/g,""));
                			var new_external_section_price = accounting.formatMoney(external_section_price+dispute_external_price, "", 2, ",", ".");
                			$("#external-section-price").text(new_external_section_price);
                			
                			//total price
                			var total_price = $("#dispute-total-price").text();
                			total_price = parseFloat(total_price.replace(/[$,]+/g,""));
                			var new_total_price = dispute_external_price + total_price;
                			new_total_price = accounting.formatMoney(new_total_price, "", 2, ",", ".");
                			$("#dispute-total-price").text(new_total_price);
                		}

            		});

					//rebind
					$("[data-toggle='toggle']").bootstrapToggle('destroy');
            		$("[data-toggle='toggle']").bootstrapToggle({
		                on: '',
		                off: ''
		            });
                		
            		$("div.toggle.btn.btn-primary").css({
            			"min-width": "50px",
					    "min-height": "25px",
					    "border-color": "#8c8c8c"
            		});

            		$("div.toggle.btn.btn-default").css({
            			"min-width": "50px",
					    "min-height": "25px",
					    "border-color": "#8c8c8c"
            		});

            		$(".btn-primary").css({
            			"background-color": "#44db5e"
            		});

            		$(".btn-primary:hover").css({
            			"background-color": "#44db5e"
            		});

            		$(".btn-default:hover").css({
            			"background-color": "#FFFFFF"
            		});

            		$(".btn").css({
            			"border-radius": "15px"
            		});

            		$("span.toggle-handle").css({
            			"width": "50%"
            		});
            		
            		//rebind event open popup
            		//magnificPopup = $.magnificPopup.instance;
            		//$('.open-popup-link').unbind();
            		$('.open-popup-link').magnificPopup({
			            type:'inline',
			            showCloseBtn: false,
			            closeOnBgClick: false
		            });

		            //bind event close popup
		            //$("#arriving_vehicle_cancel_btn").click(function(){
		            //	magnificPopup.close();
		            //});

		            //bind event click for dispute inspection item
		            //unbind first
		            $(".dispute-inspection-item").unbind("click.dispute-inspection-item-event");
		            /* start prepraring edit disupute */
		            //when dispute item click
		            //dispute - edit
		            $(".dispute-inspection-item").bind("click.dispute-inspection-item-event",function(){
		            	//clear error msg
                        $("label.error").hide();
                        $(".error").removeClass("error");
                        //set read only mode
				    	$("#arriving-vehicle-category").attr("disabled", true);
				    	$("#arriving-vehicle-section").attr("disabled", true);
				    	$("#arriving-vehicle-item").attr("disabled", true);
				    	//enable upload photo
				    	$("#arriving-vehicle-dispute-inspect-item-photo").attr("disabled", false);
				    	$("#arriving-vehicle-item-cost").attr("disabled", false);
    					$("#arriving-vehicle-dispute-note").attr("disabled", false);
    					$("#arriving_vehicle_save_btn").attr("disabled", false);
    					
		            	var item_id = $(this).data("item-id");

                        var user_id = $("input[name='user_id']").val();
		            	var image_1 = "/images_tmp/dispute_inspect_item_photo_tmp/"+item_id+"_1_"+user_id+".jpg?"+Math.random();
			            var image_2 = "/images_tmp/dispute_inspect_item_photo_tmp/"+item_id+"_2_"+user_id+".jpg?"+Math.random();
			            var image_3 = "/images_tmp/dispute_inspect_item_photo_tmp/"+item_id+"_3_"+user_id+".jpg?"+Math.random();

			            var image_upload = "/images/perform_valuation/image_upload.png";
			            $("#1").error(function() {
			                $("#1").attr("src", image_upload);
			            }).attr("src", image_1);

			            $("#2").error(function() {
			                $("#2").attr("src", image_upload);
			            }).attr("src", image_2);

			            $("#3").error(function() {
			                $("#3").attr("src", image_upload);
			            }).attr("src", image_3);

			            //auto fill cost and note
			            $("#arriving-vehicle-item-cost").val($(this).data("cost"));
			            $("#arriving-vehicle-dispute-note").val($(this).data("note"));

			            //auto select category, section, item
			            var category = "EXTERIOR_SECTION";
			            if ($(this).data("session-id") == "100") {
			            	category = "INTERIOR_SECTION";
			            } else if ($(this).data("session-id") == "200") {
			            	category = "MECHANICAL_SECTION";
			            }
			            var section_id = $(this).data("session-id");
			            var item_id = $(this).data("item-id");
			            //assign item_id to hidden field
			            $("input[name='dispute_item_id']").val(item_id);
			            $("#arriving-vehicle-category").val(category);
			            
			            /* section and item */
			            $.ajax({
							type: "GET",
							url: "/get-inspection-items-list/"+category,
							data: {
								_token: $("input[name='_token']").val()
							},
							success: function(response) {
								var inspection_items = $.parseJSON(response);
								//append data to section box
								//clear before add
								$("#arriving-vehicle-section").empty();
								$.each(inspection_items, function(index, value){

									//append sections
									$("#arriving-vehicle-section").append($("<option>", {
										value: value.section_id,
										text: value.section_name
									}));

									//append items
									//clear before add
									if (index == 0) {
										$("#arriving-vehicle-item").empty();
										$.each(value.items, function(item_index, item_value) {
											$("#arriving-vehicle-item").append($("<option>", {
												value: item_value.id,
												text: item_value.item
											}));
										});
									}

								});

								console.log("call /get-inspection-items-list/"+category, "sucess");

							},
							complete: function() {
								$(".mo-loading-image").addClass("hidden");
								//auto fill section and item
								$("#arriving-vehicle-section").val(section_id);
								$("#arriving-vehicle-section").trigger("change");
			            		$("#arriving-vehicle-item").val(item_id);
                                //$("#arriving-vehicle-item").trigger("change");
							},
							error: function(error) {
								console.log("error /get-inspection-items-list/ALL", error);
							},
							global: true
						});
						/* end section and item */

			            //rename popup title, label of button
			            $("#arriving_vehicle_save_btn").val("Update");
			            $("#arriving_vehicle_save_btn").data("status", "edit");
			            $(".dispute-popup-title").text("Edit Issue");
		            });
					/* end prepraring edit dispute */
            	}
            }
            //exterior
    		if (dispute_inspect_items.exterior.length > 0) {
    			console.log("prepare append exterior", dispute_inspect_items.exterior);
                //clear cost
                $("#external-section-price").text($("input[name='external_price']").val());

    			$.each(dispute_inspect_items.exterior, function(exterior_index, exterior_value){
    				console.log(exterior_value, "prepare append exterior");
                	//section
                	console.log($(".dispute-items"), "dispute-items");
                    
                	$(".dispute-items").each(function(index){

                		if ($(this).data("section-id") == exterior_value.section_id) {
                			$.addNewItem(exterior_value);
                			return false;
                		} else if ($(this).data("section-id") != exterior_value.section_id && index == ($(".dispute-items")).length - 1) {
                            var exterior_section_html = 
	    					"<hr class='mo-dispute-hr original dispute-section'/>"+
	                		"<div class='mo-row dispute-items col-xs-12 no-padding original dispute-section' id='section-id-"+exterior_value.section_id+"' data-section-id='"+exterior_value.section_id+"'>"+
	                    		"<span class='padding-left-20 force-font-black-color font-avenir-demi' >"+exterior_value.section_name+"</span>"+
	                		"</div>";
                			$(".exterior-section").after(exterior_section_html);
                			$.addNewItem(exterior_value);
                		}

                	});	

    			});

    		}

    		//interior
    		if (dispute_inspect_items.interior.length > 0) {
    			console.log("prepare append interior", dispute_inspect_items.interior);
    			//append item
                //clear cost
                $("#internal-section-price").text($("input[name='internal_price']").val());
                
    			$.each(dispute_inspect_items.interior, function(interior_index, interior_value) {
                	//section
                	$(".dispute-items").each(function(index){

                		if ($(this).data("section-id") == interior_value.section_id) {
                			$.addNewItem(interior_value);
                			return false;
                		}

                	});	

    			});
    		}

    		//mechanical
    		if (dispute_inspect_items.mechanical.length > 0) {
    			console.log("prepare append mechanical", dispute_inspect_items.mechanical);
    			//append item
                //clear cost
                
                $("#mechanical-section-price").text($("input[name='mechanical_price']").val());
    			$.each(dispute_inspect_items.mechanical, function(mechanical_index, mechanical_value) {
                	//section
                	$(".dispute-items").each(function(index){

                		if ($(this).data("section-id") == mechanical_value.section_id) {
                			$.addNewItem(mechanical_value);
                			return false;
                		}

                	});	

    			});
    		}
    	}
    }

    if (sessionStorage.getItem("dispute_inspect_items") != null) {
    	var dispute_inspect_items_local_storage = $.parseJSON(sessionStorage.getItem("dispute_inspect_items"));

    	$.appendDisputeInspectItems(dispute_inspect_items_local_storage);
    } else {
    	var dispute_inspect_items = {
				"exterior": [],
				"interior": [],
				"mechanical": []
			};
		sessionStorage.setItem("dispute_inspect_items", JSON.stringify(dispute_inspect_items));	
    }

    //dispute
    $(".new-dispute-inspection").click(function() {
    	//remove read only mode
    	$("#arriving-vehicle-category").attr("disabled", false);
    	$("#arriving-vehicle-section").attr("disabled", false);
    	$("#arriving-vehicle-item").attr("disabled", false);
    	$("#arriving-vehicle-dispute-inspect-item-photo").attr("disabled", false);
    	$("#arriving-vehicle-item-cost").attr("disabled", false);
    	$("#arriving-vehicle-dispute-note").attr("disabled", false);
    	$("#arriving_vehicle_save_btn").attr("disabled", false);

    	$("#arriving_vehicle_save_btn").data("status", "create");
    	$("#arriving_vehicle_save_btn").val("Save");
    	$(".dispute-popup-title").text("New Issue");
    	$("#arriving-vehicle-category").trigger("change");
    	$("#arriving-vehicle-item").trigger("change");

        $("label.error").hide();
        $(".error").removeClass("error");

    });

    var magnificPopup = $.magnificPopup.instance;

    $("#arriving_vehicle_save_btn").click(function(e) {

    	if (!$("#mo-arriving-vehicle-additional-detail-popup-form").valid()) {
    		return false;
    	}

    	var category = $("#arriving-vehicle-category").val();
    	var section_id = $("#arriving-vehicle-section").val();
    	var section_name = $("#arriving-vehicle-section option:selected").text();
    	var item_id = $("#arriving-vehicle-item").val();
    	var item_name = $("#arriving-vehicle-item option:selected").text();
    	var cost = $("#arriving-vehicle-item-cost").val();
    	var note = $("#arriving-vehicle-dispute-note").val();

    	/* start - edit dispute inspect item */
    	if ($(this).data("status") == "edit") {
    		
    		var dispute_inspect_items_local_storage = $.parseJSON(sessionStorage.getItem("dispute_inspect_items"));
    		console.log(dispute_inspect_items_local_storage, "dispute_inspect_items_local_storage");
    		
    		//update dispute item
    		switch ($("#arriving-vehicle-category").val()) {
    			case "EXTERIOR_SECTION" : 
    				$.each(dispute_inspect_items_local_storage.exterior, function(index, value){
    					$.each(value.items, function(items_index, items_value){
    						if (items_value.item_id == item_id) {

                                //remove old price here
                                var external_section_price = $("#external-section-price").text();
                                external_section_price = parseFloat(external_section_price.replace(/[$,]+/g,""));

                                var dispute_external_price = dispute_inspect_items_local_storage.exterior[index].items[items_index].item_cost;
                                dispute_external_price = parseFloat(dispute_external_price.replace(/[$,]+/g,""));

                                var new_external_section_price = accounting.formatMoney(external_section_price-dispute_external_price, "", 2, ",", ".");
                                $("#external-section-price").text(new_external_section_price);
                                
                                //total price
                                var total_price = $("#dispute-total-price").text();
                                total_price = parseFloat(total_price.replace(/[$,]+/g,""));
                                var new_total_price = total_price - dispute_external_price;
                                new_total_price = accounting.formatMoney(new_total_price, "", 2, ",", ".");
                                $("#dispute-total-price").text(new_total_price);
                                //end remove old price

    							dispute_inspect_items_local_storage.exterior[index].items[items_index].item_cost = cost;
    							dispute_inspect_items_local_storage.exterior[index].items[items_index].item_note = note;

    						}
    					});
    				});
                    
    				$.appendDisputeInspectItems(dispute_inspect_items_local_storage);
    				//window.location.reload();
    				sessionStorage.setItem("dispute_inspect_items", JSON.stringify(dispute_inspect_items_local_storage));
    				magnificPopup.close();
    				break;
    			case "INTERIOR_SECTION" :
    				$.each(dispute_inspect_items_local_storage.interior, function(index, value){
    					$.each(value.items, function(items_index, items_value){
    						if (items_value.item_id == item_id) {
                                //remove old price here
                                var section_price = $("#internal-section-price").text();
                                section_price = parseFloat(section_price.replace(/[$,]+/g,""));
                                var dispute_price = dispute_inspect_items_local_storage.interior[index].items[items_index].item_cost;
                                dispute_price = parseFloat(dispute_price.replace(/[$,]+/g,""));
                                var new_section_price = accounting.formatMoney(section_price-dispute_price, "", 2, ",", ".");
                                $("#internal-section-price").text(new_section_price);
                                
                                //total price
                                var total_price = $("#dispute-total-price").text();
                                total_price = parseFloat(total_price.replace(/[$,]+/g,""));
                                var new_total_price = total_price - dispute_price;
                                new_total_price = accounting.formatMoney(new_total_price, "", 2, ",", ".");
                                $("#dispute-total-price").text(new_total_price);
                                //end remove old price

    							dispute_inspect_items_local_storage.interior[index].items[items_index].item_cost = cost;
    							dispute_inspect_items_local_storage.interior[index].items[items_index].item_note = note;
    						}
    					});
    				});
                    

    				$.appendDisputeInspectItems(dispute_inspect_items_local_storage);
    				sessionStorage.setItem("dispute_inspect_items", JSON.stringify(dispute_inspect_items_local_storage));
    				//window.location.reload();
    				magnificPopup.close();
    				break;
    			case "MECHANICAL_SECTION" :
    				$.each(dispute_inspect_items_local_storage.mechanical, function(index, value){
    					$.each(value.items, function(items_index, items_value){
    						if (items_value.item_id == item_id) {
    							//remove old price here
                                var section_price = $("#mechanical-section-price").text();
                                section_price = parseFloat(section_price.replace(/[$,]+/g,""));
                                var dispute_price = dispute_inspect_items_local_storage.mechanical[index].items[items_index].item_cost;
                                dispute_price = parseFloat(dispute_price.replace(/[$,]+/g,""));
                                var new_section_price = accounting.formatMoney(section_price-dispute_price, "", 2, ",", ".");
                                $("#mechanical-section-price").text(new_section_price);
                                
                                //total price
                                var total_price = $("#dispute-total-price").text();
                                total_price = parseFloat(total_price.replace(/[$,]+/g,""));
                                var new_total_price = total_price - dispute_price;
                                new_total_price = accounting.formatMoney(new_total_price, "", 2, ",", ".");
                                $("#dispute-total-price").text(new_total_price);
                                //end remove old price

                                dispute_inspect_items_local_storage.mechanical[index].items[items_index].item_cost = cost;
    							dispute_inspect_items_local_storage.mechanical[index].items[items_index].item_note = note;
    						}
    					});
    				});

    				$.appendDisputeInspectItems(dispute_inspect_items_local_storage);
    				sessionStorage.setItem("dispute_inspect_items", JSON.stringify(dispute_inspect_items_local_storage));
    				//window.location.reload();
    				magnificPopup.close();

    				break;	
    		}

    		return false;
    	}
    	/* start - edit dispute inspect item */

    	/* start - create new dispute inspect item */
    	$.createDisputeInspectItems = function(array){
    		if(!Array.isArray(array)) {
    			return false;
    		} else {
    			if(array.length < 1) {
    				array.push(
	    				{
	    					section_id: section_id,
	    					section_name: section_name,
	    					items: [{
	    						item_id: item_id,
	    						item_name: item_name,
	    						item_cost: cost,
	    						item_note: note
	    					}]
	    				}
    				);
    				console.log("add dispute section and item");
    			} else {
    				$.each(array, function(index, value){
    					
    					if (value.section_id === section_id) {
    						console.log("same section_id");
    						if (value.items.length < 1) {
	    						value.items.push({
		    						item_id: item_id,
		    						item_name: item_name,
		    						item_cost: cost,
		    						item_note: note
	    						});
    							console.log("init dispute item");
    						} else {
    							$.each(value.items, function(items_index, items_value) {
    								if (items_value.item_id == item_id) {
    									items_value.item_cost = cost;
    									items_value.item_note = note;
    									console.log("reupdate item");
    								}
    								else if (items_value != item_id && items_index == value.items.length - 1) {
    									value.items.push({
				    						item_id: item_id,
				    						item_name: item_name,
				    						item_cost: cost,
				    						item_note: note
	    								});
    									console.log("add new dispute item");
    								}
    							});
    						}
    					} else if (index == array.length - 1 && value.section_id != section_id) {
    						console.log("add new dispute section and item");
    						array.push(
			    				{
			    					section_id: section_id,
			    					section_name: section_name,
			    					items: [{
			    						item_id: item_id,
			    						item_name: item_name,
			    						item_cost: cost,
			    						item_note: note
			    					}]
	    						}
		    				);
    					}

    				});
    			}

    		}
    		return array;
    	}

    	var dispute_inspect_items = null;
		if (sessionStorage.getItem("dispute_inspect_items") != null) {
	    	dispute_inspect_items = $.parseJSON(sessionStorage.getItem("dispute_inspect_items"));
	    } else {
	    	//init dispute_inspect_items
		    dispute_inspect_items = {
				"exterior": [],
				"interior": [],
				"mechanical": []
			};
	    }

    	switch (category) {
    		case "EXTERIOR_SECTION":
    			dispute_inspect_items.exterior = $.createDisputeInspectItems(dispute_inspect_items.exterior);

                $.appendDisputeInspectItems(dispute_inspect_items);
    			//window.location.reload();
    			magnificPopup.close();
    			break;
    		case "INTERIOR_SECTION":
    			dispute_inspect_items.interior = $.createDisputeInspectItems(dispute_inspect_items.interior);
    			$.appendDisputeInspectItems(dispute_inspect_items);
                //window.location.reload();
    			magnificPopup.close();
    			break;
    		case "MECHANICAL_SECTION":
    			dispute_inspect_items.mechanical = $.createDisputeInspectItems(dispute_inspect_items.mechanical);
    			$.appendDisputeInspectItems(dispute_inspect_items);
                //window.location.reload();
    			magnificPopup.close();
    			break;	
    	}
    	/* end - create new dispute inspect item */

    	console.log(dispute_inspect_items, "dispute_inspect_items");
    	sessionStorage.setItem("dispute_inspect_items", JSON.stringify(dispute_inspect_items));
    	console.log(sessionStorage.getItem("dispute_inspect_items"));

    	//var dispute_inspect_items_local_storage = $.parseJSON(sessionStorage.getItem("dispute_inspect_items"));
    	//console.log(dispute_inspect_items_local_storage, "dispute_inspect_items_local_storage");
    	//$.appendDisputeInspectItems(dispute_inspect_items_local_storage);

    });

	$("#arriving_vehicle_cancel_btn").bind("click.closepopup",function(){
		magnificPopup.close();
	});
    
});