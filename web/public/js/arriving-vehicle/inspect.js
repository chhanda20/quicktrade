$(function() {
    var version = $('#version').val();
    var valuationId = $('#valuationId').val();
    var transactionId = $('#transactionId').val();

    getInspectionItems(valuationId, version);

    var vehicleId = $('#vehicleId').val();

    $(".mo-block").first().toggleClass("mo-block-active");
    $("#inspect-item").removeClass("hidden").fadeIn("slow");
    $(".mo-block").click(function() {
        $(".mo-block").removeClass("mo-block-active");
        $(this).toggleClass("mo-block-active");
        $(".mo-block-list").hide();

        var tab = $(this).parents('.tab').attr('tab');
        $("#" + tab).hide();
        $("#" + tab).removeClass("hidden").fadeIn("slow");

        if (tab == 'inspect-item') {
            getInspectionItems(valuationId, version);
        } else {
            $.ajax({
                type: "GET",
                url: base_url+'/arriving-vehicle/option-extra/' + vehicleId,
                success: function(res) {
                    $("#option-extra").html(res);
                }
            });
        }
    });

    $('#accept_btn').click(function() {
        $.ajax({
            type: "GET",
            url: '/arriving-vehicle/accept/' + transactionId,
            success: function(res) {
                if (res) {
                    window.location = '/arriving-vehicle/arriving-list';
                }
            }
        });
    });
});

function getInspectionItems(valuationId, version) {
    $.ajax({
        type: "GET",
        url: '/arriving-vehicle/inspect-item/' + valuationId + '/'+ version,
        success: function(res) {
            $("#inspect-item").html(res);
        }
    });
}

function clearLodgeDisputeData() {
    var dispute_inspect_items = {
                "exterior": [],
                "interior": [],
                "mechanical": []
            };
    sessionStorage.setItem("dispute_inspect_items", JSON.stringify(dispute_inspect_items));
    
    
    $.ajax({
        type: "GET",
        url: "/arriving-vehicle/clear-dispute-item-photo-tmp/"+$("#userId").val(),
        success: function(res) {
            console.log(res, "/arriving-vehicle/clear-dispute-item-photo-tmp");
        },
        error: function(err) {
            console.log(err, "/arriving-vehicle/clear-dispute-item-photo-tmp, cannot clear folder cache");
        }
    });
    
}

$(".log-btn").click(function(e){
    //e.preventDefault();
    clearLodgeDisputeData();
});