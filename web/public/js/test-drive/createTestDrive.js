$("#test-drive-driver-licences-photo").change(function(){
	if ($(this).val() != null) {
		$("#mo-test-drive-add-vehicle-form").submit();
	}
	$(this).val(null);
});

$("#mo-back-license").click(function(){
	$("input[name='license_section']").val("license_back_");
});

$("#mo-front-license").click(function(){
	$("input[name='license_section']").val("license_front_");
});

$(document).ready(function() {
	var azure_account_name = $("input[name='azure-accout-name']").val();
	var customer_id = $("input[name='customer_id']").val();
    var front_driver_license = "https://"+azure_account_name+".blob.core.windows.net/user-avatar/license_front_"+customer_id+".jpg?"+Math.random();
    var back_driver_license = "https://"+azure_account_name+".blob.core.windows.net/user-avatar/license_back_"+customer_id+".jpg?"+Math.random();
    var image_upload = null;
	$("#license_front").error(function() {
	    $("#license_front").addClass("hidden");
	    $("#license_front_err_msg").removeClass("hidden");
	}).attr("src", front_driver_license);

	$("#license_back").error(function() {
		//show err image here
	    $("#license_back").addClass("hidden");
	    $("#license_back_err_msg").removeClass("hidden");
	}).attr("src", back_driver_license);
});

function baseName(str)
{
   var base = new String(str).substring(str.lastIndexOf('/') + 1); 
   if(base.lastIndexOf(".") != -1)       
        base = base.substring(0, base.lastIndexOf("."));
   return base;
}


$("#mo-test-drives-create-new").click(function() {

	if ($("#mo-test-drive-add-vehicle-form").valid()) {
		$("#mo-test-drive-add-vehicle-form").unbind();
	} else {
		return false;
	}

});


$("#mo-test-drive-add-vehicle-form").on("submit", function(e){
	e.preventDefault();
	e.stopImmediatePropagation();
	var formData = new FormData(this);
	var azure_account_name = $("input[name='azure-accout-name']").val();
	var customer_id = $("input[name='customer_id']").val();
    var front_driver_license = "https://"+azure_account_name+".blob.core.windows.net/user-avatar/license_front_"+customer_id+Math.random();
    var back_driver_license = "https://"+azure_account_name+".blob.core.windows.net/user-avatar/license_back_"+customer_id+Math.random();
	
	$(document).ajaxStart(function() {
        $(".mo-loading-image").removeClass("hidden");
        //return false;
    });
    
	$.ajax({

	    type: 'POST',
	    url: "/test-drives/upload-drive-license-photo",
	    data: formData,
	    
	    cache:false,
	    contentType: false,
	    processData: false,
	    
	    success:function(res) {
	        console.log(res);

	        uploaded_image = baseName(res).split("_")[0] +"_"+ baseName(res).split("_")[1];
	        $("#"+uploaded_image).attr("src", res+"?"+Math.random());
	        $("#"+uploaded_image).removeClass("hidden");
	    	$("#"+uploaded_image+"_err_msg").addClass("hidden");

	        console.log(res, "/test-drives/upload-drive-license-photo");
	        return false;
	    },
	    
	    error: function(err){
	        console.log(err, "error /test-drives/upload-drive-license-photo");
	    },

	    complete: function() {
	    	$(".mo-loading-image").addClass("hidden");
	    }
	    
	});

	//return false;

});