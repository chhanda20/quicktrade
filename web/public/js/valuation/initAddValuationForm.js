$(document).ready(function(){
    /** ENTER VIN **/
    var magnificPopup = $.magnificPopup.instance;
    $('.mo-enter-vin-popup').magnificPopup({
        type:'inline',
        showCloseBtn: false,
        closeOnBgClick: false,
        callbacks: {
            beforeClose: function(){
                //enterVinValidator.resetForm();
            }
        }
    });
    /** END ENTER VIN **/

    /** DISABLE FIELDS STEP BY STEP **/
    if(!$("#make").val()) {
        $("#model").prop("disabled", true);
    }
    if(!$("#model").val()) {
        $("#variant").prop("disabled", true);
    }
    if(!$("#year").val()) {
        $("#build-date").prop("disabled", true);
    }

    if(!$("#interested-make").val()) {
        $("#interested-model").prop("disabled", true);
    }
    if(!$("#interested-model").val()) {
        $("#interested-variant").prop("disabled", true);
    }

    var token = $("input[name='_token']").val();

    /***** YEAR CHANGES *****/
    $("select#year").on('change', function(){
        /** GET MAKE LIST - BY YEAR **/
        $.ajax({
            type: 'POST',
            data: {_token: token, year: $("#year").val()},
            url: "/get-glasses-data/make",
            success: function (data) {
                console.log('success: ' + data);
                if($("#make").val() == ''){
                    $("#make").find("option:gt(0)").remove();
                }
                $.each(data, function(key, value) {
                    $('#make')
                        .append($("<option></option>")
                            .attr("value",value)
                            .text(value));
                });
                /*$("#model").find("option:gt(0)").remove();
                 $("#variant").find("option:gt(0)").remove();
                 $("#build-date").val("");*/
                //disableMonthPicker(data);
                /** ENABLE BUILD DATE **/
                /*$("#build-date").prop("disabled", false);
                 triggerBuildDate();*/
            },
            error: function (data) {
                console.log('error: ' + data);
            }
        });
        if($("#make").val() && $("#model").val() && $("#variant").val()){
            filterMonthList();
        }
        /** GET MONTH LIST **/
        /*
         if(!$("#build-date").attr("disabled")){
         var years = $("#year").val();
         $('#build-date-picker').datepicker('setDate', new Date(years, 0, 24));
         $('#build-date-picker').datepicker('update');
         $('#build-date-picker').val('');
         }
         */
    });

    /**** MAKE CHANGE ****/
    /** GET MODEL LIST **/
    $("select#make").on('change', function(){
        $.ajax({
            type: 'POST',
            data: {_token: token, year: $("#year").val(), make: $("#make").val()},
            url: "/get-glasses-data/family",
            success:function(data) {
                console.log('success: '+data);
                $("#model").find("option:gt(0)").remove();
                $.each(data, function(key, value) {
                    $('#model')
                        .append($("<option></option>")
                            .attr("value",value)
                            .text(value));
                });
                /** ENABLE MODEL **/
                $("#model").prop("disabled", false);
                $("#variant").find("option:gt(0)").remove();
                $("#build-date").val("");
                filterYearList();
            },
            error: function(data){
                console.log('error: '+data);
            }
        });
    });

    /** GET INTERESTED MODEL LIST **/
    $("select#interested-make").on('change', function(){
        $.ajax({
            type: 'POST',
            data: {_token: token, make: $("#interested-make").val()},
            url: "/get-glasses-data/family",
            success:function(data) {
                console.log('success: '+data);
                $("#interested-model").find("option:gt(0)").remove();
                $.each(data, function(key, value) {
                    $('#interested-model')
                        .append($("<option></option>")
                            .attr("value",value)
                            .text(value));
                });
                /** ENABLE INTERESTED MODEL **/
                $("#interested-model").prop("disabled", false);
                $("#interested-variant").find("option:gt(0)").remove();
            },
            error: function(data){
                console.log('error: '+data);
            }
        });
    });

    function filterYearList(){
        $.ajax({
            type: 'POST',
            data: {_token: token, make: $("#make").val(), model: $("#model").val(), variant: $("#variant").val()},
            url: "/get-glasses-data/release_year",
            success:function(data) {
                console.log('success: '+data);
                var result = data.sort(function(a,b){
                    return b-a;
                });
                console.log(result);
                var current_selected_year = $("#year").val();
                var selected = "";
                $("#year").find("option:gt(0)").remove();
                $("#year").removeProp("selected");
                $.each(result, function(key, value) {
                    if(current_selected_year == value){
                        selected = "selected";
                    }else{
                        selected = "";
                    }
                    $('#year')
                        .append($("<option></option>")
                            .attr("value",value)
                            .prop("selected",selected)
                            .text(value));
                });
            },
            error: function(data){
                console.log('error: '+data);
            }
        });
    }

    function filterMonthList(){
        $.ajax({
            type: 'POST',
            data: {_token: token, year: $("#year").val(), make: $("#make").val(), model: $("#model").val(), variant: $("#variant").val()},
            url: "/get-glasses-data/release_month",
            success:function(data) {
                console.log('success: '+data);
                if($("#year").val() != ''){
                    disableMonthPicker(data);
                    /** ENABLE BUILD DATE **/
                    $("#build-date").prop("disabled", false);
                }
            },
            error: function(data){
                console.log('error: '+data);
            }
        });
    }

    /**** MODEL CHANGE ****/
    /** GET VARIANT LIST **/
    $("select#model").on('change', function(){
        $.ajax({
            type: 'POST',
            data: {_token: token, year: $("#year").val(), make: $("#make").val(), model: $("#model").val()},
            url: "/get-glasses-data/variant",
            success:function(data) {
                console.log('success: '+data);
                $("#variant").find("option:gt(0)").remove();
                $.each(data, function(key, value) {
                    $('#variant')
                        .append($("<option></option>")
                            .attr("value",value)
                            .text(value));
                });
                /** ENABLE VARIANT **/
                $("#variant").prop("disabled", false);
                $("#build-date").val("");
            },
            error: function(data){
                console.log('error: '+data);
            }
        });
        filterYearList();
    });

    /** GET INTERESTED VARIANT LIST **/
    $("select#interested-model").on('change', function(){
        $.ajax({
            type: 'POST',
            data: {_token: token, make: $("#interested-make").val(), model: $("#interested-model").val()},
            url: "/get-glasses-data/variant",
            success:function(data) {
                console.log('success: '+data);
                $("#interested-variant").find("option:gt(0)").remove();
                $.each(data, function(key, value) {
                    $('#interested-variant')
                        .append($("<option></option>")
                            .attr("value",value)
                            .text(value));
                });
                /** ENABLE VARIANT **/
                $("#interested-variant").prop("disabled", false);
            },
            error: function(data){
                console.log('error: '+data);
            }
        });
    });

    /** GET YEAR LIST - WITHOUT VARIANT **/
    /*$("select#model").on('change', function(){
     $.ajax({
     type:'GET',
     url: "/get-release-year-without-variant/"+$("select#make option:selected").text()+"/"+$("select#model option:selected").text(),
     success:function(data) {
     console.log('success: '+data);
     $("#year").find("option:gt(0)").remove();
     $.each(data, function(key, value) {
     $('#year')
     .append($("<option></option>")
     .attr("value",value)
     .text(value));
     });
     },
     error: function(data){
     console.log('error: '+data);
     }
     });
     });*/

    /***** VARIANT CHANGES *****/
    /** GET BUILD DATE - MONTH LIST **/
    $("select#variant").on('change', function(){
        filterYearList();
        filterMonthList();
    });



    /** VALIDATE CREATE VALUATION FORM **/
    $('#create-valuation-form').validate({
        rules:{
            year: "required",
            make: "required",
            model: "required",
            colour: "required",
            odometer: "required",
            variant: "required",
            interested_in_vehicle: "required",
            finance_owing: "required",
            build_date: "required",
            compliance_date: "required",
            registration_expiry_date: 'required',
            ownership_type: 'required'

        },
        errorElement: 'div',
        errorClass: 'mo-error-wrapper',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else if(element.parents("#mo-ownership-type").length){
                error.appendTo(element.parents("#mo-ownership-type"));
            } else {
                error.insertAfter(element);
            }
        },
        messages: {
            year: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter year</span>"+"</div>",
            make: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter make</span>"+"</div>",
            model: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter model</span>"+"</div>",
            colour: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter colour</span>"+"</div>",
            odometer: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter odometer</span>"+"</div>",
            variant: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter variant</span>"+"</div>",
            interested_in_vehicle: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter interested vehicle</span>"+"</div>",
            finance_owing: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter finance owing</span>"+"</div>",
            build_date: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter build date</span>"+"</div>",
            compliance_date: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter compliance date</span>"+"</div>",
            registration_expiry_date: "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please enter registration expiry date</span>"+"</div>",
            ownership_type: "<div class='mo-error-wrapper padding-left-20'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>Please choose ownership type</span>"+"</div>"
        }
    });

    /** CHECK VIN **/
    $("#create-valuation-button").click(function() {
        if($(this).data("permissions") == "valuation_perform") {
            var year = $("#year").val();
            var vin_number = $("#vin").val();
            var registration_number = $("#registration-number").val();

            $("input[name='year']").val(year);
            $("input[name='vin']").val(vin_number);
            $("input[name='registration-number']").val(registration_number);

            if ($('#create-valuation-form').valid()) {

                $.magnificPopup.open({
                    items: {
                        src: '#mo-enter-vin-popup'
                    },
                    type: 'inline'
                });

            }
            return false;
        }

    });

    $("#mo-request-btn").click(function(){
        if(!$("#create-valuation-form").valid()){
            return false;
        }
        $("#create-valuation-form").on("submit", function(){
            $(".mo-loading-image").removeClass("hidden");
        });

    });

    /** ENTER VIN **/
    $("input[name='vin']").change(function(){
        $("#vin").val($(this).val());
    });

    $("input[name='registration-number']").change(function(){
        $("#registration-number").val($(this).val());
    });

    $("#vin-confirm").click(function (){
        $("input[name='year']").rules('add', {
            required: true
        });
        $("input[name='vin']").rules('add', {
            required: true
        });
        $("input[name='registration-number']").rules('add', {
            required: true
        });

        if($("#mo-popup-enter-vin").valid()) {
            $(".mo-loading-image").removeClass("hidden");
            $("form#create-valuation-form").submit();
        }

    });

    var valuation_id = null;
    var year = null;
    var vin_number = null;
    var registration_number = null

    $(".mo-accept-valuate-btn").click(function (){

        valuation_id = $(".mo-accept-valuate-btn").data("valuation-id");
        year = $(this).parent().parent().parent().children(".year").text();
        vin_number = $(this).parent().parent().parent().children(".vin-number").text();
        registration_number = $(this).parent().parent().parent().children(".registration").text();

        $("input[name='year']").val(year);
        $("input[name='vin']").val(vin_number);
        $("input[name='registration-number']").val(registration_number);

    });

    $("#get-vin").click(function(e){

        $("input[name='year']").rules('add', {
            required: true
        });
        $("input[name='vin']").rules('add', {
            required: true
        });
        $("input[name='registration-number']").rules('remove');


        if($("#mo-popup-enter-vin").valid()) {

            var vin = $("input[name='vin']").val();
            var token = $("input[name='_token']").val();
            var year_number = $("input[name='year']").val();

            if (vin.length > 0 && year_number != null)
            {
                $.ajax( {
                    url:'/search-vin',


                    type: 'POST',
                    data: {_token: token, vin_number: vin, year: year_number},

                    success:function(data) {

                        response = jQuery.parseJSON(data);
                        if (data.length > 0 ) {
                            console.log(response);
                            $(".vin-number").text(response.vin);
                            var content = "";

                            if (response.make != null) {
                                content += +" "+ response.make;
                            }
                            if (response.model_family != null) {
                                content += +" "+ response.model_family;
                            }
                            if (response.build_date != null) {
                                content += +" "+ response.build_date;
                            }
                            if (response.variant != null) {
                                content += +" "+ response.variant;
                            }
                            if (response.series != null) {
                                content += +" "+ response.series;
                            }
                            if (response.colour != null) {
                                content += +" "+ response.colour;
                            }
                            if (response.manufacturer_color != null) {
                                content += +" "+ response.manufacturer_color;
                            }
                            if (response.body_type != null) {
                                content += +" "+ response.body_type;
                            }
                            if (response.registered_state != null) {
                                content += +" "+ response.registered_state;
                            }
                            if (response.registration_number != null) {
                                content += +" "+ response.registration_number;
                            }
                            if (response.compliance_date != null) {
                                content += +" "+ response.compliance_date;
                            }
                            $(".note-content").text(
                                content
                            );

                            $("#vin-confirm").prop("disabled", false);


                        }

                    },
                    error: function(xhr) {

                    }
                });
            }

        }
    });

    // $("#mo-popup-enter-vin").on("submit", function(){
    //     return false;
    // });

    var enterVinValidator = $("#mo-popup-enter-vin").validate({
        rules: {
            //"year" : "required",
            //"vin" : "required",
            //"registration-number" : "required"
        },
        messages: {
            "year": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
            "vin": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>",
            "registration-number": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>"+ "<span>This field cannot be empty.</span>"+"</div>"
        }
    });
    /** ENTER VIN **/
    /** END CHECK VIN **/

    $(".mo-cancel-btn").click(function() {
        $.magnificPopup.close();
    });

    $("#registration-expiry-date-picker").datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        orientation: 'bottom'
        // autoclose: true

    });

    /*$("#year").change(function(){
     //var date = $("#build-date-picker").datepicker("getDate");
     //var month = date.getMonth() + 1;
     var years = $("#year").val();
     //$("#build-date").val(month + "-" + years);
     $('#build-date-picker').datepicker('setDate', new Date(years, 11, 24));
     $('#build-date-picker').datepicker('update');
     $('#build-date-picker').val('');
     });*/

    var buildDate = $("#build-date-picker").datepicker({
        format: 'mm-yyyy',
        minViewMode: 'months',
        maxViewMode: 'months',
        orientation: 'bottom'
    });

    function disableMonthPicker(monthArrayIndex){
        //var datesToDisable = $('#datepicker1').data("datesDisabled").split(',');
        // or...
        /*console.log(monthArrayIndex);*/
        var monthNames =
            ["","Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        var datesToEnable = [];
        for(var i =0; i<monthArrayIndex.length; i++){
            datesToEnable.push(monthNames[monthArrayIndex[i]]);
        }
        console.log("datesToEnable "+datesToEnable);
        var buildDate = $("#build-date-picker").datepicker({
            format: 'mm-yyyy',
            minViewMode: 'months',
            maxViewMode: 'months',
            orientation: 'bottom'

        }).on("show", function(event) {
            event.preventDefault();
            //console.log(event);
            $(".month").each(function(index, element) {
                var el = $(element);
                //console.log(el.text()+' | '+datesToEnable.indexOf(el.text()));
                el.removeClass('disabled');
                if(datesToEnable.indexOf(el.text()) === -1){
                    el.addClass('disabled');
                }

                /*var hideMonth = $.grep( datesToEnable, function( n, i ) {
                 return n.substr(0, 3) == el.text();
                 });

                 el.removeClass('disabled');

                 if (hideMonth.length)
                 el.addClass('disabled');*/

                /* To hide those months completely...
                 if (hideMonth.length)
                 el.hide();
                 */
            });
        });

        var years = $("#year").val();
        //$('#build-date-picker').datepicker('clearDates');
        $('#build-date-picker').datepicker('setDate', new Date(years, monthArrayIndex[0]-1, 24));
        $('#build-date-picker').datepicker('update');
        //$('#build-date-picker').val('');
    }

    $("#compliance-date-picker").datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        orientation: 'bottom'
        // autoclose: true
    });

    $("#odometer").keyup(function(){
        var $this = $(this);
        $this.val($this.val().replace(/[^\d]/g, ''));
    });

    // $("#vin").keyup(function(){
    //     this.value = this.value.toLocaleUpperCase();
    // });

//            $('.glyphy').click(function (e) {
//                if ($(e.target).is('input')) {
//                    $(this).find('.glyphicon').toggleClass('glyphicon-check glyphicon-unchecked');
//                    console.log($(this).find('input').is(':checked'));
//                }
//            });

    /** VIN CHANGE **/
    $("#year").change(function(){
        $("#get_vin_number").parent().removeClass("hide");
        $(".mo-vin-checked").addClass("hide");
    });

    $("#vin").change(function(){
        $("#get_vin_number").parent().removeClass("hide");
        $(".mo-vin-checked").addClass("hide");
    });

    /** END VIN CHANGE **/

    $("#get_vin_number").click(function(){
        var vin = $("#vin").val();
        var token = $("input[name='_token']").val();
        var year_number = $("#year").val();

        if (vin.length > 0 && year_number != null)
        {
            $("#get_vin_number").parent().addClass("hide");
            $(".mo-btn-loading-2").parent().removeClass("hide");

            $.ajax( {
                url:'/search-vin',
                type: 'POST',
                data: {_token: token, vin_number: vin, year: year_number},
                success:function(data) {
                    try
                    {
                        response = jQuery.parseJSON(data);
                    }
                    catch(err)
                    {
                        response = new Object();
                        response.error = "error";
                    }
                    console.log(response);
                    if (response.hasOwnProperty("error")) {
                        $(".mo-btn-loading-2").parent().addClass("hide");
                        $("#get_vin_number").parent().removeClass("hide");
                        return false;
                    }

                    if (data.length > 0) {


                        $(".mo-btn-loading-2").parent().addClass("hide");
                        $(".mo-vin-checked").removeClass("hide");

                        //auto fill build_date
                        var build_date = new Date(response.build_date);
                        var buildDateFormatted =

                            ("0" + (build_date.getUTCMonth()+1)).slice(-2) + "-" +
                            build_date.getUTCFullYear();

                        console.log(buildDateFormatted);
                        $("#build-date").val(buildDateFormatted);
                        //end auto fill build_date

                        //auto fill color
                        $("#colour").val(response.colour);
                        //end auto fill color

                        //auto fill registration date
                        var registration_expiry_date = new Date(response.registration_expiry_date);
                        console.log("registration_expiry_date: " +registration_expiry_date);
                        var registrationDateFormatted =

                            ("0" + registration_expiry_date.getUTCDate()).slice(-2) + "-" +
                            ("0" + (registration_expiry_date.getUTCMonth()+1)).slice(-2) + "-" +
                            registration_expiry_date.getUTCFullYear();



                        $('#registration-expiry-date').datepicker('setDate', registration_expiry_date);

                        $('#registration-expiry-date').datepicker('update');
                        $("#registration-expiry-date").val(registrationDateFormatted);
                        //end auto fill registration date

                        //auto fill compliance date
                        var compliance_date = new Date(response.compliance_date);
                        var complianceDateFormatted =

                            ("0" + compliance_date.getUTCDate()).slice(-2) + "-" +
                            ("0" + (compliance_date.getUTCMonth()+1)).slice(-2) + "-" +
                            compliance_date.getUTCFullYear();

                        $('#compliance-date').datepicker('setDate', compliance_date);
                        $('#compliance-date').datepicker('update');
                        $("#compliance-date").val(complianceDateFormatted);
                        //$('#build-date-picker').val('');
                        //end auto fill compliance date

                        //auto fill engine number
                        $("#engine-number").val(response.engine_number);
                        //end auto fill engine number

                        //auto fill registration number
                        $("#registration-number").val(response.registration_number);
                        //end auto fill registration number

                        //auto fill make - Nam will change this one
                        if(response.make != null) {
                            $("#make").val(response.make);
                            $("#model").attr("disabled", false);
                        }
                        //end auto fill make

                        //auto fill model
                        var model_family = null;
                        if(response.model_family != null) {
                            $.ajax({
                                type: 'POST',
                                data: {_token: token, year: $("#year").val(), make: $("#make").val()},
                                url: "/get-glasses-data/family",
                                success:function(data) {

                                    console.log('success: '+data);
                                    $("#model").find("option:gt(0)").remove();
                                    $.each(data, function(key, value) {
                                        $('#model')
                                            .append($("<option></option>")
                                                .attr("value",value)
                                                .text(value));
                                    });
                                    /** ENABLE MODEL **/
                                    $("#model").prop("disabled", false);
                                    $("#variant").find("option:gt(0)").remove();
                                    //$("#build-date").val("");

                                    $("#model").val(response.model_family);
                                    model_family = response.model_family;

                                    //autofill variant
                                    if(response.variant != null) {

                                        $.ajax({
                                            type: 'POST',
                                            data: {_token: token, year: $("#year").val(), make: $("#make").val(), model: model_family},
                                            url: "/get-glasses-data/variant",
                                            success:function(data) {
                                                console.log('success: '+data);
                                                $("#variant").find("option:gt(0)").remove();
                                                $.each(data, function(key, value) {
                                                    $('#variant')
                                                        .append($("<option></option>")
                                                            .attr("value",value)
                                                            .text(value));
                                                });
                                                /** ENABLE VARIANT **/
                                                filterYearList();
                                                filterMonthList();
                                                $("#variant").prop("disabled", false);
                                                //$("#build-date").val("");
                                                $("#variant").val(response.variant);
                                            },
                                            error: function(data){
                                                console.log('error: '+data);
                                            }
                                        });


                                        $("input#build-date").attr("disabled", false);
                                    }
                                    //end autofill variant

                                },
                                error: function(data){
                                    console.log('error: '+data);
                                }
                            });
                            filterYearList();

                            $("#variant").attr("disabled", false);
                        }
                        //end auto fill model

                        //odometer
                        $("#odometer").val(response.odometer);
                        //end of odometer

                        //$("#date").val(response.registration_expiry_date);

                        //$("#one-owner").prop("checked", response.has_one_owner);
                        //$("#service-book").prop("checked", response.has_service_book);
                        //$("#service-history").prop("checked", response.has_service_history);

                        //key location
                        $("#location-of-vehicle-key").val(response.keys_location);
                        //end of key location

                        //trade in location
                        $("#location-of-trade-in-vehicle").val(response.trade_in_location);
                        //end of trade in location

                        //$("#interested_in_vehicle").val(response.condition);

                    }
                },
                error: function(xhr) {

                }
            });

        }

    });

    /** HIDE/SHOW SECTION IN INTERESTED PURCHASING DETAILS **/
    $('#finance-owing').change(function (){
        if($('#finance-owing').find(":selected").text() == 'Yes'){
            $('#mo-finance-owing-section').children('.mo-row').hide();
            $('#mo-finance-owing-section').children('.mo-row').removeClass('hidden').fadeIn('slow');
        } else {
            $('#mo-finance-owing-section').children('.mo-row').hide();
            $('#mo-finance-owing-section').children('.mo-row').addClass('hidden').fadeIn('slow');
        }
        /*
         $('#mo-finance-owing-section').children('.mo-row').hide();
         $('#mo-finance-owing-section').children('.mo-row').toggleClass('hidden').fadeIn('slow');
         */
    });

    $('#interested-in-vehicle').change(function (){
        if($('#interested-in-vehicle').find(":selected").text() == 'New'){
            $('#mo-interested-vehicle-section').children('.mo-row').hide();
            $('#mo-interested-vehicle-section').children('.mo-row').removeClass('hidden').fadeIn('slow');
        } else {
            $('#mo-interested-vehicle-section').children('.mo-row').hide();
            $('#mo-interested-vehicle-section').children('.mo-row').addClass('hidden').fadeIn('slow');
        }
    });

    $("input#disclosure-1").change(function() {
        if($(this).is(':checked')){
            $("#mo-repairable-write-off").removeClass("hidden");
        }else{
            $("#mo-repairable-write-off").addClass("hidden");
        }
    });

});