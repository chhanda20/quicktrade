var pathname = window.location.pathname;


/** DISABLE OR ENABLE SEARCH CONTACT FIELD **/
if ($("#new-contact").is(":checked")) {
    $("#user-search-contact").prop("disabled", true);
}
$("input[name='user-contact']").click(function () {
    if ($(this).val() == "new-contact") {
        $("#user-search-contact").prop("disabled", true);
    }
    else {
        $("#user-search-contact").prop("disabled", false);
    }
});

/** DISABLE OR ENABLE SEARCH CONTACT FIELD **/

/** VALIDATE GREATER THAN **/
$.validator.addMethod("greaterThan", function (value, element, param) {
    var $element = $(element)
        , $min;

    if (typeof(param) === "string") {
        $min = $(param);
    } else {
        $min = $("#" + $element.data("min"));
    }

    if (this.settings.onfocusout) {
        $min.off(".validate-greaterThan").on("blur.validate-greaterThan", function () {
            $element.valid();
        });
    }
    return parseInt(value) > parseInt($min.val());
}, "Max must be greater than min");

$.validator.addClassRules({
    greaterThan: {
        greaterThan: true
    }
});

/**  START VALIDATE USER  **/

$("#mo-create-user-form").validate({
    rules: {
        "user-code": "required",
        "user-firstname": "required",
        "user-address-1": "required",
        "user-country": "required",
        "user-suburb": "required",
        "user-state": "required",
        "user-mobile-number": "required",
        "user-email": {
            "required": true,
            "email": true
        },
        "user-privacy": "required",
        "user-password": {
            "required": function (e) {
                if (pathname.indexOf("/dealership/create-user") != -1) {
                    return true;
                }
                else {
                    return false;
                }
            },
            "minlength": 6
        },
        "range_from": "required"

    },
    highlight: function (element) {
        $(element).closest('.input-group').addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).closest('.input-group').removeClass('has-error');
    },
    messages: {
        "user-code": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter code</span>" + "</div>",
        "user-firstname": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user first name</span>" + "</div>",
        "user-address-1": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user address</span>" + "</div>",
        "user-country": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user country</span>" + "</div>",
        "user-suburb": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user suburb</span>" + "</div>",
        "user-state": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user state</span>" + "</div>",
        "user-email": {
            "required": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user email</span>" + "</div>",
            "email": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter valid email</span>" + "</div>"
        },
        "user-mobile-number": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user mobile number</span>" + "</div>",
        "user-privacy": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user privacy</span>" + "</div>",
        "user-password": {
            // "required": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user password</span>" + "</div>",
            "required": function (e) {
                if (pathname.indexOf("/dealership/create-user") != -1) {
                    return "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter user password</span>" + "</div>";
                } else {
                    return "";
                }
            },
            "minlength": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>Please enter more than 6 characters</span>" + "</div>"

        },
        "range_to": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>This field have to be greater than 'From'</span>" + "</div>",
        "range_from": "<div class='mo-error-wrapper'><img src='/images/svg/main/ic-warning.svg'>" + "<span>This field is not empty</span>" + "</div>"
    },
    submitHandler: function (form) {
        form.submit();
    }
});

/**  END VALIDATE USER  **/

/** SEARCH CONTACT **/

$("#user-search-contact").autocomplete({
    source: "/dealership/search-users",
    minLength: 2,
    select: function (event, ui) {
        console.log(ui.item.value2);
        $("#user-email").val(ui.item.value2.email);
        $("#user-firstname").val(ui.item.value2.first_name);
        $("#user-lastname").val(ui.item.value2.last_name);
        $("#user-mobile-number").val(ui.item.value2.mobile_phone);
        $("#user-address-1").val(ui.item.value2.address1);
        $("#user-address-2").val(ui.item.value2.address2);
        $("#user-company-name").val(ui.item.value2.company_name);
        $("#user-email").val(ui.item.value2.email);
        $("input[name='contact-id']").val(ui.item.value2.id);
        $("#user-title").val(ui.item.value2.title);
        $("#user-gender").val(ui.item.value2.gender);
        $("#user-suburb").val(ui.item.value2.suburb);
        $("#user-postcode").val(ui.item.value2.postcode);

        //auto fill country and stage
        $("#user-country").val(ui.item.value2.country);
        $.autoLoadCountryState();
        $("#user-state").val(ui.item.value2.state);
        //end auto fill country and stage

        $("#user-privacy").val(ui.item.value2.privacy_flag);
        $("#user-note").text(ui.item.value2.note);
    }
});

/** END SEARCH CONTACT **/

/** AUTO FILL STATE DEPEND ON SELECTED COUNTRY **/
$.autoLoadCountryState = function () {
    var country_list = jQuery.parseJSON($("#country-list").val());
    $("#state option[value]").remove();
    $.each(country_list, function (index, value) {
        $.each(value.stateList, function (index2, value2) {
            if (parseInt($("#user-country").val()) == parseInt(value2.country_id)) {
                $("#user-state").append($("<option>", {
                    value: value2.id,
                    text: value2.name
                }));
            }
        });
    });

    $("#user-state").val($("input[name='user-state-data']").val());
}

$(document).ready(function () {
    var token = $('input[name="_token"]').val();
    var country_list = jQuery.parseJSON($("#country-list").val());

    /*$("#range_from, #range_to").keyup(function(){
        var $this = $(this);
        $this.val($this.val().replace(/[^\d+]/g, ''));
    });*/

    $("#range_from, #range_to").keyup(function(e){
        var $this = $(this);
        $this.val($this.val().replace(/^[^1-9]+/,''));
        if($this.val() > 100000 && e.keyCode != 46 && e.keyCode != 8){
            $this.val(100000);
        }
    });

    $("#include, #exclude").keyup(function(){
        var $this = $(this);
        $this.val($this.val().replace(/[^\d,+]/g, ''));
    });


    $("#state option[value]").remove();
    $.each(country_list, function (index, value) {
        $.each(value.stateList, function (index2, value2) {
            if (parseInt($("#user-country").val()) == parseInt(value2.country_id)) {
                $("#user-state").append($("<option>", {
                    value: value2.id,
                    text: value2.name
                }));
            }
        });
    });
    $("#user-state").val($("input[name='user-state-data']").val());
    $("#input-country-new-user").val($("#user-country").val());

    showDealerLocation(token);

    /*$(".mo-role-category").on("change", function(){
        var id = $(this).attr("category_id");
        if ($(this).is(":checked")) {
            $(".mo-role-list-item[data-category-id = "+id+"]").removeClass("hidden");
        } else {
            $(".mo-role-list-item[data-category-id = "+id+"]").addClass("hidden");
        }
    });*/

    $(".mo-role-category").click(function(){
        var id = $(this).attr("category_id");
        if($(this).bind('click')){
            $(".mo-role-list-item[data-category-id = "+id+"]").toggleClass("hidden");
        }
    });

    $("#user-mobile-number").keyup(function(){
        var $this = $(this);
        $this.val($this.val().replace(/[^\d+]/g, ''));
    });

});

$("#user-country").on("change", function () {
    var country_list = jQuery.parseJSON($("#country-list").val());
    $("#user-state option[value]").remove();
    $.each(country_list, function (index, value) {
        $.each(value.stateList, function (index2, value2) {
            if (parseInt($("#user-country").val()) == parseInt(value2.country_id)) {
                $("#user-state").append($("<option>", {
                    value: value2.id,
                    text: value2.name
                }));
            }
        });
    });
});
/** END AUTO FILL STATE DEPEND ON SELECTED COUNTRY **/

/** Toggle Show/Hide Password **/
(function ($) {
    $.toggleShowPassword = function (options) {

        var settings = $.extend(options);
        var control = $(settings.control);
        var field = $(settings.field);

        control.bind('click', function () {
            if (field.is(':password')) {
                $(".mo-user-show-password").hide();
                $(".mo-user-password").show();
                field.attr('type', 'text');
            } else {
                $(".mo-user-show-password").show();
                $(".mo-user-password").hide();
                control.show();
                field.attr('type', 'password');
            }
        });
    };
}(jQuery));

//Here how to call above plugin from everywhere in your application document body
$.toggleShowPassword({
    field: '#user-password',
    control: '.mo-user-show-password'
});

$.toggleShowPassword({
    field: '#user-password',
    control: '.mo-user-password'
});

$("#user-create").click(function (e) {
    console.log('inside');
    e.preventDefault();
    var formData = $("#mo-create-user-form").serialize();
    $(document).bind("ajaxStart.mine", function () {
        $(".mo-loading-image").removeClass("hidden");
    });
    var requestURL = "/dealership/create-user";
    if ($("#user-id").val() != '') {
        requestURL = "/dealership/update-user/" + $("#user-id").val();
    }

    if ($('#mo-create-user-form').valid()) {
        console.log('valid');
        $.ajax({
            type: "POST",
            url: requestURL,
            data: formData,
            success: function (data) {
                //console.log(data);
                if (data != 'success') {
                    var response = jQuery.parseJSON(data);
                    //console.log(response.detail);
                    $(".alert .mo-error-message").remove();
                    $(".alert br").remove();
                    $.each(response.detail, function (index, value) {
                        var warning_message = '<span class="mo-error-message text-capitalize">' + index + ' is ' + value + '</span><br>';
                        $(".alert").append(warning_message);
                        //console.log(value);
                    });
                    $(".alert").removeClass("hidden");
                    $("html, body").animate({
                        scrollTop: 0
                    }, 500);
                } else if (data == 'success') {
                    window.location.href = "/dealership/users-list";
                }
            },
            error: function (data) {
                //console.log(data);
            },
            complete: function () {
                $('.mo-loading-image').addClass("hidden");
            }
        });
    }

    $(document).unbind(".mine");

});
