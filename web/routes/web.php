<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*********************** LOGIN - FORGET - RESET PASSWORD ***********************/

Route::get('/', function () {
    return view('login');
});

Route::get("/login", function () {
    return view("login");
});

Route::post('/login', 'APIController@login');

Route::post('/accept-term-condition', "APIController@acceptTermAndCondition");

Route::post('/forgot-password', 'APIController@forgotPassword');
Route::get('/forgot-password', function () {
	return view('forgot-password');
});

Route::post('/reset-password', 'APIController@setNewPassword');
Route::get('/reset-password/{password_token}', function($password_token) {
	$data = array (
            "password_token" => $password_token
            );
	return view("reset-password", $data);
});

Route::get('/sign-out', 'APIController@logout');

Route::get('/mo-admin/sign-out', 'APIController@logout');

/*********************** PROFILE DETAIL SETTINGS ***********************/

Route::get("dealership/profile-setting", "ProfileSettingController@initProfileSetting");

Route::post("dealership/update-profile-setting", "ProfileSettingController@updateProfileSetting");

/*********************** HOME - CUSTOMERS ***********************/

Route::get("/home", "CustomersController@viewCustomers");

Route::get("/customers", "CustomersController@viewCustomers");

Route::post("/customers", "CustomersController@searchCustomer");

Route::get("/create-customer", "CustomersController@initAddCustomerForm");

Route::post("/create-customer", "CustomersController@addCustomer");

Route::get("/edit-customer/{type}/{id}", "CustomersController@getCustomerById");

Route::post("/edit-customer", "CustomersController@updateCustomer");

Route::get("/customer-details/{type}/{id}", "CustomersController@getCustomerById");

/** AJAX - DATA TABLE **/

Route::get("/_customers", "CustomersController@getDataOfCustomers");

Route::post("/_customers", "CustomersController@getDataOfCustomers");

/********************************** INTERACTION  ***********************************/
//Route::get("/create-interaction", "InteractionController@initAddInteraction");

Route::post("/create-interaction", "InteractionController@createInteraction");

Route::post("/view-interaction/{contact_id?}", "InteractionController@getInteraction");

Route::post("/update-interaction/{interactionId}", "InteractionController@updateInteraction");

Route::post("/delete-interaction/{interactionId}", "InteractionController@deleteInteraction");

/*********************** VALUATIONS LIST ***********************/

Route::get("/valuations/{type}", "ValuationsController@viewValuations");

Route::get("/cancel-valuation/{id}", "ValuationsController@cancelValuation");

Route::get("/delete-valuation/{id}", "ValuationsController@deleteValuation");

Route::post("/create-valuation", "ValuationsController@createValuation");

Route::get("/create-valuation/{customer_id}", "ValuationsController@initAddValuationForm");

Route::post("/search-vin", "ValuationsController@searchVin");

Route::post("/_get-sales-info", "ValuationsController@getSalesInfo");

Route::post("/_trade", "ValuationsController@trade");

Route::post("/_re-evaluate", "ValuationsController@reEvaluate");

Route::post("/receive-vehicle/{valuation_id}", "ValuationsController@tradeIn");

Route::post("/approve-revaluation", "ValuationsController@approveRevaluation");

Route::post("/reject-revaluation", "ValuationsController@rejectRevaluation");

Route::post("/approve-valuation", "ValuationsController@approveValuate");

/** AJAX - DATA TABLE **/

Route::post("/_valuations/{type}", "ValuationsController@getDataOfValuations");

Route::get("/_valuations/{type}", "ValuationsController@getDataOfValuations");

Route::post("/get-valuations-by-customer/{customer_id}", "ValuationsController@getValuationsByCustomerId");

/*********************** GLASS MODEL ***********************/
/*Route::get("/get-model-list/{make_name}", "ValuationsController@getModelList");

Route::get("/get-variant-list/{make_name}/{family}", "ValuationsController@getVariantList");

Route::get("/get-release-year-without-variant/{make_name}/{family}", "ValuationsController@getReleaseYear");

Route::get("/get-release-year/{make_name}/{family}/{variant}", "ValuationsController@getReleaseYear");

Route::get("/get-release-month-without-variant/{make_name}/{family}/{releaseYear}", "ValuationsController@getReleaseMonth");

Route::get("/get-release-month/{make_name}/{family}/{releaseYear}/{variant}", "ValuationsController@getReleaseMonth");

Route::get("/get-model-detail-without-variant/{valuationId}/{make_name}/{family}/{releaseYear}/{releaseMonth}", "ValuationsController@getModelDetail");

Route::get("/get-model-detail/{valuationId}/{make_name}/{family}/{releaseYear}/{releaseMonth}/{variant}", "ValuationsController@getModelDetail");*/

Route::post("/get-glasses-data/{selectedField}", "ValuationsController@getGlassesData");

Route::get("/get-glasses-model-detail/{glassCode}", "ValuationsController@getGlassesModelDetail");

Route::get("/get-glasses-options-list/{glassCode}", "ValuationsController@getGlassesOptionsList");

Route::get("/get-vehicle-optional-options/{vehicleId}", "ValuationsController@getVehicleOptionalOptions");

/*********************** REVIEW VALUATIONS ***********************/

Route::get("/get-inspection-items-list/{section_type}", "ReviewValuationController@getInspectionItems");

Route::post("/get-items/{section_type}", "ReviewValuationController@getInspectionItemsBySectionGroup");

Route::get("/_get-inspection-item-detail/{valuation_id}/{inspection_item_id}", "ReviewValuationController@getInspectionItemDetail");

Route::post("/update-inspection-item", "ReviewValuationController@updateInspectionItem");

Route::post("/add-inspection-item", "ReviewValuationController@addInspectionItem");

Route::post("/add-inspection-item-with-photo", "ReviewValuationController@addInspectionItemWithPhoto");

Route::get("/review-valuation/{id}/{enable_receive_vehicle?}", "ReviewValuationController@reviewValuation");

Route::get("/azure", "ReviewValuationController@connectAzure");

Route::get("/get-azure", "ReviewValuationController@getDataFromAzure");

Route::get("/list-azure", "ReviewValuationController@listAzure");

Route::post("/save-model-spec/{id}", "ReviewValuationController@saveModelSpec");

Route::post("/save-options-and-extras/{id}", "ReviewValuationController@saveOptionsAndExtras");

Route::post("/save-mechanical-details/{id}", "ReviewValuationController@saveMechanicalDetails");

Route::post("/save-summary-info/{id}", "ReviewValuationController@saveSummaryInfo");

Route::post("/upload-image", "ReviewValuationController@uploadImageToAzure");

Route::post("/upload-item-photo-tmp", "ReviewValuationController@uploadInspectionPhotoTemporary");

Route::post("/upload-image-vehicle", "ReviewValuationController@uploadImageVehicleToAzure");

Route::post("/delete-inspection-item", "ReviewValuationController@deleteItem");

Route::get("/get-image-vehicle-link", "ReviewValuationController@getImageVehicleLink");

Route::post("/re-conditioning-cost", "ReviewValuationController@reconditioningCost");

Route::post("/check-ppsr", "ReviewValuationController@checkPPSR");

Route::post("/accept-valuation/{valuation_id}", "ReviewValuationController@acceptValuation");

Route::post("/request-valuation", "ReviewValuationController@requestValuation");

Route::post("/completeValuation/{id}", "ReviewValuationController@completeValuationForSendRequest");
/*********************** DEALERSHIP **********************/

Route::post("/dealership/get-by-group", "DealershipController@getByGroup");

/*********** LOCATION ***********/
Route::get("/dealership/location", "LocationController@viewLocations");

Route::get("/dealership/init-location-form/{type}/{companyId?}", "LocationController@initLocationForm");

Route::post("/dealership/save-location/{location_id?}", "LocationController@saveLocation");

Route::post("/dealership/get-locations", "LocationController@getLocations");

Route::post("/location/get-by-company", "LocationController@getByCompany");

/** USING FOR FILTER TRADING **/
Route::post("/get-locations-by-logged-user", "LocationController@getLocationsByCurrentUserID");/* currently disable */

Route::post("/get-all-locations", "LocationController@getAllLocations");
/*********** SETTINGS ***********/
Route::get("/dealership/settings", "DealershipSettingController@initSettingDetailsForm");

Route::post("/dealership/save-settings", "DealershipSettingController@saveSettings");

/*********** ROLE ***********/
Route::get("/dealership/roles", "RoleController@viewRoles");

Route::post("/dealership/get-roles", "RoleController@getRolesList");

Route::get("/dealership/create-new-role/{type}/{roleId?}", "RoleController@initRoleForm");

Route::post("/dealership/create-new-role", "RoleController@createNewRole");

Route::post("/dealership/update-new-role/{roleId}", "RoleController@updateNewRole");

Route::post("/dealership/delete-role/{roleId}", "RoleController@deleteRole");

/********* EXTERNAL OFFER GROUP *********/

Route::get("/dealership/external-offer-groups", "ExternalOfferGroupController@viewExternalOfferGroups");

Route::get("/dealership/search-external-offer-groups", "ExternalOfferGroupController@searchExternalOfferGroupsList");

Route::post("/dealership/get-external-offer-groups", "ExternalOfferGroupController@getExternalOfferGroupsList");

Route::get("/dealership/init-external-offer-group/{type}/{externalId?}", "ExternalOfferGroupController@initExternalForm");

Route::post("/dealership/create-external-offer-group", "ExternalOfferGroupController@createExternalOfferGroup");

Route::post("/dealership/update-external-offer-group/{externalId}", "ExternalOfferGroupController@updateExternalOfferGroup");

Route::post("/dealership/remove-external-offer-group/{externalId}", "ExternalOfferGroupController@removeExternalOfferGroup");

/*********************** USERS MANAGEMENT **********************/

Route::get("/dealership/users-list", "UsersController@viewUsersList");

Route::post("/dealership/get-users/{roleId?}", "UsersController@getUsers");

Route::get("/dealership/create-user", "UsersController@initUserForm");

Route::get("/dealership/edit-user/{user_id}", "UsersController@initUserForm");

Route::post("/dealership/create-user", "UsersController@createUser");

Route::post("/dealership/update-user/{user_id}", "UsersController@createUser");

Route::post("/dealership/update-status", "UsersController@updateStatus");

Route::post("/dealership/search-users", "UsersController@searchUsers");

Route::get("/dealership/search-users", "UsersController@searchUsers");

Route::post("/dealership/search-company-users", "UsersController@searchCompanyUsers");

/*********************** START VALUATION VERSION  ***********************/

Route::post("/dealership/get-version", "ValuationsController@getValuationVersionId");

Route::post("/dealership/create-version", "ValuationsController@createValuationVersion");

Route::get("/dealership/cancel-valuation-version/{valuation_id}", "ValuationsController@cancelValuationVersion");

/*********************** EXTERNAL OFFER ***********************/

Route::post("/request-external-offer", "ExternalOffering@sendRequestExternalOffer");

Route::get("/external-offer/incoming-offers", "ExternalOffering@getIncomingExternalOffer");

Route::get("/external-offer/outgoing-offers", "ExternalOffering@getOutgoingOffers");

Route::post("/external-offer/outgoing-offers-by-status/{status}", "ExternalOffering@getOutgoingOffersByStatus");

Route::post("/external-offer/incoming-offers-by-status/{status}", "ExternalOffering@getIncomingOfferByStatus");

Route::post("/external-offer/incoming-offers-by-filters/{status}", "ExternalOffering@getIncomingOfferByFilters");

Route::post("/external-offer/outgoing-offers-by-filters/{status}", "ExternalOffering@getOutgoingOfferByFilters");

Route::post("/external-offer/get-info", "ExternalOffering@getExternalOffer");

Route::post("/external-offer/accept", "ExternalOffering@acceptBestOffer");

Route::post("/external-offer/reject", "ExternalOffering@rejectBestOffer");

Route::post("/external-offer/block", "ExternalOffering@blockOffer");

Route::post("/external-offer/bid-price", "ExternalOffering@bidPriceForExternalOffer");

Route::post("/external-offer/set-location", "ExternalOffering@setLocationForExternalOffer");

/*********************** TRADING ***********************/

/*********** STOCK LIST ***********/
Route::get("/trading/stock-list", "StockListController@viewStockList");

Route::get("/trading/showroom-vehicle-detail/{id}/{valuation_id}", "StockListController@viewVehicleDetails");

Route::post("/trading/send-to-auction", "StockListController@sendToAuction");

Route::get("/trading/vehicle-detail/{auctionId?}", "AuctionController@viewVehicleDetail");

Route::post("/trading/get-vehicle-list", "StockListController@getVehicleList");

Route::post("/trading/get-submitted-list", "StockListController@getVehicleSubmittedList");

Route::post("/trading/get-auction-times", "StockListController@getAuctionTimes");

Route::post("/trading/get-auction-info/{auctionId}", "StockListController@getAuctionInfoById");

Route::post("/trading/cancel-request-auction/{auctionId}", "StockListController@cancelAuctionRequest");

Route::post("/trading/upload-vehicle-photo/{vehicleId}/{featured?}", "StockListController@uploadPhotoToAzure");

Route::post("/trading/upload-edited-vehicle-photo", "StockListController@uploadEditedPhotoToAzure");

Route::post("/trading/delete-vehicle-photo/{vehicleName}", "StockListController@deletePhotoFromAzure");

Route::get("/trading/send-to-auction/{vehicle_id}/{auction_id?}", "StockListController@reviewBeforeSubmitForAuction");

Route::post("/trading/cancel-modifying-version", "StockListController@cancelModifyingVersion");

/*********** AUCTION ***********/

Route::get("trading/auction", "AuctionController@getAuctionList");

Route::post("trading/get-all-item", "AuctionController@getAllItem");

Route::post("trading/get-wish-list-item", "AuctionController@getWishListItem");

Route::post("trading/get-watching-item", "AuctionController@getWatchingItem");

Route::post("trading/get-recently-viewed-item", "AuctionController@getRecentlyViewedItem");

Route::post("trading/get-selling-item", "AuctionController@getSellingItem");

Route::post("trading/watching-list", "AuctionController@setWatching");

Route::delete("trading/watching-list", "AuctionController@unsetWatching");

Route::post("trading/save-max-value", "AuctionController@saveMaxValue");

Route::post("trading/buy-now", "AuctionController@buyNow");

Route::get("auction/vehicle-detail/{vehicle_id}/{valuation_id}/{auction_id}/{isSelling?}", "AuctionController@viewVehicleDetails");

/*********** LIVE AUCTION **************/

Route::get("trading/live-auction", "AuctionController@viewLiveAuctionList");

Route::post("trading/live-auction", "AuctionController@viewLiveAuctionList");

Route::post("trading/live-auction-offset", "AuctionController@viewLiveAuctionListByOffset");

Route::post("trading/live-auction/quick-bid", "AuctionController@quickBid");

Route::post("trading/live-auction/get-max-bid", "AuctionController@getMaxBidPrice");

/*********** WISH LIST ***********/

Route::get("/trading/wishlist/{type}/{wishlist_id?}", "WishListController@initWishListForm");

Route::post("/trading/create-wishlist", "WishListController@createWishList");

Route::post("/trading/update-wishlist/{wishlist_id?}", "WishListController@updateWishList");

Route::get("/trading/wishlist", "WishListController@viewWishList");

Route::post("/trading/get-wishlist", "WishListController@getWishList");

/*********** ARRIVING VEHICLE ( CHECK UI LAYOUT ) ************/
Route::get("/arriving-vehicle/arriving-list", "ArrivingVehicleController@viewArrivingList");

Route::post("/arriving-vehicle/get-data-arriving-list", "ArrivingVehicleController@getDataArrivingList");

Route::post("/arriving-vehicle/get-inspection-items", "ArrivingVehicleController@getInspectionItems");

/*
Route::get("/arriving-vehicle/additional-detail", function(){
    return view("main.arriving-vehicle.arriving-vehicle-additional-detail");
});
*/

Route::get("/arriving-vehicle/additional-detail/{transactionId}", "ArrivingVehicleController@viewAdditionalDetails");

Route::get("/arriving-vehicle/inspect/{transactionId}", 'ArrivingVehicleController@inspect');

Route::get("/arriving-vehicle/accept/{transactionId}", 'ArrivingVehicleController@accept');

Route::get("/arriving-vehicle/inspect-item/{valuationId}/{version}", 'ArrivingVehicleController@getInspectItem');

Route::get("/arriving-vehicle/option-extra/{vehicleId}", 'ArrivingVehicleController@getOptionExtra');

Route::post("/arriving-vehicle/upload-dispute-item-photo", "ArrivingVehicleController@uploadDisputeItemPhotoToAzure");

Route::post("/arriving-vehicle/upload-dispute-item-photo-tmp", "ArrivingVehicleController@uploadDisputeItemPhotoTemporary");

Route::get("/arriving-vehicle/clear-dispute-item-photo-tmp/{userId}", "ArrivingVehicleController@clearDisputeInspectionPhotoTemporary");

Route::post("/arriving-vehicle/submit-lodge-dispute", "ArrivingVehicleController@createLodgeDispute");

/*********** DISPUTE CENTRE *********************/
Route::get("/dispute/info/{disputeType}/{transactionId}/{disputeId}", "DisputeController@viewDisputeInfo");

Route::get("/dispute/{disputeType}/{type?}", "DisputeController@viewDisputeList");

Route::post("/dispute/get-dispute-data/{disputeType}", "DisputeController@getDisputeDataList");

/*********** ACCOUNTING DOCUMENTS *********************/
Route::get("/accounting/documents", function(){
    return view("main.accounting.accounting-documents");
});

/********************************** TEST DRIVES  ***********************************/

Route::post("/test-drives/upload-drive-license-photo", "TestDrivesController@uploadDriverLicensePhoto");

Route::post("/view-test-drives/{customer_id?}", "TestDrivesController@getTestDrives");

Route::get("/test-drives/search-vehicle", "TestDrivesController@viewSearchVehicle");

Route::post("/view-search-vehicle", "TestDrivesController@getSearchVehicle");

Route::get("/test-drives/inprogress", "TestDrivesController@viewInprogressList");

Route::post("/view-in-progress", "TestDrivesController@getInProgressList");

Route::get("/test-drives/complete", "TestDrivesController@viewCompleteList");

/*Route::get("/test-drives/read-comment", function(){
    return view("main.test-drives.complete-list");
});*/

Route::post("/test-drives/view-complete", "TestDrivesController@getCompleteList");

Route::post("/test-drives/complete/{testDriveId}", "TestDrivesController@completeTestDrive");

Route::get("/test-drives/create-test-drives/{customerId}", "TestDrivesController@initCreateTestDrives");

Route::post("/test-drives/create-test-drives", "TestDrivesController@createTestDrives");

Route::get("/test-drives/term-and-condition/{customerId}", "TestDrivesController@initTermAndCondition");

Route::post("/test-drives/upload-signature-photo", "TestDrivesController@uploadSignatureToAzure");

Route::get("/test-drives/select-vehicle/{vehicleId}/{vehicleName}", "TestDrivesController@selectVehicle");

Route::post("/test-drives/manual-vehicle", "TestDrivesController@addManualVehicle");

/*********** TRANSACTION ***********/
Route::get("/transaction/view-transactions/{type}", "TransactionController@viewTransactions");

Route::post("/transaction/get-transactions/{type}/{transactionType?}", "TransactionController@getTransactions");

Route::get("/transaction/current-info", function(){
    return view("main.transaction.transaction-current-more-info");
});

Route::get("/transaction/transaction-detail/{type}/{transactionId}", "TransactionController@transactionDetails");

Route::post("/transaction/transport-ready", "TransactionController@readyForTransport");

Route::post("/transaction/inspect", "TransactionController@inspect");

Route::post("/transaction/upload-document/{transactionId}/{documentCode}", "TransactionController@uploadDocument");


/******************* DEALERSHIP DASHBOARD ****************/

Route::get("/dashboard", "DashboardController@viewDashboard");

Route::post("/dashboard", "DashboardController@getDashBoard");

Route::post("/view-inprogress-test-drive", "DashboardController@getTestDriveInprogress");

Route::post("/view-request-valuation", "DashboardController@getValuationRequest");

Route::post("/view-pending-approval-valuation", "DashboardController@getValuationPending");



/*********************************************************************
 *********************** MO-ADMIN ***********************************
 *********************************************************************/

Route::get("/mo-admin", function() {
	return view("/mo-admin/login");
});

Route::get("/mo-admin/login", function() {
	return view("/mo-admin/login");
});

Route::post("/mo-admin/login", "MOAdmin\\AuthenticationController@login");

/*********************** DEALERSHIP ***********************/

Route::get("/mo-admin/create-dealership/{type}/{companyId?}", "MOAdmin\\DealershipController@initCompanyForm");

Route::post("/mo-admin/create-dealership", "MOAdmin\\DealershipController@createCompany");

Route::post("/mo-admin/update-dealership/{companyId}", "MOAdmin\\DealershipController@updateCompany");

Route::get("/mo-admin/dealership-list", "MOAdmin\\DealershipController@viewCompanies");

Route::get("/mo-admin/get-companies", "MOAdmin\\DealershipController@getCompanies");

Route::post("/mo-admin/get-companies", "MOAdmin\\DealershipController@getCompanies");

Route::post("/mo-admin/suspend-company/{company_id}", "MOAdmin\\DealershipController@suspendComapny");

/*********************** DEALER GROUP ***********************/
Route::get("/mo-admin/create-dealer-group/{type}/{company_group_id?}", "MOAdmin\\DealerGroupController@initCompanyGroupForm");

Route::get("/mo-admin/dealer-groups-list", "MOAdmin\\DealerGroupController@viewCompanyGroups");

Route::post("/mo-admin/get-dealer-groups", "MOAdmin\\DealerGroupController@getCompanyGroups");

Route::post("/mo-admin/create-dealer-group", "MOAdmin\\DealerGroupController@createCompanyGroup");

Route::post("/mo-admin/update-dealer-group/{companyGroupId}", "MOAdmin\\DealerGroupController@updateCompanyGroup");

/*********************** SETTINGS ***********************/
Route::get("/mo-admin/settings/valuation-settings", "MOAdmin\\SettingController@initValuationSettingDetailsForm");

Route::get("/mo-admin/settings/external-offering-settings", "MOAdmin\\SettingController@initExternalOfferingSettingDetailsForm");

Route::get("/mo-admin/settings/auction-settings", "MOAdmin\\SettingController@initAuctionSettingDetailsForm");

Route::get("/mo-admin/settings/rating-settings", "MOAdmin\\SettingController@initRatingSettingDetailsForm");

/*Route::get("/mo-admin/general-settings", "MOAdmin\\SettingController@initSettingDetailsForm");*/

Route::post("/mo-admin/settings/save-settings/{settingType}", "MOAdmin\\SettingController@saveSettings");

Route::get("/mo-admin/settings/general", "MOAdmin\\SettingController@initGeneralSetting");

Route::post("/mo-admin/settings/general", "MOAdmin\\SettingController@updateTermAndCondition");

/*********************** EDIT PERMISSION ***********************/

Route::get('mo-admin/edit-permission','MOAdmin\\PermissionController@initEditPermission');

Route::post('mo-admin/update-permission', 'MOAdmin\\PermissionController@updatePermission');

/*********************** USER ***********************/

Route::get('mo-admin/user/list','MOAdmin\\UserController@viewUsersList');

Route::get('mo-admin/user/create','MOAdmin\\UserController@createForm');

Route::get('mo-admin/user/edit/{userId}','MOAdmin\\UserController@createForm');

Route::post('mo-admin/user/saveUser/{userId?}','MOAdmin\\UserController@saveUser');

Route::post("mo-admin/user/get-users/{roleId?}", "MOAdmin\\UserController@getUsers");

/*********************** START MO-ADMIN AUCTION TRADING (TRADING SECTION) ***********************/

Route::get('/mo-admin/trading/create-auction-session/{type}/{sessionId?}', "MOAdmin\\AuctionSessionController@initAuctionSession");

Route::post('/mo-admin/trading/create-auction-session', "MOAdmin\\AuctionSessionController@createAuctionSession");

Route::post('/mo-admin/trading/update-auction-session/{sessionId}', "MOAdmin\\AuctionSessionController@updateAuctionSession");

Route::get('/mo-admin/trading/auction-session-list', "MOAdmin\\AuctionSessionController@viewAuctionSession");

Route::get('/mo-admin/trading/auction-history', "MOAdmin\\AuctionSessionController@viewAuctionHistory");

Route::get('/mo-admin/trading/auction-history-detail/{auctionSessionId}', "MOAdmin\\AuctionSessionController@viewHistoryDetail");

Route::post('/mo-admin/trading/get-auction-session-list', "MOAdmin\\AuctionSessionController@getAllAuctionSession");

Route::post('/mo-admin/trading/get-auction-session-history-list', "MOAdmin\\AuctionSessionController@getAllAuctionSessionHistory");

/************************** REQUEST LIST **************************************************/

Route::get('/mo-admin/trading/request-list', "MOAdmin\\AuctionSessionController@showRequestList");

Route::post('/mo-admin/trading/request-list', "MOAdmin\\AuctionSessionController@showRequestList");

Route::post('/mo-admin/trading/request-list-offset', "MOAdmin\\AuctionSessionController@showRequestListByOffset");

Route::post('/mo-admin/trading/approve-request-list-offset', "MOAdmin\\AuctionSessionController@approveRequestList");

Route::post('/mo-admin/trading/reject-request-list-offset', "MOAdmin\\AuctionSessionController@rejectRequestList");

Route::get("/mo-admin/trading/showroom-vehicle-detail/{id}/{valuation_id}", "StockListController@viewVehicleDetails");

/*********************** MO ADMIN - START LIVE AUCTION ***********************/

Route::get('/mo-admin/trading/live-auction', "MOAdmin\\AuctionSessionController@viewLiveAuctionList");

Route::post('/mo-admin/trading/live-auction', "MOAdmin\\AuctionSessionController@viewLiveAuctionList");

Route::get("/mo-admin/trading/get-live-auction-timer", "MOAdmin\\AuctionSessionController@getLiveAuctionTimer");

/*********************** MO ADMIN - END LIVE AUCTION ***********************/

/********************************** AUCTION SETTING  ***********************************/
/*Route::get("/mo-admin/trading/setting", "MOAdmin\\AuctionTradingSettingController@initAuctionSettings");

Route::post("/mo-admin/trading/update-trading-setting", "MOAdmin\\AuctionTradingSettingController@updateAuctionSetting");*/

/*************** DO AZURE DEMO *******************/
Route::get("/azure/demo", "VehiclePhotoController@doDemoAzure");
Route::post("/azure/upload", "VehiclePhotoController@uploadPhoto");
/*************** end DO AZURE DEMO **************/

/*********** DISPUTE CENTRE ***********/
Route::get("/mo-admin/dispute/info/{transactionId}/{disputeId}", "MOAdmin\\DisputeController@viewDisputeInfo");

Route::post("/mo-admin/dispute/create-note", "MOAdmin\\DisputeController@createNote");

Route::post("/mo-admin/dispute/delete-note", "MOAdmin\\DisputeController@deleteNote");

Route::post("/mo-admin/dispute/resolve", "MOAdmin\\DisputeController@resolve");

Route::post("/mo-admin/dispute/under-review", "MOAdmin\\DisputeController@underReview");

Route::get("/mo-admin/review-valuation/{id}/{enable_receive_vehicle?}", "ReviewValuationController@reviewValuation");

Route::get('/mo-admin/dispute/show', "MOAdmin\\DisputeController@showList");

Route::post('/mo-admin/dispute/list', "MOAdmin\\DisputeController@getList");

Route::post('/mo-admin/dispute/accept', "MOAdmin\\DisputeController@accept");

Route::post("/delete-interaction/{interactionId}", "InteractionController@deleteInteraction");

/******************* MO ADMIN - START LOCATION ****************/
//Route::get("/mo-admin/create-location", function(){
//    return view("mo-admin.main.create-location");
//});

Route::get("/mo-admin/location-list", "MOAdmin\\LocationController@viewLocation");

Route::post("/mo-admin/get-location-list", "MOAdmin\\LocationController@getLocations");

Route::get("/mo-admin/init-location-form/{type}/{location_id?}", "MOAdmin\\LocationController@initLocationForm");

Route::post("/mo-admin/save-location/{locationId?}", "MOAdmin\\LocationController@saveLocation");

Route::post("mo-admin/location/dataAccessLocation", "MOAdmin\\LocationController@dataAccessLocation");

Route::post("mo-admin/location/dealerAccessLocation", "MOAdmin\\LocationController@dealerAccessLocation");

/******************* MO ADMIN - START ROLE ****************/
//Route::get("/mo-admin/create-role", function(){
//    return view("mo-admin.main.create-role");
//});

Route::get("/mo-admin/role-list", "MOAdmin\\RoleController@viewRoleList");

Route::post("/mo-admin/get-role-list", "MOAdmin\\RoleController@getRoleList");

Route::post("mo-admin/role/getByCompanyGroup", "MOAdmin\\RoleController@getByCompanyGroup");

Route::get("/mo-admin/create-role/{type}/{roleId?}", "MOAdmin\\RoleController@initCreateNewRole" );

Route::post("/mo-admin/create-role", "MOAdmin\\RoleController@createNewRole");

Route::post("/mo-admin/update-role/{roleId}", "MOAdmin\\RoleController@updateNewRole");

Route::post("/mo-admin/delete-role/{roleId}", "MOAdmin\\RoleController@deleteRole");

/******************* TRANSACTION ****************/
/*Route::get("/mo-admin/transaction-current-info", function(){
    return view("mo-admin.main.transaction.transaction-current-info");
});*/

Route::get("/mo-admin/transaction-current", function(){
    return view("mo-admin.main.transaction.transaction-current");
});

Route::get("/mo-admin/transaction/transaction-detail/{type}/{transactionId}", "MOAdmin\\TransactionController@transactionDetails");

Route::get("/mo-admin/transaction/view-transactions/{type}", "MOAdmin\\TransactionController@viewTransactions");

Route::post("/mo-admin/transaction/get-transactions/{type}/{statusType?}", "MOAdmin\\TransactionController@getTransactions");

Route::post("/mo-admin/transaction/receive-payment", "MOAdmin\\TransactionController@receivePayment");

Route::post("/mo-admin/transaction/book-transport", "MOAdmin\\TransactionController@bookTransport");

Route::post("/mo-admin/transaction/pick-up-vehicle", "MOAdmin\\TransactionController@pickUpVehicle");

Route::post("/mo-admin/transaction/shipped-vehicle", "MOAdmin\\TransactionController@shippedVehicle");

Route::post("/mo-admin/transaction/awaiting-document", "MOAdmin\\TransactionController@awaitingDocument");

Route::post("/mo-admin/transaction/complete", "MOAdmin\\TransactionController@completeTransaction");

Route::post("/mo-admin/transaction/get-statuses", "MOAdmin\\TransactionController@getStatuses");

Route::get("/mo-admin/transaction/setting-fees", "MOAdmin\\TransactionController@getTransactionFee");

Route::post("/mo-admin/transaction/edit-fees", "MOAdmin\\TransactionController@setTransactionFee");

/*********************** VALUATIONS LIST ***********************/

Route::get("/mo-admin/valuations/{type}", "MOAdmin\\ValuationController@viewValuations");

Route::post("/mo-admin/get-valuations-data/{type}", "MOAdmin\\ValuationController@getDataOfValuations");

